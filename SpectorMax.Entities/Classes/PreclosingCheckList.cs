﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class PreclosingPropertyDetails
    {
        public string InspectionOrderId { get; set; }
        public string RequesterFirstName { get; set; }
        public string RequesterLastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
