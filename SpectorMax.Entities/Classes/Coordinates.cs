﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class Coordinates
    {
        public string xc { get; set; }
        public string y { get; set; }
        public string w { get; set; }
        public string h { get; set; }
        public string IsCropped { get; set; }
    }
}
