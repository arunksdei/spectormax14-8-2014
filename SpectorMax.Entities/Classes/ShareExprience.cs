﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class ShareExprience
    {
        public string ID { get; set; }
        public string UserId { get; set; }
        public string ReportNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Comments { get; set; }
        public int StateId { get; set; }
        public string State { get; set; }
        public int? Role { get; set; }
        public bool? IsActive { get; set; }
    }

    public class LinksAndShareExeperience
    {
        public List<ShareExprience> objlstShareExprience { get; set; }
        public List<SpectorMaxDAL.applicationlink> objApplicationLinks { get; set; }
    }
}
