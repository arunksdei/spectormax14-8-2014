﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class ApplicationLinks
    {
        public int ID { get; set; }
        public int ColNUm { get; set; }
        public int? Type { get; set; }

        public string PageName { get; set; }
        public string LinkName { get; set; }
        public string LinkAddress { get; set; }
    }

    public class ApplicationLinksList
    {
        public List<ApplicationLinks> lstApplicationLinksList { get; set; }
    }
}
