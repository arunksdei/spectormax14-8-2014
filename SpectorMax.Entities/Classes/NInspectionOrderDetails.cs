﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpectorMaxDAL;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace SpectorMax.Entities.Classes
{
   public class NInspectionOrderDetails
    {
        public string InspectionOrderId { get; set; }

        [Required(ErrorMessage = "Please Enter Requested First Name")]
        public string RequestedFirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Requested Last Name")]
        public string RequestedLastName { get; set; }
        [Required(ErrorMessage = "Please Enter Requested By Phone Number")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Please Enter Requested By Address")]
        public string RequestedByAddress { get; set; }
        [Required(ErrorMessage = "Please Enter Requested By City")]
        public int? RequestedByCity { get; set; }
        [Required(ErrorMessage = "Please Enter Requested By State")]
        public int? RequestedByState { get; set; }
        [Required(ErrorMessage = "Please Enter Requested By Zip")]
        public int? RequestedByZip { get; set; }
        [Required(ErrorMessage = "Please Enter RequestedBy Email Address")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string RequestedByEmailAddress { get; set; }
        public int? RequesterType { get; set; }
        //[Required(ErrorMessage = "Please Check RequestedBy EmailContractClient")]
        public bool RequestedByEmailContractClient { get; set; }
        [Required(ErrorMessage = "Please Enter Property Address")]
        public string PropertyAddress { get; set; }
        [Required(ErrorMessage = "Please Enter Property Address City")]
        public int? PropertyAddressCity { get; set; }
        [Required(ErrorMessage = "Please Enter Property Address State")]
        public int? PropertyAddressState { get; set; }
        [Required(ErrorMessage = "Please Enter Property Address Zip")]
        public int? PropertyAddressZip { get; set; }
     
        public string OwnerName { get; set; }
        [Required(ErrorMessage = "Please Enter Owner Phone Number")]
        public string OwnerPhoneNumber { get; set; }
        
      
        [DataType(DataType.EmailAddress)]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string OwnerEmailAddress { get; set; }
       [Required(ErrorMessage = "Please Select Property Address type")]

        public int? PropertyDescription { get; set; }
        [Required(ErrorMessage = "Please Enter Property # of Units")]
        //[DataType(DataType.PhoneNumber)]
        public int? PropertyUnit { get; set; }
        [Required(ErrorMessage = "Please Enter Property Square Footage")]
        public float? PropertySquareFootage { get; set; }
        [Required(ErrorMessage = "Please Enter Property Addition")]
        public string PropertyAddition { get; set; }
        [Required(ErrorMessage = "Please Enter Property BedRoom")]
        public int? PropertyBedRoom { get; set; }
        [Required(ErrorMessage = "Please Enter Property Baths")]
        public int? PropertyBaths { get; set; }
        [Required(ErrorMessage = "Please Enter Property Estimated Monthly Utilites")]
        public float? PropertyEstimatedMonthlyUtilites { get; set; }
        [Required(ErrorMessage = "Please Enter Property Home Age")]
        public int? PropertyHomeAge { get; set; }
        [Required(ErrorMessage = "Please Enter Property RoofAge")]
        public float? PropertyRoofAge { get; set; }

        public string Direction { get; set; }
        public bool? IsAcceptedOrRejected { get; set; }
        public bool? IsRejectedByInspector { get; set; }
        public NSellerAgent sellerAgent { get; set; }
        public NBuyerAgent buyerAgent { get; set; }
        public DateTime? ScheduleInspectionDate { get; set; }

        public IEnumerable<SelectListItem> StatesList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> ZipCodesList { get; set; }
        public IEnumerable<SelectListItem> PropertyDescriptionList { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime? ScheduledDate { get; set; }
    }
   public class NSellerAgent
   {
       public string AgentID { get; set; }

       public string InspectionOrderID { get; set; }

       public string AgentType { get; set; }
       [Required(ErrorMessage = "Please Enter SellerAgent FirstName")]
       public string SellerAgentFirstName { get; set; }

       [Required(ErrorMessage = "Please Enter SellerAgentLastName")]
       public string SellerAgentLastName { get; set; }

      
       public string SellerAgentOffice { get; set; }
       [Required(ErrorMessage = "Please Enter SellerAgent Phone Day")]

       public string SellerAgentPhoneDay { get; set; }
       
       [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
       public string SellerAgentEmailAddress { get; set; }

      
       public string SellerAgentStreetAddress { get; set; }

       //[Required(ErrorMessage = "Please Enter SellerAgent City")]
       public int? SellerAgentCity { get; set; }
       //[Required(ErrorMessage = "Please Enter SellerAgent State")]
       public int? SellerAgentState { get; set; }
       //[Required(ErrorMessage = "SellerAgent Zip")]
       public int? SellerAgentZip { get; set; }






   }
   public class NBuyerAgent
   {
       public string AgentID { get; set; }
       public string InspectionOrderID { get; set; }
       public string AgentType { get; set; }
       [Required(ErrorMessage = "Please Enter BuyerAgent FirstName")]
       public string BuyerAgentFirstName { get; set; }

       [Required(ErrorMessage = "Please Enter BuyerAgent LastName")]
       public string BuyerAgentLastName { get; set; }

    
       public string BuyerAgentOffice { get; set; }

       
       public string BuyerAgentStreetAddress { get; set; }

       //[Required(ErrorMessage = "Please Enter BuyerAgent City")]
       public int? BuyerAgentCity { get; set; }

       //[Required(ErrorMessage = "Please Enter BuyerAgent State")]
       public int? BuyerAgentState { get; set; }

       //[Required(ErrorMessage = "Please Enter BuyerAgent Zip")]
       public int? BuyerAgentZip { get; set; }

     
       [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
       public string BuyerAgentEmailAddress { get; set; }



       [Required(ErrorMessage = "Please Enter BuyerAgent Phone Day")]

       public string BuyerAgentPhoneDay { get; set; }


   }

   public class NRequesterDetails
   {
       [Required(ErrorMessage = "Please Enter Requested First Name")]
       public string RequestedFirstName { get; set; }
       [Required(ErrorMessage = "Please Enter Requested Last Name")]
       public string RequestedLastName { get; set; }
       [Required(ErrorMessage = "Please Enter Requested By Phone Number")]
       public string PhoneNumber { get; set; }
       [Required(ErrorMessage = "Please Enter Requested By Address")]
       public string RequestedByAddress { get; set; }
       [Required(ErrorMessage = "Please Enter Requested By City")]
       public int? RequestedByCity { get; set; }
       [Required(ErrorMessage = "Please Enter Requested By State")]
       public int? RequestedByState { get; set; }
       [Required(ErrorMessage = "Please Enter Requested By Zip")]
       public int? RequestedByZip { get; set; }
       [Required(ErrorMessage = "Please Enter RequestedBy Email Address")]
       [DataType(DataType.EmailAddress)]
       [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
       public string RequestedByEmailAddress { get; set; }

       public virtual string RequesterType { get; set; }

       public IEnumerable<SelectListItem> StatesList { get; set; }
       public IEnumerable<SelectListItem> CityList { get; set; }
       public IEnumerable<SelectListItem> ZipCodesList { get; set; }
       public IEnumerable<SelectListItem> PropertyDescriptionList { get; set; }
   }

   public class NPropertyInfo
   {

       [Required(ErrorMessage = "Please Enter Property Address")]
       public string PropertyAddress { get; set; }
       [Required(ErrorMessage = "Please Enter Property Address City")]
       public int PropertyAddressCity { get; set; }
       [Required(ErrorMessage = "Please Enter Property Address State")]
       public int PropertyAddressState { get; set; }
       [Required(ErrorMessage = "Please Enter Property Address Zip")]
       public int PropertyAddressZip { get; set; }

       public string OwnerName { get; set; }
      
       public string OwnerPhoneNumber { get; set; }


       [DataType(DataType.EmailAddress)]
       [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
       public string OwnerEmailAddress { get; set; }
       
       public int PropertyDescription { get; set; }
       [Required(ErrorMessage = "Please Enter Property # of Units")]
       //[DataType(DataType.PhoneNumber)]
       public int PropertyUnit { get; set; }
       [Required(ErrorMessage = "Please Enter Property Square Footage")]
       public float PropertySquareFootage { get; set; }
       //[Required(ErrorMessage = "Please Enter Property Addition")]
       public string PropertyAddition { get; set; }
       [Required(ErrorMessage = "Please Enter Property BedRoom")]
       public int PropertyBedRoom { get; set; }
       [Required(ErrorMessage = "Please Enter Property Baths")]
       public int PropertyBaths { get; set; }
       [Required(ErrorMessage = "Please Enter Property Estimated Monthly Utilites")]
       public float PropertyEstimatedMonthlyUtilites { get; set; }
       [Required(ErrorMessage = "Please Enter Property Home Age")]
       public int PropertyHomeAge { get; set; }
       [Required(ErrorMessage = "Please Enter Property RoofAge")]
       public float PropertyRoofAge { get; set; }
       public string Direction { get; set; }
       public IEnumerable<SelectListItem> StatesList { get; set; }
       public IEnumerable<SelectListItem> CityList { get; set; }
       public IEnumerable<SelectListItem> ZipCodesList { get; set; }
       public IEnumerable<SelectListItem> PropertyDescriptionList { get; set; }
   }

   public class NAgentInfo
   {
       public string SellerAgentID { get; set; }

       public string InspectionOrderID { get; set; }

       public string AgentType { get; set; }
       [Required(ErrorMessage = "Please Enter SellerAgent FirstName")]
       public string SellerAgentFirstName { get; set; }

       [Required(ErrorMessage = "Please Enter SellerAgentLastName")]
       public string SellerAgentLastName { get; set; }

       //[Required(ErrorMessage = "Please Enter SellerAgent Office")]
       public string SellerAgentOffice { get; set; }
       [Required(ErrorMessage = "Please Enter SellerAgent Phone Day")]

       public string SellerAgentPhoneDay { get; set; }

       [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
       public string SellerAgentEmailAddress { get; set; }


       public string SellerAgentStreetAddress { get; set; }

       [Required(ErrorMessage = "Please Enter SellerAgent City")]
       public int SellerAgentCity { get; set; }
       [Required(ErrorMessage = "Please Enter SellerAgent State")]
       public int SellerAgentState { get; set; }
       [Required(ErrorMessage = "SellerAgent Zip")]
       public int SellerAgentZip { get; set; }


       public string BuyerAgentID { get; set; }
       
       [Required(ErrorMessage = "Please Enter BuyerAgent FirstName")]
       public string BuyerAgentFirstName { get; set; }

       [Required(ErrorMessage = "Please Enter BuyerAgent LastName")]
       public string BuyerAgentLastName { get; set; }

       //[Required(ErrorMessage = "Please Enter BuyerAgent Office")]
       public string BuyerAgentOffice { get; set; }


       public string BuyerAgentStreetAddress { get; set; }

       [Required(ErrorMessage = "Please Enter BuyerAgent City")]
       public int BuyerAgentCity { get; set; }

       [Required(ErrorMessage = "Please Enter BuyerAgent State")]
       public int BuyerAgentState { get; set; }

       [Required(ErrorMessage = "Please Enter BuyerAgent Zip")]
       public int BuyerAgentZip { get; set; }


       [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
       public string BuyerAgentEmailAddress { get; set; }



       [Required(ErrorMessage = "Please Enter BuyerAgent Phone Day")]

       public string BuyerAgentPhoneDay { get; set; }
       public IEnumerable<SelectListItem> StatesList { get; set; }
       public IEnumerable<SelectListItem> CityList { get; set; }
       public IEnumerable<SelectListItem> ZipCodesList { get; set; }

   }

   public class NScheduleInspection
   {
       public DateTime ScheduleInspectionDate { get; set; }
   }
}
