﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpectorMaxDAL;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SpectorMax.Entities.Classes
{
    public class NInspectionOrder
    {
        public string InspectionOrderId { get; set; }
        public string InspectorId { get; set; }
        public string RequesterID { get; set; }
        public string InspectionStatus { get; set;}
        public DateTime ScheduledDate { get; set; }
        public bool AssignedByAdmin { get; set; }
        public bool IsAcceptedOrRejected { get; set; }
        public bool IsRejectedByInspector { get; set; }
        public bool IsUpdatable { get; set; }
        public string ReportNo { get; set; }
        public DateTime Createddate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string DeletedBy { get; set; }
        public bool IsActive { get; set; }

    }
}
