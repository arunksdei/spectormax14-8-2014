﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpectorMax;

namespace SpectorMax.Entities.Classes
{
    public class CommonFunctions
    {
       
        public static Dictionary<int, string> PerPageRecordlist()
        {
            return new Dictionary<int, string>()
            {
                { 20, "20"},
                { 40, "40"},
                { 60, "60"},
                { 80, "80"},
                { 100, "100"}
            };
        }

    }
}