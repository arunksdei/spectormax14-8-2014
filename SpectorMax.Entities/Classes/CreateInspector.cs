﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace SpectorMax.Entities.Classes
{
    public class CreateInspector
    {
        public string InspectorDetailsID { get; set; }
        public string UserID { get; set; }
        //[Required(ErrorMessage = "Please Upload your photo")]
        public string PhotoURL { get; set; }

     //   [Required(ErrorMessage = "Please enter area where you performs inspections")]
        public string ExperienceTraining1 { get; set; }

     //   [Required(ErrorMessage = "Please enter experience")]
        public string ExperienceTraining2 { get; set; }
        //[Required(ErrorMessage = "Please enter education")]
        public string ExperienceTraining3 { get; set; }
        //[Required(ErrorMessage = "Please Upload your certificate")]
        public string Certification1 { get; set; }
        //[Required(ErrorMessage = "Please Upload your certificate")]
        public string Certification2 { get; set; }
        //[Required(ErrorMessage = "Please Upload your certificate")]
        public string Certification3 { get; set; }

        public int? PaymentPercentage { get; set; }
        public DateTime? PaymentCycleStartDate { get; set; }
        public DateTime? PaymentCycleEndDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        [Required(ErrorMessage = "Please Enter Inspector First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Inspector Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please Enter Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please Enter Address")]
        public string StreetAddress { get; set; }
     //   [Required(ErrorMessage = "Please Enter Address")]
        public string Address2 { get; set; }
        [Required(ErrorMessage = "Please Enter State")]
        public string State { get; set; }
        [Required(ErrorMessage = "Please Enter City")]
        public string City { get; set; }

         [Required(ErrorMessage = "Please Enter State")]
        public int? StateId { get; set; }
        [Required(ErrorMessage = "Please Enter City")]
        public int? CityId { get; set; }

        [Required(ErrorMessage = "Please Enter Phone")]
        public string PhoneNo { get; set; }
        [Required(ErrorMessage = "Please Enter Country")]
        public string Country { get; set; }
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        [Required(ErrorMessage = "Please Enter Email Address")]
        //[Remote("")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Please Select Date Of Birth")]
        public DateTime? dateofbirth { get; set; }
        [Required(ErrorMessage = "Date Of Birth date is required")]
        public string DOB { get; set; }

        [Required(ErrorMessage = "Please enter zipcode")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Only numbers allowed")]
        public int Zipcode { get; set; }
        [Required(ErrorMessage = "Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }
        public IEnumerable<SelectListItem> StatesList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> ZipCodesList { get; set; }
        public bool IsShowOnHomePage { get; set; }
    }
    public class AdminInspectorDetails
    {
        public List<CreateInspector> InspectorData { get; set; }
    }
}
