﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace SpectorMax.Entities.Classes
{
    public class Report
    {
        public string PaymentId { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? Zip { get; set; }
        public string OrderID { get; set; }
        public string Inspectionstatus { get; set; }
        public bool? IsRefunded { get; set; }
        public string roleName { get; set; }

        public DateTime? InspectionDate { get; set; }
        public float? AmountReceived { get; set; }
        public float? PlanAmount { get; set; }
        public bool? isrequestfromAdmin { get; set; }

        public bool? isreportviewed { get; set; }

        public string PaymentPlan { get; set; }

        public string PlanName { get; set; }

        public List<DateTime?> fromandtodate { get; set; }
        //public List<String> UsrRole { get; set; }
        public DateTime? date { get; set; }
        public List<RoleAndDate> RoleDate { get; set; }
        public RoleAndDatePayment RoleDatePayment { get; set; }
        public string InspectorName { get; set; }
        public string ReportNo { get; set; }
        public int? InspectorPercentage { get; set; }
        public float? PaymentSum { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.userinfo> InspectorDetails { get; set; }
        public bool? IsAccepted { get; set; }
        public string ReportViewdUser { get; set; }
        public double PaymentAmount { get; set; }
        public bool? IsRejectedByInspector { get; set; }
        public bool? IsAcceptedOrRejected { get; set; }
        public int? InvoiceNo {get;set;}
        public DateTime? ScheduledDate { get;set; }
    }

    public class InspectorReport
    {
        public List<Report> ReportData { get; set; }
        public Dates fromandtodate { get; set; }
        public List<PropertyAddress> objPropertyAddress { get; set; }
        public IEnumerable<SelectListItem> InspectorList { get; set; }
        // public String UsrRole { get; set; }
    }
    public class RoleAndDate
    {
        public String UsrRole { get; set; }
        public DateTime? fromdate { get; set; }
        public float? PaymentAmount { get; set; }
        public string PaymentType { get; set; }
        public string UserName { get; set; }
    }
    public class RoleAndDatePayment
    {
        public String UsrRole { get; set; }
        public DateTime? fromdate { get; set; }
        public float? PaymentAmount { get; set; }
        public string PaymentType { get; set; }
    }
}
