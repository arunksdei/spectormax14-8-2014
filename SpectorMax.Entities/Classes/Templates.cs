﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
   public class Templates
   {
       public int TemplateId { get; set; }
       public string TemplateName { get; set; }
       public string TemplateDescription { get; set; }
       public int Rate { get; set; }

   }
}
