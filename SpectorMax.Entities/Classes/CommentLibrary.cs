﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class CommentLibrary
    {
        public string MainID { get; set; }
        public string SectionName { get; set; }
        public string SubSectionID { get; set; }
        public string SubSectionName { get; set; }
        public string ItemId { get; set; }
        public string CommentBoxName { get; set; }
        public string CommentBoxText { get; set; }
        public List<AdminCommanLibrary> AdminCommanLibrary { get; set; }
    }

    public class CommentBox
    {
        public string ItemId { get; set; }
        public string CommentBoxName { get; set; }
        public string CommentBoxText { get; set; }
        public List<AdminCommanLibrary> AdminCommanLibrary { get; set; }
    }

    public class AdminCommanLibrary
    {
        public string Id { get; set; }
        public string SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string CommentBoxID { get; set; }
        public string CommentText { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string CommentBoxName { get; set; }
        public bool? IsAdminComment { get; set; }
        public bool? IsSelected { get; set; }
    }
}
