﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
 

namespace SpectorMax.Entities.Classes
{
    public class InspectorProfilePoco
    {
        public string ID { get; set; }

        [Required(ErrorMessage = "Enter Your First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Enter Your Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Enter Address")]
        [StringLength(250)]
        public string Address { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Enter Street Address")]
        public string StreetAddress { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Enter AddressLine2")]
        public string AddressLine2 { get; set; }

        [Required(ErrorMessage = "Enter State")]
        public string State { get; set; }

        [Required(ErrorMessage = "Enter City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Enter Country")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Enter PhoneNo")]
        public string PhoneNo { get; set; }

        //[Required(ErrorMessage = "Date Of Birth date is required")]
        //[DataType(DataType.DateTime)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Enter Email")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Enter ZipCode")]
        public Nullable<int> ZipCode { get; set; }

        public string Role { get; set; }
        public bool IsActive { get; set; }
        public bool IsCreatedByAdmin { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public IEnumerable<int?> ZipcodesList { get; set; }
    }
}
