﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpectorMaxDAL;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SpectorMax.Entities.Classes
{
    public class InspectionOrderDetailPoco
    {

        public string InspectionOrderId { get; set; }

        [Required(ErrorMessage = "Please Enter Owner First Name")]
        public string OwnerFirstName { get; set; }

        [Required(ErrorMessage = "Please Enter Owner Last Name")]
        public string OwnerLastName { get; set; }

        [Required(ErrorMessage = "Please Enter Address")]
        public string StreetAddess { get; set; }
        [Required(ErrorMessage = "Please Enter City")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string City { get; set; }

        [Required(ErrorMessage = "Please Enter State")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string State { get; set; }

        [Required(ErrorMessage = "Please Enter Zip code")]
        public int Zip { get; set; }

        // [Required(ErrorMessage = "Please Enter Email Address")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Phone Day")]

        public string PhoneDay { get; set; }

        [Required(ErrorMessage = "Please Enter Phone Day Second")]

        public string PhoneDay1 { get; set; }

        //[Required(ErrorMessage = "Please Enter Phone Eve")]

        public string PhoneEve { get; set; }

        //[Required(ErrorMessage = "Please Enter Phone Eve Second")]

        public string PhoneEve1 { get; set; }

        public string Other { get; set; }

        public string Other1 { get; set; }


        public bool ContractClient { get; set; }

        public bool SendCopy { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address")]
        public string PropertyAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address City")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string PropertyAddressCity { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address State")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string PropertyAddressState { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address Zip")]
        public int PropertyAddressZip { get; set; }
        [Required(ErrorMessage = "Please Enter Property Address Tenant Name")]
        public string PropertyAddressTenantName { get; set; }

        // [Required(ErrorMessage = "Please Enter Property Address CrossStreet")]
        public string PropertyAddressCrossStreet { get; set; }

        // [Required(ErrorMessage = "Please Enter Property Address Directions")]
        public string PropertyAddressDirections { get; set; }


        public string PropertyAddressMapCoord { get; set; }

        public string PropertyAddressSubDivison { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address Phone Day")]
        public string PropertyAddressPhoneDay { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address Phone Day Second")]

        public string PropertyAddressPhoneDay1 { get; set; }

        //  [Required(ErrorMessage = "Please Enter Property Address Phone Eve")]

        public string PropertyAddressPhoneEve { get; set; }



        //  [Required(ErrorMessage = "Please Enter Property Address Phone Eve Second")]

        public string PropertyAddressPhoneEve1 { get; set; }

        public string PropertyDescription { get; set; }

        [Required(ErrorMessage = "Please Enter Property # of Units")]
        //[DataType(DataType.PhoneNumber)]
        public int PropertyUnit { get; set; }

        [Required(ErrorMessage = "Please Enter Property Square Footage")]
        public int PropertySquareFootage { get; set; }

        [Required(ErrorMessage = "Please Enter Property Addition")]
        public string PropertyAddition { get; set; }

        [Required(ErrorMessage = "Please Enter Property BedRoom")]
        public int PropertyBedRoom { get; set; }

        [Required(ErrorMessage = "Please Enter Property Baths")]
        public int PropertyBaths { get; set; }

        [Required(ErrorMessage = "Please Enter Property Estimated Monthly Utilites")]
        public string PropertyEstimatedMonthlyUtilites { get; set; }


        public int PropertyHomeAge { get; set; }

        [Required(ErrorMessage = "Please Enter Property RoofAge")]
        public int PropertyRoofAge { get; set; }

        public string SpecialInstructions { get; set; }

        public string SpecialInstructions1 { get; set; }

        public bool ConfirmationPriorWithBuyer { get; set; }

        public bool ConfirmationPriorWithSellersAgent { get; set; }


        public bool ConfirmationPriorWithBuyersAgent { get; set; }
        [Required(ErrorMessage = "Please Enter Requested First Name")]
        public string RequestedBy { get; set; }

        public string RequestedFirstName { get; set; }

        [Required(ErrorMessage = "Please Enter Requested Last Name")]
        public string RequestedLastName { get; set; }

        [Required(ErrorMessage = "Please Enter Requested By Street Address")]
        public string RequestedByStreetAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Requested By City")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string RequestedByCity { get; set; }

        [Required(ErrorMessage = "Please Enter Requested By State")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string RequestedByState { get; set; }

        [Required(ErrorMessage = "Please Enter Requested By Zip")]
        public int RequestedByZip { get; set; }

        [Required(ErrorMessage = "Please Enter RequestedBy Email Address")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string RequestedByEmailAddress1 { get; set; }

        [Required(ErrorMessage = "Please Enter RequestedBy Phone Day")]

        public string RequestedByEmailPhoneDay { get; set; }

        [Required(ErrorMessage = "Please Enter RequestedBy Phone Day Second")]

        public string RequestedByEmailPhoneDay1 { get; set; }

        //  [Required(ErrorMessage = "Please Enter RequestedBy Phone Eve")]

        public string RequestedByEmailPhoneEve { get; set; }

        //  [Required(ErrorMessage = "Please Enter RequestedBy Phone Eve Second")]

        public string RequestedByEmailPhoneEve1 { get; set; }

        public string RequestedByEmailPhoneOther { get; set; }


        public string RequestedByEmailPhoneOther1 { get; set; }

        public string RequestedByEmailOther { get; set; }

        //[Required(ErrorMessage = "Please Check RequestedByEmailOthe")]
        public bool IsRequestedByEmailOther { get; set; }

        //[Required(ErrorMessage = "Please Check RequestedBy EmailContractClient")]
        public bool RequestedByEmailContractClient { get; set; }

        //[Required(ErrorMessage = "Please Check RequestedBy EmailSend")]
        public bool RequestedByEmailSend { get; set; }

        // [Required(ErrorMessage = "Please Check RequestedBy EmailOwner")]
        public bool RequestedByEmailOwner { get; set; }

        // [Required(ErrorMessage = "Please Check RequestedBy EmailOwnersAgent")]
        public bool RequestedByEmailOwnersAgent { get; set; }

        [Required(ErrorMessage = "Please Enter Client")]
        public string MainClient1 { get; set; }

        [Required(ErrorMessage = "Please Select Date")]
        public DateTime? MainDate1 { get; set; }

        // [Required(ErrorMessage = "Please Enter Client")]
        public string MainClient2 { get; set; }

        [Required(ErrorMessage = "Please Select Date")]
        public DateTime? MainDate2 { get; set; }

        [Required(ErrorMessage = "Please Select Inspection Date")]
        public DateTime? ScheduleInspectionDate { get; set; }


        public bool IsAdditionalTest1 { get; set; }
        public bool IsAdditionalTest2 { get; set; }
        public bool IsAdditionalTest3 { get; set; }
        public bool IsAdditionalTest4 { get; set; }
        public bool IsAdditionalTest5 { get; set; }
        public bool IsAdditionalTest6 { get; set; }
        public bool IsAdditionalTest7 { get; set; }
        public bool IsAdditionalTest8 { get; set; }
        public string TotalAdditionalTestingAmount { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public SellerAgent sellerAgent { get; set; }
        public BuyerAgent buyerAgent { get; set; }
        public List<emailtemplate> emailTemplate { get; set; }
        public string DatetoDisplayMain1 { get; set; }
        public string DatetoDisplay { get; set; }
        public string ScheduleInspectionDate1 { get; set; }

        public IEnumerable<SelectListItem> StatesList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> ZipCodesList { get; set; }
    }
    public class BuyerInspectionOrderDetailPoco
    {

        public string InspectionOrderId { get; set; }
        [Required(ErrorMessage = "Please Enter Property Address")]
        public string PropertyAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address City")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string PropertyAddressCity { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address State")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string PropertyAddressState { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address Zip")]
        public int PropertyAddressZip { get; set; }
        [Required(ErrorMessage = "Please Enter Property Address Tenant Name")]
        public string PropertyAddressTenantName { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address CrossStreet")]
        public string PropertyAddressCrossStreet { get; set; }

        //    [Required(ErrorMessage = "Please Enter Property Address Directions")]
        public string PropertyAddressDirections { get; set; }


        public string PropertyAddressMapCoord { get; set; }

        public string PropertyAddressSubDivison { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address Phone Day")]
        public string PropertyAddressPhoneDay { get; set; }

        [Required(ErrorMessage = "Please Enter Property Address Phone Day Second")]

        public string PropertyAddressPhoneDay1 { get; set; }

        //   [Required(ErrorMessage = "Please Enter Property Address Phone Eve")]

        public string PropertyAddressPhoneEve { get; set; }



        //     [Required(ErrorMessage = "Please Enter Property Address Phone Eve Second")]

        public string PropertyAddressPhoneEve1 { get; set; }


        [Required(ErrorMessage = "Please Enter Requested First Name")]
        public string RequestedBy { get; set; }

        public string RequestedFirstName { get; set; }

        [Required(ErrorMessage = "Please Enter Requested Last Name")]
        public string RequestedLastName { get; set; }

        [Required(ErrorMessage = "Please Enter Requested By Street Address")]
        public string RequestedByStreetAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Requested By City")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string RequestedByCity { get; set; }

        [Required(ErrorMessage = "Please Enter Requested By State")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string RequestedByState { get; set; }

        [Required(ErrorMessage = "Please Enter Requested By Zip")]
        public int RequestedByZip { get; set; }

        [Required(ErrorMessage = "Please Enter RequestedBy Email Address")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string RequestedByEmailAddress1 { get; set; }

        [Required(ErrorMessage = "Please Enter RequestedBy Phone Day")]

        public string RequestedByEmailPhoneDay { get; set; }

        [Required(ErrorMessage = "Please Enter RequestedBy Phone Day Second")]

        public string RequestedByEmailPhoneDay1 { get; set; }

        //   [Required(ErrorMessage = "Please Enter RequestedBy Phone Eve")]

        public string RequestedByEmailPhoneEve { get; set; }

        //   [Required(ErrorMessage = "Please Enter RequestedBy Phone Eve Second")]

        public string RequestedByEmailPhoneEve1 { get; set; }

        public string RequestedByEmailPhoneOther { get; set; }


        public string RequestedByEmailPhoneOther1 { get; set; }

        public string RequestedByEmailOther { get; set; }

        public IEnumerable<SelectListItem> StatesList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> ZipCodesList { get; set; }
    }
    public class SellerAgent
    {
        public string AgentID { get; set; }

        public string InspectionOrderID { get; set; }

        public string AgentType { get; set; }
        [Required(ErrorMessage = "Please Enter SellerAgent FirstName")]
        public string SellerAgentFirstName { get; set; }

        [Required(ErrorMessage = "Please Enter SellerAgentLastName")]
        public string SellerAgentLastName { get; set; }

        [Required(ErrorMessage = "Please Enter SellerAgent Office")]
        public string SellerAgentOffice { get; set; }

        [Required(ErrorMessage = "Please Enter SellerAgent StreetAddress")]
        public string SellerAgentStreetAddress { get; set; }

        [Required(ErrorMessage = "Please Enter SellerAgent City")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string SellerAgentCity { get; set; }

        [Required(ErrorMessage = "Please Enter SellerAgent State")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string SellerAgentState { get; set; }

        [Required(ErrorMessage = "SellerAgent Zip")]
        public int SellerAgentZip { get; set; }

        [Required(ErrorMessage = "Please Enter SellerAgent EmailAddress")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string SellerAgentEmailAddress { get; set; }

        // [Required(ErrorMessage = "Please Enter SellerAgent EmailAddress1")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string SellerAgentEmailAddress1 { get; set; }

        [Required(ErrorMessage = "Please Enter SellerAgent Phone Day")]

        public string SellerAgentPhoneDay { get; set; }

        [Required(ErrorMessage = "Please Enter SellerAgent Phone Day1")]
        public string SellerAgentPhoneDay1 { get; set; }

        // [Required(ErrorMessage = "Please Enter SellerAgent Phone Eve")]

        public string SellerAgentPhoneEve { get; set; }

        //   [Required(ErrorMessage = "Please Enter SellerAgent Phone Eve1")]

        public string SellerAgentPhoneEve1 { get; set; }

        public string SellerAgentPhoneOther { get; set; }


        public string SellerAgentPhoneOther1 { get; set; }

        public bool AgentSellerSend { get; set; }
    }
    public class BuyerAgent
    {
        public string AgentID { get; set; }
        public string InspectionOrderID { get; set; }
        public string AgentType { get; set; }
        [Required(ErrorMessage = "Please Enter BuyerAgent FirstName")]
        public string BuyerAgentFirstName { get; set; }

        [Required(ErrorMessage = "Please Enter BuyerAgent LastName")]
        public string BuyerAgentLastName { get; set; }

        [Required(ErrorMessage = "Please Enter BuyerAgent Office")]
        public string BuyerAgentOffice { get; set; }

        [Required(ErrorMessage = "Please Enter BuyerAgent Street Address")]
        public string BuyerAgentStreetAddress { get; set; }

        [Required(ErrorMessage = "Please Enter BuyerAgent City")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string BuyerAgentCity { get; set; }

        [Required(ErrorMessage = "Please Enter BuyerAgent State")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Use Only Characters")]
        public string BuyerAgentState { get; set; }

        [Required(ErrorMessage = "Please Enter BuyerAgent Zip")]
        public int BuyerAgentZip { get; set; }

        [Required(ErrorMessage = "Please Enter BuyerAgent Email Address")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string BuyerAgentEmailAddress { get; set; }

        //  [Required(ErrorMessage = "Please Enter BuyerAgent Email Address1")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string BuyerAgentEmailAddress1 { get; set; }

        [Required(ErrorMessage = "Please Enter BuyerAgent Phone Day")]

        public string BuyerAgentPhoneDay { get; set; }

        [Required(ErrorMessage = "Please Enter BuyerAgent Phone Day1")]

        public string BuyerAgentPhoneDay1 { get; set; }

        //     [Required(ErrorMessage = "Please Enter BuyerAgent Phone Eve")]

        public string BuyerAgentPhoneEve { get; set; }

        //     [Required(ErrorMessage = "Please Enter BuyerAgent Phone Eve1")]

        public string BuyerAgentPhoneEve1 { get; set; }

        public string BuyerAgentPhoneOther { get; set; }


        public string BuyerAgentPhoneOther1 { get; set; }

        public bool AgentBuyerSend { get; set; }
    }
    public class ViewPreviousInspection
    {
        public string InspectionOrderId { get; set; }
        public string PropertyAddress { get; set; }
        public string PropertyAddressCity { get; set; }
        public string PropertyAddressState { get; set; }
        public int? PropertyAddressZip { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string InspectorFirstName { get; set; }
        public string InspectorName { get; set; }
        public string InspectorLastName { get; set; }
        public string InspectorCity { get; set; }
        public string InspectorState { get; set; }
        public int? InspectorZip { get; set; }
        public string InspectorPhone { get; set; }
        public string InspectorAddress1 { get; set; }
        public string InspectorAddress2 { get; set; }
        public string InspectorStreetAddress { get; set; }
        public string InspectorCountry { get; set; }

        public string PhotoURL { get; set; }
        public string ExperienceTraining1 { get; set; }
        public string ExperienceTraining2 { get; set; }
        public string ExperienceTraining3 { get; set; }
        public string Certification1 { get; set; }
        public string Certification2 { get; set; }
        public string Certification3 { get; set; }
        public DateTime? dateofbirth { get; set; }
        public string ReportStatus { get; set; }
        public string ReportNumber { get; set; }
        public string RequestedbyPhoneDay { get; set; }
        public string RequestedbyFirstName { get; set; }
        public string RequestedBy { get; set; }
        public string Agent { get; set; }
        public float? HomeAge { get; set; }
        public float? Utilities { get; set; }
        public int? Bath { get; set; }
        public int? Bed { get; set; }
        public string AdditionAltration { get; set; }
        public float? RoofAge { get; set; }
        public string Direction { get; set; }
        public float? SqureFeet { get; set; }
        public string Email { get; set; }
        public DateTime? RequestDate { get; set; }
        public DateTime? ScheduleInspectionDate { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.userinfo> InspectorDetails { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.zipcodeassigned> Zip { get; set; }public List<ViewPreviousInspection> InspectionList { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.inspectionadditionalinfo> MainHomePage { get; set; }
        public Dates fromandtodate { get; set; }
        public string Role { get; set; }
        public List<SubSections> SubSectionsList { get; set; }
        public List<MainSections> MainSectionList { get; set; }
        public string AgentEmail { get; set; }
        public string AgentPhone { get; set; }
        public string MainHomeImage { get; set; }
        public bool? IsUpdatable { get; set; }
        public DateTime? CratedDate { get; set; }
        public SampleReportCount ReportCount { get; set; }
        public string ReportViewdId { get; set; }
        public bool IsPurchased { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.reportviewed> Report { get; set; }
        public string RequesterID { get; set; }
        public string RequesterID1 { get; set; }
        public bool? IsVerified { get; set; }
        public string PropertyDescription { get; set; }
        public int? Units { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.tblpropertydescription> PDescription { get; set; }
    }
    public class AllInspectionOrder
    {
        public List<ViewPreviousInspection> InspectionOrder { get; set; }
    }
    public class SubSections
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string SubSectionID { get; set; }
        public string SubSectionName { get; set; }
        public Int32? SortOrder { get; set; }
        public string CheckBoxID { get; set; }
    }
    public class MainSections
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public Int32? SortOrder { get; set; }
    }

    public class BuyerRequestModel
    {
        public string HouseNo { get; set; }
        public string OrderId { get; set; }
        public string City { get; set; }
        public string State{ get; set; }
        public int? Zip { get; set; }
        public DateTime? RequestDate{ get; set; }
        public string InspectorFirstName{ get; set; }
        public string InspectorLastName { get; set; }
        public string InspectorId { get; set; }
        public string Status { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.tblschedule> Schedule { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.userinfo> InspectorDetails { get; set; }
    }
}
