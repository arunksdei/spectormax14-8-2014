﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{

    public class SchedulerProperties
    {
        public int SchedulerId { get; set; }
        public DateTime SchedulerDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Comment { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public class BlockScheduler
    {
        public int BlockId { get; set; }
        public DateTime BlockDate { get; set; }
        public string strBlockDate { get; set; }
        public string Comment { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
