﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;


namespace SpectorMax.Entities.Classes
{
    public static class Sessions
    {
         /// <summary>
        ///  create user session on login..
        /// </summary>
        public static LoggedInUserDetails LoggedInUser { 

            get
            {
                //if (HttpContext.Current.Session["UserDetails"] == null && HttpContext.Current.Request.IsAuthenticated)
                //{
                    
                //}

                return (LoggedInUserDetails) HttpContext.Current.Session["UserDetails"]; 
            }

        }

        /// <summary>
        ///  clear user session on logout etc..
        /// </summary>
        public static LoggedInUserDetails ClearSesson
        {
            get
            {
                HttpContext.Current.Session["UserDetails"] = null;
                //LoggedUserDetailBL loggedUserDetailBL = new LoggedUserDetailBL();
                //loggedUserDetailBL.ClearSession();
                return (LoggedInUserDetails)HttpContext.Current.Session["UserDetails"];
            }
        }
    }
}
