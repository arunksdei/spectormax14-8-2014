﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class Owners
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Designation { get; set; }

    }
}
