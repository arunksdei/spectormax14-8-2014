﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class UserNotificationsEntity
    {
        public string From { get; set; }
        public string To { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string zipcode { get; set; }
        public string Message { get; set; }
        public string CreatedDate { get; set; }

        public List<UserNotifications_lst> msg_lst { get; set; }
    }

    public class UserNotifications_lst
    {
        public string From { get; set; }
        public string msd_id { get; set; }
        public string Subject { get; set; }
        public string CreatedDate { get; set; }
        public string messgae { get; set; }

    }
}
