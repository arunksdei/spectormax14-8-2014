﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace SpectorMax.Entities.Classes
{
   public class OurInspector
    {

        [Required(ErrorMessage = "Enter Email")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }


        public string InspectorDetailsID { get; set; }
        public string UserID { get; set; }
        //[Required(ErrorMessage = "Please Upload your photo")]
        public string PhotoURL { get; set; }

        
        public string ExperienceTraining1 { get; set; }

       
        public string ExperienceTraining2 { get; set; }
        
        public string ExperienceTraining3 { get; set; }
        //[Required(ErrorMessage = "Please Upload your certificate")]
        public string Certification1 { get; set; }
        //[Required(ErrorMessage = "Please Upload your certificate")]
        public string Certification2 { get; set; }
        //[Required(ErrorMessage = "Please Upload your certificate")]
        public string Certification3 { get; set; }

        
       

        public string FirstName { get; set; }
       
        public string LastName { get; set; }
       
        public string Address { get; set; }
       
        public string StreetAddress { get; set; }
       
        public string Address2 { get; set; }
       
        public string State { get; set; }
       
        public string City { get; set; }
        
        public string PhoneNo { get; set; }
       
        public string Country { get; set; }
        //[RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
       
        public string EmailAddress { get; set; }
        
        public DateTime? dateofbirth { get; set; }

        //[Required(ErrorMessage = "Please enter zipcode")]
        //[RegularExpression(@"^\d+$", ErrorMessage = "Only numbers allowed")]
        public int? Zipcode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Int32 CurrentCount { get; set; }
        public Int32 TotalCount { get; set; }
        public List<OurInspector> ListOurInspector { get; set; }
       
    }

}
