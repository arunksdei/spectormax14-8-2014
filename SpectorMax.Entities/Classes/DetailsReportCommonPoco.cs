﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SpectorMaxDAL;

namespace SpectorMax.Entities.Classes
{
    public class DetailsReportCommonPoco
    {
        public string ID { get; set; }
        public string InspectionReportId { get; set; }
        public string InspectionOrderID { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string SubSectionID { get; set; }
        public string SubSectionName { get; set; }
        public string CheckboxID { get; set; }
        public string Type { get; set; }
        public string SerialNumber { get; set; }
        public string Manufacturer { get; set; }
        public string Location { get; set; }
        public string TempSetting { get; set; }
        public string TestResult { get; set; }
        public string Variance { get; set; }
        public string Notice { get; set; }
        public string Comments { get; set; }
        public string PresetComments { get; set; }
        public int? DirectionDropID { get; set; }
        public bool? IsUpdated { get; set; }
        public bool? IsIncluded { get; set; }
        public bool? IsInspectorReviewed { get; set; }
        public string CommentedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public List<ImagesNote> ObjImagesNote { get; set; }
        public List<DetailsReportCommonPoco> ReportsDetail { get; set; }
        public ViewPreviousInspection objViewPreviousInspection { get; set; }
        public string Direction { get; set; }
        public int? MSortOrder { get; set; }
        public int? SSortOrder { get; set; }
        public IEnumerable<ImagesNote> PL { get; set; }
        public IEnumerable<UpdatedSection> UpdateImages { get; set; }
        public string SubSectionStatusId { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.directiondropmaster> DirectionDropMaster { get; set; }
        public bool IsUpdateSection { get; set; }
        public SampleReportCount ReportCount { get; set; }
    }

    public class UpdatedSection
    {
        public string ID { get; set; }
        public string InspectionReportID { get; set; }
        public string PhotoURL1 { get; set; }
        public string PhotoURL2 { get; set; }
        public string PhotoURL3 { get; set; }
        public string PhotoURL4 { get; set; }
        public string SellerComments { get; set; }
        public string InspectorComments { get; set; }
        public bool? IsApproved { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class ImagesNote
    {
        public string ID { get; set; }
        public string ImageUrl { get; set; }
        public string ImageNote { get; set; }
        public string Comments { get; set; }
        public string Direction { get; set; }
        public bool? AcceptedOrRejected { get; set; }
        public string CommentedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string InspectionOrderFId { get; set; }
    }
    public class MainSectionList
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }
    }
    public class SubSectionList
    {
        public string SectionID { get; set; }
        public string SubSectionID { get; set; }
        public string SubSectionName { get; set; }
    }

    public class ReportDetails
    {
        public List<DetailsReportCommonPoco> DetailsReport { get; set; }
    }
    public class SampleReportCount
    {
        public int GreenArrowCount { get; set; }
        public int YellowArrowCount { get; set; }
        public int RedArrowCount { get; set; }
        public int DoubleRedArrowCount { get; set; }
        public int YellowWarningCount { get; set; }
        public int RedWarningCount { get; set; }
        public int No_inspection { get; set; }
        public int Not_accessibility { get; set; }
        public int BlueIconCount { get; set; }


        public float SingleReportPrice { get; set; }
        public float OneTimePayment { get; set; }
        public int? OneTimePaymentValidity { get; set; }
        public int? PurchasePlanValidity { get; set; }
        public float PurchasePlan { get; set; }
        public ReportPropertyAddress Address { get; set; }
    }
    public class ReportPropertyAddress
    {
        public string HouseNo { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyState { get; set; }
        public int? PropertyZip { get; set; }
        public string ReportNo { get; set; }
    }
}
