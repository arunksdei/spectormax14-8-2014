﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
namespace SpectorMax.Entities.Classes
{
    public class InspectorDetails
    {
        public string InspectorDetailsID { get; set; }
        public string UserID { get; set; }
        //[Required(ErrorMessage = "Please Upload your photo")]
        public string PhotoURL { get; set; }

        [Required(ErrorMessage = "Please enter area where you performs inspections")]
        public string ExperienceTraining1 { get; set; }

        [Required(ErrorMessage = "Please enter experience")]
        public string ExperienceTraining2 { get; set; }
        [Required(ErrorMessage = "Please enter education")]
        public string ExperienceTraining3 { get; set; }
        //[Required(ErrorMessage = "Please Upload your certificate")]
        public string Certification1 { get; set; }
        //[Required(ErrorMessage = "Please Upload your certificate")]
        public string Certification2 { get; set; }
        //[Required(ErrorMessage = "Please Upload your certificate")]
        public string Certification3 { get; set; }

        public int? PaymentPercentage { get; set; }
        public DateTime? PaymentCycleStartDate { get; set; }
        public DateTime? PaymentCycleEndDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [Required(ErrorMessage = "Please Enter Owner First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Owner Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please Enter Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please Enter Address")]
        public string StreetAddress { get; set; }
        //[Required(ErrorMessage = "Please Enter Address")]
        public string Address2 { get; set; }
        //[Required(ErrorMessage = "Please Enter State")]
        public string State { get; set; }
        //[Required(ErrorMessage = "Please Enter City")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please Enter Phone")]
        public string PhoneNo { get; set; }
        [Required(ErrorMessage = "Please Enter Country")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Please Enter Email Address")]
        public string EmailAddress { get; set; }

        public DateTime? dateofbirth { get; set; }
        [Required(ErrorMessage = "Date Of Birth date is required")]
        public string DOB { get; set; }
        public List<InspectorDetails> InspectorPayment { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.zipcodeassigned> Zip { get; set; }
        public virtual IEnumerable<SpectorMaxDAL.tblzip> tzip { get; set; }
        public int Count { get; set; }
        public float? PaymentSum { get; set; }
        public float? PaymentDue { get; set; }
        public Nullable<int> StateId { get; set; }
        public Nullable<int> CityId { get; set; }
        public Nullable<int> ZipId { get; set; }
        public IEnumerable<SelectListItem> StatesList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> ZipCodesList { get; set; }
        public IEnumerable<ZipCodes> PL { get; set; }


        [Required(ErrorMessage = "Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }
    }
    public class InspectorPayment
    {
        public List<InspectorDetails> PaymentPercent { get; set; }
    }
    public class ZipCodes
    {
        public int ZipID { get; set; }
        public int? Zip { get; set; }
    }
    public class AssignedZipCode
    {
        public string ZipCodeId { get; set; }
        public string ZipCode { get; set; }
        public string InspectorID { get; set; }
    }

    public class ApplyInspectorApplication
    {
        public string InspectorDetailsID { get; set; }
        public string UserID { get; set; }
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please Enter Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please Enter Phone")]
        public string PhoneNo { get; set; }
        [Required(ErrorMessage = "Please Enter Email Address")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string EmailAddress { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int ZipId { get; set; }
        public string HowLong { get; set; }
        public string FarWillingToTravel { get; set; }
        public string IsCurrentDriverLicence { get; set; }
        public string AvailableToStart { get; set; }
        public string ConvictedofCrime { get; set; }
        public string ExplainNumberOfConviction { get; set; }
        public string ResumeFileName { get; set; }

        public string HighSchool { get; set; }
        public string HighSchoolLocation { get; set; }
        public string HighSchoolCompletedYear { get; set; }
        public string HighSchoolMajorAndDegree { get; set; }

        public string College { get; set; }
        public string CollegeLocation { get; set; }
        public string CollegeCompletedYear { get; set; }
        public string CollegeMajorAndDegree { get; set; }
        [Required(ErrorMessage = "Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }

        public string BusAndTradeSchool { get; set; }
        public string BusAndTradeSchoolLocation { get; set; }
        public string BusAndTradeSchoolCompletedYear { get; set; }
        public string BusAndTradeSchoolMajorAndDegree { get; set; }

        public string ProfessionalSchool { get; set; }
        public string ProfessionalSchoolLocation { get; set; }
        public string ProfessionalSchoolCompletedYear { get; set; }
        public string ProfessionalSchoolMajorAndDegree { get; set; }

        public IEnumerable<SelectListItem> StatesList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> ZipCodesList { get; set; }
        public HttpPostedFileBase FileUpload { get; set; }
    }
}
