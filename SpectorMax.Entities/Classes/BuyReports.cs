﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class BuyReports
    {
        public string Description { get; set; }
        public float? Amount { get; set; }
        public int? Validity { get; set; }

    }
}
