﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
   public class QRCode
    {
       public int? QRCodeNo { get; set; }
       public string ReportNo { get; set; }
    }
}
