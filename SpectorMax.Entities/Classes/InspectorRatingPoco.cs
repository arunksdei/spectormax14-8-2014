﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class InspectorRatingPoco
    {
        public float? rating { get; set; }
        public string createDate { get; set; }
        public string ratingBy { get; set; }
        public string Comment { get; set; }
        public Paging paging { get; set; }
        public string ratingID { get; set; }
        public bool? IsRequestedtoRemove { get; set; }
        public virtual InspectorDetails Inspector { get; set; }
    }
}
