﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class PropertyAddress
    {
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string ReportNo { get; set; }
        public string State { get; set; }
        public int? Zip { get; set; }
        public string OrderID { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string SubSectionID { get; set; }
        public string SubSectionName { get; set; }
        public string Comment { get; set; }
        public string InspectorName { get; set; }
        public string InspectorPercentage { get; set; }
        public DateTime? InspectionDate { get; set; }
        public string TableName { get; set; }
        public List<PropertyAddress> Property { get; set; }
        public bool? AcceptReject { get; set; }
        public DateTime? CreatedDate { get; set; }
        public List<Comments> UserComments { get; set; }
        public bool IsInspectorComment { get; set; }
        public bool CanUpdate { get; set; }
        public string SubSectionStatusID { get; set; }

    }
    public class TableNamesOrderId
    {
        public string TableName { get; set; }
        public string OrderID { get; set; }
    }

    public class Property
    {
        public List<PropertyAddress> PropertyData { get; set; }
    }
    public class Comments
    {
        public String UserName { get; set; }
        public String Comment  { get; set; }
        public String CommentBy { get; set; }
        public String FirstComment { get; set; }
        public String Role { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
