﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class MaintenanceRequired
    {
        public string SectionName { get; set; }
        public string SubSectionName { get; set; }
        public string Comment { get; set; }
        public string City { get; set; }
    }
}
