﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace SpectorMax.Entities.Classes
{
    public class UserInfoPoco
    {
        public string ID { get; set; }
        [Required(ErrorMessage = "Enter Email")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }


        public string Role { get; set; }

        [Required(ErrorMessage = "Enter First Name")]

        public string FirstName { get; set; }
        [Required(ErrorMessage = "Enter Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Enter House Number")]
        [StringLength(250)]
        public string Address { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Enter Street Number")]
        public string StreetAddress { get; set; }

        //[StringLength(50)]
        //[Required(ErrorMessage = "Enter Apartment/Suites")]
        public string AddressLine2 { get; set; }

        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Select State")]
        [Required(ErrorMessage = "Select State")]
        public string State { get; set; }

        [Required(ErrorMessage = "Please Select State")]
        public int StateId { get; set; }

        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Please Select City")]
        [Required(ErrorMessage = "Select City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Please Select City")]
        public int CityId { get; set; }

        [Required(ErrorMessage = "Enter Country")]
        public string Country { get; set; }


        [Required(ErrorMessage = "Enter PhoneNo")]
        //  [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")] 
        public string PhoneNo { get; set; }

        [Required(ErrorMessage = "Enter ZipCode")]
        public int ZipCode { get; set; }

        [Required(ErrorMessage = "Enter ZipCode")]
        public int ZipId { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [Required]
        public string DOB { get; set; }


        public IEnumerable<SelectListItem> SecurityQuestion1 { get; set; }

        [Required(ErrorMessage = "Select Security Question")]
        public string SecurityQuestion { get; set; }


        [Required(ErrorMessage = "Enter Security Answer")]
        public string SecurityAnswer { get; set; }
        public bool? IsProfileUpdated { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public List<UserRole> UserRoleList { get; set; }
        public IEnumerable<SelectListItem> StatesList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> ZipCodesList { get; set; }
        public List<UserInfoPoco> SubadminDetails { get; set; }
        public string paymenttype { get; set; }
        public string amount { get; set; }
        public virtual Permissions Permission { get; set; }

    }
    public class UserRole
    {
        public string RoleID { get; set; }
        public string RoleName { get; set; }
    }
    public class LogInModel
    {
        //public int UserLoginID { get; set; }
        [Required(ErrorMessage = "Enter Email")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string typePayment { get; set; }
        public string paymentamount { get; set; }
        public int Validity { get; set; }
        // public string loginType { get; set; }
    }

    public class ForgotPassword
    {
        //public int UserLoginID { get; set; }
        [Required(ErrorMessage = "Enter Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }

    public class ResetPassword
    {
        public IEnumerable<SelectListItem> SecurityQuestion1 { get; set; }

        public string SecurityQuestion { get; set; }

        [Required(ErrorMessage = "Please input your security answer")]
        public string SecurityAnswer { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }
    }

    public class ZipCode
    {
        //public int UserLoginID { get; set; }
        [Required(ErrorMessage = "Enter Zip Code")]
        public int Zipcode { get; set; }
    }


    public class MessageToShow
    {
        public string Message { get; set; }
        public bool Successfull { get; set; }
        public long Id { get; set; }
        public string Result { get; set; }
        public string UserID { get; set; }
        public string UserRoleType { get; set; }
        public bool? ReportType { get; set; }
    }
    public class LoggedInUserDetails
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public long ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string LoginType { get; set; }
        public string AdditionalTestingPaymentStatus { get; set; }
        public int? Zipcode { get; set; }
        public int rolecount { get; set; }
        public virtual Permissions AdminPermission { get; set; }
        public string NotificationCount { get; set; }
        public string PaymentPlan { get; set; }
        public  int? StateId { get; set; }
        public  int? CityId { get; set; }
        public  int? ZipId { get; set; }
        public  string PhoneNo { get; set; }
        public  string Address { get; set; }
        public bool? ProfileUpdated { get; set; }
    }

    public static class LoggedInUser
    {
        public static string UserId { get; set; }
        public static string RoleId { get; set; }
        public static string RoleName { get; set; }
        public static long ClientId { get; set; }
        public static string FirstName { get; set; }
        public static string LastName { get; set; }
        public static string EmailAddress { get; set; }
        public static string LoginType { get; set; }
        public static string AdditionalTestingPaymentStatus { get; set; }
        public static int? Zipcode { get; set; }
      
    }


    public class SubadminDetails
    {
        public List<UserInfoPoco> UserData { get; set; }
    }

    public class Permissions
    {
        public string PermissionID { get; set; }
        public string RoleId { get; set; }
        public bool Canreviewallinspections { get; set; }
        public bool Canreview_WithinZipcodes_sales { get; set; }
        public bool Canviewrequestforinspections { get; set; }
        public bool Cangetnoticeifinspectorhasnotresponded { get; set; }
        public bool Canviewpaymenttransactions { get; set; }
        public bool Canviewmoneyreceviedbycustomers { get; set; }
        public bool Canreceiveemail { get; set; }
    }
    public class CreateBuyer
    {

        public string ID { get; set; }
        [Required(ErrorMessage = "Enter Email")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Enter Card Holeder Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Select State")]
        public string State { get; set; }
        [Required(ErrorMessage = "Select City")]
        public string City { get; set; }
        [Required(ErrorMessage = "Enter Country")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Enter ZipCode")]
        public int ZipCode { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string paymenttype { get; set; }
        public string amount { get; set; }
        [Required(ErrorMessage = "Enter Card Number")]
        public string cardNumber { get; set; }
        [Required(ErrorMessage = "Enter Expiry Month")]
        public string expiryMonth { get; set; }
        [Required(ErrorMessage = "Enter Expiry Year")]
        public string expiryYear { get; set; }
        [Required(ErrorMessage = "Enter Secutity Code")]
        public string securityCode { get; set; }
        public int PlanValidity { get; set; }
        [Required(ErrorMessage = "Select State")]
        public int StateId { get; set; }
        [Required(ErrorMessage = "Select City")]
        public int CityId { get; set; }
        public string Role { get; set; }
        public string OrderId { get; set; }
    }

    public class BlockHomeOwner
    {

        public string FirstName { get; set; }
        public string ID { get; set; }
        public string LastName { get; set; }
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string EmailAddress { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string PhoneNo { get; set; }
        public bool IsActive { get; set; }
    }
    public class UserProfilePoco
    {
        public string ID { get; set; }
        [Required(ErrorMessage = "Enter Email")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Select City")]
        public int? CityId { get; set; }

        [Required(ErrorMessage = "Select State")]
        public int? StateId { get; set; }


        [Required(ErrorMessage = "Enter First Name")]

        public string FirstName { get; set; }
        [Required(ErrorMessage = "Enter Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Enter House Number")]
        [StringLength(250)]
        public string Address { get; set; }

        //[StringLength(50)]
        //[Required(ErrorMessage = "Enter Street Number")]
        public string StreetAddress { get; set; }


        [Required(ErrorMessage = "Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }


        [Required(ErrorMessage = "Select State")]

        public string State { get; set; }

        [Required(ErrorMessage = "Select City")]

        public string City { get; set; }
        public string Role { get; set; }

        [Required(ErrorMessage = "Enter Country")]
        public string Country { get; set; }


        [Required(ErrorMessage = "Enter PhoneNo")]
        //[StringLength(10, ErrorMessage = "The Mobile must contains 10 characters", MinimumLength = 10)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string PhoneNo { get; set; }

        [Required(ErrorMessage = "Enter ZipCode")]
        public int ZipCode { get; set; }


        public DateTime? DateOfBirth { get; set; }

        [Required(ErrorMessage = "Date Of Birth date is required")]
        public string DOB { get; set; }


        public IEnumerable<SelectListItem> SecurityQuestion1 { get; set; }

        [Required(ErrorMessage = "Select Security Question")]
        public string SecurityQuestion { get; set; }


        [Required(ErrorMessage = "Enter Security Answer")]
        public string SecurityAnswer { get; set; }
        public bool? IsProfileUpdated { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public List<UserRole> UserRoleList { get; set; }
        public IEnumerable<SelectListItem> StatesList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> ZipCodesList { get; set; }

    }

    public class AdminProfile
    {
        public string ID {get;set;}
        [Required(ErrorMessage="Please enter email address")]
        public string EmailAddress{get;set;}
        [Required(ErrorMessage = "Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string OldPassword {get;set;}
        [Required(ErrorMessage = "Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string NewPassword {get;set;}
        [Required(ErrorMessage = "Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword {get;set;}
    }
}
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public sealed class NoCacheAttribute : ActionFilterAttribute
{
    public override void OnResultExecuting(ResultExecutingContext filterContext)
    {
        filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
        filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        filterContext.HttpContext.Response.Cache.SetNoStore();

        base.OnResultExecuting(filterContext);
    }
}