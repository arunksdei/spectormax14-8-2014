﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
  public  class State
    {
      public int ID { get; set; }
      public string  StateCode { get; set; }
      public string StateName { get; set; }
     
    }
    
}
