﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class NCity
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
    }
}
