﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class NState
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
    }
}
