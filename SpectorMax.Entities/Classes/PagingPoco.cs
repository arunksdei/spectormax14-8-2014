﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using SpectorMaxDAL;
namespace SpectorMax.Entities.Classes
{
    public class PagingPoco
    {
       // public List<InspectionOrderDetailsPaging> ObjInspectionOrderDetailsPaging { get; set; }
        public List<ViewPreviousInspection> ObjInspectionOrderDetailsPaging { get; set; }
        public Paging objPaging { get; set; }
        public Dates fromandtodate { get; set; }
    }
    public class Paging
    {
        public int TotalRecord { get; set; }
        public int PageNumber { get; set; }
        public int PerPageRecord { get; set; }
        public int TotalPage { get; set; }
        public int SkipRecord { get; set; }
        public Dictionary<int, string> PerPageRecordlist { get; set; }
    }
    public class InspectionOrderDetailsPaging
    {
        public string InspectionOrderId { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string PropertyAddress { get; set; }
        public string PropertyAddressCity { get; set; }
        public string PropertyAddressState { get; set; }
        public int? PropertyAddressZip { get; set; }
        public DateTime? ScheduleInspectionDate { get; set; }
    }
    public class PaymentPoco
    {
        public List<Payments> objPayment { get; set; }
        public Paging objPaging { get; set; }
    }
    public class Payments
    {
        public string PaymentID { get; set; }
        public string ReportID { get; set; }
        public string UserID { get; set; }
        public string RoleID { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime? PaymentDate { get; set; }
        public float? PaymentAmount { get; set; }
        public string PaymentType { get; set; }
        public string PaymentInvoice { get; set; }
        public string CustomerAddress { get; set; }
        public int? InvoiceNo { get; set; }
        public string AdditionalTesting { get; set; }
    }
    public class Dates
    {
        [Required(ErrorMessage = "Please Select from date")]
        public string fromdate { get; set; }
        [Required(ErrorMessage = "Please Select to date")]
        public string todate { get; set; }
    }
}
