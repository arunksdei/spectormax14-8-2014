﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class AdditionalTesting
    {
        public bool FoundationAnalysis { get; set; }
        public float FoundationAnalysisPrice { get; set; }

        public bool GasLeakDetection { get; set; }
        public float GasLeakDetectionPrice { get; set; }

        public bool RadonTesting { get; set; }
        public float RadonTestingPrice { get; set; }

        public bool MoldMoistureAnalysis { get; set; }
        public float MoldMoistureAnalysisPrice { get; set; }

        public bool MethTesting { get; set; }
        public float MethTestingPrice { get; set; }

        public int MoldMoistureAdditionalTest { get; set; }
        public int MethTestingAdditionalTest { get; set; }
        public int TotalPrice { get; set; }
    }
}
