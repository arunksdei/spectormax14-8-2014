﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectorMax.Entities.Classes
{
    public class Notification
    {
        public int ID { get; set; }
        public int NotificationTypeId { get; set; }
        public string NotificationFromID { get; set; }
        public string NotificationFrom { get; set; }
        public string NotificationType { get; set; }
        public DateTime? NotificationDate { get; set; }
        public bool IsRead { get; set; }
        public string UserRole { get; set; }
        public string RequestId { get; set; }
        public List<Notification> NotificationDetails { get; set; }
    }
    public class AdminNotification
    {
        public List<Notification> Notifications { get; set; }
    }

    public class UserNotificationModel
    {
        public string ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? ZipCode { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Subject { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsDeleted { get; set; }
        public string ParentID { get; set; }
        public string SubparentId { get; set; }
        public string MessageForm { get; set; }
    }
}
