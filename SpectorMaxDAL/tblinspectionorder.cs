//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpectorMaxDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblinspectionorder
    {
        public string InspectionOrderID { get; set; }
        public string InspectionOrderDetailId { get; set; }
        public string RequesterID { get; set; }
        public string RequesterID1 { get; set; }
        public string InspectorID { get; set; }
        public string InspectionStatus { get; set; }
        public Nullable<System.DateTime> ScheduledDate { get; set; }
        public Nullable<bool> AssignedByAdmin { get; set; }
        public Nullable<bool> IsAcceptedOrRejected { get; set; }
        public Nullable<bool> IsRejectedByInspector { get; set; }
        public Nullable<bool> IsUpdatable { get; set; }
        public string ReportNo { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string DeletedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string VerificationNumber { get; set; }
        public Nullable<bool> IsVerified { get; set; }
    }
}
