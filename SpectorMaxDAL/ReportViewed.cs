//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpectorMaxDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class reportviewed
    {
        public string ID { get; set; }
        public string ReportID { get; set; }
        public string UserID { get; set; }
        public bool Viewed { get; set; }
        public string MachineIP { get; set; }
        public string PaymentID { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifedDate { get; set; }
        public Nullable<bool> IsPurchased { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> DeletedBy { get; set; }
    }
}
