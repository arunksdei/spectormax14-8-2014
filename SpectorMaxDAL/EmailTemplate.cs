//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpectorMaxDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class emailtemplate
    {
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string TemplateDiscription { get; set; }
        public Nullable<float> Rate { get; set; }
        public Nullable<int> Validity { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
