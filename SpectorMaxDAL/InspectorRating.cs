//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpectorMaxDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class inspectorrating
    {
        public string RatingID { get; set; }
        public string InspectorID { get; set; }
        public string InspectionID { get; set; }
        public Nullable<float> Rating1 { get; set; }
        public Nullable<float> Rating2 { get; set; }
        public Nullable<float> Rating3 { get; set; }
        public Nullable<float> Rating4 { get; set; }
        public Nullable<float> Rating5 { get; set; }
        public Nullable<float> Rating6 { get; set; }
        public string Comment { get; set; }
        public string RatingByID { get; set; }
        public bool RequestedtoRemove { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
