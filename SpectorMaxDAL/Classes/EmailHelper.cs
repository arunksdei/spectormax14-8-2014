﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net.Mail;

namespace SpectorMaxDAL.Classes
{
  public  class EmailHelper
    {
        public bool SendEmail(string From, string subject, string bcc, string Description, string To)
        {
            try
            {
                //ConfigurationSettings.AppSettings["SystemEmailFrom"].ToString();
                //Get mail message object 
                MailMessage objMailMessage = new MailMessage();
                //Assign the sender mail id in the mail object fiels   
                objMailMessage.From = new MailAddress("sdm.os46@gmail.com");
                //objMailMessage.From = new MailAddress(strFrom, csWebConfigSettings.UserType.ClientDetail.ClientEmailDisplayName);
                //Assign the receivers mail id in the mail object fiels 

                objMailMessage.To.Add(new MailAddress(To));
                //Set the other parameters of the mail 
                objMailMessage.Subject = subject;
                objMailMessage.Body = Description;
                objMailMessage.IsBodyHtml = true;
                objMailMessage.Priority = MailPriority.Normal;
                objMailMessage.Headers.Add("Message-Id", String.Concat("<", DateTime.Now.ToString("yyMMdd"), ".", DateTime.Now.ToString("HHmmss"), "@>"));
                //Assign the mail server authentication details in the mail object fiels 

                //Create smtp client object to send the message and assign the parameters requested by he smtp client to send message
                SmtpClient objSmptp = new SmtpClient();


                objSmptp.Host = ConfigurationManager.AppSettings["smtpHost"].ToString();  //objSmptp.Host = "smtp.gmail.com";
                objSmptp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]); //objSmptp.Port = 587;
                objSmptp.DeliveryMethod = SmtpDeliveryMethod.Network;
                objSmptp.UseDefaultCredentials = false;
                //System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential("vpb.sdei@gmail.com", "vpb123456");
                string userName = ConfigurationManager.AppSettings["smtpUserName"].ToString();
                string password = ConfigurationManager.AppSettings["smtpPassword"].ToString();
                objSmptp.Credentials = new System.Net.NetworkCredential(userName, password); //("sdm.os46@gmail.com", "sdm#e2012");

                objSmptp.EnableSsl = true;
                //send mail
                objSmptp.Send(objMailMessage);
                //objSmptp.SendAsync(objMailMessage, null);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
