﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectroMaxBAL.Classes
{
    public class UserInfoPoco
    {        
        public string ID { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string StreetAddress { get; set; }
        public string AddressLine2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public int? ZipCode { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public List<UserRole> UserRoleList { get; set; }
    }
    public class UserRole
    {       
        public string RoleID { get; set; }
        public string RoleName { get; set; }
    }
    public class LogInModel
    {
        //public int UserLoginID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        // public string loginType { get; set; }
    }

    public class MessageToShow
    {
        public string Message { get; set; }
        public bool Successfull { get; set; }
        public long Id { get; set; }
        public string Result { get; set; }
        public string UserID { get; set; }
        public string UserRoleType { get; set; }
    }
    public class PreClosingCheckList
    {      
        public string[][] CheckList { get; set; }
    }
}
