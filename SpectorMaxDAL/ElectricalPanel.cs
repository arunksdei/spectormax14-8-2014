//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpectorMaxDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class electricalpanel
    {
        public string ID { get; set; }
        public string ReportID { get; set; }
        public string SectionID { get; set; }
        public string SubsectionID { get; set; }
        public string ConditionID { get; set; }
        public string LocationOfPanel { get; set; }
        public string EntranceWire { get; set; }
        public string PanelRating { get; set; }
        public string PanelBrand { get; set; }
        public string PanelSize { get; set; }
        public string Comments { get; set; }
        public string Notice { get; set; }
        public string DirectionDrop { get; set; }
        public string PresetComment { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
