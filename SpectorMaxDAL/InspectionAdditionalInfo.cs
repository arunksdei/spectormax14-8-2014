//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpectorMaxDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class inspectionadditionalinfo
    {
        public string AdditionalInfoId { get; set; }
        public string InspectionOrderID { get; set; }
        public string PresentAtInspection { get; set; }
        public string BuildingStatus { get; set; }
        public string WeatherCondition { get; set; }
        public string Tempature { get; set; }
        public Nullable<bool> UtilitiesOn { get; set; }
        public Nullable<bool> NoElectricity { get; set; }
        public Nullable<bool> NoWater { get; set; }
        public Nullable<bool> NoGas { get; set; }
        public string StartTime { get; set; }
        public string FinishTime { get; set; }
        public string StartMiles { get; set; }
        public string FinishMiles { get; set; }
        public Nullable<bool> AttendeesOwnerSeller { get; set; }
        public Nullable<bool> AttendeesSellerAgent { get; set; }
        public Nullable<bool> AttendeesBuyersAgent { get; set; }
        public Nullable<bool> AttendeesBuyer { get; set; }
        public string SpecialNotes { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Imagearry { get; set; }
    }
}
