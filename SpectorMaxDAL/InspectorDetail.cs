//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpectorMaxDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class inspectordetail
    {
        public string InspectorDetailsID { get; set; }
        public string UserID { get; set; }
        public string InspectorNo { get; set; }
        public string PhotoURL { get; set; }
        public string ExperienceTraining1 { get; set; }
        public string ExperienceTraining2 { get; set; }
        public string ExperienceTraining3 { get; set; }
        public string Certification1 { get; set; }
        public string Certification2 { get; set; }
        public string Certification3 { get; set; }
        public Nullable<int> PaymentPercentage { get; set; }
        public Nullable<System.DateTime> PaymentCycleStartDate { get; set; }
        public Nullable<System.DateTime> PaymentCycleEndDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsCreatedByAdmin { get; set; }
        public string HowLong { get; set; }
        public string FarWillingToTravel { get; set; }
        public Nullable<bool> IsCurrentDriverLicence { get; set; }
        public Nullable<System.DateTime> AvailableToStart { get; set; }
        public Nullable<bool> ConvictedofCrime { get; set; }
        public string ExplainNumberOfConviction { get; set; }
        public string ResumeFileName { get; set; }
        public string HighSchool { get; set; }
        public string HighSchoolLocation { get; set; }
        public string HighSchoolCompletedYear { get; set; }
        public string HighSchoolMajorAndDegree { get; set; }
        public string College { get; set; }
        public string CollegeLocation { get; set; }
        public string CollegeCompletedYear { get; set; }
        public string CollegeMajorAndDegree { get; set; }
        public string BusAndTradeSchool { get; set; }
        public string BusAndTradeSchoolLocation { get; set; }
        public string BusAndTradeSchoolCompletedYear { get; set; }
        public string BusAndTradeSchoolMajorAndDegree { get; set; }
        public string ProfessionalSchool { get; set; }
        public string ProfessionalSchoolLocation { get; set; }
        public string ProfessionalSchoolCompletedYear { get; set; }
        public string ProfessionalSchoolMajorAndDegree { get; set; }
        public bool IsShowOnHomePage { get; set; }
    }
}
