﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.Communication;
using PayPal.Payments.DataObjects;
using System.Collections.Specialized;

namespace SpectorMax.Utilities.Classes
{
    public class PayPalDirectPayment
    {
        public string PaymentToPayPal(string cardNumber, string expiryMonth, string expiryYear, string securityCode, string amount)
        {
            string message = "";
            string messageResponse = "";
            try
            {
                string PayPalRequest = "TRXTYPE=S" //S - sale transaction
               + "&TENDER=C" //C - Credit card
               + "&ACCT=" + cardNumber //card number
               + "&EXPDATE=" + expiryMonth + expiryYear.Substring(2, 2)
               + "&CVV2=" + securityCode  //card validation value (card security code)               
               + "&AMT=" + amount
               + "&COMMENT1=My Product Sale"
               + "&USER=" + ConfigurationManager.AppSettings["USER"]
               + "&VENDOR=" + ConfigurationManager.AppSettings["VENDOR"]
               + "&PARTNER=" + ConfigurationManager.AppSettings["PARTNER"]
                    //+ "&Signature=" + ConfigurationManager.AppSettings["Signature"]
               + "&PWD=" + ConfigurationManager.AppSettings["PWD"];


                // Create an instantce of PayflowNETAPI.
                PayflowNETAPI PayflowNETAPI = new PayflowNETAPI();

                // RequestId is a unique string that is required for each & every transaction. 
                // The merchant can use her/his own algorithm to generate this unique request id or 
                // use the SDK provided API to generate this as shown below (PayflowUtility.RequestId).
                string PayPalResponse = PayflowNETAPI.SubmitTransaction(PayPalRequest, PayflowUtility.RequestId);

                //place data from PayPal into a namevaluecollection
                NameValueCollection RequestCollection = GetPayPalCollection(PayflowNETAPI.TransactionRequest);
                NameValueCollection ResponseCollection = GetPayPalCollection(PayPalResponse);

                //show request
                message += "<span class=\"heading\">PayPal Payflow Pro transaction request</span><br />";
                 message += ShowPayPalInfo(RequestCollection);

                //show response
                  message += "<br /><br /><span class=\"heading\">PayPal Payflow Pro transaction response</span><br />";
                message += ShowPayPalInfo(ResponseCollection);
                messageResponse = ShowPayPalInfo(ResponseCollection);

                //if (message.Contains("RESULT:</span> 0<br />") && messageResponse.Contains("Successful."))
                // {

                // }
                // else if (message.Contains("RESULT:</span> 23<br /><span"))
                // {
                //     string x = message;
                // }
                //show transaction errors if any
                string TransErrors = PayflowNETAPI.TransactionContext.ToString();
                if (TransErrors != null && TransErrors.Length > 0)
                {
                     message += "<br /><br /><span class=\"bold-text\">Transaction Errors:</span> " + TransErrors;
                }

                //show transaction status
                 //message += "<br /><br /><span class=\"bold-text\">Status:</span> " + PayflowUtility.GetStatus(PayPalResponse);
                messageResponse += PayflowUtility.GetStatus(PayPalResponse);
            }
            catch (Exception ex)
            {
                  message += ex.Message.ToString();
            }
            return messageResponse;
        }

        private NameValueCollection GetPayPalCollection(string payPalInfo)
        {
            //place the responses into collection
            NameValueCollection PayPalCollection = new System.Collections.Specialized.NameValueCollection();
            string[] ArrayReponses = payPalInfo.Split('&');

            for (int i = 0; i < ArrayReponses.Length; i++)
            {
                string[] Temp = ArrayReponses[i].Split('=');
                PayPalCollection.Add(Temp[0], Temp[1]);
            }
            return PayPalCollection;
        }
        private string ShowPayPalInfo(NameValueCollection collection)
        {
            string PayPalInfo = "";
            foreach (string key in collection.AllKeys)
            {
                PayPalInfo += "<br /><span class=\"bold-text\">" + key + ":</span> " + collection[key];
            }
            return PayPalInfo;
        }

    }
}
