﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectormaxService.DATA.ResultPacket
{
    public class CustomPhotoLibrary
    {
        public string ID { get; set; }
        public string InspectionOrderID { get; set; }
        public string SectionID { get; set; }
        public string SubSectionId { get; set; }
        public string Notes { get; set; }
        public string Comments { get; set; }
        public string CommentedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string imagearr { get; set; }
        public string DirectionDropID { get; set; }
        public string InpectionReportFID { get; set; }
    }
}
