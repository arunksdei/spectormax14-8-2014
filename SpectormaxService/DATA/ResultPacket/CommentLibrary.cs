﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectormaxService.DATA.ResultPacket
{
     
    public class CommentLibrary
    {
        public string Id { get; set; }
        public string SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string CommentBoxID { get; set; }
        public string CommentText { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        //public bool? IsDeleted { get; set; }
        //public DateTime? DeletedDate { get; set; }
        public int? status { get; set; }
        public string CommentBoxName { get; set; }
    }
    
    
    public class SubSectionList
    {
        public string SubSectionID { get; set; }
        //public string SubSectionName { get; set; }
        public List<CommentLibrary> CommentLibrary { get; set; }
    }


    public class Section
    {
        public string SectionID { get; set; }
        //public string SectionName { get; set; }        
        public List<SubSectionList> SubSection { get; set; }
    }
}
