﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectormaxService.DATA.ResultPacket
{
    public class ServiceStatus
    {
        public string InspectionID { get; set; }
        public string InspectionStatus { get; set; }
    }
}
