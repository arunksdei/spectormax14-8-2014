﻿using SpectorMaxDAL;
using System;
using System.Collections.Generic;

namespace SpectormaxService.DATA.ResultPacket
{
    public class InspectionOrderDetailPocoNew
    {
        public string InspectionOrderID { get; set; }
        //public string InspectionOrderDetailId { get; set; }
        public string RequesterID { get; set; }
        public string InspectorID { get; set; }
        public string InspectionStatus { get; set; }
        public string ScheduledDate { get; set; }
        public string IsUpdatable { get; set; }
        public string IsActive { get; set; }

        /*Requester Details*/
        public string RequesterFName { get; set; }
        public string RequesterLName { get; set; }
        public string RequesterPhone { get; set; }
        public string RequesterAddress { get; set; }
        public string RequesterState { get; set; }
        public string RequesterCity { get; set; }
        public string RequesterZip { get; set; }
        public string RequesterType { get; set; }
        public string RequestedByEmailAddress { get; set; }

        /*Property info*/
        public string PropertyAddress { get; set; }
        public string PropertyState { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyZip { get; set; }
        public string PropertyEmail { get; set; }
        public string OwnerName { get; set; }
        public string OwnerPhone { get; set; }
        public string PropertyDescription { get; set; }
        public string Units { get; set; }
        public string SqureFootage { get; set; }
        public string Additions_Alterations { get; set; }
        public string Bedrooms { get; set; }
        public string Bathrooms { get; set; }
        public string EstimatedMonthlyUtilities { get; set; }
        public string YearBuilt { get; set; }
        public string RoofAge { get; set; }
        public string Directions { get; set; }

        /*Agent Info*/
        public SellerAgentNew sellerAgent { get; set; }
        public BuyerAgentNew buyerAgent { get; set; }

        /*If payment done in IPAD*/
        //public string PaymentInvoiceNo { get; set; }
        //public string PaymentAmount { get; set; }

        //public string ReportNo { get; set; }
        //public string InspectorEmailID { get; set; }
        public string UserRole { get; set; }
    }

    public class SellerAgentNew
    {
        public string AgentID { get; set; }
        public string InspectionOrderID { get; set; }
        public string AgentType { get; set; }
        public string SellerAgentFirstName { get; set; }
        public string SellerAgentLastName { get; set; }
        public string SellerAgentOffice { get; set; }
        public string SellerAgentStreetAddress { get; set; }
        public string SellerAgentCity { get; set; }
        public string SellerAgentState { get; set; }
        public string    SellerAgentZip { get; set; }
        public string SellerAgentEmailAddress { get; set; }
        public string SellerAgentPhoneDay { get; set; }
    }
    public class BuyerAgentNew
    {
        public string AgentID { get; set; }
        public string InspectionOrderID { get; set; }
        public string AgentType { get; set; }
        public string BuyerAgentFirstName { get; set; }
        public string BuyerAgentLastName { get; set; }
        public string BuyerAgentOffice { get; set; }
        public string BuyerAgentStreetAddress { get; set; }
        public string BuyerAgentCity { get; set; }
        public string BuyerAgentState { get; set; }
        public string BuyerAgentZip { get; set; }
        public string BuyerAgentEmailAddress { get; set; }
        public string BuyerAgentPhoneDay { get; set; }
    }

}
