﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectormaxService.DATA.ResultPacket
{
    public class HVACEquipBoileRadiantSectionService
    {
        public string ID { get; set; }
        public string InspectionOrderId { get; set; }
        public string SectionID { get; set; }
        public string SubSectionID { get; set; }
        public string CheckBoxID { get; set; }
        public string Type { get; set; }
        public string EnergySource { get; set; }
        public string ManufacturerDate { get; set; }
        public string Manufacturer { get; set; }
        public string SerialNumber { get; set; }
        public string BTU { get; set; }
        public string Notice { get; set; }
        public string Comments { get; set; }
        public string DirectionDropID { get; set; }
        public string PresetComment { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
