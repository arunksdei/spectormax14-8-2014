﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectormaxService.DATA.ResultPacket
{
    public class InspectionResultDataNew
    {
        public string InspectionOrderID { get; set; }
        public string InspectionOrderDetailId { get; set; }
        public string RequesterID { get; set; }
        public string InspectorID { get; set; }
        public string ScheduledDate { get; set; }
        //public string ScheduledDate { get; set; }
        //public string ScheduledDate_IPAD { get; set; }
        //public bool IsUpdatable { get; set; }

        /*Requester Details*/
        public string RequesterFName { get; set; }
        public string RequesterLName { get; set; }
        public string RequesterPhone { get; set; }
        public string RequesterAddress { get; set; }
        public string RequesterState { get; set; }
        public string RequesterCity { get; set; }
        public Nullable<int> RequesterZip { get; set; }
        public Nullable<int> RequesterType { get; set; }
        public string RequestedByEmailAddress { get; set; }

        /*Property info*/
        public string PropertyAddress { get; set; }
        public string PropertyState { get; set; }
        public string PropertyCity { get; set; }
        public Nullable<int> PropertyZip { get; set; }
        public string PropertyEmail { get; set; }
        public string OwnerName { get; set; }
        public string OwnerPhone { get; set; }
        public string PropertyDescription { get; set; }
        public Nullable<int> Units { get; set; }
        public Nullable<float> SqureFootage { get; set; }
        public string Additions_Alterations { get; set; }
        public Nullable<int> Bedrooms { get; set; }
        public Nullable<int> Bathrooms { get; set; }
        public Nullable<float> EstimatedMonthlyUtilities { get; set; }
        public Nullable<int> YearBuilt { get; set; }
        public Nullable<float> RoofAge { get; set; }
        public string Directions { get; set; }

        ///*Agent Info*/
        //public SellerAgent sellerAgent { get; set; }
        //public BuyerAgent buyerAgent { get; set; }

        public string ReportNo { get; set; }
        //public string UserRole { get; set; }

        public string ServerTime { get; set; }
        public string AgentID { get; set; }
        public string AgentType { get; set; }

        public string BuyerAgentFirstName { get; set; }
        public string BuyerAgentLastName { get; set; }
        public string BuyerAgentOffice { get; set; }
        public string BuyerAgentStreetAddress { get; set; }
        public string BuyerAgentCity { get; set; }
        public string BuyerAgentState { get; set; }
        public int? BuyerAgentZip { get; set; }
        public string BuyerAgentEmailAddress { get; set; }
        public string BuyerAgentPhoneDay { get; set; }


        public string SellerAgentFirstName { get; set; }
        public string SellerAgentLastName { get; set; }
        public string SellerAgentOffice { get; set; }
        public string SellerAgentStreetAddress { get; set; }
        public string SellerAgentCity { get; set; }
        public string SellerAgentState { get; set; }
        public int? SellerAgentZip { get; set; }
        public string SellerAgentEmailAddress { get; set; }
        public string SellerAgentPhoneDay { get; set; }
    }
}
