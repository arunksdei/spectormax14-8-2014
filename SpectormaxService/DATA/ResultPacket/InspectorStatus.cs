﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectormaxService.DATA.ResultPacket
{
   public class InspectorStatus
    {
        public string UserName { get; set; }
        public string InspectorName { get; set; }
        public string Password { get; set; }
        public bool Status { get; set; }
        public List<int?> zipcodelist { get; set; }
    }

   //public class assigedzipcode
   //{
   //    public int Zipcode { get; set; }
   //}
}
