﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectormaxService.DATA.ResultPacket
{
    public class PaymentData
    {
        public string SquareUpNo { get; set; }
        public string InspectionID { get; set; }
        public string Amount { get; set; }
        public string EmailID { get; set; }
    }
}
