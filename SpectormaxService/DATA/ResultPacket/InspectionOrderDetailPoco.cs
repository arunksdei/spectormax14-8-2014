﻿using SpectorMaxDAL;
using System;
using System.Collections.Generic;

namespace SpectormaxService.DATA.ResultPacket
{
    public class InspectionOrderDetailPoco
    {

        public string RequesterEmailID { get; set; }
        public string InspectionOrderId { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string StreetAddess { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneDay { get; set; }
        public string PhoneDay1 { get; set; }
        public string PhoneEve { get; set; }
        public string PhoneEve1 { get; set; }
        public string Other { get; set; }
        public string Other1 { get; set; }
        public bool ContractClient { get; set; }
        public bool SendCopy { get; set; }
        public string PropertyAddress { get; set; }
        public string PropertyAddressCity { get; set; }
        public string PropertyAddressState { get; set; }
        public int PropertyAddressZip { get; set; }
        public string PropertyAddressTenantName { get; set; }
        public string PropertyAddressCrossStreet { get; set; }
        public string PropertyAddressDirections { get; set; }
        public string PropertyAddressMapCoord { get; set; }
        public string PropertyAddressSubDivison { get; set; }
        public string PropertyAddressPhoneDay { get; set; }
        public string PropertyAddressPhoneDay1 { get; set; }
        public string PropertyAddressPhoneEve { get; set; }
        public string PropertyAddressPhoneEve1 { get; set; }
        public string PropertyDescription { get; set; }
        public int PropertyUnit { get; set; }
        public int PropertySquareFootage { get; set; }
        public string PropertyAddition { get; set; }
        public int PropertyBedRoom { get; set; }
        public int PropertyBaths { get; set; }
        public string PropertyEstimatedMonthlyUtilites { get; set; }
        public int PropertyHomeAge { get; set; }
        public int PropertyRoofAge { get; set; }
        public string SpecialInstructions { get; set; }
        public string SpecialInstructions1 { get; set; }
        public bool ConfirmationPriorWithBuyer { get; set; }
        public bool ConfirmationPriorWithSellersAgent { get; set; }
        public bool ConfirmationPriorWithBuyersAgent { get; set; }
        public string RequestedBy { get; set; }
        public string RequestedFirstName { get; set; }
        public string RequestedLastName { get; set; }
        public string RequestedByStreetAddress { get; set; }
        public string RequestedByCity { get; set; }
        public string RequestedByState { get; set; }
        public int RequestedByZip { get; set; }
        public string RequestedByEmailAddress1 { get; set; }
        public string RequestedByEmailPhoneDay { get; set; }
        public string RequestedByEmailPhoneDay1 { get; set; }
        public string RequestedByEmailPhoneEve { get; set; }
        public string RequestedByEmailPhoneEve1 { get; set; }
        public string RequestedByEmailPhoneOther { get; set; }
        public string RequestedByEmailPhoneOther1 { get; set; }
        public string RequestedByEmailOther { get; set; }
        public bool IsRequestedByEmailOther { get; set; }
        public bool RequestedByEmailContractClient { get; set; }
        public bool RequestedByEmailSend { get; set; }
        public bool RequestedByEmailOwner { get; set; }
        public bool RequestedByEmailOwnersAgent { get; set; }
        public string MainClient1 { get; set; }
        public string MainDate1 { get; set; }
        public string MainClient2 { get; set; }
        public string MainDate2 { get; set; }
        public string ScheduleInspectionDate { get; set; }
        public bool IsAdditionalTest1 { get; set; }
        public bool IsAdditionalTest2 { get; set; }
        public bool IsAdditionalTest3 { get; set; }
        public bool IsAdditionalTest4 { get; set; }
        public bool IsAdditionalTest5 { get; set; }
        public bool IsAdditionalTest6 { get; set; }
        public bool IsAdditionalTest7 { get; set; }
        public bool IsAdditionalTest8 { get; set; }
        public string TotalAdditionalTestingAmount { get; set; }
        public DateTime CreatedDate { get; set; }       
        public DateTime ModifiedDate { get; set; }
        public SellerAgent sellerAgent { get; set; }
        public BuyerAgent buyerAgent { get; set; }
        //public string DatetoDisplayMain1 { get; set; }
        //public string DatetoDisplay { get; set; }
        public string InspectorEmailID { get; set; }
        public string SquareUpNo { get; set; }
    }

    public class SellerAgent
    {
        public string AgentID { get; set; }
        public string InspectionOrderID { get; set; }
        public string AgentType { get; set; }
        public string SellerAgentFirstName { get; set; }
        public string SellerAgentLastName { get; set; }
        public string SellerAgentOffice { get; set; }
        public string SellerAgentStreetAddress { get; set; }
        public int? SellerAgentCity { get; set; }
        public int? SellerAgentState { get; set; }
        public int SellerAgentZip { get; set; }
        public string SellerAgentEmailAddress { get; set; }
        public string SellerAgentEmailAddress1 { get; set; }
        public string SellerAgentPhoneDay { get; set; }
        public string SellerAgentPhoneDay1 { get; set; }
        public string SellerAgentPhoneEve { get; set; }
        public string SellerAgentPhoneEve1 { get; set; }
        public string SellerAgentPhoneOther { get; set; }
        public string SellerAgentPhoneOther1 { get; set; }
        public bool AgentSellerSend { get; set; }
    }
    public class BuyerAgent
    {
        public string AgentID { get; set; }   
        public string InspectionOrderID { get; set; }
        public string AgentType { get; set; }
        public string BuyerAgentFirstName { get; set; }
        public string BuyerAgentLastName { get; set; }
        public string BuyerAgentOffice { get; set; }
        public string BuyerAgentStreetAddress { get; set; }
        public int? BuyerAgentCity { get; set; }
        public int? BuyerAgentState { get; set; }
        public int BuyerAgentZip { get; set; }
        public string BuyerAgentEmailAddress { get; set; }
        public string BuyerAgentEmailAddress1 { get; set; }
        public string BuyerAgentPhoneDay { get; set; }
        public string BuyerAgentPhoneDay1 { get; set; }
        public string BuyerAgentPhoneEve { get; set; }
        public string BuyerAgentPhoneEve1 { get; set; }
        public string BuyerAgentPhoneOther { get; set; }
        public string BuyerAgentPhoneOther1 { get; set; }
        public bool AgentBuyerSend { get; set; }
    }
    
}
