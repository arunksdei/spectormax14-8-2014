﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectormaxService.DATA
{
    public class CityStateAndZip
    {
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
        public int? ZipID { get; set; }
        public int? ZipCode { get; set; }
    }
}
