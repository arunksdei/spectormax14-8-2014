﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net.Mail;


namespace SpectormaxService.DATA
{
  public  class EmailHelper
    {
        public bool SendEmail(string From, string subject, string bcc, string Description, string To)
        {
            try
            {
                MailMessage objMailMessage = new MailMessage();
                objMailMessage.From = new MailAddress(ConfigurationManager.AppSettings["smtpUserName"].ToString());
                objMailMessage.To.Add(new MailAddress(To));
                objMailMessage.Subject = subject;
                objMailMessage.Priority = MailPriority.Normal;
                objMailMessage.Headers.Add("Message-Id", String.Concat("<", DateTime.Now.ToString("yyMMdd"), ".", DateTime.Now.ToString("HHmmss"), "@>"));
                objMailMessage.Body = Description;
                objMailMessage.IsBodyHtml = true;
                objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(Description, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(Description, null, "text/html");
                objMailMessage.AlternateViews.Add(plainView);
                objMailMessage.AlternateViews.Add(htmlView);

                SmtpClient objSmptp = new SmtpClient();
                objSmptp.Host = ConfigurationManager.AppSettings["smtpHost"].ToString();  //objSmptp.Host = "smtp.gmail.com";
                objSmptp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]); //objSmptp.Port = 587;
                objSmptp.DeliveryMethod = SmtpDeliveryMethod.Network;
                objSmptp.UseDefaultCredentials = false;
                objSmptp.EnableSsl = false;

                string userName = ConfigurationManager.AppSettings["smtpUserName"].ToString();
                string password = ConfigurationManager.AppSettings["smtpPassword"].ToString();
                objSmptp.Credentials = new System.Net.NetworkCredential(userName, password); //("sdm.os46@gmail.com", "sdm#e2012");
                objSmptp.Send(objMailMessage);

                return true;

                //MailMessage objMailMessage = new MailMessage();
                //objMailMessage.From = new MailAddress(ConfigurationManager.AppSettings["smtpUserName"].ToString());
                //objMailMessage.To.Add(new MailAddress(To));
                //objMailMessage.Subject = subject;
                //objMailMessage.Body = Description;
                //objMailMessage.IsBodyHtml = true;
                //objMailMessage.Priority = MailPriority.Normal;
                //objMailMessage.Headers.Add("Message-Id", String.Concat("<", DateTime.Now.ToString("yyMMdd"), ".", DateTime.Now.ToString("HHmmss"), "@>"));

                //SmtpClient objSmptp = new SmtpClient();
                //objSmptp.Host = ConfigurationManager.AppSettings["smtpHost"].ToString();  //objSmptp.Host = "smtp.gmail.com";
                //objSmptp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]); //objSmptp.Port = 587;
                //objSmptp.DeliveryMethod = SmtpDeliveryMethod.Network;
                //objSmptp.UseDefaultCredentials = false;
                 
                //string userName = ConfigurationManager.AppSettings["smtpUserName"].ToString();
                //string password = ConfigurationManager.AppSettings["smtpPassword"].ToString();
                //objSmptp.Credentials = new System.Net.NetworkCredential(userName, password); //("sdm.os46@gmail.com", "sdm#e2012");
                //objSmptp.EnableSsl = true;
                //objSmptp.Send(objMailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
