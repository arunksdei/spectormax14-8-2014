﻿using SpectorMaxDAL;
using SpectormaxService.DATA.ResultPacket;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using System.Text.RegularExpressions;

namespace SpectormaxService.DATA
{
    public class Repositary
    {
        public T SaveObject<T>(T obj) where T : class
        {
            var dbSet = DatabaseContext.dbContext.Set<T>();
            dbSet.Add(obj);
            return obj;
        }


        public List<InspectionResultDataNew> GetInspectionsFromNew(DateTime fromDate, string emailAddress)
        {
            List<InspectionResultDataNew> resultset = new List<InspectionResultDataNew>();
            InspectionResultDataNew objInspectionResultData;

            try
            {
                var insectorId = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault().ID;
                //var _IODdata = DatabaseContext.dbContext.InsectionOrderDetails.Where(x => x.CreatedDate >= fromDate && ).ToList();
                var _IODdata = (from UI in DatabaseContext.dbContext.userinfoes
                                join IO in DatabaseContext.dbContext.tblinspectionorders on UI.ID equals IO.InspectorID
                                join IOD in DatabaseContext.dbContext.tblinspectionorderdetails on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                join User in DatabaseContext.dbContext.userinfoes on IO.RequesterID equals User.ID
                                where IO.InspectionStatus == "Pending" && IO.InspectorID == insectorId && IO.Createddate >= fromDate && IO.IsAcceptedOrRejected == true 
                                select IOD).ToList();


                foreach (var item in _IODdata)
                {
                    objInspectionResultData = new InspectionResultDataNew();
                    
                    /*Requester Info*/
                    objInspectionResultData.InspectionOrderID = item.InspectionorderdetailsID == null ? "" : item.InspectionorderdetailsID;
                    objInspectionResultData.RequesterFName = item.RequesterFName == null ? "" : item.RequesterFName;
                    objInspectionResultData.RequesterLName = item.RequesterLName == null ? "" : item.RequesterLName;
                    objInspectionResultData.RequesterPhone = item.RequesterPhone == null ? "" : item.RequesterPhone;
                    objInspectionResultData.RequesterAddress = item.RequesterAddress == null ? "" : item.RequesterAddress;

                    int city_id = Convert.ToInt32(item.RequesterCity);
                    if (city_id > 0)
                        objInspectionResultData.RequesterCity = DatabaseContext.dbContext.tblcities.Where(x => x.CityId == city_id).FirstOrDefault().CityName;

                    int State_id = Convert.ToInt32(item.RequesterState);
                    if (State_id > 0)
                        objInspectionResultData.RequesterState = DatabaseContext.dbContext.tblstates.Where(x => x.StateId == State_id).FirstOrDefault().StateName;

                    if (item.RequesterZip > 0)
                        objInspectionResultData.RequesterZip = DatabaseContext.dbContext.tblzips.Where(x => x.ZipId == item.RequesterZip).FirstOrDefault().Zip;

                    objInspectionResultData.RequestedByEmailAddress = item.RequestedByEmailAddress;
                    objInspectionResultData.RequesterType = item.RequesterType;


                    /*Property Info*/
                    objInspectionResultData.PropertyAddress = item.PropertyAddress == null ? "" : item.PropertyAddress;

                    city_id = Convert.ToInt32(item.PropertyCity);
                    if (city_id > 0)
                        objInspectionResultData.PropertyCity = DatabaseContext.dbContext.tblcities.Where(x => x.CityId == city_id).FirstOrDefault().CityName;

                    State_id = Convert.ToInt32(item.PropertyState);
                    if (State_id > 0)
                        objInspectionResultData.PropertyState = DatabaseContext.dbContext.tblstates.Where(x => x.StateId == State_id).FirstOrDefault().StateName;

                    if (item.PropertyZip > 0)
                        objInspectionResultData.PropertyZip = DatabaseContext.dbContext.tblzips.Where(x => x.ZipId == item.PropertyZip).FirstOrDefault().Zip;

                    objInspectionResultData.PropertyEmail = item.PropertyEmail == null ? "" : item.PropertyEmail;
                    objInspectionResultData.OwnerName = item.OwnerName == null ? "" : item.OwnerName;
                    objInspectionResultData.OwnerPhone = item.OwnerPhone == null ? "" : item.OwnerPhone;
                    objInspectionResultData.PropertyDescription = Convert.ToString(item.PropertyDescription);
                    objInspectionResultData.Units = item.Units == null ? 0 : item.Units;
                    objInspectionResultData.SqureFootage = item.SqureFootage == null ? 0 : item.SqureFootage;
                    objInspectionResultData.Additions_Alterations = item.Additions_Alterations == null ? "" : item.Additions_Alterations;
                    objInspectionResultData.Bedrooms = item.Bedrooms == null ? 0 : item.Bedrooms;
                    objInspectionResultData.Bathrooms = item.Bathrooms == null ? 0 : item.Bathrooms;
                    objInspectionResultData.EstimatedMonthlyUtilities = item.EstimatedMonthlyUtilities == null ? 0 : item.EstimatedMonthlyUtilities;
                    objInspectionResultData.YearBuilt = item.YearBuilt == null ? 0 : item.YearBuilt;
                    objInspectionResultData.RoofAge = item.RoofAge == null ? 0 : item.RoofAge;
                    objInspectionResultData.Directions = item.Directions == null ? "" : item.Directions;

                    var furtherdetails = DatabaseContext.dbContext.tblinspectionorders.Where(x => x.InspectionOrderDetailId == item.InspectionorderdetailsID).FirstOrDefault();

                    var TimeDetails = DatabaseContext.dbContext.tblschedules.FirstOrDefault(x => x.InspectionOrderId == item.InspectionorderdetailsID);
                    objInspectionResultData.ScheduledDate = Convert.ToDateTime(furtherdetails.ScheduledDate).ToString("MM/dd/yyyy HH:mm:ss");
                    objInspectionResultData.ScheduledDate = Convert.ToDateTime(objInspectionResultData.ScheduledDate.Replace("00:00:00", TimeDetails.StartTime)).ToString("MM/dd/yyyy HH:mm:ss");
                    objInspectionResultData.ReportNo = furtherdetails.ReportNo;
                    objInspectionResultData.ServerTime = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                    resultset.Add(objInspectionResultData);
                }

                foreach (var item in resultset)
                {
                    var AgentDetails = DatabaseContext.dbContext.agentlibraries.Where(x => x.InspectionOrderID == item.InspectionOrderID).ToList();
                    foreach (var Agent in AgentDetails)
                    {
                        item.AgentID = Agent.AgentID;
                        if (Agent.AgentType == "2")
                        {
                            item.AgentType = "BuyerAgent";
                            item.BuyerAgentFirstName = Agent.AgentFirstName == null ? "" : Agent.AgentFirstName;
                            item.BuyerAgentLastName = Agent.AgentLastName == null ? "" : Agent.AgentLastName;
                            item.BuyerAgentOffice = Agent.AgentOffice == null ? "" : Agent.AgentOffice;
                            item.BuyerAgentPhoneDay = Agent.AgentPhoneDay == null ? "" : Agent.AgentPhoneDay;
                            item.BuyerAgentStreetAddress = Agent.AgentStreetAddress == null ? "" : Agent.AgentStreetAddress;

                            int city_id = Convert.ToInt32(Agent.AgentCity);
                            if (city_id > 0)
                                item.BuyerAgentCity = DatabaseContext.dbContext.tblcities.Where(x => x.CityId == city_id).FirstOrDefault().CityName;
                            else
                                item.BuyerAgentCity = "";
                            int state_id = Convert.ToInt32(Agent.AgentState);
                            if (state_id > 0)
                                item.BuyerAgentState = DatabaseContext.dbContext.tblstates.Where(x => x.StateId == state_id).FirstOrDefault().StateName;
                            else
                                item.BuyerAgentState = "";
                            if (item.BuyerAgentZip > 0)
                                item.BuyerAgentZip = DatabaseContext.dbContext.tblzips.Where(x => x.ZipId == Agent.AgentZip).FirstOrDefault().Zip;
                            else
                                item.BuyerAgentZip = 0;
                            item.BuyerAgentEmailAddress = Agent.AgentEmailAddress == null ? "" : Agent.AgentEmailAddress;

                        }
                        else
                        {
                            item.AgentType = "SellerAgent";
                            item.SellerAgentFirstName = Agent.AgentFirstName == null ? "" : Agent.AgentFirstName;
                            item.SellerAgentLastName = Agent.AgentLastName == null ? "" : Agent.AgentLastName;
                            item.SellerAgentOffice = Agent.AgentOffice == null ? "" : Agent.AgentOffice;
                            item.SellerAgentPhoneDay = Agent.AgentPhoneDay == null ? "" : Agent.AgentPhoneDay;
                            item.SellerAgentStreetAddress = Agent.AgentStreetAddress == null ? "" : Agent.AgentStreetAddress;

                            int city_id = Convert.ToInt32(Agent.AgentCity);
                            if (city_id > 0)
                                item.SellerAgentCity = DatabaseContext.dbContext.tblcities.Where(x => x.CityId == city_id).FirstOrDefault().CityName;
                            else
                                item.SellerAgentCity = "";

                            int state_id = Convert.ToInt32(Agent.AgentState);
                            if (state_id > 0)
                                item.SellerAgentState = DatabaseContext.dbContext.tblstates.Where(x => x.StateId == state_id).FirstOrDefault().StateName;
                            else
                                item.SellerAgentState = "";
                            if (item.SellerAgentZip > 0)
                                item.SellerAgentZip = DatabaseContext.dbContext.tblzips.Where(x => x.ZipId == Agent.AgentZip).FirstOrDefault().Zip;
                            else
                                item.SellerAgentZip = 0;

                            item.SellerAgentEmailAddress = Agent.AgentEmailAddress == null ? "" : Agent.AgentEmailAddress;
                        }
                    }
                }
                return resultset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InspectionResultData> GetInspectionsFrom(DateTime fromDate, string emailAddress)
        {
            List<InspectionResultData> resultset = new List<InspectionResultData>();
            InspectionResultData objInspectionResultData;

            try
            {
                var insectorId = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault().ID;

                //var _IODdata = DatabaseContext.dbContext.InsectionOrderDetails.Where(x => x.CreatedDate >= fromDate && ).ToList();

                var _IODdata = (from UI in DatabaseContext.dbContext.userinfoes
                                join IO in DatabaseContext.dbContext.inspectionorders on UI.ID equals IO.InspectorID
                                join IOD in DatabaseContext.dbContext.insectionorderdetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                join User in DatabaseContext.dbContext.userinfoes on IO.RequesterID equals User.ID
                                where IO.InspectorID == insectorId && IOD.CreatedDate >= fromDate && (IO.IsAcceptedOrRejected == true || User.Role == "9C15FC8A-CDD3-49CD-9")
                                select IOD).ToList();


                foreach (var item in _IODdata)
                {
                    objInspectionResultData = new InspectionResultData();
                    objInspectionResultData.InspectionOrderId = item.InspectionOrderId == null ? "" : item.InspectionOrderId;
                    objInspectionResultData.OwnerFirstName = item.OwnerFirstName == null ? "" : item.OwnerFirstName;
                    objInspectionResultData.OwnerLastName = item.OwnerLastName == null ? "" : item.OwnerLastName;
                    objInspectionResultData.StreetAddess = item.StreetAddess == null ? "" : item.StreetAddess;
                    objInspectionResultData.City = item.City == null ? "" : item.City;
                    objInspectionResultData.State = item.State == null ? "" : item.State;
                    objInspectionResultData.Zip = item.Zip == null ? 0 : item.Zip;
                    objInspectionResultData.EmailAddress = item.EmailAddress == null ? "" : item.EmailAddress;
                    var Uniqueid = DatabaseContext.dbContext.inspectionorders.Where(x => x.InspectionOrderID == item.InspectionOrderId).FirstOrDefault().UniqueID;
                    if (Uniqueid != null)
                    {
                        objInspectionResultData.ReportId = DatabaseContext.dbContext.inspectionorders.Where(x => x.InspectionOrderID == item.InspectionOrderId).FirstOrDefault().UniqueID;
                    }
                    string RequstID = DatabaseContext.dbContext.inspectionorders.Where(x => x.InspectionOrderID == objInspectionResultData.InspectionOrderId).FirstOrDefault().RequesterID;
                    objInspectionResultData.CreatorEmailID = DatabaseContext.dbContext.userinfoes.Where(x => x.ID == RequstID).FirstOrDefault().EmailAddress;

                    bool ifexist = DatabaseContext.dbContext.userinfoes.Any(x => x.EmailAddress == item.EmailAddress);
                    if (ifexist)
                    {
                        string RoleName = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == item.EmailAddress).FirstOrDefault().Role;
                        if (RoleName != null || RoleName != "")
                        {
                            if (RoleName == "9C15FC8A-CDD3-49CD-9")
                            {
                                objInspectionResultData.RoleName = "Home Seller";
                            }
                            else if (RoleName == "88741AFB-09FE-44C2-B")
                            {
                                objInspectionResultData.RoleName = "Home Buyer";
                            }
                            else
                            {
                                objInspectionResultData.RoleName = "";
                            }
                        }
                    }

                    objInspectionResultData.PhoneDay = item.PhoneDay == null ? "" : item.PhoneDay;
                    objInspectionResultData.PhoneDay1 = item.PhoneDay1 == null ? "" : item.PhoneDay1;
                    objInspectionResultData.PhoneEve = item.PhoneEve == null ? "" : item.PhoneEve;
                    objInspectionResultData.PhoneEve1 = item.PhoneEve1 == null ? "" : item.PhoneEve1;
                    objInspectionResultData.Other = item.Other == null ? "" : item.Other;
                    objInspectionResultData.Other1 = item.Other1 == null ? "" : item.Other1;
                    objInspectionResultData.ContractClient = item.ContractClient;
                    objInspectionResultData.SendCopy = item.SendCopy;
                    objInspectionResultData.PropertyAddress = item.PropertyAddress == null ? "" : item.PropertyAddress;
                    objInspectionResultData.PropertyAddressCity = item.PropertyAddressCity == null ? "" : item.PropertyAddressCity;

                    objInspectionResultData.PropertyAddressState = item.PropertyAddressState == null ? "" : item.PropertyAddressState;
                    objInspectionResultData.PropertyAddressZip = item.PropertyAddressZip == null ? 0 : item.PropertyAddressZip;
                    objInspectionResultData.PropertyAddressTenantName = item.PropertyAddressTenantName == null ? "" : item.PropertyAddressTenantName;
                    objInspectionResultData.PropertyAddressCrossStreet = item.PropertyAddressCrossStreet == null ? "" : item.PropertyAddressCrossStreet;
                    objInspectionResultData.PropertyAddressDirections = item.PropertyAddressDirections == null ? "" : item.PropertyAddressDirections;
                    objInspectionResultData.PropertyAddressMapCoord = item.PropertyAddressMapCoord == null ? "" : item.PropertyAddressMapCoord;
                    objInspectionResultData.PropertyAddressSubDivison = item.PropertyAddressSubDivison == null ? "" : item.PropertyAddressSubDivison;
                    objInspectionResultData.PropertyAddressPhoneDay = item.PropertyAddressPhoneDay == null ? "" : item.PropertyAddressPhoneDay;
                    objInspectionResultData.PropertyAddressPhoneDay1 = item.PropertyAddressPhoneDay1 == null ? "" : item.PropertyAddressPhoneDay1;
                    objInspectionResultData.PropertyAddressPhoneEve = item.PropertyAddressPhoneEve == null ? "" : item.PropertyAddressPhoneEve;
                    objInspectionResultData.PropertyAddressPhoneEve1 = item.PropertyAddressPhoneEve1 == null ? "" : item.PropertyAddressPhoneEve1;
                    objInspectionResultData.PropertyDescription = item.PropertyDescription == null ? "" : item.PropertyDescription;
                    objInspectionResultData.PropertyUnit = item.PropertyUnit == null ? 0 : item.PropertyUnit;
                    objInspectionResultData.PropertySquareFootage = item.PropertySquareFootage == null ? 0 : item.PropertySquareFootage;
                    objInspectionResultData.PropertyAddition = item.PropertyAddition == null ? "" : item.PropertyAddition;
                    objInspectionResultData.PropertyBedRoom = item.PropertyBedRoom == null ? 0 : item.PropertyBedRoom;
                    objInspectionResultData.PropertyBaths = item.PropertyBaths == null ? 0 : item.PropertyBaths;
                    objInspectionResultData.PropertyEstimatedMonthlyUtilites = item.PropertyEstimatedMonthlyUtilites == null ? "" : item.PropertyEstimatedMonthlyUtilites;
                    objInspectionResultData.PropertyHomeAge = item.PropertyHomeAge == null ? 0 : item.PropertyHomeAge;
                    objInspectionResultData.PropertyRoofAge = item.PropertyRoofAge == null ? 0 : item.PropertyRoofAge;
                    objInspectionResultData.SpecialInstructions = item.SpecialInstructions == null ? "" : item.SpecialInstructions;
                    objInspectionResultData.ConfirmationPriorWithBuyer = item.ConfirmationPriorWithBuyer;
                    objInspectionResultData.ConfirmationPriorWithSellersAgent = item.ConfirmationPriorWithSellersAgent;
                    objInspectionResultData.ConfirmationPriorWithBuyersAgent = item.ConfirmationPriorWithBuyersAgent;

                    objInspectionResultData.RequestedBy = item.RequestedBy == null ? "" : item.RequestedBy;
                    objInspectionResultData.RequestedFirstName = item.RequestedFirstName == null ? "" : item.RequestedFirstName;
                    objInspectionResultData.RequestedLastName = item.RequestedLastName == null ? "" : item.RequestedLastName;
                    objInspectionResultData.RequestedByStreetAddress = item.RequestedByStreetAddress == null ? "" : item.RequestedByStreetAddress;
                    objInspectionResultData.RequestedByCity = item.RequestedByCity == null ? "" : item.RequestedByCity;
                    objInspectionResultData.RequestedByState = item.RequestedByState == null ? "" : item.RequestedByState;
                    objInspectionResultData.RequestedByZip = item.RequestedByZip == null ? 0 : item.RequestedByZip;
                    objInspectionResultData.RequestedByEmailAddress1 = item.RequestedByEmailAddress1 == null ? "" : item.RequestedByEmailAddress1;
                    objInspectionResultData.RequestedByEmailPhoneDay = item.RequestedByEmailPhoneDay == null ? "" : item.RequestedByEmailPhoneDay;
                    objInspectionResultData.RequestedByEmailPhoneDay1 = item.RequestedByEmailPhoneDay1 == null ? "" : item.RequestedByEmailPhoneDay1;
                    objInspectionResultData.RequestedByEmailPhoneEve = item.RequestedByEmailPhoneEve == null ? "" : item.RequestedByEmailPhoneEve;
                    objInspectionResultData.RequestedByEmailPhoneEve1 = item.RequestedByEmailPhoneEve1 == null ? "" : item.RequestedByEmailPhoneEve1;
                    objInspectionResultData.RequestedByEmailPhoneOther = item.RequestedByEmailPhoneOther == null ? "" : item.RequestedByEmailPhoneOther;
                    objInspectionResultData.RequestedByEmailPhoneOther1 = item.RequestedByEmailPhoneOther1 == null ? "" : item.RequestedByEmailPhoneOther1;
                    objInspectionResultData.RequestedByEmailOther = item.RequestedByEmailOther == null ? "" : item.RequestedByEmailOther;
                    objInspectionResultData.IsRequestedByEmailOther = item.IsRequestedByEmailOther;
                    objInspectionResultData.RequestedByEmailContractClient = item.RequestedByEmailContractClient;
                    objInspectionResultData.RequestedByEmailSend = item.RequestedByEmailSend;
                    objInspectionResultData.RequestedByEmailOwner = item.RequestedByEmailOwner;
                    objInspectionResultData.RequestedByEmailOwnersAgent = item.RequestedByEmailOwnersAgent;

                    objInspectionResultData.MainClient1 = item.MainClient1 == null ? "" : item.MainClient1;

                    objInspectionResultData.MainDate1 = item.MainDate1;
                    objInspectionResultData.MainDate1_new = item.MainDate1.ToString() == null ? "" : item.MainDate1.ToString();

                    objInspectionResultData.MainClient2 = item.MainClient2 == null ? "" : item.MainClient2;

                    objInspectionResultData.MainDate2 = item.MainDate2;
                    objInspectionResultData.MainDate2_new = item.MainDate2.ToString() == null ? "" : item.MainDate2.ToString();

                    objInspectionResultData.ScheduleInspectionDate = item.ScheduleInspectionDate;
                    objInspectionResultData.ScheduleInspectionDate_new = item.ScheduleInspectionDate.ToString() == null ? "" : item.ScheduleInspectionDate.ToString();

                    objInspectionResultData.IsAdditionalTest1 = item.IsAdditionalTest1;
                    objInspectionResultData.IsAdditionalTest2 = item.IsAdditionalTest2;
                    objInspectionResultData.IsAdditionalTest3 = item.IsAdditionalTest3;
                    objInspectionResultData.IsAdditionalTest4 = item.IsAdditionalTest4;
                    objInspectionResultData.IsAdditionalTest5 = item.IsAdditionalTest5;
                    objInspectionResultData.IsAdditionalTest6 = item.IsAdditionalTest6;
                    objInspectionResultData.IsAdditionalTest7 = item.IsAdditionalTest7;
                    objInspectionResultData.IsAdditionalTest8 = item.IsAdditionalTest8;
                    objInspectionResultData.TotalAdditionalTestingAmount = item.TotalAdditionalTestingAmount == null ? "" : item.TotalAdditionalTestingAmount;

                    objInspectionResultData.CreatedDate = item.CreatedDate;
                    objInspectionResultData.CreatedDate_new = item.CreatedDate.ToString() == null ? "" : item.CreatedDate.ToString(); ;
                    objInspectionResultData.ModifiedDate = item.ModifiedDate;
                    objInspectionResultData.ModifiedDate_new = item.ModifiedDate.ToString() == null ? "" : item.ModifiedDate.ToString();
                    objInspectionResultData.ServerTime = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                    resultset.Add(objInspectionResultData);
                }

                foreach (var item in resultset)
                {
                    var AgentDetails = DatabaseContext.dbContext.agentlibraries.Where(x => x.InspectionOrderID == item.InspectionOrderId).ToList();
                    foreach (var Agent in AgentDetails)
                    {
                        item.AgentID = Agent.AgentID;
                        if (Agent.AgentType == "2")
                        {
                            item.AgentType = "BuyerAgent";
                            item.BuyerAgentFirstName = Agent.AgentFirstName == null ? "" : Agent.AgentFirstName;
                            item.BuyerAgentLastName = Agent.AgentLastName == null ? "" : Agent.AgentLastName;
                            item.BuyerAgentOffice = Agent.AgentOffice == null ? "" : Agent.AgentOffice;
                            item.BuyerAgentPhoneDay = Agent.AgentPhoneDay == null ? "" : Agent.AgentPhoneDay;
                            item.BuyerAgentPhoneDay1 = Agent.AgentPhoneDay1 == null ? "" : Agent.AgentPhoneDay1;
                            item.BuyerAgentPhoneEve = Agent.AgentPhoneEve == null ? "" : Agent.AgentPhoneEve;
                            item.BuyerAgentPhoneEve1 = Agent.AgentPhoneEve1 == null ? "" : Agent.AgentPhoneEve1;
                            item.BuyerAgentPhoneOther = Agent.AgentPhoneOther == null ? "" : Agent.AgentPhoneOther;
                            item.BuyerAgentPhoneOther1 = Agent.AgentPhoneOther1 == null ? "" : Agent.AgentPhoneOther1;
                            item.BuyerAgentStreetAddress = Agent.AgentStreetAddress == null ? "" : Agent.AgentStreetAddress;
                            // item.BuyerAgentCity = Agent.AgentCity == null ? "" : Agent.AgentCity;
                            //  item.BuyerAgentState = Agent.AgentState == null ? "" : Agent.AgentState;
                            item.BuyerAgentZip = Agent.AgentZip ?? 0;
                            item.AgentBuyerSend = Agent.AgentSend;
                            item.BuyerAgentEmailAddress = Agent.AgentEmailAddress == null ? "" : Agent.AgentEmailAddress;
                            item.BuyerAgentEmailAddress1 = Agent.AgentEmailAddress == null ? "" : Agent.AgentEmailAddress;
                        }
                        else
                        {
                            item.AgentType = "SellerAgent";
                            item.SellerAgentFirstName = Agent.AgentFirstName == null ? "" : Agent.AgentFirstName;
                            item.SellerAgentLastName = Agent.AgentLastName == null ? "" : Agent.AgentLastName;
                            item.SellerAgentOffice = Agent.AgentOffice == null ? "" : Agent.AgentOffice;
                            item.SellerAgentPhoneDay = Agent.AgentPhoneDay == null ? "" : Agent.AgentPhoneDay;
                            item.SellerAgentPhoneDay1 = Agent.AgentPhoneDay1 == null ? "" : Agent.AgentPhoneDay1;
                            item.SellerAgentPhoneEve = Agent.AgentPhoneEve == null ? "" : Agent.AgentPhoneEve;
                            item.SellerAgentPhoneEve1 = Agent.AgentPhoneEve1 == null ? "" : Agent.AgentPhoneEve1;
                            item.SellerAgentPhoneOther = Agent.AgentPhoneOther == null ? "" : Agent.AgentPhoneOther;
                            item.SellerAgentPhoneOther1 = Agent.AgentPhoneOther1 == null ? "" : Agent.AgentPhoneOther1;
                            item.SellerAgentStreetAddress = Agent.AgentStreetAddress == null ? "" : Agent.AgentStreetAddress;
                            //    item.SellerAgentCity = Agent.AgentCity == null ? "" : Agent.AgentCity;
                            //    item.SellerAgentState = Agent.AgentState == null ? "" : Agent.AgentState;
                            item.SellerAgentZip = Agent.AgentZip ?? 0;
                            item.AgentSellerSend = Agent.AgentSend;
                            item.SellerAgentEmailAddress = Agent.AgentEmailAddress == null ? "" : Agent.AgentEmailAddress;
                            item.SellerAgentEmailAddress1 = Agent.AgentEmailAddress == null ? "" : Agent.AgentEmailAddress;
                        }
                    }
                }
                return resultset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Stream GetAdminCommentLibrary(DateTime CreatedDate,string inspectorId)
        {
            List<Section> objSection = new List<Section>();
            List<SubSectionList> objSubSectionList = new List<SubSectionList>();
            List<CommentLibrary> objCommentLibrary = new List<CommentLibrary>();


            int SectionCount = 0;
            //JsonObject 

            //var test = DatabaseContext.dbContext.AdminCommentLibraries.Where(x => x.ModifiendDate >= CreatedDate && x.IsDeleted == false).Distinct().ToList().Select(y => y.SectionId).ToList();
            StringBuilder JsonString = new StringBuilder();
            JsonString.Append("{");
            objSection = (from s in DatabaseContext.dbContext.admincommentlibraries
                          where s.ModifiendDate >= CreatedDate && s.IsDeleted == false
                          select new Section
                                       {
                                           SectionID = s.SectionId
                                       }).Distinct().ToList();

            foreach (var item in objSection)
            {
                SectionCount++;
                int SubSectionCount = 0;
                JsonString.Append("\"" + item.SectionID + "\":{");

                objSubSectionList = (from ss in DatabaseContext.dbContext.admincommentlibraries
                                     where ss.SectionId == item.SectionID
                                     && ss.ModifiendDate >= CreatedDate && ss.IsDeleted == false
                                     select new SubSectionList
                                     {
                                         SubSectionID = ss.SubSectionId
                                     }).Distinct().ToList();
                item.SubSection = objSubSectionList;
                foreach (var itemsub in objSubSectionList)
                {
                    SubSectionCount++;
                    JsonString.Append("\"" + itemsub.SubSectionID + "\":[");
                    objCommentLibrary = (from k in DatabaseContext.dbContext.tblinspectorcomments
                                         join l in DatabaseContext.dbContext.admincommentlibraries
                                         on k.CommentId equals l.Id
                                         where k.UserId == inspectorId
                                         //from acl in DatabaseContext.dbContext.admincommentlibraries
                                         where k.ModifiedDate >= CreatedDate && l.SubSectionId == itemsub.SubSectionID
                                         //&& acl.IsDeleted == false
                                         select new CommentLibrary
                                         {
                                             Id = l.Id,
                                             SectionId = l.SectionId,
                                             SubSectionId = l.SubSectionId,
                                             CommentBoxID = l.CommentBoxID,
                                             CommentBoxName = l.CommentBoxName,
                                             CommentText = l.CommentText,
                                             CreatedDate = l.CreatedDate,
                                             ModifiedDate = l.ModifiendDate,
                                             //IsDeleted = acl.IsDeleted
                                         }).ToList();
                    itemsub.CommentLibrary = objCommentLibrary;
                    int Commentcount = 0;

                    foreach (var itemLib in objCommentLibrary)
                    {
                        Commentcount++;
                        JsonString.Append("{");
                        JsonString.Append("\"CommentBoxID\"" + ":\"" + itemLib.CommentBoxID + "\",");
                        JsonString.Append("\"CommentBoxName\"" + ":\"" + itemLib.CommentBoxName + "\",");
                        itemLib.CommentText = Regex.Replace(itemLib.CommentText, @"\s+", " ");
                        JsonString.Append("\"CommentText\"" + ":\"" + itemLib.CommentText.Trim() + "\"");
                        if (objCommentLibrary.Count == Commentcount)
                        {
                            JsonString.Append("}");
                        }
                        else
                        {
                            JsonString.Append("},");
                        }

                    }
                    if (SubSectionCount == objSubSectionList.Count)
                        JsonString.Append("]");
                    else
                        JsonString.Append("],");
                }

                //   JsonString.Append("}");
                if (SectionCount == objSection.Count)
                {
                    JsonString.Append("},");
                    JsonString.Append("\"ServerTime\":{");
                    JsonString.Append("\"ServerTime\"" + ":\"" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "\"}");
                }
                else
                    JsonString.Append("},");

                //  JsonString.Append("}");
            }
            JsonString.Append("}");


            return new MemoryStream(Encoding.UTF8.GetBytes(JsonString.ToString()));
            // return objSection;
        }


        
    }
}