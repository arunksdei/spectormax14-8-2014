﻿using SpectorMaxDAL;
using SpectormaxService.DATA;
using SpectormaxService.DATA.ResultPacket;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Transactions;
using System.IO;
using System.Web;
using System.Drawing;
using System.Data.Common;
using System.Data;
using System.Data.Objects;
using MySql.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace SpectormaxService
{
    public class ResultSet
    {
        public bool SuccessFlag { get; set; }
        public string ErrorMessage { get; set; }
    }

    public static class DatabaseContext
    {
        private static db_spectromaxEntities _db_spectromaxEntities;
        public static db_spectromaxEntities dbContext
        {
            get
            {
                return _db_spectromaxEntities ?? (_db_spectromaxEntities = new db_spectromaxEntities());
            }
        }
    }

    public class SpectormaxServiceAPI : ISpectormaxServiceAPI
    {
        Repositary repo = new Repositary();
        ResultSet result;
        inspectionreport ObjInspectionReport;
        subsectionstatu objSubSectionStatu;
        #region Get Methods

        public string GetTest()
        {
            return "Hello World";
        }

        public List<InspectionResultDataNew> GetInsectionOrderDetailsNew(string fromDate, string emailAddress)
        {
            DateTime dt;
            if (fromDate == "")
            {
                dt = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault().CreatedDate.Value;
                return repo.GetInspectionsFromNew(dt, emailAddress);
            }
            else if (DateTime.TryParse(fromDate, out dt))
            {
                return repo.GetInspectionsFromNew(dt, emailAddress);
            }
            return null;
        }

        public List<InspectionResultData> GetInsectionOrderDetails(string fromDate, string emailAddress)
        {
            DateTime dt;
            if (fromDate == "")
            {
                dt = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault().CreatedDate.Value;
                return repo.GetInspectionsFrom(dt, emailAddress);
            }
            else if (DateTime.TryParse(fromDate, out dt))
            {
                return repo.GetInspectionsFrom(dt, emailAddress);
            }
            return null;
        }
        public Stream GetAdminCommentLibrary(string CreatedDate, string inspectorId)
        {
            DateTime dt;
            if (DateTime.TryParse(CreatedDate, out dt))
                return repo.GetAdminCommentLibrary(dt,inspectorId);
            return null;
        }
        public List<emailtemplate> GetAdditionalTestingPrice()
        {
            List<emailtemplate> objListofEmailTemplate = new List<emailtemplate>();
            objListofEmailTemplate = DatabaseContext.dbContext.emailtemplates.ToList().GetRange(5, 8);
            return objListofEmailTemplate;
        }
        public InspectorStatus GetInspectorStatus(string username, string password)
        {
            InspectorStatus objInspectorStatus = new InspectorStatus();
            try
            {
                var InspectorList = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == username.Trim() && x.Password == password).ToList();
                if (InspectorList.Count > 0)
                {
                    var InspectorID = InspectorList.FirstOrDefault().ID;

                    InspectorStatus objtemplist = new InspectorStatus();
                    //objtemplist.zipcodelist = DatabaseContext.dbContext.zipcodeassigneds.Where(x => x.InspectorID == InspectorID).ToList().Select(x => x.ZipCode).ToList();
                    objtemplist.zipcodelist = (from k in DatabaseContext.dbContext.tblzips
                                               join z in DatabaseContext.dbContext.zipcodeassigneds on k.ZipId equals z.ZipCode
                                               where z.InspectorID == InspectorID
                                               select k.Zip
                                               ).ToList();

                    objInspectorStatus = (from InsDetails in DatabaseContext.dbContext.inspectordetails
                                          join uinfo in DatabaseContext.dbContext.userinfoes on InsDetails.UserID equals uinfo.ID
                                          join zipcode in DatabaseContext.dbContext.zipcodeassigneds on uinfo.ID equals zipcode.InspectorID
                                          where InsDetails.UserID == InspectorID
                                          select new InspectorStatus
                                          {
                                              UserName = uinfo.EmailAddress.Trim(),
                                              InspectorName = uinfo.FirstName + " " + uinfo.LastName,
                                              Password = uinfo.Password,
                                              Status = uinfo.IsActive,
                                              zipcodelist = objtemplist.zipcodelist
                                          }).FirstOrDefault();

                    if (objInspectorStatus == null)
                        objInspectorStatus = new InspectorStatus();

                    return objInspectorStatus;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "") { result.SuccessFlag = false; result.ErrorMessage = "Can not connect to database"; }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return objInspectorStatus;
        }

        /// <summary>
        /// Function called when user selects NO option in IPAD at the time of options(YES or NO) appears to buy updatable report
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public ResultSet NonUpdatableReport(string emailAddress, string InspectionID)
        {
            try
            {
               var db_con = new db_spectromaxEntities();
               result = new ResultSet();
               bool userExists = DatabaseContext.dbContext.userinfoes.Any(x => x.EmailAddress == emailAddress);
               if (!userExists)
               {
                   RegisterUserIfNoteExist(emailAddress, Guid.NewGuid().ToString(), "7D9A1A9C-5305-455F-A07C-0BF3080A6753");
                   var requesterId = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault();
                   db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().IsUpdatable = false;
                   db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().RequesterID1 = requesterId.ID;
                   db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().IsVerified = false;
                   db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().InspectionStatus = "Completed";
                   /*Save Random number in table*/
                   //Random random = new Random();
                   //int num = random.Next(10000);
                   //db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().VerificationNumber = num.ToString();
                   db_con.SaveChanges();
                   /*region Send random number in email*/
                   #region Send random number in email
                   //string MailTemplate = db_con.emailtemplates.Where(x => x.TemplateId == 54).FirstOrDefault().TemplateDiscription;
                   //var UserInfo = db_con.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault();
                   //MailTemplate = MailTemplate.Replace("{First Name}", UserInfo.FirstName);
                   //MailTemplate = MailTemplate.Replace("{Last Name}", UserInfo.LastName);
                   //MailTemplate = MailTemplate.Replace("{reportNo}", db_con.tblinspectionorders.FirstOrDefault(x => x.InspectionOrderDetailId == InspectionID).ReportNo);
                   //MailTemplate = MailTemplate.Replace("UniqueId", "UniqueId="+ db_con.tblinspectionorders.FirstOrDefault(x => x.InspectionOrderDetailId == InspectionID).ReportNo);
                   //MailTemplate = MailTemplate.Replace("Please click here update the report", "");
                   //MailTemplate = MailTemplate.Replace("Dashboard", "Dashboard/" + InspectionID);
                   string MailTemplate = db_con.emailtemplates.Where(x => x.TemplateId == 54).FirstOrDefault().TemplateDiscription;
                   var UserInfo = db_con.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault();
                   MailTemplate = MailTemplate.Replace("{First Name}", UserInfo.FirstName);
                   MailTemplate = MailTemplate.Replace("{Last Name}", UserInfo.LastName);
                   MailTemplate = MailTemplate.Replace("{reportNo}", db_con.tblinspectionorders.FirstOrDefault(x => x.InspectionOrderDetailId == InspectionID).ReportNo);
                   MailTemplate = MailTemplate.Replace("Your verification number to update the reopot is:{Number}", " ");
                   MailTemplate = MailTemplate.Replace("Dashboard", "Dashboard/" + InspectionID);
                   EmailHelper email = new EmailHelper();
                   //string emailBody = "Verification number to update the reopot is: " + num.ToString();
                   //MailTemplate = MailTemplate.Replace("{Number}", num.ToString());
                   bool status = email.SendEmail("", "Spectormax : Purchase Report", "", MailTemplate,emailAddress);
                   #endregion
                   //EmailHelper email = new EmailHelper();
                   //string emailBody = "Verification number to update the reopot is: " + num.ToString();
                   //bool status = email.SendEmail("", "Updatable Report Verification Number", "", emailBody, emailAddress);
                   result.SuccessFlag = true;
                   return result;
               }
               else
               {
                   var requesterId = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault();
                   db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().IsUpdatable = false;
                   db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().RequesterID1 = requesterId.ID;
                   db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().IsVerified = false;
                   db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().InspectionStatus = "Completed";
                   /*Save Random number in table*/
                   //Random random = new Random();
                   //int num = random.Next(10000);
                   //db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionID).FirstOrDefault().VerificationNumber = num.ToString();
                   db_con.SaveChanges();
                   /*region Send random number in email*/
                   #region Send random number in email
                   //string MailTemplate = db_con.emailtemplates.Where(x => x.TemplateId == 54).FirstOrDefault().TemplateDiscription;
                   //var UserInfo = db_con.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault();
                   //MailTemplate = MailTemplate.Replace("{First Name}", UserInfo.FirstName);
                   //MailTemplate = MailTemplate.Replace("{Last Name}", UserInfo.LastName);
                   //MailTemplate = MailTemplate.Replace("{reportNo}", db_con.tblinspectionorders.FirstOrDefault(x => x.InspectionOrderDetailId == InspectionID).ReportNo);
                   //MailTemplate = MailTemplate.Replace("UniqueId", "UniqueId=" + db_con.tblinspectionorders.FirstOrDefault(x => x.InspectionOrderDetailId == InspectionID).ReportNo);
                   //MailTemplate = MailTemplate.Replace("Please click here update the report", "");
                   //MailTemplate = MailTemplate.Replace("Dashboard", "Dashboard/" + InspectionID);
                   string MailTemplate = db_con.emailtemplates.Where(x => x.TemplateId == 54).FirstOrDefault().TemplateDiscription;
                   var UserInfo = db_con.userinfoes.Where(x => x.EmailAddress == emailAddress).FirstOrDefault();
                   MailTemplate = MailTemplate.Replace("{First Name}", UserInfo.FirstName);
                   MailTemplate = MailTemplate.Replace("{Last Name}", UserInfo.LastName);
                   MailTemplate = MailTemplate.Replace("{reportNo}", db_con.tblinspectionorders.FirstOrDefault(x => x.InspectionOrderDetailId == InspectionID).ReportNo);
                   MailTemplate = MailTemplate.Replace("Your verification number to update the reopot is:{Number}", " ");
                   MailTemplate = MailTemplate.Replace("Dashboard", "Dashboard/" + InspectionID);

                   EmailHelper email = new EmailHelper();
                   //string emailBody = "Verification number to update the reopot is: " + num.ToString();
                   //MailTemplate = MailTemplate.Replace("{Number}", num.ToString());
                   bool status = email.SendEmail("", "Spectormax : Purchase Report", "", MailTemplate, emailAddress);
                   #endregion
                   //EmailHelper email = new EmailHelper();
                   //string emailBody = "Verification number to update the reopot is: " + num.ToString();
                   //bool status = email.SendEmail("", "Updatable Report Verification Number", "", emailBody, emailAddress);
                   result.SuccessFlag = true;
                   return result;
               }
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "") { result.SuccessFlag = false; result.ErrorMessage = "Can not connect to database"; }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }


        public ResultSet ForgotPassword(string emailAddress)
        {

            try
            {
                var InspectorList = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == emailAddress).ToList();
                if (InspectorList.Count > 0)
                {
                    result = new ResultSet();
                    if (InspectorList != null)
                    {
                        if (InspectorList.FirstOrDefault().Role == "85DA6641-E08A-4192-A")
                        {
                            InspectorList.FirstOrDefault().IsActive = false;
                            result.SuccessFlag = true;
                            DatabaseContext.dbContext.SaveChanges();
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "") { result.SuccessFlag = false; result.ErrorMessage = "Can not connect to database"; }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet CheckUserEmailExists(string emailAddress)
        {
            result = new ResultSet();
            try
            {
                var InspectorList = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == emailAddress).ToList();
                if (InspectorList.Count > 0)
                {
                    if (InspectorList != null)
                    {
                        if (InspectorList.Any(x => x.Role == "9C15FC8A-CDD3-49CD-9"))
                            result.SuccessFlag = true;
                    }
                    return result;
                }
                else
                    result.SuccessFlag = false;
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "") { result.SuccessFlag = false; result.ErrorMessage = "Can not connect to database"; }
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }

        public List<CityStateAndZip> GetCityStateandZip()
        {
            List<CityStateAndZip> objCityStateAndZip = new List<CityStateAndZip>();

            objCityStateAndZip = (from s in DatabaseContext.dbContext.tblstates
                                  join c in DatabaseContext.dbContext.tblcities on s.StateId equals c.StateId
                                  join z in DatabaseContext.dbContext.tblzips on c.CityId equals z.CityId
                                  select new CityStateAndZip
                                  {
                                      StateID = s.StateId,
                                      StateName = s.StateName,
                                      CityID = c.CityId,
                                      CityName = c.CityName,
                                      ZipID = z.ZipId,
                                      ZipCode = z.Zip
                                  }).ToList();

            return objCityStateAndZip;

        }

        #endregion

        #region Post Methods

        public ResultSet AddInspectionAdditionalInfo(List<inspectionadditionalinfo> InspectionAdditionalInfo)
        {
            result = new ResultSet();
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    foreach (var item in InspectionAdditionalInfo)
                    {
                        item.AdditionalInfoId = Guid.NewGuid().ToString();
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.inspectionadditionalinfoes.Add(item);
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddAgentLibrary(List<agentlibrary> AgentLibrary)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    string[] allIds = AgentLibrary.Select(x => x.AgentID).ToArray();
                    List<agentlibrary> Avl_Id = DatabaseContext.dbContext.agentlibraries.Where(a => allIds.Contains(a.AgentID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            AgentLibrary.Remove(AgentLibrary.Single(s => s.AgentID == item.AgentID));
                        }
                    }


                    result = new ResultSet();
                    foreach (var item in AgentLibrary)
                    {
                        db_con.agentlibraries.Add(item);
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddAppliancescommonsections(List<appliancescommonsection> Appliancescommonsection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = Appliancescommonsection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<appliancescommonsection> Avl_Id = DatabaseContext.dbContext.appliancescommonsections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            Appliancescommonsection.Remove(Appliancescommonsection.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in Appliancescommonsection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.appliancescommonsections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComments;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckboxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/

                        if (item.CheckboxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }

        public ResultSet AddAppliancesDishwashDisposalRangeSection(List<appliancesdishwashdisposalrange_section> AppliancesDishwashDisposalRange_section)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = AppliancesDishwashDisposalRange_section.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<appliancesdishwashdisposalrange_section> Avl_Id = DatabaseContext.dbContext.appliancesdishwashdisposalrange_sections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            AppliancesDishwashDisposalRange_section.Remove(AppliancesDishwashDisposalRange_section.Single(s => s.ID == item.ID));
                        }
                    }


                    foreach (var item in AppliancesDishwashDisposalRange_section)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.appliancesdishwashdisposalrange_sections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InsepectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionId;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = null;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InsepectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionId;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddAppliancesDryerDoorBellSectiion(List<appliancesdryerdoorbellsectiion> AppliancesDryerDoorBellSectiion)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = AppliancesDryerDoorBellSectiion.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<appliancesdryerdoorbellsectiion> Avl_Id = DatabaseContext.dbContext.appliancesdryerdoorbellsectiions.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            AppliancesDryerDoorBellSectiion.Remove(AppliancesDryerDoorBellSectiion.Single(s => s.ID == item.ID));
                        }
                    }


                    foreach (var item in AppliancesDryerDoorBellSectiion)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.appliancesdryerdoorbellsectiions.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubsectionID;
                        ObjInspectionReport.PresetComments = item.PresentComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = null;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubsectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddAppliancesHoodVentOtherSection(List<applianceshoodventothersection> AppliancesHoodVentOtherSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = AppliancesHoodVentOtherSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<applianceshoodventothersection> Avl_Id = DatabaseContext.dbContext.applianceshoodventothersections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            AppliancesHoodVentOtherSection.Remove(AppliancesHoodVentOtherSection.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in AppliancesHoodVentOtherSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.applianceshoodventothersections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderId;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubsectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderId;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubsectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddAppliancesOvenSection(List<appliancesovensection> AppliancesOvenSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = AppliancesOvenSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<appliancesovensection> Avl_Id = DatabaseContext.dbContext.appliancesovensections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            AppliancesOvenSection.Remove(AppliancesOvenSection.Single(s => s.ID == item.ID));
                        }
                    }


                    foreach (var item in AppliancesOvenSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.appliancesovensections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = null;
                        ObjInspectionReport.CheckboxID = item.CheckboxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckboxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddElectrialCommanSection(List<electrialcommansection> ElectrialCommanSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = ElectrialCommanSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<electrialcommansection> Avl_Id = DatabaseContext.dbContext.electrialcommansections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            ElectrialCommanSection.Remove(ElectrialCommanSection.Single(s => s.ID == item.ID));
                        }
                    }


                    foreach (var item in ElectrialCommanSection)
                    {

                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.electrialcommansections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.ReportID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubsectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.NoticeValue;
                        if (item.DirctionDrop != null && item.DirctionDrop != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirctionDrop);
                        else
                            item.DirctionDrop = null;
                        ObjInspectionReport.Type = item.TypeValue;
                        ObjInspectionReport.CheckboxID = item.ConditionID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.ConditionID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.ReportID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubsectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddElectricalPanel(List<electricalpanel> ElectricalPanel)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = ElectricalPanel.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<electricalpanel> Avl_Id = DatabaseContext.dbContext.electricalpanels.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            ElectricalPanel.Remove(ElectricalPanel.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in ElectricalPanel)
                    {

                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.electricalpanels.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.ReportID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubsectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDrop != null && item.DirectionDrop != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDrop);
                        else
                            item.DirectionDrop = null;
                        ObjInspectionReport.Type = null;
                        ObjInspectionReport.CheckboxID = item.ConditionID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.ConditionID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.ReportID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubsectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddElectricalRoom(List<electricalroom> ElectricalRoom)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = ElectricalRoom.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<electricalroom> Avl_Id = DatabaseContext.dbContext.electricalrooms.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            ElectricalRoom.Remove(ElectricalRoom.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in ElectricalRoom)
                    {

                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.electricalrooms.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.ReportID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDrop != null && item.DirectionDrop != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDrop);
                        else
                            item.DirectionDrop = null;
                        ObjInspectionReport.Type = null;
                        ObjInspectionReport.CheckboxID = item.ConditionID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.ConditionID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.ReportID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddElectricalService(List<electricalservice> ElectricalService)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = ElectricalService.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<electricalservice> Avl_Id = DatabaseContext.dbContext.electricalservices.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            ElectricalService.Remove(ElectricalService.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in ElectricalService)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.electricalservices.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.ReportID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDrop != null && item.DirectionDrop != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDrop);
                        else
                            item.DirectionDrop = null;
                        ObjInspectionReport.Type = null;
                        ObjInspectionReport.CheckboxID = item.ConditionID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.ConditionID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.ReportID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddGroundCommonSection(List<groundcommonsection> GroundCommonSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allProductIds = GroundCommonSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<groundcommonsection> Avl_GroundCommonSection = DatabaseContext.dbContext.groundcommonsections.Where(a => allProductIds.Contains(a.ID)).ToList();
                    if (Avl_GroundCommonSection.Count > 0)
                    {
                        foreach (var item in Avl_GroundCommonSection)
                        {
                            GroundCommonSection.Remove(GroundCommonSection.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in GroundCommonSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.groundcommonsections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddGroundGassection(List<groundgassection> GroundGassection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = GroundGassection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<groundgassection> Avl_Id = DatabaseContext.dbContext.groundgassections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            GroundGassection.Remove(GroundGassection.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in GroundGassection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.groundgassections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = null;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddGroundH2OSection(List<groundh2osection> GroundH2OSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = GroundH2OSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<groundh2osection> Avl_Id = DatabaseContext.dbContext.groundh2osection.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            GroundH2OSection.Remove(GroundH2OSection.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in GroundH2OSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.groundh2osection.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionId;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = null;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionId;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddGroundSewerSection(List<groundsewersection> GroundSewerSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = GroundSewerSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<groundsewersection> Avl_Id = DatabaseContext.dbContext.groundsewersections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            GroundSewerSection.Remove(GroundSewerSection.Single(s => s.ID == item.ID));
                        }
                    }


                    foreach (var item in GroundSewerSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.groundsewersections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.C_InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = null;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.C_InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddGroundSprinkSysSection(List<groundsprinksyssection> GroundSprinkSysSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();
                    string[] allIds = GroundSprinkSysSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<groundsprinksyssection> Avl_Id = DatabaseContext.dbContext.groundsprinksyssections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            GroundSprinkSysSection.Remove(GroundSprinkSysSection.Single(s => s.ID == item.ID));
                        }
                    }


                    foreach (var item in GroundSprinkSysSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.groundsprinksyssections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.Preset;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddHVACCommonSection(List<hvaccommonsection> HVACCommonSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = HVACCommonSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<hvaccommonsection> Avl_Id = DatabaseContext.dbContext.hvaccommonsections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            HVACCommonSection.Remove(HVACCommonSection.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in HVACCommonSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.hvaccommonsections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            ObjInspectionReport.ItemID = item.ID;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddHVACCooling(List<HVACCoolingService> HVACCooling)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = HVACCooling.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<string> Avl_Id = DatabaseContext.dbContext.hvaccoolings.Where(a => allIds.Contains(a.ID)).ToList().Select(m => m.ID).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            HVACCooling.Remove(HVACCooling.Single(s => s.ID == item));
                        }
                    }

                    foreach (var item in HVACCooling)
                    {
                        hvaccooling objHVACCooling = new hvaccooling();
                        objHVACCooling.ID = item.ID;
                        objHVACCooling.InspectionOrderID = item.InspectionOrderID;
                        objHVACCooling.BTU = item.BTU;
                        objHVACCooling.CheckBoxID = item.CheckBoxID;
                        objHVACCooling.Comments = item.Comments;
                        objHVACCooling.CreatedDate = DateTime.Now;
                        objHVACCooling.ModifiedDate = DateTime.Now;
                        objHVACCooling.Notice = item.Notice;
                        objHVACCooling.PresetComment = item.PresetComment;
                        objHVACCooling.SectionID = item.SectionID;
                        objHVACCooling.SerialNumber = item.SerialNumber;
                        objHVACCooling.SubsectionID = item.SubsectionID;
                        objHVACCooling.Type = item.Type;
                        objHVACCooling.Location = item.Location;
                        objHVACCooling.TempDifferential = item.TempDifferential;
                        objHVACCooling.DirectionDropID = item.DirectionDropID;
                        objHVACCooling.Manufacturer = item.Manufacturer;
                        if (item.ManufacturerDate != "")
                            objHVACCooling.ManufacturerDate = DateTime.Parse(item.ManufacturerDate);
                        else
                            objHVACCooling.ManufacturerDate = null;
                        db_con.hvaccoolings.Add(objHVACCooling);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubsectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubsectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddHVACDuctsVents(List<hvacductsvent> HVACDuctsVent)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = HVACDuctsVent.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<hvacductsvent> Avl_Id = DatabaseContext.dbContext.hvacductsvents.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            HVACDuctsVent.Remove(HVACDuctsVent.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in HVACDuctsVent)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.hvacductsvents.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddHVACEquipBoileRadiantSection(List<HVACEquipBoileRadiantSectionService> HVACEquipBoileRadiantSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = HVACEquipBoileRadiantSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<string> Avl_Id = DatabaseContext.dbContext.hvacequipboileradiantsections.Where(a => allIds.Contains(a.ID)).ToList().Select(m => m.ID).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            HVACEquipBoileRadiantSection.Remove(HVACEquipBoileRadiantSection.Single(s => s.ID == item));
                        }
                    }


                    foreach (var item in HVACEquipBoileRadiantSection)
                    {
                        hvacequipboileradiantsection objHVACEquipBoileRadiantSection = new hvacequipboileradiantsection();

                        objHVACEquipBoileRadiantSection.ID = item.ID;
                        objHVACEquipBoileRadiantSection.InspectionOrderId = item.InspectionOrderId;
                        objHVACEquipBoileRadiantSection.BTU = item.BTU;
                        objHVACEquipBoileRadiantSection.CheckBoxID = item.CheckBoxID;
                        objHVACEquipBoileRadiantSection.Comments = item.Comments;
                        objHVACEquipBoileRadiantSection.CreatedDate = DateTime.Now;
                        objHVACEquipBoileRadiantSection.ModifiedDate = DateTime.Now;
                        objHVACEquipBoileRadiantSection.Notice = item.Notice;
                        objHVACEquipBoileRadiantSection.PresetComment = item.PresetComment;
                        objHVACEquipBoileRadiantSection.SectionID = item.SectionID;
                        objHVACEquipBoileRadiantSection.SerialNumber = item.SerialNumber;
                        objHVACEquipBoileRadiantSection.SubSectionID = item.SubSectionID;
                        objHVACEquipBoileRadiantSection.Type = item.Type;
                        objHVACEquipBoileRadiantSection.EnergySource = item.EnergySource;
                        objHVACEquipBoileRadiantSection.Manufacturer = item.Manufacturer;
                        objHVACEquipBoileRadiantSection.SerialNumber = item.SerialNumber;
                        objHVACEquipBoileRadiantSection.DirectionDropID = item.DirectionDropID;

                        if (item.ManufacturerDate != "")
                            objHVACEquipBoileRadiantSection.ManufacturerDate = DateTime.Parse(item.ManufacturerDate);
                        else
                            objHVACEquipBoileRadiantSection.ManufacturerDate = null;

                        db_con.hvacequipboileradiantsections.Add(objHVACEquipBoileRadiantSection);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderId;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderId;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddOptionalCommonSections(List<optionalcommonsection> OptionalCommonSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = OptionalCommonSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<optionalcommonsection> Avl_Id = DatabaseContext.dbContext.optionalcommonsections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            OptionalCommonSection.Remove(OptionalCommonSection.Single(s => s.ID == item.ID));
                        }
                    }


                    foreach (var item in OptionalCommonSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.optionalcommonsections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddOptionalTestingSections(List<optionaltestingsection> OptionalTestingSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = OptionalTestingSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<optionaltestingsection> Avl_Id = DatabaseContext.dbContext.optionaltestingsections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            OptionalTestingSection.Remove(OptionalTestingSection.Single(s => s.ID == item.ID));
                        }
                    }


                    foreach (var item in OptionalTestingSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.optionaltestingsections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubsectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubsectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddPlumbingCommonSection(List<plumbingcommonsection> PlumbingCommonSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = PlumbingCommonSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<plumbingcommonsection> Avl_Id = DatabaseContext.dbContext.plumbingcommonsections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            PlumbingCommonSection.Remove(PlumbingCommonSection.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in PlumbingCommonSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.plumbingcommonsections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderId;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderId;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddPlumbingMainlineKitchenOthers_Sections(List<plumbingmainlinekitchenothers_section> PlumbingMainlineKitchenOthers_Section)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = PlumbingMainlineKitchenOthers_Section.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<plumbingmainlinekitchenothers_section> Avl_Id = DatabaseContext.dbContext.plumbingmainlinekitchenothers_sections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            PlumbingMainlineKitchenOthers_Section.Remove(PlumbingMainlineKitchenOthers_Section.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in PlumbingMainlineKitchenOthers_Section)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.plumbingmainlinekitchenothers_sections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderId;
                        ObjInspectionReport.SectionId = item.SectionID;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderId;
                            objSubSectionStatu.SectionID = item.SectionID;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddPlumbingWaterheat(List<PlumbingWaterheatService> PlumbingWaterheat)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = PlumbingWaterheat.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<string> Avl_Id = DatabaseContext.dbContext.plumbingmainlinekitchenothers_sections.Where(a => allIds.Contains(a.ID)).ToList().Select(x => x.ID).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            PlumbingWaterheat.Remove(PlumbingWaterheat.Single(s => s.ID == item));
                        }
                    }

                    foreach (var item in PlumbingWaterheat)
                    {
                        plumbingwaterheat objPlumbingWaterheat = new plumbingwaterheat();
                        objPlumbingWaterheat.ID = item.ID;
                        objPlumbingWaterheat.InspectionOrderId = item.InspectionOrderId;
                        objPlumbingWaterheat.SectionId = item.SectionId;
                        objPlumbingWaterheat.SubSectionID = item.SubSectionID;
                        objPlumbingWaterheat.CheckBoxID = item.CheckBoxID;
                        objPlumbingWaterheat.Type = item.Type;
                        objPlumbingWaterheat.Capacity = item.Capacity;
                        objPlumbingWaterheat.Manufacturer = item.Manufacturer;
                        objPlumbingWaterheat.DOB = item.DOB;
                        objPlumbingWaterheat.Location = item.Location;
                        objPlumbingWaterheat.StaticPressure = item.StaticPressure;
                        objPlumbingWaterheat.Notice = item.Notice;
                        objPlumbingWaterheat.Comments = item.Comments;
                        objPlumbingWaterheat.DirectionDropID = item.DirectionDropID;
                        objPlumbingWaterheat.PresetComment = item.PresetComment;
                        objPlumbingWaterheat.CreatedDate = DateTime.Now;
                        objPlumbingWaterheat.ModifiedDate = DateTime.Now;

                        db_con.plumbingwaterheats.Add(objPlumbingWaterheat);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderId;
                        ObjInspectionReport.SectionId = item.SectionId;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderId;
                            objSubSectionStatu.SectionID = item.SectionId;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddToOptionalEnergy(List<optionalenergy> optionalEnergy)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();
                    string[] allIds = optionalEnergy.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<optionalenergy> Avl_Id = DatabaseContext.dbContext.optionalenergies.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            optionalEnergy.Remove(optionalEnergy.Single(s => s.ID == item.ID));
                        }
                    }
                    foreach (var item in optionalEnergy)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.optionalenergies.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.inspectionOrderID;
                        ObjInspectionReport.SectionId = item.sectionID;
                        ObjInspectionReport.SubsectionId = item.subsectionID;
                        ObjInspectionReport.PresetComments = null;
                        ObjInspectionReport.Comments = item.comments;
                        ObjInspectionReport.Notice = item.notice;
                        if (item.directionDropID != null && item.directionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.directionDropID);
                        else
                            item.directionDropID = null;
                        ObjInspectionReport.Type = item.type;
                        ObjInspectionReport.CheckboxID = item.checkboxID;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.checkboxID != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.inspectionOrderID;
                            objSubSectionStatu.SectionID = item.sectionID;
                            objSubSectionStatu.SubSectionID = item.subsectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            ObjInspectionReport.ItemID = item.ID;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }
        public ResultSet AddStructuralCommonSections(List<structuralcommonsection> StructuralCommonSection)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    string[] allIds = StructuralCommonSection.Select(x => x.ID).ToArray();//{ "5", "12", "2", "2" };
                    List<structuralcommonsection> Avl_Id = DatabaseContext.dbContext.structuralcommonsections.Where(a => allIds.Contains(a.ID)).ToList();
                    if (Avl_Id.Count > 0)
                    {
                        foreach (var item in Avl_Id)
                        {
                            StructuralCommonSection.Remove(StructuralCommonSection.Single(s => s.ID == item.ID));
                        }
                    }

                    foreach (var item in StructuralCommonSection)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        db_con.structuralcommonsections.Add(item);

                        /*Adding data to Inspection Report and SubSectionStatus Tables*/
                        ObjInspectionReport = new inspectionreport();
                        ObjInspectionReport.ID = Guid.NewGuid().ToString();
                        ObjInspectionReport.InspectionOrderId = item.InspectionOrderID;
                        ObjInspectionReport.SectionId = item.SectionId;
                        ObjInspectionReport.SubsectionId = item.SubSectionID;
                        ObjInspectionReport.PresetComments = item.PresetComment;
                        ObjInspectionReport.Comments = item.Comments;
                        ObjInspectionReport.Notice = item.Notice;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            ObjInspectionReport.DirectionDropID = int.Parse(item.DirectionDropID);
                        else
                            item.DirectionDropID = null;
                        ObjInspectionReport.Type = item.Type;
                        ObjInspectionReport.CheckboxID = item.CheckBoxiD;
                        ObjInspectionReport.CreatedDate = DateTime.Now;
                        ObjInspectionReport.ModifiedDate = DateTime.Now;
                        ObjInspectionReport.ItemID = item.ID;
                        db_con.inspectionreports.Add(ObjInspectionReport);

                        /*Adding Data to SubSectionStatus*/
                        if (item.CheckBoxiD != "1")
                        {
                            objSubSectionStatu = new subsectionstatu();
                            objSubSectionStatu.ID = Guid.NewGuid().ToString();
                            objSubSectionStatu.InspectionOrderID = item.InspectionOrderID;
                            objSubSectionStatu.SectionID = item.SectionId;
                            objSubSectionStatu.SubSectionID = item.SubSectionID;
                            objSubSectionStatu.Status = false;
                            objSubSectionStatu.IsUpdated = false;
                            objSubSectionStatu.Comment = item.Comments;
                            objSubSectionStatu.CreatedDate = DateTime.Now;
                            db_con.subsectionstatus.Add(objSubSectionStatu);
                        }
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }


        public ResultSet AddInspectionRequestNew(List<InspectionOrderDetailPocoNew> InspectionOrderDetailPoco)
        {
            result = new ResultSet();
            try
            {

                string Message = "The Following Zip Codes Are Not Available In Database:";
                string ZipCodes = string.Empty;
                foreach (var item in InspectionOrderDetailPoco)
                {
                    int zipval = Convert.ToInt32(item.PropertyZip);
                    var Zip = DatabaseContext.dbContext.tblzips.FirstOrDefault(x => x.Zip == zipval);
                    if (Zip != null)
                    {
                        var requesterId = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == item.RequestedByEmailAddress).FirstOrDefault();
                        if (requesterId != null)
                        {
                            SaveInspectionRequestNew(item, requesterId.ID);
                        }
                        else
                        {
                            string RequesterID = Guid.NewGuid().ToString();
                            RegisterUserIfNoteExist(item.RequestedByEmailAddress, RequesterID, "7D9A1A9C-5305-455F-A07C-0BF3080A6753");

                            SaveInspectionRequestNew(item, RequesterID);

                            //if (item.PaymentAmount != "")
                            //{
                            //    Payment objPayment = new Payment();
                            //    objPayment.CreatedDate = DateTime.Now;
                            //    if (item.PaymentAmount != null)
                            //    {
                            //        objPayment.PaymentAmount = float.Parse(item.PaymentAmount);
                            //    }
                            //    else
                            //    {
                            //        objPayment.PaymentAmount = 0;
                            //    }
                            //    objPayment.PaymentID = Guid.NewGuid().ToString();
                            //    objPayment.PaymentStatus = "Success";
                            //    objPayment.ReportID = item.InspectionOrderID;
                            //    objPayment.RoleID = item.UserRole;
                            //    objPayment.PaymentDate = DateTime.Now;
                            //    objPayment.UserID = RequesterID;
                            //    DatabaseContext.dbContext.SaveChanges();
                            //}
                        }
                    }
                    else
                    {
                        ZipCodes += item.PropertyZip.ToString() + ",";
                    }
                }
                result.SuccessFlag = true;
                if (ZipCodes.Length > 0)
                {
                    ZipCodes = ZipCodes.Remove(ZipCodes.Length - 1);
                    result.ErrorMessage = Message + ZipCodes;
                }
                else
                {
                    result.ErrorMessage = null;
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }

        public List<InspectionOrderDetailPocoNew> AddInspectionRequestNew1()
        {
            List<InspectionOrderDetailPocoNew> res = new List<InspectionOrderDetailPocoNew>();

            InspectionOrderDetailPocoNew obj = new InspectionOrderDetailPocoNew() { buyerAgent = new BuyerAgentNew(), sellerAgent = new SellerAgentNew() };
            //InspectionOrderDetailPocoNew obj1 = new InspectionOrderDetailPocoNew() { buyerAgent = new BuyerAgent(), sellerAgent = new SellerAgent() };
            res.Add(obj);
            //res.Add(obj1);

            return res;
        }

        //public ResultSet AddInsectionOrderDetail(List<InsectionOrderDetail> InsectionOrderDetail)
        //{
        //    try
        //    {
        //        var db_con = new db_spectromaxEntities();
        //        using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
        //        {
        //            result = new ResultSet();

        //            foreach (var item in InsectionOrderDetail)
        //            {
        //                item.CreatedDate = DateTime.Now;
        //                item.ModifiedDate = DateTime.Now;
        //                db_con.InsectionOrderDetails.Add(item);
        //            }
        //            db_con.SaveChanges();
        //            result.SuccessFlag = true;
        //            scope.Complete();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.SuccessFlag = false;
        //        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
        //    }
        //    return result;
        //}

        //public ResultSet AddInspectionRequest(List<InspectionOrderDetailPoco> InspectionOrderDetailPoco)
        //{
        //    result = new ResultSet();
        //    try
        //    {
        //        using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
        //        {
        //            string Message = "The Following Zip Codes Are Not Available In Database:";
        //            string ZipCodes = string.Empty;
        //            foreach (var item in InspectionOrderDetailPoco)
        //            {          
        //               var Zip = DatabaseContext.dbContext.ZipCodes.FirstOrDefault(x => x.Zip == item.PropertyAddressZip);
        //                if (Zip!=null)
        //                {
        //                    var requesterId = DatabaseContext.dbContext.UserInfoes.Where(x => x.EmailAddress == item.RequesterEmailID).FirstOrDefault();
        //                    if (requesterId != null)
        //                    {
        //                        if (requesterId.Role != "9C15FC8A-CDD3-49CD-9")
        //                        {
        //                            result.ErrorMessage = "Requester should be a Home Seller";
        //                            result.SuccessFlag = true;
        //                            return result;
        //                        }
        //                        else
        //                        {
        //                            SaveInspectionRequest(item, requesterId.ID);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string RequesterID = Guid.NewGuid().ToString();
        //                        RegisterUserIfNoteExist(item.RequesterEmailID, RequesterID);

        //                        SaveInspectionRequest(item, RequesterID);

        //                        if (item.SquareUpNo != "")
        //                        {
        //                            Payment objPayment = new Payment();
        //                            objPayment.CreatedDate = DateTime.Now;
        //                            if (item.TotalAdditionalTestingAmount != null)
        //                            {
        //                                objPayment.PaymentAmount = float.Parse(item.TotalAdditionalTestingAmount);
        //                            }
        //                            else
        //                            {
        //                                objPayment.PaymentAmount = 0;
        //                            }
        //                            objPayment.PaymentID = Guid.NewGuid().ToString();
        //                            objPayment.PaymentStatus = "Success";
        //                            objPayment.ReportID = item.InspectionOrderId;
        //                            objPayment.RoleID = "9C15FC8A-CDD3-49CD-9";
        //                            objPayment.PaymentDate = DateTime.Now;
        //                            objPayment.UserID = RequesterID;
        //                            DatabaseContext.dbContext.SaveChanges();
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    ZipCodes += item.PropertyAddressZip.ToString()+",";  
        //                } 
        //            }
        //            result.SuccessFlag = true;
        //            if (ZipCodes.Length > 0)
        //            {
        //                ZipCodes = ZipCodes.Remove(ZipCodes.Length - 1);
        //                result.ErrorMessage = Message + ZipCodes;
        //            }
        //            else
        //            {
        //                result.ErrorMessage = null;
        //            }
        //        }
        //    }
        //    catch (DbEntityValidationException e)
        //    {
        //        StringBuilder sb = new StringBuilder();
        //        foreach (var eve in e.EntityValidationErrors)
        //        {
        //            foreach (var ve in eve.ValidationErrors)
        //            {
        //                sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
        //                                            ve.PropertyName,
        //                                            ve.ErrorMessage));
        //            }
        //        }
        //        result.SuccessFlag = false;
        //        result.ErrorMessage = sb.ToString();
        //    }   
        //    catch (Exception ex)
        //    {
        //        if (ex.Message != null || ex.Message != "")
        //        {
        //            if (ex.Message.ToString().ToLower().Contains("underlying"))
        //            {
        //                result.SuccessFlag = false;
        //                result.ErrorMessage = "Can not connect to database";
        //            }
        //            else
        //            {
        //                result.SuccessFlag = false;
        //                result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
        //                if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
        //                {
        //                    result.SuccessFlag = true;
        //                    result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
        //                }
        //            }
        //        }
        //        else
        //        {
        //            result.SuccessFlag = false;
        //            result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
        //        }
        //    }
        //    return result;
        //}

        public ResultSet UpdateInspectionStatus(List<ServiceStatus> objServiceStatus)
        {
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();
                    tblinspectionorder objInspectionOrder = new tblinspectionorder();
                    foreach (var item in objServiceStatus)
                    {
                        objInspectionOrder = db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == item.InspectionID).FirstOrDefault();
                        objInspectionOrder.InspectionStatus = "Completed";
                        //db_con.InspectionOrders.Where(x => x.InspectionId == item.InspectionID).FirstOrDefault().InspectionStatus = "Completed";
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }

        public ResultSet Payment(PaymentData objPaymentData)
        {
            result = new ResultSet();
            try
            {
                var db_con = new db_spectromaxEntities();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    var requesterId = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == objPaymentData.EmailID).FirstOrDefault();

                    string newuserid = Guid.NewGuid().ToString();

                    if (requesterId == null)
                    {
                        RegisterUserIfNoteExist(objPaymentData.EmailID, newuserid, "7D9A1A9C-5305-455F-A07C-0BF3080A6753");
                        requesterId = DatabaseContext.dbContext.userinfoes.Where(x => x.EmailAddress == objPaymentData.EmailID).FirstOrDefault();
                    }

                    if (requesterId != null)
                    {
                        if (!db_con.payments.Any(x => x.ReportID == objPaymentData.InspectionID))
                        {
                            if (objPaymentData.SquareUpNo != "")
                            {
                                payment objPayment = new payment();
                                objPayment.CreatedDate = DateTime.Now;
                                if (objPaymentData.Amount != "")
                                {
                                    objPayment.PaymentAmount = float.Parse(objPaymentData.Amount);
                                }
                                else
                                {
                                    objPayment.PaymentAmount = 0;
                                }
                                objPayment.PaymentID = Guid.NewGuid().ToString();
                                objPayment.PaymentStatus = "Success";
                                objPayment.UserID = db_con.userinfoes.Where(x => x.EmailAddress == objPaymentData.EmailID).FirstOrDefault().ID;
                                objPayment.ReportID = objPaymentData.InspectionID;
                                objPayment.RoleID = requesterId.Role;
                                objPayment.PaymentDate = DateTime.Now;
                                objPayment.PaymentType = "UpdateReport";
                                objPayment.SquareUpNumber = objPaymentData.SquareUpNo;
                                objPayment.PaymentAmount = objPaymentData.Amount == "" ? 0 : Convert.ToSingle(objPaymentData.Amount);
                                var MaxInvoiceNo = DatabaseContext.dbContext.payments.Max(x => x.InvoiceNo);

                                if (MaxInvoiceNo == null || Convert.ToInt32(MaxInvoiceNo) == 0) { objPayment.InvoiceNo = 2000; }
                                else { objPayment.InvoiceNo = Convert.ToInt32(MaxInvoiceNo) + 1; }

                                db_con.payments.Add(objPayment);
                                db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == objPaymentData.InspectionID).FirstOrDefault().IsUpdatable = true;
                                //db_con.tblinspectionorderdetails.Where(x => x.InspectionorderdetailsID == objPaymentData.InspectionID).FirstOrDefault().IsAdditionalTest2 = true;
                                //db_con.tblinspectionorderdetails.Where(x => x.InspectionorderdetailsID == objPaymentData.InspectionID).FirstOrDefault().TotalAdditionalTestingAmount = objPaymentData.Amount;

                                /*Buyer created a request and inviting seller*/
                                db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == objPaymentData.InspectionID).FirstOrDefault().RequesterID1 = requesterId.ID;
                                db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == objPaymentData.InspectionID).FirstOrDefault().IsVerified = false;
                                db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == objPaymentData.InspectionID).FirstOrDefault().InspectionStatus = "Completed";
                                //if (requesterId.EmailAddress != objPaymentData.EmailID)
                                //{
                                //    var sellerUserDetails = db_con.userinfoes.Where(x => x.EmailAddress == objPaymentData.EmailID).FirstOrDefault();
                                //    db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == objPaymentData.InspectionID).FirstOrDefault().RequesterID1 = sellerUserDetails.ID;
                                //}
                                //else
                                //{
                                //    db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == objPaymentData.InspectionID).FirstOrDefault().RequesterID1 = requesterId.ID;
                                //}

                                payment objpayment = new payment();
                                #region Save Random number in table
                                Random random = new Random();
                                int num = random.Next(10000);
                                db_con.tblinspectionorders.Where(x => x.InspectionOrderDetailId == objPaymentData.InspectionID).FirstOrDefault().VerificationNumber = num.ToString();
                                #endregion

                                #region Send random number in email

                                string MailTemplate = db_con.emailtemplates.Where(x => x.TemplateId == 53).FirstOrDefault().TemplateDiscription;
                                var UserInfo = db_con.userinfoes.Where(x => x.EmailAddress == objPaymentData.EmailID).FirstOrDefault();
                                MailTemplate = MailTemplate.Replace("{First Name}", UserInfo.FirstName);
                                MailTemplate = MailTemplate.Replace("{Last Name}", UserInfo.LastName);
                                MailTemplate = MailTemplate.Replace("{reportNo}", db_con.tblinspectionorders.FirstOrDefault(x => x.InspectionOrderDetailId == objPaymentData.InspectionID).ReportNo);
                                MailTemplate = MailTemplate.Replace("Please click here to update the report", "");
                                MailTemplate = MailTemplate.Replace("Dashboard", "Dashboard/" + objPaymentData.InspectionID);

                                EmailHelper email = new EmailHelper();
                                //string emailBody = "Verification number to update the reopot is: " + num.ToString();
                                MailTemplate = MailTemplate.Replace("{Code}", num.ToString());
                                bool status = email.SendEmail("", "Updatable Report Verification Number", "", MailTemplate, objPaymentData.EmailID);
                                #endregion

                                db_con.SaveChanges();
                            }
                        }
                        result.SuccessFlag = true;
                        scope.Complete();
                        scope.Dispose();
                        return result;
                    }
                    else
                    {
                        result.ErrorMessage = "Requester should be a Home Seller";
                        result.SuccessFlag = false;
                        return result;
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                        {
                            result.SuccessFlag = true;
                            result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }

        private void SaveInspectionRequestNew(DATA.ResultPacket.InspectionOrderDetailPocoNew item, string RequesterID)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.RequiresNew))
                {

                    tblinspectionorderdetail objtblInspectionOrderDetails = new tblinspectionorderdetail();
                    agentlibrary objAgentLibraryBuyer = new agentlibrary();
                    agentlibrary objAgentLibrarySeller = new agentlibrary();
                    tblinspectionorder objtblInspectionOrder = new tblinspectionorder();

                    #region Check If Inspector Exist

                    int zipval = Convert.ToInt32(item.PropertyZip);
                    int zipid = DatabaseContext.dbContext.tblzips.Where(x => x.Zip == zipval).FirstOrDefault().ZipId;
                    string assignedInspectorId = DatabaseContext.dbContext.zipcodeassigneds.FirstOrDefault(x => x.ZipCode == zipid) != null ? DatabaseContext.dbContext.zipcodeassigneds.FirstOrDefault(x => x.ZipCode == zipid).InspectorID : "";

                    //var countReports = "";
                    int TotalInspectorReportCount = 0;
                    string UniqueId = "Empty";

                    if (assignedInspectorId != "")
                    {
                        int Stateval = DatabaseContext.dbContext.tblstates.FirstOrDefault(x => x.StateName == item.PropertyState).StateId;

                        var countReports = (from IO in DatabaseContext.dbContext.tblinspectionorders
                                            join IOD in DatabaseContext.dbContext.tblinspectionorderdetails on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                            where IO.InspectorID == assignedInspectorId && IOD.PropertyState == Stateval
                                            select IO.InspectorID);
                        TotalInspectorReportCount = countReports.Count();

                        string state = DatabaseContext.dbContext.tblstates.Where(x => x.StateId == Stateval).FirstOrDefault().StateName;
                        UniqueId = state + DatabaseContext.dbContext.inspectordetails.Where(x => x.UserID == assignedInspectorId).Select(y => y.InspectorNo).FirstOrDefault() + (TotalInspectorReportCount + 1).ToString();
                    }

                    #endregion


                    #region Save Order Details
                    /*Requester Info*/
                    objtblInspectionOrderDetails.InspectionorderdetailsID = item.InspectionOrderID;
                    //objtblInspectionOrderDetails.InspectionOrderId = objtblInspectionOrder.InspectionOrderID;
                    objtblInspectionOrderDetails.RequesterFName = item.RequesterFName.Trim();
                    objtblInspectionOrderDetails.RequesterLName = item.RequesterLName.Trim();
                    objtblInspectionOrderDetails.RequesterPhone = item.RequesterPhone;

                    //int RequestState = DatabaseContext.dbContext.tblstates.Where(x => x.StateName == item.RequesterState).FirstOrDefault().StateId;
                    objtblInspectionOrderDetails.RequesterState = DatabaseContext.dbContext.tblstates.FirstOrDefault(x => x.StateName == item.RequesterState).StateId;

                    //int RequestCity = DatabaseContext.dbContext.tblcities.Where(x => x.CityName == item.RequesterCity && x.StateId == RequestState).FirstOrDefault().CityId;
                    objtblInspectionOrderDetails.RequesterCity = DatabaseContext.dbContext.tblcities.FirstOrDefault(x => x.CityName == item.RequesterCity).CityId;//item.RequesterCity;


                    //objtblInspectionOrderDetails.RequesterZip = Convert.ToInt32(item.RequesterZip);
                    int requester_zip = Convert.ToInt32(item.RequesterZip);
                    objtblInspectionOrderDetails.RequesterZip = DatabaseContext.dbContext.tblzips.FirstOrDefault(x => x.Zip == requester_zip).ZipId;

                    objtblInspectionOrderDetails.RequestedByEmailAddress = item.RequestedByEmailAddress.Trim();
                    objtblInspectionOrderDetails.RequesterAddress = item.RequesterAddress.Trim();
                    objtblInspectionOrderDetails.RequesterType = Convert.ToInt32(item.RequesterType);

                    /*Property Info*/
                    objtblInspectionOrderDetails.PropertyAddress = item.PropertyAddress;
                    objtblInspectionOrderDetails.OwnerPhone = item.OwnerPhone;
                    //int PropState = DatabaseContext.dbContext.tblstates.Where(x => x.StateName == item.RequesterState).FirstOrDefault().StateId;
                    //objtblInspectionOrderDetails.PropertyState = PropState;//item.PropertyState;
                    objtblInspectionOrderDetails.PropertyState = DatabaseContext.dbContext.tblstates.FirstOrDefault(x => x.StateName == item.PropertyState).StateId;
                    //int PropCity = DatabaseContext.dbContext.tblstates.Where(x => x.StateName == item.RequesterState).FirstOrDefault().StateId;
                    objtblInspectionOrderDetails.PropertyCity = DatabaseContext.dbContext.tblcities.FirstOrDefault(x => x.CityName == item.PropertyCity).CityId;//PropCity;//item.PropertyCity;

                    int Property_zip = Convert.ToInt32(item.RequesterZip);
                    objtblInspectionOrderDetails.PropertyZip = DatabaseContext.dbContext.tblzips.FirstOrDefault(x => x.Zip == Property_zip).ZipId;//Convert.ToInt32(item.PropertyZip);

                    objtblInspectionOrderDetails.PropertyEmail = item.PropertyEmail;
                    objtblInspectionOrderDetails.OwnerName = item.OwnerName;
                    objtblInspectionOrderDetails.PropertyDescription = Convert.ToInt32(item.PropertyDescription);
                    objtblInspectionOrderDetails.Units = Convert.ToInt32(item.Units);
                    objtblInspectionOrderDetails.SqureFootage = Convert.ToInt32(item.SqureFootage);
                    objtblInspectionOrderDetails.Additions_Alterations = item.Additions_Alterations;
                    objtblInspectionOrderDetails.Bedrooms = Convert.ToInt32(item.Bedrooms);
                    objtblInspectionOrderDetails.Bathrooms = Convert.ToInt32(item.Bathrooms);
                    objtblInspectionOrderDetails.EstimatedMonthlyUtilities = Convert.ToInt32(item.EstimatedMonthlyUtilities);
                    objtblInspectionOrderDetails.YearBuilt = Convert.ToInt32(item.YearBuilt);
                    objtblInspectionOrderDetails.RoofAge = Convert.ToInt32(item.RoofAge);
                    objtblInspectionOrderDetails.Directions = item.Directions.Trim();
                    DatabaseContext.dbContext.tblinspectionorderdetails.Add(objtblInspectionOrderDetails);

                    /*Buyer Agent*/

                    objAgentLibraryBuyer.AgentID = Guid.NewGuid().ToString();
                    objAgentLibraryBuyer.InspectionOrderID = item.InspectionOrderID;
                    objAgentLibraryBuyer.AgentType = "2";
                    objAgentLibraryBuyer.AgentFirstName = item.buyerAgent.BuyerAgentFirstName;
                    objAgentLibraryBuyer.AgentLastName = item.buyerAgent.BuyerAgentLastName;
                    objAgentLibraryBuyer.AgentOffice = item.buyerAgent.BuyerAgentOffice;
                    objAgentLibraryBuyer.AgentPhoneDay = item.buyerAgent.BuyerAgentPhoneDay;
                    objAgentLibraryBuyer.AgentStreetAddress = item.buyerAgent.BuyerAgentStreetAddress;
                    //objAgentLibraryBuyer.AgentCity = item.buyerAgent.BuyerAgentCity;
                    if (item.buyerAgent.BuyerAgentCity != null && item.buyerAgent.BuyerAgentCity != "")
                        objAgentLibraryBuyer.AgentCity = DatabaseContext.dbContext.tblcities.FirstOrDefault(x => x.CityName == item.buyerAgent.BuyerAgentCity).CityId;
                    // objAgentLibraryBuyer.AgentState = item.buyerAgent.BuyerAgentState; ;
                    if (item.buyerAgent.BuyerAgentState != null && item.buyerAgent.BuyerAgentState != "")
                        objAgentLibraryBuyer.AgentState = DatabaseContext.dbContext.tblstates.FirstOrDefault(x => x.StateName == item.buyerAgent.BuyerAgentState).StateId;//PropCity;//item.PropertyCity;
                    if (item.buyerAgent.BuyerAgentZip != null && item.buyerAgent.BuyerAgentZip != "")
                    {
                        int Agent_zip = Convert.ToInt32(item.RequesterZip);
                        objAgentLibraryBuyer.AgentZip = DatabaseContext.dbContext.tblzips.FirstOrDefault(x => x.Zip == Agent_zip).ZipId;
                        //objAgentLibraryBuyer.AgentZip = Convert.ToInt32(item.buyerAgent.BuyerAgentZip);
                    }
                    objAgentLibraryBuyer.AgentEmailAddress = item.buyerAgent.BuyerAgentEmailAddress;
                    DatabaseContext.dbContext.agentlibraries.Add(objAgentLibraryBuyer);

                    /*Seller Agent*/
                    objAgentLibrarySeller.AgentID = Guid.NewGuid().ToString();
                    objAgentLibrarySeller.InspectionOrderID = item.InspectionOrderID;
                    objAgentLibrarySeller.AgentType = "1";
                    objAgentLibrarySeller.AgentFirstName = item.sellerAgent.SellerAgentFirstName;
                    objAgentLibrarySeller.AgentLastName = item.sellerAgent.SellerAgentLastName;
                    objAgentLibrarySeller.AgentOffice = item.sellerAgent.SellerAgentOffice;
                    objAgentLibrarySeller.AgentPhoneDay = item.sellerAgent.SellerAgentPhoneDay;
                    objAgentLibrarySeller.AgentStreetAddress = item.sellerAgent.SellerAgentStreetAddress;
                    //objAgentLibrarySeller.AgentCity = item.sellerAgent.SellerAgentCity;
                    if (item.sellerAgent.SellerAgentCity != null && item.sellerAgent.SellerAgentCity != "")
                        objAgentLibrarySeller.AgentCity = DatabaseContext.dbContext.tblcities.FirstOrDefault(x => x.CityName == item.sellerAgent.SellerAgentCity).CityId;
                    //objAgentLibrarySeller.AgentState = item.sellerAgent.SellerAgentState; ;
                    if (item.sellerAgent.SellerAgentState != null && item.sellerAgent.SellerAgentState != "")
                        objAgentLibrarySeller.AgentState = DatabaseContext.dbContext.tblstates.FirstOrDefault(x => x.StateName == item.sellerAgent.SellerAgentState).StateId;
                    if (item.sellerAgent.SellerAgentZip != null && item.sellerAgent.SellerAgentZip != "")
                    {
                        //objAgentLibrarySeller.AgentZip = Convert.ToInt32(item.sellerAgent.SellerAgentZip);
                        int Agent_zip = Convert.ToInt32(item.RequesterZip);
                        objAgentLibrarySeller.AgentZip = DatabaseContext.dbContext.tblzips.FirstOrDefault(x => x.Zip == Agent_zip).ZipId;
                    }
                    objAgentLibrarySeller.AgentEmailAddress = item.sellerAgent.SellerAgentEmailAddress;
                    DatabaseContext.dbContext.agentlibraries.Add(objAgentLibrarySeller);
                    #endregion


                    #region Save Data in Inspection Order table
                    #region Commented Code
                    //MySqlConnection conn = new MySqlConnection((((System.Data.Entity.DbContext)(DatabaseContext.dbContext)).Database.Connection).ConnectionString);
                    //using (var ts = new System.Transactions.TransactionScope(scopeOption: TransactionScopeOption.RequiresNew))
                    //{
                    //    //string connStr = ConfigurationManager.ConnectionStrings["SpectorMaxConnectionString1"].ConnectionString;

                    //    //conn.Close();
                    //    conn.Open();
                    //    MySqlCommand cmd = new MySqlCommand("srv_insertinspectiondata", conn);
                    //    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //    cmd.Parameters.AddWithValue("InspectionOrderID", Guid.NewGuid().ToString());

                    //    cmd.Parameters.AddWithValue("InspectionOrderDetailId", objtblInspectionOrderDetails.InspectionorderdetailsID);
                    //    cmd.Parameters.AddWithValue("RequesterID", RequesterID);
                    //    cmd.Parameters.AddWithValue("InspectorID", "InspectorID");//assignedInspectorId);

                    //    cmd.Parameters.AddWithValue("ScheduledDate", Convert.ToDateTime(item.ScheduledDate));
                    //    cmd.Parameters.AddWithValue("AssignedByAdmin", false);
                    //    cmd.Parameters.AddWithValue("InspectionStatus", "Pending");

                    //    cmd.Parameters.AddWithValue("IsAcceptedOrRejected", false);
                    //    cmd.Parameters.AddWithValue("IsRejectedByInspector", false);
                    //    cmd.Parameters.AddWithValue("IsUpdatable", false);

                    //    cmd.Parameters.AddWithValue("ReportNo", "ReportNo");// UniqueId);
                    //    cmd.Parameters.AddWithValue("CreatedBy", RequesterID);
                    //    cmd.Parameters.AddWithValue("Createddate", System.DateTime.Now);

                    //    cmd.Parameters.AddWithValue("IsDeleted", false);
                    //    cmd.Parameters.AddWithValue("IsActive", true);

                    //    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    //    int val = cmd.ExecuteNonQuery();
                    //    conn.Close();

                    //   
                    //}

                    //MySqlCommand cmd1 = new MySqlCommand("Select * from tblinspectionorder1",conn); 

                    //MySqlDataAdapter da1 = new MySqlDataAdapter(cmd1);
                    //cmd1.CommandType = CommandType.Text;
                    //DataSet ds = new DataSet();
                    //da1.Fill(ds); 
                    #endregion

                    objtblInspectionOrder.InspectionOrderID = Guid.NewGuid().ToString();
                    objtblInspectionOrder.InspectionOrderDetailId = item.InspectionOrderID;
                    objtblInspectionOrder.RequesterID = RequesterID;
                    objtblInspectionOrder.InspectorID = assignedInspectorId;
                    objtblInspectionOrder.ScheduledDate = Convert.ToDateTime(item.ScheduledDate);
                    objtblInspectionOrder.AssignedByAdmin = false;
                    objtblInspectionOrder.InspectionStatus = "Pending";
                    objtblInspectionOrder.IsAcceptedOrRejected = false;
                    objtblInspectionOrder.IsRejectedByInspector = false;
                    objtblInspectionOrder.IsUpdatable = false;
                    objtblInspectionOrder.ReportNo = UniqueId;
                    objtblInspectionOrder.CreatedBy = RequesterID;
                    objtblInspectionOrder.Createddate = System.DateTime.Now;
                    objtblInspectionOrder.IsDeleted = false;
                    objtblInspectionOrder.IsActive = true;
                    DatabaseContext.dbContext.tblinspectionorders.Add(objtblInspectionOrder);

                    if (assignedInspectorId == "")
                    {
                        notificationdetail objNotification = new notificationdetail();
                        objNotification.NotifcationFrom = RequesterID;
                        objNotification.NotificationTo = DatabaseContext.dbContext.userinfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                        objNotification.NotificationTypeID = 2;
                        objNotification.IsRead = false;
                        objNotification.CreatedDate = System.DateTime.Now;
                        objNotification.RequestID = objtblInspectionOrderDetails.InspectionorderdetailsID;
                        DatabaseContext.dbContext.notificationdetails.Add(objNotification);
                    }

                    int status = DatabaseContext.dbContext.SaveChanges();
                    #endregion

                    ts.Complete();
                    ts.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /*Cancel Request from IPAD*/
        public ResultSet CancelRequest(string InspectionOrderID)
        {
            result = new ResultSet();

            try
            {
                notificationdetail objNotification = new notificationdetail();

                if (DatabaseContext.dbContext.tblinspectionorders.Any(x => x.InspectionOrderDetailId == InspectionOrderID))
                {

                    string RequesterId = DatabaseContext.dbContext.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionOrderID).FirstOrDefault().RequesterID;

                    objNotification.NotifcationFrom = RequesterId;
                    objNotification.NotificationTo = DatabaseContext.dbContext.userinfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                    objNotification.NotificationTypeID = 2;
                    objNotification.IsRead = false;
                    objNotification.CreatedDate = System.DateTime.Now;
                    objNotification.RequestID = InspectionOrderID;
                    DatabaseContext.dbContext.notificationdetails.Add(objNotification);
                    DatabaseContext.dbContext.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionOrderID).FirstOrDefault().IsRejectedByInspector = true;
                    DatabaseContext.dbContext.tblinspectionorders.Where(x => x.InspectionOrderDetailId == InspectionOrderID).FirstOrDefault().IsAcceptedOrRejected = false;
                    DatabaseContext.dbContext.SaveChanges();
                    result.ErrorMessage = null;
                    result.SuccessFlag = true;
                }
                else
                {
                    result.ErrorMessage = null;
                    result.SuccessFlag = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException != null)
                        {
                            if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                            {
                                result.SuccessFlag = true;
                                result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                            }
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }

                return result;
            }
        }

        //private void SaveInspectionRequest(DATA.ResultPacket.InspectionOrderDetailPoco item, string RequesterID)
        //{


        //    InsectionOrderDetail insectionOrderDetail = new SpectorMaxDAL.InsectionOrderDetail();
        //    insectionOrderDetail.MainDate1 = Convert.ToDateTime(item.MainDate1);
        //    insectionOrderDetail.MainDate2 = Convert.ToDateTime(item.MainDate2);
        //    insectionOrderDetail.ScheduleInspectionDate = Convert.ToDateTime(item.ScheduleInspectionDate);
        //    // Inserting Property's Owner information
        //    insectionOrderDetail.InspectionOrderId = item.InspectionOrderId;
        //    insectionOrderDetail.OwnerFirstName = item.OwnerFirstName;
        //    insectionOrderDetail.OwnerLastName = item.OwnerLastName;
        //    insectionOrderDetail.StreetAddess = item.StreetAddess;
        //    insectionOrderDetail.City = item.City;
        //    insectionOrderDetail.State = item.State;
        //    insectionOrderDetail.Zip = item.Zip;
        //    insectionOrderDetail.EmailAddress = item.EmailAddress;
        //    insectionOrderDetail.PhoneDay = item.PhoneDay;
        //    insectionOrderDetail.PhoneDay1 = item.PhoneDay1;
        //    insectionOrderDetail.PhoneEve = item.PhoneEve;
        //    insectionOrderDetail.PhoneEve1 = item.PhoneEve1;
        //    insectionOrderDetail.Other = item.Other;
        //    insectionOrderDetail.Other1 = item.Other1;
        //    insectionOrderDetail.ContractClient = item.ContractClient;
        //    insectionOrderDetail.SendCopy = item.SendCopy;
        //    // Inserting Property information
        //    insectionOrderDetail.PropertyAddress = item.PropertyAddress;
        //    insectionOrderDetail.PropertyAddressCity = item.PropertyAddressCity;
        //    insectionOrderDetail.PropertyAddressCrossStreet = item.PropertyAddressCrossStreet;
        //    insectionOrderDetail.PropertyAddressState = item.PropertyAddressState;
        //    insectionOrderDetail.PropertyAddressZip = item.PropertyAddressZip;
        //    insectionOrderDetail.PropertyAddressTenantName = item.PropertyAddressTenantName;
        //    insectionOrderDetail.PropertyAddressSubDivison = item.PropertyAddressSubDivison;
        //    insectionOrderDetail.PropertyAddressMapCoord = item.PropertyAddressMapCoord;
        //    insectionOrderDetail.PropertyAddressPhoneDay = item.PropertyAddressPhoneDay;
        //    insectionOrderDetail.PropertyAddressPhoneDay1 = item.PropertyAddressPhoneDay1;
        //    insectionOrderDetail.PropertyAddressPhoneEve = item.PropertyAddressPhoneEve;
        //    insectionOrderDetail.PropertyAddressPhoneEve1 = item.PropertyAddressPhoneEve1;
        //    insectionOrderDetail.PropertyAddressDirections = item.PropertyAddressDirections;
        //    insectionOrderDetail.PropertyDescription = item.PropertyDescription;
        //    insectionOrderDetail.PropertyUnit = item.PropertyUnit;
        //    insectionOrderDetail.PropertySquareFootage = item.PropertySquareFootage;
        //    insectionOrderDetail.PropertyAddition = item.PropertyAddition;
        //    insectionOrderDetail.PropertyBedRoom = item.PropertyBedRoom;
        //    insectionOrderDetail.PropertyBaths = item.PropertyBaths;
        //    insectionOrderDetail.PropertyEstimatedMonthlyUtilites = item.PropertyEstimatedMonthlyUtilites;
        //    insectionOrderDetail.PropertyHomeAge = item.PropertyHomeAge;
        //    insectionOrderDetail.PropertyRoofAge = item.PropertyRoofAge;
        //    insectionOrderDetail.SpecialInstructions = item.SpecialInstructions + item.SpecialInstructions1;
        //    insectionOrderDetail.ConfirmationPriorWithBuyer = item.ConfirmationPriorWithBuyer;
        //    insectionOrderDetail.ConfirmationPriorWithBuyersAgent = item.ConfirmationPriorWithBuyersAgent;
        //    insectionOrderDetail.ConfirmationPriorWithSellersAgent = item.ConfirmationPriorWithSellersAgent;
        //    // Inserting Requested by information
        //    insectionOrderDetail.RequestedBy = item.RequestedBy;
        //    insectionOrderDetail.RequestedByEmailPhoneDay = item.RequestedByEmailPhoneDay;
        //    insectionOrderDetail.RequestedByEmailPhoneDay1 = item.RequestedByEmailPhoneDay1;
        //    insectionOrderDetail.RequestedByEmailPhoneEve = item.RequestedByEmailPhoneEve;
        //    insectionOrderDetail.RequestedByEmailPhoneEve1 = item.RequestedByEmailPhoneEve1;
        //    insectionOrderDetail.RequestedByEmailPhoneOther = item.RequestedByEmailPhoneOther;
        //    insectionOrderDetail.RequestedByEmailPhoneOther1 = item.RequestedByEmailPhoneOther1;
        //    insectionOrderDetail.RequestedByStreetAddress = item.RequestedByStreetAddress;
        //    insectionOrderDetail.RequestedByCity = item.RequestedByCity;
        //    insectionOrderDetail.RequestedByState = item.RequestedByState;
        //    insectionOrderDetail.RequestedByZip = item.RequestedByZip;
        //    insectionOrderDetail.RequestedByEmailAddress1 = item.RequestedByEmailAddress1;
        //    insectionOrderDetail.IsRequestedByEmailOther = item.IsRequestedByEmailOther;
        //    insectionOrderDetail.SpecialInstructions = item.SpecialInstructions + item.SpecialInstructions1;
        //    insectionOrderDetail.RequestedByEmailSend = item.RequestedByEmailSend;
        //    insectionOrderDetail.RequestedByEmailOther = item.RequestedByEmailOther;
        //    insectionOrderDetail.RequestedByEmailOwner = item.RequestedByEmailOwner;
        //    insectionOrderDetail.RequestedByEmailOwnersAgent = item.RequestedByEmailOwnersAgent;
        //    insectionOrderDetail.RequestedByEmailContractClient = item.RequestedByEmailContractClient;

        //    insectionOrderDetail.MainClient1 = item.MainClient1;
        //    insectionOrderDetail.MainClient2 = item.MainClient2;
        //    insectionOrderDetail.MainDate1 = Convert.ToDateTime(item.MainDate1);
        //    insectionOrderDetail.MainDate2 = Convert.ToDateTime(item.MainDate2);
        //    insectionOrderDetail.ScheduleInspectionDate = Convert.ToDateTime(item.ScheduleInspectionDate);

        //    insectionOrderDetail.IsAdditionalTest1 = item.IsAdditionalTest1;
        //    insectionOrderDetail.IsAdditionalTest2 = item.IsAdditionalTest2;
        //    insectionOrderDetail.IsAdditionalTest3 = item.IsAdditionalTest3;
        //    insectionOrderDetail.IsAdditionalTest4 = item.IsAdditionalTest4;
        //    insectionOrderDetail.IsAdditionalTest5 = item.IsAdditionalTest5;
        //    insectionOrderDetail.IsAdditionalTest6 = item.IsAdditionalTest6;
        //    insectionOrderDetail.IsAdditionalTest7 = item.IsAdditionalTest7;
        //    insectionOrderDetail.IsAdditionalTest8 = item.IsAdditionalTest8;
        //    insectionOrderDetail.TotalAdditionalTestingAmount = item.TotalAdditionalTestingAmount;

        //    insectionOrderDetail.ModifiedDate = DateTime.Now;
        //    insectionOrderDetail.CreatedDate = DateTime.Now;

        //    /*Insert into table inspectionOrder*/
        //    InspectionOrder objInspectionOrder = new InspectionOrder();
        //    objInspectionOrder.InspectionId = Guid.NewGuid().ToString();
        //    objInspectionOrder.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
        //    objInspectionOrder.InspectionStatus = "Pending";

        //    //      objInspectionOrder.InspectorID = DatabaseContext.dbContext.UserInfoes.Where(x => x.EmailAddress == item.InspectorEmailID).FirstOrDefault().ID;
        //    objInspectionOrder.CreatedDate = DateTime.Now;
        //    objInspectionOrder.AssignedByAdmin = false;
        //    string assignedInspectorId = DatabaseContext.dbContext.ZipCodeAssigneds.FirstOrDefault(x => x.ZipCode == item.PropertyAddressZip) != null ? DatabaseContext.dbContext.ZipCodeAssigneds.FirstOrDefault(x => x.ZipCode == item.PropertyAddressZip).InspectorID : "";
        //    //var countReports = "";
        //    int TotalInspectorReportCount = 0;
        //    string UniqueId = "Empty";
        //    if (assignedInspectorId != "")
        //    {
        //        var countReports = (from IO in DatabaseContext.dbContext.InspectionOrders
        //                            join IOD in DatabaseContext.dbContext.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
        //                            where IO.InspectorID == assignedInspectorId && IOD.PropertyAddressState == item.PropertyAddressState
        //                            select IO.InspectorID);
        //        TotalInspectorReportCount = countReports.Count();
        //        UniqueId = item.PropertyAddressState + DatabaseContext.dbContext.InspectorDetails.Where(x => x.UserID == assignedInspectorId).Select(y => y.InspectorNo).FirstOrDefault() + (TotalInspectorReportCount + 1).ToString();
        //    }
        //    if (assignedInspectorId == "")
        //    {
        //        NotificationDetail objNotification = new NotificationDetail();
        //        objNotification.NotifcationFrom = RequesterID;
        //        objNotification.NotificationTo = DatabaseContext.dbContext.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
        //        objNotification.NotificationTypeID = 2;
        //        objNotification.IsRead = false;
        //        objNotification.CreatedDate = System.DateTime.Now;
        //        objNotification.RequestID = insectionOrderDetail.InspectionOrderId;
        //        DatabaseContext.dbContext.NotificationDetails.Add(objNotification);

        //    }
        //    objInspectionOrder.InspectorID = assignedInspectorId;
        //    if (assignedInspectorId != "")
        //    {
        //        objInspectionOrder.IsAcceptedOrRejected = true;
        //    }
        //    else
        //    {
        //        objInspectionOrder.IsAcceptedOrRejected = false;
        //    }
        //    objInspectionOrder.UniqueID = UniqueId;
        //    objInspectionOrder.Comment = "";
        //    objInspectionOrder.RequesterID = RequesterID;
        //    objInspectionOrder.ScheduleDate = Convert.ToDateTime(item.ScheduleInspectionDate);
        //    objInspectionOrder.ModifiedDate = DateTime.Now;

        //    /*Save Inspection Inspection Request*/
        //    DatabaseContext.dbContext.InspectionOrders.Add(objInspectionOrder);
        //    //DatabaseContext.dbContext.SaveChanges();
        //    /*Save Inspection Inspection Request Details*/
        //    DatabaseContext.dbContext.InsectionOrderDetails.Add(insectionOrderDetail);
        //    //DatabaseContext.dbContext.SaveChanges();
        //    /*Inserting buyer's information*/
        //    AgentLibrary AgentLibraryinfo;
        //    AgentLibrary AgentLibraryinfo1;
        //    if (item.buyerAgent != null)
        //    {
        //        AgentLibraryinfo = new SpectorMaxDAL.AgentLibrary();
        //        AgentLibraryinfo.AgentID = item.buyerAgent.AgentID;
        //        AgentLibraryinfo.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
        //        AgentLibraryinfo.AgentType = "2";
        //        AgentLibraryinfo.AgentFirstName = item.buyerAgent.BuyerAgentFirstName;
        //        AgentLibraryinfo.AgentLastName = item.buyerAgent.BuyerAgentLastName;
        //        AgentLibraryinfo.AgentOffice = item.buyerAgent.BuyerAgentOffice;
        //        AgentLibraryinfo.AgentPhoneDay = item.buyerAgent.BuyerAgentPhoneDay;
        //        AgentLibraryinfo.AgentPhoneDay1 = item.buyerAgent.BuyerAgentPhoneDay1;
        //        AgentLibraryinfo.AgentPhoneEve = item.buyerAgent.BuyerAgentPhoneEve;
        //        AgentLibraryinfo.AgentPhoneEve1 = item.buyerAgent.BuyerAgentPhoneEve1;
        //        AgentLibraryinfo.AgentPhoneOther = item.buyerAgent.BuyerAgentPhoneOther;
        //        AgentLibraryinfo.AgentPhoneOther1 = item.buyerAgent.BuyerAgentPhoneOther1;
        //        AgentLibraryinfo.AgentStreetAddress = item.buyerAgent.BuyerAgentStreetAddress;
        //        //  AgentLibraryinfo.AgentCity = item.buyerAgent.BuyerAgentCity;
        //        //   AgentLibraryinfo.AgentState = item.buyerAgent.BuyerAgentState;
        //        AgentLibraryinfo.AgentZip = item.buyerAgent.BuyerAgentZip;
        //        AgentLibraryinfo.AgentSend = item.buyerAgent.AgentBuyerSend;
        //        AgentLibraryinfo.AgentEmailAddress = item.buyerAgent.BuyerAgentEmailAddress;
        //        AgentLibraryinfo.AgentEmailAddress = item.buyerAgent.BuyerAgentEmailAddress1;
        //        DatabaseContext.dbContext.AgentLibraries.Add(AgentLibraryinfo);
        //        DatabaseContext.dbContext.SaveChanges();
        //    }

        //    // Inserting sellers's information
        //    if (item.sellerAgent != null)
        //    {
        //        AgentLibraryinfo1 = new SpectorMaxDAL.AgentLibrary();
        //        AgentLibraryinfo1.AgentID = item.sellerAgent.AgentID;
        //        AgentLibraryinfo1.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
        //        AgentLibraryinfo1.AgentType = "1";
        //        AgentLibraryinfo1.AgentFirstName = item.sellerAgent.SellerAgentFirstName;
        //        AgentLibraryinfo1.AgentLastName = item.sellerAgent.SellerAgentLastName;
        //        AgentLibraryinfo1.AgentOffice = item.sellerAgent.SellerAgentOffice;
        //        AgentLibraryinfo1.AgentPhoneDay = item.sellerAgent.SellerAgentPhoneDay;
        //        AgentLibraryinfo1.AgentPhoneDay1 = item.sellerAgent.SellerAgentPhoneDay1;
        //        AgentLibraryinfo1.AgentPhoneEve = item.sellerAgent.SellerAgentPhoneEve;
        //        AgentLibraryinfo1.AgentPhoneEve1 = item.sellerAgent.SellerAgentPhoneEve1;
        //        AgentLibraryinfo1.AgentPhoneOther = item.sellerAgent.SellerAgentPhoneOther;
        //        AgentLibraryinfo1.AgentPhoneOther1 = item.sellerAgent.SellerAgentPhoneOther1;
        //        AgentLibraryinfo1.AgentStreetAddress = item.sellerAgent.SellerAgentStreetAddress;
        //        //    AgentLibraryinfo1.AgentCity = item.sellerAgent.SellerAgentCity;
        //        //  AgentLibraryinfo1.AgentState = item.sellerAgent.SellerAgentState;
        //        AgentLibraryinfo1.AgentZip = item.sellerAgent.SellerAgentZip;
        //        AgentLibraryinfo1.AgentEmailAddress = item.sellerAgent.SellerAgentEmailAddress;
        //        AgentLibraryinfo1.AgentEmailAddress = item.sellerAgent.SellerAgentEmailAddress1;
        //        AgentLibraryinfo1.AgentSend = item.sellerAgent.AgentSellerSend;
        //        DatabaseContext.dbContext.AgentLibraries.Add(AgentLibraryinfo1);
        //        DatabaseContext.dbContext.SaveChanges();
        //    }
        //}

        private void RegisterUserIfNoteExist(string RequsterEmailId, string RequesterID, string UserRole)
        {
            userinfo objUserInfo = new userinfo();
            objUserInfo.ID = RequesterID;
            objUserInfo.EmailAddress = RequsterEmailId;
            objUserInfo.Password = new Random().Next(100, 1000000).ToString();
            objUserInfo.Role = "7D9A1A9C-5305-455F-A07C-0BF3080A6753";//"9C15FC8A-CDD3-49CD-9";
            objUserInfo.IsActive = true;
            objUserInfo.IsDeleted = false;
            objUserInfo.IsEmailVerified = true;
            objUserInfo.SecurityQuestion = "1";
            objUserInfo.SecurityAnswer = "Sample";
            objUserInfo.CreatedDate = DateTime.Now;
            objUserInfo.ModifiedDate = DateTime.Now;
            objUserInfo.IsProfileUpdated = false;
            DatabaseContext.dbContext.userinfoes.Add(objUserInfo);
            DatabaseContext.dbContext.SaveChanges();

            /*Send Invitation Mail*/
            EmailHelper email = new EmailHelper();
            string emailBody = DatabaseContext.dbContext.emailtemplates.FirstOrDefault(x => x.TemplateId == 39).TemplateDiscription;
            emailBody = emailBody.Replace("[EmailAddress]", RequsterEmailId);
            emailBody = emailBody.Replace("[Password]", objUserInfo.Password);
            bool status = email.SendEmail("", "Thank you for registerting with Spectormax", "", emailBody, RequsterEmailId);
        }
        public string ServiceTest(string Message)
        {
            return Message;
        }



        public ResultSet AddToPhotoLibrary(List<CustomPhotoLibrary> objPhotoLibrary)
        {
            try
            {
                var db_con = new db_spectromaxEntities();

                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    result = new ResultSet();

                    foreach (var item in objPhotoLibrary)
                    {
                        string PhotoName = Guid.NewGuid().ToString();
                        /*Converting bytes to image and saving image to folder*/
                        using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(item.imagearr.Split(',')[1])))
                        {
                            using (Bitmap bm2 = new Bitmap(ms))
                            {
                                bm2.Save(System.Configuration.ConfigurationManager.AppSettings["ImageLocation"] + "/" + PhotoName + ".jpg");
                            }
                        }

                        photolibrary objDBPhotoLibrary = new photolibrary();
                        objDBPhotoLibrary.ID = item.ID;
                        objDBPhotoLibrary.InspectionOrderID = item.InspectionOrderID;
                        objDBPhotoLibrary.SectionID = item.SectionID;
                        objDBPhotoLibrary.SubSectionId = item.SubSectionId;
                        objDBPhotoLibrary.Notes = item.Notes;
                        objDBPhotoLibrary.Comments = null;
                        objDBPhotoLibrary.CommentedBy = null;
                        objDBPhotoLibrary.CreatedDate = DateTime.Now;
                        objDBPhotoLibrary.ModifiedDate = DateTime.Now;
                        objDBPhotoLibrary.PhotoUrl = PhotoName + ".jpg";//item.imagearr;
                        objDBPhotoLibrary.AcceptedOrRejected = false;
                        objDBPhotoLibrary.IsRead = false;
                        if (item.DirectionDropID != null && item.DirectionDropID != "")
                            objDBPhotoLibrary.DirectionDrop = int.Parse(item.DirectionDropID);
                        else
                            objDBPhotoLibrary.DirectionDrop = null;
                        objDBPhotoLibrary.InpectionReportFID = item.InpectionReportFID;
                        db_con.photolibraries.Add(objDBPhotoLibrary);
                    }
                    db_con.SaveChanges();
                    result.SuccessFlag = true;
                    scope.Complete();
                }
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                result.SuccessFlag = false;
                result.ErrorMessage = sb.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                {
                    if (ex.Message.ToString().ToLower().Contains("underlying"))
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = "Can not connect to database";
                    }
                    else
                    {
                        result.SuccessFlag = false;
                        result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                        if (ex.InnerException != null)
                        {
                            if (ex.InnerException.InnerException.Message.Contains("Duplicate"))
                            {
                                result.SuccessFlag = true;
                                result.ErrorMessage = "Record exist for key = " + ex.InnerException.InnerException.Message.Split('\'')[1];
                            }
                        }
                    }
                }
                else
                {
                    result.SuccessFlag = false;
                    result.ErrorMessage = ex.Message + (ex.InnerException == null ? "" : ex.InnerException.InnerException.Message);
                }
            }
            return result;
        }

        public Stream CommentLibrary(string InspectorEmailId, List<CommentLibrary> objCommentLibrary)
        {
            try
            {
                //List<CommentLibrary> objComment_Library = new List<CommentLibrary>();
                string InspectorId = DatabaseContext.dbContext.userinfoes.FirstOrDefault(x => x.EmailAddress == InspectorEmailId).ID;

                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    /*Operation on Comment Library*/
                    foreach (var item in objCommentLibrary)
                    {
                        if (item.status != null)
                        {
                            int operation = Convert.ToInt32(item.status);
                            if (operation == 0)
                            {

                                string cmmtid = DatabaseContext.dbContext.commentboxes.FirstOrDefault(x => x.SectionId == item.SectionId && x.SubSectionId == item.SubSectionId && x.CommentBoxName == item.CommentBoxName).Id;

                                admincommentlibrary objadmincommentlibrary = new admincommentlibrary();
                                objadmincommentlibrary.Id = Guid.NewGuid().ToString();
                                objadmincommentlibrary.CommentBoxID = cmmtid;//item.CommentBoxID;
                                objadmincommentlibrary.CommentBoxName = item.CommentBoxName;
                                objadmincommentlibrary.CommentText = item.CommentText;
                                objadmincommentlibrary.CreatedDate = DateTime.Now;
                                objadmincommentlibrary.ModifiendDate = DateTime.Now;
                                objadmincommentlibrary.SectionId = item.SectionId;
                                objadmincommentlibrary.SubSectionId = item.SubSectionId;
                                objadmincommentlibrary.IsAdminComment = false;
                                objadmincommentlibrary.IsDeleted = false;
                                objadmincommentlibrary.UserId = InspectorId;
                                DatabaseContext.dbContext.admincommentlibraries.Add(objadmincommentlibrary);
                            }
                            else if (operation == 1)
                            {
                                //string cmmtid = DatabaseContext.dbContext.commentboxes.FirstOrDefault(x => x.SectionId == item.SectionId && x.SubSectionId == item.SubSectionId && x.CommentBoxName == item.CommentBoxName).Id;
                                DatabaseContext.dbContext.admincommentlibraries.FirstOrDefault(x => x.Id == item.Id).CommentText = item.CommentText;
                            }
                            else if (operation == 2)
                            {
                                //string cmmtid = DatabaseContext.dbContext.commentboxes.FirstOrDefault(x => x.SectionId == item.SectionId && x.SubSectionId == item.SubSectionId && x.CommentBoxName == item.CommentBoxName).Id;
                                DatabaseContext.dbContext.admincommentlibraries.FirstOrDefault(x => x.Id == item.Id).IsDeleted = true;
                            }
                        }
                    }
                    DatabaseContext.dbContext.SaveChanges();
                    ts.Complete();
                    ts.Dispose();
                }
                ///*Get Comment from DB*/
                //objComment_Library = (from k in DatabaseContext.dbContext.admincommentlibraries
                //                      where (k.UserId == InspectorId || k.UserId == "EFCFD2D4-157C-49A5-8985-19B5E03635DC") && k.IsDeleted == false
                //                      select new CommentLibrary
                //                      {
                //                          CommentText = k.CommentText,
                //                          CommentBoxName = k.CommentBoxName,
                //                          Id = k.Id,
                //                          SectionId = k.SectionId,
                //                          SubSectionId = k.SubSectionId,
                //                      }).ToList();

                //return objComment_Library;


                List<Section> objSection = new List<Section>();
                List<SubSectionList> objSubSectionList = new List<SubSectionList>();
                List<CommentLibrary> objComment_Library = new List<CommentLibrary>();


                int SectionCount = 0;
                //JsonObject 

                //var test = DatabaseContext.dbContext.AdminCommentLibraries.Where(x => x.ModifiendDate >= CreatedDate && x.IsDeleted == false).Distinct().ToList().Select(y => y.SectionId).ToList();
                StringBuilder JsonString = new StringBuilder();
                JsonString.Append("{");
                objSection = (from k in DatabaseContext.dbContext.admincommentlibraries
                              //where s.ModifiendDate >= CreatedDate && s.IsDeleted == false
                              /*"EFCFD2D4-157C-49A5-8985-19B5E03635DC" is admin ID. Using this id service will fetch the admin comment also*/
                              where (k.UserId == InspectorId || k.UserId == "EFCFD2D4-157C-49A5-8985-19B5E03635DC") && k.IsDeleted == false
                              select new Section
                              {
                                  SectionID = k.SectionId
                              }).Distinct().ToList();

                foreach (var item in objSection)
                {
                    SectionCount++;
                    int SubSectionCount = 0;
                    JsonString.Append("\"" + item.SectionID + "\":{");

                    objSubSectionList = (from ss in DatabaseContext.dbContext.admincommentlibraries
                                         where ss.SectionId == item.SectionID
                                         && ss.IsDeleted == false
                                         select new SubSectionList
                                         {
                                             SubSectionID = ss.SubSectionId
                                         }).Distinct().ToList();
                    item.SubSection = objSubSectionList;
                    foreach (var itemsub in objSubSectionList)
                    {
                        SubSectionCount++;
                        JsonString.Append("\"" + itemsub.SubSectionID + "\":[");
                        objCommentLibrary = (from acl in DatabaseContext.dbContext.admincommentlibraries
                                             where acl.SubSectionId == itemsub.SubSectionID
                                             && acl.IsDeleted == false
                                             select new CommentLibrary
                                             {
                                                 Id = acl.Id,
                                                 SectionId = acl.SectionId,
                                                 SubSectionId = acl.SubSectionId,
                                                 CommentBoxID = acl.CommentBoxID,
                                                 CommentBoxName = acl.CommentBoxName,
                                                 CommentText = acl.CommentText,
                                                 CreatedDate = acl.CreatedDate,
                                                 ModifiedDate = acl.ModifiendDate,
                                                 //IsDeleted = acl.IsDeleted
                                             }).ToList();
                        itemsub.CommentLibrary = objCommentLibrary;
                        int Commentcount = 0;

                        foreach (var itemLib in objCommentLibrary)
                        {
                            Commentcount++;
                            JsonString.Append("{");
                            JsonString.Append("\"CommentBoxID\"" + ":\"" + itemLib.Id + "\",");
                            JsonString.Append("\"CommentBoxName\"" + ":\"" + itemLib.CommentBoxName + "\",");
                            itemLib.CommentText = Regex.Replace(itemLib.CommentText, @"\s+", " ");
                            JsonString.Append("\"CommentText\"" + ":\"" + itemLib.CommentText.Trim() + "\"");
                            if (objCommentLibrary.Count == Commentcount)
                            {
                                JsonString.Append("}");
                            }
                            else
                            {
                                JsonString.Append("},");
                            }
                        }
                        if (SubSectionCount == objSubSectionList.Count)
                            JsonString.Append("]");
                        else
                            JsonString.Append("],");


                    }

                    //   JsonString.Append("}");
                    //if (SectionCount == objSection.Count)
                    //{
                    //    JsonString.Append("},");
                    //    JsonString.Append("\"ServerTime\":{");
                    //    JsonString.Append("\"ServerTime\"" + ":\"" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "\"}");
                    //}
                    //else
                    JsonString.Append("},");

                    //  JsonString.Append("}");

                }
                JsonString.Append("}");


                return new MemoryStream(Encoding.UTF8.GetBytes(JsonString.ToString()));

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //static void ExecuteSql(string[] _lst)
        //{
        //    using (var context = new db_spectromaxEntities())
        //    {
        //        string sql = "select * from Chapter3.Student where Degree = @Major";
        //        var args = new DbParameter[] { new SqlParameter { ParameterName = "Major", Value = "Masters" } };
        //        var students = context.ExecuteStoreQuery<Student>(sql, args);
        //    }
        //}
        #endregion
    }
}
