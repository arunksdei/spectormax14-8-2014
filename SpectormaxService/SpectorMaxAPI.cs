﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using SpectorMaxDAL;
using SpectormaxService.DATA.ResultPacket;
using System.IO;
using SpectormaxService.DATA;

namespace SpectormaxService
{
    [ServiceContract]
    
    public interface ISpectormaxServiceAPI
    {
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddAgentLibrary(List<agentlibrary> AgentLibrary);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddAppliancescommonsections(List<appliancescommonsection> Appliancescommonsection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddAppliancesDishwashDisposalRangeSection(List<appliancesdishwashdisposalrange_section> AppliancesDishwashDisposalRange_section);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddAppliancesDryerDoorBellSectiion(List<appliancesdryerdoorbellsectiion> AppliancesDryerDoorBellSectiion);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddAppliancesHoodVentOtherSection(List<applianceshoodventothersection> AppliancesHoodVentOtherSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddAppliancesOvenSection(List<appliancesovensection> AppliancesOvenSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddElectrialCommanSection(List<electrialcommansection> ElectrialCommanSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddElectricalPanel(List<electricalpanel> ElectricalPanel);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddElectricalService(List<electricalservice> ElectricalService);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddElectricalRoom(List<electricalroom> ElectricalRoom);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddGroundCommonSection(List<groundcommonsection> GroundCommonSection);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddGroundGassection(List<groundgassection> GroundGassection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddGroundH2OSection(List<groundh2osection> GroundH2OSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddGroundSewerSection(List<groundsewersection> GroundSewerSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddGroundSprinkSysSection(List<groundsprinksyssection> GroundSprinkSysSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddHVACCommonSection(List<hvaccommonsection> HVACCommonSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddHVACCooling(List<HVACCoolingService> HVACCooling);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddHVACDuctsVents(List<hvacductsvent> HVACDuctsVent);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddHVACEquipBoileRadiantSection(List<HVACEquipBoileRadiantSectionService> HVACEquipBoileRadiantSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddOptionalCommonSections(List<optionalcommonsection> OptionalCommonSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddOptionalTestingSections(List<optionaltestingsection> OptionalTestingSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddPlumbingCommonSection(List<plumbingcommonsection> PlumbingCommonSection);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddPlumbingMainlineKitchenOthers_Sections(List<plumbingmainlinekitchenothers_section> PlumbingMainlineKitchenOthers_Section);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddPlumbingWaterheat(List<PlumbingWaterheatService> PlumbingWaterheat);
        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddStructuralCommonSections(List<structuralcommonsection> StructuralCommonSection);
        
        //[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ResultSet AddInsectionOrderDetail(List<InsectionOrderDetail> InsectionOrderDetail);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddInspectionAdditionalInfo(List<inspectionadditionalinfo> InspectionAdditionalInfo);

        //[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ResultSet AddInspectionRequest(List<InspectionOrderDetailPoco> AddInspectionRequest);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddInspectionRequestNew(List<InspectionOrderDetailPocoNew> AddInspectionRequest);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddToOptionalEnergy(List<optionalenergy> OptionalEnergy);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet AddToPhotoLibrary(List<CustomPhotoLibrary> PhotoLibrary);


        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet UpdateInspectionStatus(List<ServiceStatus> objServiceStatus);

        [WebInvoke(Method = "POST", UriTemplate = "CommentLibrary?InspectorEmailId={InspectorEmailId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Stream CommentLibrary(string InspectorEmailId, List<CommentLibrary> objCommentLibrary);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultSet Payment(PaymentData objPaymentData);
        
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string ServiceTest(string Message);
        /// <summary>
        /// date format should be mm-dd-yyyy
        /// </summary>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        [WebGet(UriTemplate = "GetInsectionOrderDetails?fromdate={fromDate}&emailaddress={emailaddress}", ResponseFormat = WebMessageFormat.Json)]
        List<InspectionResultData> GetInsectionOrderDetails(string fromDate,string emailaddress);

        [WebGet(UriTemplate = "GetInsectionOrderDetailsNew?fromdate={fromDate}&emailaddress={emailaddress}", ResponseFormat = WebMessageFormat.Json)]
        List<InspectionResultDataNew> GetInsectionOrderDetailsNew(string fromDate, string emailaddress);

        [WebGet(UriTemplate = "GetAdminCommentLibrary?CreatedDate={CreatedDate}&inspectorId={inspectorId}", ResponseFormat = WebMessageFormat.Json)]
        Stream GetAdminCommentLibrary(string CreatedDate, string inspectorId);

        [WebGet(UriTemplate = "GetAdditionalTestingPrice", ResponseFormat = WebMessageFormat.Json)]
        List<emailtemplate> GetAdditionalTestingPrice();

        /// <summary>
        /// Check Inspector Login 
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [WebGet(UriTemplate = "GetInspectorStatus?username={username}&password={password}", ResponseFormat = WebMessageFormat.Json)]
        InspectorStatus  GetInspectorStatus(string username,string password);

        [WebGet(UriTemplate = "ForgotPassword?emailaddress={emailaddress}", ResponseFormat = WebMessageFormat.Json)]
        ResultSet ForgotPassword(string emailAddress);

        [WebGet(UriTemplate = "CheckUserEmailExists?emailaddress={emailaddress}", ResponseFormat = WebMessageFormat.Json)]
        ResultSet CheckUserEmailExists(string emailAddress);

        [WebGet(UriTemplate = "GetCityStateandZip", ResponseFormat = WebMessageFormat.Json)]
        List<CityStateAndZip> GetCityStateandZip();

        [WebGet(UriTemplate = "GetTest", ResponseFormat = WebMessageFormat.Json)]
        string GetTest();

        [WebGet(UriTemplate="AddInspectionRequestNew1", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<InspectionOrderDetailPocoNew> AddInspectionRequestNew1();

        [WebGet(UriTemplate = "CancelRequest?InspectionOrderID={InspectionOrderID}", ResponseFormat = WebMessageFormat.Json)]
        ResultSet CancelRequest(string InspectionOrderID);

        [WebGet(UriTemplate = "NonUpdatableReport?emailAddress={emailAddress}&InspectionID={InspectionID}", ResponseFormat = WebMessageFormat.Json)]
        ResultSet NonUpdatableReport(string emailAddress, string InspectionID);
    }
}