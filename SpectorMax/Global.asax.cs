﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SpectorMaxDAL;
using SpectorMax.Models;

namespace SpectorMax
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


          

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
                //  new { controller = "Admin", action = "Home", id = UrlParameter.Optional } // Parameter defaults
            );
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }

        void Session_Start(object sender, EventArgs e)
        {
           
        }

        void Application_Error(Object sender, EventArgs e)
        {
            try
            {
                SpectorMaxContext context = new SpectorMaxContext();
                Exception ex = Server.GetLastError().GetBaseException();
                string ErrorDetails = "MESSAGE: " + ex.Message + "\nSOURCE: " + ex.Source + "\nFORM: " + Request.Form.ToString() + "\nQUERYSTRING: " + Request.QueryString.ToString() + "\nTARGETSITE: " + ex.TargetSite + "\nSTACKTRACE: " + ex.StackTrace;

                errorlog objErrDetails = new errorlog();
                objErrDetails.ID = Guid.NewGuid().ToString();
                objErrDetails.Errordetails = ErrorDetails;
                objErrDetails.CreatedDate = DateTime.Now;
                context.ErrLog.Add(objErrDetails);
                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        void Session_End(Object sender, EventArgs E)
        {


        }

        void Application_Exit(Object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }
    }
}