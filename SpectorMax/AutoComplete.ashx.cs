﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Web.Script.Serialization;

namespace SpectorMax.App_Code
{
    /// <summary>
    /// Summary description for AutoComplete
    /// </summary>
    public class AutoComplete : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //string connStr = ConfigurationManager.ConnectionStrings["db_spectromaxEntities"].ConnectionString;.
            List<string> lstZips = new List<string>();
            string firstchar = context.Request.QueryString["data"];
            string connStr = ConfigurationManager.ConnectionStrings["SpectorMaxConnectionString"].ConnectionString;
            MySqlConnection conn = new MySqlConnection(connStr);
            string sql = "Select Zip from tblzip where Zip like '" + firstchar + "%' LIMIT 10";
            MySqlCommand cmd = new MySqlCommand("Select Zip from tblzip where Zip like'" + firstchar + "%' LIMIT 10", conn);

            conn.Open();
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                lstZips.Add(reader.GetString(0));
                //context.Response.Write(reader.GetString(0) + Environment.NewLine);
            }
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string lookedUpZips = javaScriptSerializer.Serialize(lstZips);
            context.Response.ContentType = "text/html";
            context.Response.Write(lookedUpZips);
            reader.Close();
            conn.Close();

           // return objtemp;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}