﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SpectorMax.Entities.Classes;
using SpectorMax.Models;
using SpectorMaxDAL;
using System.IO;
using System.Web;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
namespace SpectorMax.Controllers
{
    public class NotificationController : BaseController
    {

        #region Global Variables
        private SpectorMaxContext context = new SpectorMaxContext();
        private MessageToShow _messageToShow = new MessageToShow();
        public static List<TableNamesOrderId> objTableNamesOrderId = new List<TableNamesOrderId>();
        List<string> imagesNameList = new List<string>();
        string I_OrderId = string.Empty;
        #endregion

        #region Index
        //
        // GET: /Notification/

        public ActionResult Index()
        {

            List<PropertyAddress> objList_PropertyAddress = new List<PropertyAddress>();
            List<inspectionorder> ArrayOfOrderPlaced = context.InspectionOrders.Where(x => x.RequesterID == Sessions.LoggedInUser.UserId && x.IsUpdatable == true).ToList();
            List<string> Order_id = new List<string>();

            foreach (var item in ArrayOfOrderPlaced)
            {
                if (context.ElectrialCommanSection.Any(x => x.ReportID == item.InspectionOrderID && x.ConditionID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "ElectrialCommanSection", OrderID = item.InspectionOrderID });
                }
                if (context.ElectricalPanel.Any(x => x.ReportID == item.InspectionOrderID && x.ConditionID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "ElectricalPanel", OrderID = item.InspectionOrderID });
                }
                if (context.ElectricalRoom.Any(x => x.ReportID == item.InspectionOrderID && x.ConditionID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "ElectricalRoom", OrderID = item.InspectionOrderID });
                }
                if (context.ElectricalService.Any(x => x.ReportID == item.InspectionOrderID && x.ConditionID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "ElectricalService", OrderID = item.InspectionOrderID });

                }
                if (context.Appliancescommonsections.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckboxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "Appliancescommonsections", OrderID = item.InspectionOrderID });

                }

                if (context.AppliancesDishwashDisposalRange_sections.Any(x => x.InsepectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "AppliancesDishwashDisposalRange_sections", OrderID = item.InspectionOrderID });

                }
                if (context.AppliancesDryerDoorBellSectiions.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "AppliancesDryerDoorBellSectiions", OrderID = item.InspectionOrderID });

                }
                if (context.AppliancesHoodVentOtherSections.Any(x => x.InspectionOrderId == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "AppliancesHoodVentOtherSections", OrderID = item.InspectionOrderID });

                }
                if (context.AppliancesOvenSections.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckboxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "AppliancesOvenSections", OrderID = item.InspectionOrderID });

                }
                if (context.GroundCommonSections.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "GroundCommonSections", OrderID = item.InspectionOrderID });

                }
                if (context.GroundGassections.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "GroundGassections", OrderID = item.InspectionOrderID });

                }
                if (context.GroundH2OSections.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "GroundH2OSections", OrderID = item.InspectionOrderID });

                }
                if (context.GroundSewerSections.Any(x => x.C_InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "GroundSewerSections", OrderID = item.InspectionOrderID });
                }
                if (context.GroundSprinkSysSections.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "GroundSprinkSysSections", OrderID = item.InspectionOrderID });
                }
                if (context.HVACCommonSections.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "HVACCommonSections", OrderID = item.InspectionOrderID });
                }
                if (context.HVACCoolings.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "HVACCoolings", OrderID = item.InspectionOrderID });
                }
                if (context.HVACDuctsVents.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "HVACDuctsVents", OrderID = item.InspectionOrderID });
                }
                if (context.HVACEquipBoileRadiantSections.Any(x => x.InspectionOrderId == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "HVACEquipBoileRadiantSections", OrderID = item.InspectionOrderID });
                }
                if (context.OptionalCommonSections.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "OptionalCommonSections", OrderID = item.InspectionOrderID });
                }
                if (context.PlumbingMainlineKitchenOthers_Sections.Any(x => x.InspectionOrderId == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "PlumbingMainlineKitchenOthers_Sections", OrderID = item.InspectionOrderID });
                }
                if (context.PlumbingWaterheats.Any(x => x.InspectionOrderId == item.InspectionOrderID && x.CheckBoxID == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "PlumbingWaterheats", OrderID = item.InspectionOrderID });
                }
                if (context.StructuralCommonSections.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CheckBoxiD == "3"))
                {
                    objTableNamesOrderId.Add(new TableNamesOrderId { TableName = "StructuralCommonSections", OrderID = item.InspectionOrderID });
                }
            }

            var distinctOrderId = objTableNamesOrderId.Select(x => x.OrderID).Distinct().ToList();

            foreach (var item in distinctOrderId)
            {
                PropertyAddress objPropertyAddress = new PropertyAddress();

                var Property_Address = context.InsectionOrderDetails.Where(x => x.InspectionOrderId == item).FirstOrDefault();
                objPropertyAddress.OwnerFirstName = Property_Address.OwnerFirstName;
                objPropertyAddress.OwnerLastName = Property_Address.OwnerLastName;
                objPropertyAddress.StreetAddress = Property_Address.StreetAddess;
                objPropertyAddress.City = Property_Address.City;
                objPropertyAddress.State = Property_Address.State;
                objPropertyAddress.Zip = Property_Address.Zip;
                objPropertyAddress.OrderID = item;
                objList_PropertyAddress.Add(objPropertyAddress);
            }

            return View(objList_PropertyAddress);
        }
        #endregion

        #region GetDetails
        /// <summary>
        /// POST:// Method for Get List of all subsection(nofification) those are need to maintenance against inspectionorderid and sectionid
        /// </summary>
        /// <param name="OrderID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetDetails(string OrderID)
        {

            //ViewBag.OrderID = OrderID;
            List<PropertyAddress> objDetails = new List<PropertyAddress>();
            string result = RenderRazorViewToString("~/Views/Notification/_PartialMaintenanceDetails.cshtml", objDetails);
            return ReturnJson(result);
        }
        #endregion


        #region PartialUpdateSubSectionInfor
        /// <summary>
        /// GET: method for Enter Maintenance comments from home seller to inspector.
        /// <param name="orderId"></param>
        /// <param name="sectionId"></param>
        /// <param name="subsectionId"></param>
        /// <param name="subSectionName"></param>
        /// <param name="tableName"></param>
        /// </summary>
        /// <returns></returns>

        public ActionResult PartialUpdateSubSectionInfor(string orderId, string sectionId, string subsectionId, string tableName, string subSectionName, string InspectionReportID)
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    ViewBag.OrderId = orderId;
                    ViewBag.SectionID = sectionId;
                    ViewBag.SubSectionID = subsectionId;
                    ViewBag.TableName = tableName;
                    ViewBag.SubSectionName = subSectionName;
                    ViewBag.InspectionReportID = InspectionReportID;
                    return PartialView("_PartialUpdateSubSectionInfor");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion

        #region UpdateSubSectionInfoPost
        /// <summary>
        /// POST: // method for update information by home seller accourding to inspector comment against to inspectionorderid.
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="sectionId"></param>
        /// <param name="subsectionId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateSubSectionInfoPost(string orderId, string sectionId, string subsectionId, string tableName, string comments, string imagesUrl, string InspectionReportID, string xc)
        {

            #region Previous Code

            //try
            //{
            //    if (Sessions.LoggedInUser != null)
            //    {
            //        string[] imagesUrls = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<string[]>(imagesUrl);

            //        for (int i = 0; i < imagesUrls.Length; i++)
            //        {
            //            string imgurl = imagesUrls[i];
            //            PhotoLibrary objPhotoLibrary = new PhotoLibrary();
            //            objPhotoLibrary.ID = Guid.NewGuid().ToString();
            //            objPhotoLibrary.InspectionOrderID = orderId;
            //            objPhotoLibrary.SectionID = sectionId;
            //            objPhotoLibrary.SubSectionId = subsectionId;
            //            objPhotoLibrary.Comments = comments;
            //            objPhotoLibrary.CommentedBy = Sessions.LoggedInUser.UserId;
            //            if (imagesUrls[i] == "noImage.png")
            //            {
            //                objPhotoLibrary.PhotoUrl = imagesUrls[i];
            //            }
            //            else
            //            {
            //                objPhotoLibrary.PhotoUrl = ConvertToImage(imagesUrls[i].Split(',')[1]);
            //            }
            //            objPhotoLibrary.IsRead = false;
            //            objPhotoLibrary.CreatedDate = DateTime.Now;
            //            objPhotoLibrary.ModifiedDate = DateTime.Now;
            //            objPhotoLibrary.InpectionReportFID = SubSectionStatusID;
            //            context.PhotoLibraries.Add(objPhotoLibrary);
            //            context.SaveChanges();
            //        }
            //        SubSectionStatu objSubsection = new SubSectionStatu();
            //        objSubsection = context.SubSectionStatu.FirstOrDefault(x => x.ID == SubSectionStatusID);
            //        objSubsection.IsUpdated = true;
            //        context.SaveChanges();
            //        _messageToShow.Message = "Updation done successfully.";
            //    }

            //}
            //catch (Exception ex)
            //{
            //    _messageToShow.Message = "There is some problem, please try after sometime";//ex.Message.ToString();
            //}
            //return Json(_messageToShow);

            #endregion


            if (Sessions.LoggedInUser != null)
            {

                if (context.tblPhotoLibrary.Any(x => x.InspectionReportID == InspectionReportID))
                {
                    _messageToShow.Message = "Already Updated.";
                    return Json(_messageToShow);
                }

                string[] imagesUrls = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<string[]>(imagesUrl);
                List<Coordinates> imgCoordinates = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<Coordinates>>(xc);
                tblphotolibrary objtblPhotoLibrary;

                objtblPhotoLibrary = new tblphotolibrary();
                objtblPhotoLibrary.ID = Guid.NewGuid().ToString();
                objtblPhotoLibrary.InspectionReportID = InspectionReportID;
                objtblPhotoLibrary.SellerComment = comments;
                objtblPhotoLibrary.CreatedDate = System.DateTime.Now;
                if (imagesUrls[0] == "noImage.png")
                {
                    objtblPhotoLibrary.PhotoURL1 = imagesUrls[0];

                }
                else
                {
                    if (imgCoordinates[0].IsCropped == "cropped")
                    {
                        objtblPhotoLibrary.PhotoURL1 = ConvertToImage(imagesUrls[0].Split(',')[1]);
                        string fpath = Path.Combine(Server.MapPath(@"~\Images\Uploaded\"), objtblPhotoLibrary.PhotoURL1);
                        Image oimg = Image.FromFile(fpath);
                        Rectangle cropcords = new Rectangle(
                        Convert.ToInt32(imgCoordinates[0].xc),
                        Convert.ToInt32(imgCoordinates[0].y),
                        Convert.ToInt32(imgCoordinates[0].w),
                        Convert.ToInt32(imgCoordinates[0].h));
                        string cfname, cfpath;
                        Bitmap bitMap = new Bitmap(cropcords.Width, cropcords.Height, oimg.PixelFormat);
                        Graphics grph = Graphics.FromImage(bitMap);
                        grph.DrawImage(oimg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), cropcords, GraphicsUnit.Pixel);
                        cfname = "crop_" + objtblPhotoLibrary.PhotoURL1;
                        cfpath = Path.Combine(Server.MapPath(@"~\Images\Uploaded\"), cfname);
                        bitMap.Save(cfpath);
                        objtblPhotoLibrary.PhotoURL1 = cfname;
                    }
                    else
                    {
                        if (!imagesUrls[0].Contains("noImage"))
                            objtblPhotoLibrary.PhotoURL1 = ConvertToImage(imagesUrls[0].Split(',')[1]);
                    }
                }
                if (imagesUrls[1] == "noImage.png")
                {
                    objtblPhotoLibrary.PhotoURL2 = imagesUrls[1];
                }
                else
                {
                    if (imgCoordinates[1].IsCropped == "cropped")
                    {
                        objtblPhotoLibrary.PhotoURL2 = ConvertToImage(imagesUrls[1].Split(',')[1]);


                        string fpath = Path.Combine(Server.MapPath(@"~\Images\Uploaded\"), objtblPhotoLibrary.PhotoURL2);
                        Image oimg = Image.FromFile(fpath);
                        Rectangle cropcords = new Rectangle(
                        Convert.ToInt32(imgCoordinates[1].xc),
                        Convert.ToInt32(imgCoordinates[1].y),
                        Convert.ToInt32(imgCoordinates[1].w),
                        Convert.ToInt32(imgCoordinates[1].h));
                        string cfname, cfpath;
                        Bitmap bitMap = new Bitmap(cropcords.Width, cropcords.Height, oimg.PixelFormat);
                        Graphics grph = Graphics.FromImage(bitMap);
                        grph.DrawImage(oimg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), cropcords, GraphicsUnit.Pixel);
                        cfname = "crop_" + objtblPhotoLibrary.PhotoURL2;
                        cfpath = Path.Combine(Server.MapPath(@"~\Images\Uploaded\"), cfname);
                        bitMap.Save(cfpath);
                        objtblPhotoLibrary.PhotoURL2 = cfname;
                    }
                    else
                    {
                        if (!imagesUrls[1].Contains("noImage"))
                            objtblPhotoLibrary.PhotoURL2 = ConvertToImage(imagesUrls[1].Split(',')[1]);
                    }
                }
                if (imagesUrls[2] == "noImage.png")
                {
                    objtblPhotoLibrary.PhotoURL3 = imagesUrls[2];
                }
                else
                {
                    if (imgCoordinates[2].IsCropped == "cropped")
                    {
                        objtblPhotoLibrary.PhotoURL3 = ConvertToImage(imagesUrls[2].Split(',')[1]);
                        string fpath = Path.Combine(Server.MapPath(@"~\Images\Uploaded\"), objtblPhotoLibrary.PhotoURL3);
                        Image oimg = Image.FromFile(fpath);
                        Rectangle cropcords = new Rectangle(
                        Convert.ToInt32(imgCoordinates[2].xc),
                        Convert.ToInt32(imgCoordinates[2].y),
                        Convert.ToInt32(imgCoordinates[2].w),
                        Convert.ToInt32(imgCoordinates[2].h));
                        string cfname, cfpath;
                        Bitmap bitMap = new Bitmap(cropcords.Width, cropcords.Height, oimg.PixelFormat);
                        Graphics grph = Graphics.FromImage(bitMap);
                        grph.DrawImage(oimg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), cropcords, GraphicsUnit.Pixel);
                        cfname = "crop_" + objtblPhotoLibrary.PhotoURL3;
                        cfpath = Path.Combine(Server.MapPath(@"~\Images\Uploaded\"), cfname);
                        bitMap.Save(cfpath);
                        objtblPhotoLibrary.PhotoURL3 = cfname;
                    }
                    else
                    {
                        if (!imagesUrls[2].Contains("noImage"))
                            objtblPhotoLibrary.PhotoURL3 = ConvertToImage(imagesUrls[2].Split(',')[1]);

                    }
                }
                if (imagesUrls[3] == "noImage.png")
                {
                    objtblPhotoLibrary.PhotoURL4 = imagesUrls[3];
                }
                else
                {
                    if (imgCoordinates[3].IsCropped == "cropped")
                    {

                        objtblPhotoLibrary.PhotoURL4 = ConvertToImage(imagesUrls[3].Split(',')[1]);
                        string fpath = Path.Combine(Server.MapPath(@"~\Images\Uploaded\"), objtblPhotoLibrary.PhotoURL4);
                        Image oimg = Image.FromFile(fpath);
                        Rectangle cropcords = new Rectangle(
                        Convert.ToInt32(imgCoordinates[3].xc),
                        Convert.ToInt32(imgCoordinates[3].y),
                        Convert.ToInt32(imgCoordinates[3].w),
                        Convert.ToInt32(imgCoordinates[3].h));
                        string cfname, cfpath;
                        Bitmap bitMap = new Bitmap(cropcords.Width, cropcords.Height, oimg.PixelFormat);
                        Graphics grph = Graphics.FromImage(bitMap);
                        grph.DrawImage(oimg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), cropcords, GraphicsUnit.Pixel);
                        cfname = "crop_" + objtblPhotoLibrary.PhotoURL4;
                        cfpath = Path.Combine(Server.MapPath(@"~\Images\Uploaded\"), cfname);
                        bitMap.Save(cfpath);
                        objtblPhotoLibrary.PhotoURL4 = cfname;
                    }
                    else
                    {
                        if (!imagesUrls[3].Contains("noImage"))
                            objtblPhotoLibrary.PhotoURL4 = ConvertToImage(imagesUrls[3].Split(',')[1]);
                    }
                }

                objtblPhotoLibrary.SellerComment = comments;
                //objtblPhotoLibrary.IsApproved = false;
                objtblPhotoLibrary.IsDeleted = false;
                objtblPhotoLibrary.IsUpdated = true;
                context.tblPhotoLibrary.Add(objtblPhotoLibrary);

                inspectionreport objSubsection = new inspectionreport();
                objSubsection = context.InspectionReport.FirstOrDefault(x => x.ID == InspectionReportID);
                objSubsection.IsUpdated = true;
                objSubsection.IsInspectorReviewed = true;
                context.SaveChanges();
                _messageToShow.Message = "Updation done successfully.";
            }
            else
            {
                _messageToShow.Message = "Session Over";
            }

            return Json(_messageToShow);
        }
        #endregion

        #region Image To Base64
        /// <summary>
        /// Converts image to base64 string
        /// </summary>
        /// <param name="image"></param>
        /// <param name="format"></param>
        /// <param name="imageExt"></param>
        /// <returns></returns>
        private string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format, string imageExt)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = "data:image/" + imageExt + ";base64!@#$%^&*" + Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
        #endregion

        #region ConvertToImage
        /// <summary>
        /// function to convert image in base 64
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="sectionId"></param>
        /// <param name="subsectionId"></param>
        /// <returns></returns>
        public string ConvertToImage(string base64)
        {
            try
            {
                string FileName = string.Empty;

                using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(base64)))
                {
                    using (Bitmap bm2 = new Bitmap(ms))
                    {
                        FileName = Guid.NewGuid().ToString() + ".jpg";

                        Save(bm2, 180, 150, 100, Server.MapPath(@"~\Images\Uploaded\"), FileName);
                        //  bm2.Save(Server.MapPath(@"~\Images\Uploaded\") + FileName);
                    }
                }
                return FileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(Bitmap image, int maxWidth, int maxHeight, int quality, string filePath, string FileName)
        {
            // Get the image's original width and height
            int originalWidth = image.Width;
            int originalHeight = image.Height;

            // To preserve the aspect ratio
            float ratioX = (float)maxWidth / (float)originalWidth;
            float ratioY = (float)maxHeight / (float)originalHeight;
            float ratio = Math.Min(ratioX, ratioY);

            // New width and height based on aspect ratio
            int newWidth = (int)(originalWidth * ratio);
            int newHeight = (int)(originalHeight * ratio);

            // Convert other formats (including CMYK) to RGB.
            Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

            // Draws the image in the specified size with quality mode set to HighQuality
            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            // Get an ImageCodecInfo object that represents the JPEG codec.
            ImageCodecInfo imageCodecInfo = this.GetEncoderInfo(ImageFormat.Jpeg);

            // Create an Encoder object for the Quality parameter.
            Encoder encoder = Encoder.Quality;

            // Create an EncoderParameters object. 
            EncoderParameters encoderParameters = new EncoderParameters(1);

            // Save the image as a JPEG file with quality level.
            EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
            encoderParameters.Param[0] = encoderParameter;
            newImage.Save(filePath + FileName, imageCodecInfo, encoderParameters);
        }
        private ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }
        public string ConvertToImage_CropImage(string base64)
        {
            try
            {
                string FileName = string.Empty;
                using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(base64)))
                {
                    using (Bitmap bm2 = new Bitmap(ms))
                    {
                        FileName = Guid.NewGuid().ToString() + ".jpg";
                        //bm2.Size = new Bitmap(null, new Size(350, 500));
                        bm2.Save(Server.MapPath(@"~\Images\CropImage\") + FileName);
                    }
                }
                return FileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UploadBatchDataFile
        /// <summary>
        /// POST: method for upload images on maintenance screen.
        /// </summary>
        /// <returns></returns>
        /// 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UploadBatchDataFile(HttpPostedFileBase qqfile)
        {
            try
            {
                if (qqfile != null && qqfile.ContentLength > 0)
                {
                    //Below line get Extension of file and remove orignal file name.
                    //string fileName = Guid.NewGuid().ToString() + Path.GetExtension(qqfile.FileName);

                    string fileName = Guid.NewGuid().ToString() + "_" + qqfile.FileName;
                    var path = Path.Combine(Server.MapPath("~/Images/Uploaded"), fileName);
                    imagesNameList.Add(fileName);
                    qqfile.SaveAs(@path);
                    // further processing on file can be done here
                    return Json(new { success = true, Imagesurl = imagesNameList });
                    // return Json(new { success = true, Imagesurl = imagesName });               
                }
                else
                {
                    // this works for Firefox, Chrome
                    var filename = Request["qqfile"];
                    if (!string.IsNullOrEmpty(filename))
                    {
                        //Below line get Extension of file and remove orignal file name.
                        //string newFileName = Guid.NewGuid().ToString() + Path.GetExtension(qqfile.FileName);

                        string newFileName = Guid.NewGuid().ToString() + "_" + qqfile.FileName;
                        filename = Path.Combine(Server.MapPath("~/Images/Uploaded"), newFileName);
                        //using (var output = File.Create(filename))
                        //{
                        //    Request.InputStream.CopyTo(output);
                        //}
                        // further processing on file can be done here
                        return Json(new { success = true, Imagesurl = imagesNameList });

                    }
                }
                return Json(new { success = false });
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        #endregion


        #region NewNotification
        /// <summary>
        /// function to only those defective section on which no updateds made by Seller
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult NewNotification()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Home Seller" || Sessions.LoggedInUser.RoleName == "Home Buyer"))
                {

                    inspectionorder objInspectionOrder = new inspectionorder();
                    List<PropertyAddress> objPropertyAddress = new List<PropertyAddress>();
                    List<PropertyAddress> objPropertyAddress_Updated = new List<PropertyAddress>();

                    objPropertyAddress = (from IO in context.InspectionOrders
                                          join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                          join SecStatus in context.SubSectionStatu on IOD.InspectionOrderId equals SecStatus.InspectionOrderID
                                          where IO.RequesterID == Sessions.LoggedInUser.UserId && SecStatus.Status == false && SecStatus.IsUpdated == false && IO.IsUpdatable == true
                                          select new PropertyAddress
                                          {
                                              OrderID = IO.InspectionOrderID,
                                              OwnerFirstName = IOD.OwnerFirstName,
                                              OwnerLastName = IOD.OwnerLastName,
                                              StreetAddress = IOD.StreetAddess,
                                              City = IOD.City,
                                              State = IOD.State,
                                              Zip = IOD.Zip
                                          }).Distinct().ToList();

                    ViewBag.NewNotification = objPropertyAddress.Count;


                    /*Getting count of updated inspection orders*/
                    objPropertyAddress_Updated = (from sectionStatus in context.SubSectionStatu
                                                  join IO in context.InspectionOrders on sectionStatus.InspectionOrderID equals IO.InspectionOrderID
                                                  join IOD in context.InsectionOrderDetails on sectionStatus.InspectionOrderID equals IOD.InspectionOrderId
                                                  where IO.RequesterID == Sessions.LoggedInUser.UserId && sectionStatus.IsUpdated == true
                                                  select new PropertyAddress
                                                  {
                                                      OrderID = IO.InspectionOrderID,
                                                      OwnerFirstName = IOD.OwnerFirstName,
                                                      OwnerLastName = IOD.OwnerLastName,
                                                      StreetAddress = IOD.StreetAddess,
                                                      City = IOD.City,
                                                      State = IOD.State,
                                                      Zip = IOD.Zip
                                                  }).Distinct().ToList();

                    ViewBag.UpdatedNotification = objPropertyAddress_Updated.Count;


                    if (Request.Url.Query.Contains("UpdateStatus"))
                    {
                        ///*For Updated Property Status*/

                        List<PropertyAddress> objUpdateStatus = new List<PropertyAddress>();

                        objUpdateStatus = (from sectionStatus in context.SubSectionStatu
                                           join IO in context.InspectionOrders on sectionStatus.InspectionOrderID equals IO.InspectionOrderID
                                           join IOD in context.InsectionOrderDetails on sectionStatus.InspectionOrderID equals IOD.InspectionOrderId
                                           join PhG in context.PhotoLibraries on IO.InspectionOrderID equals PhG.InspectionOrderID
                                           where IO.RequesterID == Sessions.LoggedInUser.UserId && sectionStatus.IsUpdated == true
                                           select new PropertyAddress
                                           {
                                               OrderID = IO.InspectionOrderID,
                                               OwnerFirstName = IOD.OwnerFirstName,
                                               OwnerLastName = IOD.OwnerLastName,
                                               StreetAddress = IOD.StreetAddess,
                                               City = IOD.City,
                                               State = IOD.State,
                                               Zip = IOD.Zip,
                                               CreatedDate = PhG.CreatedDate,
                                               /*Below property is used here to store user id */
                                               Comment = PhG.CommentedBy
                                           }).OrderByDescending(x => x.CreatedDate).ToList().OrderByDescending(x => x.CreatedDate).GroupBy(x => x.OrderID).Select(grp => grp.FirstOrDefault()).ToList();

                        foreach (var item in objUpdateStatus)
                        {
                            item.IsInspectorComment = context.UserInfoes.Where(x => x.ID == item.Comment).FirstOrDefault().Role == "85DA6641-E08A-4192-A" ? true : false;
                        }
                        return View(objUpdateStatus);
                    }

                    return View(objPropertyAddress);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetRejectedOrders
        /// <summary>
        /// function to only those defective section on which updateds made by Seller
        /// <param name="id"></param>
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult GetRejectedOrders(int id)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Home Seller" || Sessions.LoggedInUser.RoleName == "Home Buyer"))
                {
                    //InspectionOrder objInspectionOrder = new InspectionOrder();
                    List<PropertyAddress> objPropertyAddress = new List<PropertyAddress>();

                    objPropertyAddress = (from sectionStatus in context.SubSectionStatu
                                          join IO in context.InspectionOrders on sectionStatus.InspectionOrderID equals IO.InspectionOrderID
                                          join IOD in context.InsectionOrderDetails on sectionStatus.InspectionOrderID equals IOD.InspectionOrderId
                                          join PhG in context.PhotoLibraries on IO.InspectionOrderID equals PhG.InspectionOrderID
                                          where IO.RequesterID == Sessions.LoggedInUser.UserId && sectionStatus.IsUpdated == true
                                          select new PropertyAddress
                                          {
                                              OrderID = IO.InspectionOrderID,
                                              OwnerFirstName = IOD.OwnerFirstName,
                                              OwnerLastName = IOD.OwnerLastName,
                                              StreetAddress = IOD.StreetAddess,
                                              City = IOD.City,
                                              State = IOD.State,
                                              Zip = IOD.Zip,
                                              CreatedDate = PhG.CreatedDate,
                                              /*Below property is used here to store user id */
                                              Comment = PhG.CommentedBy
                                          }).OrderByDescending(x => x.CreatedDate).ToList().OrderByDescending(x => x.CreatedDate).GroupBy(x => x.OrderID).Select(grp => grp.FirstOrDefault()).ToList();

                    foreach (var item in objPropertyAddress)
                    {
                        item.IsInspectorComment = context.UserInfoes.Where(x => x.ID == item.Comment).FirstOrDefault().Role == "85DA6641-E08A-4192-A" ? true : false;
                    }

                    return PartialView("_RejectedNotifications", objPropertyAddress);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion



        #region MaintananceDetails
        /// <summary>
        /// function to get the defective sections accoording to orderid
        /// <param name="OrderID"></param>
        /// <param name="IsUpdated"></param>
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult MaintananceDetails(string OrderID, bool IsUpdated)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Home Seller" || Sessions.LoggedInUser.RoleName == "Home Buyer"))
                {
                    ViewBag.OrderID = OrderID;
                    List<PropertyAddress> objPropertyAddress = new List<PropertyAddress>();
                    List<Comments> objComments = new List<Comments>();
                    objPropertyAddress = (from sectionStatus in context.SubSectionStatu
                                          join IO in context.InspectionOrders on sectionStatus.InspectionOrderID equals IO.InspectionOrderID
                                          join IOD in context.InsectionOrderDetails on sectionStatus.InspectionOrderID equals IOD.InspectionOrderId
                                          join Section in context.MainSections on sectionStatus.SectionID equals Section.ID
                                          join SubSections in context.SubSections on sectionStatus.SubSectionID equals SubSections.ID
                                          where IO.RequesterID == Sessions.LoggedInUser.UserId && sectionStatus.InspectionOrderID == OrderID && sectionStatus.IsUpdated == IsUpdated
                                          select new PropertyAddress
                                          {

                                              SectionID = sectionStatus.SectionID,
                                              SubSectionID = sectionStatus.SubSectionID,
                                              SectionName = Section.SectionName,
                                              SubSectionName = SubSections.SubSectionName,
                                              OrderID = sectionStatus.InspectionOrderID,
                                              Comment = sectionStatus.Comment,
                                              TableName = Section.SectionName,
                                              AcceptReject = sectionStatus.Status,
                                              CanUpdate = true,
                                              SubSectionStatusID = sectionStatus.ID,
                                              StreetAddress = IOD.StreetAddess,
                                              City = IOD.City,
                                              State = IOD.State,
                                              Zip = IOD.Zip

                                          }).Distinct().ToList();
                    if (IsUpdated)
                    {
                        foreach (var item in objPropertyAddress)
                        {
                            objComments = (from PG in context.PhotoLibraries
                                           join IO in context.InspectionOrders on PG.InspectionOrderID equals IO.InspectionOrderID
                                           join UI in context.UserInfoes on PG.CommentedBy equals UI.ID
                                           join Roles in context.Roles on UI.Role equals Roles.RoleID
                                           where PG.InspectionOrderID == item.OrderID && PG.SectionID == item.SectionID && PG.SubSectionId == item.SubSectionID && PG.Comments != null
                                           select new Comments
                                           {
                                               Comment = PG.Comments,
                                               CommentBy = PG.CommentedBy,
                                               UserName = UI.FirstName + UI.LastName,
                                               CreatedDate = PG.CreatedDate,
                                               Role = Roles.RoleName
                                           }).Distinct().ToList();
                            string comment = objComments.OrderByDescending(x => x.CreatedDate).FirstOrDefault().CommentBy;
                            item.UserComments = objComments.GroupBy(x => x.Comment).Select(y => y.ToList().FirstOrDefault()).OrderBy(z => z.CreatedDate).ToList();
                            if (comment == Sessions.LoggedInUser.UserId)
                            {
                                item.CanUpdate = false;
                            }
                            else
                            {
                                item.CanUpdate = true;
                            }
                            //var list = objComments.GroupBy(x => x.Comment).Select(y => y.ToList().FirstOrDefault()).ToList();
                        }
                    }

                    return View(objPropertyAddress);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            // return View();

        }
        #endregion
        public List<PropertyAddress> RetunPropertyList()
        {
            try
            {
                List<PropertyAddress> objDetails = new List<PropertyAddress>();
                List<PropertyAddress> objPerSectionDetails = new List<PropertyAddress>();

                List<PropertyAddress> objList_PropertyAddress = new List<PropertyAddress>();
                List<string> ArrayOfOrderPlaced = context.InspectionOrders.Where(x => x.RequesterID == Sessions.LoggedInUser.UserId && x.InspectionStatus == "Completed").Select(y => y.InspectionOrderID).Distinct().ToList();
                List<string> Order_id = new List<string>();

                /**************************************/
                var objlist_ElectrialCommanSection = (from k in context.ElectrialCommanSection
                                                      where k.ConditionID == "3" && k.ReportID == I_OrderId//ArrayOfOrderPlaced.Contains(k.ReportID)
                                                      join m in context.MainSections on k.SectionID equals m.ID
                                                      join ss in context.SubSections on k.SubsectionID equals ss.ID
                                                      select new PropertyAddress
                                                      {
                                                          OrderID = k.ReportID,
                                                          SectionID = k.SectionID,
                                                          SubSectionID = k.SubsectionID,
                                                          SectionName = m.SectionName,
                                                          SubSectionName = ss.SubSectionName,
                                                          Comment = k.Comments,
                                                          TableName = "ElectrialCommanSection"
                                                      }).FirstOrDefault();

                if (objlist_ElectrialCommanSection != null)
                    objPerSectionDetails.Add(objlist_ElectrialCommanSection);

                var objlist_ElectricalPanel = (from k in context.ElectricalPanel
                                               where k.ConditionID == "3" && k.ReportID == I_OrderId//ArrayOfOrderPlaced.Contains(k.ReportID)
                                               join m in context.MainSections on k.SectionID equals m.ID
                                               join ss in context.SubSections on k.SubsectionID equals ss.ID
                                               select new PropertyAddress
                                               {
                                                   OrderID = k.ReportID,
                                                   SectionID = k.SectionID,
                                                   SubSectionID = k.SubsectionID,
                                                   SectionName = m.SectionName,
                                                   SubSectionName = ss.SubSectionName,
                                                   Comment = k.Comments,
                                                   TableName = "ElectricalPanel"
                                               }).FirstOrDefault();

                if (objlist_ElectricalPanel != null)
                    objPerSectionDetails.Add(objlist_ElectricalPanel);

                var objlist_ElectricalRoom = (from k in context.ElectricalRoom
                                              where k.ConditionID == "3" && k.ReportID == I_OrderId//ArrayOfOrderPlaced.Contains(k.ReportID)
                                              join m in context.MainSections on k.SectionID equals m.ID
                                              join ss in context.SubSections on k.SubSectionID equals ss.ID
                                              select new PropertyAddress
                                              {
                                                  OrderID = k.ReportID,
                                                  SectionID = k.SectionID,
                                                  SubSectionID = k.SubSectionID,
                                                  SectionName = m.SectionName,
                                                  SubSectionName = ss.SubSectionName,
                                                  Comment = k.Comments,
                                                  TableName = "ElectricalRoom"
                                              }).FirstOrDefault();

                if (objlist_ElectricalRoom != null)
                    objPerSectionDetails.Add(objlist_ElectricalRoom);

                var objlist_ElectricalService = (from k in context.ElectricalService
                                                 where k.ConditionID == "3" && k.ReportID == I_OrderId//ArrayOfOrderPlaced.Contains(k.ReportID)
                                                 join m in context.MainSections on k.SectionID equals m.ID
                                                 join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                 select new PropertyAddress
                                                 {
                                                     OrderID = k.ReportID,
                                                     SectionID = k.SectionID,
                                                     SubSectionID = k.SubSectionID,
                                                     SectionName = m.SectionName,
                                                     SubSectionName = ss.SubSectionName,
                                                     Comment = k.Comments,
                                                     TableName = "ElectricalService"
                                                 }).FirstOrDefault();

                if (objlist_ElectricalService != null)
                    objPerSectionDetails.Add(objlist_ElectricalService);


                var objlist_Appliancescommonsections = (from k in context.Appliancescommonsections
                                                        where k.CheckboxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                                        join m in context.MainSections on k.SectionID equals m.ID
                                                        join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                        select new PropertyAddress
                                                        {
                                                            OrderID = k.InspectionOrderID,
                                                            SectionID = k.SectionID,
                                                            SubSectionID = k.SubSectionID,
                                                            SectionName = m.SectionName,
                                                            SubSectionName = ss.SubSectionName,
                                                            Comment = k.Comments,
                                                            TableName = "Appliancescommonsections"
                                                        }).FirstOrDefault();

                if (objlist_Appliancescommonsections != null)
                    objPerSectionDetails.Add(objlist_Appliancescommonsections);

                var objlist_AppliancesDishwashDisposalRange_sections = (from k in context.AppliancesDishwashDisposalRange_sections
                                                                        where k.CheckBoxID == "3" && k.InsepectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InsepectionOrderID)
                                                                        join m in context.MainSections on k.SectionID equals m.ID
                                                                        join ss in context.SubSections on k.SubSectionId equals ss.ID
                                                                        select new PropertyAddress
                                                                        {
                                                                            OrderID = k.InsepectionOrderID,
                                                                            SectionID = k.SectionID,
                                                                            SubSectionID = k.SubSectionId,
                                                                            SectionName = m.SectionName,
                                                                            SubSectionName = ss.SubSectionName,
                                                                            Comment = k.Comments,
                                                                            TableName = "AppliancesDishwashDisposalRange_sections"
                                                                        }).FirstOrDefault();

                if (objlist_AppliancesDishwashDisposalRange_sections != null)
                    objPerSectionDetails.Add(objlist_AppliancesDishwashDisposalRange_sections);

                var objlist_AppliancesDryerDoorBellSectiions = (from k in context.AppliancesDryerDoorBellSectiions
                                                                where k.CheckBoxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                                                join m in context.MainSections on k.SectionID equals m.ID
                                                                join ss in context.SubSections on k.SubsectionID equals ss.ID
                                                                select new PropertyAddress
                                                                {
                                                                    OrderID = k.InspectionOrderID,
                                                                    SectionID = k.SectionID,
                                                                    SubSectionID = k.SubsectionID,
                                                                    SectionName = m.SectionName,
                                                                    SubSectionName = ss.SubSectionName,
                                                                    Comment = k.Comments,
                                                                    TableName = "AppliancesDryerDoorBellSectiions"
                                                                }).FirstOrDefault();

                if (objlist_AppliancesDryerDoorBellSectiions != null)
                    objPerSectionDetails.Add(objlist_AppliancesDryerDoorBellSectiions);

                var objlist_AppliancesHoodVentOtherSections = (from k in context.AppliancesHoodVentOtherSections
                                                               where k.CheckBoxID == "3" && k.InspectionOrderId == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderId)
                                                               join m in context.MainSections on k.SectionID equals m.ID
                                                               join ss in context.SubSections on k.SubsectionID equals ss.ID
                                                               select new PropertyAddress
                                                               {
                                                                   OrderID = k.InspectionOrderId,
                                                                   SectionID = k.SectionID,
                                                                   SubSectionID = k.SubsectionID,
                                                                   SectionName = m.SectionName,
                                                                   SubSectionName = ss.SubSectionName,
                                                                   Comment = k.Comments,
                                                                   TableName = "AppliancesHoodVentOtherSections"
                                                               }).FirstOrDefault();

                if (objlist_AppliancesHoodVentOtherSections != null)
                    objPerSectionDetails.Add(objlist_AppliancesHoodVentOtherSections);

                var objlist_AppliancesOvenSections = (from k in context.AppliancesOvenSections
                                                      where k.CheckboxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                                      join m in context.MainSections on k.SectionID equals m.ID
                                                      join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                      select new PropertyAddress
                                                      {
                                                          OrderID = k.InspectionOrderID,
                                                          SectionID = k.SectionID,
                                                          SubSectionID = k.SubSectionID,
                                                          SectionName = m.SectionName,
                                                          SubSectionName = ss.SubSectionName,
                                                          Comment = k.Comments,
                                                          TableName = "AppliancesOvenSections"
                                                      }).FirstOrDefault();

                if (objlist_AppliancesOvenSections != null)
                    objPerSectionDetails.Add(objlist_AppliancesOvenSections);

                var objlist_GroundCommonSections = (from k in context.GroundCommonSections
                                                    where k.CheckBoxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                                    join m in context.MainSections on k.SectionID equals m.ID
                                                    join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                    select new PropertyAddress
                                                    {
                                                        OrderID = k.InspectionOrderID,
                                                        SectionID = k.SectionID,
                                                        SubSectionID = k.SubSectionID,
                                                        SectionName = m.SectionName,
                                                        SubSectionName = ss.SubSectionName,
                                                        Comment = k.Comments,
                                                        TableName = "GroundCommonSections"
                                                    }).FirstOrDefault();

                if (objlist_GroundCommonSections != null)
                    objPerSectionDetails.Add(objlist_GroundCommonSections);

                var objlist_GroundGassections = (from k in context.GroundGassections
                                                 where k.CheckBoxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                                 join m in context.MainSections on k.SectionID equals m.ID
                                                 join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                 select new PropertyAddress
                                                 {
                                                     OrderID = k.InspectionOrderID,
                                                     SectionID = k.SectionID,
                                                     SubSectionID = k.SubSectionID,
                                                     SectionName = m.SectionName,
                                                     SubSectionName = ss.SubSectionName,
                                                     Comment = k.Comments,
                                                     TableName = "GroundGassections"
                                                 }).FirstOrDefault();

                if (objlist_GroundGassections != null)
                    objPerSectionDetails.Add(objlist_GroundGassections);

                var objlist_GroundH2OSections = (from k in context.GroundH2OSections
                                                 where k.CheckBoxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                                 join m in context.MainSections on k.SectionId equals m.ID
                                                 join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                 select new PropertyAddress
                                                 {
                                                     OrderID = k.InspectionOrderID,
                                                     SectionID = k.SectionId,
                                                     SubSectionID = k.SubSectionID,
                                                     SectionName = m.SectionName,
                                                     SubSectionName = ss.SubSectionName,
                                                     Comment = k.Comments,
                                                     TableName = "GroundH2OSections"
                                                 }).FirstOrDefault();

                if (objlist_GroundH2OSections != null)
                    objPerSectionDetails.Add(objlist_GroundH2OSections);

                var objlist_GroundSewerSections = (from k in context.GroundSewerSections
                                                   where k.CheckBoxID == "3" && k.C_InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.C_InspectionOrderID)
                                                   join m in context.MainSections on k.SectionID equals m.ID
                                                   join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                   select new PropertyAddress
                                                   {
                                                       OrderID = k.C_InspectionOrderID,
                                                       SectionID = k.SectionID,
                                                       SubSectionID = k.SubSectionID,
                                                       SectionName = m.SectionName,
                                                       SubSectionName = ss.SubSectionName,
                                                       Comment = k.Comments,
                                                       TableName = "GroundSewerSections"
                                                   }).FirstOrDefault();

                if (objlist_GroundSewerSections != null)
                    objPerSectionDetails.Add(objlist_GroundSewerSections);

                var objlist_GroundSprinkSysSections = (from k in context.GroundSprinkSysSections
                                                       where k.CheckBoxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                                       join m in context.MainSections on k.SectionID equals m.ID
                                                       join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                       select new PropertyAddress
                                                       {
                                                           OrderID = k.InspectionOrderID,
                                                           SectionID = k.SectionID,
                                                           SubSectionID = k.SubSectionID,
                                                           SectionName = m.SectionName,
                                                           SubSectionName = ss.SubSectionName,
                                                           Comment = k.Comments,
                                                           TableName = "GroundSprinkSysSections"
                                                       }).FirstOrDefault();

                if (objlist_GroundSprinkSysSections != null)
                    objPerSectionDetails.Add(objlist_GroundSprinkSysSections);

                var objlist_HVACCoolings = (from k in context.HVACCoolings
                                            where k.CheckBoxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                            join m in context.MainSections on k.SectionID equals m.ID
                                            join ss in context.SubSections on k.SubsectionID equals ss.ID
                                            select new PropertyAddress
                                            {
                                                OrderID = k.InspectionOrderID,
                                                SectionID = k.SectionID,
                                                SubSectionID = k.SubsectionID,
                                                SectionName = m.SectionName,
                                                SubSectionName = ss.SubSectionName,
                                                Comment = k.Comments,
                                                TableName = "HVACCoolings"
                                            }).FirstOrDefault();

                if (objlist_HVACCoolings != null)
                    objPerSectionDetails.Add(objlist_HVACCoolings);

                var objlist_HVACDuctsVents = (from k in context.HVACDuctsVents
                                              where k.CheckBoxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                              join m in context.MainSections on k.SectionID equals m.ID
                                              join ss in context.SubSections on k.SubSectionID equals ss.ID
                                              select new PropertyAddress
                                              {
                                                  OrderID = k.InspectionOrderID,
                                                  SectionID = k.SectionID,
                                                  SubSectionID = k.SubSectionID,
                                                  SectionName = m.SectionName,
                                                  SubSectionName = ss.SubSectionName,
                                                  Comment = k.Comments,
                                                  TableName = "HVACDuctsVents"
                                              }).FirstOrDefault();

                if (objlist_HVACDuctsVents != null)
                    objPerSectionDetails.Add(objlist_HVACDuctsVents);

                var objlist_HVACEquipBoileRadiantSections = (from k in context.HVACEquipBoileRadiantSections
                                                             where k.CheckBoxID == "3" && k.InspectionOrderId == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderId)
                                                             join m in context.MainSections on k.SectionID equals m.ID
                                                             join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                             select new PropertyAddress
                                                             {
                                                                 OrderID = k.InspectionOrderId,
                                                                 SectionID = k.SectionID,
                                                                 SubSectionID = k.SubSectionID,
                                                                 SectionName = m.SectionName,
                                                                 SubSectionName = ss.SubSectionName,
                                                                 Comment = k.Comments,
                                                                 TableName = "HVACEquipBoileRadiantSections"
                                                             }).FirstOrDefault();

                if (objlist_HVACEquipBoileRadiantSections != null)
                    objPerSectionDetails.Add(objlist_HVACEquipBoileRadiantSections);

                var objlist_OptionalCommonSections = (from k in context.OptionalCommonSections
                                                      where k.CheckBoxID == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                                      join m in context.MainSections on k.SectionID equals m.ID
                                                      join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                      select new PropertyAddress
                                                      {
                                                          OrderID = k.InspectionOrderID,
                                                          SectionID = k.SectionID,
                                                          SubSectionID = k.SubSectionID,
                                                          SectionName = m.SectionName,
                                                          SubSectionName = ss.SubSectionName,
                                                          Comment = k.Comments,
                                                          TableName = "OptionalCommonSections"
                                                      }).FirstOrDefault();

                if (objlist_OptionalCommonSections != null)
                    objPerSectionDetails.Add(objlist_OptionalCommonSections);

                var objlist_PlumbingMainlineKitchenOthers_Sections = (from k in context.PlumbingMainlineKitchenOthers_Sections
                                                                      where k.CheckBoxID == "3" && k.InspectionOrderId == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderId)
                                                                      join m in context.MainSections on k.SectionID equals m.ID
                                                                      join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                                      select new PropertyAddress
                                                                      {
                                                                          OrderID = k.InspectionOrderId,
                                                                          SectionID = k.SectionID,
                                                                          SubSectionID = k.SubSectionID,
                                                                          SectionName = m.SectionName,
                                                                          SubSectionName = ss.SubSectionName,
                                                                          Comment = k.Comments,
                                                                          TableName = "PlumbingMainlineKitchenOthers_Sections"
                                                                      }).FirstOrDefault();

                if (objlist_PlumbingMainlineKitchenOthers_Sections != null)
                    objPerSectionDetails.Add(objlist_PlumbingMainlineKitchenOthers_Sections);

                var objlist_PlumbingWaterheats = (from k in context.PlumbingWaterheats
                                                  where k.CheckBoxID == "3" && k.InspectionOrderId == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderId)
                                                  join m in context.MainSections on k.SectionId equals m.ID
                                                  join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                  select new PropertyAddress
                                                  {
                                                      OrderID = k.InspectionOrderId,
                                                      SectionID = k.SectionId,
                                                      SubSectionID = k.SubSectionID,
                                                      SectionName = m.SectionName,
                                                      SubSectionName = ss.SubSectionName,
                                                      Comment = k.Comments,
                                                      TableName = "PlumbingWaterheats"
                                                  }).FirstOrDefault();

                if (objlist_PlumbingWaterheats != null)
                    objPerSectionDetails.Add(objlist_PlumbingWaterheats);

                var objlist_StructuralCommonSections = (from k in context.StructuralCommonSections
                                                        where k.CheckBoxiD == "3" && k.InspectionOrderID == I_OrderId//ArrayOfOrderPlaced.Contains(k.InspectionOrderID)
                                                        join m in context.MainSections on k.SectionId equals m.ID
                                                        join ss in context.SubSections on k.SubSectionID equals ss.ID
                                                        select new PropertyAddress
                                                        {
                                                            OrderID = k.InspectionOrderID,
                                                            SectionID = k.SectionId,
                                                            SubSectionID = k.SubSectionID,
                                                            SectionName = m.SectionName,
                                                            SubSectionName = ss.SubSectionName,
                                                            Comment = k.Comments,
                                                            TableName = "StructuralCommonSections"
                                                        }).FirstOrDefault();

                if (objlist_StructuralCommonSections != null)
                    objPerSectionDetails.Add(objlist_StructuralCommonSections);

                return objPerSectionDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public JsonResult Cropper(int x, int y, int w, int h, string img)
        {
            string abc = img.Split(',')[1];
            string filename = ConvertToImage_CropImage(img.Split(',')[1]);
            string temp = "";
            string path = Server.MapPath("~/Images/CropImage/" + filename);
            try
            {

                Image oldImage = Image.FromFile(path);

                //double x1 = x / Convert.ToDouble(oldImage.Width) * 500;
                //double y1 = y / Convert.ToDouble(oldImage.Height) * 350;
                //double w1 = w / Convert.ToDouble(oldImage.Width) * 500;
                //double h1 = y / Convert.ToDouble(oldImage.Height) * 350;

                Rectangle cropArea = new Rectangle(Convert.ToInt32(x), Convert.ToInt32(y), Convert.ToInt32(w), Convert.ToInt32(h));
                //  Image oldImage = Image.FromFile(Server.MapPath(img));
                //temp = ImageToBase64(CropImage(oldImage, cropArea), oldImage.RawFormat, ".jpg");
                Bitmap newImage = CropImage(oldImage, cropArea) as Bitmap;
                temp = ImageToBase64(newImage, oldImage.RawFormat, ".jpg");
                path = path.Substring(0, path.LastIndexOf("\\") + 1);
                //string fName = "temp_" + System.Guid.NewGuid().ToString() + ".jpg";
                //path = path + fName;
                //temp = fName;//path + "temp_" + System.Guid.NewGuid().ToString() + ".jpg";
                //saveJpeg(path, newImage, 1000);
                //Image newImage = FixedSize(oldImage, oldImage.Width, oldImage.Height, true);//CropImage(oldImage, cropArea) as Bitmap;
                //path = path.Substring(0, path.LastIndexOf("\\") + 1);
                //path = path + "temp.jpg";
                //saveJpeg(path, (Bitmap)newImage, 1000);

                //path = @System.Configuration.ConfigurationManager.AppSettings["localhost"];



            }
            catch (Exception ex)
            {

            }

            return Json(temp);
        }

        private ImageCodecInfo getEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];

            return null;
        }

        private void saveJpeg(string path, Bitmap img, long quality)
        {
            // Encoder parameter for image quality
            EncoderParameter qualityParam = new EncoderParameter(
            System.Drawing.Imaging.Encoder.Quality, (long)quality);

            // Jpeg image codec
            ImageCodecInfo jpegCodec = getEncoderInfo("image/jpeg");

            if (jpegCodec == null)
            {
                return;
            }
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            img.Save(path, jpegCodec, encoderParams);
        }

        private static Image CropImage(Image img, Rectangle cropArea)
        {
            try
            {
                //Bitmap bmpImage = new Bitmap(img);
                //Image bmpCrop =  FixedSize(bmpImage,cropArea.Width,cropArea.Height,true); //bmpImage.Clone(cropArea, bmpImage.PixelFormat);
                //return (Image)(bmpCrop);
                Bitmap bmpImage = new Bitmap(img);
                Bitmap bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);
                return (Image)(bmpCrop);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public static System.Drawing.Image FixedSize(Image image, int Width, int Height, bool needToFill)
        {
            #region много арифметики
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            int sourceX = 0;
            int sourceY = 0;
            double destX = 0;
            double destY = 0;

            double nScale = 0;
            double nScaleW = 0;
            double nScaleH = 0;

            nScaleW = ((double)Width / (double)sourceWidth);
            nScaleH = ((double)Height / (double)sourceHeight);
            if (!needToFill)
            {
                nScale = Math.Min(nScaleH, nScaleW);
            }
            else
            {
                nScale = Math.Max(nScaleH, nScaleW);
                destY = (Height - sourceHeight * nScale) / 2;
                destX = (Width - sourceWidth * nScale) / 2;
            }

            if (nScale > 1)
                nScale = 1;

            int destWidth = (int)Math.Round(sourceWidth * nScale);
            int destHeight = (int)Math.Round(sourceHeight * nScale);
            #endregion

            System.Drawing.Bitmap bmPhoto = null;
            try
            {
                bmPhoto = new System.Drawing.Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("destWidth:{0}, destX:{1}, destHeight:{2}, desxtY:{3}, Width:{4}, Height:{5}",
                    destWidth, destX, destHeight, destY, Width, Height), ex);
            }
            using (System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto))
            {
                //grPhoto.InterpolationMode = _interpolationMode;
                //grPhoto.CompositingQuality = _compositingQuality;
                //grPhoto.SmoothingMode = _smoothingMode;

                Rectangle to = new System.Drawing.Rectangle((int)Math.Round(destX), (int)Math.Round(destY), destWidth, destHeight);
                Rectangle from = new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
                //Console.WriteLine("From: " + from.ToString());
                //Console.WriteLine("To: " + to.ToString());
                grPhoto.DrawImage(image, to, from, System.Drawing.GraphicsUnit.Pixel);

                return bmPhoto;
            }
        }

        [HttpPost]
        public JsonResult AcceptDecline(string photoLibraryID, string Approved, string Comment = "")
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    tblphotolibrary objtblPhotoLibrary = new tblphotolibrary();
                    objtblPhotoLibrary = context.tblPhotoLibrary.FirstOrDefault(x => x.ID == photoLibraryID);
                    if (objtblPhotoLibrary != null)
                    {
                        if (Approved == "1")
                        {
                            objtblPhotoLibrary.IsApproved = true;
                            objtblPhotoLibrary.InspectorComment = Comment;
                        }
                        else
                        {
                            objtblPhotoLibrary.IsApproved = false;
                            objtblPhotoLibrary.InspectorComment = Comment;
                        }
                    }
                    context.SaveChanges();
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult UserNotifications()
        {
            SpectorMax.Entities.Classes.UserNotificationsEntity objUserNotificationsEntity = new UserNotificationsEntity();

            //objUserNotificationsEntity.msg_lst = from k in context.UsersNotification
            //                                     join u in context.UserInfoes on k.From equals u.ID 
            //                                     select new UserNotifications_lst
            //                                     {

            //                                     }



            return View(objUserNotificationsEntity);
        }

        [HttpPost]
        public JsonResult GetDetails_Inspector(string firstname, string lastname, int? zipcode)
        {
            try
            {
                string inspectorid = context.ZipCodeAssigned.Where(x => x.ZipCode == zipcode).FirstOrDefault().InspectorID;
                userinfo objuserinfo = context.UserInfoes.Where(x => x.ID == inspectorid).FirstOrDefault();
                return ReturnJson(objuserinfo);
            }
            catch (Exception)
            {
                throw;
            }

            ////ViewBag.OrderID = OrderID;
            //List<PropertyAddress> objDetails = new List<PropertyAddress>();
            //string result = RenderRazorViewToString("~/Views/Notification/_PartialMaintenanceDetails.cshtml", objDetails);
            //return ReturnJson(result);
        }

    }
}
