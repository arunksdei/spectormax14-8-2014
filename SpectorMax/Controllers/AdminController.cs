﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMax.Models;
using SpectorMax.Entities.Classes;
using SpectorMaxDAL;
using System.IO;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Data.Objects.SqlClient;

namespace SpectorMax.Controllers
{
    public class AdminController : BaseController
    {

        #region Global Variables
        //
        // GET: /Admin/
        public LoggedInUserDetails UserInformation { get { return (LoggedInUserDetails)Session["UserDetails"]; } }
        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _messageToShow = new MessageToShow();
        #endregion


        public ActionResult Home()
        {
            Session["UserDetails"] = null;
            Session.Abandon();
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }
        #region AdminDashboard
        /// <summary>
        /// function to get data on Admin Dashboard.
        /// </summary>
        /// <returns></returns>
        /// 

        public ActionResult AdminDashboard()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    var TotalPay = (from IO in context.InspectionOrders
                                    join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                    join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                    join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                    join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                    join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                    select Pay.PaymentAmount).Sum();

                    ViewBag.TotalPayment = TotalPay;

                    var TotalSubAdmin = (from k in context.UserInfoes
                                         where k.Role == SpectorMax.Entities.roles.Sub_Admin && k.IsDeleted == false
                                         select k.Role).Count();
                    ViewBag.TotalSubAdmin = TotalSubAdmin;

                    var TotalRegisterdInspector = (from k in context.UserInfoes
                                                   join ID in context.InspectorDetail on k.ID equals ID.UserID
                                                   where k.Role == SpectorMax.Entities.roles.Property_Inspector && k.IsDeleted == false
                                                   select k.Role).Count();
                    ViewBag.TotalRegisterdInspector = TotalRegisterdInspector;

                    var TotalInspectionReport = (from IO in context.tblinspectionorder
                                                 join IOD in context.tblinspectionorderdetail
                                                 on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                                 join UI in context.UserInfoes
                                                 on IO.InspectorID equals UI.ID into g
                                                 join req in context.UserInfoes on IO.RequesterID equals req.ID
                                                 join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                                 where IO.InspectionStatus == "Completed" && IO.IsDeleted == false
                                                 select new
                                                 {
                                                     IO.InspectionOrderDetailId
                                                 }).Count();
                    ViewBag.TotalInspectionReport = TotalInspectionReport;

                    var RejectedInspectionRequest = (from IO in context.tblinspectionorder
                                                     join IOD in context.tblinspectionorderdetail
                                                     on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                                     join UI in context.UserInfoes
                                                     on IO.InspectorID equals UI.ID into g
                                                     join req in context.UserInfoes on IO.RequesterID equals req.ID
                                                     join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                                     //where IO.Comment != "" && IO.Comment != null && IO.IsAcceptedOrRejected == false
                                                     where (IO.InspectorID == null || IO.InspectorID == "" || IO.IsRejectedByInspector == true) && IO.IsDeleted == false
                                                     select new
                                                     {
                                                         IO.InspectionOrderDetailId
                                                     }).Count();

                    ViewBag.RejectedInspectionRequest = RejectedInspectionRequest;

                    var Notifications = (from NO in context.NotificationDetail
                                         join NT in context.NotificationType on NO.NotificationTypeID equals NT.ID
                                         join UI in context.UserInfoes on NO.NotifcationFrom equals UI.ID
                                         where NO.IsRead == false
                                         select NO.ID
                                      ).Count();
                    ViewBag.Notifications = Notifications;
                    return View();
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region LogOn
        /// <summary>
        /// POST: // method for loging to registered user.
        /// </summary>
        /// <param name="logInModel"></param>
        /// <returns></returns>
        /// 


        [HttpPost] //JsonResult       
        public ActionResult LogOn(LogInModel logInModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    HttpCookie userDetails = new HttpCookie("UserDetails");
                    string message = string.Empty;
                    var result = context.UserInfoes.FirstOrDefault(x => x.EmailAddress == logInModel.Email && x.Password == logInModel.Password && x.IsActive == true && x.Role == "AA11573C-7688-4786-9");
                    if (result == null)
                    {
                        ViewBag.Message = "Invalid email or password";
                        _messageToShow.Message = "Invalid email or password";
                        return RedirectToAction("Home", "Admin", new { message = _messageToShow.Message });
                    }

                    else if (result.IsActive == false)
                    {
                        _messageToShow.Message = "Your account is inactive please contact to your admin.";
                        //return RedirectToAction("LogOn"); 
                        return RedirectToAction("Home", "Admin", new { message = _messageToShow.Message });
                    }
                    else
                    {
                        ///*On User First Login*/
                        //if (result.CurrentLoginDate == null && result.LastLoginDate == null)
                        //{
                        //    result.LastLoginDate = DateTime.Now;
                        //    result.CurrentLoginDate = DateTime.Now;
                        //}
                        ///*On Second Second login or when both date are same*/
                        //else if (result.CurrentLoginDate == result.LastLoginDate)
                        //{
                        //    result.CurrentLoginDate = DateTime.Now;
                        //}
                        ///*If Current Login Date Is Greater Then Last login Date*/
                        //else if (result.CurrentLoginDate > result.LastLoginDate)
                        //{
                        //    result.LastLoginDate = result.CurrentLoginDate;
                        //    result.CurrentLoginDate = DateTime.Now;
                        //}                       
                        //context.SaveChanges();


                        //FormsAuthentication.SetAuthCookie(logInModel.Email, true);
                        if (logInModel.RememberMe == true)
                        {
                            string userinfo = (Convert.ToString(logInModel.Email) + "~/!2@?<:>~" + Convert.ToString(logInModel.Password));
                            HttpCookie usercredentialsCookie = new HttpCookie("admincredentials");
                            usercredentialsCookie.Values.Add("meda", userinfo);
                            usercredentialsCookie.Expires = DateTime.Now.AddYears(1);
                            Response.Cookies.Add(usercredentialsCookie);
                        }
                        else
                        {
                            Response.Cookies["usercredentials"].Expires = DateTime.Now.AddYears(-1);
                        }

                        string userRoleName = context.Roles.FirstOrDefault(x => x.RoleID == result.Role).RoleName;
                        _messageToShow.Message = "Sucess";
                        _messageToShow.UserID = result.ID;
                        _messageToShow.UserRoleType = userRoleName;
                        LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails();
                        Permissions objPermission = new Permissions();
                        loggedInUserDetails.UserId = result.ID;
                        loggedInUserDetails.RoleId = result.Role;
                        loggedInUserDetails.RoleName = userRoleName;
                        loggedInUserDetails.FirstName = result.FirstName;
                        loggedInUserDetails.LastName = result.LastName;
                        loggedInUserDetails.EmailAddress = result.EmailAddress;
                        loggedInUserDetails.Zipcode = result.ZipCode;
                        loggedInUserDetails.rolecount = context.UserInfoes.Where(x => x.EmailAddress == logInModel.Email).ToList().Count;
                        permission userPermissions = new permission();
                        userPermissions = context.Permission.FirstOrDefault(x => x.UserID == result.ID);
                        objPermission.Cangetnoticeifinspectorhasnotresponded = (bool)userPermissions.Cangetnoticeifinspectorhasnotresponded;
                        objPermission.Canreceiveemail = (bool)userPermissions.Canreceiveemail;
                        objPermission.Canreview_WithinZipcodes_sales = (bool)userPermissions.Canreview_WithinZipcodes_sales;
                        objPermission.Canreviewallinspections = (bool)userPermissions.Canreviewallinspections;
                        objPermission.Canviewmoneyreceviedbycustomers = (bool)userPermissions.Canviewmoneyreceviedbycustomers;
                        objPermission.Canviewpaymenttransactions = (bool)userPermissions.Canviewpaymenttransactions;
                        objPermission.Canviewrequestforinspections = (bool)userPermissions.Canviewrequestforinspections;
                        loggedInUserDetails.AdminPermission = objPermission;
                        //Store the User's Details to a session
                        Session["UserDetails"] = loggedInUserDetails;
                        LoggedInUser.UserId = result.ID;
                        LoggedInUser.UserId = result.ID;
                        LoggedInUser.RoleId = result.Role;
                        LoggedInUser.RoleName = userRoleName;
                        LoggedInUser.FirstName = result.FirstName;
                        LoggedInUser.LastName = result.LastName;
                        LoggedInUser.EmailAddress = result.EmailAddress;
                        LoggedInUser.Zipcode = result.ZipCode;

                        if (logInModel.RememberMe == true)
                        {
                            userDetails["Email"] = logInModel.Email;
                            //userDetails["Password"] = logInModel.Password;
                            userDetails.Expires = DateTime.Now.AddDays(30);
                            Response.Cookies.Add(userDetails);
                        }
                        //else
                        //{
                        //    userDetails.Expires = DateTime.Now.AddDays(-1);
                        //    userDetails.Value = string.Empty;
                        //}

                        if (userRoleName == "Admin" || userRoleName == "Sub-Admin")
                        {
                            return RedirectToAction("AdminDashboard", "Admin", new { userid = result.ID });
                        }
                        else
                        {
                            _messageToShow.Message = "You are not assigned to any role. Please contact to your Administrator.";
                            return RedirectToAction("Home", "Admin", new { message = _messageToShow.Message });
                        }
                        // return Json(_messageToShow);

                    }

                }
                catch (Exception ex)
                {
                    _messageToShow.Message = ex.Message;
                    return RedirectToAction("Home", "Admin", new { message = _messageToShow.Message });
                }

            }
            else
            {
                return RedirectToAction("Home", "Admin");
            }
        }
        #endregion

        #region ForgotPassword
        /// <summary>
        /// GET: // method for Forgot Password.
        /// </summary>
        /// <returns></returns>

        public ActionResult ForgotPassword()
        {
            return View();
        }

        #endregion

        #region ForGotPassword-HttpPost
        /// <summary>
        /// POST: // method for Forgot Password.
        /// </summary>
        /// <param name="emailTo"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ForgotPassword(string emailTo)
        {
            EmailHelper email = new EmailHelper();
            string userpassword = context.UserInfoes.FirstOrDefault(x => x.EmailAddress == emailTo) != null ? context.UserInfoes.FirstOrDefault(x => x.EmailAddress == emailTo).Password.ToString() : "";
            if (userpassword != "")
            {
                string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "ForgotPassword").TemplateDiscription;
                emailBody = emailBody.Replace("{Password}", userpassword);

                bool status = email.SendEmail("SpectorMax", "ForgotPassword", "", emailBody, emailTo);
                if (status == true)
                    _messageToShow.Message = "Sucess";
                else
                    _messageToShow.Message = "Mail Send Failure";
            }
            else
            {
                _messageToShow.Message = "Wrong Email";
            }
            return Json(_messageToShow);
        }
        #endregion

        #region ViewAllInspectors
        /// <summary>
        /// View All Inspectors
        /// </summary>
        /// <param name="emailTo"></param>
        /// <returns></returns>
        /// 

        public ActionResult ViewAllInspectors()
        {
            return View();
        }
        #endregion

        #region LogOff
        /// <summary>
        /// method logout to logged in user.
        /// </summary>
        /// <returns></returns>

        public ActionResult LogOff()
        {
            //Session["UserDetails"] = null;
            Session.Abandon();
            return RedirectToAction("Home", "Admin");
        }
        #endregion

        #region Template
        [HttpPost]
        public JsonResult TemplateID(int TemplateID)
        {
            string templateHTML = context.EmailTemplates.FirstOrDefault(x => x.TemplateId == TemplateID).TemplateDiscription;
            return Json(templateHTML);
        }
        #endregion

        #region GetAllTemplates

        public ActionResult GetAllTemplates()
        {
            List<emailtemplate> objListOfEmailTemplates = context.EmailTemplates.ToList(); ;
            return View(objListOfEmailTemplates);
        }
        #endregion

        #region GetCommentLibraryMenu

        public ActionResult ManageCommentLibrary()
        {
            if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
            {
                IEnumerable<CommentLibrary> objCommentMenu = (from k in context.MainSections
                                                              join sub in context.SubSections on k.ID equals sub.SectionId
                                                              join item in context.CommentBox on sub.ID equals item.SubSectionId
                                                              select new CommentLibrary
                                                              {
                                                                  MainID = k.ID,
                                                                  SectionName = k.SectionName,
                                                                  SubSectionID = sub.ID,
                                                                  SubSectionName = sub.SubSectionName,
                                                                  ItemId = item.Id,
                                                                  CommentBoxName = item.CommentBoxName
                                                              }).ToList();

                return View(objCommentMenu);
            }
            else
            {
                return RedirectToAction("Home", "Admin");
            }
        }
        #endregion

        #region CreateInspector
        /// <summary>
        /// method create new inspector
        /// <param name="Id"></param>
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateInspector(string Id)
        {
            if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
            {
                #region Get Create Inspector View
                CreateInspector objInspectorDetail = new CreateInspector();
                if (Id == null)
                {
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "--Select--";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    objInspectorDetail.StatesList = items;
                    objInspectorDetail.ZipCodesList = objZipCodeList;
                    objInspectorDetail.CityList = objCityList;
                    return View(objInspectorDetail);
                }
                #endregion

                #region Get Edit Inspector View
                else
                {
                    userinfo objuserinfo = new userinfo();
                    ViewBag.Inspectorid = Id;
                    try
                    {
                        objuserinfo = context.UserInfoes.Where(x => x.ID == Id).FirstOrDefault();
                        if (objuserinfo != null)
                        {
                            objInspectorDetail.FirstName = objuserinfo.FirstName;
                            objInspectorDetail.LastName = objuserinfo.LastName;
                            objInspectorDetail.Address = objuserinfo.Address;
                            objInspectorDetail.Address2 = objuserinfo.AddressLine2;
                            objInspectorDetail.StateId = objuserinfo.StateId;
                            objInspectorDetail.StreetAddress = objuserinfo.StreetAddress;
                            objInspectorDetail.CityId = objuserinfo.CityId;
                            objInspectorDetail.Country = objuserinfo.Country;
                            objInspectorDetail.EmailAddress = objuserinfo.EmailAddress;
                            //objInspectorDetail.dateofbirth = objuserinfo.DateOfBirth.Value.Date;
                            if (objuserinfo.DateOfBirth != null)
                                objInspectorDetail.DOB = Convert.ToDateTime(objuserinfo.DateOfBirth).ToString("MM-yyyy");
                            else
                                objInspectorDetail.DOB = DateTime.Now.ToString("MM-yyyy");

                            objInspectorDetail.PhoneNo = objuserinfo.PhoneNo;
                            objInspectorDetail.Password = objuserinfo.Password;
                            objInspectorDetail.ConfirmPassword = objuserinfo.Password;
                            objInspectorDetail.UserID = objuserinfo.ID;

                            var inspectorDetail = context.InspectorDetail.FirstOrDefault(x => x.UserID == Id);
                            objInspectorDetail.IsShowOnHomePage = inspectorDetail.IsShowOnHomePage;
                            if (inspectorDetail != null)
                            {
                                objInspectorDetail.PhotoURL = inspectorDetail.PhotoURL;
                                objInspectorDetail.ExperienceTraining1 = inspectorDetail.ExperienceTraining1;
                                objInspectorDetail.ExperienceTraining2 = inspectorDetail.ExperienceTraining2;
                                objInspectorDetail.ExperienceTraining3 = inspectorDetail.ExperienceTraining3;
                                objInspectorDetail.Certification1 = inspectorDetail.Certification1;
                                objInspectorDetail.Certification2 = inspectorDetail.Certification2;
                                objInspectorDetail.Certification3 = inspectorDetail.Certification3;
                            }
                            else
                            {
                                objInspectorDetail.PhotoURL = "";
                                objInspectorDetail.ExperienceTraining1 = "";
                                objInspectorDetail.ExperienceTraining2 = "";
                                objInspectorDetail.ExperienceTraining3 = "";
                                objInspectorDetail.Certification1 = "";
                                objInspectorDetail.Certification2 = "";
                                objInspectorDetail.Certification3 = "";

                            }
                            List<NState> objState = new List<NState>();
                            objState = (from state in context.tblstate
                                        where state.IsActive == true
                                        select new NState
                                        {
                                            StateId = state.StateId,
                                            StateName = state.StateName
                                        }).OrderBy(x => x.StateName).ToList();
                            var CityList = (from k in context.tblcity
                                            where k.StateId == objuserinfo.StateId
                                            select new NCity
                                            {
                                                CityId = k.CityId,
                                                CityName = k.CityName
                                            }).OrderBy(x => x.CityName).ToList();
                            var ZipList = (from k in context.tblzip
                                           where k.CityId == objuserinfo.CityId
                                           select new NZip
                                           {
                                               Zip = k.Zip,
                                               ZipId = k.ZipId
                                           }).ToList();
                            List<SelectListItem> Stateitems = new List<SelectListItem>();
                            List<SelectListItem> Cityitems = new List<SelectListItem>();
                            List<SelectListItem> Zipitems = new List<SelectListItem>();
                            List<SelectListItem> objCityList = new List<SelectListItem>();
                            List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                            SelectListItem objSelectListItem = new SelectListItem();
                            objSelectListItem.Text = "--Select--";
                            objSelectListItem.Value = "--Select--";
                            //  Stateitems.Add(objSelectListItem);
                            objCityList.Add(objSelectListItem);
                            objZipCodeList.Add(objSelectListItem);
                            foreach (var t in objState)
                            {
                                SelectListItem s = new SelectListItem();
                                s.Text = t.StateName.ToString();
                                s.Value = t.StateId.ToString();
                                Stateitems.Add(s);
                            }
                            objInspectorDetail.StatesList = Stateitems;
                            foreach (var item in CityList)
                            {
                                SelectListItem s = new SelectListItem();
                                s.Text = item.CityName;
                                s.Value = item.CityId.ToString();
                                Cityitems.Add(s);
                            }
                            objInspectorDetail.CityList = Cityitems;
                            foreach (var item in ZipList)
                            {
                                SelectListItem s = new SelectListItem();
                                s.Text = item.Zip.ToString();
                                s.Value = item.ZipId.ToString();
                                Zipitems.Add(s);
                            }
                            objInspectorDetail.ZipCodesList = Zipitems;
                        }
                        return View(objInspectorDetail);

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                #endregion
                }
            }
            else
            {
                return RedirectToAction("Home", "Admin");
            }
        }
        #endregion

        #region CreateInspector-HttpPost
        /// <summary>
        /// method to save  new inspector--HttpPost
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateInspector(CreateInspector InspectorDetailsModel)
        {
            try
            {

                EmailHelper email = new EmailHelper();
                #region "Update Inspector details"
                if (InspectorDetailsModel.UserID != null)
                {
                    if (context.UserInfoes.FirstOrDefault(x => x.EmailAddress == InspectorDetailsModel.EmailAddress && x.ID != InspectorDetailsModel.UserID && x.IsDeleted == false) == null)
                    {

                        userinfo objuserinfo = new userinfo();
                        objuserinfo = context.UserInfoes.FirstOrDefault(x => x.ID == InspectorDetailsModel.UserID);
                        objuserinfo.FirstName = InspectorDetailsModel.FirstName;
                        objuserinfo.LastName = InspectorDetailsModel.LastName;
                        objuserinfo.Address = InspectorDetailsModel.Address;
                        objuserinfo.AddressLine2 = InspectorDetailsModel.Address2;
                        objuserinfo.StateId = InspectorDetailsModel.StateId;
                        objuserinfo.StreetAddress = InspectorDetailsModel.StreetAddress;
                        objuserinfo.CityId = InspectorDetailsModel.CityId;
                        objuserinfo.Country = InspectorDetailsModel.Country;
                        objuserinfo.EmailAddress = InspectorDetailsModel.EmailAddress;
                        objuserinfo.Password = InspectorDetailsModel.Password;

                        DateTime? dt = CreateDate(InspectorDetailsModel.DOB);
                        objuserinfo.DateOfBirth = dt;
                        objuserinfo.PhoneNo = InspectorDetailsModel.PhoneNo;
                        objuserinfo.ModifiedDate = DateTime.Now;
                        objuserinfo.ZipId = InspectorDetailsModel.Zipcode;
                        objuserinfo.Role = SpectorMax.Entities.roles.Property_Inspector;
                        objuserinfo.IsActive = true;
                        //objuserinfo.CreatedDate = DateTime.Now;
                        //objuserinfo.ModifiedDate = DateTime.Now;
                        objuserinfo.IsDeleted = false;
                        context.SaveChanges();

                        inspectordetail objInspectorDetail = new inspectordetail();
                        objInspectorDetail = context.InspectorDetail.FirstOrDefault(x => x.UserID == InspectorDetailsModel.UserID);
                        objInspectorDetail.IsShowOnHomePage = InspectorDetailsModel.IsShowOnHomePage;
                        objInspectorDetail.UserID = objuserinfo.ID;
                        string FileName = string.Empty;
                        if (InspectorDetailsModel.PhotoURL != null)
                        {
                            if (InspectorDetailsModel.PhotoURL.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(InspectorDetailsModel.PhotoURL.Split(',')[1]);
                                objInspectorDetail.PhotoURL = FileName;
                            }
                            else
                                objInspectorDetail.PhotoURL = InspectorDetailsModel.PhotoURL;
                        }
                        if (InspectorDetailsModel.Certification1 != null)
                        {
                            if (InspectorDetailsModel.Certification1.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(InspectorDetailsModel.Certification1.Split(',')[1]);
                                objInspectorDetail.Certification1 = FileName;
                            }
                            else
                                objInspectorDetail.Certification1 = objInspectorDetail.Certification1;
                        }

                        if (InspectorDetailsModel.Certification2 != null)
                        {
                            if (InspectorDetailsModel.Certification2.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(InspectorDetailsModel.Certification2.Split(',')[1]);
                                objInspectorDetail.Certification2 = FileName;
                            }
                            else
                                objInspectorDetail.Certification2 = objInspectorDetail.Certification2;
                        }

                        if (InspectorDetailsModel.Certification3 != null)
                        {

                            if (InspectorDetailsModel.Certification3.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(InspectorDetailsModel.Certification3.Split(',')[1]);
                                objInspectorDetail.Certification3 = FileName;
                            }
                            else
                                objInspectorDetail.Certification3 = objInspectorDetail.Certification3;
                        }
                        objInspectorDetail.ExperienceTraining1 = InspectorDetailsModel.ExperienceTraining1;
                        objInspectorDetail.ExperienceTraining2 = InspectorDetailsModel.ExperienceTraining2;
                        objInspectorDetail.ExperienceTraining3 = InspectorDetailsModel.ExperienceTraining3;

                        objInspectorDetail.CreatedDate = DateTime.Now;
                        objInspectorDetail.ModifiedDate = DateTime.Now;
                        objInspectorDetail.PaymentCycleEndDate = DateTime.Now;
                        objInspectorDetail.PaymentCycleStartDate = DateTime.Now;
                        objInspectorDetail.PaymentPercentage = 10;
                        context.SaveChanges();


                        string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "InpectorCreated").TemplateDiscription;
                        emailBody = emailBody.Replace("{Inspector Name}", InspectorDetailsModel.FirstName + " " + InspectorDetailsModel.LastName);
                        emailBody = emailBody.Replace("{User Name}", InspectorDetailsModel.EmailAddress);
                        emailBody = emailBody.Replace("{Password}", InspectorDetailsModel.Password);
                        bool status = email.SendEmail("", "Inspector Created", " ", emailBody, InspectorDetailsModel.EmailAddress);

                        ViewBag.Message = "Inspector Details Updated Successfully";

                        var States = context.ZipCode.Select(x => x.State).Distinct();
                        List<SelectListItem> items = new List<SelectListItem>();
                        List<SelectListItem> objCityList = new List<SelectListItem>();
                        List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                        SelectListItem objSelectListItem = new SelectListItem();
                        objSelectListItem.Text = "--Select--";
                        objSelectListItem.Value = "--Select--";
                        //  items.Add(objSelectListItem);
                        objCityList.Add(objSelectListItem);
                        objZipCodeList.Add(objSelectListItem);
                        foreach (var t in States)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = t.ToString();
                            s.Value = t.ToString();
                            items.Add(s);
                        }
                        items = items.OrderBy(x => x.Text).ToList();
                        items.Insert(0, objSelectListItem);
                        InspectorDetailsModel.StatesList = items;
                        InspectorDetailsModel.ZipCodesList = objZipCodeList;
                        InspectorDetailsModel.CityList = objCityList;
                        // return View(InspectorDetailsModel);
                        return Json("successUpdate", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Email Exist", JsonRequestBehavior.AllowGet);
                    }
                }

                #endregion
                #region Create Inspector Details
                else
                {
                    if (context.UserInfoes.FirstOrDefault(x => x.EmailAddress == InspectorDetailsModel.EmailAddress && x.IsDeleted == false) == null)
                    {
                        string InspectorNo = string.Empty;
                        int TotalInspectors = 0;
                        var count = from UI in context.UserInfoes
                                    where UI.State == InspectorDetailsModel.State && UI.Role == SpectorMax.Entities.roles.Property_Inspector && UI.IsDeleted == false
                                    select UI.ID;
                        if (count.Any())
                        {
                            TotalInspectors = count.Count();
                        }
                        if (TotalInspectors < 9)
                        {
                            InspectorNo = "00" + (TotalInspectors + 1).ToString();
                        }
                        else if (TotalInspectors > 9 && TotalInspectors < 99)
                        {
                            InspectorNo = "0" + (TotalInspectors + 1).ToString();
                        }
                        else
                        {
                            InspectorNo = (TotalInspectors + 1).ToString();
                        }

                        //string InspectorNo = (TotalInspectors + 1).ToString();
                        SpectorMaxDAL.userinfo objuserinfo = new userinfo();
                        objuserinfo.ID = Guid.NewGuid().ToString();
                        objuserinfo.FirstName = InspectorDetailsModel.FirstName;
                        objuserinfo.LastName = InspectorDetailsModel.LastName;
                        objuserinfo.Address = InspectorDetailsModel.Address;
                        objuserinfo.AddressLine2 = InspectorDetailsModel.Address2;
                        objuserinfo.StateId = InspectorDetailsModel.StateId;
                        objuserinfo.StreetAddress = InspectorDetailsModel.StreetAddress;
                        objuserinfo.CityId = InspectorDetailsModel.CityId;
                        objuserinfo.Country = InspectorDetailsModel.Country;
                        objuserinfo.EmailAddress = InspectorDetailsModel.EmailAddress;
                        objuserinfo.Password = InspectorDetailsModel.Password;
                        DateTime? dt = CreateDate(InspectorDetailsModel.DOB);
                        objuserinfo.DateOfBirth = dt;
                        objuserinfo.PhoneNo = InspectorDetailsModel.PhoneNo;
                        objuserinfo.ModifiedDate = DateTime.Now;
                        objuserinfo.ZipCode = InspectorDetailsModel.Zipcode;
                        objuserinfo.Role = SpectorMax.Entities.roles.Property_Inspector;
                        objuserinfo.IsActive = true;
                        objuserinfo.CreatedDate = DateTime.Now;
                        objuserinfo.ModifiedDate = DateTime.Now;
                        objuserinfo.IsDeleted = false;
                        context.UserInfoes.Add(objuserinfo);
                        context.SaveChanges();

                        /*Add inspector details in inspector details table*/
                        SpectorMaxDAL.inspectordetail objInspectorDetail = new inspectordetail();
                        objInspectorDetail.InspectorDetailsID = Guid.NewGuid().ToString();
                        objInspectorDetail.UserID = objuserinfo.ID;
                        objInspectorDetail.IsShowOnHomePage = InspectorDetailsModel.IsShowOnHomePage;
                        string FileName = string.Empty;
                        if (InspectorDetailsModel.PhotoURL != null)
                        {

                            FileName = ConvertToImage(InspectorDetailsModel.PhotoURL.Split(',')[1]);
                            objInspectorDetail.PhotoURL = FileName;
                        }
                        else
                        {
                            objInspectorDetail.PhotoURL = InspectorDetailsModel.PhotoURL;
                        }


                        if (InspectorDetailsModel.Certification1 != null)
                        {

                            FileName = ConvertToImage(InspectorDetailsModel.Certification1.Split(',')[1]);
                            objInspectorDetail.Certification1 = FileName;
                        }
                        else
                            objInspectorDetail.Certification1 = string.Empty;


                        if (InspectorDetailsModel.Certification2 != null)
                        {

                            FileName = ConvertToImage(InspectorDetailsModel.Certification2.Split(',')[1]);
                            objInspectorDetail.Certification2 = FileName;
                        }
                        else
                            objInspectorDetail.Certification2 = objInspectorDetail.Certification2;


                        if (InspectorDetailsModel.Certification3 != null)
                        {
                            FileName = ConvertToImage(InspectorDetailsModel.Certification3.Split(',')[1]);
                            objInspectorDetail.Certification3 = FileName;
                        }
                        else
                            objInspectorDetail.Certification3 = string.Empty;

                        objInspectorDetail.ExperienceTraining1 = InspectorDetailsModel.ExperienceTraining1;
                        objInspectorDetail.ExperienceTraining2 = InspectorDetailsModel.ExperienceTraining2;
                        objInspectorDetail.ExperienceTraining3 = InspectorDetailsModel.ExperienceTraining3;
                        objInspectorDetail.CreatedDate = DateTime.Now;
                        objInspectorDetail.ModifiedDate = DateTime.Now;
                        objInspectorDetail.PaymentCycleEndDate = DateTime.Now;
                        objInspectorDetail.PaymentCycleStartDate = DateTime.Now;
                        objInspectorDetail.PaymentPercentage = 10;
                        objInspectorDetail.InspectorNo = InspectorNo;
                        context.InspectorDetail.Add(objInspectorDetail);
                        context.SaveChanges();

                        string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "InpectorCreated").TemplateDiscription;
                        emailBody = emailBody.Replace("{Inspector Name}", InspectorDetailsModel.FirstName + " " + InspectorDetailsModel.LastName);
                        emailBody = emailBody.Replace("{User Name}", InspectorDetailsModel.EmailAddress);
                        emailBody = emailBody.Replace("{Password}", InspectorDetailsModel.Password);
                        bool status = email.SendEmail("", "Inspector Created", " ", emailBody, InspectorDetailsModel.EmailAddress);



                        ViewBag.Message = "Inspector Details Inserted Successfully";
                        List<NState> objState = new List<NState>();
                        objState = (from state in context.tblstate
                                    where state.IsActive == true
                                    select new NState
                                    {
                                        StateId = state.StateId,
                                        StateName = state.StateName
                                    }).OrderBy(x => x.StateName).ToList();
                        List<SelectListItem> items = new List<SelectListItem>();
                        List<SelectListItem> objCityList = new List<SelectListItem>();
                        List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                        SelectListItem objSelectListItem = new SelectListItem();
                        objSelectListItem.Text = "--Select--";
                        objSelectListItem.Value = "--Select--";
                        //  items.Add(objSelectListItem);
                        objCityList.Add(objSelectListItem);
                        objZipCodeList.Add(objSelectListItem);
                        foreach (var t in objState)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = t.StateName.ToString();
                            s.Value = t.StateId.ToString();
                            items.Add(s);
                        }
                        items = items.OrderBy(x => x.Text).ToList();
                        items.Insert(0, objSelectListItem);
                        InspectorDetailsModel.StatesList = items;
                        InspectorDetailsModel.ZipCodesList = objZipCodeList;
                        InspectorDetailsModel.CityList = objCityList;
                        //   return View(InspectorDetailsModel);
                        if (status == true)
                        {
                            return Json("successCreate", JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json("Email Not Sent", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("Email Exist", JsonRequestBehavior.AllowGet);
                    }
                }


            }
                #endregion

            catch (Exception ex)
            {
                throw ex;

            }
            return View();
        }

        private DateTime? CreateDate(string Dob)
        {

            DateTime dt = new DateTime(Convert.ToInt32(Dob.Split('-')[1]), Convert.ToInt32(Dob.Split('-')[0]), 1);
            return dt;
        }

        #endregion

        #region Convert Image To Base64
        public string ConvertToImage(string base64)
        {
            string FileName = string.Empty;

            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(base64)))
            {
                using (Bitmap bm2 = new Bitmap(ms))
                {
                    FileName = Guid.NewGuid().ToString() + ".jpg";
                    bm2.Save(Server.MapPath(@"~\Images\ProfileImgAndCertiticate\") + FileName);
                }
            }
            return FileName;
        }
        #endregion

        #region ManageInspector
        /// <summary>
        /// method to manage inspectors
        /// </summary>
        /// <returns></returns>

        public ActionResult ManageInspector()
        {

            if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
            {


                InspectorDetails objInspectorDetails = new InspectorDetails();
                List<InspectorDetails> lstobjInspectorDetails = new List<InspectorDetails>();
                //lstobjInspectorDetails = (from UI in context.UserInfoes
                //                          join IO in

                //                              (from val in context.InspectionOrders
                //                               group val by val.InspectorID into g
                //                               select new { ID = g.Key, test = g.Count() }
                //                          ) on UI.ID equals IO.ID into cls
                //                          from v in cls.DefaultIfEmpty()
                //                          join Roles in context.Roles on UI.Role equals Roles.RoleID
                //                          join zip in context.ZipCodeAssigned on UI.ID equals zip.InspectorID into joined

                //                          join Details in context.InspectorDetail on UI.ID equals Details.UserID
                //                          where Roles.RoleName == "Property Inspector" && UI.IsActive == true && UI.IsDeleted==false
                //                          orderby UI.FirstName 
                //                          select new InspectorDetails
                //                          {
                //                              InspectorDetailsID = Details.InspectorDetailsID,
                //                              FirstName = UI.FirstName,
                //                              LastName = UI.LastName,
                //                              EmailAddress = UI.EmailAddress,
                //                              UserID = UI.ID,
                //                              Zip = joined,
                //                              Count = v.test == null ? 0 : v.test
                //                          }).OrderBy(x => x.FirstName).ToList();

                lstobjInspectorDetails = (from UI in context.UserInfoes
                                          join Roles in context.Roles on UI.Role equals Roles.RoleID
                                          join Details in context.InspectorDetail on UI.ID equals Details.UserID
                                          //join zip in context.ZipCodeAssigned on UI.ID equals zip.InspectorID
                                          //join t in context.tblzip on zip.ZipCode equals t.ZipId into joined
                                          where Roles.RoleName == "Property Inspector" && UI.IsActive == true && UI.IsDeleted == false
                                          orderby UI.FirstName
                                          select new InspectorDetails
                                       {
                                           InspectorDetailsID = Details.InspectorDetailsID,
                                           FirstName = UI.FirstName,
                                           LastName = UI.LastName,
                                           EmailAddress = UI.EmailAddress,
                                           UserID = UI.ID,
                                           PL = (from z in context.ZipCodeAssigned
                                                 join tblz in context.tblzip on z.ZipCode equals tblz.ZipId
                                                 where z.InspectorID == UI.ID
                                                 select new ZipCodes
                                                 {
                                                     ZipID = tblz.ZipId,
                                                     Zip = tblz.Zip
                                                 }),
                                           //        Count = v.test == null ? 0 : v.test
                                       }).OrderBy(x => x.FirstName).ToList();
                objInspectorDetails.InspectorPayment = lstobjInspectorDetails;




                return View(objInspectorDetails);
            }
            else
            {
                return RedirectToAction("Home", "Admin");
            }

        }
        #endregion

        #region GetZipCodes
        /// <summary>
        /// method to ZipCodes of users
        /// <param name="id"></param>
        /// </summary>
        /// <returns></returns>

        public JsonResult GetZipCodes(string id)
        {
            var zipcodes = from Zip in context.ZipCodeAssigned
                           join tzip in context.tblzip on Zip.ZipCode equals tzip.ZipId
                           where Zip.InspectorID == id
                           select tzip;
            var ZipData = zipcodes.ToList().Select(m => new SelectListItem() { Text = m.Zip.ToString(), Value = m.ZipId.ToString() });
            return Json(ZipData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DeleteZipCodes
        /// <summary>
        /// method to DeleteZipCodes
        /// <param name="id"></param>
        /// <param name="InspectorDetailsid"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult DeleteZipCodes(string id, string InspectorDetailsid)
        {
            try
            {
                int[] Codes = Array.ConvertAll<string, int>(id.Split(','), Convert.ToInt32);
                var zipcode = from zip in context.ZipCodeAssigned
                              join tZip in context.tblzip on zip.ZipCode equals tZip.ZipId
                              where Codes.Contains(tZip.ZipId) && zip.InspectorID == InspectorDetailsid
                              select zip;
                foreach (var zip in zipcode)
                {
                    context.ZipCodeAssigned.Remove(zip);
                }
                context.SaveChanges();
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region AddZipCode
        /// <summary>
        /// method to AddZipCodes
        /// <param name="id"></param>
        /// <param name="InspectorDetailsid"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult AddZipCode(string id, string InspectorDetailsid)
        {
            try
            {

                int AssignZipCode = Convert.ToInt32(id);
                tblzip objtblzip = context.tblzip.Where(x => x.Zip == AssignZipCode).FirstOrDefault();
                if (objtblzip != null)
                {
                    var zipcode = from zip in context.ZipCodeAssigned
                                  where zip.ZipCode == objtblzip.ZipId && zip.InspectorID != InspectorDetailsid
                                  select zip;
                    if (zipcode.Any())
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        SpectorMaxDAL.zipcodeassigned objZipCode = new zipcodeassigned();
                        objZipCode.ZipcodeID = Guid.NewGuid().ToString();
                        objZipCode.ZipCode = objtblzip.ZipId;
                        objZipCode.InspectorID = InspectorDetailsid;
                        context.ZipCodeAssigned.Add(objZipCode);
                        context.SaveChanges();
                        return Json("Success&" + objZipCode.ZipcodeID + "&" + objtblzip.Zip, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("NotExist", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region CheckEmail
        /// <summary>
        /// Check Email Of Inspector
        /// <param name="EmailAddress">
        /// <param name="Userid"></param></param>
        /// </summary>
        /// <returns></returns>

        public JsonResult CheckEmail(string EmailAddress, string Userid)
        {

            var email = from user in context.UserInfoes
                        where user.EmailAddress == EmailAddress && user.ID != Userid
                        select user.EmailAddress;
            if (email.Any())
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("success", JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region CheckZipcode
        /// <summary>
        /// Check Zipcode Of Inspector
        /// <param name="Userid"></param>
        /// <param name="Zipcode"></param>
        /// </summary>
        /// <returns></returns>

        public JsonResult CheckZipcode(string Zipcode, string Userid)
        {
            int zip = Convert.ToInt32(Zipcode);

            var InspectorZip = from zipcodes in context.ZipCodeAssigned
                               join InspectorInfo in context.InspectorDetail on zipcodes.InspectorID equals InspectorInfo.InspectorDetailsID
                               join user in context.UserInfoes on InspectorInfo.UserID equals user.ID
                               where zipcodes.ZipCode == (int?)zip && InspectorInfo.UserID != Userid
                               select zipcodes.ZipCode;
            if (InspectorZip.Any())
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("success", JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region ManagePlans
        /// <summary>
        /// Method to manage subscription plans
        /// </summary>
        /// <returns></returns>
        public ActionResult ManagePlans()
        {
            if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
            {
                try
                {
                    int[] template = { 31, 37, 17 };

                    IEnumerable<emailtemplate> objListOfEmailTemplates = from emailtemplate in context.EmailTemplates
                                                                         where template.Contains(emailtemplate.TemplateId) && emailtemplate.IsActive == true
                                                                         select emailtemplate;

                    ViewBag.Description = objListOfEmailTemplates.FirstOrDefault().TemplateDiscription;
                    ViewBag.ListBox = objListOfEmailTemplates;
                    return View(objListOfEmailTemplates.FirstOrDefault());

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return RedirectToAction("Home", "Admin");
            }
        }
        #endregion

        #region ManagePlans-HttpPost
        /// <summary>
        /// Method to update plans
        /// <param name="EmailTemplateModel"></param>
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ManagePlans(emailtemplate EmailTemplateModel)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    emailtemplate objEmailTemplate = context.EmailTemplates.FirstOrDefault(x => x.TemplateId == EmailTemplateModel.TemplateId);
                    objEmailTemplate.TemplateDiscription = EmailTemplateModel.TemplateDiscription;
                    objEmailTemplate.Validity = EmailTemplateModel.Validity;
                    objEmailTemplate.Rate = EmailTemplateModel.Rate;
                    context.SaveChanges();
                    ViewBag.Message = "Plan Template Updated Successfully";

                    int[] template = { 2, 3, 4, 17, 18, 31, 37 };

                    IEnumerable<emailtemplate> objListOfEmailTemplates = from emailtemplate in context.EmailTemplates
                                                                         where template.Contains(emailtemplate.TemplateId)
                                                                         select emailtemplate;


                    ViewBag.ListBox = objListOfEmailTemplates;

                    return View(objEmailTemplate);
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetTemplateData
        /// <summary>
        /// Get Template Data
        /// <param name="TemplateId"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult GetTemplateData(string TemplateId)
        {
            try
            {

                int id = Convert.ToInt32(TemplateId);
                var templateHTML = context.EmailTemplates.FirstOrDefault(x => x.TemplateId == id);
                return Json(templateHTML, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ManageTemplates
        /// <summary>
        /// Method to manage Template
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageTemplates()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    int[] template = { 1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 51, 30, 32, 33, 34, 35, 36, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 53, 54, 55 };

                    List<emailtemplate> objListOfEmailTemplates = (from emailtemplate in context.EmailTemplates
                                                                   where template.Contains(emailtemplate.TemplateId) && emailtemplate.IsActive == true
                                                                   select emailtemplate).OrderBy(x => x.TemplateName).ToList();
                    ViewBag.Description = objListOfEmailTemplates.FirstOrDefault().TemplateDiscription;
                    ViewBag.ListBox = objListOfEmailTemplates;
                    return View(objListOfEmailTemplates.FirstOrDefault());
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ManageSubadmins
        /// <summary>
        /// Method to create Subadmins
        /// <param name="Id"></param>
        /// </summary>
        /// <returns></returns>

        public ActionResult ManageSubadmins(string Id)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    UserInfoPoco objUserInfo = new UserInfoPoco();
                    Permissions Permissions = new Permissions();
                    if (Id != null)
                    {
                        userinfo objuser = new userinfo();
                        permission objPermission = new permission();
                        objPermission = context.Permission.FirstOrDefault(x => x.UserID == Id);
                        objuser = context.UserInfoes.FirstOrDefault(x => x.ID == Id);
                        objUserInfo.FirstName = objuser.FirstName;
                        objUserInfo.LastName = objuser.LastName;
                        objUserInfo.PhoneNo = objuser.PhoneNo;
                        objUserInfo.State = objuser.State;
                        objUserInfo.StreetAddress = objuser.StreetAddress;
                        objUserInfo.Address = objuser.Address;
                        objUserInfo.AddressLine2 = objuser.AddressLine2;
                        objUserInfo.City = objuser.City;
                        objUserInfo.Country = objuser.Country;
                        objUserInfo.DOB = Convert.ToDateTime(objuser.DateOfBirth).ToString("MM-yyyy");
                        objUserInfo.DateOfBirth = objuser.DateOfBirth;
                        objUserInfo.EmailAddress = objuser.EmailAddress;
                        objUserInfo.Password = objuser.Password;
                        objUserInfo.ConfirmPassword = objuser.Password;
                        objUserInfo.ID = objuser.ID;
                        if (objPermission != null)
                        {
                            Permissions.Cangetnoticeifinspectorhasnotresponded = (bool)objPermission.Cangetnoticeifinspectorhasnotresponded;
                            Permissions.Canreceiveemail = (bool)objPermission.Canreceiveemail;
                            Permissions.Canreview_WithinZipcodes_sales = (bool)objPermission.Canreview_WithinZipcodes_sales;
                            Permissions.Canreviewallinspections = (bool)objPermission.Canreviewallinspections;
                            Permissions.Canviewmoneyreceviedbycustomers = (bool)objPermission.Canviewmoneyreceviedbycustomers;
                            Permissions.Canviewpaymenttransactions = (bool)objPermission.Canviewpaymenttransactions;
                            Permissions.Canviewrequestforinspections = (bool)objPermission.Canviewrequestforinspections;
                        }
                        objUserInfo.Permission = Permissions;
                        List<NState> objState = new List<NState>();
                        objState = (from state in context.tblstate
                                    where state.IsActive == true
                                    select new NState
                                    {
                                        StateId = state.StateId,
                                        StateName = state.StateName
                                    }).OrderBy(x => x.StateName).ToList();
                        var CityList = (from k in context.tblcity
                                        where k.StateId == objUserInfo.StateId
                                        select new NCity
                                        {
                                            CityId = k.CityId,
                                            CityName = k.CityName
                                        }).ToList();
                        var ZipList = (from k in context.tblzip
                                       where k.CityId == objUserInfo.CityId
                                       select new NZip
                                       {
                                           Zip = k.Zip,
                                           ZipId = k.ZipId
                                       }).ToList();
                        List<SelectListItem> Stateitems = new List<SelectListItem>();
                        List<SelectListItem> Cityitems = new List<SelectListItem>();
                        List<SelectListItem> Zipitems = new List<SelectListItem>();
                        List<SelectListItem> objCityList = new List<SelectListItem>();
                        List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                        SelectListItem objSelectListItem = new SelectListItem();
                        objSelectListItem.Text = "--Select--";
                        objSelectListItem.Value = "--Select--";
                        //  Stateitems.Add(objSelectListItem);
                        objCityList.Add(objSelectListItem);
                        objZipCodeList.Add(objSelectListItem);
                        foreach (var t in objState)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = t.StateName.ToString();
                            s.Value = t.StateId.ToString();
                            Stateitems.Add(s);
                        }
                        objUserInfo.StatesList = Stateitems;
                        foreach (var item in CityList)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = item.CityName;
                            s.Value = item.CityId.ToString();
                            Cityitems.Add(s);
                        }
                        objUserInfo.CityList = Cityitems;
                        foreach (var item in ZipList)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = item.Zip.ToString();
                            s.Value = item.ZipId.ToString();
                            Zipitems.Add(s);
                        }
                        objUserInfo.ZipCodesList = Zipitems;
                        return View(objUserInfo);

                    }
                    else
                    {
                        List<NState> objState = new List<NState>();
                        objState = (from state in context.tblstate
                                    where state.IsActive == true
                                    select new NState
                                    {
                                        StateId = state.StateId,
                                        StateName = state.StateName
                                    }).OrderBy(x => x.StateName).ToList();
                        List<SelectListItem> items = new List<SelectListItem>();
                        List<SelectListItem> objCityList = new List<SelectListItem>();
                        List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                        SelectListItem objSelectListItem = new SelectListItem();
                        objSelectListItem.Text = "--Select--";
                        objSelectListItem.Value = "--Select--";
                        //  items.Add(objSelectListItem);
                        objCityList.Add(objSelectListItem);
                        objZipCodeList.Add(objSelectListItem);
                        foreach (var t in objState)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = t.StateName.ToString();
                            s.Value = t.StateId.ToString();
                            items.Add(s);
                        }
                        items = items.OrderBy(x => x.Text).ToList();
                        items.Insert(0, objSelectListItem);
                        objUserInfo.StatesList = items;
                        objUserInfo.ZipCodesList = objZipCodeList;
                        objUserInfo.CityList = objCityList;
                        return View(objUserInfo);
                    }

                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ManageSubadmins-Post
        /// <summary>
        /// Method to save Subadmins[post]
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageSubadmins(UserInfoPoco userInfoPoco)
        {

            try
            {
                if (userInfoPoco.ID == null)
                {
                    if (context.UserInfoes.FirstOrDefault(x => x.EmailAddress == userInfoPoco.EmailAddress && x.IsDeleted == false) == null)
                    {
                        var SubadminRole = context.Roles.FirstOrDefault(x => x.RoleName == "Sub-Admin").RoleID;
                        userinfo userInfo = new userinfo();
                        userInfo.ID = Guid.NewGuid().ToString();
                        userInfo.Address = userInfoPoco.Address;
                        userInfo.AddressLine2 = userInfoPoco.AddressLine2;
                        userInfo.CityId = userInfoPoco.CityId;
                        userInfo.Country = userInfoPoco.Country;
                        userInfo.CreatedDate = DateTime.Now;
                        userInfo.ModifiedDate = DateTime.Now;
                        DateTime? dt = CreateDate(userInfoPoco.DOB);
                        userInfo.DateOfBirth = dt;
                        //userInfo.DateOfBirth = userInfoPoco.DateOfBirth;
                        userInfo.EmailAddress = userInfoPoco.EmailAddress;
                        userInfo.FirstName = userInfoPoco.FirstName;
                        userInfo.LastName = userInfoPoco.LastName;
                        userInfo.ModifiedDate = userInfoPoco.ModifiedDate;
                        userInfo.Password = userInfoPoco.Password;
                        userInfo.PhoneNo = userInfoPoco.PhoneNo;
                        userInfo.ZipCode = userInfoPoco.ZipCode;
                        userInfo.StateId = userInfoPoco.StateId;
                        userInfo.Country = userInfoPoco.Country;
                        userInfo.StreetAddress = userInfoPoco.StreetAddress;
                        userInfo.Role = SubadminRole;
                        userInfo.IsActive = true;
                        userInfo.IsDeleted = false;
                        context.UserInfoes.Add(userInfo);
                        context.SaveChanges();

                        permission userPermissions = new permission();
                        userPermissions.PermissionID = Guid.NewGuid().ToString();
                        userPermissions.RoleID = SubadminRole;
                        userPermissions.UserID = userInfo.ID;
                        userPermissions.Cangetnoticeifinspectorhasnotresponded = userInfoPoco.Permission.Cangetnoticeifinspectorhasnotresponded;
                        userPermissions.Canreceiveemail = userInfoPoco.Permission.Canreceiveemail;
                        userPermissions.Canreview_WithinZipcodes_sales = userInfoPoco.Permission.Canreview_WithinZipcodes_sales;
                        userPermissions.Canreviewallinspections = userInfoPoco.Permission.Canreviewallinspections;
                        userPermissions.Canviewmoneyreceviedbycustomers = userInfoPoco.Permission.Canviewmoneyreceviedbycustomers;
                        userPermissions.Canviewpaymenttransactions = userInfoPoco.Permission.Canviewpaymenttransactions;
                        userPermissions.Canviewrequestforinspections = userInfoPoco.Permission.Canviewrequestforinspections;
                        context.Permission.Add(userPermissions);
                        context.SaveChanges();
                        UserInfoPoco objUserinfo = new UserInfoPoco();
                        List<NState> objState = new List<NState>();
                        objState = (from state in context.tblstate
                                    where state.IsActive == true
                                    select new NState
                                    {
                                        StateId = state.StateId,
                                        StateName = state.StateName
                                    }).OrderBy(x => x.StateName).ToList();
                        List<SelectListItem> items = new List<SelectListItem>();
                        List<SelectListItem> objCityList = new List<SelectListItem>();
                        List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                        SelectListItem objSelectListItem = new SelectListItem();
                        objSelectListItem.Text = "--Select--";
                        objSelectListItem.Value = "--Select--";
                        //  items.Add(objSelectListItem);
                        objCityList.Add(objSelectListItem);
                        objZipCodeList.Add(objSelectListItem);
                        foreach (var t in objState)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = t.StateName.ToString();
                            s.Value = t.StateId.ToString();
                            items.Add(s);
                        }
                        items = items.OrderBy(x => x.Text).ToList();
                        items.Insert(0, objSelectListItem);
                        objUserinfo.StatesList = items;
                        objUserinfo.ZipCodesList = objZipCodeList;
                        objUserinfo.CityList = objCityList;
                        return View(objUserinfo);
                    }
                    else
                    {
                        return Json("Email Id Alredy exist", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (context.UserInfoes.FirstOrDefault(x => x.EmailAddress == userInfoPoco.EmailAddress && x.ID != userInfoPoco.ID && x.IsDeleted == false) == null)
                    {
                        userinfo userInfo = new userinfo();
                        userInfo = context.UserInfoes.FirstOrDefault(x => x.ID == userInfoPoco.ID);
                        userInfo.Address = userInfoPoco.Address;
                        userInfo.AddressLine2 = userInfoPoco.AddressLine2;
                        userInfo.City = userInfoPoco.City;
                        userInfo.Country = userInfoPoco.Country;
                        userInfo.CreatedDate = DateTime.Now;
                        userInfo.ModifiedDate = DateTime.Now;
                        DateTime? dt = CreateDate(userInfoPoco.DOB);
                        userInfo.DateOfBirth = dt;
                        userInfo.EmailAddress = userInfoPoco.EmailAddress;
                        userInfo.FirstName = userInfoPoco.FirstName;
                        userInfo.LastName = userInfoPoco.LastName;
                        userInfo.ModifiedDate = userInfoPoco.ModifiedDate;
                        userInfo.Password = userInfoPoco.Password;
                        userInfo.PhoneNo = userInfoPoco.PhoneNo;
                        userInfo.ZipCode = userInfoPoco.ZipCode;
                        userInfo.State = userInfoPoco.State;
                        userInfo.Country = userInfoPoco.Country;
                        userInfo.StreetAddress = userInfoPoco.StreetAddress;
                        userInfo.IsActive = true;
                        context.SaveChanges();
                        permission userPermissions = new permission();
                        userPermissions = context.Permission.FirstOrDefault(x => x.UserID == userInfoPoco.ID);
                        userPermissions.Cangetnoticeifinspectorhasnotresponded = userInfoPoco.Permission.Cangetnoticeifinspectorhasnotresponded;
                        userPermissions.Canreceiveemail = userInfoPoco.Permission.Canreceiveemail;
                        userPermissions.Canreview_WithinZipcodes_sales = userInfoPoco.Permission.Canreview_WithinZipcodes_sales;
                        userPermissions.Canreviewallinspections = userInfoPoco.Permission.Canreviewallinspections;
                        userPermissions.Canviewmoneyreceviedbycustomers = userInfoPoco.Permission.Canviewmoneyreceviedbycustomers;
                        userPermissions.Canviewpaymenttransactions = userInfoPoco.Permission.Canviewpaymenttransactions;
                        userPermissions.Canviewrequestforinspections = userInfoPoco.Permission.Canviewrequestforinspections;
                        context.SaveChanges();
                        UserInfoPoco objUserinfo = new UserInfoPoco();
                        List<NState> objState = new List<NState>();
                        objState = (from state in context.tblstate
                                    where state.IsActive == true
                                    select new NState
                                    {
                                        StateId = state.StateId,
                                        StateName = state.StateName
                                    }).OrderBy(x => x.StateName).ToList();
                        List<SelectListItem> items = new List<SelectListItem>();
                        List<SelectListItem> objCityList = new List<SelectListItem>();
                        List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                        SelectListItem objSelectListItem = new SelectListItem();
                        objSelectListItem.Text = "--Select--";
                        objSelectListItem.Value = "--Select--";
                        //  items.Add(objSelectListItem);
                        objCityList.Add(objSelectListItem);
                        objZipCodeList.Add(objSelectListItem);
                        foreach (var t in objState)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = t.StateName.ToString();
                            s.Value = t.StateId.ToString();
                            items.Add(s);
                        }
                        items = items.OrderBy(x => x.Text).ToList();
                        items.Insert(0, objSelectListItem);
                        objUserinfo.StatesList = items;
                        objUserinfo.ZipCodesList = objZipCodeList;
                        objUserinfo.CityList = objCityList;
                        return View(objUserinfo);
                    }
                    else
                    {
                        return Json("Email Id Alredy exist", JsonRequestBehavior.AllowGet);
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ListAllSubadmins
        /// <summary>
        /// Method to List all subadmind
        /// </summary>
        /// <returns></returns>
        public ActionResult ListAllSubadmins()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    SpectorMax.Entities.Classes.UserInfoPoco objInspectorDetails = new UserInfoPoco();
                    List<UserInfoPoco> objInspectorData = new List<UserInfoPoco>();
                    objInspectorData = (from Details in context.UserInfoes
                                        join UsrRole in context.Roles on Details.Role equals UsrRole.RoleID
                                        where UsrRole.RoleName == "Sub-Admin" && Details.IsDeleted == false
                                        select new UserInfoPoco
                                        {
                                            FirstName = Details.FirstName,
                                            LastName = Details.LastName,
                                            EmailAddress = Details.EmailAddress,
                                            PhoneNo = Details.PhoneNo,
                                            ID = Details.ID,
                                        }).ToList();

                    objInspectorDetails.SubadminDetails = objInspectorData;

                    List<SpectorMax.Entities.Classes.State> stateList = (from x in context.State
                                                                         select new SpectorMax.Entities.Classes.State
                                                                         {
                                                                             ID = x.ID,
                                                                             StateName = x.StateName
                                                                         }).OrderBy(w => w.StateName).ToList();
                    ViewBag.StateList = new SelectList(stateList, "ID", "StateName");

                    return View(objInspectorDetails);
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetAssignedStates
        /// <summary>
        /// Method to get all GetAssignedStates
        /// <param name="id"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAssignedStates(string id)
        {
            try
            {
                var states = from stateAssigned in context.StatesAssigned
                             join UsrState in context.State on stateAssigned.StateID equals UsrState.ID
                             where stateAssigned.SubAdminID == id
                             select new SpectorMax.Entities.Classes.State
                             {
                                 ID = UsrState.ID,
                                 StateName = UsrState.StateName
                             };
                var Statedata = states.ToList().Select(m => new SelectListItem() { Text = m.ID.ToString(), Value = m.StateName });
                return Json(Statedata, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region DeleteState
        /// <summary>
        /// method to DeleteState
        /// <param name="id"></param>
        /// <param name="SubAdminId"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult DeleteState(string id, string SubAdminId)
        {
            try
            {
                string[] IdArray = id.Split(',');
                int iStateId;
                statesassigned stateAssigned = new statesassigned();
                for (int i = 0; i < IdArray.Length; i++)
                {
                    iStateId = Convert.ToInt32(IdArray[i]);
                    stateAssigned = context.StatesAssigned.FirstOrDefault(x => x.StateID == iStateId && x.SubAdminID == SubAdminId);
                    context.StatesAssigned.Remove(stateAssigned);
                    context.SaveChanges();
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region AssignStates
        /// <summary>
        /// method to AssignStates
        /// <param name="id"></param>
        /// <param name="SubAdminId"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult AssignStates(string id, string SubAdminId)
        {
            try
            {
                int StateId = Convert.ToInt32(id);
                var AssignedState = from StateAssigned in context.StatesAssigned
                                    where StateAssigned.ID == StateId && StateAssigned.SubAdminID != SubAdminId
                                    select StateAssigned;
                if (AssignedState.Any())
                {
                    return Json("State already assigned to some one else", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    SpectorMaxDAL.statesassigned objStateAssigned = new statesassigned();

                    objStateAssigned.StateID = StateId;
                    objStateAssigned.SubAdminID = SubAdminId;
                    context.StatesAssigned.Add(objStateAssigned);
                    context.SaveChanges();
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region ManageUsers
        /// <summary>
        /// method to ManageUsers
        /// <param name="id"></param>
        /// <param name="SubAdminId"></param>
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageUsers()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    SpectorMax.Entities.Classes.UserInfoPoco objInspectorDetails = new UserInfoPoco();
                    List<UserInfoPoco> objInspectorData = new List<UserInfoPoco>();
                    objInspectorData = (from Details in context.UserInfoes
                                        join UsrRole in context.Roles on Details.Role equals UsrRole.RoleID
                                        where (UsrRole.RoleName == "SiteUser") && Details.IsDeleted == false
                                        select new UserInfoPoco
                                        {
                                            FirstName = Details.FirstName,
                                            LastName = Details.LastName,
                                            EmailAddress = Details.EmailAddress,
                                            PhoneNo = Details.PhoneNo,
                                            ID = Details.ID,
                                        }).ToList();

                    objInspectorDetails.SubadminDetails = objInspectorData;
                    return View(objInspectorDetails);
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region BlockHomeOwner
        /// <summary>
        /// method to BlockHomeOwner
        /// <param name="id"></param>
        /// </summary>
        /// <returns></returns>
        public ActionResult BlockHomeOwner(string Id)
        {
            try
            {
                BlockHomeOwner objUserInfo = new BlockHomeOwner();
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    BlockHomeOwner objuser = new BlockHomeOwner();
                    objuser = (from user in context.UserInfoes
                               where user.ID == Id
                               select new BlockHomeOwner
                               {
                                   ID = user.ID,
                                   FirstName = user.FirstName,
                                   LastName = user.LastName,
                                   EmailAddress = user.EmailAddress,
                                   IsActive = user.IsActive,
                                   PhoneNo = user.PhoneNo
                               }).FirstOrDefault();
                    objUserInfo.FirstName = objuser.FirstName;
                    objUserInfo.LastName = objuser.LastName;
                    objUserInfo.PhoneNo = objuser.PhoneNo;
                    objUserInfo.EmailAddress = objuser.EmailAddress;
                    objUserInfo.ID = objuser.ID;
                    objUserInfo.IsActive = objuser.IsActive;
                    ViewData["BlockUser"] = null;
                    return View(objUserInfo);
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region BlockHomeOwner-HttpPost
        /// <summary>
        /// method to BlockHomeOwner
        /// <param name="UserinfoModel"></param>
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BlockHomeOwner(BlockHomeOwner UserinfoModel)
        {
            try
            {
                userinfo userInfo = new userinfo();
                userInfo = context.UserInfoes.FirstOrDefault(x => x.ID == UserinfoModel.ID);
                if (userInfo.IsActive == true)
                    userInfo.IsActive = false;
                else
                    userInfo.IsActive = true;
                context.SaveChanges();
                //  userInfo = context.UserInfoes.FirstOrDefault(x => x.ID == UserinfoModel.ID);
                BlockHomeOwner objuserInfo = new BlockHomeOwner();
                objuserInfo.IsActive = userInfo.IsActive;
                objuserInfo.FirstName = objuserInfo.FirstName;
                objuserInfo.LastName = objuserInfo.LastName;
                objuserInfo.EmailAddress = objuserInfo.EmailAddress;
                objuserInfo.PhoneNo = objuserInfo.PhoneNo;
                if (userInfo.IsActive)
                    ViewData["BlockUser"] = "User Unblocked Successfully";
                else
                    ViewData["BlockUser"] = "User Blocked Successfully";
                return View(objuserInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public ActionResult Admin_PaymentReport()
        {
            try
            {

                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    InspectorReport objInspectorReport = new InspectorReport();
                    List<Report> objListOfInspectorPaymentReports = new List<Report>();

                    objListOfInspectorPaymentReports = (from IO in context.tblinspectionorder
                                                        join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                                        join Pay in context.Payments on IO.InspectionOrderDetailId equals Pay.ReportID
                                                        join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                                        join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                                        join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                                        join s in context.tblstate on IOD.PropertyState equals s.StateId
                                                        join c in context.tblcity on IOD.PropertyCity equals c.CityId
                                                        join z in context.tblzip on IOD.PropertyZip equals z.ZipId

                                                        select new Report
                                                        {
                                                            OwnerFirstName = IOD.RequesterFName,
                                                            OwnerLastName = IOD.RequesterLName,
                                                            StreetAddress = IOD.PropertyAddress,
                                                            City = c.CityName,
                                                            State = s.StateName,
                                                            InspectionDate = IO.ScheduledDate,
                                                            Zip = z.Zip,
                                                            /*Get Role Name*/
                                                            AmountReceived = Pay.PaymentAmount,
                                                            PaymentPlan = Pay.PaymentType,
                                                            date = Pay.PaymentDate,
                                                            ReportNo = IO.ReportNo,
                                                            InvoiceNo = Pay.InvoiceNo,
                                                            PaymentId = Pay.PaymentID,
                                                            IsRefunded = Pay.IsDeleted,
                                                            /*Inspector Details*/
                                                            InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                                            InspectorPercentage = InsDtls.PaymentPercentage
                                                        }).ToList();

                    objInspectorReport.ReportData = objListOfInspectorPaymentReports;

                    var TotalAmount = (from IO in context.tblinspectionorder
                                       join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                       join Pay in context.Payments on IO.InspectionOrderDetailId equals Pay.ReportID
                                       join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                       join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                       join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                       join s in context.tblstate on IOD.PropertyState equals s.StateId
                                       join c in context.tblcity on IOD.PropertyCity equals c.CityId
                                       join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                       where Pay.IsDeleted == false || Pay.IsDeleted == null
                                       select new
                                       {
                                           Amount = Pay.PaymentAmount
                                       }).Sum(x => x.Amount);




                    var TotalRefundedAmount = (from IO in context.tblinspectionorder
                                               join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                               join Pay in context.Payments on IO.InspectionOrderDetailId equals Pay.ReportID
                                               join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                               join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                               join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                               join s in context.tblstate on IOD.PropertyState equals s.StateId
                                               join c in context.tblcity on IOD.PropertyCity equals c.CityId
                                               join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                               where Pay.IsDeleted == true
                                               select new
                                               {
                                                   Amount = Pay.PaymentAmount
                                               }).Sum(x => x.Amount);

                    ViewBag.NonRefundedAmount = TotalAmount;
                    ViewBag.RefundedAmount = TotalRefundedAmount;

                    return View(objInspectorReport);
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public JsonResult FilterByDate(string InspectorName, string startDate, string endDate, string searchCriteria, string searchby)
        {
            try
            {


                DateTime? _startdate = null;
                DateTime? _enddate = null;
                if (!String.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) != null)
                {
                    if (endDate == "" || endDate == null)
                        _enddate = DateTime.Now;
                    else
                        _enddate = DateTime.Parse(endDate).Date.AddHours(23);
                    _startdate = DateTime.Parse(startDate).Date;

                }

                InspectorReport objInspectorReport = new InspectorReport();
                List<Report> objReportTotalSales = new List<Report>();
                Dates obj = new Dates();
                obj.fromdate = startDate;
                obj.todate = endDate;
                objInspectorReport.fromandtodate = obj;
                if (searchCriteria == "No Criteria" && searchby == "No Search" && (startDate != null || startDate != ""))
                {
                    objReportTotalSales = (from IO in context.InspectionOrders
                                           join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                           join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                           join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                           join rol in context.Roles on UI.Role equals rol.RoleID
                                           join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                           join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                           where Pay.PaymentDate >= _startdate && Pay.PaymentDate <= _enddate
                                           select new Report
                                           {
                                               OwnerFirstName = IOD.OwnerFirstName,
                                               OwnerLastName = IOD.OwnerLastName,
                                               StreetAddress = IOD.PropertyAddress,
                                               City = IOD.PropertyAddressCity,
                                               State = IOD.PropertyAddressState,
                                               InspectionDate = IO.CreatedDate,
                                               Zip = IOD.Zip,
                                               /*Get Role Name*/
                                               roleName = rol.RoleName,
                                               AmountReceived = Pay.PaymentAmount,
                                               PaymentPlan = Pay.PaymentType,
                                               date = Pay.PaymentDate,
                                               ReportNo = IO.UniqueID,
                                               /*Inspector Details*/
                                               InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                               InspectorPercentage = InsDtls.PaymentPercentage
                                           }).ToList();
                }
                else if (searchCriteria != "No Criteria" && (startDate != null && startDate != ""))
                {
                    if (searchCriteria == "Inspector")
                    {
                        objReportTotalSales = (from IO in context.InspectionOrders
                                               join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                               join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                               join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                               join rol in context.Roles on UI.Role equals rol.RoleID
                                               join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                               join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                               where IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate && UInfo.FirstName == searchby
                                               select new Report
                                               {
                                                   OwnerFirstName = IOD.OwnerFirstName,
                                                   OwnerLastName = IOD.OwnerLastName,
                                                   StreetAddress = IOD.PropertyAddress,
                                                   City = IOD.PropertyAddressCity,
                                                   State = IOD.PropertyAddressState,
                                                   InspectionDate = IO.CreatedDate,
                                                   Zip = IOD.Zip,
                                                   /*Get Role Name*/
                                                   roleName = rol.RoleName,
                                                   AmountReceived = Pay.PaymentAmount,
                                                   PaymentPlan = Pay.PaymentType,
                                                   date = Pay.PaymentDate,
                                                   ReportNo = IO.UniqueID,
                                                   /*Inspector Details*/
                                                   InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                                   InspectorPercentage = InsDtls.PaymentPercentage
                                               }).ToList();
                    }
                    else if (searchCriteria == "Zip")
                    {
                        int zip = Convert.ToInt32(searchby);
                        objReportTotalSales = (from IO in context.InspectionOrders
                                               join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                               join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                               join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                               join rol in context.Roles on UI.Role equals rol.RoleID
                                               join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                               join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                               where (Pay.PaymentDate >= _startdate && Pay.PaymentDate <= _enddate) && IOD.Zip == zip
                                               select new Report
                                               {
                                                   OwnerFirstName = IOD.OwnerFirstName,
                                                   OwnerLastName = IOD.OwnerLastName,
                                                   StreetAddress = IOD.PropertyAddress,
                                                   City = IOD.PropertyAddressCity,
                                                   State = IOD.PropertyAddressState,
                                                   InspectionDate = IO.CreatedDate,
                                                   Zip = IOD.Zip,
                                                   /*Get Role Name*/
                                                   roleName = rol.RoleName,
                                                   AmountReceived = Pay.PaymentAmount,
                                                   PaymentPlan = Pay.PaymentType,
                                                   date = Pay.PaymentDate,
                                                   ReportNo = IO.UniqueID,
                                                   /*Inspector Details*/
                                                   InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                                   InspectorPercentage = InsDtls.PaymentPercentage
                                               }).ToList();
                    }
                    else
                    {

                        objReportTotalSales = (from IO in context.InspectionOrders
                                               join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                               join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                               join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                               join rol in context.Roles on UI.Role equals rol.RoleID
                                               join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                               join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                               where IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate && IOD.PropertyAddress == searchby
                                               select new Report
                                               {
                                                   OwnerFirstName = IOD.OwnerFirstName,
                                                   OwnerLastName = IOD.OwnerLastName,
                                                   StreetAddress = IOD.PropertyAddress,
                                                   City = IOD.PropertyAddressCity,
                                                   State = IOD.PropertyAddressState,
                                                   InspectionDate = IO.CreatedDate,
                                                   Zip = IOD.Zip,
                                                   /*Get Role Name*/
                                                   roleName = rol.RoleName,
                                                   AmountReceived = Pay.PaymentAmount,
                                                   PaymentPlan = Pay.PaymentType,
                                                   date = Pay.PaymentDate,
                                                   ReportNo = IO.UniqueID,
                                                   /*Inspector Details*/
                                                   InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                                   InspectorPercentage = InsDtls.PaymentPercentage
                                               }).ToList();
                    }

                }

                else if (searchCriteria == "No Criteria" && searchby == "No Search" && (startDate == null || startDate == ""))
                {
                    objReportTotalSales = (from IO in context.InspectionOrders
                                           join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                           join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                           join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                           join rol in context.Roles on UI.Role equals rol.RoleID
                                           join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                           join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID

                                           select new Report
                                           {
                                               OwnerFirstName = IOD.OwnerFirstName,
                                               OwnerLastName = IOD.OwnerLastName,
                                               StreetAddress = IOD.PropertyAddress,
                                               City = IOD.PropertyAddressCity,
                                               State = IOD.PropertyAddressState,
                                               InspectionDate = IO.CreatedDate,
                                               Zip = IOD.Zip,
                                               /*Get Role Name*/
                                               roleName = rol.RoleName,
                                               AmountReceived = Pay.PaymentAmount,
                                               PaymentPlan = Pay.PaymentType,
                                               date = Pay.PaymentDate,
                                               ReportNo = IO.UniqueID,
                                               /*Inspector Details*/
                                               InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                               InspectorPercentage = InsDtls.PaymentPercentage
                                           }).ToList();
                }

                else if (searchCriteria == "Inspector")
                {
                    objReportTotalSales = (from IO in context.InspectionOrders
                                           join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                           join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                           join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                           join rol in context.Roles on UI.Role equals rol.RoleID
                                           join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                           join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                           where UInfo.FirstName == searchby
                                           select new Report
                                           {
                                               OwnerFirstName = IOD.OwnerFirstName,
                                               OwnerLastName = IOD.OwnerLastName,
                                               StreetAddress = IOD.PropertyAddress,
                                               City = IOD.PropertyAddressCity,
                                               State = IOD.PropertyAddressState,
                                               InspectionDate = IO.CreatedDate,
                                               Zip = IOD.Zip,
                                               /*Get Role Name*/
                                               roleName = rol.RoleName,
                                               AmountReceived = Pay.PaymentAmount,
                                               PaymentPlan = Pay.PaymentType,
                                               date = Pay.PaymentDate,
                                               ReportNo = IO.UniqueID,
                                               /*Inspector Details*/
                                               InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                               InspectorPercentage = InsDtls.PaymentPercentage
                                           }).ToList();
                }
                else if (searchCriteria == "Zip")
                {
                    int zip = Convert.ToInt32(searchby);
                    objReportTotalSales = (from IO in context.InspectionOrders
                                           join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                           join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                           join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                           join rol in context.Roles on UI.Role equals rol.RoleID
                                           join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                           join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                           where IOD.Zip == zip
                                           select new Report
                                           {
                                               OwnerFirstName = IOD.OwnerFirstName,
                                               OwnerLastName = IOD.OwnerLastName,
                                               StreetAddress = IOD.PropertyAddress,
                                               City = IOD.PropertyAddressCity,
                                               State = IOD.PropertyAddressState,
                                               InspectionDate = IO.CreatedDate,
                                               Zip = IOD.Zip,
                                               /*Get Role Name*/
                                               roleName = rol.RoleName,
                                               AmountReceived = Pay.PaymentAmount,
                                               PaymentPlan = Pay.PaymentType,
                                               date = Pay.PaymentDate,
                                               ReportNo = IO.UniqueID,
                                               /*Inspector Details*/
                                               InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                               InspectorPercentage = InsDtls.PaymentPercentage
                                           }).ToList();
                }
                else
                {
                    objReportTotalSales = (from IO in context.InspectionOrders
                                           join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                           join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                           join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                           join rol in context.Roles on UI.Role equals rol.RoleID
                                           join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                           join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                           where IOD.PropertyAddress == searchby
                                           select new Report
                                           {
                                               OwnerFirstName = IOD.OwnerFirstName,
                                               OwnerLastName = IOD.OwnerLastName,
                                               StreetAddress = IOD.PropertyAddress,
                                               City = IOD.PropertyAddressCity,
                                               State = IOD.PropertyAddressState,
                                               InspectionDate = IO.CreatedDate,
                                               Zip = IOD.Zip,
                                               /*Get Role Name*/
                                               roleName = rol.RoleName,
                                               AmountReceived = Pay.PaymentAmount,
                                               PaymentPlan = Pay.PaymentType,
                                               date = Pay.PaymentDate,
                                               ReportNo = IO.UniqueID,
                                               /*Inspector Details*/
                                               InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                               InspectorPercentage = InsDtls.PaymentPercentage
                                           }).ToList();
                }
                objInspectorReport.ReportData = objReportTotalSales;
                _messageToShow.Result = RenderRazorViewToString("_PartialAdmin_PaymentReport", objInspectorReport);


                if (objInspectorReport.ReportData == null)
                    _messageToShow.Message = "No Data in Selected Dates";
                return ReturnJson(_messageToShow);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult InspectionReportsNew()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    List<Report> objReportTotalSales = new List<Report>();

                    objReportTotalSales = (from IO in context.tblinspectionorder
                                           join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                           join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                           join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                           join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                           join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                           where IO.InspectionStatus == "Completed" && IO.IsDeleted == false
                                           orderby IO.ScheduledDate descending
                                           select new Report
                                           {
                                               OwnerFirstName = IOD.RequesterFName,
                                               OwnerLastName = IOD.RequesterLName,
                                               StreetAddress = IOD.PropertyAddress,
                                               InspectionDate = IO.ScheduledDate,
                                               OrderID = IO.InspectionOrderDetailId,
                                               Zip = z.Zip,
                                               ReportNo = IO.ReportNo,
                                               /*Inspector Details*/
                                               InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                           }).ToList();
                    return View(objReportTotalSales);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult InspectionReports(string startDate, string endDate)
        {
            try
            {
                InspectorReport objInspectorReport = new InspectorReport();
                var Inspectors = context.UserInfoes.Where(x => x.Role == "85DA6641-E08A-4192-A" && x.IsDeleted == false && x.IsActive == true);
                List<SelectListItem> items = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--All--";
                objSelectListItem.Value = "--All--";

                foreach (var t in Inspectors)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.FirstName + " " + t.LastName;
                    s.Value = t.FirstName + " " + t.LastName;
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                objInspectorReport.InspectorList = items;


                if ((startDate == null && endDate == null) || (startDate == "" && endDate == ""))
                {

                    List<Report> objReportTotalSales = new List<Report>();

                    objReportTotalSales = (from IO in context.InspectionOrders
                                           join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                           join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                           join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                           join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID

                                           where IO.InspectionStatus == "Completed"
                                           orderby IO.ScheduleDate descending
                                           select new Report
                                           {
                                               OwnerFirstName = IOD.OwnerFirstName,
                                               OwnerLastName = IOD.OwnerLastName,
                                               StreetAddress = IOD.PropertyAddress,
                                               City = IOD.PropertyAddressCity,
                                               State = IOD.PropertyAddressState,
                                               InspectionDate = IO.ScheduleDate,
                                               OrderID = IO.InspectionOrderID,
                                               Zip = IOD.Zip,
                                               ReportNo = IO.UniqueID,
                                               /*Inspector Details*/
                                               InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                           }).ToList();
                    objInspectorReport.ReportData = objReportTotalSales;
                    _messageToShow.Result = RenderRazorViewToString("_PartialAdminInspectionReport", objInspectorReport);

                    if (objInspectorReport.ReportData == null)
                        _messageToShow.Message = "No Data in Selected Dates";
                }
                else
                {
                    DateTime? _startdate = null;
                    DateTime? _enddate = null;

                    if (!String.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) != null)
                    {
                        _startdate = DateTime.Parse(startDate);
                        if (endDate == "" || endDate == null)
                            _enddate = DateTime.Now;
                        else
                            _enddate = DateTime.Parse(endDate);

                        //   InspectorReport objInspectorReport = new InspectorReport();
                        List<Report> objReportTotalSales = new List<Report>();

                        objReportTotalSales = (from IO in context.InspectionOrders
                                               join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                               join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                               join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                               join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                               where IO.InspectionStatus == "Completed" && IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                               select new Report
                                               {
                                                   OwnerFirstName = IOD.OwnerFirstName,
                                                   OwnerLastName = IOD.OwnerLastName,
                                                   StreetAddress = IOD.PropertyAddress,
                                                   City = IOD.PropertyAddressCity,
                                                   State = IOD.PropertyAddressState,
                                                   InspectionDate = IO.ScheduleDate,
                                                   OrderID = IO.InspectionOrderID,
                                                   Zip = IOD.Zip,
                                                   ReportNo = IO.UniqueID,
                                                   /*Inspector Details*/
                                                   InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                               }).ToList();
                        objInspectorReport.ReportData = objReportTotalSales;
                        Dates objDate = new Dates();
                        objDate.fromdate = startDate;
                        objDate.todate = endDate;
                        objInspectorReport.fromandtodate = objDate;
                        _messageToShow.Result = RenderRazorViewToString("_PartialAdminInspectionReport", objInspectorReport);

                        if (objInspectorReport.ReportData == null)
                            _messageToShow.Message = "No Data in Selected Dates";
                    }
                }


                return ReturnJson(_messageToShow);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public JsonResult RejectedInspectionReport(string startDate, string endDate)
        {
            try
            {

                InspectorReport objAdminPendingReport = new InspectorReport();
                Dates obj = new Dates();
                obj.fromdate = startDate;
                obj.todate = endDate;
                objAdminPendingReport.fromandtodate = obj;
                List<Report> objPendingRequest = new List<Report>();


                if ((startDate == null && endDate == null) || (startDate == "" && endDate == ""))
                {

                    objPendingRequest = (from IO in context.InspectionOrders
                                         join IOD in context.InsectionOrderDetails
                                         on IO.InspectionOrderID equals IOD.InspectionOrderId
                                         join UI in context.UserInfoes
                                         on IO.InspectorID equals UI.ID into g
                                         join req in context.UserInfoes on IO.RequesterID equals req.ID
                                         join role in context.Roles on req.Role equals role.RoleID
                                         //where IO.Comment != "" && IO.Comment != null && IO.IsAcceptedOrRejected == false
                                         where (IO.InspectorID == null || IO.InspectorID == "" || IO.Comment != "" && IO.Comment != null) && IO.IsAcceptedOrRejected == false
                                         select new Report
                                         {
                                             OrderID = IOD.InspectionOrderId,
                                             OwnerFirstName = IOD.OwnerFirstName,
                                             OwnerLastName = IOD.OwnerLastName,
                                             StreetAddress = IOD.PropertyAddress,
                                             City = IOD.PropertyAddressCity,
                                             State = IOD.PropertyAddressState,
                                             InspectionDate = IO.CreatedDate,
                                             Zip = IOD.PropertyAddressZip,
                                             /*Inspector Details*/
                                             InspectorDetails = g,
                                             ReportNo = IO.UniqueID,
                                             roleName = role.RoleName
                                         }).ToList();

                    objAdminPendingReport.ReportData = objPendingRequest;
                    _messageToShow.Result = RenderRazorViewToString("_PartialAdminRejectedRequest", objAdminPendingReport);


                    if (objAdminPendingReport.ReportData == null)
                        _messageToShow.Message = "No Data in Selected Dates";
                }
                else
                {
                    DateTime? _startdate = null;
                    DateTime? _enddate = null;
                    Dates objDate = new Dates();
                    objDate.fromdate = startDate;
                    objDate.todate = endDate;
                    objAdminPendingReport.fromandtodate = objDate;

                    if (!String.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) != null)
                    {
                        if (endDate == null || endDate == "")
                            _enddate = DateTime.Now;
                        else
                            _enddate = DateTime.Parse(endDate).Date.AddHours(23);
                        _startdate = DateTime.Parse(startDate).Date;

                        objPendingRequest = (from IO in context.InspectionOrders
                                             join IOD in context.InsectionOrderDetails
                                             on IO.InspectionOrderID equals IOD.InspectionOrderId
                                             join UI in context.UserInfoes
                                             on IO.InspectorID equals UI.ID into g
                                             //where IO.Comment != "" && IO.Comment != null && IO.IsAcceptedOrRejected == false
                                             where (IO.InspectorID == null || IO.InspectorID == "") && IO.IsAcceptedOrRejected == false &&
                                             IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                             select new Report
                                             {
                                                 OrderID = IOD.InspectionOrderId,
                                                 OwnerFirstName = IOD.OwnerFirstName,
                                                 OwnerLastName = IOD.OwnerLastName,
                                                 StreetAddress = IOD.PropertyAddress,
                                                 City = IOD.PropertyAddressCity,
                                                 State = IOD.PropertyAddressState,
                                                 InspectionDate = IO.CreatedDate,
                                                 Zip = IOD.Zip,
                                                 ReportNo = IO.ReportNo,
                                                 /*Inspector Details*/
                                                 InspectorDetails = g
                                             }).ToList();
                        objAdminPendingReport.ReportData = objPendingRequest;
                        _messageToShow.Result = RenderRazorViewToString("_PartialAdminRejectedRequest", objAdminPendingReport);


                        if (objAdminPendingReport.ReportData == null)
                            _messageToShow.Message = "No Data in Selected Dates";

                    }
                }
                return ReturnJson(_messageToShow);

            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult RejectedInspectionRequest()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    List<Report> objPendingRequest = null;
                    objPendingRequest = (from IO in context.tblinspectionorder
                                         join IOD in context.tblinspectionorderdetail
                                         on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                         join UI in context.UserInfoes
                                         on IO.InspectorID equals UI.ID into g
                                         join req in context.UserInfoes on IO.RequesterID equals req.ID
                                         join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                         //where IO.Comment != "" && IO.Comment != null && IO.IsAcceptedOrRejected == false
                                         where (IO.InspectorID == null || IO.InspectorID == "" || IO.IsRejectedByInspector == true) && IO.IsDeleted == false
                                         select new Report
                                         {
                                             OrderID = IOD.InspectionorderdetailsID,
                                             OwnerFirstName = IOD.RequesterFName,
                                             OwnerLastName = IOD.RequesterLName,
                                             StreetAddress = IOD.PropertyAddress,
                                             //City = IOD.PropertyAddressCity,
                                             // State = IOD.PropertyAddressState,
                                             InspectionDate = IO.ScheduledDate,
                                             Zip = z.Zip,
                                             /*Inspector Details*/
                                             InspectorDetails = g,
                                             ReportNo = IO.ReportNo,
                                             //roleName = role.RoleName
                                         }).ToList();

                    return View(objPendingRequest);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult PendingInspectionRequest()
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {

                    List<Report> objPendingRequest = null;
                    objPendingRequest = (from IO in context.tblinspectionorder
                                         join IOD in context.tblinspectionorderdetail
                                         on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                         join UI in context.UserInfoes
                                         on IO.InspectorID equals UI.ID into g
                                         join req in context.UserInfoes on IO.RequesterID equals req.ID
                                         join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                         //where IO.Comment != "" && IO.Comment != null && IO.IsAcceptedOrRejected == false
                                         where (IO.InspectorID != null || IO.InspectorID != "") && IO.IsAcceptedOrRejected == false && IO.IsRejectedByInspector == false && IO.IsDeleted == false
                                         select new Report
                                         {
                                             OrderID = IOD.InspectionorderdetailsID,
                                             OwnerFirstName = IOD.RequesterFName,
                                             OwnerLastName = IOD.RequesterLName,
                                             StreetAddress = IOD.PropertyAddress,
                                             //City = IOD.PropertyAddressCity,
                                             // State = IOD.PropertyAddressState,
                                             InspectionDate = IO.ScheduledDate,
                                             Zip = z.Zip,
                                             /*Inspector Details*/
                                             InspectorDetails = g,
                                             ReportNo = IO.ReportNo,
                                             //roleName = role.RoleName
                                         }).ToList();

                    return View(objPendingRequest);

                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult PendingInspectionReport(string startDate, string endDate)
        {
            try
            {

                InspectorReport objAdminPendingReport = new InspectorReport();
                InspectorReport objInspectorReport = new InspectorReport();
                var Inspectors = context.UserInfoes.Where(x => x.Role == "85DA6641-E08A-4192-A" && x.IsDeleted == false && x.IsActive == true);
                List<SelectListItem> items = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--All--";
                objSelectListItem.Value = "--All--";

                foreach (var t in Inspectors)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.FirstName + t.LastName;
                    s.Value = t.FirstName + t.LastName;
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                objAdminPendingReport.InspectorList = items;
                List<Report> objPendingRequest = new List<Report>();

                Dates obj = new Dates();
                obj.fromdate = startDate;
                obj.todate = endDate;
                objAdminPendingReport.fromandtodate = obj;

                if ((startDate == null && endDate == null) || (startDate == "" && endDate == ""))
                {

                    objPendingRequest = (from IO in context.InspectionOrders
                                         join IOD in context.InsectionOrderDetails
                                         on IO.InspectionOrderID equals IOD.InspectionOrderId
                                         join UI in context.UserInfoes
                                         on IO.InspectorID equals UI.ID into g
                                         join req in context.UserInfoes on IO.RequesterID equals req.ID
                                         join role in context.Roles on req.Role equals role.RoleID
                                         //where IO.Comment != "" && IO.Comment != null && IO.IsAcceptedOrRejected == false
                                         where IO.IsAcceptedOrRejected != false && IO.InspectionStatus == "Pending"
                                         select new Report
                                         {
                                             OrderID = IOD.InspectionOrderId,
                                             OwnerFirstName = IOD.OwnerFirstName,
                                             OwnerLastName = IOD.OwnerLastName,
                                             StreetAddress = IOD.PropertyAddress,
                                             City = IOD.PropertyAddressCity,
                                             State = IOD.PropertyAddressState,
                                             InspectionDate = IO.CreatedDate,
                                             Zip = IOD.PropertyAddressZip,
                                             /*Inspector Details*/
                                             InspectorDetails = g,
                                             roleName = role.RoleName,
                                             ReportNo = IO.UniqueID
                                         }).ToList();

                    objAdminPendingReport.ReportData = objPendingRequest;
                    _messageToShow.Result = RenderRazorViewToString("_PartialAdminPendingRequest", objAdminPendingReport);


                    if (objAdminPendingReport.ReportData == null)
                        _messageToShow.Message = "No Data in Selected Dates";
                }
                else
                {
                    DateTime? _startdate = null;
                    DateTime? _enddate = null;
                    if (!String.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) != null)
                    {
                        if (endDate == null || endDate == "")
                            _enddate = DateTime.Now.Date;
                        else
                            _enddate = DateTime.Parse(endDate);
                        _startdate = DateTime.Parse(startDate);

                        objPendingRequest = (from IO in context.InspectionOrders
                                             join IOD in context.InsectionOrderDetails
                                             on IO.InspectionOrderID equals IOD.InspectionOrderId
                                             join UI in context.UserInfoes
                                             on IO.InspectorID equals UI.ID into g
                                             join req in context.UserInfoes on IO.RequesterID equals req.ID
                                             join role in context.Roles on req.Role equals role.RoleID
                                             //where IO.Comment != "" && IO.Comment != null && IO.IsAcceptedOrRejected == false
                                             where IO.IsAcceptedOrRejected != false && IO.InspectionStatus == "Pending" &&
                                             IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                             select new Report
                                             {
                                                 OrderID = IOD.InspectionOrderId,
                                                 OwnerFirstName = IOD.OwnerFirstName,
                                                 OwnerLastName = IOD.OwnerLastName,
                                                 StreetAddress = IOD.PropertyAddress,
                                                 City = IOD.PropertyAddressCity,
                                                 State = IOD.PropertyAddressState,
                                                 InspectionDate = IO.CreatedDate,
                                                 Zip = IOD.Zip,
                                                 ReportNo = IO.ReportNo,
                                                 /*Inspector Details*/
                                                 InspectorDetails = g,
                                                 roleName = role.RoleName,
                                             }).ToList();
                        objAdminPendingReport.ReportData = objPendingRequest;
                        _messageToShow.Result = RenderRazorViewToString("_PartialAdminPendingRequest", objAdminPendingReport);


                        if (objAdminPendingReport.ReportData == null)
                            _messageToShow.Message = "No Data in Selected Dates";

                    }
                }
                return ReturnJson(_messageToShow);

            }
            catch (Exception)
            {

                throw;
            }
        }
        #region InspectorPayment
        /// <summary>
        /// method to List payment of all Inspectors
        /// </summary>
        /// <returns></returns>
        /// 

        public ActionResult InspectorPayment()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    InspectorDetails objInspectorDetails = new InspectorDetails();
                    List<InspectorDetails> lstobjInspectorDetails = new List<InspectorDetails>();
                    lstobjInspectorDetails = (from ID in context.InspectorDetail
                                              join UI in context.UserInfoes on ID.UserID equals UI.ID
                                              join Roles in context.Roles on UI.Role equals Roles.RoleID
                                              where Roles.RoleName == "Property Inspector" && UI.IsActive == true && UI.IsDeleted == false
                                              select new InspectorDetails
                                              {
                                                  InspectorDetailsID = ID.InspectorDetailsID,
                                                  FirstName = UI.FirstName,
                                                  LastName = UI.LastName,
                                                  PaymentPercentage = ID.PaymentPercentage
                                              }).OrderBy(x => x.FirstName).ToList();
                    objInspectorDetails.InspectorPayment = lstobjInspectorDetails;
                    return View("InspectorPayment", objInspectorDetails);
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdatePercent
        /// <summary>
        /// Method to AssignInspector
        /// <param name="orderid"></param>
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdatePercent(string inspectorId, string newPercent)
        {
            try
            {
                inspectordetail objInspectorDetail = new inspectordetail();
                objInspectorDetail = context.InspectorDetail.Where(x => x.InspectorDetailsID == inspectorId).FirstOrDefault();
                objInspectorDetail.PaymentPercentage = Convert.ToInt32(newPercent);
                context.SaveChanges();
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AssignInspector
        /// <summary>
        /// Method to AssignInspector
        /// <param name="orderid"></param>
        /// </summary>
        /// <returns></returns>
        public ActionResult AssignInspector(string orderid)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    ViewBag.OrderId = orderid;
                    InspectorDetails objInspectorDetails = new InspectorDetails();
                    List<InspectorDetails> lstobjInspectorDetails = new List<InspectorDetails>();
                    lstobjInspectorDetails = (from UI in context.UserInfoes
                                              join IO in

                                                  (from val in context.tblinspectionorder
                                                   group val by val.InspectorID into g
                                                   select new { ID = g.Key, test = g.Count() }
                                              ) on UI.ID equals IO.ID into cls
                                              from v in cls.DefaultIfEmpty()
                                              join Roles in context.Roles on UI.Role equals Roles.RoleID
                                              join ID in context.InspectorDetail on UI.ID equals ID.UserID
                                              //  join zip in context.ZipCodeAssigned on UI.ID equals zip.InspectorID into joined
                                              where Roles.RoleName == "Property Inspector" && UI.IsActive == true && UI.IsDeleted == false
                                              select new InspectorDetails
                                             {
                                                 InspectorDetailsID = UI.ID,
                                                 FirstName = UI.FirstName,
                                                 LastName = UI.LastName,
                                                 EmailAddress = UI.EmailAddress,
                                                 //  Zip = joined,
                                                 PL = (from z in context.ZipCodeAssigned
                                                       join tblz in context.tblzip on z.ZipCode equals tblz.ZipId
                                                       where z.InspectorID == UI.ID
                                                       select new ZipCodes
                                                       {
                                                           ZipID = tblz.ZipId,
                                                           Zip = tblz.Zip
                                                       }),
                                                 Count = v.test == null ? 0 : v.test
                                             }).OrderBy(x => x.FirstName).ToList();
                    objInspectorDetails.InspectorPayment = lstobjInspectorDetails;
                    return View("AssignInspector", objInspectorDetails);
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetPaymentCycle
        /// <summary>
        /// Method to get Inspector Payment Cycle
        /// <param name="id"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult AssignPaymentCycle(string id)
        {
            try
            {
                var InspectorPayment = from PaymentCycle in context.InspectorDetail
                                       where PaymentCycle.InspectorDetailsID == id
                                       select new
                                       {
                                           PaymentCycle.PaymentCycleStartDate,
                                           PaymentCycle.PaymentCycleEndDate
                                       };
                return Json(InspectorPayment, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdatePaymentCycle
        /// <summary>
        /// Method to Update Inspector Payment Cycle
        /// <param name="id"></param>
        /// <param name="FromDate"></param>
        /// <param name="EndDate"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult UpdatePaymentCycle(string id, string FromDate, string EndDate)
        {
            try
            {
                inspectordetail objInspectorDetail = new inspectordetail();
                objInspectorDetail = context.InspectorDetail.Where(x => x.InspectorDetailsID == id).FirstOrDefault();
                objInspectorDetail.PaymentCycleStartDate = Convert.ToDateTime(FromDate);
                objInspectorDetail.PaymentCycleEndDate = Convert.ToDateTime(EndDate);
                context.SaveChanges();
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region AssignInspectoinRequest
        /// <summary>
        /// Method to Assign Inspection Request to Inspector
        /// <param name="id"></param>
        /// <param name="FromDate"></param>
        /// <param name="EndDate"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult AssignInspectoinRequest(string id, string Orderid)
        {
            try
            {
                //Orderid = "6bb32ef5-19f8-402e-a006-93770ad2f126";
                string RequestorEmail = (from IO in context.tblinspectionorder
                                         join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                         where IO.InspectionOrderDetailId == Orderid
                                         select UI.EmailAddress).SingleOrDefault();
                tblinspectionorder objInspectionOrder = new tblinspectionorder();
                EmailHelper email = new EmailHelper();
                var state = from IOD in context.tblinspectionorderdetail
                            join s in context.tblstate on IOD.PropertyState equals s.StateId
                            where IOD.InspectionorderdetailsID == Orderid
                            select IOD.PropertyState;
                int propertyState = 0;
                if (state != null)
                {
                    propertyState = Convert.ToInt32(state.FirstOrDefault());
                }


                int TotalInspectorReportCount = 0;
                string UniqueId = "Empty";
                /*Report ID Logic*/

                if (id != "")
                {
                    var countReports = (from IO in context.tblinspectionorder
                                        join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                        where IO.InspectorID == id && IOD.PropertyState == propertyState && IO.IsDeleted == false
                                        select IO.InspectorID);
                    TotalInspectorReportCount = countReports.Count();
                    string state1 = context.tblstate.Where(x => x.StateId == propertyState).FirstOrDefault().StateName;
                    UniqueId = state1 + context.InspectorDetail.Where(x => x.UserID == id).Select(y => y.InspectorNo).FirstOrDefault() + (TotalInspectorReportCount + 1).ToString();
                }

                /*End*/

                objInspectionOrder = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == Orderid).FirstOrDefault();
                objInspectionOrder.InspectorID = id;
                objInspectionOrder.ReportNo = UniqueId;
                objInspectionOrder.AssignedByAdmin = true;
                //objInspectionOrder.IsAcceptedOrRejected = false;
                objInspectionOrder.IsAcceptedOrRejected = false;
                objInspectionOrder.IsRejectedByInspector = false;
                context.SaveChanges();

                var Notification = context.NotificationDetail.FirstOrDefault(x => x.RequestID == Orderid);
                if (Notification != null)
                {
                    notificationdetail objNotification = new notificationdetail();
                    objNotification = context.NotificationDetail.FirstOrDefault(x => x.RequestID == Orderid);
                    objNotification.IsRead = true;
                    context.SaveChanges();
                }
                string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "Inspection Assigned").TemplateDiscription;
                bool status = email.SendEmail("", "Inspection Request Assigned", " ", emailBody, RequestorEmail);
                if (status == true)
                    return Json("success", JsonRequestBehavior.AllowGet);
                else
                    return Json("Mail Not Send", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region RequestMap
        /// <summary>
        /// Method to Assign Inspection Request to Inspector
        /// <param name="id"></param>
        /// <param name="FromDate"></param>
        /// <param name="EndDate"></param>
        /// </summary>
        /// <returns></returns>
        public ActionResult RequestMap()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    ViewBag.States = new SelectList(context.tblstate.OrderBy(x => x.StateName), "StateId", "StateName", 0);

                    //var Zip = from IO in context.InspectionOrders
                    //          join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                    //          join zip in context.ZipCode on IOD.PropertyAddressZip equals zip.Zip
                    //          where IO.IsAcceptedOrRejected == false && IO.AssignedByAdmin == false
                    //          select new
                    //          {
                    //              ID = IOD.InspectionOrderId,
                    //              FirstName = IOD.OwnerFirstName,
                    //              LastNAme = IOD.OwnerLastName,
                    //              Zip = zip.Zip,
                    //              latitude = zip.latitude,
                    //              longitude = zip.longitude
                    //          };
                    return View();
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetMapZipCodes
        /// <summary>
        /// Method to Get Zip Codes for google map
        /// <param name="id"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMapZipCodes(string Stateid)
        {
            try
            {
                if (Stateid == "")
                {
                    Stateid = "PR";
                }
                int state = Convert.ToInt32(Stateid);
                var Zip = from IO in context.tblinspectionorder
                          join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                          join zip in context.tblzip on IOD.PropertyZip equals zip.ZipId
                          join s in context.tblstate on IOD.PropertyState equals s.StateId
                          where IOD.PropertyState == state && IO.IsAcceptedOrRejected == false && IO.AssignedByAdmin == false && IO.IsRejectedByInspector == true && IO.IsDeleted == false
                          select new
                          {
                              ID = IOD.InspectionorderdetailsID,
                              FirstName = IOD.RequesterFName,
                              LastNAme = IOD.RequesterLName,
                              Zip = zip.Zip,
                              latitude = zip.Latitude,
                              longitude = zip.Longitude
                          };
                return Json(Zip, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion


        #region GetMapZipCodes
        /// <summary>
        /// Method to Get Zip Codes for google map
        /// <param name="id"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllMapZipCodes(string Stateid)
        {
            try
            {


                var Zip = from IO in context.tblinspectionorder
                          join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                          join zip in context.tblzip on IOD.PropertyZip equals zip.ZipId
                          where IO.IsAcceptedOrRejected == false && IO.AssignedByAdmin == false && IO.IsRejectedByInspector == true && IO.IsDeleted == false
                          select new
                          {
                              ID = IOD.InspectionorderdetailsID,
                              FirstName = IOD.RequesterFName,
                              LastNAme = IOD.RequesterLName,
                              Zip = zip.Zip,
                              latitude = zip.Latitude,
                              longitude = zip.Longitude
                          };
                return Json(Zip, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion


        #region GetAllInspectionReports
        /// <summary>
        /// Method to Get AllInspection Reports
        /// <param name="id"></param>
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAllInspectionReports()
        {
            try
            {



                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    ViewPreviousInspection objInspectionOrder = new ViewPreviousInspection();
                    List<ViewPreviousInspection> lstInspectionOrder = new List<ViewPreviousInspection>();
                    lstInspectionOrder = (from IO in context.tblinspectionorder
                                          join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                          join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                          join req in context.UserInfoes on IO.RequesterID equals req.ID
                                          join s in context.tblstate on IOD.PropertyState equals s.StateId
                                          join c in context.tblcity on IOD.PropertyCity equals c.CityId
                                          join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                          where IO.IsDeleted == false
                                          select new ViewPreviousInspection
                                          {
                                              OwnerFirstName = IOD.RequesterFName,
                                              OwnerLastName = IOD.RequesterLName,
                                              InspectionOrderId = IOD.InspectionorderdetailsID,
                                              PropertyAddress = IOD.PropertyAddress,
                                              PropertyAddressCity = c.CityName,
                                              PropertyAddressState = s.StateName,
                                              PropertyAddressZip = z.Zip,
                                              ReportNumber = IO.ReportNo,
                                              RequestDate = IO.ScheduledDate,
                                              ReportStatus = IO.InspectionStatus,
                                              InspectorName = UI.FirstName + " " + UI.LastName

                                          }).ToList();
                    objInspectionOrder.InspectionList = lstInspectionOrder;
                    return View(objInspectionOrder);

                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region FilterInspectionReport
        /// <summary>
        /// Method to Filter Inspection Report
        ///<param name="startDate"></param>
        ///<param name="endDate"></param>
        /// </summary>
        /// <returns></returns>
        public JsonResult FilterInspectionReport(string startDate, string endDate)
        {
            try
            {
                DateTime? _startdate = null;
                DateTime? _enddate = null;


                ViewPreviousInspection objInspectionOrder = new ViewPreviousInspection();
                List<ViewPreviousInspection> lstInspectionOrder = new List<ViewPreviousInspection>();

                Dates obj = new Dates();
                obj.fromdate = startDate;
                obj.todate = endDate;
                objInspectionOrder.fromandtodate = obj;
                if (!String.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) != null)
                {
                    _startdate = DateTime.Parse(startDate).Date;
                    if (endDate == null || endDate == null)
                        _enddate = DateTime.Now;
                    else
                        _enddate = DateTime.Parse(endDate).Date.AddHours(23);

                    lstInspectionOrder = (from IO in context.InspectionOrders
                                          join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                          join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                          join req in context.UserInfoes on IO.RequesterID equals req.ID
                                          join role in context.Roles on req.Role equals role.RoleID
                                          where IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                          select new ViewPreviousInspection
                                          {
                                              OwnerFirstName = IOD.OwnerFirstName,
                                              OwnerLastName = IOD.OwnerLastName,
                                              InspectionOrderId = IOD.InspectionOrderId,
                                              PropertyAddress = IOD.PropertyAddress,
                                              PropertyAddressCity = IOD.PropertyAddressCity,
                                              PropertyAddressState = IOD.PropertyAddressState,
                                              RequestedbyPhoneDay = IOD.PropertyAddressPhoneDay,
                                              Email = IOD.EmailAddress,
                                              RequestDate = IO.CreatedDate,
                                              ReportStatus = IO.InspectionStatus,
                                              InspectorName = UI.FirstName + " " + UI.LastName,
                                              Role = role.RoleName
                                          }).ToList();
                    objInspectionOrder.InspectionList = lstInspectionOrder;
                }
                else
                {
                    lstInspectionOrder = (from IO in context.InspectionOrders
                                          join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                          join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                          join req in context.UserInfoes on IO.RequesterID equals req.ID
                                          join role in context.Roles on req.Role equals role.RoleID

                                          select new ViewPreviousInspection
                                          {
                                              OwnerFirstName = IOD.OwnerFirstName,
                                              OwnerLastName = IOD.OwnerLastName,
                                              InspectionOrderId = IOD.InspectionOrderId,
                                              PropertyAddress = IOD.PropertyAddress,
                                              PropertyAddressCity = IOD.PropertyAddressCity,
                                              PropertyAddressState = IOD.PropertyAddressState,
                                              RequestedbyPhoneDay = IOD.PropertyAddressPhoneDay,
                                              Email = IOD.EmailAddress,
                                              RequestDate = IO.CreatedDate,
                                              ReportStatus = IO.InspectionStatus,
                                              InspectorName = UI.FirstName + " " + UI.LastName,
                                              Role = role.RoleName
                                          }).ToList();
                    objInspectionOrder.InspectionList = lstInspectionOrder;
                }
                _messageToShow.Result = RenderRazorViewToString("_PartialInspectionReport", objInspectionOrder);


                if (objInspectionOrder.InspectionList == null)
                    _messageToShow.Message = "No Data in Selected Dates";
                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetOfferTemplate
        /// <summary>
        /// Method to Get Offer Template
        /// </summary>
        /// <returns></returns>
        public JsonResult GetOfferTemplate()
        {
            try
            {
                var emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "InviteInspector");
                return Json(emailBody, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion


        #region SendOfferTemplate
        /// <summary>
        /// Method to Get Offer Template
        /// </summary>
        /// <returns></returns>
        public JsonResult SendOfferTemplate(string emailBody, string emailToSend)
        {
            try
            {
                EmailHelper email = new EmailHelper();
                bool status = email.SendEmail("", "Offer", " ", emailBody, emailToSend);
                if (status == true)
                    return Json("success", JsonRequestBehavior.AllowGet);
                else
                    return Json("error", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region UserPaymentReport
        /// <summary>
        /// Method to UserPaymentReport
        /// </summary>
        /// <returns></returns>
        public ActionResult UserPaymentReport(string userID)
        {
            try
            {
                InspectorReport objInspectorReport = new InspectorReport();
                List<Report> objReportTotalSales = new List<Report>();
                objReportTotalSales = (from IO in context.tblinspectionorder
                                       join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                       join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                       join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                       join rol in context.Roles on UI.Role equals rol.RoleID
                                       join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                       join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                       join s in context.tblstate on IOD.PropertyState equals s.StateId
                                       join c in context.tblcity on IOD.PropertyCity equals c.CityId
                                       join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                       where IO.RequesterID == userID && IO.IsDeleted == false
                                       select new Report
                                       {
                                           OwnerFirstName = IOD.RequesterFName,
                                           OwnerLastName = IOD.RequesterLName,
                                           StreetAddress = IOD.PropertyAddress,
                                           City = c.CityName,
                                           State = s.StateName,
                                           InspectionDate = IO.Createddate,
                                           Zip = z.Zip,
                                           /*Get Role Name*/
                                           roleName = rol.RoleName,
                                           AmountReceived = Pay.PaymentAmount,
                                           PaymentPlan = Pay.PaymentType,
                                           date = Pay.PaymentDate,
                                           /*Inspector Details*/
                                           InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                           InspectorPercentage = InsDtls.PaymentPercentage
                                       }).ToList();
                objInspectorReport.ReportData = objReportTotalSales;
                return PartialView("_PartialUserPaymentReport", objInspectorReport);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region InspectorDuePayment
        /// <summary>
        /// Method to check inspector due payment
        /// <param name="orderid"></param>
        /// </summary>
        /// <returns></returns>
        public ActionResult InspectorDuePayment()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Admin" || Sessions.LoggedInUser.RoleName == "Sub-Admin"))
                {
                    InspectorDetails objInspectorDetails = new InspectorDetails();
                    List<InspectorDetails> lstobjInspectorDetails = new List<InspectorDetails>();
                    lstobjInspectorDetails = (from UI in context.UserInfoes
                                              join IO in

                                                  (from val in context.InspectionOrders
                                                   group val by val.InspectorID into g
                                                   select new { ID = g.Key, test = g.Count() }
                                              ) on UI.ID equals IO.ID into cls
                                              from v in cls.DefaultIfEmpty()
                                              join Roles in context.Roles on UI.Role equals Roles.RoleID
                                              join IND in context.InspectorDetail on UI.ID equals IND.UserID
                                              join zip in context.ZipCodeAssigned on UI.ID equals zip.InspectorID into joined
                                              where Roles.RoleName == "Property Inspector" && UI.IsActive == true && UI.IsDeleted == false
                                              select new InspectorDetails
                                              {
                                                  InspectorDetailsID = UI.ID,
                                                  FirstName = UI.FirstName,
                                                  LastName = UI.LastName,
                                                  EmailAddress = UI.EmailAddress,
                                                  Zip = joined,
                                                  Count = v.test == null ? 0 : v.test
                                              }).OrderBy(x => x.FirstName).ToList();
                    objInspectorDetails.InspectorPayment = lstobjInspectorDetails;


                    var payment = (from uI in context.UserInfoes
                                   join IND in context.InspectorDetail on uI.ID equals IND.UserID
                                   join io in context.InspectionOrders on uI.ID equals io.InspectorID
                                   join pn in context.Payments on io.InspectionOrderID equals pn.ReportID
                                   join rl in context.Roles on uI.Role equals rl.RoleID
                                   where rl.RoleName == "Property Inspector"
                                   select new
                                   {
                                       uI.ID,
                                       pn.PaymentAmount,
                                       IND.PaymentPercentage

                                   }).GroupBy(x => x.ID);

                    float? d;
                    string ID;
                    int? Percent;
                    foreach (var item in payment)
                    {
                        Percent = item.Select(x => x.PaymentPercentage).FirstOrDefault();
                        ID = item.Select(x => x.ID).FirstOrDefault();
                        d = item.Sum(x => x.PaymentAmount);
                        objInspectorDetails.InspectorPayment.ForEach(
                            c =>
                            {
                                if (c.InspectorDetailsID == ID)
                                {
                                    c.PaymentSum = d;
                                    if (Percent > 0)
                                    {

                                        c.PaymentDue = ((d * (100 - Percent)) / 100);
                                        c.PaymentPercentage = Percent;
                                    }
                                }
                            }
                            );

                    }
                    return View("InspectorDuePayment", objInspectorDetails);
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
        //#region GetInspectorReportPayment
        ///// <summary>
        ///// Method to get GetInspectorReportPayment
        ///// <param name="orderid"></param>
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult GetInspectorReportPayment(string InspectorID)
        //{
        //    try
        //    {
        //        DateTime? startdate;
        //        DateTime? enddate;
        //        int startday;
        //        int endday;

        //        var PaymentCycle = (from ID in context.InspectorDetail
        //                            where ID.UserID == InspectorID
        //                            select new
        //                            {
        //                                ID.PaymentCycleStartDate,
        //                                ID.PaymentCycleEndDate
        //                            }).FirstOrDefault();
        //        startdate = PaymentCycle.PaymentCycleStartDate;
        //        enddate = PaymentCycle.PaymentCycleEndDate;

        //        startday = startdate.Value.Day;
        //        endday = enddate.Value.Day;

        //        startdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, startday);
        //        enddate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, endday);


        //        InspectorReport objInspectorReport = new InspectorReport();
        //        List<Report> objListOfInspectorReportViewed = new List<Report>();
        //        List<Report> objListOfInspectorReportViewedPayment = new List<Report>();
        //        objListOfInspectorReportViewed = (from IO in context.InspectionOrders
        //                                          join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
        //                                          join rpt in context.ReportViewed on IO.InspectionId equals rpt.ReportID
        //                                          join UI in context.UserInfoes on rpt.UserID equals UI.ID
        //                                          join rol in context.Roles on UI.Role equals rol.RoleID
        //                                          //join PM in context.Payments on IOD.InspectionOrderId equals PM.ReportID
        //                                          where IO.InspectorID == InspectorID && rpt.CreatedDate >= startdate && rpt.CreatedDate <= enddate
        //                                          select new Report
        //                                          {

        //                                              OrderID = rpt.ReportID,
        //                                              OwnerFirstName = IOD.OwnerFirstName,
        //                                              OwnerLastName = IOD.OwnerLastName,
        //                                              StreetAddress = IOD.PropertyAddress,
        //                                              City = IOD.PropertyAddressCity,
        //                                              State = IOD.PropertyAddressState,
        //                                              InspectionDate = IO.CreatedDate,
        //                                              Inspectionstatus = IO.InspectionStatus,
        //                                              isrequestfromAdmin = IO.AssignedByAdmin,
        //                                              roleName = rol.RoleName,
        //                                              date = rpt.CreatedDate
        //                                          }).ToList();

        //        objListOfInspectorReportViewedPayment = (from IO in context.InspectionOrders
        //                                                 join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
        //                                                 join PM in context.Payments on IOD.InspectionOrderId equals PM.ReportID
        //                                                 where IO.InspectorID == InspectorID && PM.PaymentDate >= startdate && PM.CreatedDate <= enddate
        //                                                 select new Report
        //                                                 {
        //                                                     AmountReceived = PM.PaymentAmount,
        //                                                     PaymentPlan = PM.PaymentType
        //                                                 }).ToList();


        //        for (int j = 0; j < objListOfInspectorReportViewedPayment.Count; j++)
        //        {
        //            objListOfInspectorReportViewed[j].AmountReceived = objListOfInspectorReportViewedPayment[j].AmountReceived;
        //            objListOfInspectorReportViewed[j].PaymentPlan = objListOfInspectorReportViewedPayment[j].PaymentPlan;
        //        }



        //        int i = 0;
        //        foreach (var item in objListOfInspectorReportViewed)
        //        {
        //            float amount = Convert.ToSingle(item.AmountReceived);
        //            List<DateTime?> lstdates = objListOfInspectorReportViewed.Where(x => x.OrderID == item.OrderID).Select(x => x.date).ToList();
        //            List<RoleAndDate> roels = (from role in objListOfInspectorReportViewed
        //                                       where role.OrderID == item.OrderID
        //                                       select new RoleAndDate
        //                                       {
        //                                           UsrRole = role.roleName,
        //                                           fromdate = role.date,
        //                                           PaymentAmount = role.AmountReceived,
        //                                           PaymentType = role.PaymentPlan
        //                                       }).ToList();

        //            objListOfInspectorReportViewed[i].fromandtodate = lstdates;
        //            objListOfInspectorReportViewed[i].RoleDate = roels;
        //            i++;
        //        }
        //        objInspectorReport.ReportData = (objListOfInspectorReportViewed.Select(r => r.OrderID)
        //                                        .Distinct()
        //                                        .Select(r => objListOfInspectorReportViewed.FirstOrDefault(t => t.OrderID == r)).ToList());
        //        return View(objInspectorReport);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}
        //#endregion


        public ActionResult Notifications()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    List<Notification> lstNotification = new List<Notification>();
                    Notification objNotification = new Notification();
                    lstNotification = (from NO in context.NotificationDetail
                                       join NT in context.NotificationType on NO.NotificationTypeID equals NT.ID
                                       join UI in context.UserInfoes on NO.NotifcationFrom equals UI.ID
                                       join RL in context.Roles on UI.Role equals RL.RoleID
                                       where NO.IsRead == false
                                       orderby NO.CreatedDate descending
                                       select new Notification
                                       {
                                           ID = NO.ID,
                                           NotificationFrom = UI.FirstName + " " + UI.LastName,
                                           NotificationFromID = UI.ID,
                                           NotificationType = NT.NotificationType1,
                                           NotificationDate = NO.CreatedDate,
                                           UserRole = RL.RoleName,
                                           RequestId = NO.RequestID
                                       }).ToList();
                    objNotification.NotificationDetails = lstNotification;
                    return View(objNotification);
                }
                else
                {
                    return RedirectToAction("Home", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult AdminNotificationOperation(string NotificationId, string NotificationFrom, string NotificationType, string RequestId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    int id = Convert.ToInt32(NotificationId);
                    if (NotificationType == "Forgot Password")
                    {
                        var UserDetails = (from UI in context.UserInfoes
                                           where UI.ID == NotificationFrom
                                           select UI).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            userinfo objUserInfo = new userinfo();
                            objUserInfo = context.UserInfoes.Where(x => x.ID == NotificationFrom).FirstOrDefault();
                            objUserInfo.IsActive = true;
                            context.SaveChanges();

                            EmailHelper email = new EmailHelper();
                            string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "ForgotPassword").TemplateDiscription;
                            emailBody = emailBody.Replace("{Password}", UserDetails.Password);

                            bool status = email.SendEmail("SpectorMax", "ForgotPassword", "", emailBody, UserDetails.EmailAddress);
                            if (status == true)
                            {
                                _messageToShow.Message = "Success";
                                _messageToShow.Result = "Password Sent Successfully.";
                                notificationdetail objNotificaionDetails = new notificationdetail();
                                objNotificaionDetails = context.NotificationDetail.FirstOrDefault(x => x.ID == id);
                                objNotificaionDetails.IsRead = true;
                                context.SaveChanges();
                            }
                            else
                                _messageToShow.Message = "error";
                        }
                        else
                        {
                            _messageToShow.Message = "error";
                        }
                    }
                    else if (NotificationType == "Inspector Registered")
                    {
                        var UserDetails = (from UI in context.UserInfoes
                                           where UI.ID == NotificationFrom
                                           select UI).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            userinfo objUserInfo = new userinfo();
                            objUserInfo = context.UserInfoes.Where(x => x.ID == NotificationFrom).FirstOrDefault();
                            objUserInfo.IsActive = true;
                            context.SaveChanges();

                            EmailHelper email = new EmailHelper();
                            string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "ForgotPassword").TemplateDiscription;
                            emailBody = emailBody.Replace("{Password}", UserDetails.Password);

                            bool status = email.SendEmail("SpectorMax", "ForgotPassword", "", emailBody, UserDetails.EmailAddress);
                            if (status == true)
                            {
                                notificationdetail objNotificaionDetails = new notificationdetail();
                                objNotificaionDetails = context.NotificationDetail.FirstOrDefault(x => x.ID == id);
                                objNotificaionDetails.IsRead = true;
                                context.SaveChanges();
                                _messageToShow.Message = "Success";
                                _messageToShow.Result = "Inspector Account Is Activated Successfully.";
                            }
                            else
                                _messageToShow.Message = "error";
                        }
                        else
                            _messageToShow.Message = "error";
                    }
                    else if (NotificationType == "Remove Rating And Reviews")
                    {
                        inspectorrating objInspectorRatings = new inspectorrating();
                        objInspectorRatings = context.InspectorRatings.FirstOrDefault(x => x.RatingID == RequestId);
                        context.InspectorRatings.Remove(objInspectorRatings);
                        context.SaveChanges();

                        notificationdetail objNotificaionDetails = new notificationdetail();
                        objNotificaionDetails = context.NotificationDetail.FirstOrDefault(x => x.ID == id);
                        objNotificaionDetails.IsRead = true;
                        context.SaveChanges();
                        _messageToShow.Message = "Success";
                        _messageToShow.Result = "Rating And Reviews Removed Successfully.";
                    }
                }
                return Json(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetComment(string CommentBoxId)
        {
            CommentLibrary ObjCommentLibrary = new CommentLibrary();
            List<AdminCommanLibrary> objAdminCommanLibrary = new List<AdminCommanLibrary>();


            objAdminCommanLibrary = (from acl in context.AdminCommentLibrary
                                     where acl.CommentBoxID == CommentBoxId && acl.IsDeleted == false && acl.IsAdminComment == true
                                     select new AdminCommanLibrary
                                     {
                                         CommentText = acl.CommentText,
                                         CommentBoxID = acl.CommentBoxID,
                                         Id = acl.Id
                                     }).ToList();
            ObjCommentLibrary.AdminCommanLibrary = objAdminCommanLibrary;
            if (objAdminCommanLibrary.Count > 0)
                _messageToShow.Message = "Success";
            else
                _messageToShow.Message = "Item Not Found";
            _messageToShow.Result = RenderRazorViewToString("_ManageCommentLibrary", ObjCommentLibrary);
            return Json(_messageToShow);
        }
        public JsonResult SaveComment(string SectionId, string SubSectionId, string CommentBoxId, string CommentText, string CommentBoxName)
        {
            try
            {
                admincommentlibrary objAdminCommentLibrary = new admincommentlibrary();
                objAdminCommentLibrary.Id = Guid.NewGuid().ToString();
                objAdminCommentLibrary.SectionId = SectionId;
                objAdminCommentLibrary.SubSectionId = SubSectionId;
                objAdminCommentLibrary.CommentBoxID = CommentBoxId;
                objAdminCommentLibrary.CommentText = CommentText;
                objAdminCommentLibrary.CreatedDate = DateTime.Now;//.ToUniversalTime();
                objAdminCommentLibrary.ModifiendDate = DateTime.Now;//.ToUniversalTime();
                objAdminCommentLibrary.IsDeleted = false;
                objAdminCommentLibrary.IsAdminComment = true;
                objAdminCommentLibrary.UserId = Sessions.LoggedInUser.UserId;
                objAdminCommentLibrary.CommentBoxName = CommentBoxName;
                context.AdminCommentLibrary.Add(objAdminCommentLibrary);
                context.SaveChanges();
                CommentLibrary ObjCommentLibrary = new CommentLibrary();
                List<AdminCommanLibrary> objAdminCommanLibrary = new List<AdminCommanLibrary>();


                objAdminCommanLibrary = (from acl in context.AdminCommentLibrary
                                         where acl.CommentBoxID == CommentBoxId && acl.IsDeleted == false
                                         select new AdminCommanLibrary
                                         {
                                             CommentText = acl.CommentText,
                                             CommentBoxID = acl.CommentBoxID,
                                             Id = acl.Id
                                         }).ToList();
                ObjCommentLibrary.AdminCommanLibrary = objAdminCommanLibrary;
                _messageToShow.Message = "Success";
                _messageToShow.Result = RenderRazorViewToString("_ManageCommentLibrary", ObjCommentLibrary);
                return Json(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult UpdateDelete(string AdminCommentLibraryId, string status, string CommentText)
        {
            try
            {
                admincommentlibrary objAdminCommentLibrary = new admincommentlibrary();
                objAdminCommentLibrary = context.AdminCommentLibrary.Where(x => x.Id == AdminCommentLibraryId).FirstOrDefault();
                if (status == "update")
                    objAdminCommentLibrary.CommentText = CommentText;
                else
                    objAdminCommentLibrary.IsDeleted = true;
                objAdminCommentLibrary.ModifiendDate = DateTime.Now;//ToUniversalTime();
                context.SaveChanges();
                //CommentLibrary ObjCommentLibrary = new CommentLibrary();
                //List<AdminCommanLibrary> objAdminCommanLibrary = new List<AdminCommanLibrary>();


                //objAdminCommanLibrary = (from acl in context.AdminCommentLibrary
                //                         where acl.CommentBoxID == objAdminCommentLibrary.CommentBoxID && acl.IsDeleted == false
                //                         select new AdminCommanLibrary
                //                         {
                //                             CommentText = acl.CommentText,
                //                             CommentBoxID = acl.CommentBoxID,
                //                             Id = acl.Id
                //                         }).ToList();
                //ObjCommentLibrary.AdminCommanLibrary = objAdminCommanLibrary;
                _messageToShow.Message = "Success";
                //_messageToShow.Result = RenderRazorViewToString("_ManageCommentLibrary", ObjCommentLibrary);
                return Json(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult DeleteSingleUser(string UserId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    userinfo objUserInfo = new userinfo();
                    string ReturnMsg = string.Empty;
                    objUserInfo = context.UserInfoes.Where(x => x.ID == UserId).FirstOrDefault();
                    if (objUserInfo != null)
                    {
                        objUserInfo.IsDeleted = true;
                        context.SaveChanges();
                        ReturnMsg = "Success";
                    }
                    else
                    {
                        ReturnMsg = "Error";
                    }
                    return Json(ReturnMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("SessionOver", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public JsonResult DeleteSingleInspector(string UserId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    userinfo objUserInfo = new userinfo();
                    List<zipcodeassigned> objzip = new List<zipcodeassigned>();
                    string ReturnMsg = string.Empty;

                    objUserInfo = context.UserInfoes.Where(x => x.ID == UserId).FirstOrDefault();
                    var zip = context.ZipCodeAssigned.Where(x => x.InspectorID == UserId).ToList();
                    if (objUserInfo != null)
                    {
                        objUserInfo.IsDeleted = true;

                        ReturnMsg = "Success";
                    }
                    if (zip != null)
                    {
                        foreach (var item in zip)
                        {
                            context.ZipCodeAssigned.Remove(item);
                        }
                    }
                    context.SaveChanges();

                    return Json(ReturnMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("SessionOver", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult DeleteSingleAdmin(string UserId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    userinfo objUserInfo = new userinfo();
                    string ReturnMsg = string.Empty;
                    objUserInfo = context.UserInfoes.Where(x => x.ID == UserId).FirstOrDefault();
                    if (objUserInfo != null)
                    {
                        objUserInfo.IsDeleted = true;
                        context.SaveChanges();
                        ReturnMsg = "Success";
                    }
                    else
                    {
                        ReturnMsg = "Error";
                    }
                    return Json(ReturnMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("SessionOver", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult DeleteMultipleAdmin(string UserId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    string[] Users = UserId.Split(',');
                    List<userinfo> objUserInfo = new List<userinfo>();
                    objUserInfo = (from user in context.UserInfoes
                                   where Users.Contains(user.ID)
                                   select user).ToList();
                    if (objUserInfo != null)
                    {
                        foreach (var item in objUserInfo)
                        {
                            item.IsDeleted = true;
                        }
                    }
                    context.SaveChanges();
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("SessionOver", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult DeleteMultipleUser(string UserId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    string[] Users = UserId.Split(',');
                    List<userinfo> objUserInfo = new List<userinfo>();
                    objUserInfo = (from user in context.UserInfoes
                                   where Users.Contains(user.ID)
                                   select user).ToList();
                    if (objUserInfo != null)
                    {
                        foreach (var item in objUserInfo)
                        {
                            item.IsDeleted = true;
                        }
                    }
                    context.SaveChanges();
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("SessionOver", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult DeleteMultipleInspector(string UserId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    string[] Users = UserId.Split(',');
                    List<userinfo> objUserInfo = new List<userinfo>();
                    objUserInfo = (from user in context.UserInfoes
                                   where Users.Contains(user.ID)
                                   select user).ToList();
                    if (objUserInfo != null)
                    {
                        foreach (var item in objUserInfo)
                        {
                            item.IsDeleted = true;
                        }
                    }
                    context.SaveChanges();
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("SessionOver", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetAssignedReport()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    tblsamplereport objSampleReport = context.tblsamplereport.FirstOrDefault();
                    _messageToShow.Message = "Success";
                    _messageToShow.Result = objSampleReport.ReportNo;
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    _messageToShow.Message = "Session Over";
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public JsonResult AssignRequest(string ReportNo)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    tblinspectionorder objInspectionOrder = context.tblinspectionorder.Where(x => x.ReportNo == ReportNo && x.InspectionStatus == "Completed").FirstOrDefault();
                    if (objInspectionOrder != null)
                    {
                        tblsamplereport objsamplereport = context.tblsamplereport.FirstOrDefault();
                        if (objsamplereport != null)
                        {
                            objsamplereport.ReportID = objInspectionOrder.InspectionOrderDetailId;
                            objsamplereport.ReportNo = objInspectionOrder.ReportNo;
                            objsamplereport.CreatedBy = Sessions.LoggedInUser.UserId;
                            objsamplereport.IsDeleted = false;
                            objsamplereport.IsActive = true;
                            objsamplereport.CreatedDate = System.DateTime.Now;
                            context.SaveChanges();
                            return Json("Success", JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json("NotExist", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("NotExist", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ShareYourExperiencr()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    List<ShareExprience> objExp = null;
                    objExp = (from exp in context.tblTestimonial
                              // join user in context.UserInfoes on exp.UserID equals user.ID
                              join s in context.tblstate on exp.StateId equals s.StateId
                              where exp.IsDeleted == false
                              select new ShareExprience
                              {
                                  ID = exp.ID,
                                  //   UserId = exp.UserID,
                                  FirstName = exp.FirstName,
                                  LastName = exp.LastName,
                                  State = s.StateName,
                                  Comments = exp.Comments,
                                  IsActive = exp.IsActive,
                                  ReportNo = exp.ReportNo,
                                  Role = exp.Role
                              }).ToList();
                    return View(objExp);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public JsonResult DeleteTestimonial(string Id)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    tbltestimonial objshare = context.tblTestimonial.Where(x => x.ID == Id).FirstOrDefault();
                    if (objshare != null)
                    {
                        objshare.IsDeleted = true;
                        // objshare.DeletedBy = Sessions.LoggedInUser.UserId;
                        objshare.DeletedDate = System.DateTime.Now;
                    }
                    context.SaveChanges();
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult EditTestimonial(string Id, string Comments, string IsActive)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    tbltestimonial objTest = context.tblTestimonial.Where(x => x.ID == Id).FirstOrDefault();
                    if (objTest != null)
                    {
                        objTest.Comments = Comments;
                        if (IsActive == "checked")
                            objTest.IsActive = true;
                        else
                            objTest.IsActive = false;
                        context.SaveChanges();
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json("ISession Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region Create New Inspection
        /// <summary>
        /// function to Create New Inspection Form
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateNewInspection(string Orderid)
        {

            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    NInspectionOrderDetails insectionOrderDetailPoco = new NInspectionOrderDetails();
                    insectionOrderDetailPoco.sellerAgent = new NSellerAgent();
                    insectionOrderDetailPoco.buyerAgent = new NBuyerAgent();
                    tblinspectionorderdetail objtblOrderDetail = context.tblinspectionorderdetail.Where(x => x.InspectionorderdetailsID == Orderid).FirstOrDefault();


                    if (objtblOrderDetail != null)
                    {
                        /*Requester Details*/
                        insectionOrderDetailPoco.RequestedFirstName = objtblOrderDetail.RequesterFName;
                        insectionOrderDetailPoco.RequestedLastName = objtblOrderDetail.RequesterLName;
                        insectionOrderDetailPoco.RequestedByAddress = objtblOrderDetail.RequesterAddress;
                        insectionOrderDetailPoco.RequestedByCity = objtblOrderDetail.RequesterCity;
                        insectionOrderDetailPoco.RequestedByState = objtblOrderDetail.RequesterState;
                        insectionOrderDetailPoco.RequestedByZip = objtblOrderDetail.RequesterZip;
                        insectionOrderDetailPoco.RequesterType = objtblOrderDetail.RequesterType;
                        insectionOrderDetailPoco.RequestedByEmailAddress = objtblOrderDetail.RequestedByEmailAddress;
                        insectionOrderDetailPoco.PhoneNumber = objtblOrderDetail.RequesterPhone;

                        /*Property Info*/
                        insectionOrderDetailPoco.PropertyAddress = objtblOrderDetail.PropertyAddress;
                        insectionOrderDetailPoco.OwnerPhoneNumber = objtblOrderDetail.OwnerPhone;
                        insectionOrderDetailPoco.PropertyAddressState = objtblOrderDetail.PropertyState;
                        insectionOrderDetailPoco.PropertyAddressCity = objtblOrderDetail.PropertyCity;
                        insectionOrderDetailPoco.PropertyAddressZip = objtblOrderDetail.PropertyZip;
                        insectionOrderDetailPoco.OwnerName = objtblOrderDetail.OwnerName;
                        insectionOrderDetailPoco.OwnerEmailAddress = objtblOrderDetail.PropertyEmail;
                        insectionOrderDetailPoco.PropertyDescription = objtblOrderDetail.PropertyDescription;
                        insectionOrderDetailPoco.PropertyUnit = objtblOrderDetail.Units;
                        insectionOrderDetailPoco.PropertySquareFootage = objtblOrderDetail.SqureFootage;
                        insectionOrderDetailPoco.PropertyAddition = objtblOrderDetail.Additions_Alterations;
                        insectionOrderDetailPoco.PropertyBedRoom = objtblOrderDetail.Bedrooms;
                        insectionOrderDetailPoco.PropertyBaths = objtblOrderDetail.Bathrooms;
                        insectionOrderDetailPoco.PropertyEstimatedMonthlyUtilites = objtblOrderDetail.EstimatedMonthlyUtilities;
                        insectionOrderDetailPoco.PropertyHomeAge = objtblOrderDetail.YearBuilt;
                        insectionOrderDetailPoco.PropertyRoofAge = objtblOrderDetail.RoofAge;
                        insectionOrderDetailPoco.Direction = objtblOrderDetail.Directions;

                        /*Buyer Agent Info*/

                        agentlibrary AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                        AgentLibraryinfo = context.AgentLibraries.FirstOrDefault(x => x.InspectionOrderID == Orderid && x.AgentType == "1");

                        if (AgentLibraryinfo != null)
                        {
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentFirstName = AgentLibraryinfo.AgentFirstName;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentLastName = AgentLibraryinfo.AgentLastName;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentOffice = AgentLibraryinfo.AgentOffice;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay = AgentLibraryinfo.AgentPhoneDay;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentStreetAddress = AgentLibraryinfo.AgentStreetAddress;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentState = AgentLibraryinfo.AgentState;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentCity = AgentLibraryinfo.AgentCity;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentZip = AgentLibraryinfo.AgentZip;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress = AgentLibraryinfo.AgentEmailAddress;
                        }
                        AgentLibraryinfo = context.AgentLibraries.FirstOrDefault(x => x.InspectionOrderID == Orderid && x.AgentType == "2");

                        /*Seller Agent*/
                        if (AgentLibraryinfo != null)
                        {
                            insectionOrderDetailPoco.sellerAgent.SellerAgentFirstName = AgentLibraryinfo.AgentFirstName;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentLastName = AgentLibraryinfo.AgentLastName;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentOffice = AgentLibraryinfo.AgentOffice;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay = AgentLibraryinfo.AgentPhoneDay;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentStreetAddress = AgentLibraryinfo.AgentStreetAddress;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentState = AgentLibraryinfo.AgentState;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentCity = AgentLibraryinfo.AgentCity;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentZip = AgentLibraryinfo.AgentZip;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress = AgentLibraryinfo.AgentEmailAddress;
                        }

                        tblinspectionorder objtblInspection = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == Orderid).FirstOrDefault();
                        if (objtblInspection != null)
                        {
                            insectionOrderDetailPoco.ScheduleInspectionDate = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == Orderid).FirstOrDefault().ScheduledDate;
                            insectionOrderDetailPoco.IsAcceptedOrRejected = objtblInspection.IsAcceptedOrRejected;
                            insectionOrderDetailPoco.IsRejectedByInspector = objtblInspection.IsRejectedByInspector;

                            if (insectionOrderDetailPoco.IsAcceptedOrRejected == true)
                            {
                                tblschedule objSchedule = context.tblSchedule.Where(x => x.InspectionOrderId == Orderid).FirstOrDefault();
                                if (objSchedule != null)
                                {
                                    insectionOrderDetailPoco.StartTime = objSchedule.StartTime;
                                    insectionOrderDetailPoco.EndTime = objSchedule.EndTime;
                                    insectionOrderDetailPoco.ScheduledDate = objSchedule.ScheduleDate;
                                }
                            }

                        }

                    }


                    #region Bind States
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "0";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    insectionOrderDetailPoco.StatesList = items;
                    insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                    insectionOrderDetailPoco.CityList = objCityList;

                    #endregion

                    #region Bind Property Description
                    List<NPropertyDescription> ObjPropertyDesc = new List<NPropertyDescription>();
                    ObjPropertyDesc = (from PropertyDesc in context.tblpropertydescription
                                       select new NPropertyDescription
                                       {
                                           PropertyDescriptionId = PropertyDesc.PropertyDescriptionId,
                                           Type = PropertyDesc.Type
                                       }).OrderBy(x => x.Type).ToList();
                    List<SelectListItem> PropertyDescItem = new List<SelectListItem>();
                    SelectListItem objSelectListItemProperty = new SelectListItem();
                    objSelectListItemProperty.Text = "--Select--";
                    objSelectListItemProperty.Value = "--Select--";
                    foreach (var t in ObjPropertyDesc)
                    {
                        SelectListItem s1 = new SelectListItem();
                        s1.Text = t.Type.ToString();
                        s1.Value = t.PropertyDescriptionId.ToString();
                        PropertyDescItem.Add(s1);
                    }
                    PropertyDescItem = PropertyDescItem.OrderBy(x => x.Text).ToList();
                    PropertyDescItem.Insert(0, objSelectListItemProperty);
                    insectionOrderDetailPoco.PropertyDescriptionList = PropertyDescItem;
                    #endregion
                    return View(insectionOrderDetailPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
        public JsonResult DeleteOrder(string OrderId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    tblinspectionorder objIO = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == OrderId).FirstOrDefault();
                    //tblinspectionorderdetail objIOD = context.tblinspectionorderdetail.Where(x => x.InspectionorderdetailsID == OrderId).FirstOrDefault();
                    if (objIO != null)
                    {
                        objIO.IsDeleted = true;
                        objIO.DeletedBy = Sessions.LoggedInUser.UserId;
                        context.SaveChanges();
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ChangeProfile()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    ViewBag.Email = Sessions.LoggedInUser.EmailAddress;
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "admin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult ChangeProfile(AdminProfile objAdminProfile)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    if (objAdminProfile != null)
                    {
                        if (context.UserInfoes.FirstOrDefault(x => x.EmailAddress == objAdminProfile.EmailAddress && x.ID != Sessions.LoggedInUser.UserId && x.IsDeleted == false) == null)
                        {
                            if (context.UserInfoes.Where(x => x.Password == objAdminProfile.OldPassword).FirstOrDefault() != null)
                            {
                                userinfo objuser = context.UserInfoes.Where(x => x.ID == Sessions.LoggedInUser.UserId).FirstOrDefault();
                                objuser.EmailAddress = objAdminProfile.EmailAddress;
                                objuser.Password = objAdminProfile.NewPassword;
                                context.SaveChanges();
                                return Json("Success", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("Old Password Not Matched", JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json("Email Already Exist", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult DeletePayment(string Id)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {
                    payment objpayment = context.Payments.Where(x => x.PaymentID == Id).FirstOrDefault();
                    if (objpayment != null)
                    {
                        objpayment.IsDeleted = true;
                        context.SaveChanges();
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult ManageOwners()
        {
            if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
            {
                List<Owners> objowner = (from owner in context.tblowner
                                         select new Owners
                                         {
                                             ID = owner.ID,
                                             Name = owner.Name,
                                             Designation = owner.Designation
                                         }).ToList();

                return View(objowner);
            }
            else
            {
                return View("Home", "admin");
            }

        }
        public ActionResult EditOwners(string Id)
        {
            if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
            {
                int Ownerid = Convert.ToInt32(Id);
                Owners objowner = (from owner in context.tblowner
                                   where owner.ID == Ownerid
                                   select new Owners
                                   {
                                       ID = owner.ID,
                                       Name = owner.Name,
                                       Designation = owner.Designation,
                                       Image = owner.Image
                                   }).FirstOrDefault();

                return View(objowner);
            }
            else
            {
                return View("Index", "Home");
            }
        }

        [HttpPost]
        public JsonResult EditOwners(Owners ObjOwner)
        {
            if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
            {
                string FileName = string.Empty;
                tblowner owner = context.tblowner.Where(x => x.ID == ObjOwner.ID).FirstOrDefault();
                owner.Name = ObjOwner.Name;
                owner.Designation = ObjOwner.Designation;
                if (ObjOwner.Image != null)
                {
                    if (ObjOwner.Image.ToLower().Contains("base64"))
                    {
                        FileName = ConvertToImage(ObjOwner.Image.Split(',')[1]);
                        owner.Image = FileName;
                    }
                    else
                        owner.Image = ObjOwner.Image;
                }
                context.SaveChanges();
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Session Over", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ApplicationLinks()
        {
            return View();
        }

        public JsonResult GetApplicationLinks(string PageName)
        {
            string Pageval = PageName.Split(' ')[0];
            List<ApplicationLinks> lstApplicationLinks = new List<ApplicationLinks>();
            lstApplicationLinks = (from k in context.applicationlink
                                   where k.PageName == Pageval
                                   select new ApplicationLinks
                                      {
                                          ID = k.Id,
                                          LinkName = k.LinkName,
                                          PageName = k.PageName,
                                          LinkAddress = k.LinkAddress,
                                          Type = k.Type
                                      }).ToList();

            _messageToShow.Result = RenderRazorViewToString("_ApplicationLinks", lstApplicationLinks);
            return Json(_messageToShow);
        }

        public JsonResult AddApplicationLinks(string PgName, string LName, string LAddress, string ClNum)
        {
            try
            {
                applicationlink objapplicationlink = new applicationlink();
                objapplicationlink.LinkAddress = LAddress;
                objapplicationlink.LinkName = LName;
                objapplicationlink.PageName = PgName;
                objapplicationlink.Type = 2;
                if (ClNum != null) { objapplicationlink.ColNum = Convert.ToInt32(ClNum); } else { objapplicationlink.ColNum = null; }  //ClNum != null ? Convert.ToInt32(ClNum) : null; //Convert.TClNum;
                context.applicationlink.Add(objapplicationlink);
                context.SaveChanges();

                _messageToShow.Result = "Success ! Record Saved Sucessfully !";

            }
            catch (Exception ex)
            {
                _messageToShow.Result = "Error ! Some problem occurred";
            }

            return Json(_messageToShow);
        }

        public JsonResult UpdateDeleteLinks(int LinkId, string status, string LinkDisplayName, string LinkAddress)
        {
            try
            {
                applicationlink objapplicationlink = new applicationlink();
                objapplicationlink = context.applicationlink.Where(x => x.Id == LinkId).FirstOrDefault();
                if (status == "update")
                {
                    objapplicationlink.LinkName = LinkDisplayName;
                    objapplicationlink.LinkAddress = LinkAddress;
                }
                else
                    context.applicationlink.Remove(objapplicationlink);

                context.SaveChanges();

                _messageToShow.Message = "Success";
                return Json(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult PreClosingChecklist()
        {
            UserInfoPoco userInfo = new UserInfoPoco();
            List<NState> objState = new List<NState>();
            objState = (from state in context.tblstate
                        where state.IsActive == true
                        select new NState
                        {
                            StateId = state.StateId,
                            StateName = state.StateName
                        }).OrderBy(x => x.StateName).ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            List<SelectListItem> objCityList = new List<SelectListItem>();
            List<SelectListItem> objZipCodeList = new List<SelectListItem>();

            SelectListItem objSelectListItem = new SelectListItem();
            objSelectListItem.Text = "--Select--";
            objSelectListItem.Value = "--Select--";
            objCityList.Add(objSelectListItem);
            objZipCodeList.Add(objSelectListItem);
            foreach (var t in objState)
            {
                SelectListItem s = new SelectListItem();
                s.Text = t.StateName.ToString();
                s.Value = t.StateId.ToString();
                items.Add(s);
            }
            items = items.OrderBy(x => x.Text).ToList();
            items.Insert(0, objSelectListItem);
            userInfo.StatesList = items;
            userInfo.ZipCodesList = objZipCodeList;
            userInfo.CityList = objCityList;
            return View(userInfo);
        }

        public JsonResult GetPreClosingList(int? zipcode)
        {
            List<PreclosingPropertyDetails> objListPreclosingPropertyDetails = (from m in context.tblinspectionorderdetail
                                                                                join k in context.Checklists
                                                                                on m.InspectionorderdetailsID equals k.InspectionOrderId
                                                                                where m.PropertyZip == zipcode
                                                                                select new PreclosingPropertyDetails
                                                                                {
                                                                                    InspectionOrderId = k.InspectionOrderId,
                                                                                    RequesterFirstName = m.RequesterFName,
                                                                                    RequesterLastName = m.RequesterLName,
                                                                                    Email = m.RequestedByEmailAddress,
                                                                                    Phone = m.RequesterPhone
                                                                                }).Distinct().ToList();


            _messageToShow.Result = RenderRazorViewToString("_PartialPreClosingList", objListPreclosingPropertyDetails);
            return Json(_messageToShow);
        }

        public ActionResult PreClosingList()
        {
            string Inspectionid = Request.Url.Segments[3].ToString();
            return View(context.Checklists.Where(x => x.InspectionOrderId == Inspectionid).ToList());
        }
    }
}

