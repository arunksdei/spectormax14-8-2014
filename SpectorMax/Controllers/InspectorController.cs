﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Mvc;
using SpectorMax.Models;
using SpectorMax.Entities.Classes;
using SpectorMaxDAL;
using System.IO;
using System.Drawing;
using System.Web.Script.Serialization;
using MySql.Data.MySqlClient;
using System.Configuration;
using Newtonsoft.Json;
namespace SpectorMax.Controllers
{
    public class InspectorController : BaseController
    {


        #region Global Variables
        /*Initilizing Context*/
        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _messageToShow = new MessageToShow();
        // static List<MainSectionList> mainSectionList = new List<MainSectionList>();
        // static List<SubSectionList> subSectionList = new List<SubSectionList>();
        List<string> imagesNameList = new List<string>();
        string connStr = ConfigurationManager.ConnectionStrings["SpectorMaxConnectionString"].ConnectionString;
        #endregion

        #region Index
        //
        // GET: /Inspector/
        public ActionResult Index()
        {
            if (Sessions.LoggedInUser != null)
                return View();
            else
                return RedirectToAction("Index", "Home");
        }
        #endregion

        #region ReportData

        /// <summary>
        /// Function to Show various reports to inspector
        /// </summary>
        /// <param name="inspectionOrderid"></param>
        /// <returns></returns>

        public ActionResult ReportData()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    string ReportType = string.Empty;

                    if (!string.IsNullOrEmpty(Request.QueryString["ReportType"].ToString()))
                        ReportType = Request.QueryString["ReportType"].ToString();

                    #region Total Sales
                    /*Report Type = Total Sales*/
                    if (ReportType == "TotalSales")
                    {
                        InspectorReport objInspectorReport = new InspectorReport();
                        List<Report> objReportTotalSales = new List<Report>();

                        objReportTotalSales = (from IO in context.InspectionOrders
                                               join IOD in context.InsectionOrderDetails
                                               on IO.InspectionOrderID equals IOD.InspectionOrderId
                                               where IO.InspectorID == Sessions.LoggedInUser.UserId
                                               join Pay in context.Payments
                                               on IO.InspectionOrderID equals Pay.ReportID
                                               select new Report
                                               {
                                                   OwnerFirstName = IOD.OwnerFirstName,
                                                   OwnerLastName = IOD.OwnerLastName,
                                                   StreetAddress = IOD.PropertyAddress,
                                                   City = IOD.PropertyAddressCity,
                                                   State = IOD.PropertyAddressState,
                                                   InspectionDate = IO.CreatedDate,
                                                   AmountReceived = Pay.PaymentAmount,
                                                   PaymentPlan = Pay.PaymentType,
                                               }).ToList();

                        objInspectorReport.ReportData = objReportTotalSales;
                        return View(objInspectorReport);
                    }
                    #endregion

                    #region Total Report Created and Inspection Reports
                    /*Report Type = Total Report Created and Inspection Reports*/
                    else if (ReportType == "TotalReportCreated" || ReportType == "InspectionReports" || ReportType == "InspectionCompleted")
                    {
                        /*Data filtered for completed report only*/
                        if (ReportType == "InspectionCompleted" || ReportType == "InspectionReports")
                        {
                            InspectorReport objInspectorReport = new InspectorReport();
                            List<Report> objListOfInspectorInspectionCompleted = new List<Report>();

                            objListOfInspectorInspectionCompleted = (from IO in context.InspectionOrders
                                                                     join IOD in context.InsectionOrderDetails
                                                                     on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                                     where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.InspectionStatus == "Completed"
                                                                     select new Report
                                                                     {
                                                                         OwnerFirstName = IOD.OwnerFirstName,
                                                                         OwnerLastName = IOD.OwnerLastName,
                                                                         StreetAddress = IOD.PropertyAddress,
                                                                         City = IOD.PropertyAddressCity,
                                                                         State = IOD.PropertyAddressState,
                                                                         InspectionDate = IO.CreatedDate,
                                                                         Inspectionstatus = IO.InspectionStatus,
                                                                         OrderID = IO.InspectionOrderID,
                                                                         ReportNo = IO.UniqueID
                                                                     }).ToList();

                            foreach (var item in objListOfInspectorInspectionCompleted)
                            {
                                var item1 = context.Payments.Where(x => x.ReportID == item.OrderID).ToList();
                                if (item1 != null)
                                {
                                    var Paymentplan = item1.Where(x => x.PaymentType == "HomeSeller").FirstOrDefault();
                                    if (Paymentplan != null)
                                        item.AmountReceived = context.Payments.Where(x => x.ReportID == item.OrderID).FirstOrDefault().PaymentAmount;
                                }
                            }

                            objInspectorReport.ReportData = objListOfInspectorInspectionCompleted;
                            return View(objInspectorReport);
                        }
                        else
                        {
                            InspectorReport objInspectorReport = new InspectorReport();
                            List<Report> objListOfInspectorTotalReportCreated = new List<Report>();

                            objListOfInspectorTotalReportCreated = (from IO in context.InspectionOrders
                                                                    join IOD in context.InsectionOrderDetails
                                                                    on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                                    join Pay in context.Payments on IO.InspectionOrderID equals Pay.ReportID
                                                                    where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.InspectionStatus == "Completed"
                                                                    select new Report
                                                                    {
                                                                        OwnerFirstName = IOD.OwnerFirstName,
                                                                        OwnerLastName = IOD.OwnerLastName,
                                                                        StreetAddress = IOD.PropertyAddress,
                                                                        City = IOD.PropertyAddressCity,
                                                                        PaymentPlan = Pay.PaymentType,
                                                                        State = IOD.PropertyAddressState,
                                                                        InspectionDate = IO.CreatedDate,
                                                                        Inspectionstatus = IO.InspectionStatus
                                                                    }).ToList();
                            objInspectorReport.ReportData = objListOfInspectorTotalReportCreated;
                            return View(objInspectorReport);
                        }
                    }
                    #endregion

                    #region Payment Reports
                    /*Report Type = Payment Reports*/
                    else if (ReportType == "PaymentReports")
                    {
                        InspectorReport objInspectorReport = new InspectorReport();
                        List<Report> objListOfInspectorPaymentReports = new List<Report>();

                        objListOfInspectorPaymentReports = (from IO in context.tblinspectionorder
                                                            join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                                            join Pay in context.Payments on IO.InspectionOrderDetailId equals Pay.ReportID
                                                            join UI in context.UserInfoes on IO.RequesterID equals UI.ID
                                                            join InsDtls in context.InspectorDetail on IO.InspectorID equals InsDtls.UserID
                                                            join UInfo in context.UserInfoes on IO.InspectorID equals UInfo.ID
                                                            join s in context.tblstate on IOD.PropertyState equals s.StateId
                                                            join c in context.tblcity on IOD.PropertyCity equals c.CityId
                                                            join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                                            where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.IsDeleted == false
                                                            select new Report
                                                            {
                                                                OwnerFirstName = IOD.RequesterFName,
                                                                OwnerLastName = IOD.RequesterLName,
                                                                StreetAddress = IOD.PropertyAddress,
                                                                City = c.CityName,
                                                                State = s.StateName,
                                                                InspectionDate = IO.ScheduledDate,
                                                                Zip = z.Zip,
                                                                /*Get Role Name*/
                                                                AmountReceived = Pay.PaymentAmount,
                                                                PaymentPlan = Pay.PaymentType,
                                                                date = Pay.PaymentDate,
                                                                ReportNo = IO.ReportNo,
                                                                InvoiceNo = Pay.InvoiceNo,
                                                                PaymentId = Pay.PaymentID,
                                                                /*Inspector Details*/
                                                                InspectorName = UInfo.FirstName + " " + UInfo.LastName,
                                                                InspectorPercentage = InsDtls.PaymentPercentage
                                                            }).ToList();

                        objInspectorReport.ReportData = objListOfInspectorPaymentReports;
                        return View(objInspectorReport);
                    }
                    #endregion

                    #region Inspection Request
                    /*Report Type = Inspection Request*/
                    else if (ReportType == "InspectionRequest")
                    {
                        InspectorReport objInspectorReport = new InspectorReport();
                        List<Report> objListOfInspectorInspectionRequest = new List<Report>();

                        objListOfInspectorInspectionRequest = (from IO in context.tblinspectionorder
                                                               join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                                               join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                                               join req in context.UserInfoes on IO.RequesterID equals req.ID

                                                               join state in context.tblstate on IOD.PropertyState equals state.StateId
                                                               join city in context.tblcity on IOD.PropertyCity equals city.CityId
                                                               join zip in context.tblzip on IOD.PropertyZip equals zip.ZipId
                                                               where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.InspectionStatus != "Completed" && IO.IsDeleted == false
                                                               && IO.IsAcceptedOrRejected == false
                                                               select new Report
                                                               {
                                                                   OrderID = IOD.InspectionorderdetailsID,
                                                                   OwnerFirstName = IOD.RequesterFName,
                                                                   OwnerLastName = IOD.RequesterLName,
                                                                   StreetAddress = IOD.PropertyAddress,
                                                                   City = city.CityName,
                                                                   State = state.StateName,
                                                                   InspectionDate = IO.ScheduledDate,
                                                                   isrequestfromAdmin = IO.AssignedByAdmin,

                                                                   IsAccepted = IO.IsAcceptedOrRejected,
                                                                   ReportNo = IO.ReportNo,
                                                                   Zip = zip.Zip,
                                                                   IsRejectedByInspector = IO.IsRejectedByInspector,
                                                                   IsAcceptedOrRejected = IO.IsAcceptedOrRejected
                                                               }).OrderByDescending(x => x.InspectionDate).ToList();

                        objInspectorReport.ReportData = objListOfInspectorInspectionRequest.OrderByDescending(x => x.InspectionDate).ToList();
                        return View(objInspectorReport);
                    }
                    #endregion

                    #region Archived Reports
                    /*Report Type = Archived Reports*/
                    else if (ReportType == "ArchivedReports")
                    {
                        InspectorReport objInspectorReport = new InspectorReport();
                        List<Report> objListOfInspectorArchivedReports = new List<Report>();
                        var currentDate = DateTime.Now.AddMonths(-6);
                        objListOfInspectorArchivedReports = (from IO in context.InspectionOrders
                                                             join IOD in context.InsectionOrderDetails
                                                             on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                             where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.CreatedDate < currentDate
                                                             select new Report
                                                             {
                                                                 OwnerFirstName = IOD.OwnerFirstName,
                                                                 OwnerLastName = IOD.OwnerLastName,
                                                                 StreetAddress = IOD.PropertyAddress,
                                                                 City = IOD.PropertyAddressCity,
                                                                 State = IOD.PropertyAddressState,
                                                                 InspectionDate = IO.CreatedDate,
                                                             }).ToList();

                        objInspectorReport.ReportData = objListOfInspectorArchivedReports;
                        return View(objInspectorReport);
                    }
                    #endregion

                    #region Report Viewed
                    /*Report Type = Archived Reports*/
                    else if (ReportType == "ReportViewed")
                    {
                        InspectorReport objInspectorReport = new InspectorReport();
                        List<Report> objListOfInspectorReportViewed = new List<Report>();

                        //objListOfInspectorReportViewed = (from IO in context.InspectionOrders
                        //                                  join IOD in context.InsectionOrderDetails
                        //                                  on IO.InspectionOrderID equals IOD.InspectionOrderId
                        //                                  where IO.InspectorID == Sessions.LoggedInUser.UserId
                        //                                  join rpt in context.ReportViewed
                        //                                  on IO.InspectionId equals rpt.ReportID
                        //                                  join UI in context.UserInfoes
                        //                                  on IO.RequesterID equals UI.ID
                        //                                  join rol in context.Roles
                        //                                  on UI.Role equals rol.RoleID
                        //                                  select new Report
                        //                                  {
                        //                                      OrderID = rpt.ReportID,
                        //                                      OwnerFirstName = IOD.OwnerFirstName,
                        //                                      OwnerLastName = IOD.OwnerLastName,
                        //                                      StreetAddress = IOD.PropertyAddress,
                        //                                      City = IOD.PropertyAddressCity,
                        //                                      State = IOD.PropertyAddressState,
                        //                                      InspectionDate = IO.CreatedDate,
                        //                                      Inspectionstatus = IO.InspectionStatus,
                        //                                      isrequestfromAdmin = IO.AssignedByAdmin,
                        //                                      roleName = rol.RoleName,
                        //                                      date = rpt.CreatedDate
                        //                                  }).ToList();


                        objListOfInspectorReportViewed = (from IO in context.InspectionOrders
                                                          join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                          join rpt in context.ReportViewed on IO.InspectionId equals rpt.ReportID
                                                          join UI in context.UserInfoes on rpt.UserID equals UI.ID
                                                          join rol in context.Roles on UI.Role equals rol.RoleID
                                                          where IO.InspectorID == Sessions.LoggedInUser.UserId
                                                          select new Report
                                                          {
                                                              OrderID = rpt.ReportID,
                                                              OwnerFirstName = IOD.OwnerFirstName,
                                                              OwnerLastName = IOD.OwnerLastName,
                                                              StreetAddress = IOD.PropertyAddress,
                                                              City = IOD.PropertyAddressCity,
                                                              State = IOD.PropertyAddressState,
                                                              InspectionDate = IO.CreatedDate,
                                                              Inspectionstatus = IO.InspectionStatus,
                                                              isrequestfromAdmin = IO.AssignedByAdmin,
                                                              roleName = rol.RoleName,
                                                              date = rpt.CreatedDate,
                                                              ReportViewdUser = UI.FirstName + " " + UI.LastName
                                                          }).ToList();



                        int i = 0;
                        foreach (var item in objListOfInspectorReportViewed)
                        {
                            List<DateTime?> lstdates = objListOfInspectorReportViewed.Where(x => x.OrderID == item.OrderID).Select(x => x.date).ToList();
                            List<RoleAndDate> roels = (from role in objListOfInspectorReportViewed
                                                       where role.OrderID == item.OrderID
                                                       select new RoleAndDate
                                                       {
                                                           UsrRole = role.roleName,
                                                           fromdate = role.date,
                                                           UserName = role.ReportViewdUser
                                                       }).ToList();
                            objListOfInspectorReportViewed[i].fromandtodate = lstdates;
                            objListOfInspectorReportViewed[i].RoleDate = roels;
                            i++;
                        }
                        objInspectorReport.ReportData = (objListOfInspectorReportViewed.Select(r => r.OrderID)
                                                        .Distinct()
                                                        .Select(r => objListOfInspectorReportViewed.FirstOrDefault(t => t.OrderID == r)).ToList());
                        return View(objInspectorReport);
                    }
                    #endregion

                    #region Notification
                    /*Report Type = NotificationForInspector*/
                    else if (ReportType == "NotificationForInspector")
                    {
                        InspectorReport objInspectorReport = new InspectorReport();
                        List<PropertyAddress> objList_PropertyAddress = new List<PropertyAddress>();
                        List<string> _orderId = new List<string>();
                        List<inspectionorder> ArrayOfOrderPlaced = context.InspectionOrders.Where(x => x.InspectorID == Sessions.LoggedInUser.UserId).ToList();
                        int Count = 0;
                        bool IsExist = false;
                        //  x.PhotoUrl != null &&
                        foreach (var item in ArrayOfOrderPlaced)
                        {
                            IsExist = context.PhotoLibraries.Any(x => x.InspectionOrderID == item.InspectionOrderID && x.CommentedBy != Sessions.LoggedInUser.UserId && x.Comments != null && (x.IsRead == false || x.IsRead == null));
                            if (IsExist)
                            { Count++; IsExist = false; _orderId.Add(item.InspectionOrderID); }
                        }
                        foreach (var item in _orderId)
                        {
                            PropertyAddress objPropertyAddress = new PropertyAddress();

                            var Property_Address = context.InsectionOrderDetails.Where(x => x.InspectionOrderId == item).FirstOrDefault();
                            objPropertyAddress.OwnerFirstName = Property_Address.OwnerFirstName;
                            objPropertyAddress.OwnerLastName = Property_Address.OwnerLastName;
                            objPropertyAddress.StreetAddress = Property_Address.StreetAddess;
                            objPropertyAddress.City = Property_Address.City;
                            objPropertyAddress.State = Property_Address.State;
                            objPropertyAddress.Zip = Property_Address.Zip;
                            objPropertyAddress.OrderID = item;
                            objPropertyAddress.InspectionDate = Property_Address.ScheduleInspectionDate;
                            objList_PropertyAddress.Add(objPropertyAddress);
                        }
                        objInspectorReport.objPropertyAddress = objList_PropertyAddress;
                        return View(objInspectorReport);
                    }
                    #endregion

                    #region Updatable Report
                    else if (ReportType == "UpdateReport")
                    {
                        List<PropertyAddress> objList_PropertyAddress = new List<PropertyAddress>();
                        objList_PropertyAddress = (from report in context.tblPhotoLibrary
                                                   join IOD in context.InsectionOrderDetails on report.InspectionReportID equals IOD.InspectionOrderId
                                                   join IO in context.InspectionOrders on IOD.InspectionOrderId equals IO.InspectionOrderID
                                                   where IO.InspectorID == Sessions.LoggedInUser.UserId && report.IsApproved != false && report.IsApproved != true
                                                   select new PropertyAddress
                                                   {
                                                       StreetAddress = IOD.PropertyAddress,
                                                       ReportNo = IO.ReportNo,
                                                       City = IOD.PropertyAddressCity,
                                                       State = IOD.PropertyAddressState,
                                                       Zip = IOD.PropertyAddressZip,
                                                       OrderID = IOD.InspectionOrderId
                                                   }).ToList();
                        return View(objList_PropertyAddress);
                    }

                    #endregion

                    #region ViewAndEditReport
                    else if (ReportType == "ViewAndEditReport")
                    {
                        MySqlConnection conn = new MySqlConnection(connStr);
                        conn.Open();
                        MySqlCommand cmd = new MySqlCommand("ViewAndEditReport", conn);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("UserID", Sessions.LoggedInUser.UserId);
                        MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        InspectorReport objInspectorReport = new InspectorReport();
                        List<Report> objListOfInspectorTotalReportCreated = new List<Report>();
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            objListOfInspectorTotalReportCreated = ds.Tables[0].AsEnumerable().Select(datarow =>
                               new Report
                               {
                                   OwnerFirstName = datarow["OwnerFirstName"].ToString(),
                                   OwnerLastName = datarow["OwnerLastName"].ToString(),
                                   StreetAddress = datarow["PropertyAddress"].ToString(),
                                   City = datarow["PropertyCity"].ToString(),
                                   State = datarow["PropertyState"].ToString(),
                                   Zip = Convert.ToInt32(datarow["PropertyZip"]),
                                   InspectionDate = Convert.ToDateTime(datarow["ScheduledDate"]),
                                   ReportNo = datarow["ReportNo"].ToString(),
                                   OrderID = datarow["InspectionOrderDetailsId"].ToString(),

                                   PaymentAmount = Convert.ToDouble(datarow["PaymentAmount"])
                               }).ToList();


                        }
                        objInspectorReport.ReportData = objListOfInspectorTotalReportCreated;
                        return View(objInspectorReport);
                    #endregion
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region ViewAllComments
        /// <summary>
        /// Function to Show all the updates made by seller
        /// </summary>
        /// <param name="inspectionOrderid"></param>
        /// <returns></returns>

        public ActionResult ViewAllComments(string inspectionOrderid)
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    Session["CountPhotoLibrary"] = "1";
                    DetailsReportCommonPoco objDetailsReportCommonPoco = null;
                    ViewBag.InspectionOrderId = inspectionOrderid;

                    var Section = (from SubSectionStatus in context.SubSectionStatu
                                   join Msection in context.MainSections on SubSectionStatus.SectionID equals Msection.ID
                                   join Ssection in context.SubSections on SubSectionStatus.SubSectionID equals Ssection.ID
                                   join PG in context.PhotoLibraries on SubSectionStatus.ID equals PG.InpectionReportFID
                                   where SubSectionStatus.InspectionOrderID == inspectionOrderid &&
                                   SubSectionStatus.Status != true && SubSectionStatus.IsUpdated == true
                                   select new
                                   {
                                       SubSectionStatus.SectionID,
                                       SubSectionStatus.SubSectionID,
                                       Msection.SectionName,
                                       Ssection.SubSectionName
                                   }).OrderByDescending(x => x.SectionID).Skip(0).Take(1).ToList();
                    foreach (var item in Section)
                    {
                        List<photolibrary> objPhotoLibrary = context.PhotoLibraries.Where(x => x.InspectionOrderID == inspectionOrderid && x.SectionID == item.SectionID && x.SubSectionId == item.SubSectionID && x.CommentedBy != Sessions.LoggedInUser.UserId && x.Comments != null && x.PhotoUrl != null && x.IsRead == false).OrderByDescending(x => x.CreatedDate).Take(4).ToList();
                        if (objPhotoLibrary.Count > 0)
                        {
                            objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                            objDetailsReportCommonPoco.InspectionOrderID = inspectionOrderid;
                            objDetailsReportCommonPoco.SectionID = objPhotoLibrary[0].SectionID;
                            objDetailsReportCommonPoco.SubSectionID = objPhotoLibrary[0].SubSectionId;
                            objDetailsReportCommonPoco.Comments = objPhotoLibrary[0].Comments;
                            objDetailsReportCommonPoco.CommentedBy = objPhotoLibrary[0].CommentedBy;
                            objDetailsReportCommonPoco.ObjImagesNote = objPhotoLibrary.Select(y => new ImagesNote { ID = y.ID, ImageUrl = y.PhotoUrl }).ToList();
                            objDetailsReportCommonPoco.SectionName = item.SectionName;
                            objDetailsReportCommonPoco.SubSectionStatusId = objPhotoLibrary[0].InpectionReportFID;
                            objDetailsReportCommonPoco.SubSectionName = item.SubSectionName;
                        }
                    }
                    return View(objDetailsReportCommonPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Maintainenance Required
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inspectionOrderid"></param>
        /// <returns></returns>

        [HttpPost]
        public JsonResult ViewAllCommentsPost(string inspectionOrderid)
        {

            try
            {
                DetailsReportCommonPoco objDetailsReportCommonPoco = null;
                ViewBag.InspectionOrderId = inspectionOrderid;
                int CountPhotoLibrary = Convert.ToInt32(Session["CountPhotoLibrary"]);
                var Section = (from SubSectionStatus in context.SubSectionStatu
                               join Msection in context.MainSections on SubSectionStatus.SectionID equals Msection.ID
                               join Ssection in context.SubSections on SubSectionStatus.SubSectionID equals Ssection.ID
                               where SubSectionStatus.InspectionOrderID == inspectionOrderid &&
                               SubSectionStatus.Status != true && SubSectionStatus.IsUpdated == true
                               select new
                               {
                                   SubSectionStatus.SectionID,
                                   SubSectionStatus.SubSectionID,
                                   Msection.SectionName,
                                   Ssection.SubSectionName
                               }).OrderByDescending(x => x.SectionID).Skip(CountPhotoLibrary).Take(1).ToList();
                if (Section.Any())
                {
                    foreach (var item in Section)
                    {
                        List<photolibrary> objPhotoLibrary = context.PhotoLibraries.Where(x => x.InspectionOrderID == inspectionOrderid && x.SectionID == item.SectionID && x.SubSectionId == item.SubSectionID && x.CommentedBy != Sessions.LoggedInUser.UserId && x.Comments != null && x.PhotoUrl != null && x.IsRead == false).OrderByDescending(x => x.CreatedDate).Take(4).ToList();


                        if (objPhotoLibrary.Count > 0)
                        {
                            objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                            objDetailsReportCommonPoco.InspectionOrderID = inspectionOrderid;
                            objDetailsReportCommonPoco.SectionID = objPhotoLibrary[0].SectionID;
                            objDetailsReportCommonPoco.SubSectionID = objPhotoLibrary[0].SubSectionId;
                            objDetailsReportCommonPoco.Comments = objPhotoLibrary[0].Comments;
                            objDetailsReportCommonPoco.CommentedBy = objPhotoLibrary[0].CommentedBy;
                            objDetailsReportCommonPoco.ObjImagesNote = objPhotoLibrary.Select(y => new ImagesNote { ID = y.ID, ImageUrl = y.PhotoUrl }).ToList();
                            objDetailsReportCommonPoco.SectionName = item.SectionName;
                            objDetailsReportCommonPoco.SubSectionStatusId = objPhotoLibrary[0].InpectionReportFID;
                            objDetailsReportCommonPoco.SubSectionName = item.SubSectionName;
                        }
                    }
                }
                if (objDetailsReportCommonPoco != null)
                {
                    CountPhotoLibrary++;
                    Session["CountPhotoLibrary"] = CountPhotoLibrary;
                    _messageToShow.Result = RenderRazorViewToString("_PartialViewAllcomments", objDetailsReportCommonPoco);
                }
                else
                {
                    _messageToShow.Message = "No More Data";
                }
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
            }

            return ReturnJson(_messageToShow);
        }
        #endregion

        #region SaveCommentsByInspector

        /// <summary>
        /// Function to save inspector comments in to photolibrary 
        /// </summary>
        /// <param name="inspectionOrderid"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="comments"></param>
        /// <param name="isRejectedOrAccepted"></param>
        ///<param name="imagesUrl"></param>
        /// <returns></returns>

        [HttpPost]
        public JsonResult SaveCommentsByInspector(string inspectionOrderId, string sectionId, string subSectionId, string comments, bool isRejectedOrAccepted, string imagesUrl, string SubSectionStatusId)
        {
            try
            {

                string[] imagesUrls = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<string[]>(imagesUrl);
                for (int i = 0; i < imagesUrls.Length; i++)
                {
                    photolibrary objPhotoLibrary = new photolibrary();
                    objPhotoLibrary.ID = Guid.NewGuid().ToString();
                    objPhotoLibrary.InspectionOrderID = inspectionOrderId;
                    objPhotoLibrary.SectionID = sectionId;
                    objPhotoLibrary.SubSectionId = subSectionId;
                    objPhotoLibrary.CommentedBy = Sessions.LoggedInUser.UserId;
                    //objPhotoLibrary.PhotoUrl = ConvertToImage(imagesUrls[i].Split(',')[1]);
                    objPhotoLibrary.PhotoUrl = imagesUrls[i];
                    objPhotoLibrary.IsRead = false;
                    objPhotoLibrary.Comments = comments;
                    objPhotoLibrary.AcceptedOrRejected = isRejectedOrAccepted;
                    objPhotoLibrary.CreatedDate = DateTime.Now;
                    objPhotoLibrary.ModifiedDate = DateTime.Now;
                    objPhotoLibrary.InpectionReportFID = SubSectionStatusId;
                    context.PhotoLibraries.Add(objPhotoLibrary);
                    context.SaveChanges();
                }
                /*Update Status of report in SubSectionStatus--Start*/
                if (isRejectedOrAccepted)
                {
                    subsectionstatu objSubsection = new subsectionstatu();
                    //objSubsection = context.SubSectionStatu.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId && x.SectionID == sectionId && x.SubSectionID == subSectionId);
                    objSubsection = context.SubSectionStatu.FirstOrDefault(x => x.ID == SubSectionStatusId);
                    objSubsection.Status = true;
                    context.SaveChanges();
                }
                /*End*/
                _messageToShow.Message = "Success";
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
            }
            return Json(_messageToShow);
        }
        #endregion

        #region ManageProfile
        /// <summary>
        /// Function to Manage Inspector Profile
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageProfile()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    userinfo objuserinfo = new userinfo();
                    InspectorDetails objInspectorDetail = new InspectorDetails();
                    objuserinfo = context.UserInfoes.Where(x => x.ID == Sessions.LoggedInUser.UserId).FirstOrDefault();
                    objInspectorDetail.FirstName = objuserinfo.FirstName;
                    objInspectorDetail.LastName = objuserinfo.LastName;
                    objInspectorDetail.Address = objuserinfo.Address;
                    objInspectorDetail.Address2 = objuserinfo.AddressLine2;
                    objInspectorDetail.StateId = objuserinfo.StateId;
                    objInspectorDetail.StreetAddress = objuserinfo.StreetAddress;
                    objInspectorDetail.CityId = objuserinfo.CityId;
                    objInspectorDetail.Country = objuserinfo.Country;
                    objInspectorDetail.Password = objuserinfo.Password;
                    objInspectorDetail.ConfirmPassword = objuserinfo.Password;
                    objInspectorDetail.EmailAddress = objuserinfo.EmailAddress;
                    objInspectorDetail.dateofbirth = objuserinfo.DateOfBirth;
                    objInspectorDetail.DOB = Convert.ToDateTime(objuserinfo.DateOfBirth).ToString("MM-yyyy");
                    objInspectorDetail.PhoneNo = objuserinfo.PhoneNo;
                    var inspectorDetail = context.InspectorDetail.FirstOrDefault(x => x.UserID == Sessions.LoggedInUser.UserId);
                    if (inspectorDetail != null)
                    {
                        objInspectorDetail.PhotoURL = inspectorDetail.PhotoURL;
                        objInspectorDetail.ExperienceTraining1 = inspectorDetail.ExperienceTraining1;
                        objInspectorDetail.ExperienceTraining2 = inspectorDetail.ExperienceTraining2;
                        objInspectorDetail.ExperienceTraining3 = inspectorDetail.ExperienceTraining3;
                        objInspectorDetail.Certification1 = inspectorDetail.Certification1;
                        objInspectorDetail.Certification2 = inspectorDetail.Certification2;
                        objInspectorDetail.Certification3 = inspectorDetail.Certification3;
                    }
                    else
                    {
                        objInspectorDetail.PhotoURL = "";
                        objInspectorDetail.ExperienceTraining1 = "";
                        objInspectorDetail.ExperienceTraining2 = "";
                        objInspectorDetail.ExperienceTraining3 = "";
                        objInspectorDetail.Certification1 = "";
                        objInspectorDetail.Certification2 = "";
                        objInspectorDetail.Certification3 = "";

                    }
                    #region Bind States
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "0";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    objInspectorDetail.StatesList = items;
                    objInspectorDetail.CityList = objCityList;

                    #endregion
                    return View(objInspectorDetail);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ManageProfile(HttpPost)
        /// <summary>
        /// Function to Manage Inspector Profile on update
        /// <param name="objInspectorDetail"></param>
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageProfile(InspectorDetails objInspectorDetail)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    userinfo objuserinfo = new userinfo();
                    objuserinfo = context.UserInfoes.FirstOrDefault(x => x.ID == Sessions.LoggedInUser.UserId);
                    objuserinfo.FirstName = objInspectorDetail.FirstName;
                    objuserinfo.LastName = objInspectorDetail.LastName;
                    objuserinfo.Address = objInspectorDetail.Address;
                    objuserinfo.AddressLine2 = objInspectorDetail.Address2;
                    objuserinfo.StateId = objInspectorDetail.StateId;
                    objuserinfo.StreetAddress = objInspectorDetail.StreetAddress;
                    objuserinfo.CityId = objInspectorDetail.CityId;
                    objuserinfo.Country = objInspectorDetail.Country;
                    objuserinfo.EmailAddress = objInspectorDetail.EmailAddress;
                    objuserinfo.Password = objInspectorDetail.Password;

                    DateTime? dt = CreateDate(objInspectorDetail.DOB);
                    objuserinfo.DateOfBirth = dt;
                    //  objuserinfo.DateOfBirth = objInspectorDetail.dateofbirth;
                    objuserinfo.PhoneNo = objInspectorDetail.PhoneNo;
                    objuserinfo.ModifiedDate = DateTime.Now;
                    context.SaveChanges();

                    inspectordetail objIns_Profile = new inspectordetail();
                    objIns_Profile = context.InspectorDetail.FirstOrDefault(x => x.UserID == Sessions.LoggedInUser.UserId);
                    if (objIns_Profile != null)
                    {
                        string FileName = string.Empty;
                        if (objInspectorDetail.Certification1 != null)
                        {
                            if (objInspectorDetail.Certification1.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(objInspectorDetail.Certification1.Split(',')[1]);
                                objInspectorDetail.Certification1 = FileName;
                                objIns_Profile.Certification1 = objInspectorDetail.Certification1;
                            }
                            else
                                objIns_Profile.Certification1 = objInspectorDetail.Certification1;
                        }

                        if (objInspectorDetail.Certification2 != null)
                        {
                            if (objInspectorDetail.Certification2.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(objInspectorDetail.Certification2.Split(',')[1]);
                                objInspectorDetail.Certification2 = FileName;
                                objIns_Profile.Certification2 = objInspectorDetail.Certification2;
                            }
                            else
                                objIns_Profile.Certification2 = objInspectorDetail.Certification2;
                        }

                        if (objInspectorDetail.Certification3 != null)
                        {

                            if (objInspectorDetail.Certification3.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(objInspectorDetail.Certification3.Split(',')[1]);
                                objInspectorDetail.Certification3 = FileName;
                                objIns_Profile.Certification3 = objInspectorDetail.Certification3;
                            }
                            else
                                objIns_Profile.Certification3 = objInspectorDetail.Certification3;
                        }
                        objIns_Profile.ExperienceTraining1 = objInspectorDetail.ExperienceTraining1;
                        objIns_Profile.ExperienceTraining2 = objInspectorDetail.ExperienceTraining2;
                        objIns_Profile.ExperienceTraining3 = objInspectorDetail.ExperienceTraining3;

                        if (objInspectorDetail.PhotoURL != null)
                        {

                            if (objInspectorDetail.PhotoURL.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(objInspectorDetail.PhotoURL.Split(',')[1]);
                                objInspectorDetail.PhotoURL = FileName;
                                objIns_Profile.PhotoURL = objInspectorDetail.PhotoURL;
                            }
                            else
                                objIns_Profile.PhotoURL = objInspectorDetail.PhotoURL;
                        }

                        objIns_Profile.ModifiedDate = DateTime.Now;
                        context.SaveChanges();
                        ViewData["MessageType"] = "success";
                    }
                    else
                    {
                        objIns_Profile = new inspectordetail();
                        objIns_Profile.InspectorDetailsID = Guid.NewGuid().ToString();
                        objIns_Profile.UserID = Sessions.LoggedInUser.UserId;

                        string FileName = string.Empty;
                        if (objInspectorDetail.Certification1 != null)
                        {
                            if (objInspectorDetail.Certification1.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(objInspectorDetail.Certification1.Split(',')[1]);
                                objInspectorDetail.Certification1 = FileName;
                                objIns_Profile.Certification1 = objInspectorDetail.Certification1;
                            }
                            else
                                objIns_Profile.Certification1 = objInspectorDetail.Certification1;
                        }

                        if (objInspectorDetail.Certification2 != null)
                        {
                            if (objInspectorDetail.Certification2.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(objInspectorDetail.Certification2.Split(',')[1]);
                                objInspectorDetail.Certification2 = FileName;
                                objIns_Profile.Certification2 = objInspectorDetail.Certification2;
                            }
                            else
                                objIns_Profile.Certification2 = objInspectorDetail.Certification2;
                        }

                        if (objInspectorDetail.Certification3 != null)
                        {

                            if (objInspectorDetail.Certification3.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(objInspectorDetail.Certification3.Split(',')[1]);
                                objInspectorDetail.Certification3 = FileName;
                                objIns_Profile.Certification3 = objInspectorDetail.Certification3;
                            }
                            else
                                objIns_Profile.Certification3 = objInspectorDetail.Certification3;
                        }

                        objIns_Profile.PhotoURL = objInspectorDetail.PhotoURL;
                        objIns_Profile.Certification1 = objInspectorDetail.Certification1;
                        objIns_Profile.Certification2 = objInspectorDetail.Certification2;
                        objIns_Profile.Certification3 = objInspectorDetail.Certification3;

                        if (objInspectorDetail.PhotoURL != null)
                        {

                            if (objInspectorDetail.PhotoURL.ToLower().Contains("base64"))
                            {
                                FileName = ConvertToImage(objInspectorDetail.PhotoURL.Split(',')[1]);
                                objInspectorDetail.PhotoURL = FileName;
                                objIns_Profile.PhotoURL = objInspectorDetail.PhotoURL;
                            }
                            else
                                objIns_Profile.PhotoURL = objInspectorDetail.PhotoURL;
                        }

                        objIns_Profile.ExperienceTraining1 = objInspectorDetail.ExperienceTraining1;
                        objIns_Profile.ExperienceTraining2 = objInspectorDetail.ExperienceTraining2;
                        objIns_Profile.ExperienceTraining3 = objInspectorDetail.ExperienceTraining3;
                        objIns_Profile.CreatedDate = DateTime.Now;
                        objIns_Profile.ModifiedDate = DateTime.Now;
                        context.InspectorDetail.Add(objIns_Profile);
                        context.SaveChanges();
                        ViewData["MessageType"] = "success";
                    }

                }
                else
                {
                    ViewData["MessageType"] = "error";
                    var AllError = ModelState.Values.SelectMany(x => x.Errors).ToArray();
                }
                #region Bind States
                List<NState> objState = new List<NState>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "0";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                objInspectorDetail.StatesList = items;
                objInspectorDetail.CityList = objCityList;

                #endregion
                return View(objInspectorDetail);
            }
            catch (Exception ex)
            {
                ViewData["MessageType"] = "error";
                return null;

            }


        }


        private DateTime? CreateDate(string Dob)
        {

            DateTime dt = new DateTime(Convert.ToInt32(Dob.Split('-')[1]), Convert.ToInt32(Dob.Split('-')[0]), 1);
            return dt;
        }
        #endregion

        #region ConvertToImage
        /// <summary>
        /// function to convert image
        /// <param name="base64"></param>
        /// </summary>
        /// <returns></returns>
        /// 
        public string ConvertToImage(string base64)
        {
            try
            {
                string FileName = string.Empty;

                using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(base64)))
                {
                    using (Bitmap bm2 = new Bitmap(ms))
                    {
                        FileName = Guid.NewGuid().ToString() + ".jpg";
                        bm2.Save(Server.MapPath(@"~\Images\ProfileImgAndCertiticate\") + FileName);
                    }
                }
                return FileName;
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion

        #region UploadBatchDataFile
        /// <summary>
        /// POST: method for upload profile images for Inspector.
        /// <param name="qqfile"></param>
        /// </summary>
        /// <returns></returns>
        /// 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UploadBatchDataFile(HttpPostedFileBase qqfile)
        {
            try
            {

                if (qqfile != null && qqfile.ContentLength > 0)
                {
                    //Below line get Extension of file and remove orignal file name.
                    //string fileName = Guid.NewGuid().ToString() + Path.GetExtension(qqfile.FileName);

                    string fileName = Guid.NewGuid().ToString() + "_" + qqfile.FileName;
                    var path = Path.Combine(Server.MapPath("~/Images/ProfileImgAndCertiticate"), fileName);
                    imagesNameList.Add(fileName);
                    qqfile.SaveAs(@path);
                    // further processing on file can be done here
                    return Json(new { success = true, Imagesurl = imagesNameList });
                    // return Json(new { success = true, Imagesurl = imagesName });               
                }
                else
                {
                    // this works for Firefox, Chrome
                    var filename = Request["qqfile"];
                    if (!string.IsNullOrEmpty(filename))
                    {
                        //Below line get Extension of file and remove orignal file name.
                        //string newFileName = Guid.NewGuid().ToString() + Path.GetExtension(qqfile.FileName);

                        string newFileName = Guid.NewGuid().ToString() + "_" + qqfile.FileName;
                        filename = Path.Combine(Server.MapPath("~/Images/ProfileImgAndCertiticate"), newFileName);
                        //using (var output = File.Create(filename))
                        //{
                        //    Request.InputStream.CopyTo(output);
                        //}
                        // further processing on file can be done here
                        return Json(new { success = true, Imagesurl = imagesNameList });

                    }
                }
                return Json(new { success = false });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region FilterByDate
        /// <summary>
        /// function used in reports to filter by date
        /// </summary>
        /// <param name="ReportType"></param>
        /// <param name="endDate"></param>
        /// <param name="startDate"></param>
        /// <returns></returns>
        /// 
        public JsonResult FilterByDate(string ReportType, string startDate, string endDate)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    InspectorReport objInspectorReport = new InspectorReport();
                    List<Report> objReportTotalSales = new List<Report>();

                    Dates obj = new Dates();
                    obj.fromdate = startDate;
                    obj.todate = endDate;
                    objInspectorReport.fromandtodate = obj;

                    if (startDate != null || startDate != "")
                    {
                        DateTime? _enddate = null;
                        DateTime _startdate = DateTime.Parse(startDate).Date;
                        if (endDate == "" || endDate == null)
                            _enddate = DateTime.Now;
                        else
                            _enddate = DateTime.Parse(endDate).Date.AddHours(23);



                        #region Total Sales
                        /*Report Type = Total Sales*/
                        if (ReportType == "TotalSales")
                        {


                            objReportTotalSales = (from IO in context.InspectionOrders
                                                   join IOD in context.InsectionOrderDetails
                                                   on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                   where IO.InspectorID == Sessions.LoggedInUser.UserId
                                                   join Pay in context.Payments
                                                   on IO.InspectionOrderID equals Pay.ReportID
                                                   where IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                                   select new Report
                                                   {
                                                       OwnerFirstName = IOD.OwnerFirstName,
                                                       OwnerLastName = IOD.OwnerLastName,
                                                       StreetAddress = IOD.PropertyAddress,
                                                       City = IOD.PropertyAddressCity,
                                                       State = IOD.PropertyAddressState,
                                                       InspectionDate = IO.CreatedDate,
                                                       AmountReceived = Pay.PaymentAmount,
                                                   }).ToList();

                            objInspectorReport.ReportData = objReportTotalSales;
                            _messageToShow.Result = RenderRazorViewToString("_PartialTotalSales", objInspectorReport);
                            return ReturnJson(_messageToShow);
                        }

                        #endregion

                        #region Total Report Created and Inspection Reports and InspectionCompleted
                        /*Report Type = Total Report Created and Inspection Reports*/
                        else if (ReportType == "TotalReportCreated" || ReportType == "InspectionReports" || ReportType == "InspectionCompleted")
                        {
                            /*Data filtered for completed report only*/
                            if (ReportType == "InspectionCompleted" || ReportType == "InspectionReports")
                            {

                                List<Report> objListOfInspectorInspectionCompleted = new List<Report>();

                                objListOfInspectorInspectionCompleted = (from IO in context.InspectionOrders
                                                                         join IOD in context.InsectionOrderDetails
                                                                         on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                                         where IO.InspectorID == Sessions.LoggedInUser.UserId
                                                                         && IO.InspectionStatus == "Completed" && IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                                                         select new Report
                                                                         {
                                                                             OwnerFirstName = IOD.OwnerFirstName,
                                                                             OwnerLastName = IOD.OwnerLastName,
                                                                             StreetAddress = IOD.PropertyAddress,
                                                                             City = IOD.PropertyAddressCity,
                                                                             State = IOD.PropertyAddressState,
                                                                             InspectionDate = IO.CreatedDate,
                                                                             Inspectionstatus = IO.InspectionStatus
                                                                         }).ToList();
                                objInspectorReport.ReportData = objListOfInspectorInspectionCompleted;
                                _messageToShow.Result = RenderRazorViewToString("_PartialInspectionReports", objInspectorReport);
                                return ReturnJson(_messageToShow);
                            }
                            else
                            {

                                List<Report> objListOfInspectorTotalReportCreated = new List<Report>();

                                objListOfInspectorTotalReportCreated = (from IO in context.InspectionOrders
                                                                        join IOD in context.InsectionOrderDetails
                                                                        on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                                        where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.InspectionStatus == "Completed"
                                                                        && IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                                                        select new Report
                                                                        {
                                                                            OwnerFirstName = IOD.OwnerFirstName,
                                                                            OwnerLastName = IOD.OwnerLastName,
                                                                            StreetAddress = IOD.PropertyAddress,
                                                                            City = IOD.PropertyAddressCity,
                                                                            State = IOD.PropertyAddressState,
                                                                            InspectionDate = IO.CreatedDate,
                                                                            Inspectionstatus = IO.InspectionStatus
                                                                        }).ToList();
                                objInspectorReport.ReportData = objListOfInspectorTotalReportCreated;
                                _messageToShow.Result = RenderRazorViewToString("_PartialTotalReportCreated", objInspectorReport);
                                return ReturnJson(_messageToShow);
                            }
                        }
                        #endregion

                        #region Archived Reports
                        /*Report Type = Archived Reports*/
                        else if (ReportType == "ArchivedReports")
                        {

                            List<Report> objListOfInspectorArchivedReports = new List<Report>();
                            var currentDate = DateTime.Now.AddMonths(-6);
                            objListOfInspectorArchivedReports = (from IO in context.InspectionOrders
                                                                 join IOD in context.InsectionOrderDetails
                                                                 on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                                 where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.CreatedDate < currentDate
                                                                 where IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                                                 select new Report
                                                                 {
                                                                     OwnerFirstName = IOD.OwnerFirstName,
                                                                     OwnerLastName = IOD.OwnerLastName,
                                                                     StreetAddress = IOD.PropertyAddress,
                                                                     City = IOD.PropertyAddressCity,
                                                                     State = IOD.PropertyAddressState,
                                                                     InspectionDate = IO.CreatedDate,
                                                                 }).ToList();

                            objInspectorReport.ReportData = objListOfInspectorArchivedReports;
                            _messageToShow.Result = RenderRazorViewToString("_PartialArchivedReports", objInspectorReport);
                            return ReturnJson(_messageToShow);
                        }
                        #endregion

                        #region Inspection Request
                        /*Report Type = Inspection Request*/
                        else if (ReportType == "InspectionRequest")
                        {

                            List<Report> objListOfInspectorInspectionRequest = new List<Report>();

                            objListOfInspectorInspectionRequest = (from IO in context.InspectionOrders
                                                                   join IOD in context.InsectionOrderDetails
                                                                   on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                                   join UI in context.UserInfoes
                                                                   on IO.InspectorID equals UI.ID
                                                                   where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.InspectionStatus != "Completed" && IO.IsAcceptedOrRejected == false
                                                                   && IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                                                   //&& IOD.CreatedDate < UI.LastLoginDate
                                                                   //join Pay in context.Payments
                                                                   //on IO.InspectionOrderID equals Pay.ReportID                                                          
                                                                   select new Report
                                                                   {
                                                                       OrderID = IOD.InspectionOrderId,
                                                                       OwnerFirstName = IOD.OwnerFirstName,
                                                                       OwnerLastName = IOD.OwnerLastName,
                                                                       StreetAddress = IOD.PropertyAddress,
                                                                       City = IOD.PropertyAddressCity,
                                                                       State = IOD.PropertyAddressState,
                                                                       InspectionDate = IO.CreatedDate,
                                                                       isrequestfromAdmin = IO.AssignedByAdmin,
                                                                   }).ToList();

                            objInspectorReport.ReportData = objListOfInspectorInspectionRequest;

                            _messageToShow.Result = RenderRazorViewToString("_PartialInspectionRequest", objInspectorReport);
                            return ReturnJson(_messageToShow);
                        }
                        #endregion

                        #region Payment Reports
                        /*Report Type = Payment Reports*/
                        else if (ReportType == "PaymentReports")
                        {

                            List<Report> objListOfInspectorPaymentReports = new List<Report>();

                            objListOfInspectorPaymentReports = (from IO in context.InspectionOrders
                                                                join IOD in context.InsectionOrderDetails
                                                                on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                                where IO.InspectorID == Sessions.LoggedInUser.UserId
                                                                join Pay in context.Payments
                                                                on IO.InspectionOrderID equals Pay.ReportID
                                                                join UI in context.UserInfoes
                                                                on IO.RequesterID equals UI.ID
                                                                join rol in context.Roles
                                                                on UI.Role equals rol.RoleID
                                                                where IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                                                select new Report
                                                                {
                                                                    OwnerFirstName = IOD.OwnerFirstName,
                                                                    OwnerLastName = IOD.OwnerLastName,
                                                                    StreetAddress = IOD.PropertyAddress,
                                                                    City = IOD.PropertyAddressCity,
                                                                    State = IOD.PropertyAddressState,
                                                                    InspectionDate = IO.CreatedDate,
                                                                    /*Get Role Name*/
                                                                    roleName = rol.RoleName,
                                                                    AmountReceived = Pay.PaymentAmount,
                                                                    PaymentPlan = Pay.PaymentType,
                                                                }).ToList();

                            objInspectorReport.ReportData = objListOfInspectorPaymentReports;
                            _messageToShow.Result = RenderRazorViewToString("_PartialPaymentReports", objInspectorReport);
                            return ReturnJson(_messageToShow);
                        }
                        #endregion

                        #region Report Viewed
                        /*Report Type = Archived Reports*/
                        else if (ReportType == "ReportViewed")
                        {

                            InspectorReport objInspectorReport1 = new InspectorReport();
                            List<Report> objListOfInspectorReportViewed = new List<Report>();

                            objListOfInspectorReportViewed = (from IO in context.InspectionOrders
                                                              join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                              join rpt in context.ReportViewed on IO.InspectionId equals rpt.ReportID
                                                              join UI in context.UserInfoes on rpt.UserID equals UI.ID
                                                              join rol in context.Roles on UI.Role equals rol.RoleID
                                                              where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.CreatedDate >= _startdate && IO.CreatedDate <= _enddate
                                                              select new Report
                                                              {
                                                                  OrderID = rpt.ReportID,
                                                                  OwnerFirstName = IOD.OwnerFirstName,
                                                                  OwnerLastName = IOD.OwnerLastName,
                                                                  StreetAddress = IOD.PropertyAddress,
                                                                  City = IOD.PropertyAddressCity,
                                                                  State = IOD.PropertyAddressState,
                                                                  InspectionDate = IO.CreatedDate,
                                                                  Inspectionstatus = IO.InspectionStatus,
                                                                  isrequestfromAdmin = IO.AssignedByAdmin,
                                                                  roleName = rol.RoleName,
                                                                  date = rpt.CreatedDate,
                                                                  ReportViewdUser = UI.FirstName + " " + UI.LastName
                                                              }).ToList();



                            int i = 0;
                            foreach (var item in objListOfInspectorReportViewed)
                            {
                                List<DateTime?> lstdates = objListOfInspectorReportViewed.Where(x => x.OrderID == item.OrderID).Select(x => x.date).ToList();
                                List<RoleAndDate> roels = (from role in objListOfInspectorReportViewed
                                                           where role.OrderID == item.OrderID
                                                           select new RoleAndDate
                                                           {
                                                               UsrRole = role.roleName,
                                                               fromdate = role.date,
                                                               UserName = role.ReportViewdUser
                                                           }).ToList();
                                objListOfInspectorReportViewed[i].fromandtodate = lstdates;
                                objListOfInspectorReportViewed[i].RoleDate = roels;
                                i++;
                            }
                            objInspectorReport1.ReportData = (objListOfInspectorReportViewed.Select(r => r.OrderID)
                                                            .Distinct()
                                                            .Select(r => objListOfInspectorReportViewed.FirstOrDefault(t => t.OrderID == r)).ToList());
                            _messageToShow.Result = RenderRazorViewToString("_PartialReportViewed", objInspectorReport1);
                            return ReturnJson(_messageToShow);
                        }
                        #endregion

                        else
                            return ReturnJson(_messageToShow.Message = "No Data in Selected Dates");
                    }
                    else
                        return ReturnJson(_messageToShow.Message = "No Data in Selected Dates");
                }
                else
                    return ReturnJson(_messageToShow.Message = "logoff");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public JsonResult GetLastQRCodePrinted()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    var LastQrcode = context.tblQRCode.Where(x => x.InspectorId == Sessions.LoggedInUser.UserId && x.IsPrinted == true).OrderByDescending(x => x.Createddate).FirstOrDefault();
                    if (LastQrcode != null)
                    {
                        _messageToShow.Message = "Success";
                        _messageToShow.Result = LastQrcode.QrCodeNumber.ToString();

                    }
                    else
                    {
                        _messageToShow.Message = "No Code";

                    }
                }
                else
                {
                    _messageToShow.Message = "Session Over";

                }
                return Json(_messageToShow, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetReportNo(string ReportNoFrom, string ReportNoTo)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    if ((ReportNoFrom != null && ReportNoFrom != string.Empty) && (ReportNoTo != null && ReportNoTo != string.Empty))
                    {
                        int QRCodeFrom = Convert.ToInt32(ReportNoFrom);
                        int QRCodeTo = Convert.ToInt32(ReportNoTo);
                        List<QRCode> lstQRCode = new List<QRCode>();
                        lstQRCode = (from QR in context.tblQRCode
                                     where QR.InspectorId == Sessions.LoggedInUser.UserId &&
                                     QR.QrCodeNumber >= QRCodeFrom && QR.QrCodeNumber <= QRCodeTo
                                     select new QRCode
                                     {
                                         QRCodeNo = QR.QrCodeNumber,
                                         ReportNo = QR.ReportNo
                                     }).ToList();

                        //var LastQrcode = context.tblQRCode.Where(x => x.InspectorId == Sessions.LoggedInUser.UserId && x.QrCodeNumber >= QRCodeFrom && x.QrCodeNumber <= ToNo).OrderByDescending(x => x.Createddate); 
                        if (lstQRCode != null && lstQRCode.Count() > 0)
                        {
                            ViewBag.Number = lstQRCode.Max(x => x.QRCodeNo);
                            return Json(lstQRCode, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json("NoCode", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult PrintQrCode(string ReportNoFrom, string ReportNoTo)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    if ((ReportNoFrom != null && ReportNoFrom != string.Empty) && (ReportNoTo != null && ReportNoTo != string.Empty))
                    {
                        int QRCodeFrom = Convert.ToInt32(ReportNoFrom);
                        int QRCodeTo = Convert.ToInt32(ReportNoTo);
                        List<QRCode> lstQRCode = new List<QRCode>();
                        lstQRCode = (from QR in context.tblQRCode
                                     where QR.InspectorId == Sessions.LoggedInUser.UserId &&
                                      QR.QrCodeNumber >= QRCodeFrom && QR.QrCodeNumber <= QRCodeTo
                                     orderby QR.QrCodeNumber
                                     select new QRCode
                                     {
                                         QRCodeNo = QR.QrCodeNumber,
                                         ReportNo = QR.ReportNo
                                     }).ToList();
                        if (lstQRCode != null && lstQRCode.Count() > 0)
                        {
                            return Json(lstQRCode, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json("NoCode", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult SaveLastRepotNo(string ReportNoTo)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    if ((ReportNoTo != null && ReportNoTo != string.Empty))
                    {
                        int QrCode = Convert.ToInt32(ReportNoTo);
                        tblqrcode objQr = context.tblQRCode.Where(x => x.InspectorId == Sessions.LoggedInUser.UserId && x.QrCodeNumber == QrCode).OrderByDescending(x => x.Createddate).FirstOrDefault();
                        if (objQr != null)
                        {
                            objQr.IsPrinted = true;
                            context.SaveChanges();
                            return Json("Success", JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json("error", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Create New Inspection
        /// <summary>
        /// function to Create New Inspection Form
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateNewInspection(string Orderid)
        {

            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    NInspectionOrderDetails insectionOrderDetailPoco = new NInspectionOrderDetails();
                    insectionOrderDetailPoco.sellerAgent = new NSellerAgent();
                    insectionOrderDetailPoco.buyerAgent = new NBuyerAgent();
                    tblinspectionorderdetail objtblOrderDetail = context.tblinspectionorderdetail.Where(x => x.InspectionorderdetailsID == Orderid).FirstOrDefault();


                    if (objtblOrderDetail != null)
                    {
                        /*Requester Details*/
                        insectionOrderDetailPoco.RequestedFirstName = objtblOrderDetail.RequesterFName;
                        insectionOrderDetailPoco.RequestedLastName = objtblOrderDetail.RequesterLName;
                        insectionOrderDetailPoco.RequestedByAddress = objtblOrderDetail.RequesterAddress;
                        insectionOrderDetailPoco.RequestedByCity = objtblOrderDetail.RequesterCity;
                        insectionOrderDetailPoco.RequestedByState = objtblOrderDetail.RequesterState;
                        insectionOrderDetailPoco.RequestedByZip = objtblOrderDetail.RequesterZip;
                        insectionOrderDetailPoco.RequesterType = objtblOrderDetail.RequesterType;
                        insectionOrderDetailPoco.RequestedByEmailAddress = objtblOrderDetail.RequestedByEmailAddress;
                        insectionOrderDetailPoco.PhoneNumber = objtblOrderDetail.RequesterPhone;

                        /*Property Info*/
                        insectionOrderDetailPoco.PropertyAddress = objtblOrderDetail.PropertyAddress;
                        insectionOrderDetailPoco.OwnerPhoneNumber = objtblOrderDetail.OwnerPhone;
                        insectionOrderDetailPoco.PropertyAddressState = objtblOrderDetail.PropertyState;
                        insectionOrderDetailPoco.PropertyAddressCity = objtblOrderDetail.PropertyCity;
                        insectionOrderDetailPoco.PropertyAddressZip = objtblOrderDetail.PropertyZip;
                        insectionOrderDetailPoco.OwnerName = objtblOrderDetail.OwnerName;
                        insectionOrderDetailPoco.OwnerEmailAddress = objtblOrderDetail.PropertyEmail;
                        insectionOrderDetailPoco.PropertyDescription = objtblOrderDetail.PropertyDescription;
                        insectionOrderDetailPoco.PropertyUnit = objtblOrderDetail.Units;
                        insectionOrderDetailPoco.PropertySquareFootage = objtblOrderDetail.SqureFootage;
                        insectionOrderDetailPoco.PropertyAddition = objtblOrderDetail.Additions_Alterations;
                        insectionOrderDetailPoco.PropertyBedRoom = objtblOrderDetail.Bedrooms;
                        insectionOrderDetailPoco.PropertyBaths = objtblOrderDetail.Bathrooms;
                        insectionOrderDetailPoco.PropertyEstimatedMonthlyUtilites = objtblOrderDetail.EstimatedMonthlyUtilities;
                        insectionOrderDetailPoco.PropertyHomeAge = objtblOrderDetail.YearBuilt;
                        insectionOrderDetailPoco.PropertyRoofAge = objtblOrderDetail.RoofAge;
                        insectionOrderDetailPoco.Direction = objtblOrderDetail.Directions;

                        /*Buyer Agent Info*/

                        agentlibrary AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                        AgentLibraryinfo = context.AgentLibraries.FirstOrDefault(x => x.InspectionOrderID == Orderid && x.AgentType == "1");

                        if (AgentLibraryinfo != null)
                        {
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentFirstName = AgentLibraryinfo.AgentFirstName;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentLastName = AgentLibraryinfo.AgentLastName;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentOffice = AgentLibraryinfo.AgentOffice;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay = AgentLibraryinfo.AgentPhoneDay;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentStreetAddress = AgentLibraryinfo.AgentStreetAddress;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentState = AgentLibraryinfo.AgentState;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentCity = AgentLibraryinfo.AgentCity;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentZip = AgentLibraryinfo.AgentZip;
                            insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress = AgentLibraryinfo.AgentEmailAddress;
                        }
                        AgentLibraryinfo = context.AgentLibraries.FirstOrDefault(x => x.InspectionOrderID == Orderid && x.AgentType == "2");

                        /*Seller Agent*/
                        if (AgentLibraryinfo != null)
                        {
                            insectionOrderDetailPoco.sellerAgent.SellerAgentFirstName = AgentLibraryinfo.AgentFirstName;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentLastName = AgentLibraryinfo.AgentLastName;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentOffice = AgentLibraryinfo.AgentOffice;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay = AgentLibraryinfo.AgentPhoneDay;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentStreetAddress = AgentLibraryinfo.AgentStreetAddress;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentState = AgentLibraryinfo.AgentState;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentCity = AgentLibraryinfo.AgentCity;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentZip = AgentLibraryinfo.AgentZip;
                            insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress = AgentLibraryinfo.AgentEmailAddress;
                        }

                        tblinspectionorder objtblInspection = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == Orderid).FirstOrDefault();
                        if (objtblInspection != null)
                        {
                            insectionOrderDetailPoco.ScheduleInspectionDate = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == Orderid).FirstOrDefault().ScheduledDate;
                            insectionOrderDetailPoco.IsAcceptedOrRejected = objtblInspection.IsAcceptedOrRejected;
                            insectionOrderDetailPoco.IsRejectedByInspector = objtblInspection.IsRejectedByInspector;

                            if (insectionOrderDetailPoco.IsAcceptedOrRejected == true)
                            {
                                tblschedule objSchedule = context.tblSchedule.Where(x => x.InspectionOrderId == Orderid).FirstOrDefault();
                                if (objSchedule != null)
                                {
                                    insectionOrderDetailPoco.StartTime = objSchedule.StartTime;
                                    insectionOrderDetailPoco.EndTime = objSchedule.EndTime;
                                    insectionOrderDetailPoco.ScheduledDate = objSchedule.ScheduleDate;
                                }
                            }

                        }

                    }


                    #region Bind States
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "0";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    insectionOrderDetailPoco.StatesList = items;
                    insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                    insectionOrderDetailPoco.CityList = objCityList;

                    #endregion

                    #region Bind Property Description
                    List<NPropertyDescription> ObjPropertyDesc = new List<NPropertyDescription>();
                    ObjPropertyDesc = (from PropertyDesc in context.tblpropertydescription
                                       select new NPropertyDescription
                                       {
                                           PropertyDescriptionId = PropertyDesc.PropertyDescriptionId,
                                           Type = PropertyDesc.Type
                                       }).OrderBy(x => x.Type).ToList();
                    List<SelectListItem> PropertyDescItem = new List<SelectListItem>();
                    SelectListItem objSelectListItemProperty = new SelectListItem();
                    objSelectListItemProperty.Text = "--Select--";
                    objSelectListItemProperty.Value = "--Select--";
                    foreach (var t in ObjPropertyDesc)
                    {
                        SelectListItem s1 = new SelectListItem();
                        s1.Text = t.Type.ToString();
                        s1.Value = t.PropertyDescriptionId.ToString();
                        PropertyDescItem.Add(s1);
                    }
                    PropertyDescItem = PropertyDescItem.OrderBy(x => x.Text).ToList();
                    PropertyDescItem.Insert(0, objSelectListItemProperty);
                    insectionOrderDetailPoco.PropertyDescriptionList = PropertyDescItem;
                    #endregion
                    return View(insectionOrderDetailPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion


        public JsonResult DeclineRequest(string OrderId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    string msg = "Success";
                    notificationdetail objNotification = new notificationdetail();
                    objNotification.NotifcationFrom = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == OrderId).FirstOrDefault().RequesterID;
                    objNotification.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                    objNotification.NotificationTypeID = 2;
                    objNotification.IsRead = false;
                    objNotification.CreatedDate = System.DateTime.Now;
                    objNotification.RequestID = OrderId;
                    context.NotificationDetail.Add(objNotification);

                    tblinspectionorder objinspectionOrder = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == OrderId).FirstOrDefault();
                    if (objinspectionOrder != null)
                    {
                        objinspectionOrder.IsRejectedByInspector = true;
                    }
                    context.SaveChanges();
                    return Json(msg);
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region ManageCommentLibrary

        public ActionResult ManageCommentLibrary()
        {
            if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
            {
                IEnumerable<CommentLibrary> objCommentMenu = (from k in context.MainSections
                                                              join sub in context.SubSections on k.ID equals sub.SectionId
                                                              join item in context.CommentBox on sub.ID equals item.SubSectionId
                                                              select new CommentLibrary
                                                              {
                                                                  MainID = k.ID,
                                                                  SectionName = k.SectionName,
                                                                  SubSectionID = sub.ID,
                                                                  SubSectionName = sub.SubSectionName,
                                                                  ItemId = item.Id,
                                                                  CommentBoxName = item.CommentBoxName
                                                              }).ToList();

                return View(objCommentMenu);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion

        public JsonResult GetAllComment(string CommentBoxId)
        {
            CommentLibrary ObjCommentLibrary = new CommentLibrary();
            List<AdminCommanLibrary> objAdminCommanLibrary = new List<AdminCommanLibrary>();
            List<AdminCommanLibrary> objUserCommanLibrary = new List<AdminCommanLibrary>();

            objUserCommanLibrary = (from acl in context.AdminCommentLibrary
                                    where acl.CommentBoxID == CommentBoxId && acl.IsDeleted == false && acl.IsAdminComment == false && acl.UserId == Sessions.LoggedInUser.UserId
                                    select new AdminCommanLibrary
                                    {
                                        CommentText = acl.CommentText,
                                        CommentBoxID = acl.CommentBoxID,
                                        Id = acl.Id,
                                        IsAdminComment = acl.IsAdminComment,
                                    }).ToList();
            objAdminCommanLibrary = (from acl in context.AdminCommentLibrary
                                     where acl.CommentBoxID == CommentBoxId && acl.IsDeleted == false && acl.IsAdminComment == true
                                     select new AdminCommanLibrary
                                         {
                                             CommentText = acl.CommentText,
                                             CommentBoxID = acl.CommentBoxID,
                                             Id = acl.Id,
                                             IsAdminComment = acl.IsAdminComment,
                                         }).ToList();

            foreach (var item in objAdminCommanLibrary)
            {
                if (context.tblinspectorcomment.Any(x => x.CommentId == item.Id))
                    item.IsSelected = true;
            }


            objAdminCommanLibrary.AddRange(objUserCommanLibrary);

            ObjCommentLibrary.AdminCommanLibrary = objAdminCommanLibrary;
            if (objAdminCommanLibrary.Count > 0)
                _messageToShow.Message = "Success";
            else
                _messageToShow.Message = "Item Not Found";
            _messageToShow.Result = RenderRazorViewToString("_ManageCommentLibrary", ObjCommentLibrary);
            return Json(_messageToShow);
        }

        public JsonResult GetComment(string CommentBoxId)
        {
            CommentLibrary ObjCommentLibrary = new CommentLibrary();
            List<AdminCommanLibrary> objAdminCommanLibrary = new List<AdminCommanLibrary>();
            List<AdminCommanLibrary> objUserCommanLibrary = new List<AdminCommanLibrary>();

            objAdminCommanLibrary = (from k in context.tblinspectorcomment
                                     join l in context.AdminCommentLibrary
                                     on k.CommentId equals l.Id
                                     where k.CommentBoxId == CommentBoxId
                                     select new AdminCommanLibrary
                                     {
                                         CommentBoxID = l.CommentBoxID,
                                         CommentText = l.CommentText,
                                         Id = l.Id,
                                         IsAdminComment = l.IsAdminComment
                                     }).ToList();

            ObjCommentLibrary.AdminCommanLibrary = objAdminCommanLibrary;
            if (objAdminCommanLibrary.Count > 0)
                _messageToShow.Message = "Success";
            else
                _messageToShow.Message = "Item Not Found";
            _messageToShow.Result = RenderRazorViewToString("_ManageCommentLibrary", ObjCommentLibrary);
            return Json(_messageToShow);
        }

        public JsonResult SaveComment(string SectionId, string SubSectionId, string CommentBoxId, string CommentText, string CommentBoxName)
        {
            try
            {
                admincommentlibrary objAdminCommentLibrary = new admincommentlibrary();
                objAdminCommentLibrary.Id = Guid.NewGuid().ToString();
                objAdminCommentLibrary.SectionId = SectionId;
                objAdminCommentLibrary.SubSectionId = SubSectionId;
                objAdminCommentLibrary.CommentBoxID = CommentBoxId;
                objAdminCommentLibrary.CommentText = CommentText;
                objAdminCommentLibrary.CreatedDate = DateTime.Now;//.ToUniversalTime();
                objAdminCommentLibrary.ModifiendDate = DateTime.Now;//.ToUniversalTime();
                objAdminCommentLibrary.IsDeleted = false;
                objAdminCommentLibrary.IsAdminComment = false;
                objAdminCommentLibrary.UserId = Sessions.LoggedInUser.UserId;
                objAdminCommentLibrary.CommentBoxName = CommentBoxName;
                context.AdminCommentLibrary.Add(objAdminCommentLibrary);
                context.SaveChanges();
                CommentLibrary ObjCommentLibrary = new CommentLibrary();

                List<AdminCommanLibrary> objAdminCommanLibrary = new List<AdminCommanLibrary>();
                List<AdminCommanLibrary> objUserCommanLibrary = new List<AdminCommanLibrary>();

                objUserCommanLibrary = (from acl in context.AdminCommentLibrary
                                        where acl.CommentBoxID == CommentBoxId && acl.IsDeleted == false && acl.IsAdminComment == false && acl.UserId == Sessions.LoggedInUser.UserId
                                        select new AdminCommanLibrary
                                        {
                                            CommentText = acl.CommentText,
                                            CommentBoxID = acl.CommentBoxID,
                                            Id = acl.Id,
                                            IsAdminComment = acl.IsAdminComment,
                                        }).ToList();
                objAdminCommanLibrary = (from acl in context.AdminCommentLibrary
                                         where acl.CommentBoxID == CommentBoxId && acl.IsDeleted == false && acl.IsAdminComment == true
                                         select new AdminCommanLibrary
                                         {
                                             CommentText = acl.CommentText,
                                             CommentBoxID = acl.CommentBoxID,
                                             Id = acl.Id,
                                             IsAdminComment = acl.IsAdminComment,
                                         }).ToList();

                objAdminCommanLibrary.AddRange(objUserCommanLibrary);
                ObjCommentLibrary.AdminCommanLibrary = objAdminCommanLibrary;
                _messageToShow.Message = "Success";
                _messageToShow.Result = RenderRazorViewToString("_ManageCommentLibrary", ObjCommentLibrary);
                return Json(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult SaveInspectorComment(string commentid, string CommentBoxId)
        {
            try
            {
                string list = JsonConvert.DeserializeObject<string>(commentid);
                var result = list.Trim(',').Split(',');

                tblinspectorcomment objtblinspectorcomment;
                CommentLibrary ObjCommentLibrary = new CommentLibrary();
                List<AdminCommanLibrary> objLstAdminCommanLibrary = new List<AdminCommanLibrary>();
                AdminCommanLibrary objAdminCommentLibrary = new AdminCommanLibrary();
                //DateTime? creationdate = null;

                List<tblinspectorcomment> objlsttblinspectorcomment = context.tblinspectorcomment.Where(x => x.UserId == Sessions.LoggedInUser.UserId && x.CommentBoxId == CommentBoxId).ToList();
                //objlsttblinspectorcomment.RemoveRange(0, objlsttblinspectorcomment.Count);

                foreach (var item in result)
                {
                    objtblinspectorcomment = new tblinspectorcomment();

                    //if (objtblinspectorcomment != null)
                    //{
                    //    context.tblinspectorcomment.Remove(objtblinspectorcomment);
                    //    creationdate = objtblinspectorcomment.CreatedDate;
                    //}

                    objtblinspectorcomment = new tblinspectorcomment();
                    objtblinspectorcomment.ID = Guid.NewGuid().ToString();
                    objtblinspectorcomment.CommentId = item;
                    objtblinspectorcomment.CommentBoxId = CommentBoxId;
                    objtblinspectorcomment.UserId = Sessions.LoggedInUser.UserId;
                    if (objlsttblinspectorcomment.Count > 0)
                        if (objlsttblinspectorcomment.Any(x => x.CreatedDate != null && x.CommentId == item))
                            objtblinspectorcomment.CreatedDate = objlsttblinspectorcomment.FirstOrDefault(x => x.CreatedDate != null && x.CommentId == item).CreatedDate == null ? objlsttblinspectorcomment.FirstOrDefault(x => x.CreatedDate != null).CreatedDate : DateTime.Now;
                        else
                            objtblinspectorcomment.CreatedDate = DateTime.Now;
                    else
                        objtblinspectorcomment.CreatedDate = DateTime.Now;
                    objtblinspectorcomment.ModifiedDate = DateTime.Now;
                    context.tblinspectorcomment.Add(objtblinspectorcomment);
                    objAdminCommentLibrary = (from acl in context.AdminCommentLibrary
                                              where acl.Id == item
                                              select new AdminCommanLibrary
                                              {
                                                  CommentText = acl.CommentText,
                                                  CommentBoxID = acl.CommentBoxID,
                                                  Id = acl.Id,
                                                  IsAdminComment = acl.IsAdminComment,
                                              }).FirstOrDefault();

                    objLstAdminCommanLibrary.Add(objAdminCommentLibrary);
                }
                if (objlsttblinspectorcomment.Count > 0)
                {
                    foreach (var item in objlsttblinspectorcomment)
                    {
                        context.tblinspectorcomment.Remove(item);
                    }
                }

                context.SaveChanges();




                ObjCommentLibrary.AdminCommanLibrary = objLstAdminCommanLibrary;
                _messageToShow.Message = "Success";
                _messageToShow.Result = RenderRazorViewToString("_ManageCommentLibrary", ObjCommentLibrary);
                return Json(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult UpdateDelete(string AdminCommentLibraryId, string status, string CommentText)
        {
            try
            {
                admincommentlibrary objAdminCommentLibrary = new admincommentlibrary();
                objAdminCommentLibrary = context.AdminCommentLibrary.Where(x => x.Id == AdminCommentLibraryId).FirstOrDefault();
                if (status == "update")
                    objAdminCommentLibrary.CommentText = CommentText;
                else
                    objAdminCommentLibrary.IsDeleted = true;
                objAdminCommentLibrary.ModifiendDate = DateTime.Now;//ToUniversalTime();
                context.SaveChanges();
                CommentLibrary ObjCommentLibrary = new CommentLibrary();

                List<AdminCommanLibrary> objAdminCommanLibrary = new List<AdminCommanLibrary>();
                List<AdminCommanLibrary> objUserCommanLibrary = new List<AdminCommanLibrary>();

                objUserCommanLibrary = (from acl in context.AdminCommentLibrary
                                        where acl.CommentBoxID == objAdminCommentLibrary.CommentBoxID && acl.IsDeleted == false && acl.IsAdminComment == false && acl.UserId == Sessions.LoggedInUser.UserId
                                        select new AdminCommanLibrary
                                        {
                                            CommentText = acl.CommentText,
                                            CommentBoxID = acl.CommentBoxID,
                                            Id = acl.Id,
                                            IsAdminComment = acl.IsAdminComment,
                                        }).ToList();
                objAdminCommanLibrary = (from acl in context.AdminCommentLibrary
                                         where acl.CommentBoxID == objAdminCommentLibrary.CommentBoxID && acl.IsDeleted == false && acl.IsAdminComment == true
                                         select new AdminCommanLibrary
                                         {
                                             CommentText = acl.CommentText,
                                             CommentBoxID = acl.CommentBoxID,
                                             Id = acl.Id,
                                             IsAdminComment = acl.IsAdminComment,
                                         }).ToList();

                objAdminCommanLibrary.AddRange(objUserCommanLibrary);
                ObjCommentLibrary.AdminCommanLibrary = objAdminCommanLibrary;
                _messageToShow.Message = "Success";
                _messageToShow.Result = RenderRazorViewToString("_ManageCommentLibrary", ObjCommentLibrary);
                return Json(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult AccceptOrDeclineRequest(string Type, string inspectionOrderDetailsId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    string msg = "Success";
                    if (inspectionOrderDetailsId != null && inspectionOrderDetailsId != "")
                    {
                        var inspection = (from ins in context.tblinspectionorder where ins.InspectionOrderDetailId == inspectionOrderDetailsId select ins).FirstOrDefault();
                        if (inspection != null)
                        {
                            if (Type == "Rejected")
                            {
                                inspection.IsRejectedByInspector = true;

                                notificationdetail objNotification = new notificationdetail();
                                objNotification.NotifcationFrom = context.InspectionOrders.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderDetailsId).RequesterID;
                                objNotification.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                                objNotification.NotificationTypeID = 2;
                                objNotification.IsRead = false;
                                objNotification.CreatedDate = System.DateTime.Now;
                                objNotification.RequestID = inspectionOrderDetailsId;
                                context.NotificationDetail.Add(objNotification);


                            }
                            else
                            {
                                inspection.IsAcceptedOrRejected = true;
                            }
                            inspection.ModifiedDate = DateTime.Now;
                            inspection.ModifiedBy = Sessions.LoggedInUser.UserId;
                            context.SaveChanges();
                        }
                        else
                        {
                            msg = "Error";
                        }
                    }
                    return Json(msg);
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Update Report

        public ActionResult UpdateRepot()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    MySqlConnection conn = new MySqlConnection(connStr);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("GetUpdatableReport", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserID", Sessions.LoggedInUser.UserId);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    List<PropertyAddress> objList_PropertyAddress = new List<PropertyAddress>();
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        objList_PropertyAddress = ds.Tables[0].AsEnumerable().Select(datarow =>
                         new PropertyAddress
                         {
                             StreetAddress = datarow["PropertyAddress"].ToString(),
                             ReportNo = datarow["ReportNo"].ToString(),
                             City = datarow["City"].ToString(),
                             State = datarow["State"].ToString(),
                             Zip = Convert.ToInt32(datarow["Zip"]),
                             OrderID = datarow["OrderID"].ToString()



                         }).ToList();
                        //objList_PropertyAddress = (from report in context.tblPhotoLibrary
                        //                           join IRep in context.InspectionReport on report.InspectionReportID equals IRep.ID
                        //                           join IOD in context.InsectionOrderDetails on IRep.InspectionOrderId equals IOD.InspectionOrderId
                        //                           join IO in context.InspectionOrders on IOD.InspectionOrderId equals IO.InspectionOrderID
                        //                           where IO.InspectorID == Sessions.LoggedInUser.UserId && report.IsApproved == null

                        //                           select new PropertyAddress
                        //                           {
                        //                               StreetAddress = IOD.PropertyAddress,
                        //                               ReportNo = IO.UniqueID,
                        //                               City = IOD.PropertyAddressCity,
                        //                               State = IOD.PropertyAddressState,
                        //                               Zip = IOD.PropertyAddressZip,
                        //                               OrderID = IOD.InspectionOrderId
                        //                           }).GroupBy(X=>X.OrderID).ToList();
                    }



                    return View(objList_PropertyAddress);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        #endregion


        public ActionResult InspectionSchedule()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    InspectorReport objInspectorReport = new InspectorReport();
                    List<Report> objListOfInspectorInspectionRequest = new List<Report>();

                    objListOfInspectorInspectionRequest = (from IO in context.tblinspectionorder
                                                           join IOD in context.tblinspectionorderdetail
                                                           on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                                           join schedule in context.tblSchedule on IOD.InspectionorderdetailsID equals schedule.InspectionOrderId
                                                           join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                                           join req in context.UserInfoes on IO.RequesterID equals req.ID
                                                           join role in context.Roles on req.Role equals role.RoleID
                                                           join state in context.tblstate on IOD.PropertyState equals state.StateId
                                                           join city in context.tblcity on IOD.PropertyCity equals city.CityId
                                                           join zip in context.tblzip on IOD.PropertyZip equals zip.ZipId
                                                           where IO.InspectorID == Sessions.LoggedInUser.UserId && IO.InspectionStatus != "Completed" && IO.IsDeleted == false && IO.IsAcceptedOrRejected == true

                                                           //&& IOD.CreatedDate < UI.LastLoginDate
                                                           //join Pay in context.Payments
                                                           //on IO.InspectionOrderID equals Pay.ReportID                                                          
                                                           select new Report
                                                           {
                                                               OrderID = IOD.InspectionorderdetailsID,
                                                               OwnerFirstName = IOD.RequesterFName,
                                                               OwnerLastName = IOD.RequesterLName,
                                                               StreetAddress = IOD.PropertyAddress,
                                                               City = city.CityName,
                                                               State = state.StateName,
                                                               InspectionDate = IO.ScheduledDate,
                                                               isrequestfromAdmin = IO.AssignedByAdmin,
                                                               roleName = role.RoleName,
                                                               IsAccepted = IO.IsAcceptedOrRejected,
                                                               ReportNo = IO.ReportNo,
                                                               Zip = zip.Zip,
                                                               IsRejectedByInspector = IO.IsRejectedByInspector,
                                                               IsAcceptedOrRejected = IO.IsAcceptedOrRejected,
                                                               ScheduledDate = schedule.ScheduleDate
                                                           }).OrderByDescending(x => x.InspectionDate).ToList();

                    objInspectorReport.ReportData = objListOfInspectorInspectionRequest.OrderByDescending(x => x.InspectionDate).ToList();
                    return View(objInspectorReport);
                }
                else
                {
                    return View("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
