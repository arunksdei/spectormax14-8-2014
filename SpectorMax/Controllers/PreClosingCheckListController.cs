﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMax.Entities.Classes;
using SpectorMaxDAL;
using SpectorMax.Models;

namespace SpectorMax.Controllers
{
    public class PreClosingCheckListController : Controller
    {
        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _messageToShow = new MessageToShow();
        //
        // GET: /PreClosingCheckList/

        


        public ActionResult Index()
        {
            List<ViewPreviousInspection> objViewPreviousInspection = new List<ViewPreviousInspection>();
            objViewPreviousInspection = (from k in context.InspectionOrders
                                         join Ins in context.InsectionOrderDetails
                                         on k.InspectionOrderID equals Ins.InspectionOrderId                                         
                                         where 
                                         !context.Checklists.Select(x => x.InspectionOrderId).Contains(k.InspectionOrderID)                                          
                                         && k.RequesterID == Sessions.LoggedInUser.UserId
                                         select new ViewPreviousInspection
                                         {
                                             InspectionOrderId = k.InspectionOrderID,
                                             PropertyAddress = Ins.PropertyAddress,
                                             PropertyAddressCity = Ins.PropertyAddressCity,
                                             PropertyAddressState = Ins.PropertyAddressState
                                         }).ToList();

            return View(objViewPreviousInspection);
        }

        /// <summary>
        /// POST: / method for save preclosing data in table 
        /// </summary>
        /// <param name="checkListValues"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SavePreClosingCheckList(string[][] checkListValues) //string[,]
        {
            try
            {
                if (checkListValues != null && checkListValues.Length > 0)
                {
                    for (int i = 0; i < checkListValues.Length-1; i++)
                    {
                        checklist checkList = new checklist();
                        checkList.CheckListID = Guid.NewGuid().ToString();
                        checkList.PreClosingID = checkListValues[i][1].ToString();
                        checkList.IsSelected = checkListValues[i][0].Equals("1") ? true : false;
                        checkList.InspectionOrderId = checkListValues[76][0];
                        checkList.IsInserted = true;
                        context.Checklists.Add(checkList);
                        
                    }
                    context.SaveChanges();
                    _messageToShow.Message = "Success";
                }
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
            }
            return Json(_messageToShow);
        }
    }
}
