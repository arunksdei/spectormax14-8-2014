﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMax.Entities.Classes;
using SpectorMaxDAL;
using SpectorMax.Models;
using System.Drawing;
using System.IO;
using System.Data;
using MySql.Data.MySqlClient;
using System.Transactions;

namespace SpectorMax.Controllers
{
    public class ReportController : BaseController
    {

        #region Global Variables

        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _messageToShow = new MessageToShow();
        int ImageCount;
        int skip;
        int TakeImg;
        //List<string> mainSectionList = new List<string>();
        //List<string> subSectionList = new List<string>();

        string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["SpectorMaxConnectionString"].ConnectionString;

        #endregion

        #region Index
        //
        // GET: /Report/

        public ActionResult Index()
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        #endregion

        #region ViewDetailReport
        /// <summary>
        /// GET:// Method for get detail report aginst inspection order id.
        /// <param name="inspectionOrderId"></param>
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewDetailReport(string inspectionOrderId, string PaymentType)
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {


                    List<SubSections> lstSubSections = new List<SubSections>();

                    var TermsAndConditions = from k in context.EmailTemplates
                                             where k.TemplateId == 26
                                             select k.TemplateDiscription;

                    ViewBag.Message = TermsAndConditions.FirstOrDefault();

                    #region SubSection

                    lstSubSections = (from IR in context.InspectionReport
                                      join SS in context.SubSections on IR.SubsectionId equals SS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new SubSections
                                      {
                                          SectionID = IR.SectionId,
                                          //SectionName = MS.SectionName,
                                          SubSectionID = IR.SubsectionId,
                                          SubSectionName = SS.SubSectionName,
                                          SortOrder = SS.SortOrder,
                                          CheckBoxID = IR.CheckboxID
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion


                    #region MainSection
                    List<MainSections> lstMainSection = new List<MainSections>();

                    lstMainSection = (from IR in context.InspectionReport
                                      join MS in context.MainSections on IR.SectionId equals MS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new MainSections
                                      {
                                          SectionID = MS.ID,
                                          SectionName = MS.SectionName,
                                          SortOrder = MS.SortOrder
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion
                    /*Check whether Inspection Rating is already done for this report--Start*/
                    #region Is InspectionDone
                    var IsInspectionDone = from Ratings in context.InspectorRatings
                                           where Ratings.InspectionID == inspectionOrderId
                                           select Ratings.RatingID;
                    if (IsInspectionDone.Any())
                    {
                        ViewBag.InspectionDone = "InpectionAlreadyDone";
                    }
                    else
                    {
                        ViewBag.InspectionDone = null;
                    }

                    #endregion
                    /*End*/

                    /*Get Details Report--Start*/

                    #region Get Detail Report
                    DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                    ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
                    List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
                    ViewBag.InspectionOrderId = inspectionOrderId;
                    objviewPreviousInspection = (from detail in context.tblinspectionorderdetail
                                                 join IO in context.tblinspectionorder on detail.InspectionorderdetailsID equals IO.InspectionOrderDetailId
                                                 join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                                 join ID in context.InspectorDetail on UI.ID equals ID.UserID
                                                 join PD in context.tblpropertydescription on detail.PropertyDescription equals PD.PropertyDescriptionId
                                                 join AI in context.InspectionAdditionalInfo on detail.InspectionorderdetailsID equals AI.InspectionOrderID into g
                                                 join agent in context.AgentLibraries on detail.InspectionorderdetailsID equals agent.InspectionOrderID
                                                 where detail.InspectionorderdetailsID == inspectionOrderId && agent.AgentType == "1" && IO.IsDeleted == false
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = inspectionOrderId,
                                                     PropertyAddress = detail.PropertyAddress,
                                                     RequestedBy = detail.RequesterFName + " " + detail.RequesterLName,
                                                     Agent = agent.AgentFirstName + agent.AgentLastName,
                                                     AgentEmail = agent.AgentEmailAddress,
                                                     AgentPhone = agent.AgentPhoneDay + " " + agent.AgentPhoneDay1,
                                                     ScheduleInspectionDate = IO.ScheduledDate,
                                                     HomeAge = detail.RoofAge,
                                                     SqureFeet = detail.SqureFootage,
                                                     ReportNumber = IO.ReportNo,
                                                     Bath = detail.Bathrooms,
                                                     Bed = detail.Bedrooms,
                                                     PropertyDescription = PD.Type,
                                                     Units = detail.Units,
                                                     Utilities = detail.EstimatedMonthlyUtilities,
                                                     RoofAge = detail.RoofAge,
                                                     Direction = detail.Directions,
                                                     AdditionAltration = detail.Additions_Alterations,
                                                     InspectorFirstName = UI.FirstName,
                                                     InspectorLastName = UI.LastName,
                                                     InspectorStreetAddress = UI.StreetAddress,
                                                     ExperienceTraining1 = ID.ExperienceTraining1,
                                                     ExperienceTraining2 = ID.ExperienceTraining2,
                                                     ExperienceTraining3 = ID.ExperienceTraining3,
                                                     Certification1 = ID.Certification1,
                                                     Certification2 = ID.Certification2,
                                                     Certification3 = ID.Certification3,
                                                     PhotoURL = ID.PhotoURL,
                                                     Email = UI.EmailAddress,
                                                     InspectorCity = UI.City,
                                                     InspectorState = UI.State,
                                                     InspectorZip = UI.ZipCode,
                                                     InspectorCountry = UI.Country,
                                                     InspectorPhone = UI.PhoneNo,
                                                     InspectorAddress1 = UI.Address,
                                                     InspectorAddress2 = UI.AddressLine2,
                                                     MainHomePage = g
                                                 }).FirstOrDefault();



                    /*End*/
                    #endregion


                    if (objviewPreviousInspection != null)
                    {
                        objviewPreviousInspection.MainSectionList = lstMainSection;
                        objviewPreviousInspection.SubSectionsList = lstSubSections;

                    }
                    objDetailsReportCommonPoco.objViewPreviousInspection = objviewPreviousInspection;



                    #region Get Report Count
                    SampleReportCount objSampleReportCount = new SampleReportCount();
                    var Temp_Report = from IR in context.InspectionReport
                                      where IR.InspectionOrderId == inspectionOrderId
                                      group IR by IR.CheckboxID into g
                                      select new
                                      {
                                          Category = g.Key,
                                          Products = g
                                      };

                    foreach (var item in Temp_Report)
                    {

                        if (item.Category == "1")
                            objSampleReportCount.GreenArrowCount = item.Products.Count();
                        else if (item.Category == "2")
                            objSampleReportCount.YellowArrowCount = item.Products.Count();
                        else if (item.Category == "3")
                            objSampleReportCount.RedArrowCount = item.Products.Count();
                        else if (item.Category == "4")
                            objSampleReportCount.DoubleRedArrowCount = item.Products.Count();
                        else if (item.Category == "5")
                            objSampleReportCount.YellowWarningCount = item.Products.Count();
                        else if (item.Category == "6")
                            objSampleReportCount.RedWarningCount = item.Products.Count();
                        else if (item.Category == "7")
                            objSampleReportCount.No_inspection = item.Products.Count();
                        else if (item.Category == "8")
                            objSampleReportCount.Not_accessibility = item.Products.Count();
                    }
                    objSampleReportCount.BlueIconCount = (from IR in context.InspectionReport
                                                          join PL in context.tblPhotoLibrary on IR.ID equals PL.InspectionReportID
                                                          where IR.InspectionOrderId == inspectionOrderId && PL.IsApproved == true
                                                          select new Report
                                                          {
                                                              OrderID = PL.ID
                                                          }).Count();

                    #endregion

                    objDetailsReportCommonPoco.ReportCount = objSampleReportCount;

                    return View(objDetailsReportCommonPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region BindImages
        /// <summary>
        /// private method for bind images for report aginst to sectionid, subsectionid, inspectionorderid.
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="inspectionOrderId"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        private List<ImagesNote> BindImages(string sectionId, string subSectionId, string inspectionOrderId, bool Status)
        {

            try
            {
                List<ImagesNote> ObjImagesNotes = null;
                if (Status == false)

                    ObjImagesNotes = (from imagesLibrary in context.PhotoLibraries
                                      join Direction in context.DirectionDropMaster on imagesLibrary.DirectionDrop equals Direction.DirectionDropID
                                      where imagesLibrary.SectionID == sectionId
                                      && imagesLibrary.SubSectionId == subSectionId
                                      && imagesLibrary.InspectionOrderID == inspectionOrderId
                                      && imagesLibrary.Comments == null && imagesLibrary.CommentedBy == null
                                      && imagesLibrary.AcceptedOrRejected == true && imagesLibrary.PhotoUrl != null
                                      select new ImagesNote
                                      {
                                          ImageUrl = imagesLibrary.PhotoUrl,
                                          ImageNote = imagesLibrary.Notes,
                                          CreatedDate = imagesLibrary.CreatedDate,
                                          Direction = Direction.DirectionDropValue
                                      }).OrderBy(item => item.CreatedDate).ToList();
                return ObjImagesNotes;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        #endregion

        #region DetailReportLoadMore
        /// <summary>
        /// POST: method for get Detail report aginst inspection order id on mouse scroll.
        /// <param name="inspectionOrderId"></param>
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        public JsonResult DetailReportLoadMore(string inspectionOrderId)
        {
            try
            {
                DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                List<DetailsReportCommonPoco> objReport2 = new List<DetailsReportCommonPoco>();
                ViewBag.InspectionOrderId = inspectionOrderId;
                int count = Convert.ToInt32(Session["ReportCount"]);
                ImageCount = Convert.ToInt32(Session["ImageCount"]);
                if (ImageCount <= 0)
                {
                    Session["TakeImg"] = 0;
                    objDetailsReportCommonPoco = (from rep in context.InspectionReport
                                                  join IO in context.InspectionOrders on rep.InspectionOrderId equals IO.InspectionOrderID
                                                  join Msection in context.MainSections on rep.SectionId equals Msection.ID
                                                  join SSection in context.SubSections on rep.SubsectionId equals SSection.ID
                                                  where rep.InspectionOrderId == inspectionOrderId
                                                  select new DetailsReportCommonPoco
                                                  {
                                                      ID = "",
                                                      SectionID = rep.SectionId,
                                                      SubSectionID = rep.SubsectionId,
                                                      SectionName = Msection.SectionName,
                                                      SubSectionName = SSection.SubSectionName,
                                                      CheckboxID = rep.CheckboxID,
                                                      Type = rep.Type,
                                                      DirectionDropID = rep.DirectionDropID,
                                                      PresetComments = rep.PresetComments,
                                                      Comments = rep.Comments,
                                                      Notice = rep.Notice
                                                  }).OrderBy(x => x.SectionName).Skip(count).FirstOrDefault();

                    if (objDetailsReportCommonPoco != null)
                    {

                        var imagecount = (from PH in context.PhotoLibraries
                                          where PH.InspectionOrderID == inspectionOrderId &&
                                          PH.SectionID == objDetailsReportCommonPoco.SectionID &&
                                          PH.SubSectionId == objDetailsReportCommonPoco.SubSectionID
                                          select new
                                          {
                                              PH.InspectionOrderID,
                                              PH.DirectionDrop
                                          }).GroupBy(x => x.DirectionDrop);
                        if (imagecount.Count() > 0)
                        {
                            Session["TakeImg"] = 4;
                        }
                        Session["ImageCount"] = imagecount.Count();
                        Session["OrderId"] = inspectionOrderId;
                        Session["Msection"] = objDetailsReportCommonPoco.SectionID;
                        Session["Ssection"] = objDetailsReportCommonPoco.SubSectionID;
                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(objDetailsReportCommonPoco.SectionID, objDetailsReportCommonPoco.SubSectionID, inspectionOrderId, false);
                        if (objDetailsReportCommonPoco.ObjImagesNote.Count != 0)
                        {
                            objDetailsReportCommonPoco.Direction = objDetailsReportCommonPoco.ObjImagesNote.FirstOrDefault().Direction;
                        }

                        count++;
                        Session["ReportCount"] = count.ToString();
                    }
                }


                else
                {

                    string section = Session["Msection"].ToString();
                    string SubSection = Session["Ssection"].ToString();
                    objDetailsReportCommonPoco = (from rep in context.InspectionReport
                                                  join IO in context.InspectionOrders on rep.InspectionOrderId equals IO.InspectionOrderID
                                                  join Msection in context.MainSections on rep.SectionId equals Msection.ID
                                                  join SSection in context.SubSections on rep.SubsectionId equals SSection.ID
                                                  where rep.InspectionOrderId == inspectionOrderId && rep.SectionId == section
                                                  && rep.SubsectionId == SubSection
                                                  select new DetailsReportCommonPoco
                                                  {
                                                      ID = "",
                                                      SectionID = rep.SectionId,
                                                      SubSectionID = rep.SubsectionId,
                                                      SectionName = Msection.SectionName,
                                                      SubSectionName = SSection.SubSectionName,
                                                      CheckboxID = rep.CheckboxID,
                                                      Type = rep.Type,
                                                      DirectionDropID = rep.DirectionDropID,
                                                      PresetComments = rep.PresetComments,
                                                      Comments = rep.Comments,
                                                      Notice = rep.Notice
                                                  }).FirstOrDefault();

                    objDetailsReportCommonPoco.SubSectionID = Session["Ssection"].ToString();
                    objDetailsReportCommonPoco.SectionID = Session["Msection"].ToString();
                    objDetailsReportCommonPoco.ObjImagesNote = BindImages(Session["Msection"].ToString(), Session["Ssection"].ToString(), Session["OrderId"].ToString(), false);
                    if (objDetailsReportCommonPoco.ObjImagesNote.Count != 0)
                    {
                        objDetailsReportCommonPoco.Direction = objDetailsReportCommonPoco.ObjImagesNote.FirstOrDefault().Direction;
                    }
                }
                #region Comment

                //try
                //{
                //    bool status = true;

                //    for (int i = 0; i < mainSectionList.Count; i++)
                //    {
                //        if (status == false)
                //        {
                //            break;
                //        }
                //        var sectionId = mainSectionList[i].SectionID;
                //        var sectionName = mainSectionList[i].SectionName;

                //        subSectionList = subSectionList.Where(x => x.SectionID == sectionId).Select(y => new SubSectionList { SubSectionID = y.SubSectionID, SubSectionName = y.SubSectionName, SectionID = y.SectionID }).ToList();
                //        if (subSectionList.Count > 0)
                //        {

                //            for (int j = 0; j < subSectionList.Count; j++)
                //            {
                //                var subsectionid = subSectionList[j].SubSectionID;
                //                var subSectionName = subSectionList[j].SubSectionName;
                //                objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                //                if (sectionName == "Appliances")
                //                {
                //                    objDetailsReportCommonPoco = (from appliancescommonsections in context.Appliancescommonsections
                //                                                  where appliancescommonsections.SectionID == sectionId
                //                                                  && appliancescommonsections.InspectionOrderID == inspectionOrderId
                //                                                  && appliancescommonsections.SubSectionID == subsectionid
                //                                                  select new DetailsReportCommonPoco
                //                                                  {
                //                                                      ID = appliancescommonsections.ID,
                //                                                      SectionID = appliancescommonsections.SectionID,
                //                                                      SubSectionID = appliancescommonsections.SubSectionID,
                //                                                      SectionName = sectionName,
                //                                                      SubSectionName = subSectionName,
                //                                                      CheckboxID = appliancescommonsections.CheckboxID,
                //                                                      Type = appliancescommonsections.Type,
                //                                                      DirectionDropID = appliancescommonsections.DirectionDropID,
                //                                                      PresetComments = appliancescommonsections.PresetComments,
                //                                                      Comments = appliancescommonsections.Comments,
                //                                                      Notice = appliancescommonsections.Notice
                //                                                  }).FirstOrDefault();

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from appliancesDishwashDisposalRange_sections in context.AppliancesDishwashDisposalRange_sections
                //                                                      where appliancesDishwashDisposalRange_sections.SectionID == sectionId
                //                                                      && appliancesDishwashDisposalRange_sections.InsepectionOrderID == inspectionOrderId
                //                                                      && appliancesDishwashDisposalRange_sections.SubSectionId == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = appliancesDishwashDisposalRange_sections.ID,
                //                                                          SectionID = appliancesDishwashDisposalRange_sections.SectionID,
                //                                                          SubSectionID = appliancesDishwashDisposalRange_sections.SubSectionId,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = appliancesDishwashDisposalRange_sections.CheckBoxID,
                //                                                          Manufacturer = appliancesDishwashDisposalRange_sections.Manufacturer,
                //                                                          SerialNumber = appliancesDishwashDisposalRange_sections.SerialNumber,
                //                                                          Location = appliancesDishwashDisposalRange_sections.Location,
                //                                                          DirectionDropID = appliancesDishwashDisposalRange_sections.DirectionDropID,
                //                                                          PresetComments = appliancesDishwashDisposalRange_sections.PresetComment,
                //                                                          Comments = appliancesDishwashDisposalRange_sections.Comments,
                //                                                          Notice = appliancesDishwashDisposalRange_sections.Notice
                //                                                      }).FirstOrDefault();
                //                    }

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from appliancesDryerDoorBellSectiions in context.AppliancesDryerDoorBellSectiions
                //                                                      where appliancesDryerDoorBellSectiions.SectionID == sectionId
                //                                                      && appliancesDryerDoorBellSectiions.InspectionOrderID == inspectionOrderId
                //                                                      && appliancesDryerDoorBellSectiions.SubsectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = appliancesDryerDoorBellSectiions.ID,
                //                                                          SectionID = appliancesDryerDoorBellSectiions.SectionID,
                //                                                          SubSectionID = appliancesDryerDoorBellSectiions.SubsectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = appliancesDryerDoorBellSectiions.CheckBoxID,
                //                                                          DirectionDropID = appliancesDryerDoorBellSectiions.DirectionDropID,
                //                                                          PresetComments = appliancesDryerDoorBellSectiions.PresentComment,
                //                                                          Comments = appliancesDryerDoorBellSectiions.Comments,
                //                                                          Notice = appliancesDryerDoorBellSectiions.Notice
                //                                                      }).FirstOrDefault();
                //                    }

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from appliancesHoodVentOtherSections in context.AppliancesHoodVentOtherSections
                //                                                      where appliancesHoodVentOtherSections.SectionID == sectionId
                //                                                      && appliancesHoodVentOtherSections.InspectionOrderId == inspectionOrderId
                //                                                      && appliancesHoodVentOtherSections.SubsectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = appliancesHoodVentOtherSections.ID,
                //                                                          SectionID = appliancesHoodVentOtherSections.SectionID,
                //                                                          SubSectionID = appliancesHoodVentOtherSections.SubsectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = appliancesHoodVentOtherSections.CheckBoxID,
                //                                                          Type = appliancesHoodVentOtherSections.Type,
                //                                                          DirectionDropID = appliancesHoodVentOtherSections.DirectionDropID,
                //                                                          PresetComments = appliancesHoodVentOtherSections.PresetComment,
                //                                                          Comments = appliancesHoodVentOtherSections.Comments,
                //                                                          Notice = appliancesHoodVentOtherSections.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from appliancesOvenSections in context.AppliancesOvenSections
                //                                                      where appliancesOvenSections.SectionID == sectionId
                //                                                      && appliancesOvenSections.InspectionOrderID == inspectionOrderId
                //                                                      && appliancesOvenSections.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = appliancesOvenSections.ID,
                //                                                          SectionID = appliancesOvenSections.SectionID,
                //                                                          SubSectionID = appliancesOvenSections.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = appliancesOvenSections.CheckboxID,
                //                                                          Manufacturer = appliancesOvenSections.Manufacturer,
                //                                                          SerialNumber = appliancesOvenSections.SerialNumber,
                //                                                          Location = appliancesOvenSections.Location,
                //                                                          TempSetting = appliancesOvenSections.TempSetting,
                //                                                          TestResult = appliancesOvenSections.TestResult,
                //                                                          Variance = appliancesOvenSections.Variance,
                //                                                          DirectionDropID = appliancesOvenSections.DirectionDropID,
                //                                                          PresetComments = appliancesOvenSections.PresetComment,
                //                                                          Comments = appliancesOvenSections.Comments,
                //                                                          Notice = appliancesOvenSections.Notice
                //                                                      }).FirstOrDefault();
                //                    }

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        subSectionList.RemoveAt(j);
                //                        if (subSectionList.Count > 0)
                //                        {
                //                            j = j - 1;
                //                        }
                //                    }

                //                }

                //                else if (sectionName == "Electrical")
                //                {
                //                    objDetailsReportCommonPoco = (from electrialCommanSection in context.ElectrialCommanSection
                //                                                  where electrialCommanSection.SectionID == sectionId
                //                                                  && electrialCommanSection.ReportID == inspectionOrderId
                //                                                  && electrialCommanSection.SubsectionID == subsectionid
                //                                                  select new DetailsReportCommonPoco
                //                                                  {
                //                                                      ID = electrialCommanSection.ID,
                //                                                      SectionID = electrialCommanSection.SectionID,
                //                                                      SubSectionID = electrialCommanSection.SubsectionID,
                //                                                      SectionName = sectionName,
                //                                                      SubSectionName = subSectionName,
                //                                                      CheckboxID = electrialCommanSection.ConditionID,
                //                                                      Type = electrialCommanSection.TypeValue,
                //                                                      DirectionDropID = electrialCommanSection.DirctionDrop,
                //                                                      PresetComments = electrialCommanSection.PresetComment,
                //                                                      Comments = electrialCommanSection.Comments,
                //                                                      Notice = electrialCommanSection.NoticeValue
                //                                                  }).FirstOrDefault();

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }

                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from electricalPanel in context.ElectricalPanel
                //                                                      where electricalPanel.SectionID == sectionId
                //                                                      && electricalPanel.ReportID == inspectionOrderId
                //                                                      && electricalPanel.SubsectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = electricalPanel.ID,
                //                                                          SectionID = electricalPanel.SectionID,
                //                                                          SubSectionID = electricalPanel.SubsectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = electricalPanel.ConditionID,
                //                                                          Type = "",//electricalPanel.TypeValue,
                //                                                          DirectionDropID = electricalPanel.DirectionDrop,
                //                                                          PresetComments = electricalPanel.PresetComment,
                //                                                          Comments = electricalPanel.Comments,
                //                                                          Notice = electricalPanel.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from electricalRoom in context.ElectricalRoom
                //                                                      where electricalRoom.SectionID == sectionId
                //                                                      && electricalRoom.ReportID == inspectionOrderId
                //                                                      && electricalRoom.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = electricalRoom.ID,
                //                                                          SectionID = electricalRoom.SectionID,
                //                                                          SubSectionID = electricalRoom.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = electricalRoom.ConditionID,
                //                                                          Type = electricalRoom.RoomType,
                //                                                          DirectionDropID = electricalRoom.DirectionDrop,
                //                                                          PresetComments = electricalRoom.PresetComment,
                //                                                          Comments = electricalRoom.Comments,
                //                                                          Notice = electricalRoom.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from electricalService in context.ElectricalService
                //                                                      where electricalService.SectionID == sectionId
                //                                                      && electricalService.ReportID == inspectionOrderId
                //                                                      && electricalService.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = electricalService.ID,
                //                                                          SectionID = electricalService.SectionID,
                //                                                          SubSectionID = electricalService.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = electricalService.ConditionID,
                //                                                          Type = "",//electricalRoom.TypeValue,
                //                                                          DirectionDropID = electricalService.DirectionDrop,
                //                                                          PresetComments = electricalService.PresetComment,
                //                                                          Comments = electricalService.Comments,
                //                                                          Notice = electricalService.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        subSectionList.RemoveAt(j);
                //                        if (subSectionList.Count > 0)
                //                        {
                //                            j = j - 1;
                //                        }
                //                    }
                //                }

                //                  //Bind data for Grounds Section.
                //                else if (sectionName == "Grounds")
                //                {
                //                    objDetailsReportCommonPoco = (from groundCommonSections in context.GroundCommonSections
                //                                                  where groundCommonSections.SectionID == sectionId
                //                                                  && groundCommonSections.InspectionOrderID == inspectionOrderId
                //                                                  && groundCommonSections.SubSectionID == subsectionid
                //                                                  select new DetailsReportCommonPoco
                //                                                  {
                //                                                      ID = groundCommonSections.ID,
                //                                                      SectionID = groundCommonSections.SectionID,
                //                                                      SubSectionID = groundCommonSections.SubSectionID,
                //                                                      SectionName = sectionName,
                //                                                      SubSectionName = subSectionName,
                //                                                      CheckboxID = groundCommonSections.CheckBoxID,
                //                                                      Type = groundCommonSections.Type,
                //                                                      DirectionDropID = groundCommonSections.DirectionDropID,
                //                                                      PresetComments = groundCommonSections.PresetComment,
                //                                                      Comments = groundCommonSections.Comments,
                //                                                      Notice = groundCommonSections.Notice
                //                                                  }).FirstOrDefault();

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }

                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from groundGassections in context.GroundGassections
                //                                                      where groundGassections.SectionID == sectionId
                //                                                      && groundGassections.InspectionOrderID == inspectionOrderId
                //                                                      && groundGassections.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = groundGassections.ID,
                //                                                          SectionID = groundGassections.SectionID,
                //                                                          SubSectionID = groundGassections.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = groundGassections.CheckBoxID,
                //                                                          Type = "",//groundGassections.Type,
                //                                                          DirectionDropID = groundGassections.DirectionDropID,
                //                                                          PresetComments = groundGassections.PresetComment,
                //                                                          Comments = groundGassections.Comments,
                //                                                          Notice = groundGassections.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }

                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from groundH2OSections in context.GroundH2OSections
                //                                                      where groundH2OSections.SectionId == sectionId
                //                                                      && groundH2OSections.InspectionOrderID == inspectionOrderId
                //                                                      && groundH2OSections.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = groundH2OSections.ID,
                //                                                          SectionID = groundH2OSections.SectionId,
                //                                                          SubSectionID = groundH2OSections.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = groundH2OSections.CheckBoxID,
                //                                                          Type = "",//groundH2OSections.Type,
                //                                                          DirectionDropID = groundH2OSections.DirectionDropID,
                //                                                          PresetComments = groundH2OSections.PresetComment,
                //                                                          Comments = groundH2OSections.Comments,
                //                                                          Notice = groundH2OSections.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from groundSewerSections in context.GroundSewerSections
                //                                                      where groundSewerSections.SectionID == sectionId
                //                                                      && groundSewerSections.C_InspectionOrderID == inspectionOrderId
                //                                                      && groundSewerSections.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = groundSewerSections.ID,
                //                                                          SectionID = groundSewerSections.SectionID,
                //                                                          SubSectionID = groundSewerSections.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = groundSewerSections.CheckBoxID,
                //                                                          Type = "",//groundSewerSections.Type,
                //                                                          DirectionDropID = groundSewerSections.DirectionDropID,
                //                                                          PresetComments = groundSewerSections.PresetComment,
                //                                                          Comments = groundSewerSections.Comments,
                //                                                          Notice = groundSewerSections.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }

                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from groundSprinkSysSections in context.GroundSprinkSysSections
                //                                                      where groundSprinkSysSections.SectionID == sectionId
                //                                                      && groundSprinkSysSections.InspectionOrderID == inspectionOrderId
                //                                                      && groundSprinkSysSections.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = groundSprinkSysSections.ID,
                //                                                          SectionID = groundSprinkSysSections.SectionID,
                //                                                          SubSectionID = groundSprinkSysSections.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = groundSprinkSysSections.CheckBoxID,
                //                                                          Type = groundSprinkSysSections.Type,
                //                                                          DirectionDropID = groundSprinkSysSections.DirectionDropID,
                //                                                          PresetComments = groundSprinkSysSections.Preset,
                //                                                          Comments = groundSprinkSysSections.Comments,
                //                                                          Notice = groundSprinkSysSections.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        subSectionList.RemoveAt(j);
                //                        if (subSectionList.Count > 0)
                //                        {
                //                            j = j - 1;
                //                        }
                //                    }
                //                }

                //                else if (sectionName == "HVAC")
                //                {
                //                    objDetailsReportCommonPoco = (from hVACCommonSections in context.HVACCommonSections
                //                                                  where hVACCommonSections.SectionID == sectionId
                //                                                  && hVACCommonSections.InspectionOrderID == inspectionOrderId
                //                                                  && hVACCommonSections.SubSectionID == subsectionid
                //                                                  select new DetailsReportCommonPoco
                //                                                  {
                //                                                      ID = hVACCommonSections.ID,
                //                                                      SectionID = hVACCommonSections.SectionID,
                //                                                      SubSectionID = hVACCommonSections.SubSectionID,
                //                                                      SectionName = sectionName,
                //                                                      SubSectionName = subSectionName,
                //                                                      CheckboxID = hVACCommonSections.CheckBoxID,
                //                                                      Type = hVACCommonSections.Type,
                //                                                      DirectionDropID = hVACCommonSections.DirectionDropID,
                //                                                      PresetComments = hVACCommonSections.PresetComment,
                //                                                      Comments = hVACCommonSections.Comments,
                //                                                      Notice = hVACCommonSections.Notice
                //                                                  }).FirstOrDefault();

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }

                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from hVACCoolings in context.HVACCoolings
                //                                                      where hVACCoolings.SectionID == sectionId
                //                                                      && hVACCoolings.InspectionOrderID == inspectionOrderId
                //                                                      && hVACCoolings.SubsectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = hVACCoolings.ID,
                //                                                          SectionID = hVACCoolings.SectionID,
                //                                                          SubSectionID = hVACCoolings.SubsectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = hVACCoolings.CheckBoxID,
                //                                                          Type = hVACCoolings.Type,
                //                                                          DirectionDropID = hVACCoolings.DirectionDropID,
                //                                                          PresetComments = hVACCoolings.PresetComment,
                //                                                          Comments = hVACCoolings.Comments,
                //                                                          Notice = hVACCoolings.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }

                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from hVACDuctsVents in context.HVACDuctsVents
                //                                                      where hVACDuctsVents.SectionID == sectionId
                //                                                      && hVACDuctsVents.InspectionOrderID == inspectionOrderId
                //                                                      && hVACDuctsVents.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = hVACDuctsVents.ID,
                //                                                          SectionID = hVACDuctsVents.SectionID,
                //                                                          SubSectionID = hVACDuctsVents.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = hVACDuctsVents.CheckBoxID,
                //                                                          Type = hVACDuctsVents.Type,
                //                                                          DirectionDropID = hVACDuctsVents.DirectionDropID,
                //                                                          PresetComments = hVACDuctsVents.PresetComment,
                //                                                          Comments = hVACDuctsVents.Comments,
                //                                                          Notice = hVACDuctsVents.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }


                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from hVACEquipBoileRadiantSections in context.HVACEquipBoileRadiantSections
                //                                                      where hVACEquipBoileRadiantSections.SectionID == sectionId
                //                                                      && hVACEquipBoileRadiantSections.InspectionOrderId == inspectionOrderId
                //                                                      && hVACEquipBoileRadiantSections.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = hVACEquipBoileRadiantSections.ID,
                //                                                          SectionID = hVACEquipBoileRadiantSections.SectionID,
                //                                                          SubSectionID = hVACEquipBoileRadiantSections.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = hVACEquipBoileRadiantSections.CheckBoxID,
                //                                                          Type = hVACEquipBoileRadiantSections.Type,
                //                                                          DirectionDropID = hVACEquipBoileRadiantSections.DirectionDropID,
                //                                                          PresetComments = hVACEquipBoileRadiantSections.PresetComment,
                //                                                          Comments = hVACEquipBoileRadiantSections.Comments,
                //                                                          Notice = hVACEquipBoileRadiantSections.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        subSectionList.RemoveAt(j);
                //                        if (subSectionList.Count > 0)
                //                        {
                //                            j = j - 1;
                //                        }
                //                    }
                //                }
                //                else if (sectionName == "Optional")
                //                {
                //                    objDetailsReportCommonPoco = (from optionalCommonSections in context.OptionalCommonSections
                //                                                  where optionalCommonSections.SectionID == sectionId
                //                                                  && optionalCommonSections.InspectionOrderID == inspectionOrderId
                //                                                  && optionalCommonSections.SubSectionID == subsectionid
                //                                                  select new DetailsReportCommonPoco
                //                                                  {
                //                                                      ID = optionalCommonSections.ID,
                //                                                      SectionID = optionalCommonSections.SectionID,
                //                                                      SubSectionID = optionalCommonSections.SubSectionID,
                //                                                      SectionName = sectionName,
                //                                                      SubSectionName = subSectionName,
                //                                                      CheckboxID = optionalCommonSections.CheckBoxID,
                //                                                      Type = optionalCommonSections.Type,
                //                                                      DirectionDropID = optionalCommonSections.DirectionDropID,
                //                                                      PresetComments = optionalCommonSections.PresetComment,
                //                                                      Comments = optionalCommonSections.Comments,
                //                                                      Notice = optionalCommonSections.Notice
                //                                                  }).FirstOrDefault();

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }

                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from optionalTestingSections in context.OptionalTestingSections
                //                                                      where optionalTestingSections.SectionID == sectionId
                //                                                      && optionalTestingSections.InspectionOrderID == inspectionOrderId
                //                                                      && optionalTestingSections.SubsectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = optionalTestingSections.ID,
                //                                                          SectionID = optionalTestingSections.SectionID,
                //                                                          SubSectionID = optionalTestingSections.SubsectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = optionalTestingSections.CheckBoxID,
                //                                                          Type = optionalTestingSections.Type,
                //                                                          DirectionDropID = optionalTestingSections.DirectionDropID,
                //                                                          PresetComments = optionalTestingSections.PresetComment,
                //                                                          Comments = "",//optionalTestingSections.Comments,
                //                                                          Notice = optionalTestingSections.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        subSectionList.RemoveAt(j);
                //                        if (subSectionList.Count > 0)
                //                        {
                //                            j = j - 1;
                //                        }
                //                    }
                //                }
                //                else if (sectionName == "Plumbing")
                //                {
                //                    objDetailsReportCommonPoco = (from plumbingCommonSections in context.PlumbingCommonSections
                //                                                  where plumbingCommonSections.SectionID == sectionId
                //                                                  && plumbingCommonSections.InspectionOrderId == inspectionOrderId
                //                                                  && plumbingCommonSections.SubSectionID == subsectionid
                //                                                  select new DetailsReportCommonPoco
                //                                                  {
                //                                                      ID = plumbingCommonSections.ID,
                //                                                      SectionID = plumbingCommonSections.SectionID,
                //                                                      SubSectionID = plumbingCommonSections.SubSectionID,
                //                                                      SectionName = sectionName,
                //                                                      SubSectionName = subSectionName,
                //                                                      CheckboxID = plumbingCommonSections.CheckBoxID,
                //                                                      Type = plumbingCommonSections.Type,
                //                                                      DirectionDropID = plumbingCommonSections.DirectionDropID,
                //                                                      PresetComments = plumbingCommonSections.PresetComment,
                //                                                      Comments = plumbingCommonSections.Comments,
                //                                                      Notice = plumbingCommonSections.Notice
                //                                                  }).FirstOrDefault();

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }

                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from plumbingMainlineKitchenOthers_Sections in context.PlumbingMainlineKitchenOthers_Sections
                //                                                      where plumbingMainlineKitchenOthers_Sections.SectionID == sectionId
                //                                                      && plumbingMainlineKitchenOthers_Sections.InspectionOrderId == inspectionOrderId
                //                                                      && plumbingMainlineKitchenOthers_Sections.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = plumbingMainlineKitchenOthers_Sections.ID,
                //                                                          SectionID = plumbingMainlineKitchenOthers_Sections.SectionID,
                //                                                          SubSectionID = plumbingMainlineKitchenOthers_Sections.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = plumbingMainlineKitchenOthers_Sections.CheckBoxID,
                //                                                          Type = plumbingMainlineKitchenOthers_Sections.Type,
                //                                                          DirectionDropID = plumbingMainlineKitchenOthers_Sections.DirectionDropID,
                //                                                          PresetComments = plumbingMainlineKitchenOthers_Sections.PresetComment,
                //                                                          Comments = plumbingMainlineKitchenOthers_Sections.Comments,
                //                                                          Notice = plumbingMainlineKitchenOthers_Sections.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }

                //                    if (objDetailsReportCommonPoco == null)
                //                    {
                //                        objDetailsReportCommonPoco = (from plumbingWaterheats in context.PlumbingWaterheats
                //                                                      where plumbingWaterheats.SectionId == sectionId
                //                                                      && plumbingWaterheats.InspectionOrderId == inspectionOrderId
                //                                                      && plumbingWaterheats.SubSectionID == subsectionid
                //                                                      select new DetailsReportCommonPoco
                //                                                      {
                //                                                          ID = plumbingWaterheats.ID,
                //                                                          SectionID = plumbingWaterheats.SectionId,
                //                                                          SubSectionID = plumbingWaterheats.SubSectionID,
                //                                                          SectionName = sectionName,
                //                                                          SubSectionName = subSectionName,
                //                                                          CheckboxID = plumbingWaterheats.CheckBoxID,
                //                                                          Type = plumbingWaterheats.Type,
                //                                                          DirectionDropID = plumbingWaterheats.DirectionDropID,
                //                                                          PresetComments = plumbingWaterheats.PresetComment,
                //                                                          Comments = plumbingWaterheats.Comments,
                //                                                          Notice = plumbingWaterheats.Notice
                //                                                      }).FirstOrDefault();
                //                    }
                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        subSectionList.RemoveAt(j);
                //                        if (subSectionList.Count > 0)
                //                        {
                //                            j = j - 1;
                //                        }
                //                    }

                //                }
                //                else if (sectionName == "Structural")
                //                {
                //                    objDetailsReportCommonPoco = (from structuralCommonSections in context.StructuralCommonSections
                //                                                  where structuralCommonSections.SectionId == sectionId
                //                                                  && structuralCommonSections.InspectionOrderID == inspectionOrderId
                //                                                  && structuralCommonSections.SubSectionID == subsectionid
                //                                                  select new DetailsReportCommonPoco
                //                                                  {
                //                                                      ID = structuralCommonSections.ID,
                //                                                      SectionID = structuralCommonSections.SectionId,
                //                                                      SubSectionID = structuralCommonSections.SubSectionID,
                //                                                      SectionName = sectionName,
                //                                                      SubSectionName = subSectionName,
                //                                                      CheckboxID = structuralCommonSections.CheckBoxiD,
                //                                                      Type = structuralCommonSections.Type,
                //                                                      DirectionDropID = structuralCommonSections.DirectionDropID,
                //                                                      PresetComments = structuralCommonSections.PresetComment,
                //                                                      Comments = structuralCommonSections.Comments,
                //                                                      Notice = structuralCommonSections.Notice
                //                                                  }).FirstOrDefault();

                //                    if (objDetailsReportCommonPoco != null)
                //                    {
                //                        objDetailsReportCommonPoco.ObjImagesNote = BindImages(sectionId, subsectionid, inspectionOrderId);
                //                        subSectionList.RemoveAt(j);
                //                        status = false;
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        subSectionList.RemoveAt(j);
                //                        if (subSectionList.Count > 0)
                //                        {
                //                            j = j - 1;
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //        else
                //        {
                //            mainSectionList.RemoveAt(i);
                //            if (mainSectionList.Count > 0)
                //            {
                //                string sectionidLocal = mainSectionList[i].SectionID;
                //                subSectionList = context.SubSections.Where(x => x.SectionId == sectionidLocal).Select(y => new SubSectionList { SubSectionID = y.ID, SubSectionName = y.SubSectionName, SectionID = y.SectionId }).ToList();
                //                i = -1;
                //            }

                //        }
                //        if (subSectionList.Count == 0)
                //        {
                //            mainSectionList.RemoveAt(i);
                //            if (mainSectionList.Count > 0)
                //            {
                //                string sectionidLocal = mainSectionList[i].SectionID;
                //                subSectionList = context.SubSections.Where(x => x.SectionId == sectionidLocal).Select(y => new SubSectionList { SubSectionID = y.ID, SubSectionName = y.SubSectionName, SectionID = y.SectionId }).ToList();
                //                i = -1;
                //            }
                //        }
                //    }
                #endregion
                if (objDetailsReportCommonPoco != null)
                {
                    _messageToShow.Result = RenderRazorViewToString("_ViewDetailReport", objDetailsReportCommonPoco);
                }
                else
                {
                    /*If any section is updated by inspector than that section append in report--Start Here*/
                    PropertyAddress objPropertyAddress = null;
                    List<ImagesNote> ObjImagesNotes1 = null;
                    DetailsReportCommonPoco objReport = null;

                    int UpdateCount = Convert.ToInt32(Session["UpdateReportCount"]);
                    objPropertyAddress = (from SectionStatus in context.SubSectionStatu
                                          join Section in context.MainSections on SectionStatus.SectionID equals Section.ID
                                          join Subsection in context.SubSections on SectionStatus.SubSectionID equals Subsection.ID
                                          where SectionStatus.InspectionOrderID == inspectionOrderId && SectionStatus.Status == true
                                          select new PropertyAddress
                                          {
                                              SectionID = SectionStatus.SectionID,
                                              OrderID = SectionStatus.InspectionOrderID,
                                              SubSectionID = SectionStatus.SubSectionID,
                                              SectionName = Section.SectionName,
                                              SubSectionName = Subsection.SubSectionName,

                                          }).OrderBy(x => x.SectionName).Skip(UpdateCount).FirstOrDefault();

                    if (objPropertyAddress != null)
                    {

                        objReport = (from PL in context.PhotoLibraries
                                     where PL.InspectionOrderID == objPropertyAddress.OrderID && PL.SectionID == objPropertyAddress.SectionID &&
                                     PL.SubSectionId == objPropertyAddress.SubSectionID && PL.AcceptedOrRejected == true
                                     select new DetailsReportCommonPoco
                                     {

                                         ID = "",
                                         SectionID = PL.SectionID,
                                         SubSectionID = PL.SubSectionId,
                                         SectionName = objPropertyAddress.TableName,
                                         SubSectionName = objPropertyAddress.SubSectionName,
                                         Comments = PL.Comments,
                                         CheckboxID = "1",
                                         Type = "",
                                         DirectionDropID = 0,
                                         PresetComments = "",
                                         Notice = "",
                                         CreatedDate = PL.CreatedDate
                                     }).OrderByDescending(x => x.CreatedDate).FirstOrDefault();

                        ObjImagesNotes1 = (from PL in context.PhotoLibraries
                                           where PL.InspectionOrderID == objPropertyAddress.OrderID && PL.SectionID == objPropertyAddress.SectionID &&
                                           PL.SubSectionId == objPropertyAddress.SubSectionID && PL.AcceptedOrRejected == true
                                           select new ImagesNote
                                           {
                                               ImageUrl = PL.PhotoUrl,
                                               CreatedDate = PL.CreatedDate
                                           }).OrderByDescending(x => x.CreatedDate).Take(4).ToList();

                        objReport.ObjImagesNote = ObjImagesNotes1;
                        UpdateCount++;
                        Session["UpdateReportCount"] = UpdateCount.ToString();
                        _messageToShow.Result = RenderRazorViewToString("_ViewDetailReport", objReport);

                    }
                    /*End Here*/
                    else
                    {

                        _messageToShow.Message = "No more Data";
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ReturnJson(_messageToShow);
        }

        #endregion

        [HttpPost]
        public JsonResult ShowDetailReport(string inspectionOrderId, string PaymentType)
        {
            try
            {
                DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
                List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
                List<DetailsReportCommonPoco> objUpdatableReport = new List<DetailsReportCommonPoco>();
                // int count = 0;
                objReport = (from rep in context.InspectionReport
                             join Msection in context.MainSections on rep.SectionId equals Msection.ID
                             join SSection in context.SubSections on rep.SubsectionId equals SSection.ID
                             join direction in context.DirectionDropMaster on rep.DirectionDropID equals direction.DirectionDropID into order
                             where rep.InspectionOrderId == inspectionOrderId
                             select new DetailsReportCommonPoco
                             {
                                 ID = rep.ItemID,
                                 InspectionReportId = rep.ID,
                                 SectionID = rep.SectionId,
                                 SubSectionID = rep.SubsectionId,
                                 SectionName = Msection.SectionName,
                                 SubSectionName = SSection.SubSectionName,
                                 InspectionOrderID = rep.InspectionOrderId,
                                 CheckboxID = rep.CheckboxID,
                                 IsUpdated = rep.IsUpdated,
                                 IsInspectorReviewed = rep.IsInspectorReviewed,
                                 Type = rep.Type,
                                 DirectionDropID = rep.DirectionDropID,
                                 //  Direction = direction.DirectionDropValue,
                                 DirectionDropMaster = order,
                                 PresetComments = rep.PresetComments,
                                 Comments = rep.Comments,
                                 Notice = rep.Notice,
                                 MSortOrder = Msection.SortOrder,
                                 SSortOrder = SSection.SortOrder,
                                 IsUpdateSection = false,
                                 PL = (from p in context.PhotoLibraries
                                       where p.InpectionReportFID == rep.ItemID
                                       select new ImagesNote
                                      {
                                          ImageUrl = p.PhotoUrl,
                                          ImageNote = p.Notes
                                      }),
                                 UpdateImages = (from k in context.tblPhotoLibrary
                                                 where k.InspectionReportID == rep.ID && k.IsUpdated == true
                                                 select new UpdatedSection
                                                 {
                                                     PhotoURL1 = k.PhotoURL1,
                                                     PhotoURL2 = k.PhotoURL2,
                                                     PhotoURL3 = k.PhotoURL3,
                                                     PhotoURL4 = k.PhotoURL4,
                                                     IsApproved = k.IsApproved,
                                                     SellerComments = k.SellerComment,
                                                     InspectorComments = k.InspectorComment,
                                                     InspectionReportID = k.InspectionReportID,
                                                     CreatedDate = k.CreatedDate,
                                                     ID = k.ID,
                                                 }
                                               ).OrderBy(x => x.CreatedDate)
                             }).OrderBy(x => x.MSortOrder).ThenBy(z => z.SSortOrder).ToList();


                //#region Get Report Count
                //SampleReportCount objSampleReportCount = new SampleReportCount();
                //var Temp_Report = from IR in context.InspectionReport
                //                  where IR.InspectionOrderId == inspectionOrderId
                //                  group IR by IR.CheckboxID into g
                //                  select new
                //                  {
                //                      Category = g.Key,
                //                      Products = g
                //                  };

                //foreach (var item in Temp_Report)
                //{

                //    if (item.Category == "1")
                //        objSampleReportCount.GreenArrowCount = item.Products.Count();
                //    else if (item.Category == "2")
                //        objSampleReportCount.YellowArrowCount = item.Products.Count();
                //    else if (item.Category == "3")
                //        objSampleReportCount.RedArrowCount = item.Products.Count();
                //    else if (item.Category == "4")
                //        objSampleReportCount.DoubleRedArrowCount = item.Products.Count();
                //    else if (item.Category == "5")
                //        objSampleReportCount.YellowWarningCount = item.Products.Count();
                //    else if (item.Category == "6")
                //        objSampleReportCount.RedWarningCount = item.Products.Count();
                //    else if (item.Category == "7")
                //        objSampleReportCount.No_inspection = item.Products.Count();
                //    else if (item.Category == "8")
                //        objSampleReportCount.Not_accessibility = item.Products.Count();
                //}
                //objSampleReportCount.BlueIconCount = (from IR in context.InspectionReport
                //                                      join PL in context.tblPhotoLibrary on IR.ID equals PL.InspectionReportID
                //                                      where IR.InspectionOrderId == inspectionOrderId && PL.IsApproved == true
                //                                      select new Report
                //                                      {
                //                                          OrderID = PL.ID
                //                                      }).Count();

                //#endregion
                

                if (objReport != null)
                {
                    foreach (var item in objReport)
                    {
                        string ReportID = item.InspectionOrderID;
                        string secID = item.SectionID;
                        string subsecID = item.SubSectionID;

                        var tempdata = context.tblisincluded.FirstOrDefault(x => x.ReportId == ReportID && x.SectionId == secID && x.SubSectionId == subsecID);
                        if (tempdata != null)
                        {
                            bool? CheckInclude = tempdata.IsIncluded;
                            if ((bool)CheckInclude)
                                item.IsIncluded = true;
                            else if (!(bool)CheckInclude)
                                item.IsIncluded = false;
                        }
                    }

                    objDetailsReportCommonPoco.ReportsDetail = objReport;
                    //   objSampleReportCount.BlueIconCount = count;
                    //objDetailsReportCommonPoco.ReportCount = objSampleReportCount;
                }

                _messageToShow.Result = RenderRazorViewToString("_ViewDetailReport", objDetailsReportCommonPoco);
                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<ImagesNote> BindSectionImages(string p)
        {
            try
            {
                List<ImagesNote> ObjImagesNotes = null;
                ObjImagesNotes = (from imagesLibrary in context.PhotoLibraries
                                  join Direction in context.DirectionDropMaster on imagesLibrary.DirectionDrop equals Direction.DirectionDropID
                                  where imagesLibrary.InpectionReportFID == p
                                  select new ImagesNote
                                  {
                                      ImageUrl = imagesLibrary.PhotoUrl,
                                      ImageNote = imagesLibrary.Notes,
                                      CreatedDate = imagesLibrary.CreatedDate,
                                      Direction = Direction.DirectionDropValue
                                  }).OrderBy(item => item.CreatedDate).ToList();
                return ObjImagesNotes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region CheckUserPayment
        /// <summary>
        /// POST: Check Buyer have purchased plan or have paid for specific report
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CheckUserPayment(string orderid)
        {
            try
            {

                List<payment> objListofPayments = new List<payment>();
                objListofPayments = context.Payments.Where(x => x.UserID == Sessions.LoggedInUser.UserId && x.PaymentType == "PaymentPlan").ToList();

                string TypePayment = string.Empty;
                DateTime PaymentDate;
                string OrderId = string.Empty;
                bool flag = false;
                string Message = "Unsubscribed";
                if (objListofPayments.Count > 0)
                {
                    foreach (var PaymentPlan in objListofPayments)
                    {
                        TypePayment = PaymentPlan.PaymentType;
                        OrderId = PaymentPlan.ReportID;
                        if (TypePayment == "PaymentPlan")
                        {
                            PaymentDate = (DateTime)PaymentPlan.PlanValidity;
                            if (PaymentDate.Date >= System.DateTime.Now.Date)
                            {
                                flag = true;
                                Message = "Subscribed";
                                break;
                            }
                            else
                            {
                                Message = "Subscription Over";
                            }
                        }
                    }
                }
                return Json(Message);
            }
            catch (Exception e)
            {
                return Json("Error");
            }

        }
        #endregion

        #region CheckSellerUserPayment
        /// <summary>
        /// POST: Check Seller have paid for specific report
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CheckSellerUserPayment(string orderid)
        {
            try
            {
                //Get Machine IP Address
                string ipaddress = "";
                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipaddress == "" || ipaddress == null)
                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                //End

                _messageToShow = new MessageToShow();
                List<string> tempvalues = new List<string>();
                var objSellerPaymentResult = (from n in context.InspectionOrders
                                              join k in context.Payments
                                              on n.InspectionOrderID equals k.ReportID
                                              into IO
                                              from I in IO.Where(f => f.ReportID == orderid && f.UserID == Sessions.LoggedInUser.UserId).DefaultIfEmpty()
                                              where n.InspectionOrderID == orderid
                                              select new
                                              {
                                                  IO,
                                                  I.PaymentStatus,
                                                  n.IsUpdatable,
                                                  I.PaymentType,
                                                  I.CreatedDate,
                                                  I.UserID
                                              }).ToList().OrderByDescending(x => x.CreatedDate).FirstOrDefault();

                if (objSellerPaymentResult.PaymentStatus != null)
                {
                    string PaymentId = objSellerPaymentResult.IO.Where(x => x.UserID == Sessions.LoggedInUser.UserId).OrderByDescending(x => x.CreatedDate).FirstOrDefault().PaymentID;
                    bool CanViewReport = true;
                    if (objSellerPaymentResult.PaymentType == "HomeSeller" || objSellerPaymentResult.PaymentType == "PaymentPlan" || objSellerPaymentResult.PaymentType == "AdditionalTestingPayment")
                    {
                        // string ipaddress = "";
                        ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                        if (ipaddress == "" || ipaddress == null)
                            ipaddress = Request.ServerVariables["REMOTE_ADDR"];

                        var reportcount = (from RV in context.ReportViewed
                                           where RV.PaymentID == PaymentId && RV.MachineIP != ipaddress
                                           select new
                                           {
                                               RV.ID,
                                               RV.MachineIP
                                           }).GroupBy(x => x.MachineIP).Count();
                        if (reportcount >= 5)
                        {
                            CanViewReport = false;
                        }

                    }

                    _messageToShow.Message = Convert.ToString(objSellerPaymentResult.IsUpdatable);
                    _messageToShow.Result = objSellerPaymentResult.PaymentType == "OneTimePaymentPlan" ? "True" : "False";
                    _messageToShow.Successfull = CanViewReport;
                    //_messageToShow.Result = Convert.ToString(objSellerPaymentResult.PaymentStatus == "Success" ? "True" : "False");
                    tempvalues.Add(_messageToShow.Message);
                    tempvalues.Add(orderid);
                    TempData["IsUpdatable"] = tempvalues;
                }
                else
                {
                    _messageToShow.Message = "False";
                    tempvalues.Add(_messageToShow.Message);
                    tempvalues.Add(orderid);
                    TempData["IsUpdatable"] = tempvalues;
                    _messageToShow.Message = null;
                }
                return Json(_messageToShow);
            }
            catch (Exception e)
            {
                return Json("Error");
            }

        }

        #endregion

        #region CheckBuyerUserPayment
        /// <summary>
        /// POST: Check Buyer have paid for specific report
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CheckBuyerUserPayment()
        {
            try
            {
                _messageToShow = new MessageToShow();
                List<string> tempvalues = new List<string>();
                var objSellerPaymentResult = from payment in context.Payments
                                             where payment.PlanValidity >= System.DateTime.Now && payment.PaymentType == "PaymentPlan"
                                             select payment;
                if (objSellerPaymentResult.Any())
                {
                    _messageToShow.Message = "False";

                }
                else
                {
                    _messageToShow.Message = "True";
                }

                return Json(_messageToShow);
            }
            catch (Exception e)
            {
                return Json("Error");
            }

        }

        #endregion

        #region SampleInspectionReport
        //
        // GET: /SampleReportCount/

        public ActionResult SampleInspectionReport(string orderid)
        {

            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    SampleReportCount objSampleReportCount = new SampleReportCount();
                    var Temp_Report = from IR in context.InspectionReport
                                      where IR.InspectionOrderId == orderid
                                      group IR by IR.CheckboxID into g
                                      select new
                                      {
                                          Category = g.Key,
                                          Products = g
                                      };

                    foreach (var item in Temp_Report)
                    {

                        if (item.Category == "1")
                            objSampleReportCount.GreenArrowCount = item.Products.Count();
                        else if (item.Category == "2")
                            objSampleReportCount.YellowArrowCount = item.Products.Count();
                        else if (item.Category == "3")
                            objSampleReportCount.RedArrowCount = item.Products.Count();
                        else if (item.Category == "4")
                            objSampleReportCount.DoubleRedArrowCount = item.Products.Count();
                        else if (item.Category == "5")
                            objSampleReportCount.YellowWarningCount = item.Products.Count();
                        else if (item.Category == "6")
                            objSampleReportCount.RedWarningCount = item.Products.Count();
                        else if (item.Category == "7")
                            objSampleReportCount.No_inspection = item.Products.Count();
                        else if (item.Category == "8")
                            objSampleReportCount.Not_accessibility = item.Products.Count();
                    }

                    objSampleReportCount.BlueIconCount = (from IR in context.InspectionReport
                                                          join PL in context.tblPhotoLibrary on IR.ID equals PL.InspectionReportID
                                                          where IR.InspectionOrderId == orderid && PL.IsApproved == true
                                                          select new Report
                                                          {
                                                              OrderID = PL.ID
                                                          }).Count();


                    ReportPropertyAddress objAddress = new ReportPropertyAddress();
                    objAddress = (from IO in context.InspectionOrders
                                  join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                  where IO.InspectionOrderID == orderid && IO.InspectionStatus == "Completed"
                                  select new ReportPropertyAddress
                                  {
                                      HouseNo = IOD.PropertyAddress,
                                      PropertyCity = IOD.PropertyAddressCity,
                                      PropertyState = IOD.PropertyAddressState,
                                      PropertyZip = IOD.PropertyAddressZip
                                  }).FirstOrDefault();
                    objSampleReportCount.SingleReportPrice = 45.99F;
                    ViewBag.OrderId = orderid;
                    if (objAddress != null)
                    {
                        objSampleReportCount.Address = objAddress;
                    }
                    return View(objSampleReportCount);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult SampleInspectionReportSeller(string orderid)
        {

            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    string ipaddress = "";
                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (ipaddress == "" || ipaddress == null)
                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];

                    reportviewed objreport;
                    objreport = context.ReportViewed.Where(x => x.ReportID == orderid && x.UserID == Sessions.LoggedInUser.UserId).FirstOrDefault();
                    if (objreport != null)
                    {
                        objreport.ModifedDate = DateTime.Now;
                    }
                    else
                    {
                        objreport = new reportviewed();
                        objreport.ID = Guid.NewGuid().ToString();
                        objreport.ReportID = orderid;
                        objreport.UserID = Sessions.LoggedInUser.UserId;
                        objreport.MachineIP = ipaddress;
                        objreport.CreatedDate = System.DateTime.Now;
                        objreport.IsPurchased = false;
                        context.ReportViewed.Add(objreport);

                    }
                    context.SaveChanges();

                    var Commnet = (from IR in context.InspectionReport
                                   where IR.InspectionOrderId == orderid && IR.Comments != null && IR.Comments != string.Empty
                                   select new
                                   {
                                       ID = IR.ID
                                   }).Count();

                    var PCommnet = (from IR in context.InspectionReport
                                    where IR.InspectionOrderId == orderid && IR.PresetComments != null && IR.PresetComments != string.Empty
                                    select new
                                    {
                                        ID = IR.ID
                                    }).Count();

                    var Photo = (from p in context.PhotoLibraries
                                 where p.InspectionOrderID == orderid
                                 select new
                                 {
                                     ID = p.ID
                                 }).Count();
                    ViewBag.Photo = Convert.ToInt32(Photo);
                    ViewBag.Comments = Convert.ToInt32(Commnet) + Convert.ToInt32(PCommnet);

                    SampleReportCount objSampleReportCount = new SampleReportCount();
                    MySqlConnection conn = new MySqlConnection(connectionstring);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("SampleInspectionReportSeller", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserID", Sessions.LoggedInUser.UserId);
                    cmd.Parameters.AddWithValue("OrderID", orderid);
                    cmd.Parameters.AddWithValue("IPAddress", ipaddress);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {


                            if (item["CheckBoxId"].ToString() == "1")
                                objSampleReportCount.GreenArrowCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "2")
                                objSampleReportCount.YellowArrowCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "3")
                                objSampleReportCount.RedArrowCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "4")
                                objSampleReportCount.DoubleRedArrowCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "5")
                                objSampleReportCount.YellowWarningCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "6")
                                objSampleReportCount.RedWarningCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "7")
                                objSampleReportCount.No_inspection = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "8")
                                objSampleReportCount.Not_accessibility = Convert.ToInt32(item["count"]);
                        }
                        objSampleReportCount.BlueIconCount = (from IR in context.InspectionReport
                                                              join PL in context.tblPhotoLibrary on IR.ID equals PL.InspectionReportID
                                                              where IR.InspectionOrderId == orderid && PL.IsApproved == true
                                                              select new Report
                                                              {
                                                                  OrderID = PL.ID
                                                              }).Count();
                    }
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                    {
                        ReportPropertyAddress ReportAddress = new ReportPropertyAddress();


                        ReportAddress.HouseNo = ds.Tables[1].Rows[0]["HouseNo"].ToString();
                        ReportAddress.PropertyCity = ds.Tables[1].Rows[0]["PropertyCity"].ToString();
                        ReportAddress.PropertyState = ds.Tables[1].Rows[0]["PropertyState"].ToString();
                        ReportAddress.PropertyZip = Convert.ToInt32(ds.Tables[1].Rows[0]["PropertyZip"]);
                        ReportAddress.ReportNo = ds.Tables[1].Rows[0]["ReportNo"].ToString();
                        objSampleReportCount.Address = ReportAddress;
                    }
                    #region Dropdowns


                    List<SelectListItem> month = new List<SelectListItem>();
                    SelectListItem objSelectListItemmonth = new SelectListItem();
                    for (int i = 1; i <= 12; i++)
                    {
                        SelectListItem s = new SelectListItem();
                        if (i < 10)
                        {
                            s.Text = "0" + i.ToString();
                            s.Value = "0" + i.ToString();
                        }
                        else
                        {
                            s.Text = i.ToString();
                            s.Value = i.ToString();
                        }

                        month.Add(s);
                    }
                    ViewBag.Month = new SelectList(month, "Value", "Text", null);


                    List<SelectListItem> year = new List<SelectListItem>();
                    SelectListItem objSelectListItemyear = new SelectListItem();
                    for (int i = 2013; i <= 2022; i++)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = i.ToString();
                        s.Value = i.ToString();
                        year.Add(s);
                    }

                    ViewBag.Year = new SelectList(year, "Value", "Text", null);


                    #endregion
                    #region Bind States
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "--Select--";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    ViewBag.StateList = items;
                    ViewBag.ZipList = objZipCodeList;
                    ViewBag.CityList = objCityList;

                    #endregion

                    int[] template = { 31, 37 };
                    IEnumerable<emailtemplate> objListOfEmailTemplates = from emailtemplate in context.EmailTemplates
                                                                         where template.Contains(emailtemplate.TemplateId)
                                                                         select emailtemplate;
                    foreach (var item in objListOfEmailTemplates)
                    {
                        if (item.TemplateName == "OneTimePayment")
                        {
                            objSampleReportCount.OneTimePayment = (float)item.Rate;
                            objSampleReportCount.OneTimePaymentValidity = item.Validity;
                        }
                        else
                        {
                            objSampleReportCount.PurchasePlan = (float)item.Rate;
                            objSampleReportCount.PurchasePlanValidity = item.Validity;
                        }

                    }
                    var PreviousAmount = (from pay in context.Payments
                                          join IO in context.tblinspectionorder on pay.ReportID equals IO.InspectionOrderDetailId
                                          where pay.UserID == Sessions.LoggedInUser.UserId && IO.IsDeleted == false
                                          select new
                                          {
                                              amount = pay.PaymentAmount
                                          }).Sum(x => x.amount);
                    if (PreviousAmount != null)
                    {
                        float amount = Convert.ToSingle(PreviousAmount);
                        if (objSampleReportCount.PurchasePlan <= amount)
                        {
                            ViewBag.IsPaid = "Already Paid";
                            ViewBag.PaidAmount = amount;
                        }
                        else
                        {
                            objSampleReportCount.PurchasePlan = objSampleReportCount.PurchasePlan - amount;
                            ViewBag.Amount = objSampleReportCount.PurchasePlan - amount;
                            ViewBag.PaidAmount = amount;
                        }
                    }
                    else
                    {
                        ViewBag.PaidAmount = 0;
                    }
                    return View(objSampleReportCount);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region FreeBasicReport



        public ActionResult FreeBasicReport(string UniqueId)
        {
            try
            {

                if (UniqueId != null && UniqueId != string.Empty)
                {
                    string inspectionorderid = context.tblinspectionorder.Where(x => x.ReportNo == UniqueId).FirstOrDefault().InspectionOrderDetailId;


                    var Commnet = (from IR in context.InspectionReport
                                   where IR.InspectionOrderId == inspectionorderid && IR.Comments != null && IR.Comments != string.Empty
                                   select new
                                   {
                                       ID = IR.ID
                                   }).Count();

                    var PCommnet = (from IR in context.InspectionReport
                                    where IR.InspectionOrderId == inspectionorderid && IR.PresetComments != null && IR.PresetComments != string.Empty
                                    select new
                                    {
                                        ID = IR.ID
                                    }).Count();

                    var Photo = (from p in context.PhotoLibraries
                                 where p.InspectionOrderID == inspectionorderid
                                 select new
                                 {
                                     ID = p.ID
                                 }).Count();
                    ViewBag.Photo = Convert.ToInt32(Photo);
                    ViewBag.Comments = Convert.ToInt32(Commnet) + Convert.ToInt32(PCommnet);

                    SampleReportCount objSampleReportCount = new SampleReportCount();
                    var Temp_Report = from IR in context.InspectionReport
                                      join IO in context.tblinspectionorder on IR.InspectionOrderId equals IO.InspectionOrderDetailId
                                      where IO.ReportNo == UniqueId && IO.InspectionStatus == "Completed" && IO.IsDeleted == false
                                      group IR by IR.CheckboxID into g
                                      select new
                                      {
                                          Category = g.Key,
                                          Products = g,


                                      };

                    ReportPropertyAddress objAddress = new ReportPropertyAddress();
                    objAddress = (from IO in context.tblinspectionorder
                                  join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                  join states in context.tblstate on IOD.PropertyState equals states.StateId
                                  join City in context.tblcity on IOD.PropertyCity equals City.CityId
                                  join Zip in context.tblzip on IOD.PropertyZip equals Zip.ZipId
                                  where IO.ReportNo == UniqueId && IO.InspectionStatus == "Completed" && IO.IsDeleted == false
                                  select new ReportPropertyAddress
                                          {
                                              HouseNo = IOD.PropertyAddress,
                                              PropertyCity = City.CityName,
                                              PropertyState = states.StateName,
                                              PropertyZip = Zip.Zip,
                                          }).FirstOrDefault();

                    foreach (var item in Temp_Report)
                    {

                        if (item.Category == "1")
                            objSampleReportCount.GreenArrowCount = item.Products.Count();
                        else if (item.Category == "2")
                            objSampleReportCount.YellowArrowCount = item.Products.Count();
                        else if (item.Category == "3")
                            objSampleReportCount.RedArrowCount = item.Products.Count();
                        else if (item.Category == "4")
                            objSampleReportCount.DoubleRedArrowCount = item.Products.Count();
                        else if (item.Category == "5")
                            objSampleReportCount.YellowWarningCount = item.Products.Count();
                        else if (item.Category == "6")
                            objSampleReportCount.RedWarningCount = item.Products.Count();
                        else if (item.Category == "7")
                            objSampleReportCount.No_inspection = item.Products.Count();
                        else if (item.Category == "8")
                            objSampleReportCount.Not_accessibility = item.Products.Count();
                    }
                    objSampleReportCount.BlueIconCount = (from IR in context.InspectionReport
                                                          join PL in context.tblPhotoLibrary on IR.ID equals PL.InspectionReportID
                                                          where IR.InspectionOrderId == inspectionorderid && PL.IsApproved == true
                                                          select new Report
                                                          {
                                                              OrderID = PL.ID
                                                          }).Count();
                    int[] template = { 31, 37 };
                    IEnumerable<emailtemplate> objListOfEmailTemplates = from emailtemplate in context.EmailTemplates
                                                                         where template.Contains(emailtemplate.TemplateId)
                                                                         select emailtemplate;
                    foreach (var item in objListOfEmailTemplates)
                    {
                        if (item.TemplateName == "OneTimePayment")
                        {
                            objSampleReportCount.OneTimePayment = (float)item.Rate;
                            objSampleReportCount.OneTimePaymentValidity = item.Validity;

                        }
                        else
                        {
                            objSampleReportCount.PurchasePlan = (float)item.Rate;
                            objSampleReportCount.PurchasePlanValidity = item.Validity;
                        }

                    }

                    #region Dropdowns


                    List<SelectListItem> month = new List<SelectListItem>();
                    SelectListItem objSelectListItemmonth = new SelectListItem();
                    for (int i = 1; i <= 12; i++)
                    {
                        SelectListItem s = new SelectListItem();
                        if (i < 10)
                        {
                            s.Text = "0" + i.ToString();
                            s.Value = "0" + i.ToString();
                        }
                        else
                        {
                            s.Text = i.ToString();
                            s.Value = i.ToString();
                        }

                        month.Add(s);
                    }
                    ViewBag.Month = new SelectList(month, "Value", "Text", null);


                    List<SelectListItem> year = new List<SelectListItem>();
                    SelectListItem objSelectListItemyear = new SelectListItem();
                    for (int i = 2013; i <= 2022; i++)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = i.ToString();
                        s.Value = i.ToString();
                        year.Add(s);
                    }

                    ViewBag.Year = new SelectList(year, "Value", "Text", null);


                    #endregion
                    #region Bind States
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "--Select--";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    ViewBag.StateList = items;
                    ViewBag.ZipList = objZipCodeList;
                    ViewBag.CityList = objCityList;

                    #endregion
                    objSampleReportCount.SingleReportPrice = 45.99F;
                    ViewBag.Count = Temp_Report.Count();
                    if (objAddress != null)
                    {
                        objSampleReportCount.Address = objAddress;
                    }
                    return View(objSampleReportCount);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        #endregion
        public ActionResult SampleReport(string inspectionOrderId)
        {
            try
            {
                // inspectionOrderId = "7a2c9f8c-6099-49e3-97c5-b6c6a4d36007";

                //if (Sessions.LoggedInUser != null)
                {
                    ViewBag.IsCheckReportViewPermission = true;

                    List<SubSections> lstSubSections = new List<SubSections>();

                    var TermsAndConditions = from k in context.EmailTemplates
                                             where k.TemplateId == 26
                                             select k.TemplateDiscription;

                    ViewBag.Message = TermsAndConditions.FirstOrDefault();

                    #region SubSection

                    lstSubSections = (from IR in context.InspectionReport
                                      join SS in context.SubSections on IR.SubsectionId equals SS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new SubSections
                                      {
                                          SectionID = IR.SectionId,
                                          //SectionName = MS.SectionName,
                                          SubSectionID = IR.SubsectionId,
                                          SubSectionName = SS.SubSectionName,
                                          SortOrder = SS.SortOrder,
                                          CheckBoxID = IR.CheckboxID
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion


                    #region MainSection
                    List<MainSections> lstMainSection = new List<MainSections>();

                    lstMainSection = (from IR in context.InspectionReport
                                      join MS in context.MainSections on IR.SectionId equals MS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new MainSections
                                      {
                                          SectionID = MS.ID,
                                          SectionName = MS.SectionName,
                                          SortOrder = MS.SortOrder
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion
                    /*Check whether Inspection Rating is already done for this report--Start*/
                    #region Is InspectionDone
                    var IsInspectionDone = from Ratings in context.InspectorRatings
                                           where Ratings.InspectionID == inspectionOrderId
                                           select Ratings.RatingID;
                    if (IsInspectionDone.Any())
                    {
                        ViewBag.InspectionDone = "InpectionAlreadyDone";
                    }
                    else
                    {
                        ViewBag.InspectionDone = null;
                    }

                    #endregion
                    /*End*/

                    /*Get Details Report--Start*/

                    #region Get Detail Report
                    DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                    ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
                    List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
                    ViewBag.InspectionOrderId = inspectionOrderId;
                    objviewPreviousInspection = (from detail in context.tblinspectionorderdetail
                                                 join IO in context.tblinspectionorder on detail.InspectionorderdetailsID equals IO.InspectionOrderDetailId
                                                 join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                                 join ID in context.InspectorDetail on UI.ID equals ID.UserID
                                                 join PD in context.tblpropertydescription on detail.PropertyDescription equals PD.PropertyDescriptionId
                                                 join AI in context.InspectionAdditionalInfo on detail.InspectionorderdetailsID equals AI.InspectionOrderID into g
                                                 join agent in context.AgentLibraries on detail.InspectionorderdetailsID equals agent.InspectionOrderID
                                                 where detail.InspectionorderdetailsID == inspectionOrderId && agent.AgentType == "1" && IO.IsDeleted == false
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = inspectionOrderId,
                                                     PropertyAddress = detail.PropertyAddress,
                                                     RequestedBy = detail.RequesterFName + " " + detail.RequesterLName,
                                                     Agent = agent.AgentFirstName + agent.AgentLastName,
                                                     AgentEmail = agent.AgentEmailAddress,
                                                     AgentPhone = agent.AgentPhoneDay + " " + agent.AgentPhoneDay1,
                                                     ScheduleInspectionDate = IO.ScheduledDate,
                                                     HomeAge = detail.RoofAge,
                                                     SqureFeet = detail.SqureFootage,
                                                     ReportNumber = IO.ReportNo,
                                                     Bath = detail.Bathrooms,
                                                     Bed = detail.Bedrooms,
                                                     PropertyDescription = PD.Type,
                                                     Units = detail.Units,
                                                     Utilities = detail.EstimatedMonthlyUtilities,
                                                     RoofAge = detail.RoofAge,
                                                     Direction = detail.Directions,
                                                     AdditionAltration = detail.Additions_Alterations,
                                                     InspectorFirstName = UI.FirstName,
                                                     InspectorLastName = UI.LastName,
                                                     InspectorStreetAddress = UI.StreetAddress,
                                                     ExperienceTraining1 = ID.ExperienceTraining1,
                                                     ExperienceTraining2 = ID.ExperienceTraining2,
                                                     ExperienceTraining3 = ID.ExperienceTraining3,
                                                     Certification1 = ID.Certification1,
                                                     Certification2 = ID.Certification2,
                                                     Certification3 = ID.Certification3,
                                                     PhotoURL = ID.PhotoURL,
                                                     Email = UI.EmailAddress,
                                                     InspectorCity = UI.City,
                                                     InspectorState = UI.State,
                                                     InspectorZip = UI.ZipCode,
                                                     InspectorCountry = UI.Country,
                                                     InspectorPhone = UI.PhoneNo,
                                                     InspectorAddress1 = UI.Address,
                                                     InspectorAddress2 = UI.AddressLine2,
                                                     MainHomePage = g
                                                 }).FirstOrDefault();



                    /*End*/
                    #endregion


                    if (objviewPreviousInspection != null)
                    {
                        objviewPreviousInspection.MainSectionList = lstMainSection;
                        objviewPreviousInspection.SubSectionsList = lstSubSections;
                    }
                    objDetailsReportCommonPoco.objViewPreviousInspection = objviewPreviousInspection;
                    return View(objDetailsReportCommonPoco);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult SampleReport1(string inspectionOrderId, string PaymentType)
        {
            try
            {
                DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
                List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
                List<DetailsReportCommonPoco> objUpdatableReport = new List<DetailsReportCommonPoco>();
                int count = 0;

                objReport = (from rep in context.InspectionReport
                             join Msection in context.MainSections on rep.SectionId equals Msection.ID
                             join SSection in context.SubSections on rep.SubsectionId equals SSection.ID
                             join direction in context.DirectionDropMaster on rep.DirectionDropID equals direction.DirectionDropID into order
                             where rep.InspectionOrderId == inspectionOrderId
                             select new DetailsReportCommonPoco
                             {
                                 ID = rep.ItemID,
                                 InspectionReportId = rep.ID,
                                 SectionID = rep.SectionId,
                                 SubSectionID = rep.SubsectionId,
                                 SectionName = Msection.SectionName,
                                 SubSectionName = SSection.SubSectionName,
                                 InspectionOrderID = rep.InspectionOrderId,
                                 CheckboxID = rep.CheckboxID,
                                 IsUpdated = rep.IsUpdated,
                                 IsInspectorReviewed = rep.IsInspectorReviewed,
                                 Type = rep.Type,
                                 DirectionDropID = rep.DirectionDropID,
                                 //  Direction = direction.DirectionDropValue,
                                 DirectionDropMaster = order,
                                 PresetComments = rep.PresetComments,
                                 Comments = rep.Comments,
                                 Notice = rep.Notice,
                                 MSortOrder = Msection.SortOrder,
                                 SSortOrder = SSection.SortOrder,
                                 IsUpdateSection = false,
                                 PL = (from p in context.PhotoLibraries
                                       where p.InpectionReportFID == rep.ItemID
                                       select new ImagesNote
                                       {
                                           ImageUrl = p.PhotoUrl,
                                           ImageNote = p.Notes
                                       }),
                                 UpdateImages = (from k in context.tblPhotoLibrary
                                                 where k.InspectionReportID == rep.ID && k.IsUpdated == true
                                                 select new UpdatedSection
                                                 {
                                                     PhotoURL1 = k.PhotoURL1,
                                                     PhotoURL2 = k.PhotoURL2,
                                                     PhotoURL3 = k.PhotoURL3,
                                                     PhotoURL4 = k.PhotoURL4,
                                                     IsApproved = k.IsApproved,
                                                     SellerComments = k.SellerComment,
                                                     InspectorComments = k.InspectorComment,
                                                     InspectionReportID = k.InspectionReportID,
                                                     CreatedDate = k.CreatedDate,
                                                     ID = k.ID
                                                 }
                                               ).OrderBy(x => x.CreatedDate)

                             }).OrderBy(x => x.MSortOrder).ThenBy(z => z.SSortOrder).ToList();


                #region Get Report Count
                SampleReportCount objSampleReportCount = new SampleReportCount();
                var Temp_Report = from IR in context.InspectionReport
                                  where IR.InspectionOrderId == inspectionOrderId
                                  group IR by IR.CheckboxID into g
                                  select new
                                  {
                                      Category = g.Key,
                                      Products = g
                                  };

                foreach (var item in Temp_Report)
                {

                    if (item.Category == "1")
                        objSampleReportCount.GreenArrowCount = item.Products.Count();
                    else if (item.Category == "2")
                        objSampleReportCount.YellowArrowCount = item.Products.Count();
                    else if (item.Category == "3")
                        objSampleReportCount.RedArrowCount = item.Products.Count();
                    else if (item.Category == "4")
                        objSampleReportCount.DoubleRedArrowCount = item.Products.Count();
                    else if (item.Category == "5")
                        objSampleReportCount.YellowWarningCount = item.Products.Count();
                    else if (item.Category == "6")
                        objSampleReportCount.RedWarningCount = item.Products.Count();
                    else if (item.Category == "7")
                        objSampleReportCount.No_inspection = item.Products.Count();
                    else if (item.Category == "8")
                        objSampleReportCount.Not_accessibility = item.Products.Count();
                }
                objSampleReportCount.BlueIconCount = (from IR in context.InspectionReport
                                                      join PL in context.tblPhotoLibrary on IR.ID equals PL.InspectionReportID
                                                      where IR.InspectionOrderId == inspectionOrderId && PL.IsApproved == true
                                                      select new Report
                                                      {
                                                          OrderID = PL.ID
                                                      }).Count();

                #endregion


                if (objReport != null)
                {
                    objDetailsReportCommonPoco.ReportsDetail = objReport;
                    //  objSampleReportCount.BlueIconCount = count;
                    objDetailsReportCommonPoco.ReportCount = objSampleReportCount;
                }

                /*Save Data in Rport Viewed table--Start*/


                _messageToShow.Result = RenderRazorViewToString("_SampleReport", objDetailsReportCommonPoco);
                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult SampleBasicReport(string UniqueId)
        {
            try
            {

                if (UniqueId != null && UniqueId != string.Empty)
                {
                    string inspectionorderid = context.tblinspectionorder.Where(x => x.ReportNo == UniqueId).FirstOrDefault().InspectionOrderDetailId;



                    var Commnet = (from IR in context.InspectionReport
                                   where IR.InspectionOrderId == inspectionorderid && IR.Comments != null && IR.Comments != string.Empty
                                   select new
                                   {
                                       ID = IR.ID
                                   }).Count();

                    var PCommnet = (from IR in context.InspectionReport
                                    where IR.InspectionOrderId == inspectionorderid && IR.PresetComments != null && IR.PresetComments != string.Empty
                                    select new
                                    {
                                        ID = IR.ID
                                    }).Count();

                    var Photo = (from p in context.PhotoLibraries
                                 where p.InspectionOrderID == inspectionorderid
                                 select new
                                 {
                                     ID = p.ID
                                 }).Count();
                    ViewBag.Photo = Convert.ToInt32(Photo);
                    ViewBag.Comments = Convert.ToInt32(Commnet) + Convert.ToInt32(PCommnet);
                    SampleReportCount objSampleReportCount = new SampleReportCount();
                    var Temp_Report = from IR in context.InspectionReport
                                      join IO in context.tblinspectionorder on IR.InspectionOrderId equals IO.InspectionOrderDetailId
                                      where IO.ReportNo == UniqueId && IO.InspectionStatus == "Completed"
                                      group IR by IR.CheckboxID into g
                                      select new
                                      {
                                          Category = g.Key,
                                          Products = g,


                                      };

                    ReportPropertyAddress objAddress = new ReportPropertyAddress();
                    objAddress = (from IO in context.tblinspectionorder
                                  join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                  join states in context.tblstate on IOD.PropertyState equals states.StateId
                                  join City in context.tblcity on IOD.PropertyCity equals City.CityId
                                  join Zip in context.tblzip on IOD.PropertyZip equals Zip.ZipId
                                  where IO.ReportNo == UniqueId && IO.InspectionStatus == "Completed" && IO.IsDeleted == false
                                  select new ReportPropertyAddress
                                  {
                                      HouseNo = IOD.PropertyAddress,
                                      PropertyCity = City.CityName,
                                      PropertyState = states.StateName,
                                      PropertyZip = Zip.Zip,
                                  }).FirstOrDefault();

                    foreach (var item in Temp_Report)
                    {

                        if (item.Category == "1")
                            objSampleReportCount.GreenArrowCount = item.Products.Count();
                        else if (item.Category == "2")
                            objSampleReportCount.YellowArrowCount = item.Products.Count();
                        else if (item.Category == "3")
                            objSampleReportCount.RedArrowCount = item.Products.Count();
                        else if (item.Category == "4")
                            objSampleReportCount.DoubleRedArrowCount = item.Products.Count();
                        else if (item.Category == "5")
                            objSampleReportCount.YellowWarningCount = item.Products.Count();
                        else if (item.Category == "6")
                            objSampleReportCount.RedWarningCount = item.Products.Count();
                        else if (item.Category == "7")
                            objSampleReportCount.No_inspection = item.Products.Count();
                        else if (item.Category == "8")
                            objSampleReportCount.Not_accessibility = item.Products.Count();
                    }
                    int[] template = { 31, 37 };
                    IEnumerable<emailtemplate> objListOfEmailTemplates = from emailtemplate in context.EmailTemplates
                                                                         where template.Contains(emailtemplate.TemplateId)
                                                                         select emailtemplate;
                    foreach (var item in objListOfEmailTemplates)
                    {
                        if (item.TemplateName == "OneTimePayment")
                        {
                            objSampleReportCount.OneTimePayment = (float)item.Rate;
                            objSampleReportCount.OneTimePaymentValidity = item.Validity;
                        }
                        else
                        {
                            objSampleReportCount.PurchasePlan = (float)item.Rate;
                            objSampleReportCount.PurchasePlanValidity = item.Validity;
                        }

                    }

                    #region Dropdowns


                    List<SelectListItem> month = new List<SelectListItem>();
                    SelectListItem objSelectListItemmonth = new SelectListItem();
                    for (int i = 1; i <= 12; i++)
                    {
                        SelectListItem s = new SelectListItem();
                        if (i < 10)
                        {
                            s.Text = "0" + i.ToString();
                            s.Value = "0" + i.ToString();
                        }
                        else
                        {
                            s.Text = i.ToString();
                            s.Value = i.ToString();
                        }

                        month.Add(s);
                    }
                    ViewBag.Month = new SelectList(month, "Value", "Text", null);


                    List<SelectListItem> year = new List<SelectListItem>();
                    SelectListItem objSelectListItemyear = new SelectListItem();
                    for (int i = 2013; i <= 2022; i++)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = i.ToString();
                        s.Value = i.ToString();
                        year.Add(s);
                    }

                    ViewBag.Year = new SelectList(year, "Value", "Text", null);


                    #endregion
                    #region Bind States
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "--Select--";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    ViewBag.StateList = items;
                    ViewBag.ZipList = objZipCodeList;
                    ViewBag.CityList = objCityList;

                    #endregion
                    objSampleReportCount.SingleReportPrice = 45.99F;
                    ViewBag.Count = Temp_Report.Count();
                    if (objAddress != null)
                    {
                        objSampleReportCount.Address = objAddress;
                    }
                    return View(objSampleReportCount);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ViewComprehensiveReport(string inspectionOrderId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "SiteUser")
                {
                    if (Sessions.LoggedInUser != null && context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == inspectionOrderId).FirstOrDefault().RequesterID == Sessions.LoggedInUser.UserId)
                    {
                        ViewBag.IsCheckReportViewPermission = true;
                    }

                    else if (Sessions.LoggedInUser.PaymentPlan == "OneTimePaymentPlan")
                    {
                        if (!CheckUserOneTimePaymentStatus(inspectionOrderId))
                        {
                            ViewBag.IsCheckReportViewPermission = false;
                            ViewBag.ReportId = inspectionOrderId;
                            return View();
                        }
                        else
                        {
                            ViewBag.IsCheckReportViewPermission = true;
                        }
                    }
                    else
                        if (Sessions.LoggedInUser.PaymentPlan == "PaymentPlan")
                        {
                            ViewBag.IsCheckReportViewPermission = true;
                        }

                        else
                            if (Sessions.LoggedInUser.PaymentPlan == "Free")
                            {

                                ViewBag.IsCheckReportViewPermission = false;
                                return View();
                            }

                }
                else if (Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    ViewBag.IsCheckReportViewPermission = true;
                }
                if (Sessions.LoggedInUser != null)
                {

                    if (Sessions.LoggedInUser.RoleName == "SiteUser")
                    {



                        string ipaddress = "";
                        ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                        if (ipaddress == "" || ipaddress == null)
                            ipaddress = Request.ServerVariables["REMOTE_ADDR"];

                        reportviewed objreport;
                        objreport = context.ReportViewed.Where(x => x.ReportID == inspectionOrderId && x.UserID == Sessions.LoggedInUser.UserId).FirstOrDefault();
                        if (objreport != null)
                        {
                            objreport.ModifedDate = DateTime.Now;
                        }
                        else
                        {
                            objreport = new reportviewed();
                            objreport.ID = Guid.NewGuid().ToString();
                            objreport.ReportID = inspectionOrderId;
                            objreport.UserID = Sessions.LoggedInUser.UserId;
                            objreport.MachineIP = ipaddress;
                            objreport.CreatedDate = System.DateTime.Now;
                            objreport.IsPurchased = false;
                            context.ReportViewed.Add(objreport);

                        }
                        context.SaveChanges();
                    }


                    List<SubSections> lstSubSections = new List<SubSections>();

                    var TermsAndConditions = from k in context.EmailTemplates
                                             where k.TemplateId == 26
                                             select k.TemplateDiscription;

                    ViewBag.Message = TermsAndConditions.FirstOrDefault();

                    #region SubSection

                    lstSubSections = (from IR in context.InspectionReport
                                      join SS in context.SubSections on IR.SubsectionId equals SS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new SubSections
                                      {
                                          SectionID = IR.SectionId,
                                          //SectionName = MS.SectionName,
                                          SubSectionID = IR.SubsectionId,
                                          SubSectionName = SS.SubSectionName,
                                          SortOrder = SS.SortOrder,
                                          CheckBoxID = IR.CheckboxID
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion


                    #region MainSection
                    List<MainSections> lstMainSection = new List<MainSections>();

                    lstMainSection = (from IR in context.InspectionReport
                                      join MS in context.MainSections on IR.SectionId equals MS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new MainSections
                                      {
                                          SectionID = MS.ID,
                                          SectionName = MS.SectionName,
                                          SortOrder = MS.SortOrder
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion
                    /*Check whether Inspection Rating is already done for this report--Start*/
                    #region Is InspectionDone
                    var IsInspectionDone = from Ratings in context.InspectorRatings
                                           where Ratings.InspectionID == inspectionOrderId
                                           select Ratings.RatingID;
                    if (IsInspectionDone.Any())
                    {
                        ViewBag.InspectionDone = "InpectionAlreadyDone";
                    }
                    else
                    {
                        ViewBag.InspectionDone = null;
                    }

                    #endregion
                    /*End*/

                    /*Get Details Report--Start*/

                    #region Get Detail Report
                    DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                    ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
                    List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
                    ViewBag.InspectionOrderId = inspectionOrderId;
                    objviewPreviousInspection = (from detail in context.tblinspectionorderdetail
                                                 join IO in context.tblinspectionorder on detail.InspectionorderdetailsID equals IO.InspectionOrderDetailId
                                                 join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                                 join ID in context.InspectorDetail on UI.ID equals ID.UserID
                                                 join PD in context.tblpropertydescription on detail.PropertyDescription equals PD.PropertyDescriptionId into p
                                                 join AI in context.InspectionAdditionalInfo on detail.InspectionorderdetailsID equals AI.InspectionOrderID into g
                                                 join agent in context.AgentLibraries on detail.InspectionorderdetailsID equals agent.InspectionOrderID
                                                 where detail.InspectionorderdetailsID == inspectionOrderId && agent.AgentType == "1" && IO.IsDeleted == false
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = inspectionOrderId,
                                                     PropertyAddress = detail.PropertyAddress,
                                                     RequestedBy = detail.RequesterFName + " " + detail.RequesterLName,
                                                     Agent = agent.AgentFirstName + agent.AgentLastName,
                                                     AgentEmail = agent.AgentEmailAddress,
                                                     AgentPhone = agent.AgentPhoneDay + " " + agent.AgentPhoneDay1,
                                                     ScheduleInspectionDate = IO.ScheduledDate,
                                                     HomeAge = detail.RoofAge,
                                                     SqureFeet = detail.SqureFootage,
                                                     ReportNumber = IO.ReportNo,
                                                     Bath = detail.Bathrooms,
                                                     Bed = detail.Bedrooms,
                                                     PDescription = p,
                                                     // PropertyDescription = PD.Type,
                                                     Units = detail.Units,
                                                     Utilities = detail.EstimatedMonthlyUtilities,
                                                     RoofAge = detail.RoofAge,
                                                     Direction = detail.Directions,
                                                     AdditionAltration = detail.Additions_Alterations,
                                                     InspectorFirstName = UI.FirstName,
                                                     InspectorLastName = UI.LastName,
                                                     InspectorStreetAddress = UI.StreetAddress,
                                                     ExperienceTraining1 = ID.ExperienceTraining1,
                                                     ExperienceTraining2 = ID.ExperienceTraining2,
                                                     ExperienceTraining3 = ID.ExperienceTraining3,
                                                     Certification1 = ID.Certification1,
                                                     Certification2 = ID.Certification2,
                                                     Certification3 = ID.Certification3,
                                                     PhotoURL = ID.PhotoURL,
                                                     Email = UI.EmailAddress,
                                                     InspectorCity = UI.City,
                                                     InspectorState = UI.State,
                                                     InspectorZip = UI.ZipCode,
                                                     InspectorCountry = UI.Country,
                                                     InspectorPhone = UI.PhoneNo,
                                                     InspectorAddress1 = UI.Address,
                                                     InspectorAddress2 = UI.AddressLine2,
                                                     MainHomePage = g
                                                 }).FirstOrDefault();



                    /*End*/
                    #endregion



                    if (objviewPreviousInspection != null)
                    {
                        objviewPreviousInspection.MainSectionList = lstMainSection;
                        objviewPreviousInspection.SubSectionsList = lstSubSections;

                    }
                    objDetailsReportCommonPoco.objViewPreviousInspection = objviewPreviousInspection;

                    #region Get Report Count
                    SampleReportCount objSampleReportCount = new SampleReportCount();
                    var Temp_Report = from IR in context.InspectionReport
                                      where IR.InspectionOrderId == inspectionOrderId
                                      group IR by IR.CheckboxID into g
                                      select new
                                      {
                                          Category = g.Key,
                                          Products = g
                                      };

                    foreach (var item in Temp_Report)
                    {

                        if (item.Category == "1")
                            objSampleReportCount.GreenArrowCount = item.Products.Count();
                        else if (item.Category == "2")
                            objSampleReportCount.YellowArrowCount = item.Products.Count();
                        else if (item.Category == "3")
                            objSampleReportCount.RedArrowCount = item.Products.Count();
                        else if (item.Category == "4")
                            objSampleReportCount.DoubleRedArrowCount = item.Products.Count();
                        else if (item.Category == "5")
                            objSampleReportCount.YellowWarningCount = item.Products.Count();
                        else if (item.Category == "6")
                            objSampleReportCount.RedWarningCount = item.Products.Count();
                        else if (item.Category == "7")
                            objSampleReportCount.No_inspection = item.Products.Count();
                        else if (item.Category == "8")
                            objSampleReportCount.Not_accessibility = item.Products.Count();
                    }
                    objSampleReportCount.BlueIconCount = (from IR in context.InspectionReport
                                                          join PL in context.tblPhotoLibrary on IR.ID equals PL.InspectionReportID
                                                          where IR.InspectionOrderId == inspectionOrderId && PL.IsApproved == true
                                                          select new Report
                                                          {
                                                              OrderID = PL.ID
                                                          }).Count();
                    #endregion
                    objDetailsReportCommonPoco.ReportCount = objSampleReportCount;
                    return View(objDetailsReportCommonPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult AdminComprehensiveReport(string inspectionOrderId)
        {
            try
            {

                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Admin")
                {


                    List<SubSections> lstSubSections = new List<SubSections>();

                    var TermsAndConditions = from k in context.EmailTemplates
                                             where k.TemplateId == 26
                                             select k.TemplateDiscription;

                    ViewBag.Message = TermsAndConditions.FirstOrDefault();

                    #region SubSection

                    lstSubSections = (from IR in context.InspectionReport
                                      join SS in context.SubSections on IR.SubsectionId equals SS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new SubSections
                                      {
                                          SectionID = IR.SectionId,
                                          //SectionName = MS.SectionName,
                                          SubSectionID = IR.SubsectionId,
                                          SubSectionName = SS.SubSectionName,
                                          SortOrder = SS.SortOrder,
                                          CheckBoxID = IR.CheckboxID
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion


                    #region MainSection
                    List<MainSections> lstMainSection = new List<MainSections>();

                    lstMainSection = (from IR in context.InspectionReport
                                      join MS in context.MainSections on IR.SectionId equals MS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new MainSections
                                      {
                                          SectionID = MS.ID,
                                          SectionName = MS.SectionName,
                                          SortOrder = MS.SortOrder
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion
                    /*Check whether Inspection Rating is already done for this report--Start*/
                    #region Is InspectionDone
                    var IsInspectionDone = from Ratings in context.InspectorRatings
                                           where Ratings.InspectionID == inspectionOrderId
                                           select Ratings.RatingID;
                    if (IsInspectionDone.Any())
                    {
                        ViewBag.InspectionDone = "InpectionAlreadyDone";
                    }
                    else
                    {
                        ViewBag.InspectionDone = null;
                    }

                    #endregion
                    /*End*/

                    /*Get Details Report--Start*/

                    #region Get Detail Report
                    DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                    ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
                    List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
                    ViewBag.InspectionOrderId = inspectionOrderId;
                    objviewPreviousInspection = (from detail in context.tblinspectionorderdetail
                                                 join IO in context.tblinspectionorder on detail.InspectionorderdetailsID equals IO.InspectionOrderDetailId
                                                 join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                                 join ID in context.InspectorDetail on UI.ID equals ID.UserID
                                                 join AI in context.InspectionAdditionalInfo on detail.InspectionorderdetailsID equals AI.InspectionOrderID into g
                                                 join agent in context.AgentLibraries on detail.InspectionorderdetailsID equals agent.InspectionOrderID
                                                 where detail.InspectionorderdetailsID == inspectionOrderId && agent.AgentType == "1" && IO.IsDeleted == false
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = inspectionOrderId,
                                                     PropertyAddress = detail.PropertyAddress,
                                                     RequestedBy = detail.RequesterFName + " " + detail.RequesterLName,
                                                     Agent = agent.AgentFirstName + agent.AgentLastName,
                                                     AgentEmail = agent.AgentEmailAddress,
                                                     AgentPhone = agent.AgentPhoneDay + " " + agent.AgentPhoneDay1,
                                                     ScheduleInspectionDate = IO.ScheduledDate,
                                                     HomeAge = detail.RoofAge,
                                                     SqureFeet = detail.SqureFootage,
                                                     ReportNumber = IO.ReportNo,
                                                     Bath = detail.Bathrooms,
                                                     Bed = detail.Bedrooms,
                                                     RoofAge = detail.RoofAge,
                                                     Direction = detail.Directions,
                                                     AdditionAltration = detail.Additions_Alterations,
                                                     InspectorFirstName = UI.FirstName,
                                                     InspectorLastName = UI.LastName,
                                                     InspectorStreetAddress = UI.StreetAddress,
                                                     ExperienceTraining1 = ID.ExperienceTraining1,
                                                     ExperienceTraining2 = ID.ExperienceTraining2,
                                                     ExperienceTraining3 = ID.ExperienceTraining3,
                                                     Certification1 = ID.Certification1,
                                                     Certification2 = ID.Certification2,
                                                     Certification3 = ID.Certification3,
                                                     PhotoURL = ID.PhotoURL,
                                                     Email = UI.EmailAddress,
                                                     InspectorCity = UI.City,
                                                     InspectorState = UI.State,
                                                     InspectorZip = UI.ZipCode,
                                                     InspectorCountry = UI.Country,
                                                     InspectorPhone = UI.PhoneNo,
                                                     InspectorAddress1 = UI.Address,
                                                     InspectorAddress2 = UI.AddressLine2,
                                                     MainHomePage = g
                                                 }).FirstOrDefault();



                    /*End*/
                    #endregion


                    if (objviewPreviousInspection != null)
                    {
                        objviewPreviousInspection.MainSectionList = lstMainSection;
                        objviewPreviousInspection.SubSectionsList = lstSubSections;

                    }
                    objDetailsReportCommonPoco.objViewPreviousInspection = objviewPreviousInspection;
                    return View(objDetailsReportCommonPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public JsonResult ViewDetailReport1(string inspectionOrderId, string PaymentType)
        {
            try
            {
                DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
                List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
                List<DetailsReportCommonPoco> objUpdatableReport = new List<DetailsReportCommonPoco>();



                objReport = (from rep in context.InspectionReport
                             join Msection in context.MainSections on rep.SectionId equals Msection.ID
                             join SSection in context.SubSections on rep.SubsectionId equals SSection.ID
                             join direction in context.DirectionDropMaster on rep.DirectionDropID equals direction.DirectionDropID into order
                             where rep.InspectionOrderId == inspectionOrderId
                             select new DetailsReportCommonPoco
                             {
                                 ID = rep.ItemID,
                                 InspectionReportId = rep.ID,
                                 SectionID = rep.SectionId,
                                 SubSectionID = rep.SubsectionId,
                                 SectionName = Msection.SectionName,
                                 SubSectionName = SSection.SubSectionName,
                                 InspectionOrderID = rep.InspectionOrderId,
                                 CheckboxID = rep.CheckboxID,
                                 IsUpdated = rep.IsUpdated,
                                 IsInspectorReviewed = rep.IsInspectorReviewed,
                                 Type = rep.Type,
                                 DirectionDropID = rep.DirectionDropID,
                                 DirectionDropMaster = order,
                                 PresetComments = rep.PresetComments,
                                 Comments = rep.Comments,
                                 Notice = rep.Notice,
                                 MSortOrder = Msection.SortOrder,
                                 SSortOrder = SSection.SortOrder,
                                 IsUpdateSection = false,
                                 PL = (from p in context.PhotoLibraries
                                       where p.InpectionReportFID == rep.ItemID
                                       select new ImagesNote
                                       {
                                           ImageUrl = p.PhotoUrl,
                                           ImageNote = p.Notes
                                       }),
                                 UpdateImages = (from k in context.tblPhotoLibrary
                                                 where k.InspectionReportID == rep.ID && k.IsUpdated == true && k.IsApproved == true
                                                 select new UpdatedSection
                                                 {
                                                     PhotoURL1 = k.PhotoURL1,
                                                     PhotoURL2 = k.PhotoURL2,
                                                     PhotoURL3 = k.PhotoURL3,
                                                     PhotoURL4 = k.PhotoURL4,
                                                     IsApproved = k.IsApproved,
                                                     SellerComments = k.SellerComment,
                                                     InspectorComments = k.InspectorComment,
                                                     InspectionReportID = k.InspectionReportID,
                                                     CreatedDate = k.CreatedDate,
                                                     ID = k.ID
                                                 }
                                         ).OrderBy(x => x.CreatedDate)
                             }).OrderBy(x => x.MSortOrder).ThenBy(z => z.SSortOrder).ToList();





                //if (objReport != null)
                //{
                //    for (var i = objReport.Count - 1; i >= 0; i--)
                //    {
                //        string ReportID = objReport[i].InspectionOrderID;
                //        string secID = objReport[i].SectionID;
                //        string subsecID = objReport[i].SubSectionID;

                //        var tempdata = context.tblisincluded.FirstOrDefault(x => x.ReportId == ReportID && x.SectionId == secID && x.SubSectionId == subsecID);
                //        if (tempdata != null)
                //        {
                //            bool? CheckInclude = tempdata.IsIncluded;
                //            objReport.Where(x => x.InspectionOrderID == ReportID && x.SectionID == secID && x.SubSectionID == subsecID).FirstOrDefault().IsIncluded = CheckInclude;
                //        }
                //    }
                //}
                foreach (var item in objReport)
                {
                    string ReportID = item.InspectionOrderID;
                    string secID = item.SectionID;
                    string subsecID = item.SubSectionID;

                    var tempdata = context.tblisincluded.FirstOrDefault(x => x.ReportId == ReportID && x.SectionId == secID && x.SubSectionId == subsecID);
                    if (tempdata != null)
                    {
                        bool? CheckInclude = tempdata.IsIncluded;
                        if ((bool)CheckInclude)
                            item.IsIncluded = true;
                        else if (!(bool)CheckInclude)
                            item.IsIncluded = false;
                    }
                }

                objDetailsReportCommonPoco.ReportsDetail = objReport;
                /*Save Data in Rport Viewed table--Start*/
                if (Sessions.LoggedInUser.RoleName == "Home Seller" || Sessions.LoggedInUser.RoleName == "Home Buyer")
                {
                    string PaymentId = string.Empty;
                    if (PaymentType == "PaymentPlan")
                    {
                        if (Sessions.LoggedInUser.RoleName == "Home Seller")
                            PaymentId = context.Payments.OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.UserID == Sessions.LoggedInUser.UserId && x.ReportID == inspectionOrderId).PaymentID;
                        else
                            PaymentId = context.Payments.OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.UserID == Sessions.LoggedInUser.UserId).PaymentID;
                    }

                    string ipaddress = "";
                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (ipaddress == "" || ipaddress == null)
                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];


                    var Inspectionid = context.InspectionOrders.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId).InspectionId;
                    reportviewed objReportViewed = new reportviewed();
                    objReportViewed.ID = Guid.NewGuid().ToString();
                    objReportViewed.UserID = Sessions.LoggedInUser.UserId;
                    objReportViewed.ReportID = Inspectionid;
                    objReportViewed.CreatedDate = System.DateTime.Now;
                    objReportViewed.ModifedDate = System.DateTime.Now;
                    objReportViewed.MachineIP = PaymentType == "PaymentPlan" ? ipaddress : "";
                    objReportViewed.PaymentID = PaymentId;
                    objReportViewed.Viewed = true;
                    context.ReportViewed.Add(objReportViewed);
                    //    context.SaveChanges();
                    /*End*/
                }

                _messageToShow.Result = RenderRazorViewToString("_ViewComprehensiveReport", objDetailsReportCommonPoco);
                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //[HttpPost]

        //public JsonResult ShowDetailReportFromSp(string inspectionOrderId, string PaymentType)
        //{
        //    try
        //    {

        //        DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
        //        ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
        //        List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
        //        List<DetailsReportCommonPoco> objUpdatableReport = new List<DetailsReportCommonPoco>();
        //        int count = 0;
        //        string connStr = "Server=108.168.203.226;Database=db_spectromax;Uid=spectromax;Pwd=spectromax;";
        //        MySqlConnection conn = new MySqlConnection(connStr);
        //        conn.Open();
        //        MySqlCommand cmd = new MySqlCommand("GetReportData", conn);
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("InspectionOrderId", inspectionOrderId);
        //        MySqlDataAdapter da = new MySqlDataAdapter(cmd);
        //        DataSet ds = new DataSet();
        //        da.Fill(ds);
        //        var Report = new List<DetailsReportCommonPoco>();
        //        var Images = new List<ImagesNote>();
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
        //        {
        //            Report = ds.Tables[0].AsEnumerable().Select(datarow =>
        //                new DetailsReportCommonPoco
        //                {
        //                    ID = datarow["ItemID"].ToString(),
        //                    SectionID = datarow["SectionId"].ToString(),
        //                    SubSectionID = datarow["SubsectionId"].ToString(),
        //                    SectionName = datarow["SectionName"].ToString(),
        //                    SubSectionName = datarow["SubSectionName"].ToString(),
        //                    CheckboxID = datarow["CheckboxID"].ToString(),
        //                    Type = datarow["Type"].ToString(),
        //                    DirectionDropID = Convert.ToInt32(datarow["DirectionDropID"]),
        //                    Direction = datarow["DirectionDropValue"].ToString(),
        //                    PresetComments = datarow["PresetComments"].ToString(),
        //                    Comments = datarow["Comments"].ToString(),
        //                    Notice = datarow["Notice"].ToString(),
        //                }).ToList();

        //            //Images = ds.Tables[1].AsEnumerable().Select(datarow =>

        //            //    new ImagesNote
        //            //    {
        //            //        ImageUrl=datarow["PhotoUrl"].ToString(),
        //            //        ImageNote = datarow["Notes"].ToString(),
        //            //        InspectionOrderFId = datarow["InpectionReportFID"].ToString()
        //            //    }).ToList(); 





        //        }

        //        conn.Close();

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool CheckUserOneTimePaymentStatus(string reportId)
        {
            bool IsValid = false;
            var chkUserPayment = (from rv in context.ReportViewed where rv.UserID == Sessions.LoggedInUser.UserId && rv.ReportID == reportId select rv).FirstOrDefault();
            if (chkUserPayment != null)
            {
                IsValid = true;
            }
            return IsValid;
        }


        public ActionResult EditComprehensiveRepot(string inspectionOrderId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {

                    ViewBag.IsCheckReportViewPermission = true;




                    List<SubSections> lstSubSections = new List<SubSections>();

                    var TermsAndConditions = from k in context.EmailTemplates
                                             where k.TemplateId == 26
                                             select k.TemplateDiscription;

                    ViewBag.Message = TermsAndConditions.FirstOrDefault();

                    #region SubSection

                    lstSubSections = (from IR in context.InspectionReport
                                      join SS in context.SubSections on IR.SubsectionId equals SS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new SubSections
                                      {
                                          SectionID = IR.SectionId,
                                          //SectionName = MS.SectionName,
                                          SubSectionID = IR.SubsectionId,
                                          SubSectionName = SS.SubSectionName,
                                          SortOrder = SS.SortOrder,
                                          CheckBoxID = IR.CheckboxID
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion


                    #region MainSection
                    List<MainSections> lstMainSection = new List<MainSections>();

                    lstMainSection = (from IR in context.InspectionReport
                                      join MS in context.MainSections on IR.SectionId equals MS.ID
                                      where IR.InspectionOrderId == inspectionOrderId
                                      select new MainSections
                                      {
                                          SectionID = MS.ID,
                                          SectionName = MS.SectionName,
                                          SortOrder = MS.SortOrder
                                      }).Distinct().OrderBy(x => x.SortOrder).ToList();

                    #endregion
                    /*Check whether Inspection Rating is already done for this report--Start*/
                    #region Is InspectionDone
                    var IsInspectionDone = from Ratings in context.InspectorRatings
                                           where Ratings.InspectionID == inspectionOrderId
                                           select Ratings.RatingID;
                    if (IsInspectionDone.Any())
                    {
                        ViewBag.InspectionDone = "InpectionAlreadyDone";
                    }
                    else
                    {
                        ViewBag.InspectionDone = null;
                    }

                    #endregion
                    /*End*/

                    /*Get Details Report--Start*/

                    #region Get Detail Report
                    DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                    ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
                    List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
                    ViewBag.InspectionOrderId = inspectionOrderId;
                    objviewPreviousInspection = (from detail in context.tblinspectionorderdetail
                                                 join IO in context.tblinspectionorder on detail.InspectionorderdetailsID equals IO.InspectionOrderDetailId
                                                 join UI in context.UserInfoes on IO.InspectorID equals UI.ID
                                                 join ID in context.InspectorDetail on UI.ID equals ID.UserID
                                                 join PD in context.tblpropertydescription on detail.PropertyDescription equals PD.PropertyDescriptionId
                                                 join AI in context.InspectionAdditionalInfo on detail.InspectionorderdetailsID equals AI.InspectionOrderID into g
                                                 join agent in context.AgentLibraries on detail.InspectionorderdetailsID equals agent.InspectionOrderID
                                                 where detail.InspectionorderdetailsID == inspectionOrderId && agent.AgentType == "1" && IO.IsDeleted == false
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = inspectionOrderId,
                                                     PropertyAddress = detail.PropertyAddress,
                                                     RequestedBy = detail.RequesterFName + " " + detail.RequesterLName,
                                                     Agent = agent.AgentFirstName + agent.AgentLastName,
                                                     AgentEmail = agent.AgentEmailAddress,
                                                     AgentPhone = agent.AgentPhoneDay + " " + agent.AgentPhoneDay1,
                                                     ScheduleInspectionDate = IO.ScheduledDate,
                                                     HomeAge = detail.RoofAge,
                                                     SqureFeet = detail.SqureFootage,
                                                     ReportNumber = IO.ReportNo,
                                                     Bath = detail.Bathrooms,
                                                     Bed = detail.Bedrooms,
                                                     PropertyDescription = PD.Type,
                                                     Units = detail.Units,
                                                     Utilities = detail.EstimatedMonthlyUtilities,
                                                     RoofAge = detail.RoofAge,
                                                     Direction = detail.Directions,
                                                     AdditionAltration = detail.Additions_Alterations,
                                                     InspectorFirstName = UI.FirstName,
                                                     InspectorLastName = UI.LastName,
                                                     InspectorStreetAddress = UI.StreetAddress,
                                                     ExperienceTraining1 = ID.ExperienceTraining1,
                                                     ExperienceTraining2 = ID.ExperienceTraining2,
                                                     ExperienceTraining3 = ID.ExperienceTraining3,
                                                     Certification1 = ID.Certification1,
                                                     Certification2 = ID.Certification2,
                                                     Certification3 = ID.Certification3,
                                                     PhotoURL = ID.PhotoURL,
                                                     Email = UI.EmailAddress,
                                                     InspectorCity = UI.City,
                                                     InspectorState = UI.State,
                                                     InspectorZip = UI.ZipCode,
                                                     InspectorCountry = UI.Country,
                                                     InspectorPhone = UI.PhoneNo,
                                                     InspectorAddress1 = UI.Address,
                                                     InspectorAddress2 = UI.AddressLine2,
                                                     MainHomePage = g
                                                 }).FirstOrDefault();



                    /*End*/
                    #endregion


                    if (objviewPreviousInspection != null)
                    {
                        objviewPreviousInspection.MainSectionList = lstMainSection;
                        objviewPreviousInspection.SubSectionsList = lstSubSections;

                    }
                    objDetailsReportCommonPoco.objViewPreviousInspection = objviewPreviousInspection;
                    return View(objDetailsReportCommonPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public JsonResult ViewDetailReportEdit(string inspectionOrderId, string PaymentType)
        {
            try
            {
                DetailsReportCommonPoco objDetailsReportCommonPoco = new DetailsReportCommonPoco();
                ViewPreviousInspection objviewPreviousInspection = new ViewPreviousInspection();
                List<DetailsReportCommonPoco> objReport = new List<DetailsReportCommonPoco>();
                List<DetailsReportCommonPoco> objUpdatableReport = new List<DetailsReportCommonPoco>();
                int count = 0;
                //string connStr = "Server=108.168.203.226;Database=db_spectromax;Uid=spectromax;Pwd=spectromax;";
                //MySqlConnection conn = new MySqlConnection(connStr);
                // conn.Open();
                // MySqlCommand cmd = new MySqlCommand("GetReportData", conn);
                //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                ////cmd.Parameters.AddWithValue("EmpId", empId);
                ////cmd.Parameters.AddWithValue("FromDate", Convert.ToDateTime(startDate.ToString("yyyy-MM-dd")));
                ////cmd.Parameters.AddWithValue("ToDate", Convert.ToDateTime(endDate.ToString("yyyy-MM-dd")));

                //MySqlDataAdapter da = new MySqlDataAdapter(cmd);

                //DataSet ds = new DataSet();

                //da.Fill(ds);
                //var Report=new List<DetailsReportCommonPoco>();
                //var Images =new List <ImagesNote>();
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                //{
                //    Report = ds.Tables[0].AsEnumerable().Select(datarow =>
                //        new DetailsReportCommonPoco
                //        {
                //            ID = datarow["ItemID"].ToString(),
                //            SectionID = datarow["SectionId"].ToString(),
                //            SubSectionID = datarow["SubsectionId"].ToString(),
                //            SectionName = datarow["SectionName"].ToString(),
                //            SubSectionName = datarow["SubSectionName"].ToString(),
                //            CheckboxID = datarow["CheckboxID"].ToString(),
                //            Type = datarow["Type"].ToString(),
                //            DirectionDropID = Convert.ToInt32(datarow["DirectionDropID"]),
                //            Direction = datarow["DirectionDropValue"].ToString(),
                //            PresetComments = datarow["PresetComments"].ToString(),
                //            Comments = datarow["Comments"].ToString(),
                //            Notice = datarow["Notice"].ToString(),
                //        }).ToList();

                //    //Images = ds.Tables[1].AsEnumerable().Select(datarow =>

                //    //    new ImagesNote
                //    //    {
                //    //        ImageUrl=datarow["PhotoUrl"].ToString(),
                //    //        ImageNote = datarow["Notes"].ToString(),
                //    //        InspectionOrderFId = datarow["InpectionReportFID"].ToString()
                //    //    }).ToList(); 





                //}

                //conn.Close();

                objReport = (from rep in context.InspectionReport
                             join Msection in context.MainSections on rep.SectionId equals Msection.ID
                             join SSection in context.SubSections on rep.SubsectionId equals SSection.ID
                             join direction in context.DirectionDropMaster on rep.DirectionDropID equals direction.DirectionDropID into order
                             where rep.InspectionOrderId == inspectionOrderId
                             select new DetailsReportCommonPoco
                             {
                                 ID = rep.ItemID,
                                 InspectionReportId = rep.ID,
                                 SectionID = rep.SectionId,
                                 SubSectionID = rep.SubsectionId,
                                 SectionName = Msection.SectionName,
                                 SubSectionName = SSection.SubSectionName,
                                 InspectionOrderID = rep.InspectionOrderId,
                                 CheckboxID = rep.CheckboxID,
                                 IsUpdated = rep.IsUpdated,
                                 IsInspectorReviewed = rep.IsInspectorReviewed,
                                 Type = rep.Type,
                                 DirectionDropID = rep.DirectionDropID,
                                 //  Direction = direction.DirectionDropValue,
                                 DirectionDropMaster = order,
                                 PresetComments = rep.PresetComments,
                                 Comments = rep.Comments,
                                 Notice = rep.Notice,
                                 MSortOrder = Msection.SortOrder,
                                 SSortOrder = SSection.SortOrder,
                                 IsUpdateSection = false,
                                 PL = (from p in context.PhotoLibraries
                                       where p.InpectionReportFID == rep.ItemID
                                       select new ImagesNote
                                       {
                                           ID = p.ID,
                                           ImageUrl = p.PhotoUrl,
                                           ImageNote = p.Notes
                                       })

                             }).OrderBy(x => x.MSortOrder).ThenBy(z => z.SSortOrder).ToList();


                #region Get Report Count
                SampleReportCount objSampleReportCount = new SampleReportCount();
                var Temp_Report = from IR in context.InspectionReport
                                  where IR.InspectionOrderId == inspectionOrderId
                                  group IR by IR.CheckboxID into g
                                  select new
                                  {
                                      Category = g.Key,
                                      Products = g
                                  };

                foreach (var item in Temp_Report)
                {

                    if (item.Category == "1")
                        objSampleReportCount.GreenArrowCount = item.Products.Count();
                    else if (item.Category == "2")
                        objSampleReportCount.YellowArrowCount = item.Products.Count();
                    else if (item.Category == "3")
                        objSampleReportCount.RedArrowCount = item.Products.Count();
                    else if (item.Category == "4")
                        objSampleReportCount.DoubleRedArrowCount = item.Products.Count();
                    else if (item.Category == "5")
                        objSampleReportCount.YellowWarningCount = item.Products.Count();
                    else if (item.Category == "6")
                        objSampleReportCount.RedWarningCount = item.Products.Count();
                    else if (item.Category == "7")
                        objSampleReportCount.Not_accessibility = item.Products.Count();
                    else if (item.Category == "8")
                        objSampleReportCount.No_inspection = item.Products.Count();
                }

                objSampleReportCount.BlueIconCount = (from IR in context.InspectionReport
                                                      join PL in context.tblPhotoLibrary on IR.ID equals PL.InspectionReportID
                                                      where IR.InspectionOrderId == inspectionOrderId && PL.IsApproved == true
                                                      select new Report
                                                      {
                                                          OrderID = PL.ID
                                                      }).Count();
                #endregion

                //#region Updated Sections
                //bool? isUpdatable = context.InspectionOrders.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId).IsUpdatable;

                //if (isUpdatable == true)
                //{

                //    objUpdatableReport = (from SubSectionstatus in context.SubSectionStatu
                //                          join Msection in context.MainSections on SubSectionstatus.SectionID equals Msection.ID
                //                          join SSection in context.SubSections on SubSectionstatus.SubSectionID equals SSection.ID
                //                          join PL in context.PhotoLibraries on SubSectionstatus.ID equals PL.InpectionReportFID
                //                          join direction in context.DirectionDropMaster on PL.DirectionDrop equals direction.DirectionDropID into order
                //                          where SubSectionstatus.InspectionOrderID == inspectionOrderId && SubSectionstatus.Status == true
                //                          && PL.AcceptedOrRejected == true
                //                          select new DetailsReportCommonPoco
                //                          {
                //                              ID = "",
                //                              SectionID = PL.SectionID,
                //                              SubSectionID = PL.SubSectionId,
                //                              SectionName = Msection.SectionName,
                //                              SubSectionName = SSection.SubSectionName,
                //                              Comments = PL.Comments,
                //                              CheckboxID = "8",
                //                              Type = "",
                //                              MSortOrder = Msection.SortOrder,
                //                              DirectionDropMaster = order,
                //                              SSortOrder = SSection.SortOrder,
                //                              DirectionDropID = 0,
                //                              PresetComments = "",
                //                              Notice = "",
                //                              CreatedDate = PL.CreatedDate,
                //                              IsUpdateSection = true,
                //                              PL = (from p in context.PhotoLibraries
                //                                    where p.InpectionReportFID == SubSectionstatus.ID
                //                                    && p.AcceptedOrRejected == true
                //                                    select new ImagesNote
                //                                    {
                //                                        ImageUrl = p.PhotoUrl,
                //                                        ImageNote = p.Notes
                //                                    })
                //                          }).OrderBy(x => x.MSortOrder).ThenBy(z => z.SSortOrder).ToList();


                //    if (objUpdatableReport.Count > 0)
                //    {

                //        for (int i = 0; i < objUpdatableReport.Count; i += 4)
                //        {
                //            count++;
                //            objReport.Add(objUpdatableReport[i]);
                //        }
                //    }
                //}

                //#endregion
                if (objReport != null)
                {
                    objDetailsReportCommonPoco.ReportsDetail = objReport;
                    objSampleReportCount.BlueIconCount = count;
                    objDetailsReportCommonPoco.ReportCount = objSampleReportCount;
                }

                /*Save Data in Rport Viewed table--Start*/
                if (Sessions.LoggedInUser.RoleName == "Home Seller" || Sessions.LoggedInUser.RoleName == "Home Buyer")
                {
                    string PaymentId = string.Empty;
                    if (PaymentType == "PaymentPlan")
                    {
                        if (Sessions.LoggedInUser.RoleName == "Home Seller")
                            PaymentId = context.Payments.OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.UserID == Sessions.LoggedInUser.UserId && x.ReportID == inspectionOrderId).PaymentID;
                        else
                            PaymentId = context.Payments.OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.UserID == Sessions.LoggedInUser.UserId).PaymentID;
                    }

                    string ipaddress = "";
                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (ipaddress == "" || ipaddress == null)
                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];


                    var Inspectionid = context.InspectionOrders.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId).InspectionId;
                    reportviewed objReportViewed = new reportviewed();
                    objReportViewed.ID = Guid.NewGuid().ToString();
                    objReportViewed.UserID = Sessions.LoggedInUser.UserId;
                    objReportViewed.ReportID = Inspectionid;
                    objReportViewed.CreatedDate = System.DateTime.Now;
                    objReportViewed.ModifedDate = System.DateTime.Now;
                    objReportViewed.MachineIP = PaymentType == "PaymentPlan" ? ipaddress : "";
                    objReportViewed.PaymentID = PaymentId;
                    objReportViewed.Viewed = true;
                    context.ReportViewed.Add(objReportViewed);
                    //    context.SaveChanges();
                    /*End*/
                }

                _messageToShow.Result = RenderRazorViewToString("_EditComprehensiveReport", objDetailsReportCommonPoco);
                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult SaveEditableReport(string txtBoxId, string Comments, string InspectionReportID)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    inspectionreport objInspectionReport = context.InspectionReport.Where(x => x.ID == InspectionReportID).FirstOrDefault();
                    if (objInspectionReport != null)
                    {
                        if (txtBoxId == "txtNotice")
                        {

                            objInspectionReport.Notice = Comments;
                        }
                        else if (txtBoxId == "txtPresetComments")
                        {
                            objInspectionReport.PresetComments = Comments;
                        }
                        else if (txtBoxId == "txtType")
                        {
                            objInspectionReport.Type = Comments;
                        }
                        context.SaveChanges();
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult SaveCheckBoxImage(string CheckBoxId, string InspectionReportID)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    inspectionreport objInspectionReport = context.InspectionReport.Where(x => x.ID == InspectionReportID).FirstOrDefault();
                    if (objInspectionReport != null)
                    {
                        objInspectionReport.CheckboxID = CheckBoxId;
                        context.SaveChanges();
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SaveImage(string PhotoLibraryID, string imagesUrl)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    string[] imagesUrls = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<string[]>(imagesUrl);
                    photolibrary objPhotoLibrary = context.PhotoLibraries.Where(x => x.ID == PhotoLibraryID).FirstOrDefault();
                    if (objPhotoLibrary != null)
                    {
                        objPhotoLibrary.PhotoUrl = ConvertToImage(imagesUrls[0].Split(',')[1]);
                        context.SaveChanges();
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }


                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Upload Image
        /// <summary>
        /// Upload Image
        /// </summary>
        /// <returns></returns>
        public string UploadImage()
        {
            int width = 0; int height = 0; Image image = null;
            string postedfile = ""; long contentLength;
            var headers = Request.Headers;
            byte[] fileData = null;

            if (string.IsNullOrEmpty(headers["X-File-Name"]))
            {
                postedfile = Request.Files[0].FileName;
                contentLength = Request.Files[0].ContentLength;
                using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
                {
                    fileData = binaryReader.ReadBytes(Request.Files[0].ContentLength);
                }
            }
            else
            {
                postedfile = headers["X-File-Name"];
                contentLength = Convert.ToInt64(headers["X-File-Size"]);
            }
            // return System.Text.Encoding.Default.GetString(fileData);
            if (fileData != null)
            {
                TempData["img"] = fileData;
                return "data:image/png;base64," + System.Convert.ToBase64String(fileData, 0, fileData.Length);
            }
            else
            {
                return "";
            }
        }
        #endregion

        #region ConvertToImage
        /// <summary>
        /// function to convert image in base 64
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="sectionId"></param>
        /// <param name="subsectionId"></param>
        /// <returns></returns>
        public string ConvertToImage(string base64)
        {
            try
            {
                string FileName = string.Empty;

                using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(base64)))
                {
                    using (Bitmap bm2 = new Bitmap(ms))
                    {
                        FileName = Guid.NewGuid().ToString() + ".jpg";
                        bm2.Save(System.Configuration.ConfigurationManager.AppSettings["ImageLocation"] + "/" + FileName);
                    }
                }
                return FileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public ActionResult BasicReport(string orderid)
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    string ipaddress = "";
                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (ipaddress == "" || ipaddress == null)
                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];

                    reportviewed objreport;
                    objreport = context.ReportViewed.Where(x => x.ReportID == orderid && x.UserID == Sessions.LoggedInUser.UserId).FirstOrDefault();
                    if (objreport != null)
                    {
                        objreport.ModifedDate = DateTime.Now;
                    }
                    else
                    {
                        objreport = new reportviewed();
                        objreport.ID = Guid.NewGuid().ToString();
                        objreport.ReportID = orderid;
                        objreport.UserID = Sessions.LoggedInUser.UserId;
                        objreport.MachineIP = ipaddress;
                        objreport.CreatedDate = System.DateTime.Now;
                        objreport.IsPurchased = false;
                        context.ReportViewed.Add(objreport);

                    }
                    context.SaveChanges();

                    var Commnet = (from IR in context.InspectionReport
                                   where IR.InspectionOrderId == orderid && IR.Comments != null && IR.Comments != string.Empty
                                   select new
                                   {
                                       ID = IR.ID
                                   }).Count();

                    var PCommnet = (from IR in context.InspectionReport
                                    where IR.InspectionOrderId == orderid && IR.PresetComments != null && IR.PresetComments != string.Empty
                                    select new
                                    {
                                        ID = IR.ID
                                    }).Count();

                    var Photo = (from p in context.PhotoLibraries
                                 where p.InspectionOrderID == orderid
                                 select new
                                 {
                                     ID = p.ID
                                 }).Count();
                    ViewBag.Photo = Convert.ToInt32(Photo);
                    ViewBag.Comments = Convert.ToInt32(Commnet) + Convert.ToInt32(PCommnet);

                    SampleReportCount objSampleReportCount = new SampleReportCount();
                    MySqlConnection conn = new MySqlConnection(connectionstring);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("SampleInspectionReportSeller", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserID", Sessions.LoggedInUser.UserId);
                    cmd.Parameters.AddWithValue("OrderID", orderid);
                    cmd.Parameters.AddWithValue("IPAddress", ipaddress);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {


                            if (item["CheckBoxId"].ToString() == "1")
                                objSampleReportCount.GreenArrowCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "2")
                                objSampleReportCount.YellowArrowCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "3")
                                objSampleReportCount.RedArrowCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "4")
                                objSampleReportCount.DoubleRedArrowCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "5")
                                objSampleReportCount.YellowWarningCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "6")
                                objSampleReportCount.RedWarningCount = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "7")
                                objSampleReportCount.No_inspection = Convert.ToInt32(item["count"]);
                            else if (item["CheckBoxId"].ToString() == "8")
                                objSampleReportCount.Not_accessibility = Convert.ToInt32(item["count"]);
                        }
                        objSampleReportCount.BlueIconCount = (from IR in context.InspectionReport
                                                              join PL in context.tblPhotoLibrary on IR.ID equals PL.InspectionReportID
                                                              where IR.InspectionOrderId == orderid && PL.IsApproved == true
                                                              select new Report
                                                              {
                                                                  OrderID = PL.ID
                                                              }).Count();
                    }
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                    {
                        ReportPropertyAddress ReportAddress = new ReportPropertyAddress();


                        ReportAddress.HouseNo = ds.Tables[1].Rows[0]["HouseNo"].ToString();
                        ReportAddress.PropertyCity = ds.Tables[1].Rows[0]["PropertyCity"].ToString();
                        ReportAddress.PropertyState = ds.Tables[1].Rows[0]["PropertyState"].ToString();
                        ReportAddress.PropertyZip = Convert.ToInt32(ds.Tables[1].Rows[0]["PropertyZip"]);
                        ReportAddress.ReportNo = ds.Tables[1].Rows[0]["ReportNo"].ToString();
                        objSampleReportCount.Address = ReportAddress;
                    }
                    #region Dropdowns


                    List<SelectListItem> month = new List<SelectListItem>();
                    SelectListItem objSelectListItemmonth = new SelectListItem();
                    for (int i = 1; i <= 12; i++)
                    {
                        SelectListItem s = new SelectListItem();
                        if (i < 10)
                        {
                            s.Text = "0" + i.ToString();
                            s.Value = "0" + i.ToString();
                        }
                        else
                        {
                            s.Text = i.ToString();
                            s.Value = i.ToString();
                        }

                        month.Add(s);
                    }
                    ViewBag.Month = new SelectList(month, "Value", "Text", null);


                    List<SelectListItem> year = new List<SelectListItem>();
                    SelectListItem objSelectListItemyear = new SelectListItem();
                    for (int i = 2013; i <= 2022; i++)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = i.ToString();
                        s.Value = i.ToString();
                        year.Add(s);
                    }

                    ViewBag.Year = new SelectList(year, "Value", "Text", null);


                    #endregion
                    #region Bind States
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "--Select--";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    ViewBag.StateList = items;
                    ViewBag.ZipList = objZipCodeList;
                    ViewBag.CityList = objCityList;

                    #endregion

                    int[] template = { 31, 37 };
                    IEnumerable<emailtemplate> objListOfEmailTemplates = from emailtemplate in context.EmailTemplates
                                                                         where template.Contains(emailtemplate.TemplateId)
                                                                         select emailtemplate;
                    foreach (var item in objListOfEmailTemplates)
                    {
                        if (item.TemplateName == "OneTimePayment")
                        {
                            objSampleReportCount.OneTimePayment = (float)item.Rate;
                            objSampleReportCount.OneTimePaymentValidity = item.Validity;
                        }
                        else
                        {
                            objSampleReportCount.PurchasePlan = (float)item.Rate;
                            objSampleReportCount.PurchasePlanValidity = item.Validity;
                        }

                    }
                    var PreviousAmount = (from pay in context.Payments
                                          join IO in context.tblinspectionorder on pay.ReportID equals IO.InspectionOrderDetailId
                                          where pay.UserID == Sessions.LoggedInUser.UserId && IO.IsDeleted == false
                                          select new
                                          {
                                              amount = pay.PaymentAmount
                                          }).Sum(x => x.amount);
                    if (PreviousAmount != null)
                    {
                        float amount = Convert.ToSingle(PreviousAmount);
                        if (objSampleReportCount.PurchasePlan <= amount)
                        {
                            ViewBag.IsPaid = "Already Paid";
                            ViewBag.PaidAmount = amount;
                        }
                        else
                        {
                            objSampleReportCount.PurchasePlan = objSampleReportCount.PurchasePlan - amount;
                            ViewBag.Amount = objSampleReportCount.PurchasePlan - amount;
                            ViewBag.PaidAmount = amount;
                        }
                    }
                    return View(objSampleReportCount);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public JsonResult SaveIncludeStatus(string ReportId, string SectionId, string SubSectiontd, bool IsInclude)
        {
            try
            {

                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "SiteUser")
                {
                    if (context.tblisincluded.Any(x => x.ReportId == ReportId && x.SectionId == SectionId && x.SubSectionId == SubSectiontd))
                    {
                        context.tblisincluded.FirstOrDefault(x => x.ReportId == ReportId && x.SectionId == SectionId && x.SubSectionId == SubSectiontd).IsIncluded = IsInclude;
                        context.SaveChanges();
                        return Json("Update", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        tblisincluded objtblisincluded = new tblisincluded();
                        objtblisincluded.ReportId = ReportId;
                        objtblisincluded.SectionId = SectionId;
                        objtblisincluded.SubSectionId = SubSectiontd;
                        objtblisincluded.IsIncluded = IsInclude;
                        objtblisincluded.CreatedDate = DateTime.Now;
                        context.tblisincluded.Add(objtblisincluded);
                        context.SaveChanges();
                    }
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
