using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;
using System.Configuration;
using SpectorMax.Utilities.Classes;
using System.Text;
using SpectorMaxDAL;
using SpectorMax.Entities.Classes;
using System.Net;


namespace SpectorMax.Controllers
{
    public class PaymentsController : BaseController
    {
        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _messageToShow = new MessageToShow();

        /// <summary>
        /// // GET: /Payments/ Method redirect to user for choose payment plan
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userRoleType"></param>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        public ViewResult Index(string userId, string userRoleType, string userRoleId, string inspectionOrderId)
        {
            try
            {
                //List<EmailTemplate> emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateName.Contains("HomeSeller")).ToList();
                List<emailtemplate> emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateId == 3).ToList();
                ViewBag.UserId = userId;
                ViewBag.UserRoleType = userRoleType;
                ViewBag.UserRole = userRoleId;
                ViewBag.inspectionOrderId = inspectionOrderId;
                return View(emailTemplateList);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public ViewResult ThankYou(string PlanType, string userId, string userRoleType)
        {
            if (userId != null)
            {
                var UserDetails = context.UserInfoes.Where(x => x.ID == userId).FirstOrDefault();
                ViewBag.UserName = UserDetails.FirstName + " " + UserDetails.LastName;
            }

            ViewBag.userRole = userRoleType;
            ViewBag.PlanType = PlanType;
            return View();
        }

        public ViewResult InnerThankYou(string PlanType)
        {
            ViewBag.PlanType = PlanType;
            return View();
        }
        /// <summary>
        /// GET: /Payments/ redirect to user on payment page for payment.
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="amountDescription"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult Payment(string userId, string userRoleId, string amount, string amountDescription, string paymentType, string inspectionOrderId, string Validity = "0", string TemplateID = "0") //string userId, string userRoleId,
        {
            try
            {

                if (Sessions.LoggedInUser != null)
                {
                    if (Request.Url.Query.Contains("OneTimePaymentPlan"))
                    {
                        ViewBag.UserId = userId;
                        ViewBag.UserRoleId = userRoleId;
                        ViewBag.Amount = amount;
                        ViewBag.AmountDescription = amountDescription;
                        ViewBag.PaymentType = paymentType;
                        ViewBag.InspectionOrderId = inspectionOrderId;

                    }
                    else if (Request.Url.Query.Contains("PaymentPlan") && (Sessions.LoggedInUser.RoleName == "Home Buyer"))
                    {
                        int templateID = Convert.ToInt32(TemplateID);
                        ViewBag.UserId = userId;
                        ViewBag.UserRoleId = userRoleId;
                        ViewBag.Amount = amount;
                        if (templateID != 0)
                        {
                            string Description = context.EmailTemplates.Where(x => x.TemplateId == templateID).FirstOrDefault().TemplateDiscription;
                            Description = Description.Replace("630px", "90%");
                            ViewBag.AmountDescription = Description;
                        }
                        else
                        {
                            ViewBag.AmountDescription = amountDescription;
                        }
                        ViewBag.PaymentType = paymentType;
                        ViewBag.InspectionOrderId = inspectionOrderId;
                        ViewBag.Validity = Validity;
                    }
                    else if (Request.Url.Query.Contains("PaymentPlan") && Sessions.LoggedInUser.RoleName == "Home Seller")
                    {
                        List<string> IsUpdatable = TempData["IsUpdatable"] as List<string>;
                        List<emailtemplate> emailTemplateList = new List<emailtemplate>();
                        if (IsUpdatable[0] == "True")
                            emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateId == 17).ToList();
                        else if (IsUpdatable[0] == "False")
                            emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateId == 18).ToList();

                        ViewBag.UserId = Sessions.LoggedInUser.UserId;
                        ViewBag.UserRoleId = Sessions.LoggedInUser.RoleId;
                        ViewBag.Amount = emailTemplateList.FirstOrDefault().Rate;
                        ViewBag.AmountDescription = emailTemplateList.FirstOrDefault().TemplateDiscription;
                        ViewBag.PaymentType = "HomeSeller";
                        ViewBag.InspectionOrderId = IsUpdatable[1];
                    }

                    else if (paymentType == "AdditionalTestingPayment")
                    {
                        List<string> IsUpdatable = TempData["IsUpdatable"] as List<string>;
                        List<emailtemplate> emailTemplateList = new List<emailtemplate>();
                        if (IsUpdatable[0] == "True")
                            emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateId == 17).ToList();
                        else if (IsUpdatable[0] == "False")
                            emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateId == 18).ToList();

                        ViewBag.UserId = Sessions.LoggedInUser.UserId;
                        ViewBag.UserRoleId = Sessions.LoggedInUser.RoleId;
                        ViewBag.Amount = emailTemplateList.FirstOrDefault().Rate;
                        ViewBag.AmountDescription = emailTemplateList.FirstOrDefault().TemplateDiscription;
                        ViewBag.PaymentType = "HomeSeller";
                        ViewBag.InspectionOrderId = IsUpdatable[1];
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {

                throw;
            }


        }

        /// <summary>
        /// GET: /Payments/ redirect to user on payment page for payment.
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="amountDescription"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult BuyerPayment(string userId, string userRoleId, string amount, string amountDescription, string paymentType, string inspectionOrderId, string Validity = "0", string TemplateID = "0") //string userId, string userRoleId,
        {
            try
            {
                int templateID = Convert.ToInt32(TemplateID);

                ViewBag.UserId = userId;
                ViewBag.UserRoleId = userRoleId;
                ViewBag.Amount = amount;
                if (templateID != 0)
                {
                    string Description = context.EmailTemplates.Where(x => x.TemplateId == templateID).FirstOrDefault().TemplateDiscription;
                    Description = Description.Replace("630px", "90%");
                    ViewBag.AmountDescription = Description;
                }
                else
                {
                    ViewBag.AmountDescription = amountDescription;
                }
                ViewBag.PaymentType = paymentType;
                ViewBag.InspectionOrderId = inspectionOrderId;
                ViewBag.Validity = Validity;

                return View();

            }
            catch (Exception)
            {

                throw;
            }


        }


        public ActionResult PaymentPopup(string amount, string amountDescription, string paymentType) //string userId, string userRoleId,
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    ViewBag.Amount = amount;
                    ViewBag.AmountDescription = amountDescription;
                    ViewBag.PaymentType = paymentType;
                    ViewBag.UserId = Sessions.LoggedInUser.UserId;
                    ViewBag.UserRoleId = Sessions.LoggedInUser.RoleId;
                    // _messageToShow.Result = "Render";
                    return PartialView("_PaymentPopup");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        ///  POST: /Payments/ method submit the payment to paypay and store the payment information in merchant database.
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <param name="expiryMonth"></param>
        /// <param name="expiryYear"></param>
        /// <param name="securityCode"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Payment(string cardNumber, string expiryMonth, string expiryYear, string securityCode, string userId, string userRoleId, string amount, string paymentType, string inspectionOrderId, string Validity = "0")
        {
            PayPalDirectPayment Payment = new PayPalDirectPayment();
            string message = Payment.PaymentToPayPal(cardNumber, expiryMonth, expiryYear, securityCode, amount);
            if (message.Contains("RESULT:</span> 0<br />") && message.Contains("Successful."))
            {
                string returnMessage = _messageToShow.Message = SaveUserPaymentInformation(userId, userRoleId, amount, paymentType, inspectionOrderId, Validity);
                if (returnMessage == "Success")
                {
                    _messageToShow.Message = "Success";
                    return Json(_messageToShow);
                }
                else
                {
                    _messageToShow.Message = returnMessage;
                    return Json(_messageToShow);
                }
            }
            else
            {
                //string returnMessage = _messageToShow.Message = SaveUserPaymentInformation(userId, userRoleId, amount, paymentType, inspectionOrderId, Validity);
                _messageToShow.Message = "Payment Failure";
                return Json(_messageToShow);
            }
        }
        /// <summary>
        /// private method for store payment information in merchant database corresponding to user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userRoleId"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        private string SaveUserPaymentInformation(string userId, string userRoleId, string amount, string paymentType, string inspectionOrderId, string validity)
        {
            try
            {
                SpectorMaxDAL.payment userPayment = new SpectorMaxDAL.payment();
                userPayment.PaymentID = Guid.NewGuid().ToString();
                DateTime ValidityEndDate;
                if (validity != "" && validity != "0")
                {
                    ValidityEndDate = DateTime.Now.AddDays(Convert.ToInt32(validity));
                }
                else
                {
                    ValidityEndDate = DateTime.Now;
                }
                if (inspectionOrderId != null)
                {
                    userPayment.ReportID = inspectionOrderId;
                }
                else
                {
                    userPayment.ReportID = "0";
                }
                userPayment.UserID = userId;
                userPayment.RoleID = userRoleId;
                userPayment.PaymentStatus = "Success";
                userPayment.PaymentDate = DateTime.Now;
                userPayment.PaymentAmount = float.Parse(amount); ;
                userPayment.PaymentType = paymentType;
                userPayment.CreatedDate = DateTime.Now;
                userPayment.PlanValidity = ValidityEndDate;
                context.Payments.Add(userPayment);
                context.SaveChanges();
                _messageToShow.Message = "Success";
                if (Sessions.LoggedInUser != null && paymentType == "AdditionalTestingPayment")
                {
                    Session["PaymentId"] = userPayment.PaymentID;
                    Sessions.LoggedInUser.AdditionalTestingPaymentStatus = "Success";
                }
                return _messageToShow.Message;
            }
            catch (Exception ex)
            {
                _messageToShow.Message = "Payment is successful, but saving Transaction information in Marchant database is fail";
                return _messageToShow.Message;
            }

        }

        /// <summary>
        ///  GET: / method for getting ViewPreviousInvoice Details for specific userid.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>

        public ViewResult Details(string userId)
        {
            if (Sessions.LoggedInUser != null)
            {
                //code is commented because custom page is not required for this time.
                // int PageNumber = 0;
                //  int PerPageRecord = 20;
                // bool islast = false;
                PaymentPoco paymentPoco = new PaymentPoco();
                paymentPoco.objPaging = new Paging();
                //paymentPoco.objPaging = PagingResult(PageNumber, PerPageRecord, islast);
                paymentPoco.objPayment = new List<Payments>();
                paymentPoco.objPayment = GetPaymentRecords(userId);
                //paymentPoco.objPayment = context.EmailTemplates.ToList().Where(x => x.TemplateName.Contains("PaymentInvoice")).ToList().FirstOrDefault();
                return View(paymentPoco);
            }
            else
            {
                return View("~/Views/Home/Index.cshtml");
            }
        }




        /// <summary>
        /// private method for getting invoice information according to page number in paging.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="SkipRecord"></param>
        /// <param name="PerPageRecord"></param>
        /// <returns></returns>
        private List<Payments> GetPaymentRecords(string userId)
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {

                    List<Payments> paymentList = new List<Payments>();
                    paymentList = (from details in context.Payments
                                   where details.UserID == userId
                                       && details.PaymentStatus == "Success"
                                   select new Payments
                                   {
                                       PaymentDate = details.PaymentDate,
                                       PaymentAmount = details.PaymentAmount,
                                       PaymentID = details.PaymentID,
                                       ReportID = details.ReportID,
                                       PaymentType = details.PaymentType,
                                       PaymentStatus = details.PaymentStatus
                                   }).ToList(); //code is commented because custom paging is not required for this time now //.OrderByDescending(x => x.PaymentDate).Skip(SkipRecord).Take(PerPageRecord)
                    if (paymentList.Count > 0)
                    {
                        paymentList.ElementAt(0).PaymentInvoice = context.EmailTemplates.ToList().Where(y => y.TemplateName.Contains("PaymentInvoice")).ToList().FirstOrDefault().TemplateDiscription.ToString();
                        var UserInfo = context.UserInfoes.ToList().Where(x => x.ID == Sessions.LoggedInUser.UserId).FirstOrDefault();

                        paymentList.ElementAt(0).CustomerAddress = UserInfo.Address + " " + UserInfo.AddressLine2 + " " + UserInfo.City + " " + UserInfo.State + " " + UserInfo.ZipCode + " " + UserInfo.Country;
                    }
                    return paymentList;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }


        }



        //
        // GET: /Payments/Create

        public ActionResult Create()
        {
            ViewBag.PossibleRoles = context.Roles;
            return View();
        }

        //
        // POST: /Payments/Create

        [HttpPost]
        public ActionResult Create(payment payment)
        {
            if (ModelState.IsValid)
            {
                context.Payments.Add(payment);
                context.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PossibleRoles = context.Roles;
            return View(payment);
        }

        //
        // GET: /Payments/Edit/5

        public ActionResult Edit(string id)
        {
            payment payment = context.Payments.Single(x => x.PaymentID == id);
            ViewBag.PossibleRoles = context.Roles;
            return View(payment);
        }

        //
        // POST: /Payments/Edit/5

        [HttpPost]
        public ActionResult Edit(payment payment)
        {
            if (ModelState.IsValid)
            {
                context.Entry(payment).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PossibleRoles = context.Roles;
            return View(payment);
        }

        //
        // GET: /Payments/Delete/5

        public ActionResult Delete(string id)
        {
            payment payment = context.Payments.Single(x => x.PaymentID == id);
            return View(payment);
        }

        //
        // POST: /Payments/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            payment payment = context.Payments.Single(x => x.PaymentID == id);
            context.Payments.Remove(payment);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult BuyComprehensiveReprot(string ReportId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
                {
                    if (ReportId != null && ReportId != "")
                    {



                        BuyReports objBuyreports = new BuyReports();
                        objBuyreports = (from pay in context.EmailTemplates
                                         where pay.TemplateName == "OneTimePayment"
                                         select new BuyReports
                                         {
                                             Description = pay.TemplateDiscription,
                                             Amount = pay.Rate,
                                             Validity = pay.Validity
                                         }).FirstOrDefault();
                        ViewBag.ReportId = ReportId;
                        ViewBag.Amount = objBuyreports.Amount;



                        PropertyAddress objPropertyAddress = new PropertyAddress() ;
                        objPropertyAddress = (from IOD in context.tblinspectionorderdetail
                                              join s in context.tblstate on IOD.PropertyState equals s.StateId
                                              join c in context.tblcity on IOD.PropertyCity equals c.CityId
                                              join z in context.tblzip on IOD.PropertyZip equals z.ZipId
                                             where IOD.InspectionorderdetailsID == ReportId
                                             select new PropertyAddress
                                             {
                                                 StreetAddress = IOD.PropertyAddress,
                                                 State=s.StateName,
                                                 City=c.CityName,
                                                 Zip=z.Zip
                                             }).FirstOrDefault();
                        
                        if (objPropertyAddress != null)
                        {
                            objBuyreports.Description = objBuyreports.Description.Replace("(house # value)", objPropertyAddress.StreetAddress);
                            objBuyreports.Description = objBuyreports.Description.Replace("(City)", objPropertyAddress.City);
                            objBuyreports.Description = objBuyreports.Description.Replace("(State)", objPropertyAddress.State);
                            objBuyreports.Description = objBuyreports.Description.Replace("(Zip)", objPropertyAddress.Zip.ToString());
                        }
                       
                        if (objBuyreports != null)
                        {
                            return View(objBuyreports);
                        }
                    }
                    else
                    {

                        if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.PaymentPlan != "PaymentPlan")
                        {

                            var PreviousAmount = (from pay in context.Payments
                                                  join IO in context.tblinspectionorder on pay.ReportID equals IO.InspectionOrderDetailId
                                                  where pay.UserID == Sessions.LoggedInUser.UserId && IO.IsDeleted == false
                                                  select new
                                                  {
                                                      amount=pay.PaymentAmount
                                                  }).Sum(x=>x.amount);



                            BuyReports objBuyreports = new BuyReports();
                            objBuyreports = (from pay in context.EmailTemplates
                                             where pay.TemplateName == "Two Month Membership"
                                             select new BuyReports
                                             {
                                                 Description = pay.TemplateDiscription,
                                                 Amount = pay.Rate,
                                                 Validity = pay.Validity
                                             }).FirstOrDefault();
                            ViewBag.ReportId = "PaymentPlan";
                            if (PreviousAmount != null)
                            {
                                float amount = Convert.ToSingle(PreviousAmount);
                                if (objBuyreports.Amount <= amount)
                                {
                                    ViewBag.IsPaid = "Already Paid";
                                    ViewBag.PaidAmount = amount;
                                }
                                else
                                {
                                    objBuyreports.Amount = objBuyreports.Amount - amount;
                                    ViewBag.Amount = objBuyreports.Amount-amount;
                                    ViewBag.PaidAmount = amount;
                                }
                            }
                            if (objBuyreports != null)
                            {
                                return View(objBuyreports);
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMsg = "Already taken membership";
                            return View();
                        
                        }   
                    }
                }
                else
                {
                    RedirectToAction("Index", "Home");
                }
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*Additional Testing Payment*/
        public JsonResult AdditionalTesting_MakePayment(string cardNumber, string expiryMonth, string expiryYear, string securityCode, string amount)
        {
            try
            {
                string result = ePayment(cardNumber, expiryMonth, expiryYear, securityCode, amount,"");
                if (result == "Success")
                {
                    //string type = SavePaymentDetails(amount, ReportID);
                    //if (type == "PaymentPlan")
                    //{
                    //    _messageToShow.Message = "SuccessPaymenPlan";
                    //}
                    //else
                    //{
                    //    Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
                    //    _messageToShow.Message = "SuccessOneTime";
                    //}
                }
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
            }

            return Json(_messageToShow, JsonRequestBehavior.AllowGet);

        }


        public JsonResult MakePayment(string cardNumber, string expiryMonth, string expiryYear, string securityCode, string amount, string ReportID)
        {
            try
            {


                string result = ePayment(cardNumber, expiryMonth, expiryYear, securityCode, amount, ReportID);
                if (result == "Success")
                {
                   string type= SavePaymentDetails(amount, ReportID);
                   if (type == "PaymentPlan")
                   {
                       _messageToShow.Message = "SuccessPaymenPlan";
                   }
                   else
                   {
                       Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
                       _messageToShow.Message = "SuccessOneTime";
                   }

                }
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
            }

            return Json(_messageToShow, JsonRequestBehavior.AllowGet);

        }
        public static string BuildPost(string sPrevPost, string sField, string sValue)
        {
            string functionReturnValue = null;
            if (sPrevPost == null)
                functionReturnValue = "";
            else
                functionReturnValue = sPrevPost + "&";
            functionReturnValue = functionReturnValue + sField;
            functionReturnValue = functionReturnValue + "=";
            functionReturnValue = functionReturnValue + HttpUtility.UrlEncode(sValue);
            return functionReturnValue;
        }

        public static string subXMLPostProcess(string sURL, string sRequestString)
        {
            string ResultString = "";
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                ResultString = wc.UploadString(sURL, sRequestString);
            }
            return ResultString;
        }

        public string SavePaymentDetails(string amount, string reportid)
        {
            string Type = string.Empty;
            SpectorMaxDAL.payment userPayment = new SpectorMaxDAL.payment();
            userPayment.PaymentID = Guid.NewGuid().ToString();
            userPayment.UserID = Sessions.LoggedInUser.UserId;
            userPayment.ReportID = reportid;
            userPayment.PaymentStatus = "Success";
            userPayment.PaymentDate = DateTime.Now;
            Random random = new Random();
            int num = random.Next(1000000);
            var MaxInvoiceNo = context.Payments.Max(x => x.InvoiceNo);
            if (MaxInvoiceNo == null || Convert.ToInt32(MaxInvoiceNo)==0)
            {
                userPayment.InvoiceNo = 2000;
            }
            else
            {
                userPayment.InvoiceNo = Convert.ToInt32(MaxInvoiceNo)+1;
            }
            userPayment.PaymentAmount = float.Parse(amount); ;
            if (reportid == "PaymentPlan")
            {
                userPayment.PaymentType = "PaymentPlan";
                userPayment.PaymentDate = System.DateTime.Now;
                userPayment.PlanValidity = System.DateTime.Now.AddDays(60);
                Sessions.LoggedInUser.PaymentPlan = "PaymentPlan";
                Type = "PaymentPlan";
            }
            else
            {
                userPayment.PaymentType = "OneTimePayment";
                Type = "OneTimePayment";
                userPayment.PaymentDate = System.DateTime.Now;
                Sessions.LoggedInUser.PaymentPlan = "OneTimePayment";
            }
            userPayment.CreatedDate = DateTime.Now;
            context.Payments.Add(userPayment);
            if (reportid != "PaymentPlan")
            {
                reportviewed objReport = context.ReportViewed.Where(x => x.ReportID == reportid && x.UserID == Sessions.LoggedInUser.UserId).FirstOrDefault();
                if (objReport != null)
                {
                    objReport.IsPurchased = true;
                    objReport.PaymentID = userPayment.PaymentID;
                }
                else
                {
                    string ipaddress = "";
                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (ipaddress == "" || ipaddress == null)
                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                    reportviewed objReportAdd = new SpectorMaxDAL.reportviewed();
                    objReportAdd.ID = Guid.NewGuid().ToString();
                    objReportAdd.CreatedDate = System.DateTime.Now;
                    objReportAdd.IsPurchased = true;
                    objReportAdd.PaymentID = userPayment.PaymentID;
                    objReportAdd.ReportID = reportid;                  
                    objReportAdd.UserID = Sessions.LoggedInUser.UserId;
                    objReportAdd.MachineIP = ipaddress;
                    context.ReportViewed.Add(objReportAdd);
                }
            }
            context.SaveChanges();
            return Type;
        }

        public ActionResult TwoMothunlimitedMembership()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public JsonResult CheckOut(CreateBuyer objBuyer)
        {
            string Message = string.Empty;
            try
            {
                
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "SiteUser")
                {

                    string sPostString = "";
                   
                    sPostString = BuildPost(sPostString, "ePNAccount", ConfigurationManager.AppSettings["ePNAccount"].ToString());
                    sPostString = BuildPost(sPostString, "CardNo", objBuyer.cardNumber);
                    sPostString = BuildPost(sPostString, "ExpMonth", objBuyer.expiryMonth);
                    sPostString = BuildPost(sPostString, "ExpYear", objBuyer.expiryYear);
                    sPostString = BuildPost(sPostString, "Total", objBuyer.amount);
                    sPostString = BuildPost(sPostString, "Address", ConfigurationManager.AppSettings["Address"].ToString());
                    sPostString = BuildPost(sPostString, "Zip", ConfigurationManager.AppSettings["Zip"].ToString());
                    sPostString = BuildPost(sPostString, "RestrictKey", ConfigurationManager.AppSettings["RestrictKey"].ToString());
                    sPostString = BuildPost(sPostString, "CVV2Type", ConfigurationManager.AppSettings["CVV2Type"].ToString());
                    sPostString = BuildPost(sPostString, "CVV2Type", objBuyer.securityCode);

                    string sResultString = subXMLPostProcess(ConfigurationManager.AppSettings["EPNApiUrl"].ToString(), sPostString);
                    //UInvalid Card
                    string reponse = sResultString.Replace("/", "").Replace('"', ' ').Replace("<html>", "").Replace("<body>", "").Replace("</html>", "").Replace("</body>", "").Trim();
                    switch (reponse)
                    {
                        case "UInvalid Card": _messageToShow.Message = "Invalid Card Number !"; break;
                        default: _messageToShow.Message = "Success"; break;
                    }
                    if (_messageToShow.Message == "Success")
                    {
                        string type = string.Empty;
                        if (objBuyer.paymenttype == "PaymentPlan")
                        {
                             type = SavePaymentDetails(objBuyer.amount, "PaymentPlan");

                        }
                        else
                        {
                            type = SavePaymentDetails(objBuyer.amount, objBuyer.OrderId);
                        }
                        if (type == "PaymentPlan")
                        {
                            Message = "SuccessPaymenPlan";
                        }
                        else
                        {
                            Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
                            Message = "SuccessOneTime";

                        }

                    }
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BuyUpdatableReport(string inspectionOrderId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "SiteUser")
                {
                    BuyReports objBuyreports = new BuyReports();
                    objBuyreports = (from pay in context.EmailTemplates
                                     where pay.TemplateName == "IsUpdatable"
                                     select new BuyReports
                                     {
                                         Description = pay.TemplateDiscription,
                                         Amount = pay.Rate,
                                         Validity = pay.Validity
                                     }).FirstOrDefault();
                    ViewBag.ReportId = inspectionOrderId;
                    ViewBag.Amount = objBuyreports.Amount;
                    if (objBuyreports != null)
                    {

                        ViewBag.ShowVerificationNo = context.tblinspectionorder.FirstOrDefault(x => x.InspectionOrderDetailId == inspectionOrderId).IsUpdatable;
                        return View(objBuyreports);
                    }
                   
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string ePayment(string cardNumber, string expiryMonth, string expiryYear, string securityCode, string amount, string ReportID)
        {
            string sPostString = "";

            sPostString = BuildPost(sPostString, "ePNAccount", ConfigurationManager.AppSettings["ePNAccount"].ToString());
            sPostString = BuildPost(sPostString, "CardNo", cardNumber);
            sPostString = BuildPost(sPostString, "ExpMonth", expiryMonth);
            sPostString = BuildPost(sPostString, "ExpYear", expiryYear);
            sPostString = BuildPost(sPostString, "Total", amount);
            sPostString = BuildPost(sPostString, "Address", ConfigurationManager.AppSettings["Address"].ToString());
            sPostString = BuildPost(sPostString, "Zip", ConfigurationManager.AppSettings["Zip"].ToString());
            sPostString = BuildPost(sPostString, "RestrictKey", ConfigurationManager.AppSettings["RestrictKey"].ToString());
            sPostString = BuildPost(sPostString, "CVV2Type", ConfigurationManager.AppSettings["CVV2Type"].ToString());
            sPostString = BuildPost(sPostString, "CVV2Type", securityCode);

            string sResultString = subXMLPostProcess(ConfigurationManager.AppSettings["EPNApiUrl"].ToString(), sPostString);
            //UInvalid Card
            string reponse = sResultString.Replace("/", "").Replace('"', ' ').Replace("<html>", "").Replace("<body>", "").Replace("</html>", "").Replace("</body>", "").Trim();
            switch (reponse)
            {
                case "UInvalid Card": _messageToShow.Message = "Invalid Card Number !"; break;
                default: _messageToShow.Message = "Success"; break;
            }
            return _messageToShow.Message;
        
        }

        public JsonResult UpdatableReprotPayment(string cardNumber, string expiryMonth, string expiryYear, string securityCode, string amount, string ReportID,string VerificationCode)
        {
            try
            {
                bool verificationResult = true;
                bool? IsReportUpdatable = context.tblinspectionorder.FirstOrDefault(x => x.InspectionOrderDetailId == ReportID).IsUpdatable;
                if ((bool)IsReportUpdatable)
                     verificationResult = CheckVerificationNumber(ReportID, VerificationCode);
                
                if (verificationResult == true)
                {
                    string result = ePayment(cardNumber, expiryMonth, expiryYear, securityCode, amount, ReportID);
                    if (result == "Success")
                    {
                        string type = SaveUpdatableReportPaymentDetails(amount, ReportID);
                        if (type == "Success")
                        {
                            _messageToShow.Message = "Success";
                        }
                    }
                }
                else
                {
                    _messageToShow.Message = "NotMatched";
                }
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
            }

            return Json(_messageToShow, JsonRequestBehavior.AllowGet);
        }

        private string SaveUpdatableReportPaymentDetails(string amount, string ReportID)
        {
            try
            {
               
                string Type = string.Empty;
                SpectorMaxDAL.payment userPayment = new SpectorMaxDAL.payment();
                userPayment.PaymentID = Guid.NewGuid().ToString();
                userPayment.UserID = Sessions.LoggedInUser.UserId;
                userPayment.ReportID = ReportID;
                userPayment.PaymentStatus = "Success";
                userPayment.PaymentDate = DateTime.Now;
                userPayment.PaymentAmount = float.Parse(amount); ;
                userPayment.PaymentType = "UpdateReport";
                userPayment.PaymentDate = System.DateTime.Now;
                Random random = new Random();
                int num = random.Next(1000000);
                
                var MaxInvoiceNo = context.Payments.Max(x => x.InvoiceNo);
                if (MaxInvoiceNo == null || Convert.ToInt32(MaxInvoiceNo) == 0)
                {
                    userPayment.InvoiceNo = 2000;
                }
                else
                {
                    userPayment.InvoiceNo = Convert.ToInt32(MaxInvoiceNo) + 1;
                }
                userPayment.CreatedDate = DateTime.Now;
                context.Payments.Add(userPayment);
                context.tblinspectionorder.FirstOrDefault(x => x.InspectionOrderDetailId == ReportID).IsVerified = true;
                tblinspectionorder objtblInspectionOrder = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId==ReportID).FirstOrDefault();
                if (objtblInspectionOrder != null)
                {
                    objtblInspectionOrder.IsUpdatable = true;
                }
                context.SaveChanges();
                return "Success";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckVerificationNumber(string ReportID, string VerificationCode)
        {
            try
            {
                tblinspectionorder VerificationNo = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == ReportID).FirstOrDefault();
                if (VerificationNo != null)
                {
                    if (VerificationNo.VerificationNumber == VerificationCode)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult VerifyReportno(string Code, string ReportId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "SiteUser")
                {
                    bool result = CheckVerificationNumber(ReportId, Code);
                    if (result)
                    {
                        tblinspectionorder objInspectionOrder = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == ReportId).FirstOrDefault();
                        if (objInspectionOrder != null)
                        {
                            objInspectionOrder.IsVerified = true;
                            context.SaveChanges();
                        }
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }
    }
}