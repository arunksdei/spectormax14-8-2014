using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;

namespace SpectorMax.Controllers
{   
    public class AgentLibrariesController : Controller
    {
        private SpectorMaxContext context = new SpectorMaxContext();

        //
        // GET: /AgentLibraries/

        public ViewResult Index()
        {
            return View(context.AgentLibraries.ToList());
        }

        //
        // GET: /AgentLibraries/Details/5

        public ViewResult Details(string id)
        {
            agentlibrary agentlibrary = context.AgentLibraries.Single(x => x.AgentID == id);
            return View(agentlibrary);
        }

        //
        // GET: /AgentLibraries/Create

        public ActionResult Create()
        {
            ViewBag.PossibleInspectionOrders = context.InspectionOrders;
            return View();
        } 

        //
        // POST: /AgentLibraries/Create

        [HttpPost]
        public ActionResult Create(agentlibrary agentlibrary)
        {
            if (ModelState.IsValid)
            {
                context.AgentLibraries.Add(agentlibrary);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.PossibleInspectionOrders = context.InspectionOrders;
            return View(agentlibrary);
        }
        
        //
        // GET: /AgentLibraries/Edit/5
 
        public ActionResult Edit(string id)
        {
            agentlibrary agentlibrary = context.AgentLibraries.Single(x => x.AgentID == id);
            ViewBag.PossibleInspectionOrders = context.InspectionOrders;
            return View(agentlibrary);
        }

        //
        // POST: /AgentLibraries/Edit/5

        [HttpPost]
        public ActionResult Edit(agentlibrary agentlibrary)
        {
            if (ModelState.IsValid)
            {
                context.Entry(agentlibrary).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PossibleInspectionOrders = context.InspectionOrders;
            return View(agentlibrary);
        }

        //
        // GET: /AgentLibraries/Delete/5
 
        public ActionResult Delete(string id)
        {
            agentlibrary agentlibrary = context.AgentLibraries.Single(x => x.AgentID == id);
            return View(agentlibrary);
        }

        //
        // POST: /AgentLibraries/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            agentlibrary agentlibrary = context.AgentLibraries.Single(x => x.AgentID == id);
            context.AgentLibraries.Remove(agentlibrary);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}