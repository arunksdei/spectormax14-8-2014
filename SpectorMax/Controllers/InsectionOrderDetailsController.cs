using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;
using SpectorMax.Entities.Classes;
using System.Data.Objects;

namespace SpectorMax.Controllers
{
    public class InsectionOrderDetailsController : BaseController
    {
        #region pivate member
        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _messageToShow = new MessageToShow();

        #endregion
        //
        // GET: /InsectionOrderDetails/

        public ViewResult Index()
        {
            return View(context.InsectionOrderDetails.ToList());
        }


        /// <summary>
        /// POST:  Actoion for Create InspectionOrder 
        /// </summary>
        /// <param name="insectionOrderDetailPoco"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(InspectionOrderDetailPoco insectionOrderDetailPoco)
        {
            // if (ModelState.IsValid)
            {
                try
                {
                    #region Update
                    if (insectionOrderDetailPoco.InspectionOrderId != null)
                    {
                        insectionorderdetail insectionOrderDetail = new SpectorMaxDAL.insectionorderdetail();
                        inspectionorder objInspectionOrder;

                        // getting Property's Owner information
                        insectionOrderDetail = context.InsectionOrderDetails.FirstOrDefault(x => x.InspectionOrderId == insectionOrderDetailPoco.InspectionOrderId);
                        insectionOrderDetail.MainDate1 = Convert.ToDateTime(insectionOrderDetailPoco.DatetoDisplayMain1);
                        insectionOrderDetail.MainDate2 = Convert.ToDateTime(insectionOrderDetailPoco.DatetoDisplay);
                        insectionOrderDetail.ScheduleInspectionDate = Convert.ToDateTime(insectionOrderDetailPoco.ScheduleInspectionDate1);
                        insectionOrderDetail.OwnerFirstName = insectionOrderDetailPoco.OwnerFirstName;
                        insectionOrderDetail.OwnerLastName = insectionOrderDetailPoco.OwnerLastName;
                        insectionOrderDetail.StreetAddess = insectionOrderDetailPoco.StreetAddess;
                        insectionOrderDetail.City = insectionOrderDetailPoco.City;
                        insectionOrderDetail.State = insectionOrderDetailPoco.State;
                        insectionOrderDetail.Zip = insectionOrderDetailPoco.Zip;
                        insectionOrderDetail.EmailAddress = insectionOrderDetailPoco.EmailAddress;
                        insectionOrderDetail.PhoneDay = insectionOrderDetailPoco.PhoneDay;
                        insectionOrderDetail.PhoneDay1 = insectionOrderDetailPoco.PhoneDay1;
                        insectionOrderDetail.PhoneEve = insectionOrderDetailPoco.PhoneEve;
                        insectionOrderDetail.PhoneEve1 = insectionOrderDetailPoco.PhoneEve1;
                        insectionOrderDetail.Other = insectionOrderDetailPoco.Other;
                        insectionOrderDetail.Other1 = insectionOrderDetailPoco.Other1;
                        insectionOrderDetail.ContractClient = insectionOrderDetailPoco.ContractClient;
                        insectionOrderDetail.SendCopy = insectionOrderDetailPoco.SendCopy;


                        insectionOrderDetail.PropertyDescription = insectionOrderDetailPoco.PropertyDescription;
                        insectionOrderDetail.PropertyUnit = insectionOrderDetailPoco.PropertyUnit;
                        insectionOrderDetail.PropertySquareFootage = insectionOrderDetailPoco.PropertySquareFootage;
                        insectionOrderDetail.PropertyAddition = insectionOrderDetailPoco.PropertyAddition;
                        insectionOrderDetail.PropertyBedRoom = insectionOrderDetailPoco.PropertyBedRoom;
                        insectionOrderDetail.PropertyBaths = insectionOrderDetailPoco.PropertyBaths;
                        insectionOrderDetail.PropertyEstimatedMonthlyUtilites = insectionOrderDetailPoco.PropertyEstimatedMonthlyUtilites;
                        insectionOrderDetail.PropertyHomeAge = insectionOrderDetailPoco.PropertyHomeAge;
                        insectionOrderDetail.PropertyRoofAge = insectionOrderDetailPoco.PropertyRoofAge;
                        insectionOrderDetail.SpecialInstructions = insectionOrderDetailPoco.SpecialInstructions + insectionOrderDetailPoco.SpecialInstructions1;
                        insectionOrderDetail.ConfirmationPriorWithBuyer = insectionOrderDetailPoco.ConfirmationPriorWithBuyer;
                        insectionOrderDetail.ConfirmationPriorWithBuyersAgent = insectionOrderDetailPoco.ConfirmationPriorWithBuyersAgent;
                        insectionOrderDetail.ConfirmationPriorWithSellersAgent = insectionOrderDetailPoco.ConfirmationPriorWithSellersAgent;

                        insectionOrderDetail.IsRequestedByEmailOther = insectionOrderDetailPoco.IsRequestedByEmailOther;
                        insectionOrderDetail.RequestedByEmailSend = insectionOrderDetailPoco.RequestedByEmailSend;
                        insectionOrderDetail.RequestedByEmailOther = insectionOrderDetailPoco.RequestedByEmailOther;
                        insectionOrderDetail.RequestedByEmailOwner = insectionOrderDetailPoco.RequestedByEmailOwner;
                        insectionOrderDetail.RequestedByEmailOwnersAgent = insectionOrderDetailPoco.RequestedByEmailOwnersAgent;
                        insectionOrderDetail.RequestedByEmailContractClient = insectionOrderDetailPoco.RequestedByEmailContractClient;

                        insectionOrderDetail.MainClient1 = insectionOrderDetailPoco.MainClient1;
                        insectionOrderDetail.MainClient2 = insectionOrderDetailPoco.MainClient2;

                        if (Sessions.LoggedInUser.AdditionalTestingPaymentStatus == "Success")
                        {
                            insectionOrderDetail.IsAdditionalTest1 = insectionOrderDetailPoco.IsAdditionalTest1;
                            insectionOrderDetail.IsAdditionalTest2 = insectionOrderDetailPoco.IsAdditionalTest2;
                            insectionOrderDetail.IsAdditionalTest3 = insectionOrderDetailPoco.IsAdditionalTest3;
                            insectionOrderDetail.IsAdditionalTest4 = insectionOrderDetailPoco.IsAdditionalTest4;
                            insectionOrderDetail.IsAdditionalTest5 = insectionOrderDetailPoco.IsAdditionalTest5;
                            insectionOrderDetail.IsAdditionalTest6 = insectionOrderDetailPoco.IsAdditionalTest6;
                            insectionOrderDetail.IsAdditionalTest7 = insectionOrderDetailPoco.IsAdditionalTest7;
                            insectionOrderDetail.IsAdditionalTest8 = insectionOrderDetailPoco.IsAdditionalTest8;
                            insectionOrderDetail.TotalAdditionalTestingAmount = insectionOrderDetailPoco.TotalAdditionalTestingAmount;
                        }
                        else
                        {
                            insectionOrderDetail.IsAdditionalTest1 = false;
                            insectionOrderDetail.IsAdditionalTest2 = false;
                            insectionOrderDetail.IsAdditionalTest3 = false;
                            insectionOrderDetail.IsAdditionalTest4 = false;
                            insectionOrderDetail.IsAdditionalTest5 = false;
                            insectionOrderDetail.IsAdditionalTest6 = false;
                            insectionOrderDetail.IsAdditionalTest7 = false;
                            insectionOrderDetail.IsAdditionalTest8 = false;
                            insectionOrderDetail.TotalAdditionalTestingAmount = "0";
                        }


                        insectionOrderDetail.ModifiedDate = DateTime.Now;//.ToUniversalTime();
                        //insectionOrderDetail.CreatedDate = DateTime.Now.ToUniversalTime();


                        // Inserting data in InspectionOrder table.
                        objInspectionOrder = context.InspectionOrders.FirstOrDefault(x => x.InspectionOrderID == insectionOrderDetailPoco.InspectionOrderId);
                        objInspectionOrder.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
                        objInspectionOrder.InspectionStatus = "Pending";
                        //string assignedInspectorId = context.UserInfoes.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip) != null ? context.UserInfoes.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip).ID : "";


                        objInspectionOrder.CreatedDate = DateTime.Now;
                        objInspectionOrder.AssignedByAdmin = false;
                        objInspectionOrder.IsAcceptedOrRejected = true;
                        objInspectionOrder.Comment = "";
                        objInspectionOrder.ScheduleDate = insectionOrderDetailPoco.ScheduleInspectionDate;
                        objInspectionOrder.ModifiedDate = DateTime.Now;

                        /*Change for is updatable*/

                        objInspectionOrder.IsUpdatable = insectionOrderDetail.IsAdditionalTest2;
                        context.SaveChanges();

                        context.SaveChanges();
                        // Inserting buyer's information
                        agentlibrary AgentLibraryinfo;
                        if (insectionOrderDetailPoco.buyerAgent != null)
                        {
                            AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                            AgentLibraryinfo.AgentID = Guid.NewGuid().ToString();
                            AgentLibraryinfo.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
                            AgentLibraryinfo.AgentType = "2";
                            AgentLibraryinfo.AgentFirstName = insectionOrderDetailPoco.buyerAgent.BuyerAgentFirstName;
                            AgentLibraryinfo.AgentLastName = insectionOrderDetailPoco.buyerAgent.BuyerAgentLastName;
                            AgentLibraryinfo.AgentOffice = insectionOrderDetailPoco.buyerAgent.BuyerAgentOffice;
                            AgentLibraryinfo.AgentPhoneDay = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay;
                            AgentLibraryinfo.AgentPhoneDay1 = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay1;
                            AgentLibraryinfo.AgentPhoneEve = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneEve;
                            AgentLibraryinfo.AgentPhoneEve1 = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneEve1;
                            AgentLibraryinfo.AgentPhoneOther = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneOther;
                            AgentLibraryinfo.AgentPhoneOther1 = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneOther1;
                            AgentLibraryinfo.AgentStreetAddress = insectionOrderDetailPoco.buyerAgent.BuyerAgentStreetAddress;
                        //    AgentLibraryinfo.AgentCity = insectionOrderDetailPoco.buyerAgent.BuyerAgentCity;
                        //    AgentLibraryinfo.AgentState = insectionOrderDetailPoco.buyerAgent.BuyerAgentState;
                            AgentLibraryinfo.AgentZip = insectionOrderDetailPoco.buyerAgent.BuyerAgentZip;
                            AgentLibraryinfo.AgentSend = insectionOrderDetailPoco.buyerAgent.AgentBuyerSend;
                            AgentLibraryinfo.AgentEmailAddress = insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress;
                            AgentLibraryinfo.AgentEmailAddress = insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress1;
                            context.AgentLibraries.Add(AgentLibraryinfo);
                            context.SaveChanges();
                        }

                        // Inserting sellers's information
                        if (insectionOrderDetailPoco.sellerAgent != null)
                        {
                            AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                            AgentLibraryinfo.AgentID = Guid.NewGuid().ToString();
                            AgentLibraryinfo.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
                            AgentLibraryinfo.AgentType = "1";
                            AgentLibraryinfo.AgentFirstName = insectionOrderDetailPoco.sellerAgent.SellerAgentFirstName;
                            AgentLibraryinfo.AgentLastName = insectionOrderDetailPoco.sellerAgent.SellerAgentLastName;
                            AgentLibraryinfo.AgentOffice = insectionOrderDetailPoco.sellerAgent.SellerAgentOffice;
                            AgentLibraryinfo.AgentPhoneDay = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay;
                            AgentLibraryinfo.AgentPhoneDay1 = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay1;
                            AgentLibraryinfo.AgentPhoneEve = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneEve;
                            AgentLibraryinfo.AgentPhoneEve1 = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneEve1;
                            AgentLibraryinfo.AgentPhoneOther = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneOther;
                            AgentLibraryinfo.AgentPhoneOther1 = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneOther1;
                            AgentLibraryinfo.AgentStreetAddress = insectionOrderDetailPoco.sellerAgent.SellerAgentStreetAddress;
                         //   AgentLibraryinfo.AgentCity = insectionOrderDetailPoco.sellerAgent.SellerAgentCity;
                         //   AgentLibraryinfo.AgentState = insectionOrderDetailPoco.sellerAgent.SellerAgentState;
                            AgentLibraryinfo.AgentZip = insectionOrderDetailPoco.sellerAgent.SellerAgentZip;
                            AgentLibraryinfo.AgentEmailAddress = insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress;
                            AgentLibraryinfo.AgentEmailAddress = insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress1;
                            AgentLibraryinfo.AgentSend = insectionOrderDetailPoco.sellerAgent.AgentSellerSend;
                            context.AgentLibraries.Add(AgentLibraryinfo);
                            context.SaveChanges();


                            SendMail(insectionOrderDetailPoco);



                            var States = context.ZipCode.Select(m => new SelectListItem() { Text = m.State, Value = m.State }).Distinct();
                            List<SelectListItem> items = new List<SelectListItem>();
                            List<SelectListItem> objCityList = new List<SelectListItem>();
                            List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                            SelectListItem objSelectListItem = new SelectListItem();
                            objSelectListItem.Text = "--Select--";
                            objSelectListItem.Value = "--Select--";
                            items.Add(objSelectListItem);
                            objCityList.Add(objSelectListItem);
                            objZipCodeList.Add(objSelectListItem);
                            //foreach (var t in States)
                            //{
                            //    SelectListItem s = new SelectListItem();
                            //    s.Text = t.ToString();
                            //    s.Value = t.ToString();
                            //    items.Add(s);
                            //}
                            insectionOrderDetailPoco.StatesList = States;
                            insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                            insectionOrderDetailPoco.CityList = objCityList;
                        }
                        _messageToShow.Message = "Buyer Request Saved";
                        ViewBag.Message = _messageToShow.Message;


                    }
                    #endregion

                    #region Create
                    else
                    {
                       
                        insectionorderdetail insectionOrderDetail = new SpectorMaxDAL.insectionorderdetail();
                        inspectionorder objInspectionOrder;
                        insectionOrderDetailPoco.MainDate1 = Convert.ToDateTime(insectionOrderDetailPoco.DatetoDisplayMain1);
                        insectionOrderDetailPoco.MainDate2 = Convert.ToDateTime(insectionOrderDetailPoco.DatetoDisplay);
                        insectionOrderDetailPoco.ScheduleInspectionDate = Convert.ToDateTime(insectionOrderDetailPoco.ScheduleInspectionDate1);



                        // Inserting Property's Owner information
                        insectionOrderDetail.InspectionOrderId = Guid.NewGuid().ToString();
                        insectionOrderDetail.OwnerFirstName = insectionOrderDetailPoco.OwnerFirstName;
                        insectionOrderDetail.OwnerLastName = insectionOrderDetailPoco.OwnerLastName;
                        insectionOrderDetail.StreetAddess = insectionOrderDetailPoco.StreetAddess;
                        insectionOrderDetail.City = insectionOrderDetailPoco.City;
                        insectionOrderDetail.State = insectionOrderDetailPoco.State;
                        insectionOrderDetail.Zip = insectionOrderDetailPoco.Zip;
                        insectionOrderDetail.EmailAddress = insectionOrderDetailPoco.EmailAddress;
                        insectionOrderDetail.PhoneDay = insectionOrderDetailPoco.PhoneDay;
                        insectionOrderDetail.PhoneDay1 = insectionOrderDetailPoco.PhoneDay1;
                        insectionOrderDetail.PhoneEve = insectionOrderDetailPoco.PhoneEve;
                        insectionOrderDetail.PhoneEve1 = insectionOrderDetailPoco.PhoneEve1;
                        insectionOrderDetail.Other = insectionOrderDetailPoco.Other;
                        insectionOrderDetail.Other1 = insectionOrderDetailPoco.Other1;
                        insectionOrderDetail.ContractClient = insectionOrderDetailPoco.ContractClient;
                        insectionOrderDetail.SendCopy = insectionOrderDetailPoco.SendCopy;

                        // Inserting Property information
                        insectionOrderDetail.PropertyAddress = insectionOrderDetailPoco.PropertyAddress;
                        insectionOrderDetail.PropertyAddressCity = insectionOrderDetailPoco.PropertyAddressCity;
                        insectionOrderDetail.PropertyAddressCrossStreet = insectionOrderDetailPoco.PropertyAddressCrossStreet;
                        insectionOrderDetail.PropertyAddressState = insectionOrderDetailPoco.PropertyAddressState;
                        insectionOrderDetail.PropertyAddressZip = insectionOrderDetailPoco.PropertyAddressZip;
                        insectionOrderDetail.PropertyAddressTenantName = insectionOrderDetailPoco.PropertyAddressTenantName;
                        insectionOrderDetail.PropertyAddressSubDivison = insectionOrderDetailPoco.PropertyAddressSubDivison;
                        insectionOrderDetail.PropertyAddressMapCoord = insectionOrderDetailPoco.PropertyAddressMapCoord;
                        insectionOrderDetail.PropertyAddressPhoneDay = insectionOrderDetailPoco.PropertyAddressPhoneDay;
                        insectionOrderDetail.PropertyAddressPhoneDay1 = insectionOrderDetailPoco.PropertyAddressPhoneDay1;
                        insectionOrderDetail.PropertyAddressPhoneEve = insectionOrderDetailPoco.PropertyAddressPhoneEve;
                        insectionOrderDetail.PropertyAddressPhoneEve1 = insectionOrderDetailPoco.PropertyAddressPhoneEve1;
                        insectionOrderDetail.PropertyAddressDirections = insectionOrderDetailPoco.PropertyAddressDirections;
                        insectionOrderDetail.PropertyDescription = insectionOrderDetailPoco.PropertyDescription;
                        insectionOrderDetail.PropertyUnit = insectionOrderDetailPoco.PropertyUnit;
                        insectionOrderDetail.PropertySquareFootage = insectionOrderDetailPoco.PropertySquareFootage;
                        insectionOrderDetail.PropertyAddition = insectionOrderDetailPoco.PropertyAddition;
                        insectionOrderDetail.PropertyBedRoom = insectionOrderDetailPoco.PropertyBedRoom;
                        insectionOrderDetail.PropertyBaths = insectionOrderDetailPoco.PropertyBaths;
                        insectionOrderDetail.PropertyEstimatedMonthlyUtilites = insectionOrderDetailPoco.PropertyEstimatedMonthlyUtilites;
                        insectionOrderDetail.PropertyHomeAge = insectionOrderDetailPoco.PropertyHomeAge;
                        insectionOrderDetail.PropertyRoofAge = insectionOrderDetailPoco.PropertyRoofAge;
                        insectionOrderDetail.SpecialInstructions = insectionOrderDetailPoco.SpecialInstructions + insectionOrderDetailPoco.SpecialInstructions1;
                        insectionOrderDetail.ConfirmationPriorWithBuyer = insectionOrderDetailPoco.ConfirmationPriorWithBuyer;
                        insectionOrderDetail.ConfirmationPriorWithBuyersAgent = insectionOrderDetailPoco.ConfirmationPriorWithBuyersAgent;
                        insectionOrderDetail.ConfirmationPriorWithSellersAgent = insectionOrderDetailPoco.ConfirmationPriorWithSellersAgent;
                        // Inserting Requested by information
                        insectionOrderDetail.RequestedBy = insectionOrderDetailPoco.RequestedBy;
                        insectionOrderDetail.RequestedByEmailPhoneDay = insectionOrderDetailPoco.RequestedByEmailPhoneDay;
                        insectionOrderDetail.RequestedByEmailPhoneDay1 = insectionOrderDetailPoco.RequestedByEmailPhoneDay1;
                        insectionOrderDetail.RequestedByEmailPhoneEve = insectionOrderDetailPoco.RequestedByEmailPhoneEve;
                        insectionOrderDetail.RequestedByEmailPhoneEve1 = insectionOrderDetailPoco.RequestedByEmailPhoneEve1;
                        insectionOrderDetail.RequestedByEmailPhoneOther = insectionOrderDetailPoco.RequestedByEmailPhoneOther;
                        insectionOrderDetail.RequestedByEmailPhoneOther1 = insectionOrderDetailPoco.RequestedByEmailPhoneOther1;
                        insectionOrderDetail.RequestedByStreetAddress = insectionOrderDetailPoco.RequestedByStreetAddress;
                        insectionOrderDetail.RequestedByCity = insectionOrderDetailPoco.RequestedByCity;
                        insectionOrderDetail.RequestedByState = insectionOrderDetailPoco.RequestedByState;
                        insectionOrderDetail.RequestedByZip = insectionOrderDetailPoco.RequestedByZip;
                        insectionOrderDetail.RequestedByEmailAddress1 = insectionOrderDetailPoco.RequestedByEmailAddress1;
                        insectionOrderDetail.IsRequestedByEmailOther = insectionOrderDetailPoco.IsRequestedByEmailOther;
                        insectionOrderDetail.RequestedByEmailSend = insectionOrderDetailPoco.RequestedByEmailSend;
                        insectionOrderDetail.RequestedByEmailOther = insectionOrderDetailPoco.RequestedByEmailOther;
                        insectionOrderDetail.RequestedByEmailOwner = insectionOrderDetailPoco.RequestedByEmailOwner;
                        insectionOrderDetail.RequestedByEmailOwnersAgent = insectionOrderDetailPoco.RequestedByEmailOwnersAgent;
                        insectionOrderDetail.RequestedByEmailContractClient = insectionOrderDetailPoco.RequestedByEmailContractClient;

                        insectionOrderDetail.MainClient1 = insectionOrderDetailPoco.MainClient1;
                        insectionOrderDetail.MainClient2 = insectionOrderDetailPoco.MainClient2;
                        insectionOrderDetail.MainDate1 = insectionOrderDetailPoco.MainDate1;
                        insectionOrderDetail.MainDate2 = insectionOrderDetailPoco.MainDate2;
                        insectionOrderDetail.ScheduleInspectionDate = insectionOrderDetailPoco.ScheduleInspectionDate;
                        if (Sessions.LoggedInUser.AdditionalTestingPaymentStatus == "Success")
                        {
                            insectionOrderDetail.IsAdditionalTest1 = insectionOrderDetailPoco.IsAdditionalTest1;
                            insectionOrderDetail.IsAdditionalTest2 = insectionOrderDetailPoco.IsAdditionalTest2;
                            insectionOrderDetail.IsAdditionalTest3 = insectionOrderDetailPoco.IsAdditionalTest3;
                            insectionOrderDetail.IsAdditionalTest4 = insectionOrderDetailPoco.IsAdditionalTest4;
                            insectionOrderDetail.IsAdditionalTest5 = insectionOrderDetailPoco.IsAdditionalTest5;
                            insectionOrderDetail.IsAdditionalTest6 = insectionOrderDetailPoco.IsAdditionalTest6;
                            insectionOrderDetail.IsAdditionalTest7 = insectionOrderDetailPoco.IsAdditionalTest7;
                            insectionOrderDetail.IsAdditionalTest8 = insectionOrderDetailPoco.IsAdditionalTest8;
                            insectionOrderDetail.TotalAdditionalTestingAmount = insectionOrderDetailPoco.TotalAdditionalTestingAmount;
                        }
                        else
                        {
                            insectionOrderDetail.IsAdditionalTest1 = false;
                            insectionOrderDetail.IsAdditionalTest2 = false;
                            insectionOrderDetail.IsAdditionalTest3 = false;
                            insectionOrderDetail.IsAdditionalTest4 = false;
                            insectionOrderDetail.IsAdditionalTest5 = false;
                            insectionOrderDetail.IsAdditionalTest6 = false;
                            insectionOrderDetail.IsAdditionalTest7 = false;
                            insectionOrderDetail.IsAdditionalTest8 = false;
                            insectionOrderDetail.TotalAdditionalTestingAmount = "0";
                        }
                        insectionOrderDetail.ModifiedDate = DateTime.Now;//.ToUniversalTime();
                        insectionOrderDetail.CreatedDate = DateTime.Now;//.ToUniversalTime();

                        // Inserting data in InspectionOrder table.
                        objInspectionOrder = new inspectionorder();
                        objInspectionOrder.InspectionId = Guid.NewGuid().ToString();
                        objInspectionOrder.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
                        objInspectionOrder.InspectionStatus = "Pending";
                        //string assignedInspectorId = context.UserInfoes.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip) != null ? context.UserInfoes.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip).ID : "";


                        /*Report ID Logic*/
                        string assignedInspectorId = context.ZipCodeAssigned.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip) != null ? context.ZipCodeAssigned.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip).InspectorID : "";
                        //var countReports = "";
                        int TotalInspectorReportCount = 0;
                        string UniqueId = "Empty";
                        if (assignedInspectorId != "")
                        {
                            var countReports = (from IO in context.InspectionOrders
                                                join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                                where IO.InspectorID == assignedInspectorId && IOD.PropertyAddressState==insectionOrderDetailPoco.PropertyAddressState
                                                select IO.InspectorID);
                            TotalInspectorReportCount = countReports.Count();
                            UniqueId = insectionOrderDetailPoco.PropertyAddressState+context.InspectorDetail.Where(x => x.UserID == assignedInspectorId).Select(y => y.InspectorNo).FirstOrDefault() + (TotalInspectorReportCount + 1).ToString();
                        }

                        /*End*/
                        if (assignedInspectorId == "")
                        {
                            notificationdetail objNotification = new notificationdetail();
                            objNotification.NotifcationFrom = Sessions.LoggedInUser.UserId;
                            objNotification.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                            objNotification.NotificationTypeID = 2;
                            objNotification.IsRead = false;
                            objNotification.CreatedDate = System.DateTime.Now;
                            objNotification.RequestID = insectionOrderDetail.InspectionOrderId;
                            context.NotificationDetail.Add(objNotification);
                            context.SaveChanges();
                        }
                        objInspectionOrder.InspectorID = assignedInspectorId;
                        objInspectionOrder.CreatedDate = DateTime.Now;
                        objInspectionOrder.AssignedByAdmin = false;
                        objInspectionOrder.Comment = "";
                        objInspectionOrder.RequesterID = Sessions.LoggedInUser.UserId;
                        objInspectionOrder.ScheduleDate = insectionOrderDetailPoco.ScheduleInspectionDate;
                        objInspectionOrder.ModifiedDate = DateTime.Now;

                        /*change for is updatable*/

                        objInspectionOrder.IsUpdatable = insectionOrderDetail.IsAdditionalTest2;

                        objInspectionOrder.UniqueID = UniqueId;
                        context.InspectionOrders.Add(objInspectionOrder);
                        context.SaveChanges();

                        context.InsectionOrderDetails.Add(insectionOrderDetail);
                        context.SaveChanges();

                        /*Update Payment Table*/
                        if (Sessions.LoggedInUser.AdditionalTestingPaymentStatus == "Success")
                        {
                            string Paymentid = Session["PaymentID"].ToString();
                            payment objpayment = context.Payments.Where(x => x.PaymentID == Paymentid).FirstOrDefault();
                            if (objpayment != null)
                            {
                                objpayment.ReportID = insectionOrderDetail.InspectionOrderId;
                                context.SaveChanges();
                                Sessions.LoggedInUser.AdditionalTestingPaymentStatus = string.Empty;
                            }
                        }

                        /*End*/

                        // Inserting buyer's information
                        agentlibrary AgentLibraryinfo;
                        if (insectionOrderDetailPoco.buyerAgent != null)
                        {
                            AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                            AgentLibraryinfo.AgentID = Guid.NewGuid().ToString();
                            AgentLibraryinfo.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
                            AgentLibraryinfo.AgentType = "2";
                            AgentLibraryinfo.AgentFirstName = insectionOrderDetailPoco.buyerAgent.BuyerAgentFirstName;
                            AgentLibraryinfo.AgentLastName = insectionOrderDetailPoco.buyerAgent.BuyerAgentLastName;
                            AgentLibraryinfo.AgentOffice = insectionOrderDetailPoco.buyerAgent.BuyerAgentOffice;
                            AgentLibraryinfo.AgentPhoneDay = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay;
                            AgentLibraryinfo.AgentPhoneDay1 = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay1;
                            AgentLibraryinfo.AgentPhoneEve = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneEve;
                            AgentLibraryinfo.AgentPhoneEve1 = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneEve1;
                            AgentLibraryinfo.AgentPhoneOther = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneOther;
                            AgentLibraryinfo.AgentPhoneOther1 = insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneOther1;
                            AgentLibraryinfo.AgentStreetAddress = insectionOrderDetailPoco.buyerAgent.BuyerAgentStreetAddress;
                        //    AgentLibraryinfo.AgentCity = insectionOrderDetailPoco.buyerAgent.BuyerAgentCity;
                       //     AgentLibraryinfo.AgentState = insectionOrderDetailPoco.buyerAgent.BuyerAgentState;
                            AgentLibraryinfo.AgentZip = insectionOrderDetailPoco.buyerAgent.BuyerAgentZip;
                            AgentLibraryinfo.AgentSend = insectionOrderDetailPoco.buyerAgent.AgentBuyerSend;
                            AgentLibraryinfo.AgentEmailAddress = insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress;
                            AgentLibraryinfo.AgentEmailAddress = insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress1;
                            context.AgentLibraries.Add(AgentLibraryinfo);
                            context.SaveChanges();
                        }

                        // Inserting sellers's information
                        if (insectionOrderDetailPoco.sellerAgent != null)
                        {
                            AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                            AgentLibraryinfo.AgentID = Guid.NewGuid().ToString();
                            AgentLibraryinfo.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
                            AgentLibraryinfo.AgentType = "1";
                            AgentLibraryinfo.AgentFirstName = insectionOrderDetailPoco.sellerAgent.SellerAgentFirstName;
                            AgentLibraryinfo.AgentLastName = insectionOrderDetailPoco.sellerAgent.SellerAgentLastName;
                            AgentLibraryinfo.AgentOffice = insectionOrderDetailPoco.sellerAgent.SellerAgentOffice;
                            AgentLibraryinfo.AgentPhoneDay = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay;
                            AgentLibraryinfo.AgentPhoneDay1 = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay1;
                            AgentLibraryinfo.AgentPhoneEve = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneEve;
                            AgentLibraryinfo.AgentPhoneEve1 = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneEve1;
                            AgentLibraryinfo.AgentPhoneOther = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneOther;
                            AgentLibraryinfo.AgentPhoneOther1 = insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneOther1;
                            AgentLibraryinfo.AgentStreetAddress = insectionOrderDetailPoco.sellerAgent.SellerAgentStreetAddress;
                       //     AgentLibraryinfo.AgentCity = insectionOrderDetailPoco.sellerAgent.SellerAgentCity;
                      //      AgentLibraryinfo.AgentState = insectionOrderDetailPoco.sellerAgent.SellerAgentState;
                            AgentLibraryinfo.AgentZip = insectionOrderDetailPoco.sellerAgent.SellerAgentZip;
                            AgentLibraryinfo.AgentEmailAddress = insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress;
                            AgentLibraryinfo.AgentEmailAddress = insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress1;
                            AgentLibraryinfo.AgentSend = insectionOrderDetailPoco.sellerAgent.AgentSellerSend;
                            context.AgentLibraries.Add(AgentLibraryinfo);
                            context.SaveChanges();
                            SendMail(insectionOrderDetailPoco);


                            var States = context.ZipCode.Select(x => x.State).Distinct();
                            List<SelectListItem> items = new List<SelectListItem>();
                            List<SelectListItem> objCityList = new List<SelectListItem>();
                            List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                            SelectListItem objSelectListItem = new SelectListItem();
                            objSelectListItem.Text = "--Select--";
                            objSelectListItem.Value = "--Select--";
                            items.Add(objSelectListItem);
                            objCityList.Add(objSelectListItem);
                            objZipCodeList.Add(objSelectListItem);
                            foreach (var t in States)
                            {
                                SelectListItem s = new SelectListItem();
                                s.Text = t.ToString();
                                s.Value = t.ToString();
                                items.Add(s);
                            }
                            insectionOrderDetailPoco.StatesList = items;
                            insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                            insectionOrderDetailPoco.CityList = objCityList;
                        }
                        _messageToShow.Message = "Data Inserted Successfully";
                        ViewBag.Message = _messageToShow.Message;
                        //return RedirectToAction("SellerDashboard", "UserInfoes", new { message = _messageToShow.Message });
                        //return RedirectToAction("SellerDashboard", "UserInfoes");

                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    _messageToShow.Message = ex.Message.ToString();
                    ViewBag.Message = _messageToShow.Message;
                }

            }

            //var errors = ModelState.Values.SelectMany(x => x.Errors);
            ////return RedirectToAction("SellerDashboard", "UserInfoes");
            return View(insectionOrderDetailPoco);
        }

        public void SendMail(InspectionOrderDetailPoco insectionOrderDetailPoco)
        {
            try
            {
                if (insectionOrderDetailPoco.SendCopy || insectionOrderDetailPoco.buyerAgent.AgentBuyerSend || insectionOrderDetailPoco.sellerAgent.AgentSellerSend || insectionOrderDetailPoco.RequestedByEmailSend)
                {
                    #region Sending Email
                    EmailHelper email = new EmailHelper();

                    string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "SendInspectionCopy").TemplateDiscription;
                    emailBody = emailBody.Replace("{PropertyAddress}", insectionOrderDetailPoco.PropertyAddress);
                    emailBody = emailBody.Replace("{PropertyState}", insectionOrderDetailPoco.PropertyAddressState);
                    emailBody = emailBody.Replace("{PropertyCity}", insectionOrderDetailPoco.PropertyAddressCity);
                    emailBody = emailBody.Replace("{PropertyZip}", insectionOrderDetailPoco.PropertyAddressZip.ToString());
                    #endregion

                    if (insectionOrderDetailPoco.SendCopy)
                    {
                        string emailBodyOwenr = emailBody;
                        emailBodyOwenr = emailBodyOwenr.Replace("{firstname}", insectionOrderDetailPoco.OwnerFirstName);
                        emailBodyOwenr = emailBodyOwenr.Replace("{lastname}", insectionOrderDetailPoco.OwnerLastName);

                        bool status = email.SendEmail("SpectorMax", "Inspection Order Created", "", emailBodyOwenr, insectionOrderDetailPoco.EmailAddress);
                    }
                    if (insectionOrderDetailPoco.buyerAgent.AgentBuyerSend)
                    {
                        string emailBodyBuyer = emailBody;
                        string EmailTo = insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress + "," + insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress1;
                        emailBodyBuyer = emailBodyBuyer.Replace("{firstname}", insectionOrderDetailPoco.buyerAgent.BuyerAgentFirstName);
                        emailBodyBuyer = emailBodyBuyer.Replace("{lastname}", insectionOrderDetailPoco.buyerAgent.BuyerAgentLastName);
                        email.SendEmail("SpectorMax", "Inspection Order Created", "", emailBodyBuyer, insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress);
                        email.SendEmail("SpectorMax", "Inspection Order Created", "", emailBodyBuyer, insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress1);

                    }
                    if (insectionOrderDetailPoco.sellerAgent.AgentSellerSend)
                    {
                        string emailBodyBuyer = emailBody;
                        string EmailTo = insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress + ";" + insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress1;
                        emailBodyBuyer = emailBodyBuyer.Replace("{firstname}", insectionOrderDetailPoco.sellerAgent.SellerAgentFirstName);
                        emailBodyBuyer = emailBodyBuyer.Replace("{lastname}", insectionOrderDetailPoco.sellerAgent.SellerAgentLastName);
                        email.SendEmail("SpectorMax", "Inspection Order Created", "", emailBodyBuyer, insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress1);
                        email.SendEmail("SpectorMax", "Inspection Order Created", "", emailBodyBuyer, insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress);

                    }
                    if (insectionOrderDetailPoco.RequestedByEmailSend)
                    {
                        string emailBodyBuyer = emailBody;
                        emailBodyBuyer = emailBodyBuyer.Replace("{firstname}", insectionOrderDetailPoco.RequestedBy);
                        emailBodyBuyer = emailBodyBuyer.Replace("{lastname}", insectionOrderDetailPoco.RequestedLastName);
                        email.SendEmail("SpectorMax", "Inspection Order Created", "", emailBodyBuyer, insectionOrderDetailPoco.RequestedByEmailAddress1);
                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;            
            }
        }

        /// <summary>
        ///  GET: / method for getting ViewPreviousInspection Details for specific userid.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ViewResult Details(string userId)
        {
            //Note Use userid or any coresponding id for fetch detail       
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    int PageNumber = 0;
                    int PerPageRecord = 20;
                    bool islast = false;
                    PagingPoco pagingPoco = new PagingPoco();
                    //pagingPoco = PagingResult(PageNumber, PerPageRecord, islast, "", "");
                    var dataInsectionOrderDetails = (from k in context.InspectionOrders
                                                     join Ins in context.InsectionOrderDetails
                                                     on k.InspectionOrderID equals Ins.InspectionOrderId
                                                     join UI in context.UserInfoes
                                                     on k.InspectorID equals UI.ID into g
                                                     where k.RequesterID == Sessions.LoggedInUser.UserId
                                                     orderby Ins.ScheduleInspectionDate descending
                                                     select new ViewPreviousInspection
                                                     {
                                                         InspectionOrderId = k.InspectionOrderID,
                                                         OwnerFirstName = Ins.OwnerFirstName,
                                                         OwnerLastName = Ins.OwnerLastName,
                                                         InspectorDetails = g,
                                                         PropertyAddress = Ins.PropertyAddress,
                                                         PropertyAddressCity = Ins.PropertyAddressCity,
                                                         PropertyAddressState = Ins.PropertyAddressState,
                                                         ScheduleInspectionDate = Ins.ScheduleInspectionDate,
                                                         ReportStatus = k.InspectionStatus,
                                                         IsUpdatable = k.IsUpdatable
                                                     }).ToList();

                    pagingPoco.ObjInspectionOrderDetailsPaging = dataInsectionOrderDetails;
                    return View(pagingPoco);
                }
                else
                {
                    return View("~/Views/Home/Index.cshtml");
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// private Method for getting ViewPreviousInspection according to paging.
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PerPageRecord"></param>
        /// <param name="islast"></param>
        /// <returns></returns>
        private PagingPoco PagingResult(int PageNumber, int PerPageRecord, bool islast, string startDate, string endDate)
        {
            try
            {
                PagingPoco pagingPoco = new PagingPoco();
                long totalRecord = 0;
                int skiprecord = 0;
                int totalPage = 0;
                int totalPages = 0;
                decimal countpage = 0;
                var dataInsectionOrderDetails = (from k in context.InspectionOrders
                                                 join Ins in context.InsectionOrderDetails
                                                 on k.InspectionOrderID equals Ins.InspectionOrderId
                                                 join UI in context.UserInfoes
                                                 on k.InspectorID equals UI.ID into g
                                                 where k.RequesterID == Sessions.LoggedInUser.UserId
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = k.InspectionOrderID,
                                                     OwnerFirstName = Ins.OwnerFirstName,
                                                     OwnerLastName = Ins.OwnerLastName,
                                                     InspectorDetails = g,
                                                     PropertyAddress = Ins.PropertyAddress,
                                                     PropertyAddressCity = Ins.PropertyAddressCity,
                                                     PropertyAddressState = Ins.PropertyAddressState,
                                                     ScheduleInspectionDate = Ins.ScheduleInspectionDate,
                                                     ReportStatus = k.InspectionStatus,
                                                     IsUpdatable = k.IsUpdatable
                                                 }).ToList();

                totalRecord = dataInsectionOrderDetails.Count;
                countpage = Convert.ToDecimal(totalRecord % PerPageRecord);
                totalPage = (int)totalRecord / PerPageRecord;
                if (countpage > 0)
                    totalPage = totalPage + 1;
                if (islast == true)
                {
                    skiprecord = (totalPage - 1) * PerPageRecord;
                    PageNumber = totalPage;
                }
                else
                {
                    PageNumber = (PageNumber > totalPage ? totalPage : (PageNumber <= 0 ? 1 : PageNumber));
                    skiprecord = (PageNumber == 1 ? 0 : PageNumber - 1) * PerPageRecord;
                }
                totalPages = (int)totalPage;
                pagingPoco.objPaging = new Paging();
                pagingPoco.objPaging.TotalRecord = (int)totalRecord;
                pagingPoco.objPaging.PageNumber = PageNumber;
                pagingPoco.objPaging.PerPageRecord = PerPageRecord;
                pagingPoco.objPaging.TotalPage = totalPages;
                pagingPoco.objPaging.PerPageRecordlist = CommonFunctions.PerPageRecordlist();

                //  if (islast == true)
                PageNumber = PageNumber - 1;

                if ((startDate != null || startDate != "") && (endDate != null && endDate != ""))
                {
                    DateTime _startdate = DateTime.Parse(startDate);
                    DateTime _enddate = DateTime.Parse(endDate);
                    pagingPoco.ObjInspectionOrderDetailsPaging = new List<ViewPreviousInspection>();

                    //Below commented code for paging in this we use perpagerecord and skiprecord parameters and search between startdate and enddate for custom paging.
                    //pagingPoco.ObjInspectionOrderDetailsPaging = dataInsectionOrderDetails.Where(x => x.ScheduleInspectionDate >= _startdate.Date && x.ScheduleInspectionDate <= _enddate).OrderByDescending(item => item.OwnerFirstName).Skip(skiprecord).Take(PerPageRecord).ToList();
                    pagingPoco.ObjInspectionOrderDetailsPaging = dataInsectionOrderDetails.Where(x => x.ScheduleInspectionDate >= _startdate.Date && x.ScheduleInspectionDate <= _enddate).ToList();

                }
                else
                {
                    //Below commented code for paging in this we use perpagerecord and skiprecord parameters for custom paging.
                    // pagingPoco.ObjInspectionOrderDetailsPaging = dataInsectionOrderDetails.OrderByDescending(item => item.OwnerFirstName).Skip(skiprecord).Take(PerPageRecord).ToList();
                    pagingPoco.ObjInspectionOrderDetailsPaging = dataInsectionOrderDetails.ToList();
                }

                return pagingPoco;
            }
            catch (Exception)
            {

                throw;
            }

        }


        /// <summary>
        /// POST: /Method will show InspectionOrderDetial record coresponding to selected page on paging.
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PerPageRecord"></param>
        /// <param name="islast"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAddtionalTestingDetails(int id)
        {
            try
            {
                var description = context.EmailTemplates.Where(x => x.TemplateId == id).FirstOrDefault().TemplateDiscription;

                return ReturnJson(description);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// POST: /Method will show InspectionOrderDetial record coresponding to selected page on paging.
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PerPageRecord"></param>
        /// <param name="islast"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult FilterInspectionOrderDetials(int PageNumber, int PerPageRecord, bool islast, string startDate, string endDate)
        {
            try
            {
                PagingPoco pagingPoco = new PagingPoco();
                pagingPoco = PagingResult(PageNumber, PerPageRecord, islast, startDate, endDate);
                Dates obj = new Dates();
                obj.fromdate = startDate;
                obj.todate = endDate;
                pagingPoco.fromandtodate = obj;
                _messageToShow.Result = RenderRazorViewToString("_PreviousInspectionDetails", pagingPoco);
                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // GET: /InsectionOrderDetails/Create

        /// <summary>
        /// GET: /Method for create new InsectionOrder
        /// </summary>
        /// <returns></returns>
        public ActionResult Create(string inspectionOrderId, string ReportType, string role)
        {

            if (Sessions.LoggedInUser != null)
            {

                if ((inspectionOrderId == null && ReportType == null && role == null && Sessions.LoggedInUser.RoleName == "Home Seller") || (inspectionOrderId != null && ReportType != null && role != null && Sessions.LoggedInUser.RoleName != "Home Seller"))
                {
                    InspectionOrderDetailPoco insectionOrderDetailPoco = new InspectionOrderDetailPoco();
                    insectionOrderDetailPoco.sellerAgent = new SellerAgent();
                    insectionOrderDetailPoco.buyerAgent = new BuyerAgent();
                    insectionOrderDetailPoco.emailTemplate = new List<emailtemplate>();
                    if (inspectionOrderId == null)
                    {
                        insectionOrderDetailPoco.emailTemplate = context.EmailTemplates.ToList().GetRange(5, 8);
                        ViewBag.Rate = insectionOrderDetailPoco.emailTemplate.Select(x => x.Rate);

                        var States = context.ZipCode.Select(x => x.State).Distinct();
                        List<SelectListItem> items = new List<SelectListItem>();
                        List<SelectListItem> objCityList = new List<SelectListItem>();
                        List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                        SelectListItem objSelectListItem = new SelectListItem();
                        objSelectListItem.Text = "--Select--";
                        objSelectListItem.Value = "--Select--";
                        //  items.Add(objSelectListItem);
                        objCityList.Add(objSelectListItem);
                        objZipCodeList.Add(objSelectListItem);
                        foreach (var t in States)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = t.ToString();
                            s.Value = t.ToString();
                            items.Add(s);
                        }
                        items = items.OrderBy(x => x.Text).ToList();
                        items.Insert(0, objSelectListItem);
                        insectionOrderDetailPoco.StatesList = items;
                        insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                        insectionOrderDetailPoco.CityList = objCityList;
                        return View(insectionOrderDetailPoco);
                    }
                    else if (role == "Home Buyer")
                    {
                        ViewBag.ReportType = ReportType;
                        ViewBag.Role = role;
                        insectionorderdetail insectionOrderDetail = new SpectorMaxDAL.insectionorderdetail();
                        if (Sessions.LoggedInUser.RoleName == "Admin")
                        {
                            var inspectorId = context.InspectionOrders.Where(x => x.InspectionOrderID == inspectionOrderId).FirstOrDefault();
                            if (inspectorId != null)
                            {
                                if (inspectorId.IsAcceptedOrRejected == false && (inspectorId.Comment != null || inspectorId.Comment!=""))
                                {
                                    ViewBag.IsInspectorAssigned = false;
                                }
                                else
                                {
                                    ViewBag.IsInspectorAssigned = true;
                                }
                            }
                            else
                            {
                                ViewBag.IsInspectorAssigned = false;
                            }
                        }

                        // getting Property's Owner information
                        insectionOrderDetail = context.InsectionOrderDetails.FirstOrDefault(x => x.InspectionOrderId == inspectionOrderId);
                        //// getting Property information
                        var inspectorId1 = context.InspectionOrders.Where(x => x.InspectionOrderID == inspectionOrderId).FirstOrDefault();
                        if (inspectorId1 != null)
                        {
                            if (inspectorId1.IsAcceptedOrRejected == false && inspectorId1.Comment != null)
                            {
                                ViewBag.Request = "false";
                                insectionOrderDetailPoco.PropertyAddress = insectionOrderDetail.PropertyAddress;
                                insectionOrderDetailPoco.PropertyAddressCrossStreet = insectionOrderDetail.PropertyAddressCrossStreet;

                                insectionOrderDetailPoco.PropertyAddressCity = insectionOrderDetail.PropertyAddressCity;
                                @ViewBag.PropertyAddressCity = insectionOrderDetailPoco.PropertyAddressCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.PropertyAddressCity, Value = insectionOrderDetailPoco.PropertyAddressCity });

                                insectionOrderDetailPoco.PropertyAddressState = insectionOrderDetail.PropertyAddressState;
                                @ViewBag.PropertyAddressState = insectionOrderDetailPoco.PropertyAddressState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.PropertyAddressState, Value = insectionOrderDetailPoco.PropertyAddressState });

                                insectionOrderDetailPoco.PropertyAddressZip = insectionOrderDetail.PropertyAddressZip ?? 0;
                                @ViewBag.PropertyAddressZip = Convert.ToString(insectionOrderDetailPoco.PropertyAddressZip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.PropertyAddressZip), Value = Convert.ToString(insectionOrderDetailPoco.PropertyAddressZip) });

                                insectionOrderDetailPoco.PropertyAddressTenantName = insectionOrderDetail.PropertyAddressTenantName;
                                insectionOrderDetailPoco.PropertyAddressSubDivison = insectionOrderDetail.PropertyAddressSubDivison;
                                insectionOrderDetailPoco.PropertyAddressMapCoord = insectionOrderDetail.PropertyAddressMapCoord;
                                insectionOrderDetailPoco.PropertyAddressPhoneDay = insectionOrderDetail.PropertyAddressPhoneDay;
                                insectionOrderDetailPoco.PropertyAddressPhoneDay1 = insectionOrderDetail.PropertyAddressPhoneDay1;
                                insectionOrderDetailPoco.PropertyAddressPhoneEve = insectionOrderDetail.PropertyAddressPhoneEve;
                                insectionOrderDetailPoco.PropertyAddressPhoneEve1 = insectionOrderDetail.PropertyAddressPhoneEve1;
                                insectionOrderDetailPoco.PropertyAddressDirections = insectionOrderDetail.PropertyAddressDirections;
                                insectionOrderDetailPoco.PropertyDescription = insectionOrderDetail.PropertyDescription;

                                insectionOrderDetailPoco.RequestedBy = insectionOrderDetail.RequestedBy;
                                insectionOrderDetailPoco.RequestedLastName = insectionOrderDetail.RequestedLastName;
                                insectionOrderDetailPoco.RequestedByEmailPhoneDay = insectionOrderDetail.RequestedByEmailPhoneDay;
                                insectionOrderDetailPoco.RequestedByEmailPhoneDay1 = insectionOrderDetail.RequestedByEmailPhoneDay1;
                                insectionOrderDetailPoco.RequestedByEmailPhoneEve = insectionOrderDetail.RequestedByEmailPhoneEve;
                                insectionOrderDetailPoco.RequestedByEmailPhoneEve1 = insectionOrderDetail.RequestedByEmailPhoneEve1;
                                insectionOrderDetailPoco.RequestedByEmailPhoneOther = insectionOrderDetail.RequestedByEmailPhoneOther;
                                insectionOrderDetailPoco.RequestedByEmailPhoneOther1 = insectionOrderDetail.RequestedByEmailPhoneOther1;
                                insectionOrderDetailPoco.RequestedByStreetAddress = insectionOrderDetail.RequestedByStreetAddress;

                                insectionOrderDetailPoco.RequestedByCity = insectionOrderDetail.RequestedByCity;
                                @ViewBag.RequestedByCity = insectionOrderDetailPoco.RequestedByCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.RequestedByCity, Value = insectionOrderDetailPoco.RequestedByCity });
                                insectionOrderDetailPoco.RequestedByState = insectionOrderDetail.RequestedByState;
                                @ViewBag.RequestedByState = insectionOrderDetailPoco.RequestedByState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.RequestedByState, Value = insectionOrderDetailPoco.RequestedByState });
                                insectionOrderDetailPoco.RequestedByZip = insectionOrderDetail.RequestedByZip ?? 0;
                                @ViewBag.RequestedByZip = Convert.ToString(insectionOrderDetailPoco.RequestedByZip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.RequestedByZip), Value = Convert.ToString(insectionOrderDetailPoco.RequestedByZip) });

                                insectionOrderDetailPoco.RequestedByEmailAddress1 = insectionOrderDetail.RequestedByEmailAddress1;
                            }
                            else
                            {
                                ViewBag.Request = "true";
                                insectionOrderDetailPoco.InspectionOrderId = inspectionOrderId;
                                insectionOrderDetailPoco.OwnerFirstName = insectionOrderDetail.OwnerFirstName;

                                insectionOrderDetailPoco.OwnerLastName = insectionOrderDetail.OwnerLastName;
                                insectionOrderDetailPoco.StreetAddess = insectionOrderDetail.StreetAddess;
                                insectionOrderDetailPoco.City = insectionOrderDetail.City;
                                //@ViewBag.City = new SelectList(insectionOrderDetailPoco.City);
                                @ViewBag.City = insectionOrderDetailPoco.City.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.City, Value = insectionOrderDetailPoco.City });

                                insectionOrderDetailPoco.State = insectionOrderDetail.State;
                                @ViewBag.State = insectionOrderDetailPoco.State.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.State, Value = insectionOrderDetailPoco.State });

                                insectionOrderDetailPoco.Zip = insectionOrderDetail.Zip ?? 0;
                                @ViewBag.Zip = Convert.ToString(insectionOrderDetailPoco.Zip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.Zip), Value = Convert.ToString(insectionOrderDetailPoco.Zip) });

                                insectionOrderDetailPoco.EmailAddress = insectionOrderDetail.EmailAddress;
                                insectionOrderDetailPoco.PhoneDay = insectionOrderDetail.PhoneDay;
                                insectionOrderDetailPoco.PhoneDay1 = insectionOrderDetail.PhoneDay1;
                                insectionOrderDetailPoco.PhoneEve = insectionOrderDetail.PhoneEve;
                                insectionOrderDetailPoco.PhoneEve1 = insectionOrderDetail.PhoneEve1;
                                insectionOrderDetailPoco.Other = insectionOrderDetail.Other;
                                insectionOrderDetailPoco.Other1 = insectionOrderDetail.Other1;
                                insectionOrderDetailPoco.ContractClient = insectionOrderDetail.ContractClient == null ? false : true;
                                insectionOrderDetailPoco.SendCopy = insectionOrderDetail.SendCopy;

                                // getting Property information
                                insectionOrderDetailPoco.PropertyAddress = insectionOrderDetail.PropertyAddress;
                                insectionOrderDetailPoco.PropertyAddressCrossStreet = insectionOrderDetail.PropertyAddressCrossStreet;

                                insectionOrderDetailPoco.PropertyAddressCity = insectionOrderDetail.PropertyAddressCity;
                                @ViewBag.PropertyAddressCity = insectionOrderDetailPoco.PropertyAddressCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.PropertyAddressCity, Value = insectionOrderDetailPoco.PropertyAddressCity });

                                insectionOrderDetailPoco.PropertyAddressState = insectionOrderDetail.PropertyAddressState;
                                @ViewBag.PropertyAddressState = insectionOrderDetailPoco.PropertyAddressState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.PropertyAddressState, Value = insectionOrderDetailPoco.PropertyAddressState });

                                insectionOrderDetailPoco.PropertyAddressZip = insectionOrderDetail.PropertyAddressZip ?? 0;
                                @ViewBag.PropertyAddressZip = Convert.ToString(insectionOrderDetailPoco.PropertyAddressZip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.PropertyAddressZip), Value = Convert.ToString(insectionOrderDetailPoco.PropertyAddressZip) });

                                insectionOrderDetailPoco.PropertyAddressTenantName = insectionOrderDetail.PropertyAddressTenantName;
                                insectionOrderDetailPoco.PropertyAddressSubDivison = insectionOrderDetail.PropertyAddressSubDivison;
                                insectionOrderDetailPoco.PropertyAddressMapCoord = insectionOrderDetail.PropertyAddressMapCoord;
                                insectionOrderDetailPoco.PropertyAddressPhoneDay = insectionOrderDetail.PropertyAddressPhoneDay;
                                insectionOrderDetailPoco.PropertyAddressPhoneDay1 = insectionOrderDetail.PropertyAddressPhoneDay1;
                                insectionOrderDetailPoco.PropertyAddressPhoneEve = insectionOrderDetail.PropertyAddressPhoneEve;
                                insectionOrderDetailPoco.PropertyAddressPhoneEve1 = insectionOrderDetail.PropertyAddressPhoneEve1;
                                insectionOrderDetailPoco.PropertyAddressDirections = insectionOrderDetail.PropertyAddressDirections;
                                insectionOrderDetailPoco.PropertyDescription = insectionOrderDetail.PropertyDescription;
                                insectionOrderDetailPoco.PropertyUnit = insectionOrderDetail.PropertyUnit ?? 0;
                                insectionOrderDetailPoco.PropertySquareFootage = insectionOrderDetail.PropertySquareFootage ?? 0;
                                insectionOrderDetailPoco.PropertyAddition = insectionOrderDetail.PropertyAddition ?? "";
                                insectionOrderDetailPoco.PropertyBedRoom = insectionOrderDetail.PropertyBedRoom ?? 0;
                                insectionOrderDetailPoco.PropertyBaths = insectionOrderDetail.PropertyBaths ?? 0;
                                insectionOrderDetailPoco.PropertyEstimatedMonthlyUtilites = insectionOrderDetail.PropertyEstimatedMonthlyUtilites;
                                insectionOrderDetailPoco.PropertyHomeAge = insectionOrderDetail.PropertyHomeAge ?? 0;
                                insectionOrderDetailPoco.PropertyRoofAge = insectionOrderDetail.PropertyRoofAge ?? 0;
                                insectionOrderDetailPoco.SpecialInstructions = insectionOrderDetail.SpecialInstructions; //insectionOrderDetailPoco.SpecialInstructions + insectionOrderDetailPoco.SpecialInstructions1
                                insectionOrderDetailPoco.ConfirmationPriorWithBuyer = insectionOrderDetail.ConfirmationPriorWithBuyer;
                                insectionOrderDetailPoco.ConfirmationPriorWithBuyersAgent = insectionOrderDetail.ConfirmationPriorWithBuyersAgent;
                                insectionOrderDetailPoco.ConfirmationPriorWithSellersAgent = insectionOrderDetail.ConfirmationPriorWithSellersAgent;

                                // getting Requested by information
                                insectionOrderDetailPoco.RequestedBy = insectionOrderDetail.RequestedBy;
                                insectionOrderDetailPoco.RequestedByEmailPhoneDay = insectionOrderDetail.RequestedByEmailPhoneDay;
                                insectionOrderDetailPoco.RequestedByEmailPhoneDay1 = insectionOrderDetail.RequestedByEmailPhoneDay1;
                                insectionOrderDetailPoco.RequestedByEmailPhoneEve = insectionOrderDetail.RequestedByEmailPhoneEve;
                                insectionOrderDetailPoco.RequestedByEmailPhoneEve1 = insectionOrderDetail.RequestedByEmailPhoneEve1;
                                insectionOrderDetailPoco.RequestedByEmailPhoneOther = insectionOrderDetail.RequestedByEmailPhoneOther;
                                insectionOrderDetailPoco.RequestedByEmailPhoneOther1 = insectionOrderDetail.RequestedByEmailPhoneOther1;
                                insectionOrderDetailPoco.RequestedByStreetAddress = insectionOrderDetail.RequestedByStreetAddress;

                                insectionOrderDetailPoco.RequestedByCity = insectionOrderDetail.RequestedByCity;
                                @ViewBag.RequestedByCity = insectionOrderDetailPoco.RequestedByCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.RequestedByCity, Value = insectionOrderDetailPoco.RequestedByCity });
                                insectionOrderDetailPoco.RequestedByState = insectionOrderDetail.RequestedByState;
                                @ViewBag.RequestedByState = insectionOrderDetailPoco.RequestedByState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.RequestedByState, Value = insectionOrderDetailPoco.RequestedByState });
                                insectionOrderDetailPoco.RequestedByZip = insectionOrderDetail.RequestedByZip ?? 0;
                                @ViewBag.RequestedByZip = Convert.ToString(insectionOrderDetailPoco.RequestedByZip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.RequestedByZip), Value = Convert.ToString(insectionOrderDetailPoco.RequestedByZip) });

                                insectionOrderDetailPoco.RequestedByEmailAddress1 = insectionOrderDetail.RequestedByEmailAddress1;
                                insectionOrderDetailPoco.IsRequestedByEmailOther = insectionOrderDetail.IsRequestedByEmailOther;
                                insectionOrderDetailPoco.RequestedByEmailSend = insectionOrderDetail.RequestedByEmailSend;
                                insectionOrderDetailPoco.RequestedByEmailOther = insectionOrderDetail.RequestedByEmailOther;
                                insectionOrderDetailPoco.RequestedByEmailOwner = insectionOrderDetail.RequestedByEmailOwner;
                                insectionOrderDetailPoco.RequestedByEmailOwnersAgent = insectionOrderDetail.RequestedByEmailOwnersAgent;
                                insectionOrderDetailPoco.RequestedByEmailContractClient = insectionOrderDetail.RequestedByEmailContractClient;

                                insectionOrderDetailPoco.MainClient1 = insectionOrderDetail.MainClient1;
                                insectionOrderDetailPoco.MainClient2 = insectionOrderDetail.MainClient2;
                                insectionOrderDetailPoco.MainDate1 = insectionOrderDetail.MainDate1;
                                insectionOrderDetailPoco.MainDate2 = insectionOrderDetail.MainDate2;
                                insectionOrderDetailPoco.ScheduleInspectionDate = insectionOrderDetail.ScheduleInspectionDate;

                                insectionOrderDetailPoco.IsAdditionalTest1 = insectionOrderDetail.IsAdditionalTest1;
                                insectionOrderDetailPoco.IsAdditionalTest2 = insectionOrderDetail.IsAdditionalTest2;
                                insectionOrderDetailPoco.IsAdditionalTest3 = insectionOrderDetail.IsAdditionalTest3;
                                insectionOrderDetailPoco.IsAdditionalTest4 = insectionOrderDetail.IsAdditionalTest4;
                                insectionOrderDetailPoco.IsAdditionalTest5 = insectionOrderDetail.IsAdditionalTest5;
                                insectionOrderDetailPoco.IsAdditionalTest6 = insectionOrderDetail.IsAdditionalTest6;
                                insectionOrderDetailPoco.IsAdditionalTest7 = insectionOrderDetail.IsAdditionalTest7;
                                insectionOrderDetailPoco.IsAdditionalTest8 = insectionOrderDetail.IsAdditionalTest8;
                                insectionOrderDetailPoco.TotalAdditionalTestingAmount = insectionOrderDetail.TotalAdditionalTestingAmount;
                                insectionOrderDetailPoco.emailTemplate = context.EmailTemplates.ToList().GetRange(5, 8);
                                if (insectionOrderDetailPoco.emailTemplate == null)
                                {
                                    insectionOrderDetailPoco.emailTemplate = new List<emailtemplate>();
                                }

                                // Inserting buyer's information
                                agentlibrary AgentLibraryinfo;
                                AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                                AgentLibraryinfo = context.AgentLibraries.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId && x.AgentType == "2");
                                if (AgentLibraryinfo != null)
                                {
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentFirstName = AgentLibraryinfo.AgentFirstName;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentLastName = AgentLibraryinfo.AgentLastName;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentOffice = AgentLibraryinfo.AgentOffice;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay = AgentLibraryinfo.AgentPhoneDay;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay1 = AgentLibraryinfo.AgentPhoneDay1;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneEve = AgentLibraryinfo.AgentPhoneEve;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneEve1 = AgentLibraryinfo.AgentPhoneEve1;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneOther = AgentLibraryinfo.AgentPhoneOther;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneOther1 = AgentLibraryinfo.AgentPhoneOther1;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentStreetAddress = AgentLibraryinfo.AgentStreetAddress;

                        //            insectionOrderDetailPoco.buyerAgent.BuyerAgentCity = AgentLibraryinfo.AgentCity;
                                    @ViewBag.BuyerAgentCity = insectionOrderDetailPoco.buyerAgent.BuyerAgentCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.buyerAgent.BuyerAgentCity, Value = insectionOrderDetailPoco.buyerAgent.BuyerAgentCity });
                        //            insectionOrderDetailPoco.buyerAgent.BuyerAgentState = AgentLibraryinfo.AgentState;
                                    @ViewBag.BuyerAgentState = insectionOrderDetailPoco.buyerAgent.BuyerAgentState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.buyerAgent.BuyerAgentState, Value = insectionOrderDetailPoco.buyerAgent.BuyerAgentState });
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentZip = AgentLibraryinfo.AgentZip ?? 0;
                                    @ViewBag.BuyerAgentZip = Convert.ToString(insectionOrderDetailPoco.buyerAgent.BuyerAgentZip).ToList().Select(x => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.buyerAgent.BuyerAgentZip), Value = Convert.ToString(insectionOrderDetailPoco.buyerAgent.BuyerAgentZip) });

                                    insectionOrderDetailPoco.buyerAgent.AgentBuyerSend = AgentLibraryinfo.AgentSend;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress = AgentLibraryinfo.AgentEmailAddress;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress1 = AgentLibraryinfo.AgentEmailAddress;

                                }


                                // getting sellers's information

                                AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                                AgentLibraryinfo = context.AgentLibraries.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId && x.AgentType == "1");
                                if (AgentLibraryinfo != null)
                                {
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentFirstName = AgentLibraryinfo.AgentFirstName;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentLastName = AgentLibraryinfo.AgentLastName;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentOffice = AgentLibraryinfo.AgentOffice;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay = AgentLibraryinfo.AgentPhoneDay;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay1 = AgentLibraryinfo.AgentPhoneDay1;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneEve = AgentLibraryinfo.AgentPhoneEve;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneEve1 = AgentLibraryinfo.AgentPhoneEve1;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneOther = AgentLibraryinfo.AgentPhoneOther;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneOther1 = AgentLibraryinfo.AgentPhoneOther1;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentStreetAddress = AgentLibraryinfo.AgentStreetAddress;

                              //      insectionOrderDetailPoco.sellerAgent.SellerAgentCity = AgentLibraryinfo.AgentCity;
                                    @ViewBag.SellerAgentCity = insectionOrderDetailPoco.sellerAgent.SellerAgentCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.sellerAgent.SellerAgentCity, Value = insectionOrderDetailPoco.sellerAgent.SellerAgentCity });

                              //      insectionOrderDetailPoco.sellerAgent.SellerAgentState = AgentLibraryinfo.AgentState;
                                    @ViewBag.SellerAgentState = insectionOrderDetailPoco.sellerAgent.SellerAgentState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.sellerAgent.SellerAgentState, Value = insectionOrderDetailPoco.sellerAgent.SellerAgentState });

                                    insectionOrderDetailPoco.sellerAgent.SellerAgentZip = AgentLibraryinfo.AgentZip ?? 0;
                                    @ViewBag.SellerAgentZip = Convert.ToString(insectionOrderDetailPoco.sellerAgent.SellerAgentZip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.sellerAgent.SellerAgentZip), Value = Convert.ToString(insectionOrderDetailPoco.sellerAgent.SellerAgentZip) });

                                    insectionOrderDetailPoco.sellerAgent.AgentSellerSend = AgentLibraryinfo.AgentSend;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress = AgentLibraryinfo.AgentEmailAddress;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress1 = AgentLibraryinfo.AgentEmailAddress;

                                }
                            }
                        }

                            var States = context.ZipCode.Select(x => x.State).Distinct();
                            List<SelectListItem> items = new List<SelectListItem>();
                            List<SelectListItem> objCityList = new List<SelectListItem>();
                            List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                            SelectListItem objSelectListItem = new SelectListItem();
                            objSelectListItem.Text = "--Select--";
                            objSelectListItem.Value = "--Select--";
                          //  items.Add(objSelectListItem);
                            objCityList.Add(objSelectListItem);
                            objZipCodeList.Add(objSelectListItem);
                            foreach (var t in States)
                            {
                                SelectListItem s = new SelectListItem();
                                s.Text = t.ToString();
                                s.Value = t.ToString();
                                items.Add(s);
                            }
                            items = items.OrderBy(x => x.Text).ToList();
                            items.Insert(0, objSelectListItem);
                            insectionOrderDetailPoco.emailTemplate = context.EmailTemplates.ToList().GetRange(5, 8);
                            ViewBag.Rate = insectionOrderDetailPoco.emailTemplate.Select(x => x.Rate);
                            if (insectionOrderDetailPoco.emailTemplate == null)
                            {
                                insectionOrderDetailPoco.emailTemplate = new List<emailtemplate>();
                            }
                            insectionOrderDetailPoco.StatesList = items;
                            insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                            insectionOrderDetailPoco.CityList = objCityList;

                        
                        return View(insectionOrderDetailPoco);
                    }
                    else
                    {
                        try
                        {


                            ViewBag.ReportType = ReportType;
                            insectionorderdetail insectionOrderDetail = new SpectorMaxDAL.insectionorderdetail();

                            if (Sessions.LoggedInUser.RoleName == "Admin")
                            {
                                var inspectorId = context.InspectionOrders.Where(x => x.InspectionOrderID == inspectionOrderId).FirstOrDefault();
                                if (inspectorId != null)
                                {
                                    if (inspectorId.IsAcceptedOrRejected == false && (inspectorId.Comment != null || inspectorId.Comment!=""))
                                    {
                                        ViewBag.IsInspectorAssigned = false;
                                    }
                                    else
                                    {
                                        ViewBag.IsInspectorAssigned = true;
                                    }
                                }
                                else
                                {
                                    ViewBag.IsInspectorAssigned = false;
                                }
                            }
                            else if (Sessions.LoggedInUser.RoleName == "Property Inspector")
                            {
                                bool inspectorId = context.InspectionOrders.Where(x => x.InspectionOrderID == inspectionOrderId).FirstOrDefault().IsAcceptedOrRejected;
                                if (inspectorId == true)
                                {
                                    ViewBag.IsAccepted = true;
                                }
                                else
                                {
                                    ViewBag.IsAccepted = false;
                                }


                            }
                            // getting Property's Owner information
                            insectionOrderDetail = context.InsectionOrderDetails.FirstOrDefault(x => x.InspectionOrderId == inspectionOrderId);
                            if (insectionOrderDetail != null)
                            {
                                insectionOrderDetailPoco.InspectionOrderId = inspectionOrderId;
                                insectionOrderDetailPoco.OwnerFirstName = insectionOrderDetail.OwnerFirstName;

                                insectionOrderDetailPoco.OwnerLastName = insectionOrderDetail.OwnerLastName;
                                insectionOrderDetailPoco.StreetAddess = insectionOrderDetail.StreetAddess;
                                insectionOrderDetailPoco.City = insectionOrderDetail.City;
                                //@ViewBag.City = new SelectList(insectionOrderDetailPoco.City);
                                @ViewBag.City = insectionOrderDetailPoco.City.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.City, Value = insectionOrderDetailPoco.City });

                                insectionOrderDetailPoco.State = insectionOrderDetail.State;
                                @ViewBag.State = insectionOrderDetailPoco.State.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.State, Value = insectionOrderDetailPoco.State });

                                insectionOrderDetailPoco.Zip = insectionOrderDetail.Zip ?? 0;
                                @ViewBag.Zip = Convert.ToString(insectionOrderDetailPoco.Zip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.Zip), Value = Convert.ToString(insectionOrderDetailPoco.Zip) });

                                insectionOrderDetailPoco.EmailAddress = insectionOrderDetail.EmailAddress;
                                insectionOrderDetailPoco.PhoneDay = insectionOrderDetail.PhoneDay;
                                insectionOrderDetailPoco.PhoneDay1 = insectionOrderDetail.PhoneDay1;
                                insectionOrderDetailPoco.PhoneEve = insectionOrderDetail.PhoneEve;
                                insectionOrderDetailPoco.PhoneEve1 = insectionOrderDetail.PhoneEve1;
                                insectionOrderDetailPoco.Other = insectionOrderDetail.Other;
                                insectionOrderDetailPoco.Other1 = insectionOrderDetail.Other1;
                                insectionOrderDetailPoco.ContractClient = insectionOrderDetail.ContractClient == null ? false : true;
                                insectionOrderDetailPoco.SendCopy = insectionOrderDetail.SendCopy;

                                // getting Property information
                                insectionOrderDetailPoco.PropertyAddress = insectionOrderDetail.PropertyAddress;
                                insectionOrderDetailPoco.PropertyAddressCrossStreet = insectionOrderDetail.PropertyAddressCrossStreet;

                                insectionOrderDetailPoco.PropertyAddressCity = insectionOrderDetail.PropertyAddressCity;
                                @ViewBag.PropertyAddressCity = insectionOrderDetailPoco.PropertyAddressCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.PropertyAddressCity, Value = insectionOrderDetailPoco.PropertyAddressCity });

                                insectionOrderDetailPoco.PropertyAddressState = insectionOrderDetail.PropertyAddressState;
                                @ViewBag.PropertyAddressState = insectionOrderDetailPoco.PropertyAddressState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.PropertyAddressState, Value = insectionOrderDetailPoco.PropertyAddressState });

                                insectionOrderDetailPoco.PropertyAddressZip = insectionOrderDetail.PropertyAddressZip ?? 0;
                                @ViewBag.PropertyAddressZip = Convert.ToString(insectionOrderDetailPoco.PropertyAddressZip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.PropertyAddressZip), Value = Convert.ToString(insectionOrderDetailPoco.PropertyAddressZip) });

                                insectionOrderDetailPoco.PropertyAddressTenantName = insectionOrderDetail.PropertyAddressTenantName;
                                insectionOrderDetailPoco.PropertyAddressSubDivison = insectionOrderDetail.PropertyAddressSubDivison;
                                insectionOrderDetailPoco.PropertyAddressMapCoord = insectionOrderDetail.PropertyAddressMapCoord;
                                insectionOrderDetailPoco.PropertyAddressPhoneDay = insectionOrderDetail.PropertyAddressPhoneDay;
                                insectionOrderDetailPoco.PropertyAddressPhoneDay1 = insectionOrderDetail.PropertyAddressPhoneDay1;
                                insectionOrderDetailPoco.PropertyAddressPhoneEve = insectionOrderDetail.PropertyAddressPhoneEve;
                                insectionOrderDetailPoco.PropertyAddressPhoneEve1 = insectionOrderDetail.PropertyAddressPhoneEve1;
                                insectionOrderDetailPoco.PropertyAddressDirections = insectionOrderDetail.PropertyAddressDirections;
                                insectionOrderDetailPoco.PropertyDescription = insectionOrderDetail.PropertyDescription;
                                insectionOrderDetailPoco.PropertyUnit = insectionOrderDetail.PropertyUnit ?? 0;
                                insectionOrderDetailPoco.PropertySquareFootage = insectionOrderDetail.PropertySquareFootage ?? 0;
                                insectionOrderDetailPoco.PropertyAddition = insectionOrderDetail.PropertyAddition ?? "";
                                insectionOrderDetailPoco.PropertyBedRoom = insectionOrderDetail.PropertyBedRoom ?? 0;
                                insectionOrderDetailPoco.PropertyBaths = insectionOrderDetail.PropertyBaths ?? 0;
                                insectionOrderDetailPoco.PropertyEstimatedMonthlyUtilites = insectionOrderDetail.PropertyEstimatedMonthlyUtilites;
                                insectionOrderDetailPoco.PropertyHomeAge = insectionOrderDetail.PropertyHomeAge ?? 0;
                                insectionOrderDetailPoco.PropertyRoofAge = insectionOrderDetail.PropertyRoofAge ?? 0;
                                insectionOrderDetailPoco.SpecialInstructions = insectionOrderDetail.SpecialInstructions; //insectionOrderDetailPoco.SpecialInstructions + insectionOrderDetailPoco.SpecialInstructions1
                                insectionOrderDetailPoco.ConfirmationPriorWithBuyer = insectionOrderDetail.ConfirmationPriorWithBuyer;
                                insectionOrderDetailPoco.ConfirmationPriorWithBuyersAgent = insectionOrderDetail.ConfirmationPriorWithBuyersAgent;
                                insectionOrderDetailPoco.ConfirmationPriorWithSellersAgent = insectionOrderDetail.ConfirmationPriorWithSellersAgent;

                                // getting Requested by information
                                insectionOrderDetailPoco.RequestedBy = insectionOrderDetail.RequestedBy;
                                insectionOrderDetailPoco.RequestedByEmailPhoneDay = insectionOrderDetail.RequestedByEmailPhoneDay;
                                insectionOrderDetailPoco.RequestedByEmailPhoneDay1 = insectionOrderDetail.RequestedByEmailPhoneDay1;
                                insectionOrderDetailPoco.RequestedByEmailPhoneEve = insectionOrderDetail.RequestedByEmailPhoneEve;
                                insectionOrderDetailPoco.RequestedByEmailPhoneEve1 = insectionOrderDetail.RequestedByEmailPhoneEve1;
                                insectionOrderDetailPoco.RequestedByEmailPhoneOther = insectionOrderDetail.RequestedByEmailPhoneOther;
                                insectionOrderDetailPoco.RequestedByEmailPhoneOther1 = insectionOrderDetail.RequestedByEmailPhoneOther1;
                                insectionOrderDetailPoco.RequestedByStreetAddress = insectionOrderDetail.RequestedByStreetAddress;

                                insectionOrderDetailPoco.RequestedByCity = insectionOrderDetail.RequestedByCity;
                                @ViewBag.RequestedByCity = insectionOrderDetailPoco.RequestedByCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.RequestedByCity, Value = insectionOrderDetailPoco.RequestedByCity });
                                insectionOrderDetailPoco.RequestedByState = insectionOrderDetail.RequestedByState;
                                @ViewBag.RequestedByState = insectionOrderDetailPoco.RequestedByState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.RequestedByState, Value = insectionOrderDetailPoco.RequestedByState });
                                insectionOrderDetailPoco.RequestedByZip = insectionOrderDetail.RequestedByZip ?? 0;
                                @ViewBag.RequestedByZip = Convert.ToString(insectionOrderDetailPoco.RequestedByZip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.RequestedByZip), Value = Convert.ToString(insectionOrderDetailPoco.RequestedByZip) });

                                insectionOrderDetailPoco.RequestedByEmailAddress1 = insectionOrderDetail.RequestedByEmailAddress1;
                                insectionOrderDetailPoco.IsRequestedByEmailOther = insectionOrderDetail.IsRequestedByEmailOther;
                                insectionOrderDetailPoco.RequestedByEmailSend = insectionOrderDetail.RequestedByEmailSend;
                                insectionOrderDetailPoco.RequestedByEmailOther = insectionOrderDetail.RequestedByEmailOther;
                                insectionOrderDetailPoco.RequestedByEmailOwner = insectionOrderDetail.RequestedByEmailOwner;
                                insectionOrderDetailPoco.RequestedByEmailOwnersAgent = insectionOrderDetail.RequestedByEmailOwnersAgent;
                                insectionOrderDetailPoco.RequestedByEmailContractClient = insectionOrderDetail.RequestedByEmailContractClient;

                                insectionOrderDetailPoco.MainClient1 = insectionOrderDetail.MainClient1;
                                insectionOrderDetailPoco.MainClient2 = insectionOrderDetail.MainClient2;
                                insectionOrderDetailPoco.MainDate1 = insectionOrderDetail.MainDate1;
                                insectionOrderDetailPoco.MainDate2 = insectionOrderDetail.MainDate2;
                                insectionOrderDetailPoco.ScheduleInspectionDate = insectionOrderDetail.ScheduleInspectionDate;

                                insectionOrderDetailPoco.IsAdditionalTest1 = insectionOrderDetail.IsAdditionalTest1;
                                insectionOrderDetailPoco.IsAdditionalTest2 = insectionOrderDetail.IsAdditionalTest2;
                                insectionOrderDetailPoco.IsAdditionalTest3 = insectionOrderDetail.IsAdditionalTest3;
                                insectionOrderDetailPoco.IsAdditionalTest4 = insectionOrderDetail.IsAdditionalTest4;
                                insectionOrderDetailPoco.IsAdditionalTest5 = insectionOrderDetail.IsAdditionalTest5;
                                insectionOrderDetailPoco.IsAdditionalTest6 = insectionOrderDetail.IsAdditionalTest6;
                                insectionOrderDetailPoco.IsAdditionalTest7 = insectionOrderDetail.IsAdditionalTest7;
                                insectionOrderDetailPoco.IsAdditionalTest8 = insectionOrderDetail.IsAdditionalTest8;
                                insectionOrderDetailPoco.TotalAdditionalTestingAmount = insectionOrderDetail.TotalAdditionalTestingAmount;
                                insectionOrderDetailPoco.emailTemplate = context.EmailTemplates.ToList().GetRange(5, 8);
                                if (insectionOrderDetailPoco.emailTemplate == null)
                                {
                                    insectionOrderDetailPoco.emailTemplate = new List<emailtemplate>();
                                }

                                // Inserting buyer's information
                                agentlibrary AgentLibraryinfo;
                                AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                                AgentLibraryinfo = context.AgentLibraries.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId && x.AgentType == "2");
                                if (AgentLibraryinfo != null)
                                {
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentFirstName = AgentLibraryinfo.AgentFirstName;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentLastName = AgentLibraryinfo.AgentLastName;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentOffice = AgentLibraryinfo.AgentOffice;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay = AgentLibraryinfo.AgentPhoneDay;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneDay1 = AgentLibraryinfo.AgentPhoneDay1;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneEve = AgentLibraryinfo.AgentPhoneEve;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneEve1 = AgentLibraryinfo.AgentPhoneEve1;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneOther = AgentLibraryinfo.AgentPhoneOther;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentPhoneOther1 = AgentLibraryinfo.AgentPhoneOther1;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentStreetAddress = AgentLibraryinfo.AgentStreetAddress;

                            //        insectionOrderDetailPoco.buyerAgent.BuyerAgentCity = AgentLibraryinfo.AgentCity;
                                    @ViewBag.BuyerAgentCity = insectionOrderDetailPoco.buyerAgent.BuyerAgentCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.buyerAgent.BuyerAgentCity, Value = insectionOrderDetailPoco.buyerAgent.BuyerAgentCity });
                           //         insectionOrderDetailPoco.buyerAgent.BuyerAgentState = AgentLibraryinfo.AgentState;
                                    @ViewBag.BuyerAgentState = insectionOrderDetailPoco.buyerAgent.BuyerAgentState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.buyerAgent.BuyerAgentState, Value = insectionOrderDetailPoco.buyerAgent.BuyerAgentState });
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentZip = AgentLibraryinfo.AgentZip ?? 0;
                                    @ViewBag.BuyerAgentZip = Convert.ToString(insectionOrderDetailPoco.buyerAgent.BuyerAgentZip).ToList().Select(x => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.buyerAgent.BuyerAgentZip), Value = Convert.ToString(insectionOrderDetailPoco.buyerAgent.BuyerAgentZip) });

                                    insectionOrderDetailPoco.buyerAgent.AgentBuyerSend = AgentLibraryinfo.AgentSend;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress = AgentLibraryinfo.AgentEmailAddress;
                                    insectionOrderDetailPoco.buyerAgent.BuyerAgentEmailAddress1 = AgentLibraryinfo.AgentEmailAddress;

                                }


                                // getting sellers's information

                                AgentLibraryinfo = new SpectorMaxDAL.agentlibrary();
                                AgentLibraryinfo = context.AgentLibraries.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId && x.AgentType == "1");
                                if (AgentLibraryinfo != null)
                                {
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentFirstName = AgentLibraryinfo.AgentFirstName;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentLastName = AgentLibraryinfo.AgentLastName;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentOffice = AgentLibraryinfo.AgentOffice;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay = AgentLibraryinfo.AgentPhoneDay;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneDay1 = AgentLibraryinfo.AgentPhoneDay1;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneEve = AgentLibraryinfo.AgentPhoneEve;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneEve1 = AgentLibraryinfo.AgentPhoneEve1;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneOther = AgentLibraryinfo.AgentPhoneOther;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentPhoneOther1 = AgentLibraryinfo.AgentPhoneOther1;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentStreetAddress = AgentLibraryinfo.AgentStreetAddress;

                               //     insectionOrderDetailPoco.sellerAgent.SellerAgentCity = AgentLibraryinfo.AgentCity;
                                    @ViewBag.SellerAgentCity = insectionOrderDetailPoco.sellerAgent.SellerAgentCity.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.sellerAgent.SellerAgentCity, Value = insectionOrderDetailPoco.sellerAgent.SellerAgentCity });

                             //       insectionOrderDetailPoco.sellerAgent.SellerAgentState = AgentLibraryinfo.AgentState;
                                    @ViewBag.SellerAgentState = insectionOrderDetailPoco.sellerAgent.SellerAgentState.ToList().Select(c => new SelectListItem { Text = insectionOrderDetailPoco.sellerAgent.SellerAgentState, Value = insectionOrderDetailPoco.sellerAgent.SellerAgentState });

                                    insectionOrderDetailPoco.sellerAgent.SellerAgentZip = AgentLibraryinfo.AgentZip ?? 0;
                                    @ViewBag.SellerAgentZip = Convert.ToString(insectionOrderDetailPoco.sellerAgent.SellerAgentZip).ToList().Select(c => new SelectListItem { Text = Convert.ToString(insectionOrderDetailPoco.sellerAgent.SellerAgentZip), Value = Convert.ToString(insectionOrderDetailPoco.sellerAgent.SellerAgentZip) });

                                    insectionOrderDetailPoco.sellerAgent.AgentSellerSend = AgentLibraryinfo.AgentSend;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress = AgentLibraryinfo.AgentEmailAddress;
                                    insectionOrderDetailPoco.sellerAgent.SellerAgentEmailAddress1 = AgentLibraryinfo.AgentEmailAddress;

                                }

                            }
                           
                        }
                        catch (Exception ex)
                        {
                            _messageToShow.Message = ex.Message.ToString();
                            ViewBag.Message = _messageToShow.Message;
                        }
                        return View(insectionOrderDetailPoco);
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        /// <summary>
        /// POST:// Method  for Update Accept/decline request by Inspector.
        /// </summary>
        /// <param name="inspectionOrderId"></param>
        /// <param name="Comments"></param>
        /// <param name="IsAcceptedOrRejected"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AcceptedOrRejected(string inspectionOrderId, string Comments, bool IsAcceptedOrRejected, string EmailTo)
        {
            try
            {
                inspectionorder objInspectionOrder = new inspectionorder();
                objInspectionOrder = context.InspectionOrders.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId);
                objInspectionOrder.Comment = Comments;
                objInspectionOrder.IsAcceptedOrRejected = IsAcceptedOrRejected;
                objInspectionOrder.ModifiedDate = DateTime.Now;
                context.SaveChanges();
                if (IsAcceptedOrRejected == false)
                {
                    notificationdetail objNotification = new notificationdetail();
                    objNotification.NotifcationFrom = context.InspectionOrders.FirstOrDefault(x => x.InspectionOrderID == inspectionOrderId).RequesterID;
                    objNotification.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                    objNotification.NotificationTypeID = 2;
                    objNotification.IsRead = false;
                    objNotification.CreatedDate = System.DateTime.Now;
                    objNotification.RequestID = inspectionOrderId;
                    context.NotificationDetail.Add(objNotification);
                    context.SaveChanges();
                }

                #region Sending Email
                EmailHelper email = new EmailHelper();
                string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "ScheduleInspection").TemplateDiscription;
                emailBody = emailBody.Replace("[Comments]", objInspectionOrder.Comment);
                bool status = email.SendEmail(Sessions.LoggedInUser.EmailAddress, "ScheduleInspectionEmail", "", emailBody, EmailTo);
                if (status == true)
                    _messageToShow.Message = "Success";
                else
                    _messageToShow.Message = "Mail Send Failure";
                #endregion
                //else
                //{
                //    _messageToShow.Message = "Success";
                //}
            }

            catch (Exception ex)
            { _messageToShow.Message = ex.Message.ToString(); }
            return Json(_messageToShow);
        }


        public ActionResult Edit(string id)
        {
            insectionorderdetail insectionorderdetail = context.InsectionOrderDetails.Single(x => x.InspectionOrderId == id);
            return View(insectionorderdetail);
        }

        //
        // POST: /InsectionOrderDetails/Edit/5

        [HttpPost]
        public ActionResult Edit(insectionorderdetail insectionorderdetail)
        {
            if (ModelState.IsValid)
            {
                context.Entry(insectionorderdetail).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(insectionorderdetail);
        }

        //
        // GET: /InsectionOrderDetails/Delete/5

        public ActionResult Delete(string id)
        {
            insectionorderdetail insectionorderdetail = context.InsectionOrderDetails.Single(x => x.InspectionOrderId == id);
            return View(insectionorderdetail);
        }

        //
        // POST: /InsectionOrderDetails/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            insectionorderdetail insectionorderdetail = context.InsectionOrderDetails.Single(x => x.InspectionOrderId == id);
            context.InsectionOrderDetails.Remove(insectionorderdetail);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}