using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;

namespace SpectorMax.Controllers
{   
    public class EmailTemplatesController : Controller
    {
        private SpectorMaxContext context = new SpectorMaxContext();

        //
        // GET: /EmailTemplates/

        public ViewResult Index()
        {
            return View(context.EmailTemplates.ToList());
        }

        //
        // GET: /EmailTemplates/Details/5

        public ViewResult Details(int id)
        {
            emailtemplate emailtemplate = context.EmailTemplates.Single(x => x.TemplateId == id);
            return View(emailtemplate);
        }

        //
        // GET: /EmailTemplates/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /EmailTemplates/Create

        [HttpPost]
        public ActionResult Create(emailtemplate emailtemplate)
        {
            if (ModelState.IsValid)
            {
                context.EmailTemplates.Add(emailtemplate);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(emailtemplate);
        }
        
        //
        // GET: /EmailTemplates/Edit/5
 
        public ActionResult Edit(int id)
        {
            emailtemplate emailtemplate = context.EmailTemplates.Single(x => x.TemplateId == id);
            return View(emailtemplate);
        }

        //
        // POST: /EmailTemplates/Edit/5

        [HttpPost]
        public ActionResult Edit(emailtemplate emailtemplate)
        {
            if (ModelState.IsValid)
            {
                context.Entry(emailtemplate).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(emailtemplate);
        }

        //
        // GET: /EmailTemplates/Delete/5
 
        public ActionResult Delete(int id)
        {
            emailtemplate emailtemplate = context.EmailTemplates.Single(x => x.TemplateId == id);
            return View(emailtemplate);
        }

        //
        // POST: /EmailTemplates/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            emailtemplate emailtemplate = context.EmailTemplates.Single(x => x.TemplateId == id);
            context.EmailTemplates.Remove(emailtemplate);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}