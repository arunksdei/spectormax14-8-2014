using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;

namespace SpectorMax.Controllers
{   
    public class InspectionOrdersController : Controller
    {
        private SpectorMaxContext context = new SpectorMaxContext();

        //
        // GET: /InspectionOrders/

        public ViewResult Index()
        {
            return View(context.InspectionOrders.ToList());
        }

        //
        // GET: /InspectionOrders/Details/5

        public ViewResult Details(string id)
        {
            inspectionorder inspectionorder = context.InspectionOrders.Single(x => x.InspectionOrderID == id);
            return View(inspectionorder);
        }

        //
        // GET: /InspectionOrders/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /InspectionOrders/Create

        [HttpPost]
        public ActionResult Create(inspectionorder inspectionorder)
        {
            if (ModelState.IsValid)
            {
                context.InspectionOrders.Add(inspectionorder);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(inspectionorder);
        }
        
        //
        // GET: /InspectionOrders/Edit/5
 
        public ActionResult Edit(string id)
        {
            inspectionorder inspectionorder = context.InspectionOrders.Single(x => x.InspectionOrderID == id);
            return View(inspectionorder);
        }

        //
        // POST: /InspectionOrders/Edit/5

        [HttpPost]
        public ActionResult Edit(inspectionorder inspectionorder)
        {
            if (ModelState.IsValid)
            {
                context.Entry(inspectionorder).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inspectionorder);
        }

        //
        // GET: /InspectionOrders/Delete/5
 
        public ActionResult Delete(string id)
        {
            inspectionorder inspectionorder = context.InspectionOrders.Single(x => x.InspectionOrderID == id);
            return View(inspectionorder);
        }

        //
        // POST: /InspectionOrders/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            inspectionorder inspectionorder = context.InspectionOrders.Single(x => x.InspectionOrderID == id);
            context.InspectionOrders.Remove(inspectionorder);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}