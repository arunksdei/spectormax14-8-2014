using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;

namespace SpectorMax.Controllers
{   
    public class ChecklistsController : Controller
    {
        private SpectorMaxContext context = new SpectorMaxContext();

        //
        // GET: /Checklists/

        public ViewResult Index()
        {
            return View(context.Checklists.ToList());
        }

        //
        // GET: /Checklists/Details/5

        public ViewResult Details(string id)
        {
            checklist checklist = context.Checklists.Single(x => x.CheckListID == id);
            return View(checklist);
        }

        //
        // GET: /Checklists/Create

        public ActionResult Create()
        {
            ViewBag.PossibleInspectionOrders = context.InspectionOrders;
            return View();
        } 

        //
        // POST: /Checklists/Create

        [HttpPost]
        public ActionResult Create(checklist checklist)
        {
            if (ModelState.IsValid)
            {
                context.Checklists.Add(checklist);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.PossibleInspectionOrders = context.InspectionOrders;
            return View(checklist);
        }
        
        //
        // GET: /Checklists/Edit/5
 
        public ActionResult Edit(string id)
        {
            checklist checklist = context.Checklists.Single(x => x.CheckListID == id);
            ViewBag.PossibleInspectionOrders = context.InspectionOrders;
            return View(checklist);
        }

        //
        // POST: /Checklists/Edit/5

        [HttpPost]
        public ActionResult Edit(checklist checklist)
        {
            if (ModelState.IsValid)
            {
                context.Entry(checklist).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PossibleInspectionOrders = context.InspectionOrders;
            return View(checklist);
        }

        //
        // GET: /Checklists/Delete/5
 
        public ActionResult Delete(string id)
        {
            checklist checklist = context.Checklists.Single(x => x.CheckListID == id);
            return View(checklist);
        }

        //
        // POST: /Checklists/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            checklist checklist = context.Checklists.Single(x => x.CheckListID == id);
            context.Checklists.Remove(checklist);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}