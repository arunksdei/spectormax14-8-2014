﻿using SpectorMax.Entities.Classes;
using SpectorMax.Models;
using SpectorMaxDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Data;
namespace SpectorMax.Controllers
{
    public class NNotificationController : BaseController
    {
        //
        // GET: /NNotification/
        private SpectorMaxContext context = new SpectorMaxContext();
        string connStr = ConfigurationManager.ConnectionStrings["SpectorMaxConnectionString"].ConnectionString;
        public ActionResult Index()
        {
            if (Sessions.LoggedInUser != null)
            {
                int totalNotification = context.UsersNotification.Count(x => x.To == Sessions.LoggedInUser.UserId && x.IsRead == false && x.IsDeleted == false);
               
                Session["TotalNotificationCount"] = totalNotification.ToString();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult SearchInspector(string FirstName, string LastName, string ZipCode)
        {
            List<UserInfoPoco> objUserInfo = null;
            if (Sessions.LoggedInUser != null)
            {
                //if (ZipCode != null && ZipCode != string.Empty)
                //{
                //    int zip = Convert.ToInt32(ZipCode);
                //    userInfo = context.UserInfoes.Where(x => x.ZipCode == zip && x.IsActive == true && x.IsDeleted != true && x.Role == "85DA6641-E08A-4192-A").ToList();
                //}
                //int zip = Convert.ToInt32(ZipCode);
                //var zipId = context.tblzip.Where(x => x.Zip == zip).FirstOrDefault();
                //int ZipCodeId=0;
                //if (zipId != null)
                //{
                //    ZipCodeId = zipId.ZipId;
                //}
                //objUserInfo = (from user in context.UserInfoes
                //            join zipassigned in context.ZipCodeAssigned on user.ID equals zipassigned.InspectorID
                //            join zipcode in context.tblzip on zipassigned.ZipCode equals zipcode.ZipId
                //            where user.FirstName == FirstName && user.LastName == LastName && zipcode.ZipId == ZipCodeId
                //            select new UserInfoPoco { 
                //            FirstName=user.FirstName,
                //            LastName=user.LastName,
                //            ZipCode=zipcode.Zip,
                //            ID=user.ID
                //            }).ToList();

                string SearchType = string.Empty;
                if (Sessions.LoggedInUser.RoleName == "SiteUser")
                {
                   

                    SearchType = "Inspector";
                }
                else
                {
                    SearchType = "Seller";
                   
                }
                //if (Sessions.LoggedInUser.RoleName == "Home Seller" || Sessions.LoggedInUser.RoleName == "Home Buyer")
                {
                    MySqlConnection conn = new MySqlConnection(connStr);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("SearchInspector", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserId", Sessions.LoggedInUser.UserId);
                    cmd.Parameters.AddWithValue("FirstName", FirstName);
                    cmd.Parameters.AddWithValue("LastName", LastName);
                    cmd.Parameters.AddWithValue("Zip", ZipCode);
                    cmd.Parameters.AddWithValue("SearchType", SearchType);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        objUserInfo = ds.Tables[0].AsEnumerable().Select(datarow =>
                         new UserInfoPoco
                            {
                                FirstName = datarow["FirstName"].ToString(),
                                LastName = datarow["LastName"].ToString(),
                                //ZipCode = Convert.ToInt32(datarow["Zip"]),
                                ID = datarow["ID"].ToString()
                            }).ToList();
                    }
                }

            }
            return PartialView("~/Views/NNotification/_PartialSearchInspector.cshtml", objUserInfo);
        }

        public ActionResult NotificationList()
        {
            bool result;
            List<UserNotificationModel> userNotificationModel = null;
            if (Sessions.LoggedInUser != null)
            {
                MySqlConnection conn = new MySqlConnection(connStr);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("NotificationList", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserId", Sessions.LoggedInUser.UserId);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        userNotificationModel =ds.Tables[0].AsEnumerable().Select(datarow =>new UserNotificationModel
                                         {
                                             ID = datarow["ID"].ToString(),
                                            
                                             FirstName = datarow["FirstName"].ToString(),
                                             
                                             LastName = datarow["LastName"].ToString(),
                                            
                                             
                                             Message = datarow["Message"].ToString(),
                                           
                                             Subject = datarow["Subject"].ToString(),
                                           
                                             CreatedDate = Convert.ToDateTime(datarow["CreatedDate"]),

                                             IsRead = (datarow["IsRead"] != null ? bool.TryParse(datarow["IsRead"].ToString(), out result) : false),
                                          
                                             ParentID = datarow["ParentId"].ToString(),
                                           
                                         }
                ).ToList();
                    }
                    

               

            }
            return PartialView("~/Views/NNotification/_PartialNotification.cshtml", userNotificationModel);
        }

        public JsonResult SaveComposeMessage(string ToUserId, string Subject, string Message)
        {
            string returnMessage = "";
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    SpectorMaxDAL.usersnotification objUsersNotification = new usersnotification();
                    objUsersNotification.ID = Guid.NewGuid().ToString();
                    objUsersNotification.To = ToUserId;
                    objUsersNotification.From = Sessions.LoggedInUser.UserId;
                    objUsersNotification.Subject = Subject;
                    objUsersNotification.Message = Message;
                    objUsersNotification.CreatedDate = DateTime.Now;
                    objUsersNotification.IsDeleted = false;
                    objUsersNotification.IsRead = false;
                    objUsersNotification.ParentId = objUsersNotification.ID;
                    context.UsersNotification.Add(objUsersNotification);
                    context.SaveChanges();
                    returnMessage = "Success";

                }
                else
                {
                    returnMessage = "Error";
                }
            }
            catch (Exception ex)
            {
                returnMessage = "Error";
            }
            return Json(returnMessage);
        }

        public JsonResult UpdateMessageReadStatus(string UserNotificationId)
        {
            string returnMessage = "";
            try
            {
                int id = Convert.ToInt32(UserNotificationId);
                if (Sessions.LoggedInUser != null)
                {
                    var userNoti = context.UsersNotification.Where(x => x.ID == UserNotificationId && x.To==Sessions.LoggedInUser.UserId);
                    if (userNoti != null)
                    {
                        foreach (var item in userNoti)
                        {
                            item.IsRead = true;
                        }
                        context.SaveChanges();
                        Session["TotalNotificationCount"] = Convert.ToInt32(Session["TotalNotificationCount"].ToString()) - 1;
                    }
                    returnMessage = "Success";
                }
                else
                {
                    returnMessage = "Error";
                }
            }
            catch (Exception ex)
            {
                returnMessage = "Error";
            }
            return Json(returnMessage);
        }
        public JsonResult DeleteNotificationMessage(string UserNotificationIds)
        {
            string returnMessage = "";
            try
            {
                string[] Users = UserNotificationIds.Split(',');
                if (Sessions.LoggedInUser != null)
                {
                    foreach (var id in Users)
                    {
                        string unId = id;
                        var userNoti = context.UsersNotification.Where(x => x.ID == unId).FirstOrDefault();
                        if (userNoti != null)
                        {
                            userNoti.IsDeleted = true;
                            context.SaveChanges();
                            Session["TotalNotificationCount"] = Convert.ToInt32(Session["TotalNotificationCount"].ToString()) - 1;
                        }
                    }
                    returnMessage = "Success";
                }
                else
                {
                    returnMessage = "Error";
                }
            }
            catch (Exception ex)
            {
                returnMessage = "Error";
            }
            return Json(returnMessage);
        }

        public ActionResult ViewMessage(string ParentID)
        {
            if (Sessions.LoggedInUser != null)
            {
                List<UserNotificationModel> objnotification = null;
                if (ParentID != null && ParentID != string.Empty)
                {


                    objnotification = (from notification in context.UsersNotification
                                       join user in context.UserInfoes on notification.From equals user.ID
                                       where notification.ParentId == ParentID && notification.IsDeleted == false
                                       select new UserNotificationModel
                                       {
                                           Message = notification.Message,
                                           Subject = notification.Subject,
                                           MessageForm = user.FirstName + " " + user.LastName,
                                           CreatedDate = notification.CreatedDate,
                                           From = notification.From
                                       }).OrderByDescending(x => x.CreatedDate).ToList();
                    if (objnotification != null)
                    {
                        ViewBag.ToUserId = objnotification.FirstOrDefault().From;
                        ViewBag.Subject = objnotification.FirstOrDefault().Subject;
                        string ToUserId = objnotification.FirstOrDefault().To;
                        if (ViewBag.ToUserId == Sessions.LoggedInUser.UserId)
                        {
                            ViewBag.IsReplyButtonShow = false;
                        }
                        else
                        {
                            ViewBag.IsReplyButtonShow = true;
                        }
                        ViewBag.MessageId = ParentID;

                        var notification = context.UsersNotification.Where(x => x.ParentId == ParentID);
                        foreach (var item in notification)
                        {
                            item.IsRead = true;
                        }
                        context.SaveChanges();
                    }
                }
                return View(objnotification);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        public JsonResult MessageReply(string Message, string To, string MessageId, string Subject)
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    SpectorMaxDAL.usersnotification objUsersNotification = new usersnotification();
                    objUsersNotification.ID = Guid.NewGuid().ToString();
                    objUsersNotification.To = To;
                    objUsersNotification.From = Sessions.LoggedInUser.UserId;
                    objUsersNotification.Subject = Subject;
                    objUsersNotification.Message = Message;
                    objUsersNotification.CreatedDate = DateTime.Now;
                    objUsersNotification.IsDeleted = false;
                    objUsersNotification.IsRead = false;
                    objUsersNotification.ParentId = MessageId;
                    context.UsersNotification.Add(objUsersNotification);
                    context.SaveChanges();
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
