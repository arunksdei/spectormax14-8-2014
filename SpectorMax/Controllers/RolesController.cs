using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;

namespace SpectorMax.Controllers
{   
    public class RolesController : Controller
    {
        private SpectorMaxContext context = new SpectorMaxContext();

        //
        // GET: /Roles/

        public ViewResult Index()
        {
            return View(context.Roles.ToList());
        }

        //
        // GET: /Roles/Details/5

        public ViewResult Details(string id)
        {
            role role = context.Roles.Single(x => x.RoleID == id);
            return View(role);
        }

        //
        // GET: /Roles/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Roles/Create

        [HttpPost]
        public ActionResult Create(role role)
        {
            if (ModelState.IsValid)
            {
                role.RoleID = "38";
                context.Roles.Add(role);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(role);
        }
        
        //
        // GET: /Roles/Edit/5
 
        public ActionResult Edit(string id)
        {
            role role = context.Roles.Single(x => x.RoleID == id);
            return View(role);
        }

        //
        // POST: /Roles/Edit/5

        [HttpPost]
        public ActionResult Edit(role role)
        {
            if (ModelState.IsValid)
            {
                context.Entry(role).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(role);
        }

        //
        // GET: /Roles/Delete/5
 
        public ActionResult Delete(string id)
        {
            role role = context.Roles.Single(x => x.RoleID == id);
            return View(role);
        }

        //
        // POST: /Roles/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            role role = context.Roles.Single(x => x.RoleID == id);
            context.Roles.Remove(role);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}