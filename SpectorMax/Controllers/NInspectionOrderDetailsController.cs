﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;
using SpectorMax.Entities.Classes;
using System.Data.Objects;
namespace SpectorMax.Controllers
{
    public class NInspectionOrderDetailsController : Controller
    {
        //
        // GET: /InspectionOrderDetails/
        #region pivate member
        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _messageToShow = new MessageToShow();

        #endregion

        #region Create New Inspection
        /// <summary>
        /// function to Create New Inspection Form
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateNewInspection()
        {

            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    NInspectionOrderDetails insectionOrderDetailPoco = new NInspectionOrderDetails();
                    insectionOrderDetailPoco.RequestedFirstName = Sessions.LoggedInUser.FirstName;
                    insectionOrderDetailPoco.RequestedLastName = Sessions.LoggedInUser.LastName;
                    insectionOrderDetailPoco.RequestedByEmailAddress = Sessions.LoggedInUser.EmailAddress;
                    #region Bind States
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "0";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    insectionOrderDetailPoco.StatesList = items;
                    insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                    insectionOrderDetailPoco.CityList = objCityList;

                    #endregion

                    #region Bind Property Description
                    List<NPropertyDescription> ObjPropertyDesc = new List<NPropertyDescription>();
                    ObjPropertyDesc = (from PropertyDesc in context.tblpropertydescription
                                       select new NPropertyDescription
                                       {
                                           PropertyDescriptionId = PropertyDesc.PropertyDescriptionId,
                                           Type = PropertyDesc.Type
                                       }).OrderBy(x => x.Type).ToList();
                    List<SelectListItem> PropertyDescItem = new List<SelectListItem>();
                    SelectListItem objSelectListItemProperty = new SelectListItem();
                    objSelectListItemProperty.Text = "--Select--";
                    objSelectListItemProperty.Value = "--Select--";
                    foreach (var t in ObjPropertyDesc)
                    {
                        SelectListItem s1 = new SelectListItem();
                        s1.Text = t.Type.ToString();
                        s1.Value = t.PropertyDescriptionId.ToString();
                        PropertyDescItem.Add(s1);
                    }
                    PropertyDescItem = PropertyDescItem.OrderBy(x => x.Text).ToList();
                    PropertyDescItem.Insert(0, objSelectListItemProperty);
                    insectionOrderDetailPoco.PropertyDescriptionList = PropertyDescItem;
                    #endregion
                    return View(insectionOrderDetailPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        public ActionResult NCreateNewInspection()
        {

            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    NRequesterDetails insectionOrderDetailPoco = new NRequesterDetails();
                    insectionOrderDetailPoco.RequestedFirstName = Sessions.LoggedInUser.FirstName;
                    insectionOrderDetailPoco.RequestedLastName = Sessions.LoggedInUser.LastName;
                    insectionOrderDetailPoco.RequestedByEmailAddress = Sessions.LoggedInUser.EmailAddress;
                    insectionOrderDetailPoco.PhoneNumber = Sessions.LoggedInUser.PhoneNo;
                    insectionOrderDetailPoco.RequestedByState = Sessions.LoggedInUser.StateId;
                    insectionOrderDetailPoco.RequestedByAddress = Sessions.LoggedInUser.Address;
                    insectionOrderDetailPoco.RequestedByCity = Sessions.LoggedInUser.CityId;
                    insectionOrderDetailPoco.RequestedByZip = Sessions.LoggedInUser.ZipId;
                    #region Bind States
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "--Select--";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    ViewBag.StateList = items;
                    insectionOrderDetailPoco.StatesList = items;
                    insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                    insectionOrderDetailPoco.CityList = objCityList;

                    #endregion

                    return View(insectionOrderDetailPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetSameAsAddressOnNCreateNewInspectopnStep2()
        {
            NInspectionOrderDetails objInspectionOrder = GetOrderDetails();

            string address = "{\"Address\":\"" + objInspectionOrder.RequestedByAddress + "\",\"Phone\":\"" + objInspectionOrder.PhoneNumber + "\",\"Email\":\"" + objInspectionOrder.RequestedByEmailAddress + "\",\"State\":\"" + objInspectionOrder.RequestedByState.ToString() + "\",\"City\":\"" + objInspectionOrder.RequestedByCity.ToString() + "\",\"Zip\":\"" + objInspectionOrder.RequestedByZip.ToString() + "\"}";
            return Json(address);
        }

        public ActionResult NCreateNewInspectopnStep2()
        {
            if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
            {
                NRequesterDetails insectionOrderDetailPoco = new NRequesterDetails();

                #region Bind States
                List<NState> objState = new List<NState>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                ViewBag.StateList = items;
                insectionOrderDetailPoco.StatesList = items;
                insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                insectionOrderDetailPoco.CityList = objCityList;

                #endregion

                return View("NCreateNewInspection", insectionOrderDetailPoco);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult NCreateNewInspectionStep3()
        {
            if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
            {
                NRequesterDetails insectionOrderDetailPoco = new NRequesterDetails();

                #region Bind States
                List<NState> objState = new List<NState>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                ViewBag.StateList = items;
                insectionOrderDetailPoco.StatesList = items;
                insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                insectionOrderDetailPoco.CityList = objCityList;

                #endregion

                return View("NCreateNewInspection", insectionOrderDetailPoco);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult NCreateNewInspectopnStep4()
        {
            if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
            {
                NRequesterDetails insectionOrderDetailPoco = new NRequesterDetails();

                #region Bind States
                List<NState> objState = new List<NState>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                ViewBag.StateList = items;
                insectionOrderDetailPoco.StatesList = items;
                insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                insectionOrderDetailPoco.CityList = objCityList;

                #endregion

                return View("NCreateNewInspection", insectionOrderDetailPoco);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public ActionResult NCreateNewInspection(NRequesterDetails objRequesterDetails, string BtnPrevious, string BtnNext)
        {

            if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
            {

                NInspectionOrderDetails objInspectionOrder = GetOrderDetails();
                objInspectionOrder.RequestedFirstName = objRequesterDetails.RequestedFirstName.Trim();
                objInspectionOrder.RequestedLastName = objRequesterDetails.RequestedLastName.Trim();
                objInspectionOrder.PhoneNumber = objRequesterDetails.PhoneNumber;
                objInspectionOrder.RequestedByState = objRequesterDetails.RequestedByState;
                objInspectionOrder.RequestedByCity = objRequesterDetails.RequestedByCity;
                objInspectionOrder.RequestedByZip = objRequesterDetails.RequestedByZip;
                objInspectionOrder.RequestedByEmailAddress = objRequesterDetails.RequestedByEmailAddress.Trim();
                objInspectionOrder.RequestedByAddress = objRequesterDetails.RequestedByAddress.Trim();
                int requestType = 0;
                switch (objRequesterDetails.RequesterType)
                {
                    case "Buyer": requestType = 1; break;
                    case "Seller": requestType = 2; break;
                    case "Agent": requestType = 3; break;
                }
                objInspectionOrder.RequesterType = requestType;//objRequesterDetails.RequesterType;


                #region Bind States
                List<NState> objState = new List<NState>();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                ViewBag.StateList = items;

                NPropertyInfo objdesc = new NPropertyInfo();

                objdesc.StatesList = items;
                objdesc.ZipCodesList = objZipCodeList;
                objdesc.CityList = objCityList;

                #endregion

                #region Bind Property Description
                List<NPropertyDescription> ObjPropertyDesc = new List<NPropertyDescription>();
                ObjPropertyDesc = (from PropertyDesc in context.tblpropertydescription
                                   select new NPropertyDescription
                                   {
                                       PropertyDescriptionId = PropertyDesc.PropertyDescriptionId,
                                       Type = PropertyDesc.Type
                                   }).OrderBy(x => x.Type).ToList();
                List<SelectListItem> PropertyDescItem = new List<SelectListItem>();
                SelectListItem objSelectListItemProperty = new SelectListItem();
                objSelectListItemProperty.Text = "--Select--";
                objSelectListItemProperty.Value = "0";
                foreach (var t in ObjPropertyDesc)
                {
                    SelectListItem s1 = new SelectListItem();
                    s1.Text = t.Type.ToString();
                    s1.Value = t.PropertyDescriptionId.ToString();
                    PropertyDescItem.Add(s1);
                }
                PropertyDescItem = PropertyDescItem.OrderBy(x => x.Text).ToList();
                PropertyDescItem.Insert(0, objSelectListItemProperty);
                objdesc.PropertyDescriptionList = PropertyDescItem;
                #endregion

                return View("NCreateNewInspectionStep2", objdesc);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public ActionResult NCreateNewInspectopnStep2(NPropertyInfo objProperty, string BtnPrevious, string BtnNext)
        {
            if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
            {

                NInspectionOrderDetails objInspectionOrder = GetOrderDetails();
                objInspectionOrder.PropertyAddressCity = objProperty.PropertyAddressCity;
                objInspectionOrder.PropertyAddressState = objProperty.PropertyAddressState;
                objInspectionOrder.PropertyAddressZip = objProperty.PropertyAddressZip;
                objInspectionOrder.PropertyAddress = objProperty.PropertyAddress;
                objInspectionOrder.OwnerName = objProperty.OwnerName;
                objInspectionOrder.OwnerPhoneNumber = objProperty.OwnerPhoneNumber;
                objInspectionOrder.OwnerEmailAddress = objProperty.OwnerEmailAddress;
                objInspectionOrder.PropertyDescription = objProperty.PropertyDescription;
                objInspectionOrder.PropertyUnit = objProperty.PropertyUnit;
                objInspectionOrder.PropertySquareFootage = objProperty.PropertySquareFootage;
                objInspectionOrder.PropertyAddition = objProperty.PropertyAddition;
                objInspectionOrder.PropertyBedRoom = objProperty.PropertyBedRoom;
                objInspectionOrder.PropertyBaths = objProperty.PropertyBaths;
                objInspectionOrder.PropertyEstimatedMonthlyUtilites = objProperty.PropertyEstimatedMonthlyUtilites;
                objInspectionOrder.PropertyHomeAge = objProperty.PropertyHomeAge;
                objInspectionOrder.PropertyRoofAge = objProperty.PropertyRoofAge;
                objInspectionOrder.Direction = objProperty.Direction;

                #region Bind States
                List<NState> objState = new List<NState>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                ViewBag.StateList = items;
                NAgentInfo objAgent = new NAgentInfo();

                objAgent.StatesList = items;
                objAgent.ZipCodesList = objZipCodeList;
                objAgent.CityList = objCityList;

                #endregion


                return View("NCreateNewInspectionStep3", objAgent);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public ActionResult NCreateNewInspectionStep3(NAgentInfo objAgent, string BtnPrevious, string BtnNext)
        {
            if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
            {

                NInspectionOrderDetails objInspectionOrder = GetOrderDetails();
                NBuyerAgent objBuyerAgent = new NBuyerAgent();
                NSellerAgent objSellerAgent = new NSellerAgent();
                objBuyerAgent.BuyerAgentFirstName = objAgent.BuyerAgentFirstName;
                objBuyerAgent.BuyerAgentLastName = objAgent.BuyerAgentLastName;
                objBuyerAgent.BuyerAgentEmailAddress = objAgent.BuyerAgentEmailAddress;
                objBuyerAgent.BuyerAgentCity = objAgent.BuyerAgentCity;
                objBuyerAgent.AgentType = "2";
                objBuyerAgent.BuyerAgentOffice = objAgent.BuyerAgentOffice;
                objBuyerAgent.BuyerAgentPhoneDay = objAgent.BuyerAgentPhoneDay;
                objBuyerAgent.BuyerAgentState = objAgent.BuyerAgentState;
                objBuyerAgent.BuyerAgentStreetAddress = objAgent.BuyerAgentStreetAddress;
                objBuyerAgent.BuyerAgentZip = objAgent.BuyerAgentZip;


                objSellerAgent.SellerAgentFirstName = objAgent.SellerAgentFirstName;
                objSellerAgent.SellerAgentLastName = objAgent.SellerAgentLastName;
                objSellerAgent.SellerAgentEmailAddress = objAgent.SellerAgentEmailAddress;
                objSellerAgent.SellerAgentCity = objAgent.SellerAgentCity;
                objSellerAgent.AgentType = "1";
                objSellerAgent.SellerAgentOffice = objAgent.SellerAgentOffice;
                objSellerAgent.SellerAgentPhoneDay = objAgent.SellerAgentPhoneDay;
                objSellerAgent.SellerAgentState = objAgent.SellerAgentState;
                objSellerAgent.SellerAgentStreetAddress = objAgent.SellerAgentStreetAddress;
                objSellerAgent.SellerAgentZip = objAgent.SellerAgentZip;

                objInspectionOrder.buyerAgent = objBuyerAgent;
                objInspectionOrder.sellerAgent = objSellerAgent;



                return View("NCreateNewInspectionStep4");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult NCreateNewInspectionStep4(NScheduleInspection objSchedule, string BtnPrevious, string BtnNext)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
                {
                    NInspectionOrderDetails objInspectionOrderDetailsModel = GetOrderDetails();
                    objInspectionOrderDetailsModel.ScheduleInspectionDate = objSchedule.ScheduleInspectionDate;

                    tblinspectionorderdetail objtblInspectionOrderDetails = new tblinspectionorderdetail();
                    tblinspectionorder objtblInspectionOrder = new tblinspectionorder();
                    agentlibrary objAgentLibraryBuyer = new agentlibrary();
                    agentlibrary objAgentLibrarySeller = new agentlibrary();


                    #region Check If Inspector Exist

                    string assignedInspectorId = context.ZipCodeAssigned.FirstOrDefault(x => x.ZipCode == objInspectionOrderDetailsModel.PropertyAddressZip) != null ? context.ZipCodeAssigned.FirstOrDefault(x => x.ZipCode == objInspectionOrderDetailsModel.PropertyAddressZip).InspectorID : "";

                    int TotalInspection = CheckTotalInspectionScheduledForTheDay(assignedInspectorId, objInspectionOrderDetailsModel.ScheduleInspectionDate);

                    // if (TotalInspection < 4)
                    {

                        //var countReports = "";
                        int TotalInspectorReportCount = 0;
                        string UniqueId = "Empty";

                        if (assignedInspectorId != "")
                        {
                            var countReports = (from IO in context.tblinspectionorder
                                                join IOD in context.tblinspectionorderdetail on IO.InspectionOrderDetailId equals IOD.InspectionorderdetailsID
                                                where IO.InspectorID == assignedInspectorId && IOD.PropertyState == objInspectionOrderDetailsModel.PropertyAddressState
                                                select IO.InspectorID);
                            TotalInspectorReportCount = countReports.Count();
                            string state = context.tblstate.Where(x => x.StateId == objInspectionOrderDetailsModel.PropertyAddressState).FirstOrDefault().StateName;
                            UniqueId = state + context.InspectorDetail.Where(x => x.UserID == assignedInspectorId).Select(y => y.InspectorNo).FirstOrDefault() + (TotalInspectorReportCount + 1).ToString();
                        }


                    #endregion


                        #region Save Order Details

                        /*Requester Info*/
                        objtblInspectionOrderDetails.InspectionorderdetailsID = Guid.NewGuid().ToString();
                        //  objtblInspectionOrderDetails.InspectionOrderId = objtblInspectionOrder.InspectionOrderID;
                        objtblInspectionOrderDetails.RequesterFName = objInspectionOrderDetailsModel.RequestedFirstName.Trim();
                        objtblInspectionOrderDetails.RequesterLName = objInspectionOrderDetailsModel.RequestedLastName.Trim();
                        objtblInspectionOrderDetails.RequesterPhone = objInspectionOrderDetailsModel.PhoneNumber;
                        objtblInspectionOrderDetails.RequesterState = objInspectionOrderDetailsModel.RequestedByState;
                        objtblInspectionOrderDetails.RequesterCity = objInspectionOrderDetailsModel.RequestedByCity;
                        objtblInspectionOrderDetails.RequesterZip = objInspectionOrderDetailsModel.RequestedByZip;
                        objtblInspectionOrderDetails.RequestedByEmailAddress = objInspectionOrderDetailsModel.RequestedByEmailAddress.Trim();
                        objtblInspectionOrderDetails.RequesterAddress = objInspectionOrderDetailsModel.RequestedByAddress.Trim();
                        objtblInspectionOrderDetails.RequesterType = objInspectionOrderDetailsModel.RequesterType;

                        /*Property Info*/
                        objtblInspectionOrderDetails.PropertyAddress = objInspectionOrderDetailsModel.PropertyAddress;
                        objtblInspectionOrderDetails.OwnerPhone = objInspectionOrderDetailsModel.OwnerPhoneNumber;
                        objtblInspectionOrderDetails.PropertyState = objInspectionOrderDetailsModel.PropertyAddressState;
                        objtblInspectionOrderDetails.PropertyCity = objInspectionOrderDetailsModel.PropertyAddressCity;
                        objtblInspectionOrderDetails.PropertyZip = objInspectionOrderDetailsModel.PropertyAddressZip;
                        objtblInspectionOrderDetails.PropertyEmail = objInspectionOrderDetailsModel.OwnerEmailAddress;
                        objtblInspectionOrderDetails.OwnerName = objInspectionOrderDetailsModel.OwnerName;
                        objtblInspectionOrderDetails.PropertyDescription = objInspectionOrderDetailsModel.PropertyDescription;
                        objtblInspectionOrderDetails.Units = objInspectionOrderDetailsModel.PropertyUnit;
                        objtblInspectionOrderDetails.SqureFootage = objInspectionOrderDetailsModel.PropertySquareFootage;
                        objtblInspectionOrderDetails.Additions_Alterations = objInspectionOrderDetailsModel.PropertyAddition;
                        objtblInspectionOrderDetails.Bedrooms = objInspectionOrderDetailsModel.PropertyBedRoom;
                        objtblInspectionOrderDetails.Bathrooms = objInspectionOrderDetailsModel.PropertyBaths;
                        objtblInspectionOrderDetails.EstimatedMonthlyUtilities = objInspectionOrderDetailsModel.PropertyEstimatedMonthlyUtilites;
                        objtblInspectionOrderDetails.YearBuilt = objInspectionOrderDetailsModel.PropertyHomeAge;
                        objtblInspectionOrderDetails.RoofAge = objInspectionOrderDetailsModel.PropertyRoofAge;
                        objtblInspectionOrderDetails.Directions = objInspectionOrderDetailsModel.Direction;

                             
                        /*Additional Testing*/
                        if (Session["AdditionalTesting"] != null)
                        {
                            AdditionalTesting objAdditionalTesting = new AdditionalTesting();
                            objAdditionalTesting = (Session["AdditionalTesting"]) as AdditionalTesting;
                            objtblInspectionOrderDetails.FoundationAnalysis = objAdditionalTesting.FoundationAnalysis;
                            objtblInspectionOrderDetails.MethTesting = objAdditionalTesting.MethTesting;
                            objtblInspectionOrderDetails.RadonTesting = objAdditionalTesting.RadonTesting;
                            objtblInspectionOrderDetails.MoldMoistureAnalysis = objAdditionalTesting.MoldMoistureAnalysis;
                            objtblInspectionOrderDetails.GasLeakDetection = objAdditionalTesting.GasLeakDetection;

                            if (objAdditionalTesting.MethTestingAdditionalTest > 0)
                                objtblInspectionOrderDetails.MethTestingAdditionalTest = true;
                            else
                                objtblInspectionOrderDetails.MethTestingAdditionalTest = false;

                            if (objAdditionalTesting.MoldMoistureAdditionalTest > 0)
                                objtblInspectionOrderDetails.MoldMoistureAdditionalTest = true;
                            else
                                objtblInspectionOrderDetails.MoldMoistureAdditionalTest = false;


                            paymentaddionaltesting objPayment = new paymentaddionaltesting();
                            
                            objPayment.ReportId = objtblInspectionOrderDetails.InspectionorderdetailsID;
                            objPayment.MethTesting = objAdditionalTesting.MethTestingPrice;
                            objPayment.RadonTesting = objAdditionalTesting.RadonTestingPrice;
                            objPayment.MoldMoistureAnalysis = objAdditionalTesting.MoldMoistureAnalysisPrice;
                            objPayment.MoldMoistureAdditionalTest = objAdditionalTesting.MoldMoistureAnalysisPrice;
                            objPayment.MethTestingAdditionalTest = objAdditionalTesting.MethTestingAdditionalTest;
                            objPayment.FoundationAnalysis = objAdditionalTesting.FoundationAnalysisPrice;
                            objPayment.GasLeakDetection = objAdditionalTesting.GasLeakDetectionPrice;
                            context.paymentaddionaltesting.Add(objPayment);
                            Session["AdditionalTesting"] = null;
                        }


                        context.tblinspectionorderdetail.Add(objtblInspectionOrderDetails);

                        /*Buyer Agent*/

                        objAgentLibraryBuyer.AgentID = Guid.NewGuid().ToString();
                        objAgentLibraryBuyer.InspectionOrderID = objtblInspectionOrderDetails.InspectionorderdetailsID;
                        objAgentLibraryBuyer.AgentType = "2";
                        objAgentLibraryBuyer.AgentFirstName = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentFirstName;
                        objAgentLibraryBuyer.AgentLastName = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentLastName;
                        objAgentLibraryBuyer.AgentOffice = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentOffice;
                        objAgentLibraryBuyer.AgentPhoneDay = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentPhoneDay;
                        objAgentLibraryBuyer.AgentStreetAddress = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentStreetAddress;
                        objAgentLibraryBuyer.AgentCity = (objInspectionOrderDetailsModel.buyerAgent.BuyerAgentCity);
                        objAgentLibraryBuyer.AgentState = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentState; ;
                        objAgentLibraryBuyer.AgentZip = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentZip;
                        objAgentLibraryBuyer.AgentEmailAddress = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentEmailAddress;
                        context.AgentLibraries.Add(objAgentLibraryBuyer);



                        /*Seller Agent*/
                        objAgentLibrarySeller.AgentID = Guid.NewGuid().ToString();
                        objAgentLibrarySeller.InspectionOrderID = objtblInspectionOrderDetails.InspectionorderdetailsID;
                        objAgentLibrarySeller.AgentType = "1";
                        objAgentLibrarySeller.AgentFirstName = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentFirstName;
                        objAgentLibrarySeller.AgentLastName = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentLastName;
                        objAgentLibrarySeller.AgentOffice = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentOffice;
                        objAgentLibrarySeller.AgentPhoneDay = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentPhoneDay;
                        objAgentLibrarySeller.AgentStreetAddress = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentStreetAddress;
                        objAgentLibrarySeller.AgentCity = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentCity;
                        objAgentLibrarySeller.AgentState = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentState; ;
                        objAgentLibrarySeller.AgentZip = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentZip;
                        objAgentLibrarySeller.AgentEmailAddress = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentEmailAddress;
                        context.AgentLibraries.Add(objAgentLibrarySeller);

                        #endregion

                        #region Save data in QrCode Table
                        if (UniqueId != string.Empty)
                        {
                            tblqrcode objQr = new tblqrcode();
                            objQr.InspectorId = assignedInspectorId;
                            objQr.ReportNo = UniqueId;
                            objQr.QrCodeNumber = TotalInspectorReportCount + 1;
                            objQr.IsPrinted = false;
                            objQr.OrderId = objtblInspectionOrderDetails.InspectionorderdetailsID;
                            objQr.Createddate = System.DateTime.Now;
                            context.tblQRCode.Add(objQr);
                        }
                        #endregion
                        #region Save Data in Inspection Order table
                        objtblInspectionOrder.InspectionOrderID = Guid.NewGuid().ToString();
                        objtblInspectionOrder.InspectionOrderDetailId = objtblInspectionOrderDetails.InspectionorderdetailsID;
                        objtblInspectionOrder.RequesterID = Sessions.LoggedInUser.UserId;
                        objtblInspectionOrder.InspectorID = assignedInspectorId;
                        objtblInspectionOrder.ScheduledDate = objInspectionOrderDetailsModel.ScheduleInspectionDate;
                        objtblInspectionOrder.AssignedByAdmin = false;
                        objtblInspectionOrder.InspectionStatus = "Pending";
                        objtblInspectionOrder.IsAcceptedOrRejected = false;
                        objtblInspectionOrder.IsRejectedByInspector = false;
                        objtblInspectionOrder.IsUpdatable = false;
                        objtblInspectionOrder.ReportNo = UniqueId;
                        objtblInspectionOrder.CreatedBy = Sessions.LoggedInUser.UserId;
                        objtblInspectionOrder.Createddate = System.DateTime.Now;
                        objtblInspectionOrder.IsDeleted = false;
                        objtblInspectionOrder.IsActive = true;
                        context.tblinspectionorder.Add(objtblInspectionOrder);

                        if (assignedInspectorId == "")
                        {
                            notificationdetail objNotification = new notificationdetail();
                            objNotification.NotifcationFrom = Sessions.LoggedInUser.UserId;
                            objNotification.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                            objNotification.NotificationTypeID = 2;
                            objNotification.IsRead = false;
                            objNotification.CreatedDate = System.DateTime.Now;
                            objNotification.RequestID = objtblInspectionOrderDetails.InspectionorderdetailsID;
                            context.NotificationDetail.Add(objNotification);
                        }

                        context.SaveChanges();

                        #endregion


                        return Json("Success", JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    return Json("Session Over", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private NInspectionOrderDetails GetOrderDetails()
        {

            if (Session["OrderDetails"] == null)
            {

                Session["OrderDetails"] = new NInspectionOrderDetails();

            }

            return (NInspectionOrderDetails)Session["OrderDetails"];

        }



        private void RemoveCricketer()
        {

            Session.Remove("cricketer");

        }



        #region Create New Inspection Post
        /// <summary>
        /// function to Create New Inspection Form
        /// </summary>
        /// <param name="NInspectionOrderDetailsModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateNewInspection(NInspectionOrderDetails objInspectionOrderDetailsModel)
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    tblinspectionorderdetail objtblInspectionOrderDetails = new tblinspectionorderdetail();
                    tblinspectionorder objtblInspectionOrder = new tblinspectionorder();
                    agentlibrary objAgentLibraryBuyer = new agentlibrary();
                    agentlibrary objAgentLibrarySeller = new agentlibrary();


                    #region Check If Inspector Exist

                    string assignedInspectorId = context.ZipCodeAssigned.FirstOrDefault(x => x.ZipCode == objInspectionOrderDetailsModel.PropertyAddressZip) != null ? context.ZipCodeAssigned.FirstOrDefault(x => x.ZipCode == objInspectionOrderDetailsModel.PropertyAddressZip).InspectorID : "";

                    int TotalInspection = CheckTotalInspectionScheduledForTheDay(assignedInspectorId, objInspectionOrderDetailsModel.ScheduleInspectionDate);

                    if (TotalInspection < 4)
                    {

                        //var countReports = "";
                        int TotalInspectorReportCount = 0;
                        string UniqueId = "Empty";
                        if (assignedInspectorId != "")
                        {
                            var countReports = (from IO in context.tblinspectionorder
                                                join IOD in context.tblinspectionorderdetail on IO.InspectionOrderID equals IOD.InspectionorderdetailsID
                                                where IO.InspectorID == assignedInspectorId && IOD.PropertyState == objInspectionOrderDetailsModel.PropertyAddressState
                                                select IO.InspectorID);
                            TotalInspectorReportCount = countReports.Count();
                            UniqueId = objInspectionOrderDetailsModel.PropertyAddressState + context.InspectorDetail.Where(x => x.UserID == assignedInspectorId).Select(y => y.InspectorNo).FirstOrDefault() + (TotalInspectorReportCount + 1).ToString();
                        }

                    #endregion


                        #region Save Order Details

                        /*Requester Info*/
                        objtblInspectionOrderDetails.InspectionorderdetailsID = Guid.NewGuid().ToString();
                        //  objtblInspectionOrderDetails.InspectionOrderId = objtblInspectionOrder.InspectionOrderID;
                        objtblInspectionOrderDetails.RequesterFName = objInspectionOrderDetailsModel.RequestedFirstName.Trim();
                        objtblInspectionOrderDetails.RequesterLName = objInspectionOrderDetailsModel.RequestedLastName.Trim();
                        objtblInspectionOrderDetails.RequesterPhone = objInspectionOrderDetailsModel.PhoneNumber;
                        objtblInspectionOrderDetails.RequesterState = objInspectionOrderDetailsModel.RequestedByState;
                        objtblInspectionOrderDetails.RequesterCity = objInspectionOrderDetailsModel.RequestedByCity;
                        objtblInspectionOrderDetails.RequesterZip = objInspectionOrderDetailsModel.RequestedByZip;
                        objtblInspectionOrderDetails.RequestedByEmailAddress = objInspectionOrderDetailsModel.RequestedByEmailAddress.Trim();
                        objtblInspectionOrderDetails.RequesterAddress = objInspectionOrderDetailsModel.RequestedByAddress.Trim();
                        objtblInspectionOrderDetails.RequesterType = objInspectionOrderDetailsModel.RequesterType;

                        /*Property Info*/

                        objtblInspectionOrderDetails.PropertyDescription = objInspectionOrderDetailsModel.PropertyDescription;
                        objtblInspectionOrderDetails.Units = objInspectionOrderDetailsModel.PropertyUnit;
                        objtblInspectionOrderDetails.SqureFootage = objInspectionOrderDetailsModel.PropertySquareFootage;
                        objtblInspectionOrderDetails.Additions_Alterations = objInspectionOrderDetailsModel.PropertyAddition;
                        objtblInspectionOrderDetails.Bedrooms = objInspectionOrderDetailsModel.PropertyBedRoom;
                        objtblInspectionOrderDetails.Bathrooms = objInspectionOrderDetailsModel.PropertyBaths;
                        objtblInspectionOrderDetails.EstimatedMonthlyUtilities = objInspectionOrderDetailsModel.PropertyEstimatedMonthlyUtilites;
                        objtblInspectionOrderDetails.YearBuilt = objInspectionOrderDetailsModel.PropertyHomeAge;
                        objtblInspectionOrderDetails.RoofAge = objInspectionOrderDetailsModel.PropertyRoofAge;
                        objtblInspectionOrderDetails.Directions = objInspectionOrderDetailsModel.Direction;
                        context.tblinspectionorderdetail.Add(objtblInspectionOrderDetails);

                        /*Buyer Agent*/

                        objAgentLibraryBuyer.AgentID = Guid.NewGuid().ToString();
                        objAgentLibraryBuyer.InspectionOrderID = objInspectionOrderDetailsModel.InspectionOrderId;
                        objAgentLibraryBuyer.AgentType = "2";
                        objAgentLibraryBuyer.AgentFirstName = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentFirstName;
                        objAgentLibraryBuyer.AgentLastName = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentLastName;
                        objAgentLibraryBuyer.AgentOffice = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentOffice;
                        objAgentLibraryBuyer.AgentPhoneDay = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentPhoneDay;
                        objAgentLibraryBuyer.AgentStreetAddress = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentStreetAddress;
                        objAgentLibraryBuyer.AgentCity = (objInspectionOrderDetailsModel.buyerAgent.BuyerAgentCity);
                        objAgentLibraryBuyer.AgentState = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentState; ;
                        objAgentLibraryBuyer.AgentZip = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentZip;
                        objAgentLibraryBuyer.AgentEmailAddress = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentEmailAddress;
                        context.AgentLibraries.Add(objAgentLibraryBuyer);


                        /*Seller Agent*/
                        objAgentLibrarySeller.AgentID = Guid.NewGuid().ToString();
                        objAgentLibrarySeller.InspectionOrderID = objInspectionOrderDetailsModel.InspectionOrderId;
                        objAgentLibrarySeller.AgentType = "1";
                        objAgentLibrarySeller.AgentFirstName = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentFirstName;
                        objAgentLibrarySeller.AgentLastName = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentLastName;
                        objAgentLibrarySeller.AgentOffice = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentOffice;
                        objAgentLibrarySeller.AgentPhoneDay = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentPhoneDay;
                        objAgentLibrarySeller.AgentStreetAddress = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentStreetAddress;
                        objAgentLibrarySeller.AgentCity = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentCity;
                        objAgentLibrarySeller.AgentState = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentState; ;
                        objAgentLibrarySeller.AgentZip = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentZip;
                        objAgentLibrarySeller.AgentEmailAddress = objInspectionOrderDetailsModel.buyerAgent.BuyerAgentEmailAddress;
                        context.AgentLibraries.Add(objAgentLibrarySeller);

                        context.SaveChanges();

                        #endregion


                        #region Save Data in Inspection Order table
                        objtblInspectionOrder.InspectionOrderID = Guid.NewGuid().ToString();
                        objtblInspectionOrder.InspectionOrderDetailId = objtblInspectionOrderDetails.InspectionorderdetailsID;
                        objtblInspectionOrder.RequesterID = Sessions.LoggedInUser.UserId;
                        objtblInspectionOrder.InspectorID = assignedInspectorId;
                        objtblInspectionOrder.ScheduledDate = objInspectionOrderDetailsModel.ScheduleInspectionDate;
                        objtblInspectionOrder.AssignedByAdmin = false;
                        objtblInspectionOrder.InspectionStatus = "Pending";
                        objtblInspectionOrder.IsAcceptedOrRejected = false;
                        objtblInspectionOrder.IsRejectedByInspector = false;
                        objtblInspectionOrder.IsUpdatable = false;
                        objtblInspectionOrder.ReportNo = UniqueId;
                        objtblInspectionOrder.CreatedBy = Sessions.LoggedInUser.UserId;
                        objtblInspectionOrder.Createddate = System.DateTime.Now;
                        objtblInspectionOrder.IsDeleted = false;
                        objtblInspectionOrder.IsActive = true;
                        context.tblinspectionorder.Add(objtblInspectionOrder);
                        #endregion




                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Select Other Date", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Session Out", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region functions to get count of order scheduled for a particular day
        /// <summary>
        /// functions to get count of order scheduled for a particular day
        /// </summary>
        /// <param name="inspectorid"></param>
        /// /// <param name="Schedule Date"></param>
        /// <returns></returns>
        private int CheckTotalInspectionScheduledForTheDay(string InspectorId, DateTime? Scheduledate)
        {
            var count = from IO in context.tblinspectionorder
                        where IO.InspectorID == InspectorId && IO.ScheduledDate == Scheduledate
                        select IO.InspectionOrderID;

            return Convert.ToInt32(count.Count());
        }

        #endregion

    }
}
