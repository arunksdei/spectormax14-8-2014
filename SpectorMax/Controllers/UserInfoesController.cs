using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;
using SpectorMax.Entities.Classes;
using System.Web.Caching;
using System.Web.Security;
using Newtonsoft.Json;
using System.Text;
using SpectorMax.Utilities.Classes;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Net;

namespace SpectorMax.Controllers
{
    [NoCache]
    public class UserInfoesController : BaseController
    {
        public LoggedInUserDetails UserInformation { get { return (LoggedInUserDetails)Session["UserDetails"]; } }
        private SpectorMaxContext context = new SpectorMaxContext();
        string connStr = ConfigurationManager.ConnectionStrings["SpectorMaxConnectionString"].ConnectionString;
        MessageToShow _messageToShow = new MessageToShow();
        //
        // GET: /UserInfoes/

        public ViewResult Index()
        {

            return View(context.UserInfoes.ToList());

        }


        /// <summary>
        ///  GET: / method for loging to registered user.
        /// </summary>
        /// <returns></returns>
        public ViewResult LogOn()
        {
            HttpCookie userDetails = Request.Cookies["UserDetails"] as HttpCookie;
            LogInModel logInModel = new LogInModel();
            if (userDetails != null)
            {
                logInModel.Email = userDetails["Email"];
                //logInModel.Password = userDetails["Password"];
            }
            return View(logInModel);
        }

        /// <summary>
        /// POST: // method for loging to registered user.
        /// </summary>
        /// <param name="logInModel"></param>
        /// <returns></returns>

        [HttpPost] //JsonResult       
        public ActionResult LogOn(LogInModel logInModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string UniqueId = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["UniqueId"];
                    string amount = logInModel.paymentamount;
                    HttpCookie userDetails = new HttpCookie("UserDetails");
                    string message = string.Empty;
                    var result = context.UserInfoes.FirstOrDefault(x => x.EmailAddress == logInModel.Email && x.Password == logInModel.Password && x.IsDeleted == false);
                    if (result == null)
                    {
                        _messageToShow.Message = "Invalid email or password";

                        return RedirectToAction("Index", "Home", new { message = _messageToShow.Message });
                    }

                    else if (result.IsActive == false)
                    {
                        _messageToShow.Message = "Your account is inactive please contact to your admin.";
                        return RedirectToAction("Index", "Home", new { message = _messageToShow.Message });
                    }
                    else
                    {
                        if (logInModel.RememberMe == true)
                        {
                            string userinfo = (Convert.ToString(logInModel.Email) + "~/!2@?<:>~" + Convert.ToString(logInModel.Password));
                            HttpCookie usercredentialsCookie = new HttpCookie("usercredentials");
                            usercredentialsCookie.Values.Add("meda", userinfo);
                            usercredentialsCookie.Expires = DateTime.Now.AddYears(1);
                            Response.Cookies.Add(usercredentialsCookie);
                        }
                        else
                        {
                            Response.Cookies["usercredentials"].Expires = DateTime.Now.AddYears(-1);
                        }

                        string userRoleName = context.Roles.FirstOrDefault(x => x.RoleID == result.Role).RoleName;
                        _messageToShow.Message = "Success";
                        _messageToShow.UserID = result.ID;
                        _messageToShow.UserRoleType = userRoleName;
                        LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails();

                        loggedInUserDetails.UserId = result.ID;
                        loggedInUserDetails.RoleId = result.Role;
                        loggedInUserDetails.RoleName = userRoleName;
                        loggedInUserDetails.FirstName = result.FirstName;
                        loggedInUserDetails.LastName = result.LastName;
                        loggedInUserDetails.EmailAddress = result.EmailAddress;
                        loggedInUserDetails.Zipcode = result.ZipCode;
                        loggedInUserDetails.rolecount = context.UserInfoes.Where(x => x.EmailAddress == logInModel.Email).ToList().Count;


                        //Store the User's Details to a session
                        Session["UserDetails"] = loggedInUserDetails;
                        LoggedInUser.UserId = result.ID;
                        LoggedInUser.UserId = result.ID;
                        LoggedInUser.RoleId = result.Role;
                        LoggedInUser.RoleName = userRoleName;
                        LoggedInUser.FirstName = result.FirstName;
                        LoggedInUser.LastName = result.LastName;
                        LoggedInUser.EmailAddress = result.EmailAddress;
                        LoggedInUser.Zipcode = result.ZipCode;

                        if (logInModel.RememberMe == true)
                        {
                            userDetails["Email"] = logInModel.Email;
                            userDetails.Expires = DateTime.Now.AddDays(30);
                            Response.Cookies.Add(userDetails);
                        }
                        if (userRoleName == "Home Seller")
                        {
                            return RedirectToAction("SellerDashboardNew", "UserInfoes", new { userid = result.ID });
                        }
                        else if (userRoleName == "Home Buyer")
                        {
                            bool doublerole = context.UserInfoes.Where(x => x.EmailAddress == Sessions.LoggedInUser.EmailAddress).ToList().Count > 1 ? true : false;
                            if (UniqueId != null && logInModel.paymentamount != null && logInModel.typePayment != null)
                            {
                                string Orderid = context.InspectionOrders.FirstOrDefault(x => x.UniqueID == UniqueId).InspectionOrderID;

                                var PlanPayment = (from payment in context.Payments
                                                   where payment.UserID == Sessions.LoggedInUser.UserId
                                                   orderby payment.CreatedDate descending
                                                   select
                                                   payment).Take(1);


                                if (PlanPayment.Any() && PlanPayment.Select(x => x.PlanValidity).First() >= System.DateTime.Now && PlanPayment.Select(x => x.PaymentType).First() == "PaymentPlan")
                                {
                                    return RedirectToAction("ViewDetailReport", "Report", new { inspectionOrderId = Orderid, PaymentType = "PaymentPlan" });
                                }
                                else
                                {
                                    if (logInModel.typePayment == "OneTimePaymentPlan")
                                        return RedirectToAction("Payment", "Payments", new { userid = result.ID, userRoleId = result.Role, amount = amount, amountDescription = "You are purchasing One time payment for purchase detailed report", paymentType = "OneTimePaymentPlan", inspectionOrderId = Orderid });
                                    else
                                        return RedirectToAction("Payment", "Payments", new { userid = result.ID, userRoleId = result.Role, amount = amount, amountDescription = "You are purchasing  payment plan for purchase detailed report", paymentType = "PaymentPlan", inspectionOrderId = Orderid, Validity = 60 });
                                }
                            }
                            else
                            {
                                return RedirectToAction("BuyerDashboardNew", "UserInfoes", new { userid = result.ID, IsBoth = doublerole });
                            }
                        }
                        else if (userRoleName == "Property Inspector")
                        {
                            return RedirectToAction("InspectorDashboard", "UserInfoes", new { userid = result.ID });
                        }

                        else
                        {
                            _messageToShow.Message = "You are not assigned to any role. Please contact to your Administrator.";
                            return RedirectToAction("Index", "Home", new { message = _messageToShow.Message });
                        }
                    }
                }
                catch (Exception ex)
                {
                    _messageToShow.Message = ex.Message;
                    return RedirectToAction("Index", "Home", new { message = _messageToShow.Message });
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        public JsonResult JSONLogOn(string UserName, string Password, bool rememberme)
        {
            try
            {
                var result = context.UserInfoes.FirstOrDefault(x => x.EmailAddress == UserName && x.Password == Password && x.IsDeleted == false);
                if (result == null)
                {
                    _messageToShow.Message = "Invalid email or password";
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
                else if (result.IsActive == false && result.IsEmailVerified == false)
                {
                    _messageToShow.Message = "Your account is not verified. Please check your inbox and verify your account.";
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
                else if (result.IsActive == false)
                {
                    _messageToShow.Message = "Your account is inactive please contact to your admin.";
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string userRoleName = context.Roles.FirstOrDefault(x => x.RoleID == result.Role).RoleName;
                    _messageToShow.Message = "Success";
                    _messageToShow.UserID = result.ID;
                    _messageToShow.UserRoleType = userRoleName;
                    LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails();
                    loggedInUserDetails.UserId = result.ID;
                    loggedInUserDetails.RoleId = result.Role;
                    loggedInUserDetails.RoleName = userRoleName;
                    loggedInUserDetails.FirstName = result.FirstName;
                    loggedInUserDetails.LastName = result.LastName;
                    loggedInUserDetails.EmailAddress = result.EmailAddress;
                    loggedInUserDetails.Zipcode = result.ZipCode;
                    loggedInUserDetails.rolecount = context.UserInfoes.Where(x => x.EmailAddress == UserName).ToList().Count;
                    loggedInUserDetails.StateId = result.StateId;
                    loggedInUserDetails.CityId = result.CityId;
                    loggedInUserDetails.ZipId = result.ZipId;
                    loggedInUserDetails.PhoneNo = result.PhoneNo;
                    loggedInUserDetails.Address = result.Address;
                    loggedInUserDetails.ProfileUpdated = result.IsProfileUpdated;
                    //Store the User's Details to a session
                    Session["UserDetails"] = loggedInUserDetails;

                    int totalNotification = context.UsersNotification.Count(x => x.To == Sessions.LoggedInUser.UserId && x.IsRead == false && x.IsDeleted == false);
                    loggedInUserDetails.NotificationCount = totalNotification.ToString();

                    Session["TotalNotificationCount"] = totalNotification.ToString();

                    //LoggedInUser.UserId = result.ID;
                    //LoggedInUser.UserId = result.ID;
                    //LoggedInUser.RoleId = result.Role;
                    //LoggedInUser.RoleName = userRoleName;
                    //LoggedInUser.FirstName = result.FirstName;
                    //LoggedInUser.LastName = result.LastName;
                    //LoggedInUser.EmailAddress = result.EmailAddress;
                    //LoggedInUser.Zipcode = result.ZipCode;

                    HttpCookie userDetails = new HttpCookie("UserDetails");
                    if (rememberme)
                    {
                        string userinfo = (Convert.ToString(UserName) + "~/!2@?<:>~" + Convert.ToString(Password));
                        HttpCookie usercredentialsCookie = new HttpCookie("usercredentials");
                        usercredentialsCookie.Values.Add("meda", userinfo);
                        usercredentialsCookie.Expires = DateTime.Now.AddYears(1);
                        Response.Cookies.Add(usercredentialsCookie);

                        userDetails["Email"] = UserName;
                        userDetails.Expires = DateTime.Now.AddDays(30);
                        Response.Cookies.Add(userDetails);
                    }
                    else
                    {
                        Response.Cookies["usercredentials"].Expires = DateTime.Now.AddYears(-1);
                    }
                    //if (userRoleName == "Home Seller")
                    //{
                    //    _messageToShow.Result = "Home Seller";
                    //    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                    //}
                    //if (userRoleName == "Home Buyer")
                    //{
                    //    _messageToShow.Result = "Home Buyer";
                    //    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                    //}
                    //if (userRoleName == "Property Inspector")
                    //{
                    //    _messageToShow.Result = "Property Inspector";
                    //    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    _messageToShow.Message = "You are not assigned to any role. Please contact to your Administrator.";
                    //    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                    //}
                    //return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(context.Database.Connection.ConnectionString + "$" + ex.Message + "$" + ex.InnerException);
            }
        }
        /// <summary>
        /// method logout to logged in user.
        /// </summary>
        /// <returns></returns>

        public JsonResult JSONLogOnAddtionalTesting(string UserName,
                                    string Password,
                                    bool rememberme,
                                    bool foundationservices,
                                    bool GasLeakDetection,
                                    bool MoldServices,
                                    int MoistureAnalysis_Qty,
                                    bool RadonServices,
                                    bool MethServices,
                                    int Meth_Qty,
                                    int TotolPrice,
                                    string foundationservicesPrice,
                                    string GasLeakDetectionPrice,
                                    string MoldServicesPrice,
                                    string RadonServicesPrice,
                                    string MethServicesPrice)
        {
            try
            {
                var result = context.UserInfoes.FirstOrDefault(x => x.EmailAddress == UserName && x.Password == Password && x.IsDeleted == false);
                if (result == null)
                {
                    _messageToShow.Message = "Invalid email or password";
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
                else if (result.IsActive == false)
                {
                    _messageToShow.Message = "Your account is inactive please contact to your admin.";
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string userRoleName = context.Roles.FirstOrDefault(x => x.RoleID == result.Role).RoleName;
                    _messageToShow.Message = "Success";
                    _messageToShow.UserID = result.ID;
                    _messageToShow.UserRoleType = userRoleName;
                    LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails();

                    loggedInUserDetails.UserId = result.ID;
                    loggedInUserDetails.RoleId = result.Role;
                    loggedInUserDetails.RoleName = userRoleName;
                    loggedInUserDetails.FirstName = result.FirstName;
                    loggedInUserDetails.LastName = result.LastName;
                    loggedInUserDetails.EmailAddress = result.EmailAddress;
                    loggedInUserDetails.Zipcode = result.ZipCode;
                    loggedInUserDetails.rolecount = context.UserInfoes.Where(x => x.EmailAddress == UserName).ToList().Count;

                    //Store the User's Details to a session
                    Session["UserDetails"] = loggedInUserDetails;


                    //Session created to store the Additional Testing selected.
                    AdditionalTesting objAdditionalTesting = new AdditionalTesting();
                    objAdditionalTesting.FoundationAnalysis = foundationservices;
                    objAdditionalTesting.FoundationAnalysisPrice = Convert.ToSingle(foundationservicesPrice);

                    objAdditionalTesting.GasLeakDetection = GasLeakDetection;
                    objAdditionalTesting.GasLeakDetectionPrice = Convert.ToSingle(GasLeakDetectionPrice);

                    objAdditionalTesting.MethTesting = MethServices;
                    objAdditionalTesting.MethTestingPrice = Convert.ToSingle(MethServicesPrice);

                    objAdditionalTesting.RadonTesting = RadonServices;
                    objAdditionalTesting.RadonTestingPrice = Convert.ToSingle(RadonServicesPrice);

                    objAdditionalTesting.MoldMoistureAnalysis = MoldServices;
                    objAdditionalTesting.MoldMoistureAnalysisPrice = Convert.ToSingle(MoldServicesPrice);

                    objAdditionalTesting.MoldMoistureAdditionalTest = MoistureAnalysis_Qty;
                    objAdditionalTesting.MethTestingAdditionalTest = Meth_Qty;
                    objAdditionalTesting.TotalPrice = TotolPrice;
                    Session["AdditionalTesting"] = objAdditionalTesting;
                    int totalNotification = context.UsersNotification.Count(x => x.To == Sessions.LoggedInUser.UserId && x.IsRead == false && x.IsDeleted == false);
                    loggedInUserDetails.NotificationCount = totalNotification.ToString();

                    Session["TotalNotificationCount"] = totalNotification.ToString();

                    HttpCookie userDetails = new HttpCookie("UserDetails");
                    if (rememberme)
                    {
                        string userinfo = (Convert.ToString(UserName) + "~/!2@?<:>~" + Convert.ToString(Password));
                        HttpCookie usercredentialsCookie = new HttpCookie("usercredentials");
                        usercredentialsCookie.Values.Add("meda", userinfo);
                        usercredentialsCookie.Expires = DateTime.Now.AddYears(1);
                        Response.Cookies.Add(usercredentialsCookie);

                        userDetails["Email"] = UserName;
                        userDetails.Expires = DateTime.Now.AddDays(30);
                        Response.Cookies.Add(userDetails);
                    }
                    else
                    {
                        Response.Cookies["usercredentials"].Expires = DateTime.Now.AddYears(-1);
                    }
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(context.Database.Connection.ConnectionString + "$" + ex.Message + "$" + ex.InnerException);
            }
        }


        public ActionResult LogOff()
        {
            //Session["UserDetails"] = null;
            Session.Abandon();
            return RedirectToAction("HomePartial", "Home");
        }

        //
        // GET: /UserInfoes/Details/5

        public ViewResult Details(string id)
        {
            try
            {
                userinfo userinfo = context.UserInfoes.Single(x => x.ID == id);
                return View(userinfo);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        /// <summary>
        /// GET: // method for register new user
        /// </summary>
        /// <returns></returns>

        public ActionResult Create()
        {
            try
            {
                UserInfoPoco userInfo = new UserInfoPoco();
                if (ModelState.IsValid)
                {
                    List<UserRole> roles = (from role in context.Roles
                                            where role.RoleName != "Admin" && role.RoleName != "Sub-Admin" && role.RoleName != "Property Inspector"
                                            select new UserRole()
                                            {
                                                RoleID = role.RoleID,
                                                RoleName = role.RoleName
                                            }).ToList();

                    userInfo.UserRoleList = roles;

                    /*Fetching Security Question*/
                    List<SelectListItem> Question = new List<SelectListItem>();
                    var SecurityQuestion = context.SecurityQuestion.ToList();
                    foreach (var t in SecurityQuestion)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.SecurityQuestion1;
                        s.Value = t.ID.ToString();
                        Question.Add(s);
                    }
                    userInfo.SecurityQuestion1 = Question;

                    /*Fetching States*/
                    //var States = context.ZipCode.Select(x => x.State).Distinct();
                    //List<SelectListItem> items = new List<SelectListItem>();
                    //List<SelectListItem> objCityList = new List<SelectListItem>();
                    //List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    //SelectListItem objSelectListItem = new SelectListItem();
                    //objSelectListItem.Text = "--Select--";
                    //objSelectListItem.Value = "--Select--";

                    //objCityList.Add(objSelectListItem);
                    //objZipCodeList.Add(objSelectListItem);
                    //foreach (var t in States)
                    //{
                    //    SelectListItem s = new SelectListItem();
                    //    s.Text = t.ToString();
                    //    s.Value = t.ToString();
                    //    items.Add(s);
                    //}
                    //items = items.OrderBy(x => x.Text).ToList();
                    //items.Insert(0, objSelectListItem);
                    //userInfo.StatesList = items;

                    //userInfo.ZipCodesList = objZipCodeList;
                    //userInfo.CityList = objCityList;
                    List<NState> objState = new List<NState>();
                    objState = (from state in context.tblstate
                                where state.IsActive == true
                                select new NState
                                {
                                    StateId = state.StateId,
                                    StateName = state.StateName
                                }).OrderBy(x => x.StateName).ToList();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "--Select--";
                    //  items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in objState)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.StateName.ToString();
                        s.Value = t.StateId.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    userInfo.StatesList = items;
                    userInfo.ZipCodesList = objZipCodeList;
                    userInfo.CityList = objCityList;
                }
                return View(userInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// POST: // method for register new user for application.
        /// </summary>
        /// <param name="userInfoPoco"></param>
        /// <returns></returns>

        [HttpPost]
        public ActionResult Create(UserInfoPoco userInfoPoco)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (context.UserInfoes.FirstOrDefault(x => x.EmailAddress == userInfoPoco.EmailAddress && x.IsDeleted == false) == null)
                    {
                        if (userInfoPoco != null)
                        {
                            string UniqueId = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["UniqueId"];
                            string role = string.Empty;
                            if (UniqueId != null)
                            {
                                role = userInfoPoco.Role;
                                userInfoPoco.Role = context.Roles.Where(x => x.RoleName == userInfoPoco.Role).Select(x => x.RoleID).FirstOrDefault();
                            }

                            userinfo userInfo = new userinfo();
                            userInfo.ID = Guid.NewGuid().ToString();
                            userInfo.Address = userInfoPoco.Address;
                            userInfo.AddressLine2 = userInfoPoco.AddressLine2;
                            userInfo.City = userInfoPoco.City;
                            userInfo.Country = userInfoPoco.Country;
                            userInfo.CreatedDate = DateTime.Now;
                            userInfo.ModifiedDate = DateTime.Now;
                            userInfo.DateOfBirth = userInfoPoco.DateOfBirth;
                            userInfo.EmailAddress = userInfoPoco.EmailAddress;
                            userInfo.FirstName = userInfoPoco.FirstName;
                            userInfo.LastName = userInfoPoco.LastName;
                            userInfo.ModifiedDate = userInfoPoco.ModifiedDate;
                            userInfo.Password = userInfoPoco.Password;
                            userInfo.PhoneNo = userInfoPoco.PhoneNo;
                            userInfo.ZipCode = userInfoPoco.ZipCode;
                            userInfo.Role = userInfoPoco.Role;
                            userInfo.State = userInfoPoco.State;
                            userInfo.Country = userInfoPoco.Country;
                            userInfo.SecurityQuestion = userInfoPoco.SecurityQuestion;
                            userInfo.SecurityAnswer = userInfoPoco.SecurityAnswer;
                            userInfo.StreetAddress = userInfoPoco.StreetAddress;
                            userInfo.IsDeleted = false;

                            if (userInfoPoco.Role == "85DA6641-E08A-4192-A") { userInfo.IsActive = false; userInfo.IsEmailVerified = false; }
                            else
                                userInfo.IsActive = true;

                            context.UserInfoes.Add(userInfo);
                            context.SaveChanges();

                            if (userInfoPoco.Role == "85DA6641-E08A-4192-A")
                            {
                                notificationdetail objNotification = new notificationdetail();
                                objNotification.NotifcationFrom = userInfo.ID;
                                objNotification.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                                objNotification.NotificationTypeID = 4;
                                objNotification.IsRead = false;
                                objNotification.CreatedDate = System.DateTime.Now;
                                objNotification.RequestID = "";
                                context.NotificationDetail.Add(objNotification);
                                context.SaveChanges();
                            }

                            if (UniqueId != null)
                            {
                                LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails();

                                loggedInUserDetails.UserId = userInfo.ID;
                                loggedInUserDetails.RoleId = userInfo.Role;
                                loggedInUserDetails.RoleName = role;
                                loggedInUserDetails.FirstName = userInfo.FirstName;
                                loggedInUserDetails.LastName = userInfo.LastName;
                                loggedInUserDetails.EmailAddress = userInfo.EmailAddress;
                                loggedInUserDetails.Zipcode = userInfo.ZipCode;
                                loggedInUserDetails.rolecount = context.UserInfoes.Where(x => x.EmailAddress == userInfo.EmailAddress).ToList().Count;


                                //Store the User's Details to a session
                                Session["UserDetails"] = loggedInUserDetails;
                                LoggedInUser.UserId = userInfo.ID;

                                LoggedInUser.RoleId = userInfo.Role;
                                LoggedInUser.RoleName = role;
                                LoggedInUser.FirstName = userInfo.FirstName;
                                LoggedInUser.LastName = userInfo.LastName;
                                LoggedInUser.EmailAddress = userInfo.EmailAddress;
                                LoggedInUser.Zipcode = userInfo.ZipCode;
                            }
                            //return Json(_messageToShow);
                            EmailHelper email = new EmailHelper();
                            string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "SystemIntroduction").TemplateDiscription;
                            emailBody = emailBody.Replace("{First Name}", userInfo.FirstName);
                            emailBody = emailBody.Replace("{Last Name}", userInfo.LastName);
                            emailBody = emailBody.Replace("{email}", userInfo.EmailAddress);
                            emailBody = emailBody.Replace("{password}", userInfo.Password);
                            emailBody = emailBody.Replace("{role}", role);

                            bool status = email.SendEmail("", "Welcome To SpectorMax", "", emailBody, userInfoPoco.EmailAddress);
                            if (status == true)
                            //   return Content("Success");
                            {
                                if (UniqueId != null)
                                {
                                    string Orderid = context.InspectionOrders.FirstOrDefault(x => x.UniqueID == UniqueId).InspectionOrderID;
                                    _messageToShow.Result = Orderid + "%^$&23" + userInfo.ID + "%^$&23" + userInfo.Role + "%^$&23" + userInfoPoco.amount + "%^$&23" + userInfoPoco.paymenttype;
                                }
                                _messageToShow.Message = "Success";
                                _messageToShow.UserID = userInfo.ID;
                                //   return Json(_messageToShow);
                            }
                            else
                            {
                                if (UniqueId != null)
                                {
                                    _messageToShow.Result = UniqueId;
                                }
                                _messageToShow.Message = "Mail Send Failure";
                                //  _messageToShow.UserID = userInfo.ID;
                            }
                            //  return Content("Mail Send Failure");
                            _messageToShow.UserID = userInfo.ID;
                            return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                            //return Content("Success");
                        }
                    }
                    else
                    {
                        //ModelState.AddModelError("EmailAddress", "Email Address Already Exist");
                        _messageToShow.Message = "Email Address Already Exist";
                        //return Json(_messageToShow);
                        return Json(_messageToShow);

                    }
                }
                catch (Exception ex)
                {
                    _messageToShow.Message = ex.Message;
                    //return Json(_messageToShow);
                    //return Json();

                }
            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors);
            _messageToShow.Message = "Display Validation Summary";
            return Json(allErrors);

        }

        /// <summary>
        /// Get City Based on State.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetCity(string State)
        {
            try
            {
                var objCityList = (from k in context.ZipCode
                                   where k.State == State
                                   select k.City).Distinct();

                List<SelectListItem> CityList = new List<SelectListItem>();
                foreach (var t in objCityList)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.ToString();
                    s.Value = t.ToString();
                    CityList.Add(s);
                }
                CityList = CityList.OrderBy(x => x.Text).ToList();
                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                CityList.Insert(0, objSelectListItem);
                return Json(CityList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
                return Json(_messageToShow);
            }
        }

        /// <summary>
        /// Get Zip Based on City.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetZipcodes(string City)
        {
            try
            {
                var CityList = (from k in context.ZipCode
                                where k.City == City
                                select k.Zip).Distinct().ToList();

                return Json(CityList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
                return Json(_messageToShow);
            }
        }


        private DateTime? CreateDate(string Dob)
        {

            DateTime dt = new DateTime(Convert.ToInt32(Dob.Split('-')[1]), Convert.ToInt32(Dob.Split('-')[0]), 1);
            return dt;
        }


        /// <summary>
        /// Upgrade account home buyer to home seller.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpgradeAccount()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Home Buyer")
                {
                    userinfo userinfo = context.UserInfoes.FirstOrDefault(x => x.EmailAddress == Sessions.LoggedInUser.EmailAddress);
                    if (userinfo != null)
                    {
                        userinfo objUserInfo = new userinfo();
                        objUserInfo.ID = Guid.NewGuid().ToString();
                        objUserInfo.FirstName = userinfo.FirstName;
                        objUserInfo.LastName = userinfo.LastName;
                        objUserInfo.EmailAddress = userinfo.EmailAddress;
                        objUserInfo.Address = userinfo.Address;
                        objUserInfo.AddressLine2 = userinfo.AddressLine2;
                        objUserInfo.City = userinfo.City;
                        objUserInfo.Country = userinfo.Country;
                        objUserInfo.DateOfBirth = userinfo.DateOfBirth;
                        objUserInfo.Password = userinfo.Password;
                        objUserInfo.PhoneNo = userinfo.PhoneNo;
                        objUserInfo.Role = "9C15FC8A-CDD3-49CD-9";
                        objUserInfo.State = userinfo.State;
                        objUserInfo.StreetAddress = userinfo.StreetAddress;
                        objUserInfo.ZipCode = userinfo.ZipCode;
                        objUserInfo.IsActive = true;
                        objUserInfo.CreatedDate = DateTime.Now;
                        objUserInfo.ModifiedDate = DateTime.Now;
                        context.UserInfoes.Add(objUserInfo);
                        context.SaveChanges();
                        /*Below is implemented, in case Home Buyer upgrades his/her account we need to increment ViewBag.Rolecount so that dropdown should be working*/
                        ViewBag.RoleCount = context.UserInfoes.Where(x => x.EmailAddress == Sessions.LoggedInUser.EmailAddress).ToList().Count;
                        _messageToShow.Message = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
            }
            return Json(_messageToShow);
        }


        //
        // GET: /UserInfoes/Edit/5

        public ActionResult Edit(string id)
        {
            userinfo userinfo = context.UserInfoes.Single(x => x.ID == id);
            return View(userinfo);
        }

        //
        // POST: /UserInfoes/Edit/5

        [HttpPost]
        public ActionResult Edit(userinfo userinfo)
        {
            if (ModelState.IsValid)
            {
                context.Entry(userinfo).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userinfo);
        }

        //
        // GET: /UserInfoes/Delete/5

        public ActionResult Delete(string id)
        {
            userinfo userinfo = context.UserInfoes.Single(x => x.ID == id);
            return View(userinfo);
        }

        //
        // POST: /UserInfoes/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            userinfo userinfo = context.UserInfoes.Single(x => x.ID == id);
            context.UserInfoes.Remove(userinfo);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// GET: // method for redirect the inspector on inspector dashboard.
        /// </summary>
        /// <returns></returns>
        public ActionResult InspectorDashboard() //string userId
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    List<string> ArrayOfOrderPlaced = context.InspectionOrders.Where(x => x.InspectorID == Sessions.LoggedInUser.UserId && x.InspectionStatus == "Completed").Select(y => y.InspectionOrderID).ToList();
                    List<string> _orderId = new List<string>();
                    int Count = 0;
                    bool IsExist = false;
                    foreach (var item in ArrayOfOrderPlaced)
                    {
                        IsExist = context.PhotoLibraries.Any(x => x.InspectionOrderID == item && x.CommentedBy != Sessions.LoggedInUser.UserId && x.IsRead == false);
                        //IsExist = context.PhotoLibraries.Any(x => x.InspectionOrderID == item.InspectionOrderID && (x.CommentedBy != Sessions.LoggedInUser.UserId || x.CommentedBy == null));
                        if (IsExist)
                        { Count++; IsExist = false; _orderId.Add(item); }
                    }
                    ViewBag.count = Count;
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult InspectorDashboardNew() //string userId
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Property Inspector")
                {
                    MySqlConnection conn = new MySqlConnection(connStr);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("GetRequestCount", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserID", Sessions.LoggedInUser.UserId);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        ViewBag.count = ds.Tables[0].Rows.Count;
                    }
                    else
                    {

                        ViewBag.count = 0;
                    }
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                    {
                        ViewBag.RequestCount = ds.Tables[1].Rows[0][0];
                    }
                    else
                    {
                        ViewBag.RequestCount = 0;
                    }
                    ViewBag.TotalCount = ViewBag.count + ViewBag.RequestCount;
                    //List<string> ArrayOfOrderPlaced = context.InspectionOrders.Where(x => x.InspectorID == Sessions.LoggedInUser.UserId && x.InspectionStatus == "Completed").Select(y => y.InspectionOrderID).ToList();
                    //List<string> _orderId = new List<string>();
                    //int Count = 0;
                    //bool IsExist = false;
                    //foreach (var item in ArrayOfOrderPlaced)
                    //{
                    //    IsExist = context.PhotoLibraries.Any(x => x.InspectionOrderID == item && x.CommentedBy != Sessions.LoggedInUser.UserId && x.IsRead == false);
                    //    //IsExist = context.PhotoLibraries.Any(x => x.InspectionOrderID == item.InspectionOrderID && (x.CommentedBy != Sessions.LoggedInUser.UserId || x.CommentedBy == null));
                    //    if (IsExist)
                    //    { Count++; IsExist = false; _orderId.Add(item); }
                    //}
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.Message.ToString());
            }
        }

        /// <summary>
        /// GET: // method for redirect the user on Dashboard page.
        /// </summary>
        /// <returns></returns>
        public ActionResult SellerDashboard() //string userId
        {
            //List<InspectionOrder> ArrayOfOrderPlaced = context.InspectionOrders.Where(x => x.RequesterID == Sessions.LoggedInUser.UserId && x.InspectionStatus == "Completed").ToList();

            //int Count = 0;
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Home Seller" || Sessions.LoggedInUser.RoleName == "Home Buyer"))
                {
                    List<PropertyAddress> objPropertyAddress = new List<PropertyAddress>();
                    //bool IsAvail_PhG = false;
                    //var section = (from Subsection in context.SubSectionStatu
                    //               join IO in context.InspectionOrders on Subsection.InspectionOrderID equals IO.InspectionOrderID
                    //               join User in context.UserInfoes on IO.RequesterID equals User.ID
                    //               where IO.RequesterID == Sessions.LoggedInUser.UserId && IO.IsUpdatable == true && Subsection.Status == false
                    //               select new
                    //               {
                    //                   Subsection.InspectionOrderID
                    //               }).Distinct().ToList();

                    //ViewBag.count = section.Count();
                    //

                    objPropertyAddress = (from IO in context.InspectionOrders
                                          join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                          join SecStatus in context.SubSectionStatu on IOD.InspectionOrderId equals SecStatus.InspectionOrderID
                                          where IO.RequesterID == Sessions.LoggedInUser.UserId && SecStatus.Status == false && SecStatus.IsUpdated == false && IO.IsUpdatable == true
                                          select new PropertyAddress
                                          {
                                              OrderID = IO.InspectionOrderID,
                                              OwnerFirstName = IOD.OwnerFirstName,
                                              OwnerLastName = IOD.OwnerLastName,
                                              StreetAddress = IOD.StreetAddess,
                                              City = IOD.City,
                                              State = IOD.State,
                                              Zip = IOD.Zip
                                          }).Distinct().ToList();

                    ViewBag.count = objPropertyAddress.Count;


                    var UpdateStatus = (from Subsection in context.SubSectionStatu
                                        join IO in context.InspectionOrders on Subsection.InspectionOrderID equals IO.InspectionOrderID
                                        join User in context.UserInfoes on IO.RequesterID equals User.ID
                                        join PG in context.PhotoLibraries on Subsection.InspectionOrderID equals PG.InspectionOrderID
                                        where IO.RequesterID == Sessions.LoggedInUser.UserId && IO.IsUpdatable == true && Subsection.IsUpdated == true
                                        select new
                                        {
                                            Subsection.InspectionOrderID,
                                            PG.CommentedBy,
                                            PG.CreatedDate
                                        }).Distinct().OrderByDescending(x => x.CreatedDate).GroupBy(x => x.InspectionOrderID).ToList();

                    int UpdateCount = 0;
                    for (int i = 0; i < UpdateStatus.Count; i++)
                    {

                        string role = UpdateStatus.ElementAt(i).OrderByDescending(x => x.CreatedDate).FirstOrDefault().CommentedBy;//UpdateStatus.ElementAt(i).FirstOrDefault().CommentedBy;
                        //string role = context.UserInfoes.Where(x => x.ID == UpdateStatus[i].FirstOrDefault().CommentedBy).FirstOrDefault().Role;
                        if (role != null)
                        {
                            bool IsInspectorComment = context.UserInfoes.Where(x => x.ID == role).FirstOrDefault().Role == "85DA6641-E08A-4192-A" ? true : false;
                            if (IsInspectorComment)
                                UpdateCount++;
                        }
                    }

                    ViewBag.UpdateCount = UpdateCount;


                    #region Commented Code
                    //List<ElectrialCommanSection> objlist_ElectrialCommanSection = context.ElectrialCommanSection.Where(x => x.ConditionID == "3").ToList();
                    //List<ElectricalPanel> objlist_ElectricalPanel = context.ElectricalPanel.Where(x => x.ConditionID == "3").ToList();
                    //List<ElectricalRoom> objlist_ElectricalRoom = context.ElectricalRoom.Where(x => x.ConditionID == "3").ToList();
                    //List<ElectricalService> objlist_ElectricalService = context.ElectricalService.Where(x => x.ConditionID == "3").ToList();

                    //List<GroundCommonSection> objlist_GroundCommonSection = context.GroundCommonSections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<GroundGassection> objlist_GroundGassection = context.GroundGassections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<GroundH2OSection> objlist_GroundH2OSection = context.GroundH2OSections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<GroundSewerSection> objlist_GroundSewerSection = context.GroundSewerSections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<GroundSprinkSysSection> objlist_GroundSprinkSysSection = context.GroundSprinkSysSections.Where(x => x.CheckBoxID == "3").ToList();

                    //List<HVACCommonSection> objlist_HVACCommonSection = context.HVACCommonSections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<HVACCooling> objlist_HVACCooling = context.HVACCoolings.Where(x => x.CheckBoxID == "3").ToList();
                    //List<HVACDuctsVent> objlist_HVACDuctsVent = context.HVACDuctsVents.Where(x => x.CheckBoxID == "3").ToList();
                    //List<HVACEquipBoileRadiantSection> objlist_HVACEquipBoileRadiantSection = context.HVACEquipBoileRadiantSections.Where(x => x.CheckBoxID == "3").ToList();

                    //List<OptionalCommonSection> objlist_OptionalCommonSection = context.OptionalCommonSections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<OptionalTestingSection> objlist_OptionalTestingSection = context.OptionalTestingSections.Where(x => x.CheckBoxID == "3").ToList();

                    //List<PlumbingCommonSection> objlist_PlumbingCommonSection = context.PlumbingCommonSections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<PlumbingMainlineKitchenOthers_Section> objlist_PlumbingMainlineKitchenOthers_Section = context.PlumbingMainlineKitchenOthers_Sections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<PlumbingWaterheat> objlist_PlumbingWaterheat = context.PlumbingWaterheats.Where(x => x.CheckBoxID == "3").ToList();

                    //List<Appliancescommonsection> objlist_Appliancescommonsection = context.Appliancescommonsections.Where(x => x.CheckboxID == "3").ToList();
                    //List<AppliancesDishwashDisposalRange_section> objlist_AppliancesDishwashDisposalRange_section = context.AppliancesDishwashDisposalRange_sections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<AppliancesDryerDoorBellSectiion> objlist_AppliancesDryerDoorBellSectiion = context.AppliancesDryerDoorBellSectiions.Where(x => x.CheckBoxID == "3").ToList();
                    //List<AppliancesHoodVentOtherSection> objlist_AppliancesHoodVentOtherSection = context.AppliancesHoodVentOtherSections.Where(x => x.CheckBoxID == "3").ToList();
                    //List<AppliancesOvenSection> objlist_AppliancesOvenSection = context.AppliancesOvenSections.Where(x => x.CheckboxID == "3").ToList();

                    //foreach (var item in ArrayOfOrderPlaced)
                    //{
                    //    if (objlist_ElectrialCommanSection.Count > 0)
                    //    {
                    //        var objExists = objlist_ElectrialCommanSection.Where(x => x.ReportID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "00CEF17D-8EF7-44E4-8C28-1EF2E02FC0A5");
                    //        if (objExists.Count() > 0 && IsAvail_PhG == false)
                    //            Count++;
                    //    }
                    //    if (objlist_ElectricalPanel.Count > 0)
                    //    {
                    //        var objExists = objlist_ElectricalPanel.Where(x => x.ReportID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "FF7B3D59-72EA-4E44-86CF-9DA8F77BB797");
                    //        if (objExists.Count() > 0 && IsAvail_PhG == false)
                    //            Count++;
                    //    }
                    //    if (objlist_ElectricalRoom.Count > 0)
                    //    {
                    //        var objExists = objlist_ElectricalRoom.Where(x => x.ReportID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "BB215ED3-D9A7-4B4D-B23D-5129AF3BB032");
                    //        if (objExists.Count() > 0 && IsAvail_PhG == false)
                    //            Count++;
                    //    }
                    //    if (objlist_ElectricalService.Count > 0)
                    //    {
                    //        var objExists = objlist_ElectricalService.Where(x => x.ReportID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "E5C4DD35-3F35-4C3F-B6B3-92D46F9C995B");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_GroundGassection.Count > 0)
                    //    {
                    //        var objExists = objlist_GroundGassection.Where(x => x.InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "A9460F5E-72E0-4EEF-AEA3-5FEA97C75955");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_GroundH2OSection.Count > 0)
                    //    {
                    //        var objExists = objlist_GroundH2OSection.Where(x => x.InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "8E5EA3C5-3257-4586-A706-ED703C09162E");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_GroundSewerSection.Count > 0)
                    //    {
                    //        var objExists = objlist_GroundSewerSection.Where(x => x.C_InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "DB33D5A2-124C-4E38-A77B-7007FA614DF2");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_GroundSprinkSysSection.Count > 0)
                    //    {
                    //        var objExists = objlist_GroundSprinkSysSection.Where(x => x.InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "E3F23374-708B-444F-96A9-EDA20E4C6FB0");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_HVACCommonSection.Count > 0)
                    //    {
                    //        var objExists = objlist_HVACCommonSection.Where(x => x.InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "E39381CD-BB69-47D9-86BA-0866EEE63068");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_HVACCooling.Count > 0)
                    //    {
                    //        var objExists = objlist_HVACCooling.Where(x => x.InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "68114F1A-FCBC-4073-8624-33325E50C78F");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_HVACDuctsVent.Count > 0)
                    //    {
                    //        var objExists = objlist_HVACDuctsVent.Where(x => x.InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "052B3DA6-B382-45A2-AC6D-DF84A3008B7C");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_HVACEquipBoileRadiantSection.Count > 0)
                    //    {
                    //        var objExists = objlist_HVACEquipBoileRadiantSection.Where(x => x.InspectionOrderId == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "AA4C793F-5496-4B30-A00F-78E740C6E061" && y.SubSectionId == "9A761BBF-7015-4497-A39E-16E92AA5986B" && y.SubSectionId == "B703AA37-46C5-43B3-BB86-86E6ECCA578C");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_Appliancescommonsection.Count > 0)
                    //    {
                    //        var objExists = objlist_Appliancescommonsection.Where(x => x.InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "AA4C793F-5496-4B30-A00F-78E740C6E061" && y.SubSectionId == "9A761BBF-7015-4497-A39E-16E92AA5986B" && y.SubSectionId == "B703AA37-46C5-43B3-BB86-86E6ECCA578C");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_AppliancesDishwashDisposalRange_section.Count > 0)
                    //    {
                    //        var objExists = objlist_AppliancesDishwashDisposalRange_section.Where(x => x.InsepectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "2202B6E9-7DD8-4E67-BBBC-96F43A9573D6" && y.SubSectionId == "48D6F01D-3457-4563-9CBD-34D04039F729" && y.SubSectionId == "11922FA2-9F31-430F-8663-232833D65EE7");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_AppliancesDryerDoorBellSectiion.Count > 0)
                    //    {
                    //        var objExists = objlist_AppliancesDryerDoorBellSectiion.Where(x => x.InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "83E17F04-8162-49AF-BCA2-53805BF361AB" && y.SubSectionId == "459D574C-BF96-455D-9BCC-B70BAB246881");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_AppliancesHoodVentOtherSection.Count > 0)
                    //    {
                    //        var objExists = objlist_AppliancesHoodVentOtherSection.Where(x => x.InspectionOrderId == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "FB1135C5-A640-49D0-8D83-B542609BE0F4" && y.SubSectionId == "2347614D-AE9E-4CF3-835E-9C55418FEC35");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //    if (objlist_AppliancesOvenSection.Count > 0)
                    //    {
                    //        var objExists = objlist_AppliancesOvenSection.Where(x => x.InspectionOrderID == item.InspectionOrderID);
                    //        //IsAvail_PhG = context.PhotoLibraries.Any(y => y.InspectionOrderID == objExists.FirstOrDefault().ID && y.IsRead == true && y.SubSectionId == "6217DDFC-F50F-46A4-9411-B854576A270F");
                    //        if (objExists.Count() > 0)
                    //            Count++;
                    //    }
                    //}

                    //#region Removing Objects;
                    //objlist_ElectrialCommanSection = null;
                    //objlist_ElectricalPanel = null;
                    //objlist_ElectricalRoom = null;
                    //objlist_ElectricalService = null;
                    //objlist_GroundGassection = null;
                    //objlist_GroundH2OSection = null;
                    //objlist_GroundSewerSection = null;
                    //objlist_GroundSprinkSysSection = null;
                    //objlist_HVACCommonSection = null;
                    //objlist_HVACCooling = null;
                    //objlist_HVACDuctsVent = null;
                    //objlist_HVACEquipBoileRadiantSection = null;
                    //objlist_Appliancescommonsection = null;
                    //objlist_AppliancesDishwashDisposalRange_section = null;
                    //objlist_AppliancesDryerDoorBellSectiion = null;
                    //objlist_AppliancesHoodVentOtherSection = null;
                    //objlist_AppliancesOvenSection = null;
                    //GC.Collect();
                    //#endregion
                    //ViewBag.count = Count; 
                    #endregion
                    return View(context.UserInfoes.FirstOrDefault(x => x.ID == (Sessions.LoggedInUser.UserId != null ? Sessions.LoggedInUser.UserId : "") && x.IsActive == true));
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// GET: // method for invite friend 
        /// </summary>
        /// <returns></returns>
        public ActionResult InviteFriend()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Home Seller" || Sessions.LoggedInUser.RoleName == "Home Buyer"))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// POST: // method for Invite Friend.
        /// </summary>
        /// <param name="emailTo"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult InviteFriend(string emailTo)
        {
            try
            {
                EmailHelper email = new EmailHelper();
                string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "InviteFriend").TemplateDiscription;
                emailBody = emailBody.Replace("{firstName}", Sessions.LoggedInUser.FirstName);
                emailBody = emailBody.Replace("{LastName}", Sessions.LoggedInUser.LastName);
                bool status = email.SendEmail(Sessions.LoggedInUser.EmailAddress, "SpectorMax Invitation", "", emailBody, emailTo);

                if (status == true)
                    _messageToShow.Message = "Success";
                else
                    _messageToShow.Message = "Mail Send Failure";

                return Json(_messageToShow);
            }
            catch (Exception)
            {

                throw;
            }

        }


        //
        // GET: /UserInfoes/ReferaFriend
        public ActionResult ReferaFriend()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "Home Seller" || Sessions.LoggedInUser.RoleName == "Home Buyer"))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //
        // POST: /UserInfoes/ReferaFriend
        [HttpPost]
        public JsonResult ReferaFriend(string emailTo)
        {

            try
            {

                EmailHelper email = new EmailHelper();
                string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "Referfriend").TemplateDiscription;
                emailBody = emailBody.Replace("{firstName}", Sessions.LoggedInUser.FirstName);
                emailBody = emailBody.Replace("{LastName}", Sessions.LoggedInUser.LastName);
                StringBuilder strRating = new StringBuilder();
                int? zipcode = context.UserInfoes.Where(x => x.ID == Sessions.LoggedInUser.UserId).FirstOrDefault().ZipCode;
                bool IsExist = false;
                //List<UserInfo> UserinZipCode = context.ZipCode.Where(x => x.Zip == zipcode).ToList();
                IsExist = context.ZipCodeAssigned.Any(x => x.ZipCode == zipcode);
                string userid = string.Empty;
                int? Inspectorzip = 0;
                if (IsExist == true)
                {
                    userid = context.ZipCodeAssigned.Where(x => x.ZipCode == zipcode).FirstOrDefault().InspectorID;
                    //Inspectorzip = context.UserInfoes.Where(x => x.ZipCode == zipcode && x.Role == "85DA6641-E08A-4192-A").FirstOrDefault().ZipCode;


                    InspectorRatingPoco objInspectorRatingPocoPoco;
                    List<InspectorRatingPoco> ListofRatingDetails = new List<InspectorRatingPoco>();
                    //if (Inspectorzip > 0)
                    {
                        var InspectorDetails = (from ID in context.InspectorDetail
                                                join IO in

                                                    (from val in context.InspectionOrders
                                                     group val by val.InspectorID into g
                                                     select new { ID = g.Key, test = g.Count() }
                                                ) on ID.UserID equals IO.ID into cls
                                                from v in cls.DefaultIfEmpty()
                                                join UI in context.UserInfoes on ID.UserID equals UI.ID
                                                join Roles in context.Roles on UI.Role equals Roles.RoleID
                                                join zip in context.ZipCodeAssigned on ID.UserID equals zip.InspectorID into joined
                                                join rating in context.InspectorRatings on ID.UserID equals rating.InspectorID
                                                where ID.UserID == userid
                                                select new InspectorDetails
                                                {
                                                    InspectorDetailsID = UI.ID,
                                                    FirstName = UI.FirstName,
                                                    LastName = UI.LastName,
                                                    Count = v.test == null ? 0 : v.test

                                                });
                        if (InspectorDetails.Any())
                        {
                            emailBody = emailBody.Replace("{Inspector Name}", InspectorDetails.Select(x => x.FirstName).FirstOrDefault() + " " + InspectorDetails.Select(x => x.LastName).FirstOrDefault());
                            emailBody = emailBody.Replace("{Total Reports}", InspectorDetails.Select(x => x.Count).FirstOrDefault().ToString());
                            emailBody = emailBody.Replace("{firstname}", Sessions.LoggedInUser.FirstName);
                            emailBody = emailBody.Replace("{lastname}", Sessions.LoggedInUser.LastName);
                            var RatingList = context.InspectorRatings.Where(x => x.InspectorID == InspectorDetails.Select(y => y.InspectorDetailsID).FirstOrDefault()).ToList();
                            if (RatingList.Count > 0)
                            {

                                foreach (var item in RatingList)
                                {
                                    strRating.Append("Rating :" + item.Rating1);
                                    strRating.Append(Environment.NewLine);
                                    strRating.Append("Comment :" + item.Comment);
                                    strRating.Append(Environment.NewLine);
                                    userinfo objratingBy = context.UserInfoes.Where(x => x.ID == item.RatingByID).ToList().FirstOrDefault();
                                    if (objratingBy != null)
                                    {
                                        strRating.Append("Rating By :" + objratingBy.FirstName + " " + objratingBy.LastName + " From " + objratingBy.City + "," + objratingBy.State + " On " + item.CreatedDate.Value.ToShortDateString() + Environment.NewLine);
                                        strRating.Append(Environment.NewLine);
                                    }
                                }
                                emailBody = emailBody.Replace("{Ratings}", strRating.ToString());

                            }
                            else
                            {
                                emailBody = emailBody.Replace("{Ratings}", "No Ratings Assigned");
                                ViewBag.Message = "No ratings assigned ";
                            }
                        }
                        bool status = email.SendEmail(Sessions.LoggedInUser.EmailAddress, "Refer Friend", "", emailBody, emailTo);
                        if (status == true)
                            _messageToShow.Message = "Success";
                        else
                            _messageToShow.Message = "Mail Send Failure";
                    }
                }
                else
                {
                    _messageToShow.Message = "No Inspector is available on this zip code";
                }

                return Json(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// GET: // method for Forgot Password.
        /// </summary>
        /// <returns></returns>

        public ActionResult ForgotPassword()
        {
            return View();
        }

        /// <summary>
        /// POST: // View for reset Password.
        /// </summary>
        /// <param name="emailTo"></param>
        /// <returns></returns>
        public ActionResult ResetPassword(string email)
        {
            ResetPassword objResetPassword = new ResetPassword();
            var userinfo = context.UserInfoes.Where(x => x.EmailAddress == email).FirstOrDefault();

            /*Fetching Security Question*/
            if (userinfo != null)
            {
                int temop = Convert.ToInt32(userinfo.SecurityQuestion);
                List<SelectListItem> Question = new List<SelectListItem>();
                var SecurityQuestion = context.SecurityQuestion.Where(x => x.ID == temop).FirstOrDefault();
                SelectListItem s = new SelectListItem();
                s.Text = SecurityQuestion.SecurityQuestion1;
                s.Value = SecurityQuestion.ID.ToString();
                Question.Add(s);
                objResetPassword.SecurityQuestion1 = Question;
            }
            return View(objResetPassword);
        }

        [HttpPost]
        public JsonResult ResetPassword(string SecurityQuestion, string SecurityAnswer, string Password, string emailaddress)
        {
            var EmailExits = (from k in context.UserInfoes
                              where k.EmailAddress == emailaddress
                              select k.EmailAddress).FirstOrDefault().Any();
            if (EmailExits)
            {
                var checkvalidity = (from k in context.UserInfoes
                                     where k.SecurityQuestion == SecurityQuestion && k.SecurityAnswer == SecurityAnswer && k.EmailAddress == emailaddress
                                     select k).Any();

                if (checkvalidity)
                {
                    context.UserInfoes.Where(x => x.EmailAddress == emailaddress).FirstOrDefault().Password = Password;
                    context.SaveChanges();
                    _messageToShow.Message = "success";
                }
                else
                    _messageToShow.Message = "failed";
            }
            else
            {
                _messageToShow.Message = "Not a valid user";
            }
            return Json(_messageToShow);
        }



        /// <summary>
        /// POST: // method for Forgot Password.
        /// </summary>
        /// <param name="emailTo"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ForgotPassword(string emailTo)
        {
            var UserDetails = context.UserInfoes.Where(x => x.EmailAddress == emailTo && x.IsDeleted != true).FirstOrDefault();
            if (UserDetails != null && (UserDetails.Role == "7D9A1A9C-5305-455F-A07C-0BF3080A6753"))
            {
                EmailHelper email = new EmailHelper();
                string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "ForgotPassword").TemplateDiscription;
                emailBody = emailBody.Replace("{First Name}", UserDetails.FirstName);
                emailBody = emailBody.Replace("{Last Name}", UserDetails.LastName);
                emailBody = emailBody.Replace("{emailaddress}", UserDetails.EmailAddress);

                bool status = email.SendEmail("SpectorMax", "Forgotten Password", "", emailBody, emailTo);
                if (status == true)
                    _messageToShow.Message = "Success";
                else
                    _messageToShow.Message = "Mail Send Failure";
            }
            else if (UserDetails != null && UserDetails.Role == "85DA6641-E08A-4192-A")
            {
                try
                {
                    context.UserInfoes.FirstOrDefault(x => x.ID == UserDetails.ID).IsActive = false;

                    notificationdetail objNotificationDetail = new notificationdetail();
                    objNotificationDetail.NotifcationFrom = UserDetails.ID;
                    objNotificationDetail.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                    objNotificationDetail.NotificationTypeID = 1;
                    objNotificationDetail.IsRead = false;
                    objNotificationDetail.CreatedDate = DateTime.Now;
                    context.NotificationDetail.Add(objNotificationDetail);
                    context.SaveChanges();
                    //_messageToShow.Message = "We have blocked your account for temporary period. A Notification has been sent to admin, You will be notified via email on your registered email Id once your password gets reset by Admin.";
                    _messageToShow.Message = "Inspector";
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                _messageToShow.Message = "Wrong Email";
            }
            return Json(_messageToShow);
        }

        /// <summary>
        /// Get:// method for buyer dashboard.
        /// </summary>
        /// <returns></returns>

        public ActionResult BuyerDashboard()
        {
            //int CacheRoleCount = context.UserInfoes.Where(x => x.EmailAddress == Sessions.LoggedInUser.EmailAddress).ToList().Count;
            //ViewBag.RoleCount = CacheRoleCount;


            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Home Buyer")
                {
                    ViewBag.rolecount = Sessions.LoggedInUser.rolecount;
                    var BuyerDashboardInstruction = from k in context.EmailTemplates
                                                    where k.TemplateId == 19
                                                    select k.TemplateDiscription;

                    ViewBag.BuyerInstruction = BuyerDashboardInstruction.FirstOrDefault();
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        /// <summary>
        /// POST:// Method for search properties for buyer aginst given search options.
        /// </summary>
        /// <param name="searchValue"></param>
        /// <param name="searchType"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SearchProperties(string searchValue, string searchType)
        {
            try
            {
                List<ViewPreviousInspection> objViewPreviousInspection = new List<ViewPreviousInspection>();

                if (searchType == "ZipCode" && searchValue != "")
                {
                    int zipCode = Convert.ToInt32(searchValue);
                    objViewPreviousInspection = (from k in context.InspectionOrders
                                                 join Ins in context.InsectionOrderDetails
                                                 on k.InspectionOrderID equals Ins.InspectionOrderId
                                                 where Ins.PropertyAddressZip == zipCode
                                                 && k.InspectionStatus == "Completed"
                                                 select new ViewPreviousInspection
                                                 {

                                                     InspectionOrderId = Ins.InspectionOrderId,
                                                     ReportNumber = k.UniqueID,
                                                     PropertyAddress = Ins.PropertyAddress,
                                                     PropertyAddressCity = Ins.PropertyAddressCity,
                                                     PropertyAddressState = Ins.PropertyAddressState,
                                                     RequestedbyPhoneDay = Ins.RequestedByEmailPhoneDay + " " + Ins.RequestedByEmailPhoneDay1,
                                                     ReportStatus = k.InspectionStatus
                                                 }).ToList();

                }
                else if (searchType == "ReportId" && searchValue != "")
                {
                    string ReportId = searchValue;
                    objViewPreviousInspection = (from k in context.InspectionOrders
                                                 join Ins in context.InsectionOrderDetails
                                                 on k.InspectionOrderID equals Ins.InspectionOrderId
                                                 where k.UniqueID == ReportId.Trim()
                                                 && k.InspectionStatus == "Completed"
                                                 select new ViewPreviousInspection
                                                 {

                                                     InspectionOrderId = Ins.InspectionOrderId,
                                                     ReportNumber = k.UniqueID,
                                                     PropertyAddress = Ins.PropertyAddress,
                                                     PropertyAddressCity = Ins.PropertyAddressCity,
                                                     PropertyAddressState = Ins.PropertyAddressState,
                                                     RequestedbyPhoneDay = Ins.RequestedByEmailPhoneDay,
                                                     ReportStatus = k.InspectionStatus
                                                 }).ToList();


                }
                else if (searchType == "City" && searchValue != "")
                {
                    objViewPreviousInspection = (from k in context.InspectionOrders
                                                 join Ins in context.InsectionOrderDetails
                                                 on k.InspectionOrderID equals Ins.InspectionOrderId
                                                 where Ins.PropertyAddressCity == searchValue.Trim()
                                                 && k.InspectionStatus == "Completed"
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = Ins.InspectionOrderId,
                                                     ReportNumber = k.UniqueID,
                                                     PropertyAddress = Ins.PropertyAddress,
                                                     PropertyAddressCity = Ins.PropertyAddressCity,
                                                     PropertyAddressState = Ins.PropertyAddressState,
                                                     RequestedbyPhoneDay = Ins.RequestedByEmailPhoneDay,
                                                     ReportStatus = k.InspectionStatus
                                                 }).ToList();
                }
                else if (searchType == "Area" && searchValue != "")
                {
                    objViewPreviousInspection = (from k in context.InspectionOrders
                                                 join Ins in context.InsectionOrderDetails
                                                 on k.InspectionOrderID equals Ins.InspectionOrderId
                                                 where Ins.PropertyAddress.Contains(searchValue)
                                                 && k.InspectionStatus == "Completed"
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = Ins.InspectionOrderId,
                                                     ReportNumber = k.UniqueID,
                                                     PropertyAddress = Ins.PropertyAddress,
                                                     PropertyAddressCity = Ins.PropertyAddressCity,
                                                     PropertyAddressState = Ins.PropertyAddressState,
                                                     RequestedbyPhoneDay = Ins.RequestedByEmailPhoneDay,
                                                     ReportStatus = k.InspectionStatus
                                                 }).ToList();
                }
                else if (searchType == "Name" && searchValue != "")
                {
                    objViewPreviousInspection = (from k in context.InspectionOrders
                                                 join Ins in context.InsectionOrderDetails
                                                 on k.InspectionOrderID equals Ins.InspectionOrderId
                                                 where Ins.PropertyAddressCity == searchValue.Trim()
                                                 && k.InspectionStatus == "Completed"
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = Ins.InspectionOrderId,
                                                     ReportNumber = k.UniqueID,
                                                     PropertyAddress = Ins.PropertyAddress,
                                                     PropertyAddressCity = Ins.PropertyAddressCity,
                                                     PropertyAddressState = Ins.PropertyAddressState,
                                                     RequestedbyPhoneDay = Ins.RequestedByEmailPhoneDay,
                                                     ReportStatus = k.InspectionStatus
                                                 }).ToList();
                }
                else if (searchType == "Address" && searchValue != "")
                {
                    objViewPreviousInspection = (from k in context.InspectionOrders
                                                 join Ins in context.InsectionOrderDetails
                                                 on k.InspectionOrderID equals Ins.InspectionOrderId
                                                 where Ins.PropertyAddress == searchValue.Trim()
                                                 && k.InspectionStatus == "Completed"
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = Ins.InspectionOrderId,
                                                     ReportNumber = k.UniqueID,
                                                     PropertyAddress = Ins.PropertyAddress,
                                                     PropertyAddressCity = Ins.PropertyAddressCity,
                                                     PropertyAddressState = Ins.PropertyAddressState,
                                                     RequestedbyPhoneDay = Ins.RequestedByEmailPhoneDay,
                                                     ReportStatus = k.InspectionStatus
                                                 }).ToList();
                }
                if (objViewPreviousInspection.Count > 0)
                {
                    _messageToShow.Message = "Success";
                    _messageToShow.Result = RenderRazorViewToString("_PartialBuyerDashboard", objViewPreviousInspection);
                    return ReturnJson(_messageToShow);
                }
                else
                {
                    _messageToShow.Message = "Item not Found";
                    _messageToShow.Result = RenderRazorViewToString("_PartialBuyerDashboard", objViewPreviousInspection);
                    return ReturnJson(_messageToShow);
                }
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
                return ReturnJson(_messageToShow);
            }
        }

        /// <summary>
        ///  Create Inspection Request
        /// </summary>
        /// <param name="searchValue"></param>
        /// <param name="searchType"></param>
        /// <returns></returns>

        public ActionResult CreateInspectionRequest()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Home Buyer")
                {
                    BuyerInspectionOrderDetailPoco insectionOrderDetailPoco = new BuyerInspectionOrderDetailPoco();
                    var States = context.ZipCode.Select(x => x.State).Distinct();
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> objCityList = new List<SelectListItem>();
                    List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                    SelectListItem objSelectListItem = new SelectListItem();
                    objSelectListItem.Text = "--Select--";
                    objSelectListItem.Value = "--Select--";
                    //items.Add(objSelectListItem);
                    objCityList.Add(objSelectListItem);
                    objZipCodeList.Add(objSelectListItem);
                    foreach (var t in States)
                    {
                        SelectListItem s = new SelectListItem();
                        s.Text = t.ToString();
                        s.Value = t.ToString();
                        items.Add(s);
                    }
                    items = items.OrderBy(x => x.Text).ToList();
                    items.Insert(0, objSelectListItem);
                    insectionOrderDetailPoco.StatesList = items;
                    insectionOrderDetailPoco.ZipCodesList = objZipCodeList;
                    insectionOrderDetailPoco.CityList = objCityList;
                    return View(insectionOrderDetailPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        ///  Create Inspection Request[Post]
        /// </summary>
        /// <param name="insectionOrderDetailPoco"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateInspectionRequest(BuyerInspectionOrderDetailPoco insectionOrderDetailPoco)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Home Buyer")
                {

                    insectionorderdetail insectionOrderDetail = new SpectorMaxDAL.insectionorderdetail();
                    //insectionOrderDetail.MainDate1 = System.DateTime.Now;
                    //insectionOrderDetail.MainDate2 = System.DateTime.Now;
                    //insectionOrderDetail.ScheduleInspectionDate = System.DateTime.Now;
                    inspectionorder objInspectionOrder;
                    insectionOrderDetail.InspectionOrderId = Guid.NewGuid().ToString();
                    insectionOrderDetail.PropertyAddress = insectionOrderDetailPoco.PropertyAddress;
                    insectionOrderDetail.PropertyAddressCity = insectionOrderDetailPoco.PropertyAddressCity;
                    insectionOrderDetail.PropertyAddressCrossStreet = insectionOrderDetailPoco.PropertyAddressCrossStreet;
                    insectionOrderDetail.PropertyAddressState = insectionOrderDetailPoco.PropertyAddressState;
                    insectionOrderDetail.PropertyAddressZip = insectionOrderDetailPoco.PropertyAddressZip;
                    insectionOrderDetail.PropertyAddressTenantName = insectionOrderDetailPoco.PropertyAddressTenantName;
                    insectionOrderDetail.PropertyAddressSubDivison = insectionOrderDetailPoco.PropertyAddressSubDivison;
                    insectionOrderDetail.PropertyAddressMapCoord = insectionOrderDetailPoco.PropertyAddressMapCoord;
                    insectionOrderDetail.PropertyAddressPhoneDay = insectionOrderDetailPoco.PropertyAddressPhoneDay;
                    insectionOrderDetail.PropertyAddressPhoneDay1 = insectionOrderDetailPoco.PropertyAddressPhoneDay1;
                    insectionOrderDetail.PropertyAddressPhoneEve = insectionOrderDetailPoco.PropertyAddressPhoneEve;
                    insectionOrderDetail.PropertyAddressPhoneEve1 = insectionOrderDetailPoco.PropertyAddressPhoneEve1;
                    insectionOrderDetail.PropertyAddressDirections = insectionOrderDetailPoco.PropertyAddressDirections;

                    insectionOrderDetail.RequestedBy = insectionOrderDetailPoco.RequestedBy;
                    insectionOrderDetail.RequestedFirstName = insectionOrderDetailPoco.RequestedFirstName;
                    insectionOrderDetail.RequestedLastName = insectionOrderDetailPoco.RequestedLastName;
                    insectionOrderDetail.RequestedByEmailPhoneDay = insectionOrderDetailPoco.RequestedByEmailPhoneDay;
                    insectionOrderDetail.RequestedByEmailPhoneDay1 = insectionOrderDetailPoco.RequestedByEmailPhoneDay1;
                    insectionOrderDetail.RequestedByEmailPhoneEve = insectionOrderDetailPoco.RequestedByEmailPhoneEve;
                    insectionOrderDetail.RequestedByEmailPhoneEve1 = insectionOrderDetailPoco.RequestedByEmailPhoneEve1;
                    insectionOrderDetail.RequestedByEmailPhoneOther = insectionOrderDetailPoco.RequestedByEmailPhoneOther;
                    insectionOrderDetail.RequestedByEmailPhoneOther1 = insectionOrderDetailPoco.RequestedByEmailPhoneOther1;
                    insectionOrderDetail.RequestedByStreetAddress = insectionOrderDetailPoco.RequestedByStreetAddress;
                    insectionOrderDetail.RequestedByCity = insectionOrderDetailPoco.RequestedByCity;
                    insectionOrderDetail.RequestedByState = insectionOrderDetailPoco.RequestedByState;
                    insectionOrderDetail.RequestedByZip = insectionOrderDetailPoco.RequestedByZip;
                    insectionOrderDetail.RequestedByEmailAddress1 = insectionOrderDetailPoco.RequestedByEmailAddress1;
                    insectionOrderDetail.CreatedDate = DateTime.Now;
                    insectionOrderDetail.ModifiedDate = DateTime.Now;

                    objInspectionOrder = new inspectionorder();
                    objInspectionOrder.InspectionId = Guid.NewGuid().ToString();
                    objInspectionOrder.InspectionOrderID = insectionOrderDetail.InspectionOrderId;
                    objInspectionOrder.InspectionStatus = "Pending";
                    objInspectionOrder.CreatedDate = DateTime.Now;
                    objInspectionOrder.ModifiedDate = DateTime.Now;
                    //string assignedInspectorId = context.UserInfoes.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip) != null ? context.UserInfoes.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip).ID : "";
                    string assignedInspectorId = context.ZipCodeAssigned.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip) != null ? context.ZipCodeAssigned.FirstOrDefault(x => x.ZipCode == insectionOrderDetailPoco.PropertyAddressZip).InspectorID : "";
                    //var countReports = "";
                    int TotalInspectorReportCount = 0;
                    string UniqueId = "Empty";
                    if (assignedInspectorId != "")
                    {
                        var countReports = (from IO in context.InspectionOrders
                                            join IOD in context.InsectionOrderDetails on IO.InspectionOrderID equals IOD.InspectionOrderId
                                            where IO.InspectorID == assignedInspectorId && IOD.PropertyAddressState == insectionOrderDetailPoco.PropertyAddressState
                                            select IO.InspectorID);
                        TotalInspectorReportCount = countReports.Count();
                        UniqueId = insectionOrderDetailPoco.PropertyAddressState + context.InspectorDetail.Where(x => x.UserID == assignedInspectorId).Select(y => y.InspectorNo).FirstOrDefault() + (TotalInspectorReportCount + 1).ToString();
                    }
                    else
                    {

                        notificationdetail objNotification = new notificationdetail();
                        objNotification.NotifcationFrom = Sessions.LoggedInUser.UserId;
                        objNotification.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                        objNotification.NotificationTypeID = 2;
                        objNotification.IsRead = false;
                        objNotification.CreatedDate = System.DateTime.Now;
                        objNotification.RequestID = insectionOrderDetail.InspectionOrderId;
                        context.NotificationDetail.Add(objNotification);
                        context.SaveChanges();

                    }

                    objInspectionOrder.InspectorID = assignedInspectorId;
                    objInspectionOrder.CreatedDate = DateTime.Now;
                    objInspectionOrder.AssignedByAdmin = false;
                    objInspectionOrder.Comment = "";
                    objInspectionOrder.RequesterID = Sessions.LoggedInUser.UserId;
                    // objInspectionOrder.ScheduleDate = insectionOrderDetailPoco.ScheduleInspectionDate;
                    objInspectionOrder.ModifiedDate = DateTime.Now;
                    objInspectionOrder.IsUpdatable = false;
                    objInspectionOrder.UniqueID = UniqueId;
                    context.InspectionOrders.Add(objInspectionOrder);
                    context.SaveChanges();

                    context.InsectionOrderDetails.Add(insectionOrderDetail);
                    context.SaveChanges();


                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        ///  Get Prevoius Inspections
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ActionResult PrevoiusInspections(string userId)
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Home Buyer")
                {
                    int PageNumber = 0;
                    int PerPageRecord = 20;
                    bool islast = false;
                    PagingPoco pagingPoco = new PagingPoco();
                    pagingPoco = PagingResult(PageNumber, PerPageRecord, islast, "", "");
                    return View(pagingPoco);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// private Method for getting ViewPreviousInspection according to paging.
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PerPageRecord"></param>
        /// <param name="islast"></param>
        /// <returns></returns>
        private PagingPoco PagingResult(int PageNumber, int PerPageRecord, bool islast, string startDate, string endDate)
        {
            try
            {
                PagingPoco pagingPoco = new PagingPoco();
                long totalRecord = 0;
                int skiprecord = 0;
                int totalPage = 0;
                int totalPages = 0;
                decimal countpage = 0;
                var dataInsectionOrderDetails = (from k in context.InspectionOrders
                                                 join Ins in context.InsectionOrderDetails
                                                 on k.InspectionOrderID equals Ins.InspectionOrderId
                                                 join UI in context.UserInfoes
                                                 on k.InspectorID equals UI.ID into g
                                                 where k.RequesterID == Sessions.LoggedInUser.UserId
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = k.InspectionOrderID,
                                                     //InspectorName = UI.FirstName == null ? string.Empty : UI.FirstName,
                                                     OwnerFirstName = Ins.OwnerFirstName,
                                                     OwnerLastName = Ins.OwnerLastName,
                                                     InspectorDetails = g,
                                                     PropertyAddress = Ins.PropertyAddress,
                                                     PropertyAddressCity = Ins.PropertyAddressCity,
                                                     PropertyAddressState = Ins.PropertyAddressState,
                                                     ScheduleInspectionDate = Ins.ScheduleInspectionDate,
                                                     ReportStatus = k.InspectionStatus
                                                 }).ToList();

                totalRecord = dataInsectionOrderDetails.Count;
                countpage = Convert.ToDecimal(totalRecord % PerPageRecord);
                totalPage = (int)totalRecord / PerPageRecord;
                if (countpage > 0)
                    totalPage = totalPage + 1;
                if (islast == true)
                {
                    skiprecord = (totalPage - 1) * PerPageRecord;
                    PageNumber = totalPage;
                }
                else
                {
                    PageNumber = (PageNumber > totalPage ? totalPage : (PageNumber <= 0 ? 1 : PageNumber));
                    skiprecord = (PageNumber == 1 ? 0 : PageNumber - 1) * PerPageRecord;
                }
                totalPages = (int)totalPage;
                pagingPoco.objPaging = new Paging();
                pagingPoco.objPaging.TotalRecord = (int)totalRecord;
                pagingPoco.objPaging.PageNumber = PageNumber;
                pagingPoco.objPaging.PerPageRecord = PerPageRecord;
                pagingPoco.objPaging.TotalPage = totalPages;
                pagingPoco.objPaging.PerPageRecordlist = CommonFunctions.PerPageRecordlist();

                //  if (islast == true)
                PageNumber = PageNumber - 1;

                if ((startDate != null || startDate != "") && (endDate != null && endDate != ""))
                {
                    DateTime _startdate = DateTime.Parse(startDate);
                    DateTime _enddate = DateTime.Parse(endDate);
                    pagingPoco.ObjInspectionOrderDetailsPaging = new List<ViewPreviousInspection>();

                    //Below commented code for paging in this we use perpagerecord and skiprecord parameters and search between startdate and enddate for custom paging.
                    //pagingPoco.ObjInspectionOrderDetailsPaging = dataInsectionOrderDetails.Where(x => x.ScheduleInspectionDate >= _startdate.Date && x.ScheduleInspectionDate <= _enddate).OrderByDescending(item => item.OwnerFirstName).Skip(skiprecord).Take(PerPageRecord).ToList();
                    pagingPoco.ObjInspectionOrderDetailsPaging = dataInsectionOrderDetails.Where(x => x.ScheduleInspectionDate >= _startdate.Date && x.ScheduleInspectionDate <= _enddate).ToList();

                }
                else
                {
                    //Below commented code for paging in this we use perpagerecord and skiprecord parameters for custom paging.
                    // pagingPoco.ObjInspectionOrderDetailsPaging = dataInsectionOrderDetails.OrderByDescending(item => item.OwnerFirstName).Skip(skiprecord).Take(PerPageRecord).ToList();
                    pagingPoco.ObjInspectionOrderDetailsPaging = dataInsectionOrderDetails.ToList();
                }

                return pagingPoco;
            }
            catch (Exception)
            {

                throw;
            }

        }



        /// <summary>
        /// Method to get Plan status of buyer
        /// </summary>
        /// <returns></returns>
        public ActionResult HomeBuyerPlan()
        {
            try
            {
                if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Home Buyer")
                {
                    List<emailtemplate> emailTemplateList = null;
                    var PlanPayment = (from payment in context.Payments
                                       where payment.UserID == Sessions.LoggedInUser.UserId
                                       orderby payment.CreatedDate descending
                                       select
                                       payment).Take(1);

                    if (PlanPayment.Any())
                    {
                        DateTime? PaymentDateValidity;
                        string PaymentType = PlanPayment.Select(x => x.PaymentType).First();
                        PaymentDateValidity = PlanPayment.Select(x => x.PlanValidity).First();
                        if (PaymentDateValidity >= System.DateTime.Now && PaymentType == "PaymentPlan")
                        {

                            ViewBag.Valid = true;
                            ViewBag.ValidityDate = PlanPayment.Select(x => x.PlanValidity).First();
                            return View(emailTemplateList);
                        }
                        else
                        {
                            //emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateName.Contains("HomeSeller")).ToList();
                            emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateId == 3).ToList();
                            ViewBag.Valid = false;
                            ViewBag.UserId = Sessions.LoggedInUser.UserId;
                            ViewBag.UserRoleType = "Home Buyer";
                            ViewBag.UserRole = Sessions.LoggedInUser.RoleId;
                            return View(emailTemplateList);
                        }

                    }
                    else
                    {
                        //emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateName.Contains("HomeSeller")).ToList();
                        emailTemplateList = context.EmailTemplates.ToList().Where(x => x.TemplateId == 3).ToList();
                        ViewBag.Valid = false;
                        ViewBag.UserId = Sessions.LoggedInUser.UserId;
                        ViewBag.UserRoleType = "Home Buyer";
                        ViewBag.UserRole = Sessions.LoggedInUser.RoleId;
                        return View(emailTemplateList);
                    }


                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }




        }


        /// <summary>
        /// User LogOn from free basic report
        /// </summary>
        /// <param name="UserName"></param>
        ///  <param name="Password"></param>
        ///  <param name="Paymentamount"></param>
        ///  <param name="typePayment"></param>
        ///  <param name="UniqueId"></param>
        /// <returns></returns>
        public JsonResult UserLogin(string UserName, string Password, string paymentamount, string typePayment, string UniqueId)
        {
            try
            {
                var result = context.UserInfoes.FirstOrDefault(x => x.EmailAddress == UserName && x.Password == Password && x.IsDeleted == false);

                if (result == null)
                {
                    _messageToShow.Message = "Invalid email or password";

                }

                else if (result.IsActive == false)
                {
                    _messageToShow.Message = "Your account is inactive please contact to your admin.";

                }
                else
                {


                    string userRoleName = context.Roles.FirstOrDefault(x => x.RoleID == result.Role).RoleName;

                    _messageToShow.UserID = result.ID;
                    _messageToShow.UserRoleType = userRoleName;
                    LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails();

                    loggedInUserDetails.UserId = result.ID;
                    loggedInUserDetails.RoleId = result.Role;
                    loggedInUserDetails.RoleName = userRoleName;
                    loggedInUserDetails.FirstName = result.FirstName;
                    loggedInUserDetails.LastName = result.LastName;
                    loggedInUserDetails.EmailAddress = result.EmailAddress;
                    loggedInUserDetails.Zipcode = result.ZipCode;
                    loggedInUserDetails.rolecount = context.UserInfoes.Where(x => x.EmailAddress == UserName).ToList().Count;
                    loggedInUserDetails.StateId = result.StateId;
                    loggedInUserDetails.CityId = result.CityId;
                    loggedInUserDetails.ZipId = result.ZipId;
                    loggedInUserDetails.PhoneNo = result.PhoneNo;
                    loggedInUserDetails.Address = result.Address;


                    //Store the User's Details to a session
                    Session["UserDetails"] = loggedInUserDetails;
                    LoggedInUser.UserId = result.ID;
                    LoggedInUser.UserId = result.ID;
                    LoggedInUser.RoleId = result.Role;
                    LoggedInUser.RoleName = userRoleName;
                    LoggedInUser.FirstName = result.FirstName;
                    LoggedInUser.LastName = result.LastName;
                    LoggedInUser.EmailAddress = result.EmailAddress;
                    LoggedInUser.Zipcode = result.ZipCode;
                    string Orderid = context.tblinspectionorder.FirstOrDefault(x => x.ReportNo == UniqueId).InspectionOrderDetailId;
                    if (paymentamount == "" && typePayment == "" && UniqueId == "")
                    {
                        _messageToShow.Message = userRoleName;
                        _messageToShow.Result = result.ID;
                    }
                    else if (typePayment == "Verification")
                    {

                        _messageToShow.Message = "Verification";
                        _messageToShow.Result = Orderid;
                    }
                    else if ((typePayment == "OneTimePayment" || typePayment == "PaymentPlan") && userRoleName == "SiteUser")
                    {
                        string PaymentResult = CheckPayment(Orderid, "OneTimePayment", Sessions.LoggedInUser.UserId);
                        if (PaymentResult == "PaymentPlan" || PaymentResult == "OneTimePayment")
                        {

                            _messageToShow.Message = "ViewReport";
                            _messageToShow.Result = Orderid;
                        }
                        else if (PaymentResult == "UpdateReport")
                        {
                            _messageToShow.Message = "UpdateReport";
                            _messageToShow.Result = Orderid;
                        }
                        else if (PaymentResult == "Free")
                        {
                            _messageToShow.Message = "FreeBasicReport";
                            _messageToShow.Result = Orderid;
                        }
                    }
                    else if (userRoleName == "Property Inspector")
                    {
                        _messageToShow.Message = "PropertyInspector";
                    }

                    else if (userRoleName == "Admin")
                    {
                        _messageToShow.Message = "Admin";
                    }
                    else
                    {
                        string PaymentResult = CheckPayment(Orderid, "OneTimePayment", Sessions.LoggedInUser.UserId);
                        if (PaymentResult == "PaymentPlan")
                        {
                            _messageToShow.Message = "ViewReport";
                            _messageToShow.Result = Orderid;
                        }
                        else
                        {
                            _messageToShow.Message = "FreeBasicReport";
                            _messageToShow.Result = Orderid;
                        }
                    }

                }

                return Json(_messageToShow, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        private string CheckPayment(string Orderid, string Paymenttype, string UserId)
        {
            try
            {
                var PaymentPlan = (from plan in context.Payments
                                   where plan.UserID == Sessions.LoggedInUser.UserId
                                   select new
                                   {
                                       p = plan.PaymentType,
                                       paymentdate = plan.CreatedDate,
                                       validity = plan.PlanValidity,
                                       ReportId = plan.ReportID
                                   }).OrderByDescending(x => x.paymentdate);
                if (PaymentPlan != null)
                {
                    var checkForPlan = PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault();
                    if (PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault() != null && Convert.ToDateTime(PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault().validity) >= System.DateTime.Now)
                    {
                        Sessions.LoggedInUser.PaymentPlan = "PaymentPlan";
                        return "PaymentPlan";
                    }
                    else if (PaymentPlan.Where(x => x.p == "OneTimePayment" && x.ReportId == Orderid).FirstOrDefault() != null)
                    {
                        Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
                        return "OneTimePayment";
                    }
                    else if (PaymentPlan.Where(x => x.p == "UpdateReport" && x.ReportId == Orderid).FirstOrDefault() != null)
                    {
                        Sessions.LoggedInUser.PaymentPlan = "Updatablereport";
                        return "UpdateReport";
                    }
                    else
                    {
                        Sessions.LoggedInUser.PaymentPlan = "Free";
                        return "Free";
                    }
                }
                else
                {
                    return "Free";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Create User From Free Basic Report
        /// </summary>
        /// <param name="CreateBuyer"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CreateUser(CreateBuyer user)
        {
            try
            {
                if (context.UserInfoes.FirstOrDefault(x => x.EmailAddress == user.EmailAddress && x.IsDeleted == false) == null)
                {
                    if (user != null)
                    {

                        /*Payment*/

                        string returnMsg = string.Empty;
                        string UniqueId = string.Empty;
                        string Orderid = string.Empty;
                        string type = string.Empty;
                        if (user.paymenttype == "PaymentPlan")
                        {

                            returnMsg = MakePayment(user.cardNumber, user.expiryMonth, user.expiryYear, user.securityCode, user.amount, "PaymentPlan");
                        }
                        else
                        {
                            returnMsg = MakePayment(user.cardNumber, user.expiryMonth, user.expiryYear, user.securityCode, user.amount, Orderid);
                        }

                        if (returnMsg == "Success")
                        {

                            /*Save User Details*/

                            UniqueId = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["UniqueId"];
                            Orderid = context.tblinspectionorder.FirstOrDefault(x => x.ReportNo == UniqueId).InspectionOrderDetailId;
                            _messageToShow.Message = "Success&" + Orderid;
                            userinfo userInfo = new userinfo();
                            userInfo.ID = Guid.NewGuid().ToString();
                            userInfo.CityId = user.CityId;
                            userInfo.Country = user.Country;
                            userInfo.CreatedDate = DateTime.Now;
                            userInfo.ModifiedDate = DateTime.Now;
                            userInfo.EmailAddress = user.EmailAddress;
                            userInfo.FirstName = user.FirstName;
                            userInfo.ModifiedDate = user.ModifiedDate;
                            userInfo.ZipCode = user.ZipCode;
                            userInfo.Role = "7D9A1A9C-5305-455F-A07C-0BF3080A6753";
                            userInfo.Password = "123456";
                            userInfo.StateId = user.StateId;
                            userInfo.Country = user.Country;
                            userInfo.IsActive = true;
                            userInfo.IsDeleted = false;
                            context.UserInfoes.Add(userInfo);
                            context.SaveChanges();

                            LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails();
                            if (UniqueId != null)
                            {


                                loggedInUserDetails.UserId = userInfo.ID;
                                loggedInUserDetails.RoleId = userInfo.Role;
                                loggedInUserDetails.RoleName = "SiteUser";
                                loggedInUserDetails.FirstName = userInfo.FirstName;
                                loggedInUserDetails.LastName = userInfo.LastName;
                                loggedInUserDetails.EmailAddress = userInfo.EmailAddress;
                                loggedInUserDetails.Zipcode = userInfo.ZipCode;


                                //Store the User's Details to a session
                                Session["UserDetails"] = loggedInUserDetails;
                                LoggedInUser.UserId = userInfo.ID;

                                LoggedInUser.RoleId = userInfo.Role;
                                LoggedInUser.RoleName = "SiteUser";
                                LoggedInUser.FirstName = userInfo.FirstName;
                                LoggedInUser.LastName = userInfo.LastName;
                                LoggedInUser.EmailAddress = userInfo.EmailAddress;
                                LoggedInUser.Zipcode = userInfo.ZipCode;
                            }


                            /**/


                            /*save Payment Details*/
                            if (user.paymenttype == "PaymentPlan")
                            {
                                type = SavePaymentDetails(user.amount, "PaymentPlan", Orderid);
                            }
                            else
                            {
                                type = SavePaymentDetails(user.amount, "OneTimePaymentPlan", Orderid);
                            }

                            /**/


                            /*Email To User*/
                            EmailHelper email = new EmailHelper();
                            string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "SystemIntroductionPassword").TemplateDiscription;
                            emailBody = emailBody.Replace("{password}", "123456");
                            emailBody = emailBody.Replace("{ReportID}", UniqueId);
                            emailBody = emailBody.Replace("{PaymentStatus}", returnMsg);
                            bool status = email.SendEmail("", "System Introduction", "", emailBody, user.EmailAddress);
                            /**/
                            _messageToShow.Message = "Success%$*" + Orderid;
                        }
                        else
                        {

                            _messageToShow.Message = "Faliure";
                        }

                    }
                    else
                    {
                        _messageToShow.Message = "Error";
                    }
                }
                else
                {
                    _messageToShow.Message = "EmailExist";

                }
                return Json(_messageToShow.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string MakePayment(string cardNumber, string expiryMonth, string expiryYear, string securityCode, string amount, string ReportID)
        {
            try
            {
                string sPostString = "";

                sPostString = BuildPost(sPostString, "ePNAccount", ConfigurationManager.AppSettings["ePNAccount"].ToString());
                sPostString = BuildPost(sPostString, "CardNo", cardNumber);
                sPostString = BuildPost(sPostString, "ExpMonth", expiryMonth);
                sPostString = BuildPost(sPostString, "ExpYear", expiryYear);
                sPostString = BuildPost(sPostString, "Total", amount);
                sPostString = BuildPost(sPostString, "Address", ConfigurationManager.AppSettings["Address"].ToString());
                sPostString = BuildPost(sPostString, "Zip", ConfigurationManager.AppSettings["Zip"].ToString());
                sPostString = BuildPost(sPostString, "RestrictKey", ConfigurationManager.AppSettings["RestrictKey"].ToString());
                sPostString = BuildPost(sPostString, "CVV2Type", ConfigurationManager.AppSettings["CVV2Type"].ToString());
                sPostString = BuildPost(sPostString, "CVV2Type", securityCode);

                string sResultString = subXMLPostProcess(ConfigurationManager.AppSettings["EPNApiUrl"].ToString(), sPostString);
                //UInvalid Card
                string reponse = sResultString.Replace("/", "").Replace('"', ' ').Replace("<html>", "").Replace("<body>", "").Replace("</html>", "").Replace("</body>", "").Trim();
                switch (reponse)
                {
                    case "UInvalid Card": _messageToShow.Message = "Invalid Card Number !"; break;
                    default: _messageToShow.Message = "Success"; break;
                }
                if (_messageToShow.Message == "Success")
                {
                    //string type = SavePaymentDetails(amount, ReportID);
                    //if (type == "PaymentPlan")
                    //{
                    //    _messageToShow.Message = "SuccessPaymenPlan";
                    //}
                    //else
                    //{
                    //    Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
                    //    _messageToShow.Message = "SuccessOneTime";
                    //}
                    return "Success";
                }
                else
                {
                    _messageToShow.Message = "Payment Failure";
                }
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
            }

            return _messageToShow.Message;

        }
        public static string BuildPost(string sPrevPost, string sField, string sValue)
        {
            string functionReturnValue = null;
            if (sPrevPost == null)
                functionReturnValue = "";
            else
                functionReturnValue = sPrevPost + "&";
            functionReturnValue = functionReturnValue + sField;
            functionReturnValue = functionReturnValue + "=";
            functionReturnValue = functionReturnValue + HttpUtility.UrlEncode(sValue);
            return functionReturnValue;
        }

        public static string subXMLPostProcess(string sURL, string sRequestString)
        {
            string ResultString = "";
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                ResultString = wc.UploadString(sURL, sRequestString);
            }
            return ResultString;
        }

        public string SavePaymentDetails(string amount, string paymenttype, string reportid)
        {
            string Type = string.Empty;
            SpectorMaxDAL.payment userPayment = new SpectorMaxDAL.payment();
            userPayment.PaymentID = Guid.NewGuid().ToString();
            userPayment.UserID = Sessions.LoggedInUser.UserId;
            userPayment.ReportID = reportid;
            userPayment.PaymentStatus = "Success";
            userPayment.PaymentDate = DateTime.Now;
            userPayment.PaymentAmount = float.Parse(amount);
            Random random = new Random();
            int num = random.Next(1000000);
            var MaxInvoiceNo = context.Payments.Max(x => x.InvoiceNo);
            if (MaxInvoiceNo == null || Convert.ToInt32(MaxInvoiceNo) == 0)
            {
                userPayment.InvoiceNo = 2000;
            }
            else
            {
                userPayment.InvoiceNo = Convert.ToInt32(MaxInvoiceNo) + 1;
            }
            if (paymenttype == "PaymentPlan")
            {
                userPayment.PaymentType = "PaymentPlan";
                userPayment.PaymentDate = System.DateTime.Now;
                userPayment.PlanValidity = System.DateTime.Now.AddDays(60);
                Sessions.LoggedInUser.PaymentPlan = "PaymentPlan";
                Type = "PaymentPlan";
            }
            else
            {
                userPayment.PaymentType = "OneTimePayment";
                Type = "OneTimePayment";
                userPayment.PaymentDate = System.DateTime.Now;
                Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
            }
            userPayment.CreatedDate = DateTime.Now;
            context.Payments.Add(userPayment);
            if (paymenttype != "PaymentPlan")
            {
                reportviewed objReport = context.ReportViewed.Where(x => x.ReportID == reportid && x.UserID == Sessions.LoggedInUser.UserId).FirstOrDefault();
                if (objReport != null)
                {
                    objReport.IsPurchased = true;
                    objReport.PaymentID = userPayment.PaymentID;
                }
                else
                {
                    string ipaddress = "";
                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (ipaddress == "" || ipaddress == null)
                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                    reportviewed objReportAdd = new SpectorMaxDAL.reportviewed();
                    objReportAdd.ID = Guid.NewGuid().ToString();
                    objReportAdd.CreatedDate = System.DateTime.Now;
                    objReportAdd.IsPurchased = true;
                    objReportAdd.PaymentID = userPayment.PaymentID;
                    objReportAdd.ReportID = reportid;
                    objReportAdd.UserID = Sessions.LoggedInUser.UserId;
                    objReportAdd.MachineIP = ipaddress;
                    context.ReportViewed.Add(objReportAdd);
                }
            }
            else
            {
                string ipaddress = "";
                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipaddress == "" || ipaddress == null)
                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                reportviewed objReportAdd = new SpectorMaxDAL.reportviewed();
                objReportAdd.ID = Guid.NewGuid().ToString();
                objReportAdd.CreatedDate = System.DateTime.Now;
                objReportAdd.IsPurchased = true;
                objReportAdd.PaymentID = userPayment.PaymentID;
                objReportAdd.ReportID = reportid;
                objReportAdd.UserID = Sessions.LoggedInUser.UserId;
                objReportAdd.MachineIP = ipaddress;
                context.ReportViewed.Add(objReportAdd);
            }
            context.SaveChanges();
            return Type;
        }





        #region ManageProfile
        /// <summary>
        /// Function to Manage Inspector Profile
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageProfile()
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    UserProfilePoco userInfo = new UserProfilePoco();
                    if (ModelState.IsValid || Sessions.LoggedInUser != null)
                    {
                        userinfo GetUserInfo = new userinfo();
                        GetUserInfo = context.UserInfoes.Where(x => x.ID == Sessions.LoggedInUser.UserId).FirstOrDefault();

                        userInfo.ID = GetUserInfo.ID;
                        userInfo.Address = GetUserInfo.Address;
                        userInfo.CityId = GetUserInfo.CityId;
                        userInfo.Country = GetUserInfo.Country;
                        userInfo.CreatedDate = DateTime.Now;
                        userInfo.ModifiedDate = DateTime.Now;

                        if (GetUserInfo.DateOfBirth != null)
                            userInfo.DOB = Convert.ToDateTime(GetUserInfo.DateOfBirth).ToString("MM-yyyy");
                        else
                            userInfo.DOB = Convert.ToDateTime(DateTime.Now).ToString("MM-yyyy");

                        userInfo.EmailAddress = GetUserInfo.EmailAddress;
                        userInfo.FirstName = GetUserInfo.FirstName;
                        userInfo.LastName = GetUserInfo.LastName;
                        userInfo.ModifiedDate = GetUserInfo.ModifiedDate;
                        userInfo.Role = GetUserInfo.Role;
                        userInfo.PhoneNo = GetUserInfo.PhoneNo;
                        userInfo.ZipCode = GetUserInfo.ZipCode ?? 0;
                        userInfo.IsProfileUpdated = GetUserInfo.IsProfileUpdated;
                        userInfo.Password = GetUserInfo.Password;
                        userInfo.ConfirmPassword = GetUserInfo.Password;
                        userInfo.StateId = GetUserInfo.StateId;
                        userInfo.Country = GetUserInfo.Country;
                        userInfo.SecurityQuestion = GetUserInfo.SecurityQuestion;
                        userInfo.SecurityAnswer = GetUserInfo.SecurityAnswer;
                        userInfo.StreetAddress = GetUserInfo.StreetAddress;

                        /*Fetching Security Question*/
                        List<SelectListItem> Question = new List<SelectListItem>();
                        var SecurityQuestion = context.SecurityQuestion.ToList();
                        foreach (var t in SecurityQuestion)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = t.SecurityQuestion1;
                            s.Value = t.ID.ToString();
                            Question.Add(s);
                        }
                        var CityList = (from k in context.ZipCode
                                        where k.State == userInfo.State
                                        select k.City).Distinct();
                        var ZipList = (from k in context.ZipCode
                                       where k.City == userInfo.City
                                       select k.Zip).Distinct();
                        userInfo.SecurityQuestion1 = Question;

                        /*Fetching States*/
                        List<NState> objState = new List<NState>();
                        objState = (from state in context.tblstate
                                    where state.IsActive == true
                                    select new NState
                                    {
                                        StateId = state.StateId,
                                        StateName = state.StateName
                                    }).OrderBy(x => x.StateName).ToList();
                        var City = (from k in context.tblcity
                                    where k.StateId == GetUserInfo.StateId
                                    select new NCity
                                    {
                                        CityId = k.CityId,
                                        CityName = k.CityName
                                    }).ToList();
                        var Zip = (from k in context.tblzip
                                   where k.CityId == GetUserInfo.CityId
                                   select new NZip
                                   {
                                       Zip = k.Zip,
                                       ZipId = k.ZipId
                                   }).ToList();
                        List<SelectListItem> Stateitems = new List<SelectListItem>();
                        List<SelectListItem> Cityitems = new List<SelectListItem>();
                        List<SelectListItem> Zipitems = new List<SelectListItem>();
                        List<SelectListItem> objCityList = new List<SelectListItem>();
                        List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                        SelectListItem objSelectListItem = new SelectListItem();
                        objSelectListItem.Text = "--Select--";
                        objSelectListItem.Value = "--Select--";
                        //  Stateitems.Add(objSelectListItem);
                        objCityList.Add(objSelectListItem);
                        objZipCodeList.Add(objSelectListItem);
                        foreach (var t in objState)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = t.StateName.ToString();
                            s.Value = t.StateId.ToString();
                            Stateitems.Add(s);
                        }
                        userInfo.StatesList = Stateitems;
                        foreach (var item in City)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = item.CityName;
                            s.Value = item.CityId.ToString();
                            Cityitems.Add(s);
                        }
                        userInfo.CityList = Cityitems;
                        foreach (var item in Zip)
                        {
                            SelectListItem s = new SelectListItem();
                            s.Text = item.Zip.ToString();
                            s.Value = item.ZipId.ToString();
                            Zipitems.Add(s);
                        }
                        userInfo.ZipCodesList = Zipitems;

                        return View(userInfo);
                    }
                    else
                    {
                        return View(userInfo);
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        [HttpPost]
        public JsonResult ManageProfile(UserProfilePoco ObjUserInfoPoco)
        {
            try
            {
                userinfo objuserinfo = new userinfo();
                objuserinfo = context.UserInfoes.FirstOrDefault(x => x.ID == ObjUserInfoPoco.ID);
                objuserinfo.FirstName = ObjUserInfoPoco.FirstName;
                objuserinfo.LastName = ObjUserInfoPoco.LastName;
                objuserinfo.Address = ObjUserInfoPoco.Address;
                objuserinfo.StateId = ObjUserInfoPoco.StateId;
                objuserinfo.StreetAddress = ObjUserInfoPoco.StreetAddress;
                objuserinfo.CityId = ObjUserInfoPoco.CityId;
                objuserinfo.Country = ObjUserInfoPoco.Country;
                DateTime? dt = CreateDate(ObjUserInfoPoco.DOB);
                objuserinfo.DateOfBirth = dt;
                objuserinfo.PhoneNo = ObjUserInfoPoco.PhoneNo;
                objuserinfo.ModifiedDate = DateTime.Now;
                objuserinfo.ZipCode = ObjUserInfoPoco.ZipCode;
                objuserinfo.SecurityQuestion = ObjUserInfoPoco.SecurityQuestion;
                objuserinfo.SecurityAnswer = ObjUserInfoPoco.SecurityAnswer;
                objuserinfo.Password = ObjUserInfoPoco.Password;
                objuserinfo.IsProfileUpdated = true;
                context.SaveChanges();
                TempData["ProfileUpdated"] = null;
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        #endregion

        public ActionResult BuyerDashboardNew()
        {
            if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Home Buyer")
            {
                var PaymentPlan = (from plan in context.Payments
                                   where plan.UserID == Sessions.LoggedInUser.UserId
                                   select new
                                   {
                                       p = plan.PaymentType,
                                       paymentdate = plan.CreatedDate,
                                       validity = plan.PlanValidity
                                   }).OrderByDescending(x => x.paymentdate).FirstOrDefault();

                if (PaymentPlan != null)
                {
                    if (PaymentPlan.p == "PaymentPlan")
                    {
                        PaymentPlan.paymentdate.Value.AddDays(60);
                        if (Convert.ToDateTime(PaymentPlan.validity) >= System.DateTime.Now)
                        {
                            Sessions.LoggedInUser.PaymentPlan = "PaymentPlan";

                        }
                    }
                    else if (PaymentPlan.p == "OneTimePayment")
                    {
                        Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
                    }
                    else
                    {
                        Sessions.LoggedInUser.PaymentPlan = "Free";
                    }

                }
                else
                {
                    Sessions.LoggedInUser.PaymentPlan = "Free";
                }

                #region Bind States
                List<NState> objState = new List<NState>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "State";
                objSelectListItem.Value = "0";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                ViewBag.StateList = items;

                #endregion

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        public ActionResult SellerDashboardNew()
        {
            if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "Home Seller")
            {
                #region Bind States
                List<NState> objState = new List<NState>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "State";
                objSelectListItem.Value = "0";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                ViewBag.StateList = items;

                #endregion

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public bool? checkIsReportUpdatable(string ReportId)
        {
            return context.tblinspectionorder.FirstOrDefault(x => x.InspectionOrderDetailId == ReportId).IsUpdatable;
        }


        public JsonResult GetVerificationNumber(string ReportId)
        {
            var VerificationNumner = context.tblinspectionorder.FirstOrDefault(x => x.InspectionOrderDetailId == ReportId);
            if (VerificationNumner != null)
            {
                _messageToShow.Message = VerificationNumner.IsVerified.ToString();
                _messageToShow.Result = VerificationNumner.VerificationNumber;
                _messageToShow.ReportType = VerificationNumner.IsUpdatable;
                _messageToShow.Successfull = true;
                return Json(_messageToShow);
            }
            return Json(_messageToShow);
        }


        public ActionResult directDashboard()
        {
            string Guid = Request.Url.ToString().Split('/').ElementAt(Request.Url.ToString().Split('/').Length - 1);
            if (Guid != null)
            {
                string ReqID = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == Guid).FirstOrDefault().RequesterID1;
                var result = context.UserInfoes.FirstOrDefault(x => x.ID == ReqID);
                TempData["ProfileUpdated"] = result.IsProfileUpdated;
                TempData["ReportId"] = Guid;
                string userRoleName = context.Roles.FirstOrDefault(x => x.RoleID == result.Role).RoleName;
                LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails();

                loggedInUserDetails.UserId = result.ID;
                loggedInUserDetails.RoleId = result.Role;
                loggedInUserDetails.RoleName = userRoleName;
                loggedInUserDetails.FirstName = result.FirstName;
                loggedInUserDetails.LastName = result.LastName;
                loggedInUserDetails.EmailAddress = result.EmailAddress;
                loggedInUserDetails.Zipcode = result.ZipCode;
                //loggedInUserDetails.rolecount = context.UserInfoes.Where(x => x.EmailAddress == UserName).ToList().Count;
                loggedInUserDetails.StateId = result.StateId;
                loggedInUserDetails.CityId = result.CityId;
                loggedInUserDetails.ZipId = result.ZipId;
                loggedInUserDetails.PhoneNo = result.PhoneNo;
                loggedInUserDetails.Address = result.Address;
                loggedInUserDetails.ProfileUpdated = result.IsProfileUpdated;
                //Store the User's Details to a session
                Session["UserDetails"] = loggedInUserDetails;
                ViewBag.Guid = Guid;
                ViewBag.IsUpdatable = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == Guid).FirstOrDefault().IsUpdatable;

                var PaymentPlan = (from plan in context.Payments
                                   where plan.UserID == Sessions.LoggedInUser.UserId
                                   select new
                                   {
                                       p = plan.PaymentType,
                                       paymentdate = plan.CreatedDate,
                                       validity = plan.PlanValidity
                                   }).OrderByDescending(x => x.paymentdate);

                if (PaymentPlan != null)
                {

                    var checkForPlan = PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault();
                    if (PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault() != null && Convert.ToDateTime(PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault().validity) >= System.DateTime.Now)
                    {
                        Sessions.LoggedInUser.PaymentPlan = "PaymentPlan";
                    }
                    else if (PaymentPlan.Where(x => x.p == "OneTimePayment").FirstOrDefault() != null)
                    {
                        Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
                    }
                    else
                    {
                        Sessions.LoggedInUser.PaymentPlan = "Free";
                    }

                    MySqlConnection conn = new MySqlConnection(connStr);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("ShareExperience", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserId", Sessions.LoggedInUser.UserId);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    List<ViewPreviousInspection> objViewPreviousInspection = new List<ViewPreviousInspection>();
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        objViewPreviousInspection = ds.Tables[0].AsEnumerable().Select(datarow =>
                       new ViewPreviousInspection
                       {
                           InspectionOrderId = datarow["InspectionOrderDetailId"].ToString(),
                           ReportNumber = datarow["ReportNo"].ToString()
                       }).ToList();
                    }

                    ViewData["ReportList"] = objViewPreviousInspection;

                }
                else
                {
                    Sessions.LoggedInUser.PaymentPlan = "Free";
                }

                #region Bind States
                List<NState> objState = new List<NState>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "State";
                objSelectListItem.Value = "0";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                ViewBag.StateList = items;

                #endregion
                return View("Dashboard");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult Dashboard()
        {
            //CreateCookie(Request.Url.ToString().Split('/')[Request.Url.ToString().Split('/').Count() - 1]);

            if (Sessions.LoggedInUser != null && Sessions.LoggedInUser.RoleName == "SiteUser")
            {
                TempData["ProfileUpdated"] = context.UserInfoes.FirstOrDefault(x => x.ID == Sessions.LoggedInUser.UserId).IsProfileUpdated;
                var PaymentPlan = (from plan in context.Payments
                                   where plan.UserID == Sessions.LoggedInUser.UserId
                                   select new
                                   {
                                       p = plan.PaymentType,
                                       paymentdate = plan.CreatedDate,
                                       validity = plan.PlanValidity
                                   }).OrderByDescending(x => x.paymentdate);

                if (PaymentPlan != null)
                {

                    var checkForPlan = PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault();
                    if (PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault() != null && Convert.ToDateTime(PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault().validity) >= System.DateTime.Now)
                    {
                        Sessions.LoggedInUser.PaymentPlan = "PaymentPlan";
                        //   return "PaymentPlan";
                    }
                    else if (PaymentPlan.Where(x => x.p == "OneTimePayment").FirstOrDefault() != null)
                    {
                        Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
                        // return "OneTimePayment";
                    }

                    else
                    {
                        Sessions.LoggedInUser.PaymentPlan = "Free";
                        //return "Free";
                    }


                    MySqlConnection conn = new MySqlConnection(connStr);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("ShareExperience", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserId", Sessions.LoggedInUser.UserId);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    List<ViewPreviousInspection> objViewPreviousInspection = new List<ViewPreviousInspection>();
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        objViewPreviousInspection = ds.Tables[0].AsEnumerable().Select(datarow =>
                       new ViewPreviousInspection
                       {
                           InspectionOrderId = datarow["InspectionOrderDetailId"].ToString(),
                           ReportNumber = datarow["ReportNo"].ToString()
                       }).ToList();
                    }

                    ViewData["ReportList"] = objViewPreviousInspection;

                }
                else
                {
                    Sessions.LoggedInUser.PaymentPlan = "Free";
                }

                #region Bind States
                List<NState> objState = new List<NState>();
                objState = (from state in context.tblstate
                            where state.IsActive == true
                            select new NState
                            {
                                StateId = state.StateId,
                                StateName = state.StateName
                            }).OrderBy(x => x.StateName).ToList();
                List<SelectListItem> items = new List<SelectListItem>();
                List<SelectListItem> objCityList = new List<SelectListItem>();
                List<SelectListItem> objZipCodeList = new List<SelectListItem>();

                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "State";
                objSelectListItem.Value = "0";
                //  items.Add(objSelectListItem);
                objCityList.Add(objSelectListItem);
                objZipCodeList.Add(objSelectListItem);
                foreach (var t in objState)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.StateName.ToString();
                    s.Value = t.StateId.ToString();
                    items.Add(s);
                }
                items = items.OrderBy(x => x.Text).ToList();
                items.Insert(0, objSelectListItem);
                ViewBag.StateList = items;

                #endregion
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }



        /// <summary>
        /// Get City Based on State.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult NGetCity(string State)
        {
            try
            {
                int stateid = Convert.ToInt32(State);
                List<NCity> objNcity = new List<NCity>();
                objNcity = (from k in context.tblcity
                            where k.StateId == stateid
                            select new NCity
                            {
                                CityId = k.CityId,
                                CityName = k.CityName
                            }).OrderBy(x => x.CityName).ToList();

                List<SelectListItem> CityList = new List<SelectListItem>();
                foreach (var t in objNcity)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.CityName.ToString();
                    s.Value = t.CityId.ToString();
                    CityList.Add(s);
                }
                CityList = CityList.OrderBy(x => x.Text).ToList();
                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                CityList.Insert(0, objSelectListItem);
                return Json(CityList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
                return Json(_messageToShow);
            }
        }

        /// <summary>
        /// Get Zip Based on City.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult NGetZipcodes(string City)
        {
            try
            {
                int CityId = Convert.ToInt32(City);
                List<NZip> objNZip = new List<NZip>();
                objNZip = (from k in context.tblzip
                           where k.CityId == CityId
                           select new NZip
                           {
                               ZipId = k.ZipId,
                               Zip = k.Zip
                           }).OrderByDescending(x => x.Zip).ToList();
                List<SelectListItem> ZipList = new List<SelectListItem>();
                foreach (var t in objNZip)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.Zip.ToString();
                    s.Value = t.ZipId.ToString();
                    ZipList.Add(s);
                }
                ZipList = ZipList.OrderBy(x => x.Text).ToList();
                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                ZipList.Insert(0, objSelectListItem);
                return Json(ZipList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
                return Json(_messageToShow);
            }
        }

        public ActionResult SearchReportByReportNo(string ReportNo)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
                {
                    List<ViewPreviousInspection> objViewPreviousInspection = new List<ViewPreviousInspection>();
                    if (ReportNo != null && ReportNo != "")
                    {
                        objViewPreviousInspection = (from k in context.tblinspectionorder
                                                     join Ins in context.tblinspectionorderdetail on k.InspectionOrderDetailId equals Ins.InspectionorderdetailsID
                                                     join states in context.tblstate on Ins.PropertyState equals states.StateId
                                                     join City in context.tblcity on Ins.PropertyCity equals City.CityId
                                                     join Zip in context.tblzip on Ins.PropertyZip equals Zip.ZipId
                                                     join RV in context.ReportViewed on Ins.InspectionorderdetailsID equals RV.ReportID into g
                                                     where k.ReportNo == ReportNo.Trim()
                                                     && k.InspectionStatus == "Completed" && k.IsDeleted == false
                                                     select new ViewPreviousInspection
                                                     {

                                                         InspectionOrderId = Ins.InspectionorderdetailsID,
                                                         ReportNumber = k.ReportNo,
                                                         PropertyAddress = Ins.PropertyAddress,
                                                         PropertyAddressCity = City.CityName,
                                                         PropertyAddressState = states.StateName,
                                                         PropertyAddressZip = Zip.Zip,
                                                         ReportStatus = k.InspectionStatus,
                                                         RequesterID = k.RequesterID,
                                                         Report = g.Where(x => x.ReportID == Ins.InspectionorderdetailsID && x.UserID == Sessions.LoggedInUser.UserId && x.IsPurchased == true)
                                                     }).ToList();

                    }
                    return View(objViewPreviousInspection);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult SearchReportByAdderss(string Address, string City, string State, string Zip)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
                {
                    MySqlConnection conn = new MySqlConnection(connStr);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("SearchReportByAddressNew", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserID", Sessions.LoggedInUser.UserId.ToString());
                    cmd.Parameters.AddWithValue("Address", Address.ToString().Trim());
                    cmd.Parameters.AddWithValue("City", City.ToString().Trim());
                    if (State.ToString() == "State")
                    {
                        cmd.Parameters.AddWithValue("State", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("State", State.ToString().Trim());
                    }
                    cmd.Parameters.AddWithValue("Zip", Zip.ToString().Trim());
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    var objViewPreviousInspection = new List<ViewPreviousInspection>();
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        objViewPreviousInspection = ds.Tables[0].AsEnumerable().Select(datarow =>
                       new ViewPreviousInspection
                       {
                           InspectionOrderId = datarow["InspectionOrderDetailId"].ToString(),
                           ReportNumber = datarow["ReportNo"].ToString(),
                           PropertyAddress = datarow["PropertyAddress"].ToString(),
                           PropertyAddressCity = datarow["PropertyCity"].ToString(),
                           PropertyAddressState = datarow["PropertyState"].ToString(),
                           PropertyAddressZip = Convert.ToInt32(datarow["PropertyZip"]),
                           ReportStatus = datarow["InspectionStatus"].ToString(),
                           RequesterID = datarow["RequesterID"].ToString()
                       }).ToList();

                    }

                    return View("SearchReportByReportNo", objViewPreviousInspection);

                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public JsonResult GetResentReportsViewed(string BuyerID)
        {
            try
            {

                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
                {
                    var PaymentPlan = (from plan in context.Payments
                                       where plan.UserID == Sessions.LoggedInUser.UserId
                                       select new
                                       {
                                           p = plan.PaymentType,
                                           paymentdate = plan.CreatedDate,
                                           validity = plan.PlanValidity
                                       }).OrderByDescending(x => x.paymentdate);

                    if (PaymentPlan != null)
                    {

                        var checkForPlan = PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault();
                        if (PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault() != null && Convert.ToDateTime(PaymentPlan.Where(x => x.p == "PaymentPlan").FirstOrDefault().validity) >= System.DateTime.Now)
                        {
                            Sessions.LoggedInUser.PaymentPlan = "PaymentPlan";
                            //   return "PaymentPlan";
                        }
                        else if (PaymentPlan.Where(x => x.p == "OneTimePayment").FirstOrDefault() != null)
                        {
                            Sessions.LoggedInUser.PaymentPlan = "OneTimePaymentPlan";
                            // return "OneTimePayment";
                        }

                        else
                        {
                            Sessions.LoggedInUser.PaymentPlan = "Free";
                            //return "Free";
                        }
                    }
                    userinfo userinfo = context.UserInfoes.FirstOrDefault(x => x.EmailAddress == Sessions.LoggedInUser.EmailAddress);
                    MySqlConnection conn = new MySqlConnection(connStr);
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("GetRecentReportViewed", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserID", Sessions.LoggedInUser.UserId);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    List<ViewPreviousInspection> objViewPreviousInspection = new List<ViewPreviousInspection>();
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        objViewPreviousInspection = ds.Tables[0].AsEnumerable().Select(datarow =>
                       new ViewPreviousInspection
                       {
                           InspectionOrderId = datarow["InspectionOrderId"].ToString(),
                           ReportNumber = datarow["UniqueID"].ToString(),
                           PropertyAddress = datarow["PropertyAddress"].ToString(),
                           PropertyAddressCity = datarow["PropertyAddressCity"].ToString(),
                           PropertyAddressState = datarow["PropertyAddressState"].ToString(),
                           PropertyAddressZip = Convert.ToInt32(datarow["PropertyAddressZip"]),
                           ReportStatus = datarow["InspectionStatus"].ToString(),
                           CratedDate = Convert.ToDateTime(datarow["CreatedDate"]),
                           ReportViewdId = datarow["ID"].ToString(),
                           IsPurchased = Convert.ToBoolean(datarow["IsPurchased"]),
                           RequesterID = datarow["RequesterID"].ToString(),
                           RequesterID1 = datarow["RequesterID1"].ToString()

                       }).ToList();
                    }

                    _messageToShow.Message = "Success";
                    _messageToShow.Result = RenderRazorViewToString("_RecentViewedReports", objViewPreviousInspection);

                }
                else
                {
                    _messageToShow.Message = "Session OVer";

                }

                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
                return ReturnJson(_messageToShow);
            }
        }

        public JsonResult YourInvoices(string UserId)
        {
            if (Sessions.LoggedInUser != null)
            {

                ViewBag.InvoiceTemplate = context.EmailTemplates.Where(x => x.TemplateName == "PaymentInvoice").FirstOrDefault().TemplateDiscription;

                PaymentPoco paymentPoco = new PaymentPoco();
                paymentPoco.objPaging = new Paging();
                //paymentPoco.objPaging = PagingResult(PageNumber, PerPageRecord, islast);
                paymentPoco.objPayment = new List<Payments>();
                paymentPoco.objPayment = GetPaymentRecords(Sessions.LoggedInUser.UserId);
                //paymentPoco.objPayment = context.EmailTemplates.ToList().Where(x => x.TemplateName.Contains("PaymentInvoice")).ToList().FirstOrDefault();
                _messageToShow.Message = "Success";
                _messageToShow.Result = RenderRazorViewToString("YourInvoices", paymentPoco);
            }
            else
            {
                _messageToShow.Message = "Session Over";
            }
            return ReturnJson(_messageToShow);

        }

        private List<Payments> GetPaymentRecords(string userId)
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {

                    List<Payments> paymentList = new List<Payments>();
                    paymentList = (from details in context.Payments
                                   where details.UserID == userId
                                       && details.PaymentStatus == "Success"
                                   select new Payments
                                   {
                                       PaymentDate = details.PaymentDate,
                                       PaymentAmount = details.PaymentAmount,
                                       PaymentID = details.PaymentID,
                                       ReportID = details.ReportID,
                                       PaymentType = details.PaymentType,
                                       PaymentStatus = details.PaymentStatus,
                                       InvoiceNo = details.InvoiceNo
                                   }).ToList(); //code is commented because custom paging is not required for this time now //.OrderByDescending(x => x.PaymentDate).Skip(SkipRecord).Take(PerPageRecord)
                    if (paymentList.Count > 0)
                    {
                        paymentList.ElementAt(0).PaymentInvoice = context.EmailTemplates.ToList().Where(y => y.TemplateName.Contains("PaymentInvoice")).ToList().FirstOrDefault().TemplateDiscription.ToString();
                        var UserInfo = context.UserInfoes.ToList().Where(x => x.ID == Sessions.LoggedInUser.UserId).FirstOrDefault();

                        paymentList.ElementAt(0).CustomerAddress = UserInfo.Address + " " + UserInfo.AddressLine2 + " " + UserInfo.City + " " + UserInfo.State + " " + UserInfo.ZipCode + " " + UserInfo.Country;
                    }


                    string additionaltestingHTML = "";
                    foreach (var item in paymentList)
                    {

                        if (context.paymentaddionaltesting.Any(x => x.ReportId == item.ReportID))
                        {
                            additionaltestingHTML = "<br/><br/><div class='col-md-12'>";

                            var report = context.paymentaddionaltesting.Where(x => x.ReportId == item.ReportID).FirstOrDefault();
                            if (report.FoundationAnalysis != null)
                                additionaltestingHTML += "<div style='float:left;clear:both'>Foundation Analysis</div><div style='float:right'>$" + report.FoundationAnalysis + "</div>";
                            if (report.GasLeakDetection != null)
                                additionaltestingHTML += "<div style='float:left;clear:both'>Gas Leak Detection</div><div style='float:right'>$" + report.GasLeakDetection + "</div>";
                            if (report.MoldMoistureAnalysis != null)
                                additionaltestingHTML += "<div style='float:left;clear:both'>Mold Moisture Analysis</div><div style='float:right'>$" + report.MoldMoistureAnalysis + "</div>";
                            if (report.MoldMoistureAdditionalTest != null)
                                additionaltestingHTML += "<div style='float:left;clear:both'>Mold Moisture additional test</div><div style='float:right'>$" + report.MoldMoistureAdditionalTest + "</div>";
                            if (report.RadonTesting != null)
                                additionaltestingHTML += "<div style='float:left;clear:both'>Radon Testing</div><div style='float:right'>$" + report.RadonTesting + "</div>";
                            if (report.MethTesting != null)
                                additionaltestingHTML += "<div style='float:left;clear:both'>Meth Testing</div><div style='float:right'>$" + report.MethTesting + "</div>";
                            if (report.MethTestingAdditionalTest != null)
                                additionaltestingHTML += "<div style='float:left;clear:both'>Meth Testing additional test</div><div style='float:right'>$" + report.MethTestingAdditionalTest + "</div>";

                            additionaltestingHTML += "</div>";
                            item.AdditionalTesting = additionaltestingHTML;
                            additionaltestingHTML = "";
                        }
                    }


                    return paymentList;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }


        }
        //Seller
        public JsonResult YourUpdatableReport()
        {
            //Note Use userid or any coresponding id for fetch detail       
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    List<ViewPreviousInspection> objPreviousInspection = new List<ViewPreviousInspection>();

                    var dataInsectionOrderDetails = (from k in context.tblinspectionorder
                                                     join Ins in context.tblinspectionorderdetail
                                                     on k.InspectionOrderDetailId equals Ins.InspectionorderdetailsID
                                                     join UI in context.UserInfoes
                                                     on k.InspectorID equals UI.ID into g
                                                     join states in context.tblstate on Ins.PropertyState equals states.StateId
                                                     join city in context.tblcity on Ins.PropertyCity equals city.CityId
                                                     join zip in context.tblzip on Ins.PropertyZip equals zip.ZipId
                                                     //where k.RequesterID == Sessions.LoggedInUser.UserId
                                                     where k.RequesterID1 == Sessions.LoggedInUser.UserId
                                                     && k.InspectionStatus == "Completed" && k.IsDeleted == false
                                                     orderby k.ScheduledDate descending
                                                     select new ViewPreviousInspection
                                                     {
                                                         InspectionOrderId = Ins.InspectionorderdetailsID,
                                                         ReportNumber = k.ReportNo,
                                                         PropertyAddress = Ins.PropertyAddress,
                                                         PropertyAddressCity = city.CityName,
                                                         PropertyAddressState = states.StateName,
                                                         PropertyAddressZip = zip.Zip,
                                                         ReportStatus = k.InspectionStatus,
                                                         IsUpdatable = k.IsUpdatable,
                                                         IsVerified = k.IsVerified
                                                     }).ToList();

                    objPreviousInspection = dataInsectionOrderDetails;
                    _messageToShow.Message = "Success";
                    _messageToShow.Result = RenderRazorViewToString("YourUpdatableReport", objPreviousInspection);
                }
                else
                {
                    _messageToShow.Message = "Session Over";
                }
                return ReturnJson(_messageToShow);
            }
            catch (Exception)
            {

                throw;
            }

        }

        //Buyer
        public JsonResult BuyerRequestReport()
        {
            //Note Use userid or any coresponding id for fetch detail       
            try
            {
                if (Sessions.LoggedInUser != null)
                {

                    List<BuyerRequestModel> objBuyerRequestModel = new List<BuyerRequestModel>();
                    objBuyerRequestModel = (from inso in context.tblinspectionorderdetail
                                            join ins in context.tblinspectionorder on inso.InspectionorderdetailsID equals ins.InspectionOrderDetailId
                                            join st in context.tblstate on inso.PropertyState equals st.StateId
                                            join cit in context.tblcity on inso.PropertyCity equals cit.CityId
                                            join zip in context.tblzip on inso.PropertyZip equals zip.ZipId
                                            join ui in context.UserInfoes on ins.InspectorID equals ui.ID into k
                                            join schedule in context.tblSchedule on inso.InspectionorderdetailsID equals schedule.InspectionOrderId into g
                                            where ins.RequesterID == Sessions.LoggedInUser.UserId && ins.IsActive == true && ins.IsDeleted == false

                                            select new BuyerRequestModel
                                            {
                                                OrderId = inso.InspectionorderdetailsID,
                                                HouseNo = inso.PropertyAddress,
                                                City = cit.CityName,
                                                State = st.StateName,
                                                Zip = zip.Zip,
                                                RequestDate = ins.Createddate,

                                                InspectorDetails = k,
                                                InspectorId = ins.InspectorID,
                                                Status = ins.InspectionStatus,
                                                Schedule = g
                                            }).ToList();


                    _messageToShow.Message = "Success";
                    // if (objBuyerRequestModel != null && objBuyerRequestModel.Count > 0)
                    {
                        _messageToShow.Result = RenderRazorViewToString("BuyerUpdatableReport", objBuyerRequestModel);
                    }
                }
                else
                {
                    _messageToShow.Message = "Session Over";
                }
                return ReturnJson(_messageToShow);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public JsonResult RequestUpdatePrivilages(string OrderID)
        {
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    if (Convert.ToString(OrderID) != null && Convert.ToString(OrderID) != "")
                    {
                        string InspectionOrderId = Convert.ToString(OrderID);
                        tblinspectionorder objInspectionOrder = new tblinspectionorder();
                        objInspectionOrder = context.tblinspectionorder.FirstOrDefault(x => x.InspectionOrderDetailId == InspectionOrderId);

                        string MailTemplate = context.EmailTemplates.Where(x => x.TemplateId == 53).FirstOrDefault().TemplateDiscription;
                        MailTemplate = MailTemplate.Replace("{First Name}", Sessions.LoggedInUser.FirstName);
                        MailTemplate = MailTemplate.Replace("{Last Name}", Sessions.LoggedInUser.FirstName);
                        MailTemplate = MailTemplate.Replace("Your Inspection request for report no :{reportNo} has been completed with spectormax", "");
                        MailTemplate = MailTemplate.Replace("Please click here update the report", "");
                        MailTemplate = MailTemplate.Replace("Dashboard", "Dashboard/" + OrderID);


                        if (objInspectionOrder != null)
                        {
                            #region Save Random number in table
                            Random random = new Random();
                            int num = random.Next(10000);
                            objInspectionOrder.VerificationNumber = num.ToString();
                            objInspectionOrder.IsVerified = false;
                            context.SaveChanges();
                            #endregion

                            #region Send random number in email

                            EmailHelper email = new EmailHelper();
                            //string emailBody = "Verification number to update the reopot is: " + num.ToString();
                            MailTemplate = MailTemplate.Replace("{Code}", num.ToString());
                            bool status = email.SendEmail("", "Updatable Report Verification Number", "", MailTemplate, Sessions.LoggedInUser.EmailAddress);
                            #endregion


                            if (status == true)
                            {
                                _messageToShow.Message = "Success";
                            }
                            else
                            {
                                _messageToShow.Message = "Mail Not Sent";
                            }
                        }
                        else
                        {
                            _messageToShow.Message = "Error";
                        }
                    }
                    else
                    {
                        _messageToShow.Message = "Error";
                    }
                }
                else
                {
                    _messageToShow.Message = "Session Over";
                }
                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult ShareYourExperience(string FirstName, string LastName, string State, string ReportNo, string Comment, string Role)
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
                {
                    int stateid = Convert.ToInt32(State);
                    tbltestimonial objTestimonials = new tbltestimonial();
                    objTestimonials.ID = Guid.NewGuid().ToString();
                    objTestimonials.UserID = Sessions.LoggedInUser.UserId;
                    objTestimonials.FirstName = FirstName;
                    objTestimonials.LastName = LastName;
                    objTestimonials.ReportNo = ReportNo;
                    objTestimonials.StateId = stateid;
                    objTestimonials.Comments = Comment;
                    objTestimonials.IsActive = false;
                    objTestimonials.IsDeleted = false;
                    objTestimonials.CreatedBy = Sessions.LoggedInUser.UserId;
                    objTestimonials.CreatedDate = System.DateTime.Now;
                    objTestimonials.Role = Convert.ToInt32(Role);
                    context.tblTestimonial.Add(objTestimonials);
                    context.SaveChanges();
                    _messageToShow.Message = "Success";



                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    _messageToShow.Message = "Session Over";
                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult ShareYourExperienceAgent(string FirstName, string LastName, string State, string Comment, string Role)
        {
            try
            {
                // if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
                {
                    int stateid = Convert.ToInt32(State);
                    tbltestimonial objTestimonials = new tbltestimonial();
                    objTestimonials.ID = Guid.NewGuid().ToString();
                    //   objTestimonials.UserID = Sessions.LoggedInUser.UserId;
                    objTestimonials.FirstName = FirstName;
                    objTestimonials.LastName = LastName;
                    //objTestimonials.ReportNo = ReportNo;
                    objTestimonials.StateId = stateid;
                    objTestimonials.Comments = Comment;
                    objTestimonials.IsActive = false;
                    objTestimonials.IsDeleted = false;
                    //   objTestimonials.CreatedBy = Sessions.LoggedInUser.UserId;
                    objTestimonials.CreatedDate = System.DateTime.Now;
                    objTestimonials.Role = Convert.ToInt32(Role);
                    context.tblTestimonial.Add(objTestimonials);
                    context.SaveChanges();
                    _messageToShow.Message = "Success";



                    return Json(_messageToShow, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public JsonResult ViewInspectorProfile(string InspectorId)
        {
            InspectorDetails objInspectorDetail = new InspectorDetails();
            try
            {
                if (Sessions.LoggedInUser != null)
                {
                    userinfo objuserinfo = new userinfo();

                    objuserinfo = context.UserInfoes.Where(x => x.ID == InspectorId).FirstOrDefault();
                    objInspectorDetail.FirstName = objuserinfo.FirstName;
                    objInspectorDetail.LastName = objuserinfo.LastName;
                    objInspectorDetail.Address = objuserinfo.Address;
                    objInspectorDetail.Address2 = objuserinfo.AddressLine2;
                    objInspectorDetail.State = objuserinfo.State;
                    objInspectorDetail.StreetAddress = objuserinfo.StreetAddress;
                    objInspectorDetail.City = objuserinfo.City;
                    objInspectorDetail.Country = objuserinfo.Country;
                    objInspectorDetail.EmailAddress = objuserinfo.EmailAddress;
                    objInspectorDetail.dateofbirth = objuserinfo.DateOfBirth;
                    objInspectorDetail.DOB = Convert.ToDateTime(objuserinfo.DateOfBirth).ToString("MM-yyyy");
                    objInspectorDetail.PhoneNo = objuserinfo.PhoneNo;
                    var inspectorDetail = context.InspectorDetail.FirstOrDefault(x => x.UserID == InspectorId);
                    if (inspectorDetail != null)
                    {
                        objInspectorDetail.PhotoURL = inspectorDetail.PhotoURL;
                        objInspectorDetail.ExperienceTraining1 = inspectorDetail.ExperienceTraining1;
                        objInspectorDetail.ExperienceTraining2 = inspectorDetail.ExperienceTraining2;
                        objInspectorDetail.ExperienceTraining3 = inspectorDetail.ExperienceTraining3;
                        objInspectorDetail.Certification1 = inspectorDetail.Certification1;
                        objInspectorDetail.Certification2 = inspectorDetail.Certification2;
                        objInspectorDetail.Certification3 = inspectorDetail.Certification3;
                    }
                    else
                    {
                        objInspectorDetail.PhotoURL = "";
                        objInspectorDetail.ExperienceTraining1 = "";
                        objInspectorDetail.ExperienceTraining2 = "";
                        objInspectorDetail.ExperienceTraining3 = "";
                        objInspectorDetail.Certification1 = "";
                        objInspectorDetail.Certification2 = "";
                        objInspectorDetail.Certification3 = "";

                    }

                    _messageToShow.Message = "Success";
                    if (objInspectorDetail != null)
                    {
                        _messageToShow.Result = RenderRazorViewToString("~/Views/UserInfoes/_ViewInspectorDetails.cshtml", objInspectorDetail).ToString();
                    }
                }
                else
                {
                    _messageToShow.Message = "Session Over";
                }
                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult ViewInspectorProfile1(string InspectorId)
        {
            InspectorDetails objInspectorDetail = new InspectorDetails();
            try
            {

                userinfo objuserinfo = new userinfo();

                objuserinfo = context.UserInfoes.Where(x => x.ID == InspectorId).FirstOrDefault();
                objInspectorDetail.FirstName = objuserinfo.FirstName;
                objInspectorDetail.LastName = objuserinfo.LastName;
                objInspectorDetail.Address = objuserinfo.Address;
                objInspectorDetail.Address2 = objuserinfo.AddressLine2;
                objInspectorDetail.State = objuserinfo.State;
                objInspectorDetail.StreetAddress = objuserinfo.StreetAddress;
                objInspectorDetail.City = objuserinfo.City;
                objInspectorDetail.Country = objuserinfo.Country;
                objInspectorDetail.EmailAddress = objuserinfo.EmailAddress;
                objInspectorDetail.dateofbirth = objuserinfo.DateOfBirth;
                objInspectorDetail.DOB = Convert.ToDateTime(objuserinfo.DateOfBirth.Value.Date).ToString("MM-yyyy");
                objInspectorDetail.PhoneNo = objuserinfo.PhoneNo;
                var inspectorDetail = context.InspectorDetail.FirstOrDefault(x => x.UserID == InspectorId);
                if (inspectorDetail != null)
                {
                    objInspectorDetail.PhotoURL = inspectorDetail.PhotoURL;
                    objInspectorDetail.ExperienceTraining1 = inspectorDetail.ExperienceTraining1;
                    objInspectorDetail.ExperienceTraining2 = inspectorDetail.ExperienceTraining2;
                    objInspectorDetail.ExperienceTraining3 = inspectorDetail.ExperienceTraining3;
                    objInspectorDetail.Certification1 = inspectorDetail.Certification1;
                    objInspectorDetail.Certification2 = inspectorDetail.Certification2;
                    objInspectorDetail.Certification3 = inspectorDetail.Certification3;
                }
                else
                {
                    objInspectorDetail.PhotoURL = "";
                    objInspectorDetail.ExperienceTraining1 = "";
                    objInspectorDetail.ExperienceTraining2 = "";
                    objInspectorDetail.ExperienceTraining3 = "";
                    objInspectorDetail.Certification1 = "";
                    objInspectorDetail.Certification2 = "";
                    objInspectorDetail.Certification3 = "";

                }

                _messageToShow.Message = "Success";
                if (objInspectorDetail != null)
                {
                    _messageToShow.Result = RenderRazorViewToString("~/Views/UserInfoes/_ViewInspectorDetails.cshtml", objInspectorDetail).ToString();
                }


                return ReturnJson(_messageToShow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult CreateSiteUser(UserInfoPoco userInfoPoco)
        {
            try
            {
                if (context.UserInfoes.FirstOrDefault(x => x.EmailAddress == userInfoPoco.EmailAddress && x.IsDeleted == false) == null)
                {

                    string UniqueId = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["UniqueId"];
                    string role = string.Empty;
                    userinfo userInfo = new userinfo();
                    userInfo.ID = Guid.NewGuid().ToString();
                    userInfo.Address = userInfoPoco.Address;
                    userInfo.AddressLine2 = userInfoPoco.AddressLine2;
                    userInfo.CityId = userInfoPoco.CityId;
                    userInfo.Country = userInfoPoco.Country;
                    userInfo.CreatedDate = DateTime.Now;
                    userInfo.ModifiedDate = DateTime.Now;
                    DateTime? dt = CreateDate(userInfoPoco.DOB);
                    userInfo.DateOfBirth = dt;
                    //userInfo.DateOfBirth = userInfoPoco.DateOfBirth;
                    userInfo.EmailAddress = userInfoPoco.EmailAddress;
                    userInfo.FirstName = userInfoPoco.FirstName;
                    userInfo.LastName = userInfoPoco.LastName;
                    userInfo.ModifiedDate = userInfoPoco.ModifiedDate;
                    userInfo.Password = userInfoPoco.Password;
                    userInfo.PhoneNo = userInfoPoco.PhoneNo;
                    userInfo.ZipId = userInfoPoco.ZipId;
                    userInfo.Role = "7D9A1A9C-5305-455F-A07C-0BF3080A6753";
                    userInfo.StateId = userInfoPoco.StateId;
                    userInfo.Country = userInfoPoco.Country;
                    userInfo.SecurityQuestion = userInfoPoco.SecurityQuestion;
                    userInfo.SecurityAnswer = userInfoPoco.SecurityAnswer;
                    userInfo.StreetAddress = userInfoPoco.StreetAddress;
                    userInfo.IsDeleted = false;
                    userInfo.IsActive = false;
                    userInfo.IsEmailVerified = false;
                    //if (userInfoPoco.Role == "85DA6641-E08A-4192-A")
                    //    userInfo.IsActive = false;
                    //else
                    //    userInfo.IsActive = true;
                    context.UserInfoes.Add(userInfo);
                    context.SaveChanges();

                    if (userInfoPoco.Role == "85DA6641-E08A-4192-A")
                    {
                        notificationdetail objNotification = new notificationdetail();
                        objNotification.NotifcationFrom = userInfo.ID;
                        objNotification.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                        objNotification.NotificationTypeID = 4;
                        objNotification.IsRead = false;
                        objNotification.CreatedDate = System.DateTime.Now;
                        objNotification.RequestID = "";
                        context.NotificationDetail.Add(objNotification);
                        context.SaveChanges();
                    }


                    //return Json(_messageToShow);
                    EmailHelper email = new EmailHelper();
                    string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "SystemIntroduction").TemplateDiscription;
                    emailBody = emailBody.Replace("{First Name}", userInfo.FirstName);
                    emailBody = emailBody.Replace("{Last Name}", userInfo.LastName);
                    emailBody = emailBody.Replace("{email}", userInfo.EmailAddress);
                    emailBody = emailBody.Replace("{password}", userInfo.Password);
                    emailBody = emailBody.Replace("ID", userInfo.ID);

                    bool status = email.SendEmail("", "Welcome To SpectorMax", "", emailBody, userInfoPoco.EmailAddress);
                    if (status == true)
                    {
                        //if (userInfo.Role == "88741AFB-09FE-44C2-B")
                        //{
                        //    return Json("Buyer", JsonRequestBehavior.AllowGet);
                        //    //_messageToShow.UserID=userInfo.ID;

                        //}
                        //else if (userInfo.Role == "9C15FC8A-CDD3-49CD-9")
                        //{
                        //    return Json("Seller", JsonRequestBehavior.AllowGet);
                        //    //_messageToShow.Message="Seller";              
                        //}
                        //else if (userInfo.Role == "7D9A1A9C-5305-455F-A07C-0BF3080A6753")
                        //{
                        //    return Json("SiteUser", JsonRequestBehavior.AllowGet);
                        //}
                        return Json("SiteUser", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //if (userInfo.Role == "88741AFB-09FE-44C2-B")
                        //{
                        //    return Json("Buyer", JsonRequestBehavior.AllowGet);
                        //    //_messageToShow.UserID=userInfo.ID;

                        //}
                        //else if (userInfo.Role == "9C15FC8A-CDD3-49CD-9")
                        //{
                        //    return Json("Seller", JsonRequestBehavior.AllowGet);
                        //    //_messageToShow.Message="Seller";              
                        //}
                        //else if (userInfo.Role == "7D9A1A9C-5305-455F-A07C-0BF3080A6753")
                        //{
                        //    return Json("SiteUser", JsonRequestBehavior.AllowGet);
                        //}
                        return Json("SiteUser", JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json("Email Address Already Exist", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult RecommendSeller(string EmailAddress)
        {
            try
            {
                EmailHelper email = new EmailHelper();
                string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "RecommendSeller").TemplateDiscription;


                bool status = email.SendEmail("", "Welcome To SpectorMax", "", emailBody, EmailAddress);
                if (status == true)
                {
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Mail Not Sent", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ConfirmEmail(string id)
        {
            try
            {
                userinfo ui = context.UserInfoes.Where(x => x.ID == id).FirstOrDefault();
                string message = string.Empty;
                if (ui != null)
                {
                    if (ui.Role != "85DA6641-E08A-4192-A")
                    {
                        ui.IsActive = true;
                        ui.IsEmailVerified = true;
                        context.SaveChanges();
                        message = "Confirmed";
                        ViewBag.Fname = ui.FirstName;
                        ViewBag.Lname = ui.LastName;
                    }
                    else
                    {
                        message = "Not Confirmed";
                    }
                }
                else
                {
                    message = "Not Confirmed";
                }
                ViewBag.message = message;
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}