using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;
using SpectorMax.Entities.Classes;
using System.Data.Objects;

namespace SpectorMax.Controllers
{
    public class InspectorRatingsController : BaseController
    {
        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _MessageToShow = new MessageToShow();
        //
        // GET: /InspectorRatings/

        public ViewResult Index(FormCollection form)
        {
              try
                {

                    if (Sessions.LoggedInUser != null)
                    {
                        int? zipcode;
                        InspectorRatingPoco objInspectorRatingPocoPoco;
                        ViewBag.Message = null;
                        List<InspectorRatingPoco> ListofRatingDetails = new List<InspectorRatingPoco>();
                        if (Sessions.LoggedInUser.RoleName == "Property Inspector")
                        {
                            var RatingList = context.InspectorRatings.ToList().Where(x => x.InspectorID == Sessions.LoggedInUser.UserId).ToList();
                            if (RatingList.Count > 0)
                            {
                                foreach (var item in RatingList)
                                {
                                    objInspectorRatingPocoPoco = new InspectorRatingPoco();
                                    objInspectorRatingPocoPoco.rating = item.Rating1;
                                    objInspectorRatingPocoPoco.Comment = item.Comment;
                                    objInspectorRatingPocoPoco.ratingID = item.RatingID;
                                    objInspectorRatingPocoPoco.IsRequestedtoRemove = item.RequestedtoRemove;
                                    userinfo objratingBy = context.UserInfoes.Where(x => x.ID == item.RatingByID).ToList().FirstOrDefault();
                                    if (objratingBy != null)
                                    {
                                        objInspectorRatingPocoPoco.ratingBy = objratingBy.FirstName + " " + objratingBy.LastName + " From " + objratingBy.City + "," + objratingBy.State + " On " + item.CreatedDate.Value.ToShortDateString(); ;
                                        ListofRatingDetails.Add(objInspectorRatingPocoPoco);
                                    }
                                }
                            }
                            else
                            {
                                //string InpectorName = context.UserInfoes.Where(x => x.ZipCode == zipcode && x.Role == "85DA6641-E08A-4192-A").FirstOrDefault().FirstName;
                                ViewBag.Message = "No ratings assigned ";
                            }
                            return View(ListofRatingDetails);
                        }
                        else
                        {

                            if (form.Count > 0)
                            {

                                zipcode = Convert.ToInt32(form[0]);


                                //Convert.ToInt32(form.Get("txtSearchLocation"));
                                bool IsExist = false;
                                //List<UserInfo> UserinZipCode = context.ZipCode.Where(x => x.Zip == zipcode).ToList();
                                IsExist = context.ZipCodeAssigned.Any(x => x.ZipCode == zipcode);

                                //IsExist = UserinZipCode.Any(x => x.Role == "85DA6641-E08A-4192-A");

                                if (IsExist == true)
                                {
                                    string userid = context.ZipCodeAssigned.Where(x => x.ZipCode == zipcode).FirstOrDefault().InspectorID;

                                    #region Inspector Details
                                    userinfo objuserinfo = new userinfo();
                                    InspectorDetails objInspectorDetail = new InspectorDetails();
                                    objuserinfo = context.UserInfoes.Where(x => x.ID == userid).FirstOrDefault();
                                    objInspectorDetail.FirstName = objuserinfo.FirstName;
                                    objInspectorDetail.LastName = objuserinfo.LastName;
                                    objInspectorDetail.Address = objuserinfo.Address;
                                    objInspectorDetail.Address2 = objuserinfo.AddressLine2;
                                    objInspectorDetail.State = objuserinfo.State;
                                    objInspectorDetail.StreetAddress = objuserinfo.StreetAddress;
                                    objInspectorDetail.City = objuserinfo.City;
                                    objInspectorDetail.Country = objuserinfo.Country;
                                    objInspectorDetail.EmailAddress = objuserinfo.EmailAddress;
                                    objInspectorDetail.dateofbirth = objuserinfo.DateOfBirth;
                                    objInspectorDetail.PhoneNo = objuserinfo.PhoneNo;
                                    var inspectorDetail = context.InspectorDetail.FirstOrDefault(x => x.UserID == userid);
                                    if (inspectorDetail != null)
                                    {
                                        objInspectorDetail.PhotoURL = inspectorDetail.PhotoURL;
                                        objInspectorDetail.ExperienceTraining1 = inspectorDetail.ExperienceTraining1;
                                        objInspectorDetail.ExperienceTraining2 = inspectorDetail.ExperienceTraining2;
                                        objInspectorDetail.ExperienceTraining3 = inspectorDetail.ExperienceTraining3;
                                        objInspectorDetail.Certification1 = inspectorDetail.Certification1;
                                        objInspectorDetail.Certification2 = inspectorDetail.Certification2;
                                        objInspectorDetail.Certification3 = inspectorDetail.Certification3;

                                    }
                                    else
                                    {
                                        objInspectorDetail.PhotoURL = "";
                                        objInspectorDetail.ExperienceTraining1 = "";
                                        objInspectorDetail.ExperienceTraining2 = "";
                                        objInspectorDetail.ExperienceTraining3 = "";
                                        objInspectorDetail.Certification1 = "";
                                        objInspectorDetail.Certification2 = "";
                                        objInspectorDetail.Certification3 = "";

                                    }

                                    #endregion


                                    objInspectorRatingPocoPoco = new InspectorRatingPoco();
                                    objInspectorRatingPocoPoco.Inspector = objInspectorDetail;
                                    var RatingList = context.InspectorRatings.ToList().Where(x => x.InspectorID == userid).ToList();
                                    if (RatingList.Count > 0)
                                    {
                                        foreach (var item in RatingList)
                                        {
                                            // objInspectorRatingPocoPoco = new InspectorRatingPoco();
                                            objInspectorRatingPocoPoco.rating = item.Rating1;
                                            objInspectorRatingPocoPoco.Comment = item.Comment;
                                            objInspectorRatingPocoPoco.ratingID = item.RatingID;
                                            objInspectorRatingPocoPoco.IsRequestedtoRemove = item.RequestedtoRemove;
                                            userinfo objratingBy = context.UserInfoes.Where(x => x.ID == item.RatingByID).ToList().FirstOrDefault();
                                            if (objratingBy != null)
                                            {
                                                objInspectorRatingPocoPoco.ratingBy = objratingBy.FirstName + " " + objratingBy.LastName + " From " + objratingBy.City + "," + objratingBy.State + " On " + item.CreatedDate.Value.ToShortDateString(); ;
                                                ListofRatingDetails.Add(objInspectorRatingPocoPoco);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ViewBag.Message = "NO RATINGS ASSIGNED YET";
                                        objInspectorRatingPocoPoco.Inspector = objInspectorDetail;
                                        ListofRatingDetails.Add(objInspectorRatingPocoPoco);
                                    }
                                }
                                else
                                {
                                    ViewBag.Message = "NO INSPECTOR IS EXIST ON THIS ZIPCODE";
                                }
                            }
                            else
                            {
                                ViewBag.Message = "NO INSPECTOR IS EXIST ON THIS ZIPCODE";
                            }
                            return View(ListofRatingDetails);
                        }
                    }
                    else
                    {
                        return View("~/Views/Home/Index.cshtml");
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
           
        }

        public JsonResult GetRating(int PageNumber, int PerPageRecord, bool islast)
        {
            InspectorRatingPoco objInspectorRatingPocoPoco;
            List<InspectorRatingPoco> ListofRatingDetails = new List<InspectorRatingPoco>();

            int zipcode = 160071;//Convert.ToInt32(form.Get("txtSearchLocation"));
            List<userinfo> UserinZipCode = context.UserInfoes.Where(x => x.ZipCode == zipcode).ToList();
            int? ZipCode = UserinZipCode.Where(x => x.Role == "85DA6641-E08A-4192-A").FirstOrDefault().ZipCode;
            string insid = context.UserInfoes.Where(x => x.ZipCode == ZipCode && x.Role == "85DA6641-E08A-4192-A").FirstOrDefault().ID;

            foreach (var item in context.InspectorRatings.ToList().Where(x => x.InspectorID == insid).ToList())
            {
                objInspectorRatingPocoPoco = new InspectorRatingPoco();
                objInspectorRatingPocoPoco.rating = item.Rating1;
                objInspectorRatingPocoPoco.Comment = item.Comment;
                userinfo objratingBy = context.UserInfoes.Where(x => x.ID == item.RatingByID).ToList().FirstOrDefault();
                objInspectorRatingPocoPoco.ratingBy = objratingBy.FirstName + " " + objratingBy.LastName + " From " + objratingBy.City + "," + objratingBy.State + " On " + item.CreatedDate.Value.ToShortDateString(); ;
                ListofRatingDetails.Add(objInspectorRatingPocoPoco);
            }
            long totalRecord = ListofRatingDetails.Count;
            ListofRatingDetails[0].paging = PagingResult(totalRecord, PageNumber, PerPageRecord, islast);
            ListofRatingDetails = ListofRatingDetails.Skip(ListofRatingDetails[0].paging.SkipRecord).Take(PerPageRecord).ToList();
            _MessageToShow.Result = RenderRazorViewToString("Index", ListofRatingDetails);
            return ReturnJson(_MessageToShow);
        }

        public ActionResult InspectorRating()
        {
            return View();
        }



        public Paging PagingResult(long totalRecord, int PageNumber, int PerPageRecord, bool islast)
        {
            Paging paging = new Paging();
            //long totalRecord = 0;
            int skiprecord = 0;
            int totalPage = 0;
            int totalPages = 0;
            decimal countpage = 0;
            //totalRecord = context.Payments.ToList().Count;
            countpage = Convert.ToDecimal(totalRecord % PerPageRecord);
            totalPage = (int)totalRecord / PerPageRecord;
            if (countpage > 0)
                totalPage = totalPage + 1;
            if (islast == true)
            {
                skiprecord = (totalPage - 1) * PerPageRecord;
                PageNumber = totalPage;
            }
            else
            {
                PageNumber = (PageNumber > totalPage ? totalPage : (PageNumber <= 0 ? 1 : PageNumber));
                skiprecord = (PageNumber == 1 ? 0 : PageNumber - 1) * PerPageRecord;
            }
            totalPages = (int)totalPage;
            paging = new Paging();
            paging.TotalRecord = (int)totalRecord;
            paging.PageNumber = PageNumber;
            paging.PerPageRecord = PerPageRecord;
            paging.TotalPage = totalPages;
            paging.SkipRecord = skiprecord;
            paging.PerPageRecordlist = CommonFunctions.PerPageRecordlist();

            //if (islast == true)
            PageNumber = PageNumber - 1;

            // Please Below code don't delete it may be usefull for testing.

            //pagingPoco.ObjInspectionOrderDetailsPaging = new List<InspectionOrderDetailsPaging>();
            //pagingPoco.ObjInspectionOrderDetailsPaging = (from details in context.InsectionOrderDetails
            //                                              select new InspectionOrderDetailsPaging
            //                                              {
            //                                                  InspectionOrderId = details.InspectionOrderId,
            //                                                  OwnerFirstName = details.OwnerFirstName,
            //                                                  OwnerLastName = details.OwnerLastName,
            //                                                  PropertyAddress = details.PropertyAddress,
            //                                                  PropertyAddressCity = details.PropertyAddressCity,
            //                                                  PropertyAddressState = details.PropertyAddressState,
            //                                                  PropertyAddressZip = details.PropertyAddressZip,
            //                                                  ScheduleInspectionDate = details.ScheduleInspectionDate
            //                                              }).OrderByDescending(item => item.OwnerFirstName).Skip(skiprecord).Take(PerPageRecord).ToList();

            return paging;
        }


        //
        // GET: /InspectorRatings/Details/5

        public ViewResult Details(string id)
        {
            inspectorrating inspectorrating = context.InspectorRatings.Single(x => x.RatingID == id);

            return View(inspectorrating);
        }

        //
        // GET: /InspectorRatings/Create

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult NCreate()
        {
            try
            {
                if (Sessions.LoggedInUser != null && (Sessions.LoggedInUser.RoleName == "SiteUser"))
                {
                    List<ViewPreviousInspection> objViewPreviousInspection = new List<ViewPreviousInspection>();
                    objViewPreviousInspection = (from k in context.tblinspectionorder
                                                 join Ins in context.tblinspectionorderdetail
                                                 on k.InspectionOrderDetailId equals Ins.InspectionorderdetailsID
                                                 join s in context.tblstate on Ins.PropertyState equals s.StateId
                                                 join c in context.tblcity on Ins.PropertyCity equals c.CityId
                                                 join z in context.tblzip on Ins.PropertyZip equals z.ZipId
                                                 where
                                                 !context.Checklists.Select(x => x.InspectionOrderId).Contains(k.InspectionOrderDetailId)
                                                 && k.RequesterID == Sessions.LoggedInUser.UserId && k.IsDeleted == false
                                                 select new ViewPreviousInspection
                                                 {
                                                     InspectionOrderId = k.InspectionOrderDetailId,
                                                     PropertyAddress = Ins.PropertyAddress + "," + c.CityName+ "," + s.StateName,
                                                     PropertyAddressCity = c.CityName,
                                                     PropertyAddressState = s.StateName
                                                 }).ToList();
                    ViewData["ReportList"] = objViewPreviousInspection;
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        // POST: /InspectorRatings/Create

        [HttpPost]
        public JsonResult PostInspectionSurvey(float rating1, float rating2, float rating3, float rating4, float rating5, float rating6, string comments, string InspectionId)
        {
            try
            {
                
                inspectorrating objInspectorRating = new SpectorMaxDAL.inspectorrating();
                objInspectorRating.RatingID = Guid.NewGuid().ToString();
                objInspectorRating.InspectorID = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == InspectionId).FirstOrDefault().InspectorID;
                objInspectorRating.InspectionID = InspectionId;
                objInspectorRating.Rating1 = rating1;
                objInspectorRating.Rating2 = rating2;
                objInspectorRating.Rating3 = rating4;
                objInspectorRating.Rating4 = rating4;
                objInspectorRating.Rating5 = rating5;
                objInspectorRating.Rating6 = rating6;
                objInspectorRating.Comment = comments;
                objInspectorRating.RatingByID = Sessions.LoggedInUser.UserId;
                objInspectorRating.RequestedtoRemove = false;
                objInspectorRating.CreatedDate = DateTime.Now;
                objInspectorRating.ModifiedDate = DateTime.Now;
                context.InspectorRatings.Add(objInspectorRating);
                context.SaveChanges();
                _MessageToShow.Message = "Success";
            }
            catch (Exception ex)
            {
                _MessageToShow.Message = ex.Message.ToString();
            }
            return Json(_MessageToShow);
        }

        //
        // GET: /InspectorRatings/Edit/5

        public ActionResult Edit(string id)
        {
            inspectorrating inspectorrating = context.InspectorRatings.Single(x => x.RatingID == id);
            return View(inspectorrating);
        }

        //
        // POST: /InspectorRatings/Edit/5

        [HttpPost]
        public ActionResult Edit(inspectorrating inspectorrating)
        {
            if (ModelState.IsValid)
            {
                context.Entry(inspectorrating).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inspectorrating);
        }

        //
        // GET: /InspectorRatings/Delete/5

        public ActionResult Delete(string id)
        {
            inspectorrating inspectorrating = context.InspectorRatings.Single(x => x.RatingID == id);
            return View(inspectorrating);
        }

        //
        // POST: /InspectorRatings/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            inspectorrating inspectorrating = context.InspectorRatings.Single(x => x.RatingID == id);
            context.InspectorRatings.Remove(inspectorrating);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }

        /*Post : Admin is requested to remove the comment from inspector profile*/
        public JsonResult RequestAdmintoRemoveComment(string RatingId)
        {
            try
            {
                context.InspectorRatings.FirstOrDefault(x => x.RatingID == RatingId).RequestedtoRemove = true;
                context.SaveChanges();

                /*Add Notification Details-Start*/

                notificationdetail objNotification = new notificationdetail();
                objNotification.NotifcationFrom = Sessions.LoggedInUser.UserId;
                objNotification.NotificationTo = context.UserInfoes.FirstOrDefault(x => x.Role == "AA11573C-7688-4786-9").ID;
                objNotification.NotificationTypeID = 3;
                objNotification.IsRead = false;
                objNotification.CreatedDate = System.DateTime.Now;
                objNotification.RequestID = RatingId;
                context.NotificationDetail.Add(objNotification);
                context.SaveChanges();

                /*End*/

                return ReturnJson("Success");
            }
            catch (Exception)
            {
                return ReturnJson("Fail");
            }
        }
    }
}