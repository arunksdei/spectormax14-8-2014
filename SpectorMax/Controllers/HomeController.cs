﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpectorMaxDAL;
using SpectorMax.Models;
using SpectorMax.Entities.Classes;
using System.Web.Caching;
using System.Web.Security;
using Newtonsoft.Json;
using System.Text;

namespace SpectorMax.Controllers
{
    public class HomeController : BaseController
    {
        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _messageToShow = new MessageToShow();
        public ActionResult Index(string message)
        {

            tblsamplereport objSample = context.tblsamplereport.FirstOrDefault();
            if (objSample != null)
            {
                Session["ReportNo"] = objSample.ReportID;
                Session["UniqueId"] = objSample.ReportNo;
            }
            ViewBag.Message = message;
            ViewBag.Date = System.DateTime.Now;
            return View();
        }

        public ActionResult InspectorRating(string ZipCode)
        {
            try
            {

                int? zipcode;
                InspectorRatingPoco objInspectorRatingPocoPoco;
                ViewBag.Message = null;
                List<InspectorRatingPoco> ListofRatingDetails = new List<InspectorRatingPoco>();

                {
                    zipcode = Convert.ToInt32(ZipCode);
                    bool IsExist = false;
                    IsExist = context.ZipCodeAssigned.Any(x => x.ZipCode == zipcode);



                    if (IsExist == true)
                    {
                        string userid = context.ZipCodeAssigned.Where(x => x.ZipCode == zipcode).FirstOrDefault().InspectorID;

                        #region Inspector Details
                        userinfo objuserinfo = new userinfo();
                        InspectorDetails objInspectorDetail = new InspectorDetails();
                        objuserinfo = context.UserInfoes.Where(x => x.ID == userid && x.Role == "85DA6641-E08A-4192-A").FirstOrDefault();
                        if (objuserinfo != null)
                        {
                            objInspectorDetail.FirstName = objuserinfo.FirstName;
                            objInspectorDetail.LastName = objuserinfo.LastName;
                            objInspectorDetail.Address = objuserinfo.Address;
                            objInspectorDetail.Address2 = objuserinfo.AddressLine2;
                            objInspectorDetail.State = objuserinfo.State;
                            objInspectorDetail.StreetAddress = objuserinfo.StreetAddress;
                            objInspectorDetail.City = objuserinfo.City;
                            objInspectorDetail.Country = objuserinfo.Country;
                            objInspectorDetail.EmailAddress = objuserinfo.EmailAddress;
                            objInspectorDetail.dateofbirth = objuserinfo.DateOfBirth;
                            objInspectorDetail.PhoneNo = objuserinfo.PhoneNo;
                            var inspectorDetail = context.InspectorDetail.FirstOrDefault(x => x.UserID == userid);
                            if (inspectorDetail != null)
                            {
                                objInspectorDetail.PhotoURL = inspectorDetail.PhotoURL;
                                objInspectorDetail.ExperienceTraining1 = inspectorDetail.ExperienceTraining1;
                                objInspectorDetail.ExperienceTraining2 = inspectorDetail.ExperienceTraining2;
                                objInspectorDetail.ExperienceTraining3 = inspectorDetail.ExperienceTraining3;
                                objInspectorDetail.Certification1 = inspectorDetail.Certification1;
                                objInspectorDetail.Certification2 = inspectorDetail.Certification2;
                                objInspectorDetail.Certification3 = inspectorDetail.Certification3;
                            }
                            else
                            {
                                objInspectorDetail.PhotoURL = "";
                                objInspectorDetail.ExperienceTraining1 = "";
                                objInspectorDetail.ExperienceTraining2 = "";
                                objInspectorDetail.ExperienceTraining3 = "";
                                objInspectorDetail.Certification1 = "";
                                objInspectorDetail.Certification2 = "";
                                objInspectorDetail.Certification3 = "";

                            }

                        #endregion


                            objInspectorRatingPocoPoco = new InspectorRatingPoco();
                            objInspectorRatingPocoPoco.Inspector = objInspectorDetail;
                            var RatingList = context.InspectorRatings.ToList().Where(x => x.InspectorID == userid).ToList();


                            if (RatingList.Count > 0)
                            {
                                foreach (var item in RatingList)
                                {
                                    objInspectorRatingPocoPoco.rating = item.Rating1;
                                    objInspectorRatingPocoPoco.Comment = item.Comment;
                                    objInspectorRatingPocoPoco.ratingID = item.RatingID;
                                    objInspectorRatingPocoPoco.IsRequestedtoRemove = item.RequestedtoRemove;
                                    userinfo objratingBy = context.UserInfoes.Where(x => x.ID == item.RatingByID).ToList().FirstOrDefault();
                                    if (objratingBy != null)
                                    {
                                        objInspectorRatingPocoPoco.ratingBy = objratingBy.FirstName + " " + objratingBy.LastName + " From " + objratingBy.City + "," + objratingBy.State + " On " + item.CreatedDate.Value.ToShortDateString(); ;
                                        ListofRatingDetails.Add(objInspectorRatingPocoPoco);
                                    }
                                }
                            }
                            else
                            {
                                ////string InpectorName = context.UserInfoes.Where(x => x.ZipCode == zipcode && x.Role == "85DA6641-E08A-4192-A").FirstOrDefault().FirstName;
                                ViewBag.Message = "NO RATINGS ASSIGNED YET";
                                objInspectorRatingPocoPoco.Inspector = objInspectorDetail;
                                ListofRatingDetails.Add(objInspectorRatingPocoPoco);
                            }
                        }
                        else
                        {
                            ViewBag.Message = "NO INSPECTOR IS EXIST ON THIS ZIPCODE";
                        }
                    }
                    else
                    {
                        ViewBag.Message = "NO INSPECTOR IS EXIST ON THIS ZIPCODE";
                    }
                    return View(ListofRatingDetails);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region CMS Pages

        public ActionResult About()
        {
            try
            {
                var AboutUs = from k in context.EmailTemplates
                              where k.TemplateId == 24
                              select k.TemplateDiscription;

                ViewBag.Message = AboutUs.FirstOrDefault();

                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult AboutWhatWeDo()
        {
            try
            {
                var AboutUs = from k in context.EmailTemplates
                              where k.TemplateId == 32
                              select k.TemplateDiscription;

                ViewBag.Message = AboutUs.FirstOrDefault();

                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult AboutOurStory()
        {
            try
            {
                var AboutUs = from k in context.EmailTemplates
                              where k.TemplateId == 34
                              select k.TemplateDiscription;

                ViewBag.Message = AboutUs.FirstOrDefault();

                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult AboutBenefits()
        {
            try
            {
                var AboutUs = from k in context.EmailTemplates
                              where k.TemplateId == 33
                              select k.TemplateDiscription;

                ViewBag.Message = AboutUs.FirstOrDefault();

                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult AboutDifference()
        {
            try
            {
                var AboutUs = from k in context.EmailTemplates
                              where k.TemplateId == 35
                              select k.TemplateDiscription;

                ViewBag.Message = AboutUs.FirstOrDefault();

                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Contact()
        {
            try
            {
                var ContactUs = from k in context.EmailTemplates
                                where k.TemplateId == 25
                                select k.TemplateDiscription;

                ViewBag.Message = ContactUs.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }


        public ActionResult PrivacyPolicy()
        {
            try
            {
                var PrivacyPolicy = from k in context.EmailTemplates
                                    where k.TemplateId == 27
                                    select k.TemplateDiscription;

                ViewBag.Message = PrivacyPolicy.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult PrivacyPolicyJSON()
        {
            try
            {
                var PrivacyPolicy = from k in context.EmailTemplates
                                    where k.TemplateId == 27
                                    select k.TemplateDiscription;

                

                _messageToShow.Message = PrivacyPolicy.FirstOrDefault(); 
                return Json(_messageToShow);
            }
            catch (Exception e)
            {
                _messageToShow.Message = e.InnerException.Message;
                return Json(_messageToShow);
            }
        }

        public JsonResult TermsAndConditionsJson()
        {
            try
            {
                var TermsAndConditions = from k in context.EmailTemplates
                                         where k.TemplateId == 26
                                         select k.TemplateDiscription;



                _messageToShow.Message = TermsAndConditions.FirstOrDefault();
                return Json(_messageToShow);
            }
            catch (Exception e)
            {
                _messageToShow.Message = e.InnerException.Message;
                return Json(_messageToShow);
            }
        }


        public JsonResult HomeInspectionUserAgreementJson()
        {
            try
            {
                var TermsAndConditions = from k in context.EmailTemplates
                                         where k.TemplateId == 55
                                         select k.TemplateDiscription;
                _messageToShow.Message = TermsAndConditions.FirstOrDefault();
                return Json(_messageToShow);
            }
            catch (Exception e)
            {
                _messageToShow.Message = e.InnerException.Message;
                return Json(_messageToShow);
            }
        }

        public ActionResult TermsAndConditions()
        {
            try
            {
                var TermsAndConditions = from k in context.EmailTemplates
                                         where k.TemplateId == 26
                                         select k.TemplateDiscription;

                ViewBag.Message = TermsAndConditions.FirstOrDefault();

                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult Philosophy()
        {
            try
            {
                var Philosophy = from k in context.EmailTemplates
                                 where k.TemplateId == 28
                                 select k.TemplateDiscription;

                ViewBag.Message = Philosophy.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult CodeofEthics()
        {
            try
            {
                var CodeofEthics = from k in context.EmailTemplates
                                   where k.TemplateId == 29
                                   select k.TemplateDiscription;

                ViewBag.Message = CodeofEthics.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult StandardofPractice()
        {
            try
            {
                string Msg = string.Empty;
                var StandardofPractice = from k in context.EmailTemplates
                                         where k.TemplateId == 30
                                         select k.TemplateDiscription;

                ViewBag.Message = StandardofPractice.FirstOrDefault();
                Msg = ViewBag.Message;
                Msg = Msg.Replace(@"<blockquote style=""margin: 0 0 0 40px; border: none; padding: 0px;>""", "");
                Msg = Msg.Replace("</blockquote>", "");
                ViewBag.Message = Msg;
                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        public ActionResult HomePartial()
        {
            try
            {
                tblsamplereport objSample = context.tblsamplereport.FirstOrDefault();
                if (objSample != null)
                {
                    Session["ReportNo"] = objSample.ReportID;
                    Session["UniqueId"] = objSample.ReportNo;
                }
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult HomeBuyerPartial()
        {
            //List<ShareExprience> objExp = null;
            LinksAndShareExeperience objExp = new LinksAndShareExeperience();
            objExp.objlstShareExprience = (from exp in context.tblTestimonial
                                           join user in context.UserInfoes on exp.UserID equals user.ID
                                           join s in context.tblstate on exp.StateId equals s.StateId
                                           where exp.IsDeleted == false && exp.IsActive == true && exp.Role == 2
                                           select new ShareExprience
                                           {
                                               ID = exp.ID,
                                               UserId = exp.UserID,
                                               FirstName = exp.FirstName,
                                               LastName = exp.LastName,
                                               State = s.StateName,
                                               Comments = exp.Comments,
                                               IsActive = exp.IsActive,
                                               ReportNo = exp.ReportNo
                                           }).ToList();

            objExp.objApplicationLinks = context.applicationlink.Where(x => x.PageName == "Buyer" && x.Type == 2).ToList();

            return View(objExp);


        }
        public ActionResult HomeSellerPartial()
        {
            LinksAndShareExeperience objExp = new LinksAndShareExeperience();
            objExp.objlstShareExprience = (from exp in context.tblTestimonial
                                           join user in context.UserInfoes on exp.UserID equals user.ID
                                           join s in context.tblstate on exp.StateId equals s.StateId
                                           where exp.IsDeleted == false && exp.IsActive == true && exp.Role == 2
                                           select new ShareExprience
                                           {
                                               ID = exp.ID,
                                               UserId = exp.UserID,
                                               FirstName = exp.FirstName,
                                               LastName = exp.LastName,
                                               State = s.StateName,
                                               Comments = exp.Comments,
                                               IsActive = exp.IsActive,
                                               ReportNo = exp.ReportNo
                                           }).ToList();

            objExp.objApplicationLinks = context.applicationlink.Where(x => x.PageName == "Seller" && x.Type == 2).ToList();

            return View(objExp);

        }
        public ActionResult HomeAgentsPartial()
        {

            #region Bind States
            List<NState> objState = new List<NState>();
            objState = (from state in context.tblstate
                        where state.IsActive == true
                        select new NState
                        {
                            StateId = state.StateId,
                            StateName = state.StateName
                        }).OrderBy(x => x.StateName).ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            List<SelectListItem> objCityList = new List<SelectListItem>();
            List<SelectListItem> objZipCodeList = new List<SelectListItem>();

            SelectListItem objSelectListItem = new SelectListItem();
            objSelectListItem.Text = "State";
            objSelectListItem.Value = "0";
            //  items.Add(objSelectListItem);
            objCityList.Add(objSelectListItem);
            objZipCodeList.Add(objSelectListItem);
            foreach (var t in objState)
            {
                SelectListItem s = new SelectListItem();
                s.Text = t.StateName.ToString();
                s.Value = t.StateId.ToString();
                items.Add(s);
            }
            items = items.OrderBy(x => x.Text).ToList();
            items.Insert(0, objSelectListItem);
            ViewBag.StateList = items;

            #endregion

            //List<ShareExprience> objExp = null;
            //objExp = (from exp in context.tblTestimonial
            //          //  join user in context.UserInfoes on exp.UserID equals user.ID
            //          join s in context.tblstate on exp.StateId equals s.StateId
            //          where exp.IsDeleted == false && exp.IsActive == true && exp.Role == 3
            //          select new ShareExprience
            //          {
            //              ID = exp.ID,
            //              //    UserId = exp.UserID,
            //              FirstName = exp.FirstName,
            //              LastName = exp.LastName,
            //              State = s.StateName,
            //              Comments = exp.Comments,
            //              IsActive = exp.IsActive,
            //              ReportNo = exp.ReportNo

            //          }).ToList();

            //return View(objExp);
            LinksAndShareExeperience objExp = new LinksAndShareExeperience();
            objExp.objlstShareExprience = (from exp in context.tblTestimonial
                                           join user in context.UserInfoes on exp.UserID equals user.ID
                                           join s in context.tblstate on exp.StateId equals s.StateId
                                           where exp.IsDeleted == false && exp.IsActive == true && exp.Role == 2
                                           select new ShareExprience
                                           {
                                               ID = exp.ID,
                                               UserId = exp.UserID,
                                               FirstName = exp.FirstName,
                                               LastName = exp.LastName,
                                               State = s.StateName,
                                               Comments = exp.Comments,
                                               IsActive = exp.IsActive,
                                               ReportNo = exp.ReportNo
                                           }).ToList();

            objExp.objApplicationLinks = context.applicationlink.Where(x => x.PageName == "Agent" && x.Type == 2).ToList();
            return View(objExp);
        }
        #region GetInspectorDetails_AboutUs
        public ActionResult HomeAboutUsPartial()
        {
            try
            {
                List<InspectorProfilePoco> objlst_Inspectordetails = (from k in context.UserInfoes
                                                                      join j in context.InspectorDetail on k.ID equals j.UserID
                                                                      join s in context.tblstate on k.StateId equals s.StateId
                                                                      join c in context.tblcity on k.CityId equals c.CityId

                                                                      where k.Role == "85DA6641-E08A-4192-A" && j.IsShowOnHomePage == true && k.IsDeleted == false && k.IsActive == true
                                                                      select new InspectorProfilePoco
                                                                      {
                                                                          FirstName = k.FirstName + " " + k.LastName,
                                                                          City = c.CityName,
                                                                          State = s.StateName,
                                                                          /*Below property is used to photoURL*/
                                                                          LastName = j.PhotoURL,
                                                                          ID = k.ID
                                                                      }).ToList();

                List<Owners> objowner = (from owner in context.tblowner
                                         select new Owners
                                         {
                                             ID = owner.ID,
                                             Name = owner.Name,
                                             Designation = owner.Designation,
                                             Image = owner.Image
                                         }).ToList();

                ViewBag.Owner = objowner;
                return View(objlst_Inspectordetails.OrderBy(x => Guid.NewGuid()).Take(10).ToList());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult InnerHomeAboutUsPartial()
        {
            try
            {
                List<InspectorProfilePoco> objlst_Inspectordetails = (from k in context.UserInfoes
                                                                      join j in context.InspectorDetail on k.ID equals j.UserID
                                                                      where k.Role == "85DA6641-E08A-4192-A"
                                                                      select new InspectorProfilePoco
                                                                      {
                                                                          FirstName = k.FirstName + " " + k.LastName,
                                                                          City = k.City,
                                                                          State = k.State,
                                                                          /*Below property is used to photoURL*/
                                                                          LastName = j.PhotoURL,
                                                                      }).ToList();

                return View(objlst_Inspectordetails.OrderBy(x => Guid.NewGuid()).Take(10).ToList());
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion
        public ActionResult HomeJoinusPartial()
        {
            LinksAndShareExeperience objExp = new LinksAndShareExeperience();
            objExp.objApplicationLinks = context.applicationlink.Where(x => x.PageName == "Joinus" && x.Type == 2).ToList();
            return View(objExp);
        }

        public ActionResult InnerHomeJoinusPartial()
        {
            return View();
        }


        public ActionResult InnerHomeBuyerPartial()
        {
            return View();
        }
        public ActionResult InnerHomeSellerPartial()
        {
            return View();
        }
        public ActionResult InnerHomeAgentsPartial()
        {

            #region Bind States
            List<NState> objState = new List<NState>();
            objState = (from state in context.tblstate
                        where state.IsActive == true
                        select new NState
                        {
                            StateId = state.StateId,
                            StateName = state.StateName
                        }).OrderBy(x => x.StateName).ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            List<SelectListItem> objCityList = new List<SelectListItem>();
            List<SelectListItem> objZipCodeList = new List<SelectListItem>();

            SelectListItem objSelectListItem = new SelectListItem();
            objSelectListItem.Text = "State";
            objSelectListItem.Value = "0";
            //  items.Add(objSelectListItem);
            objCityList.Add(objSelectListItem);
            objZipCodeList.Add(objSelectListItem);
            foreach (var t in objState)
            {
                SelectListItem s = new SelectListItem();
                s.Text = t.StateName.ToString();
                s.Value = t.StateId.ToString();
                items.Add(s);
            }
            items = items.OrderBy(x => x.Text).ToList();
            items.Insert(0, objSelectListItem);
            ViewBag.StateList = items;

            #endregion

            List<ShareExprience> objExp = null;
            objExp = (from exp in context.tblTestimonial
                      //  join user in context.UserInfoes on exp.UserID equals user.ID
                      join s in context.tblstate on exp.StateId equals s.StateId
                      where exp.IsDeleted == false && exp.IsActive == true && exp.Role == 3
                      select new ShareExprience
                      {
                          ID = exp.ID,
                          //  UserId = exp.UserID,
                          FirstName = exp.FirstName,
                          LastName = exp.LastName,
                          State = s.StateName,
                          Comments = exp.Comments,
                          IsActive = exp.IsActive,
                          ReportNo = exp.ReportNo

                      }).ToList();

            return View(objExp);

        }

        public ActionResult InnerPhilosophy()
        {
            try
            {
                var Philosophy = from k in context.EmailTemplates
                                 where k.TemplateId == 28
                                 select k.TemplateDiscription;

                ViewBag.Message = Philosophy.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult InnerContact()
        {
            try
            {
                var ContactUs = from k in context.EmailTemplates
                                where k.TemplateId == 25
                                select k.TemplateDiscription;

                ViewBag.Message = ContactUs.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public ActionResult InnerTermsAndConditions()
        {
            try
            {
                var TermsAndConditions = from k in context.EmailTemplates
                                         where k.TemplateId == 26
                                         select k.TemplateDiscription;

                ViewBag.Message = TermsAndConditions.FirstOrDefault();

                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult InnerPrivacyPolicy()
        {
            try
            {
                var PrivacyPolicy = from k in context.EmailTemplates
                                    where k.TemplateId == 27
                                    select k.TemplateDiscription;

                ViewBag.Message = PrivacyPolicy.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult InnerCodeofEthics()
        {
            try
            {
                var CodeofEthics = from k in context.EmailTemplates
                                   where k.TemplateId == 29
                                   select k.TemplateDiscription;

                ViewBag.Message = CodeofEthics.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult InnerStandardofPractice()
        {
            try
            {
                var StandardofPractice = from k in context.EmailTemplates
                                         where k.TemplateId == 30
                                         select k.TemplateDiscription;

                ViewBag.Message = StandardofPractice.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }


        public ActionResult OurInspectorView()
        {
            try
            {
                Int32 NoOfImage = 7;
                var AboutUs = from k in context.EmailTemplates
                              where k.TemplateId == 36
                              select k.TemplateDiscription;
                ViewBag.Message = AboutUs.FirstOrDefault();

                List<OurInspector> ListobjOurInspector = new List<OurInspector>();
                OurInspector objOurInspector = new OurInspector();

                if (Session["CurrentInspectorCount"] == "" || Session["CurrentInspectorCount"] == null)
                {
                    Session["CurrentInspectorCount"] = "0";
                }
                Int32 CurrentInspectorCount = Convert.ToInt32(Session["CurrentInspectorCount"]);

                ListobjOurInspector = (from io in context.InspectorDetail
                                       join ui in context.UserInfoes
                                           on io.UserID equals ui.ID
                                       where io.PhotoURL != "" && io.PhotoURL != null
                                       select new OurInspector
                                       {
                                           FirstName = ui.FirstName,
                                           LastName = ui.LastName,
                                           PhotoURL = io.PhotoURL,
                                           InspectorDetailsID = io.InspectorDetailsID,
                                           CreatedDate = io.CreatedDate
                                       }).OrderBy(x => x.CreatedDate).Take(NoOfImage).ToList();
                Session["CurrentInspectorCount"] = ListobjOurInspector.Count();
                Session["ImageCount"] = ListobjOurInspector.Count();
                objOurInspector.CurrentCount = ListobjOurInspector.Count();
                var counter = from io in context.InspectorDetail
                              join ui in context.UserInfoes on io.UserID equals ui.ID
                              where io.PhotoURL != "" && io.PhotoURL != null
                              select io.PaymentPercentage;
                objOurInspector.TotalCount = counter.Count();

                objOurInspector.ListOurInspector = ListobjOurInspector;
                return View(objOurInspector);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public JsonResult GetInspectorDetail(string InspectorDetailsID)
        {
            try
            {
                List<OurInspector> ListobjOurInspector = new List<OurInspector>();
                OurInspector objOurInspector = new OurInspector();
                objOurInspector = (from io in context.InspectorDetail
                                   join ui in context.UserInfoes
                                       on io.UserID equals ui.ID
                                   where io.InspectorDetailsID == InspectorDetailsID
                                   select new OurInspector
                                   {
                                       FirstName = ui.FirstName,
                                       LastName = ui.LastName,
                                       PhotoURL = io.PhotoURL,
                                       InspectorDetailsID = io.InspectorDetailsID,
                                       Address = ui.Address,
                                       Address2 = ui.AddressLine2,
                                       EmailAddress = ui.EmailAddress,
                                       ExperienceTraining1 = io.ExperienceTraining1,
                                       ExperienceTraining2 = io.ExperienceTraining2,
                                       ExperienceTraining3 = io.ExperienceTraining3,
                                       Certification1 = io.Certification1,
                                       Certification2 = io.Certification2,
                                       City = ui.City,
                                       Country = ui.Country,
                                       Zipcode = ui.ZipCode,
                                       State = ui.State,
                                       StreetAddress = ui.StreetAddress
                                   }).FirstOrDefault();

                //var StandardofPractice = from k in context.InsectionOrderDetails
                //                         where k.TemplateId == 30
                //                         select k.TemplateDiscription;
                //ViewBag.Message = StandardofPractice.FirstOrDefault(); ;

                _messageToShow.Result = RenderRazorViewToString("_PartialInspectorDetail", objOurInspector);
                return ReturnJson(_messageToShow);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public JsonResult GetInspectorImages(String status)
        {
            try
            {
                Int32 NoOfImage = 7;
                List<OurInspector> ListobjOurInspector = new List<OurInspector>();
                OurInspector objOurInspector = new OurInspector();

                if (Session["CurrentInspectorCount"] == "" || Session["CurrentInspectorCount"] == null)
                {
                    Session["CurrentInspectorCount"] = "0";
                }
                if (Session["ImageCount"] == "" || Session["ImageCount"] == null)
                {
                    Session["ImageCount"] = "0";
                }
                Int32 ImageCount = Convert.ToInt32(Session["ImageCount"]);
                Int32 CurrentInspectorCount = Convert.ToInt32(Session["CurrentInspectorCount"]);
                if (status == "previous")
                {
                    ListobjOurInspector = (from io in context.InspectorDetail
                                           join ui in context.UserInfoes
                                               on io.UserID equals ui.ID
                                           where io.PhotoURL != "" && io.PhotoURL != null
                                           select new OurInspector
                                           {
                                               FirstName = ui.FirstName,
                                               LastName = ui.LastName,
                                               PhotoURL = io.PhotoURL,
                                               InspectorDetailsID = io.InspectorDetailsID,
                                               CreatedDate = io.CreatedDate
                                           }).OrderBy(x => x.CreatedDate).Skip(CurrentInspectorCount - (ImageCount + NoOfImage)).Take(NoOfImage).ToList();
                    Session["CurrentInspectorCount"] = CurrentInspectorCount - ImageCount;
                    Session["ImageCount"] = ListobjOurInspector.Count();
                    objOurInspector.CurrentCount = CurrentInspectorCount - ImageCount;
                    var counter = from io in context.InspectorDetail
                                  join ui in context.UserInfoes on io.UserID equals ui.ID
                                  where io.PhotoURL != "" && io.PhotoURL != null
                                  select io.PaymentPercentage;
                    objOurInspector.TotalCount = counter.Count();

                }
                else if (status == "next")
                {
                    ListobjOurInspector = (from io in context.InspectorDetail
                                           join ui in context.UserInfoes
                                               on io.UserID equals ui.ID
                                           where io.PhotoURL != "" && io.PhotoURL != null
                                           select new OurInspector
                                           {
                                               FirstName = ui.FirstName,
                                               LastName = ui.LastName,
                                               PhotoURL = io.PhotoURL,
                                               InspectorDetailsID = io.InspectorDetailsID,
                                               CreatedDate = io.CreatedDate
                                           }).OrderBy(x => x.CreatedDate).Skip(CurrentInspectorCount).Take(NoOfImage).ToList();
                    Session["CurrentInspectorCount"] = ListobjOurInspector.Count() + CurrentInspectorCount;
                    Session["ImageCount"] = ListobjOurInspector.Count();
                    objOurInspector.CurrentCount = CurrentInspectorCount + ListobjOurInspector.Count();
                    var counter = from io in context.InspectorDetail
                                  join ui in context.UserInfoes on io.UserID equals ui.ID
                                  where io.PhotoURL != "" && io.PhotoURL != null
                                  select io.PaymentPercentage;
                    objOurInspector.TotalCount = counter.Count();
                }
                else
                {
                    ListobjOurInspector = (from io in context.InspectorDetail
                                           join ui in context.UserInfoes
                                               on io.UserID equals ui.ID
                                           where io.PhotoURL != "" && io.PhotoURL != null
                                           select new OurInspector
                                           {
                                               FirstName = ui.FirstName,
                                               LastName = ui.LastName,
                                               PhotoURL = io.PhotoURL,
                                               InspectorDetailsID = io.InspectorDetailsID,
                                               CreatedDate = io.CreatedDate
                                           }).OrderBy(x => x.CreatedDate).Take(NoOfImage).ToList();
                    Session["CurrentInspectorCount"] = ListobjOurInspector.Count();

                    objOurInspector.CurrentCount = ListobjOurInspector.Count();
                    var counter = from io in context.InspectorDetail
                                  join ui in context.UserInfoes on io.UserID equals ui.ID
                                  where io.PhotoURL != "" && io.PhotoURL != null
                                  select io.PaymentPercentage;
                    objOurInspector.TotalCount = counter.Count();
                }
                objOurInspector.ListOurInspector = ListobjOurInspector;

                _messageToShow.Result = RenderRazorViewToString("_OurInspectorView", objOurInspector);
                return ReturnJson(_messageToShow);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult HomeBuyer()
        {
            return View();
        }
        public ActionResult HomeSeller()
        {
            return View();
        }
        public ActionResult Agents()
        {
            return View();
        }
        public ActionResult RadOnTesting()
        {
            try
            {
                var RadOnTesting = from k in context.EmailTemplates
                                   where k.TemplateId == 9
                                   select k.TemplateDiscription;

                ViewBag.Message = RadOnTesting.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult Mold()
        {
            try
            {
                var RadOnTesting = from k in context.EmailTemplates
                                   where k.TemplateId == 11
                                   select k.TemplateDiscription;

                ViewBag.Message = RadOnTesting.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult MethTesting()
        {
            try
            {
                var RadOnTesting = from k in context.EmailTemplates
                                   where k.TemplateId == 10
                                   select k.TemplateDiscription;

                ViewBag.Message = RadOnTesting.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GasLeak()
        {
            try
            {
                var RadOnTesting = from k in context.EmailTemplates
                                   where k.TemplateId == 41
                                   select k.TemplateDiscription;

                ViewBag.Message = RadOnTesting.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult Foundation()
        {
            try
            {
                var RadOnTesting = from k in context.EmailTemplates
                                   where k.TemplateId == 12
                                   select k.TemplateDiscription;

                ViewBag.Message = RadOnTesting.FirstOrDefault(); ;

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult MoreInfo()
        {
            var RadOnTesting = from k in context.EmailTemplates
                               where k.TemplateId == 49
                               select k.TemplateDiscription;

            ViewBag.Message = RadOnTesting.FirstOrDefault(); ;

            return View();
        }

        public ActionResult MemberShip()
        {
            return View();
        }


        public ActionResult Details()
        {
            return View();
        }


        public ActionResult ApplyInspectorApplication()
        {
            ApplyInspectorApplication objApplyInspectorApplication = new ApplyInspectorApplication();

            List<NState> objState = new List<NState>();
            objState = (from state in context.tblstate
                        where state.IsActive == true
                        select new NState
                        {
                            StateId = state.StateId,
                            StateName = state.StateName
                        }).OrderBy(x => x.StateName).ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            List<SelectListItem> objCityList = new List<SelectListItem>();
            List<SelectListItem> objZipCodeList = new List<SelectListItem>();

            SelectListItem objSelectListItem = new SelectListItem();
            objSelectListItem.Text = "--Select--";
            objSelectListItem.Value = "--Select--";
            objCityList.Add(objSelectListItem);
            objZipCodeList.Add(objSelectListItem);
            foreach (var t in objState)
            {
                SelectListItem s = new SelectListItem();
                s.Text = t.StateName.ToString();
                s.Value = t.StateId.ToString();
                items.Add(s);
            }
            items = items.OrderBy(x => x.Text).ToList();
            items.Insert(0, objSelectListItem);
            ViewBag.StateList = items;
            objApplyInspectorApplication.StatesList = items;
            objApplyInspectorApplication.ZipCodesList = objZipCodeList;
            objApplyInspectorApplication.CityList = objCityList;

            return View(objApplyInspectorApplication);

        }


        ////do nothing on form submit
        //[HttpPost]
        //public ActionResult AddAttachment(AttachmentModel model)
        //{
        //    return Json(new { success = true });
        //}
        //this is where the file gets save in the server
        public ActionResult UploadAttachment(HttpPostedFileBase FileUpload)
        {
            if (Session["FileName"] != null && Session["FileName"].ToString() != string.Empty)
            {
                System.IO.File.Delete(Server.MapPath("~/UploadedResume/") + Session["FileName"].ToString());
                Session["FileName"] = "";
            }
            if (FileUpload != null && FileUpload.ContentLength > 0)
            {
                string fileName = System.Guid.NewGuid().ToString();
                string ext = System.IO.Path.GetExtension(FileUpload.FileName);
                FileUpload.SaveAs(Server.MapPath("~/UploadedResume/") + fileName + ext);
                Session["FileName"] = fileName + ext;
            }
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult ApplyInspectorApplication(ApplyInspectorApplication objApplyInspectorApplication)
        {
            string msg = "";
            try
            {

                //if (file != null && file.ContentLength > 0)
                // {
                var checkEmailID = (from ui in context.UserInfoes where ui.EmailAddress == objApplyInspectorApplication.EmailAddress select ui).FirstOrDefault();
                if (checkEmailID == null)
                {
                    userinfo objuserinfo = new userinfo();
                    objuserinfo.ID = Guid.NewGuid().ToString();
                    objuserinfo.FirstName = objApplyInspectorApplication.FirstName;
                    objuserinfo.LastName = objApplyInspectorApplication.LastName;
                    objuserinfo.Address = objApplyInspectorApplication.Address;
                    objuserinfo.AddressLine2 = "N/A";
                    objuserinfo.StateId = objApplyInspectorApplication.StateId;
                    objuserinfo.StreetAddress = "N/A";
                    objuserinfo.State = "N/A";
                    objuserinfo.City = "N/A";

                    objuserinfo.CityId = objApplyInspectorApplication.CityId;
                    objuserinfo.Country = "United States";
                    objuserinfo.EmailAddress = objApplyInspectorApplication.EmailAddress;
                    objuserinfo.Password = "N/A"; ;

                    //   DateTime? dt = CreateDate(InspectorDetailsModel.DOB);
                    objuserinfo.DateOfBirth = null;
                    objuserinfo.PhoneNo = objApplyInspectorApplication.PhoneNo;
                    objuserinfo.ModifiedDate = DateTime.Now;
                    objuserinfo.ZipCode = 123456;
                    objuserinfo.Role = SpectorMax.Entities.roles.Property_Inspector;
                    objuserinfo.IsActive = true;
                    objuserinfo.CreatedDate = DateTime.Now;
                    objuserinfo.ModifiedDate = DateTime.Now;
                    objuserinfo.IsDeleted = false;
                    context.UserInfoes.Add(objuserinfo);
                    context.SaveChanges();

                    inspectordetail objInspectorDetail = new inspectordetail();
                    objInspectorDetail.InspectorDetailsID = Guid.NewGuid().ToString();
                    objInspectorDetail.UserID = objuserinfo.ID;
                    objInspectorDetail.PhotoURL = "";
                    objInspectorDetail.Certification1 = "";
                    objInspectorDetail.Certification2 = "";
                    objInspectorDetail.Certification3 = "";
                    objInspectorDetail.ExperienceTraining1 = "";
                    objInspectorDetail.ExperienceTraining2 = "";
                    objInspectorDetail.ExperienceTraining3 = "";

                    //if (file != null && file.ContentLength > 0)
                    //{
                    //    string fileName = System.Guid.NewGuid().ToString();
                    //    string ext = System.IO.Path.GetExtension(file.FileName);
                    //    file.SaveAs(Server.MapPath("~/UploadedResume/") + fileName + ext);
                    //    objApplyInspectorApplication.ResumeFileName = fileName + ext;
                    //}

                    if (Session["FileName"] != null && Session["FileName"].ToString() != string.Empty)
                    {
                        objApplyInspectorApplication.ResumeFileName = Session["FileName"].ToString();
                        Session["FileName"] = "";
                    }

                    objInspectorDetail.HowLong = objApplyInspectorApplication.HowLong;
                    objInspectorDetail.FarWillingToTravel = objApplyInspectorApplication.FarWillingToTravel;
                    if (objApplyInspectorApplication.IsCurrentDriverLicence == "No")
                    {
                        objInspectorDetail.IsCurrentDriverLicence = false;
                    }
                    else
                    {
                        objInspectorDetail.IsCurrentDriverLicence = true;
                    }
                    objInspectorDetail.AvailableToStart = Convert.ToDateTime(objApplyInspectorApplication.AvailableToStart);
                    if (objApplyInspectorApplication.ConvictedofCrime == "No")
                    {
                        objInspectorDetail.ConvictedofCrime = false;
                    }
                    else
                    {
                        objInspectorDetail.ConvictedofCrime = true;
                    }
                    objInspectorDetail.ExplainNumberOfConviction = objApplyInspectorApplication.ExplainNumberOfConviction;
                    objInspectorDetail.HighSchool = "High School";
                    objInspectorDetail.HighSchoolLocation = objApplyInspectorApplication.HighSchoolLocation;
                    objInspectorDetail.HighSchoolCompletedYear = objApplyInspectorApplication.HighSchoolCompletedYear;
                    objInspectorDetail.HighSchoolMajorAndDegree = objApplyInspectorApplication.HighSchoolMajorAndDegree;
                    objInspectorDetail.College = "College";
                    objInspectorDetail.CollegeLocation = objApplyInspectorApplication.CollegeLocation;
                    objInspectorDetail.CollegeCompletedYear = objApplyInspectorApplication.CollegeCompletedYear;
                    objInspectorDetail.CollegeMajorAndDegree = objApplyInspectorApplication.CollegeMajorAndDegree;
                    objInspectorDetail.BusAndTradeSchool = "Bus & Trade School";
                    objInspectorDetail.BusAndTradeSchoolLocation = objApplyInspectorApplication.BusAndTradeSchoolLocation;
                    objInspectorDetail.BusAndTradeSchoolCompletedYear = objApplyInspectorApplication.BusAndTradeSchoolCompletedYear;
                    objInspectorDetail.BusAndTradeSchoolMajorAndDegree = objApplyInspectorApplication.BusAndTradeSchoolMajorAndDegree;
                    objInspectorDetail.ProfessionalSchool = "Professional School";
                    objInspectorDetail.ProfessionalSchoolLocation = objApplyInspectorApplication.ProfessionalSchoolLocation;
                    objInspectorDetail.ProfessionalSchoolCompletedYear = objApplyInspectorApplication.ProfessionalSchoolCompletedYear;
                    objInspectorDetail.ProfessionalSchoolMajorAndDegree = objApplyInspectorApplication.ProfessionalSchoolMajorAndDegree;

                    objInspectorDetail.CreatedDate = DateTime.Now;
                    objInspectorDetail.ModifiedDate = DateTime.Now;
                    objInspectorDetail.PaymentCycleEndDate = DateTime.Now;
                    objInspectorDetail.PaymentCycleStartDate = DateTime.Now;
                    objInspectorDetail.PaymentPercentage = 10;
                    context.InspectorDetail.Add(objInspectorDetail);
                    context.SaveChanges();
                    msg = "Inspector Application Submited Successfully!";

                }
                else
                {
                    msg = "Inspector Email Address Already Exists!";

                }
                //}
                //else
                //{
                //    msg = "Inspector Email Address Already Exists!";

                //}
            }
            catch (Exception ex)
            {

            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get City Based on State.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult NGetCity(string State)
        {
            try
            {
                int stateid = Convert.ToInt32(State);
                List<NCity> objNcity = new List<NCity>();
                objNcity = (from k in context.tblcity
                            where k.StateId == stateid
                            select new NCity
                            {
                                CityId = k.CityId,
                                CityName = k.CityName
                            }).OrderBy(x => x.CityName).ToList();

                List<SelectListItem> CityList = new List<SelectListItem>();
                foreach (var t in objNcity)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.CityName.ToString();
                    s.Value = t.CityId.ToString();
                    CityList.Add(s);
                }
                CityList = CityList.OrderBy(x => x.Text).ToList();
                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                CityList.Insert(0, objSelectListItem);
                return Json(CityList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
                return Json(_messageToShow);
            }
        }

        /// <summary>
        /// Get Zip Based on City.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult NGetZipcodes(string City)
        {
            try
            {
                int CityId = Convert.ToInt32(City);
                List<NZip> objNZip = new List<NZip>();
                objNZip = (from k in context.tblzip
                           where k.CityId == CityId
                           select new NZip
                           {
                               ZipId = k.ZipId,
                               Zip = k.Zip
                           }).OrderByDescending(x => x.Zip).ToList();
                List<SelectListItem> ZipList = new List<SelectListItem>();
                foreach (var t in objNZip)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = t.Zip.ToString();
                    s.Value = t.ZipId.ToString();
                    ZipList.Add(s);
                }
                ZipList = ZipList.OrderBy(x => x.Text).ToList();
                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Text = "--Select--";
                objSelectListItem.Value = "--Select--";
                ZipList.Insert(0, objSelectListItem);
                return Json(ZipList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _messageToShow.Message = ex.Message.ToString();
                return Json(_messageToShow);
            }
        }



        #region Seller Buyer Home Links
        public ActionResult BuyerSellerPages()
        {
            return View();
        }

        public ActionResult TheHomeInspectionProcess()
        {

            var Message = from k in context.EmailTemplates
                          where k.TemplateId == 45
                          select k.TemplateDiscription;

            ViewBag.Message = Message.FirstOrDefault(); ;
            return View();
        }

        public ActionResult Whydorealestate()
        {
            var Message = from k in context.EmailTemplates
                          where k.TemplateId == 46
                          select k.TemplateDiscription;

            ViewBag.Message = Message.FirstOrDefault(); ;
            return View();
        }

        public ActionResult WhatisaSpectormaxComprehensive()
        {
            var Message = from k in context.EmailTemplates
                          where k.TemplateId == 43
                          select k.TemplateDiscription;

            ViewBag.Message = Message.FirstOrDefault(); ;
            return View();
        }


        public ActionResult ChoosingaSpectormaxHomeInspector()
        {
            var Message = from k in context.EmailTemplates
                          where k.TemplateId == 47
                          select k.TemplateDiscription;

            ViewBag.Message = Message.FirstOrDefault(); ;
            return View();
        }

        public ActionResult WhatistheSpectormaxDifference()
        {
            var Message = from k in context.EmailTemplates
                          where k.TemplateId == 50
                          select k.TemplateDiscription;

            ViewBag.Message = Message.FirstOrDefault(); ;
            return View();
        }

        public ActionResult SpectorMaxwillInspectanyHomeforFREEif()
        {
            var Message = from k in context.EmailTemplates
                          where k.TemplateId == 48
                          select k.TemplateDiscription;

            ViewBag.Message = Message.FirstOrDefault(); ;
            return View();
        }

        public ActionResult TheBestHomeInspectorsWorkasaNeutralThirdParty()
        {
            var Message = from k in context.EmailTemplates
                          where k.TemplateId == 44
                          select k.TemplateDiscription;

            ViewBag.Message = Message.FirstOrDefault(); ;
            return View();
        }

        public ActionResult AdditionalTesting()
        {
            return View();
        }
        #endregion
    }
}
