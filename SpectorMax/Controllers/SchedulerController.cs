﻿using SpectorMax.Entities.Classes;
using SpectorMax.Models;
using SpectorMaxDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SpectorMax.Controllers
{
    public class SchedulerController : Controller
    {
        private SpectorMaxContext context = new SpectorMaxContext();
        MessageToShow _messageToShow = new MessageToShow();
        //
        // GET: /Scheduler/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Events(string start, string end)
        {
            var scheduler = (from sc in context.tblSchedule
                             join io in context.tblinspectionorderdetail
                                 on sc.InspectionOrderId equals io.InspectionorderdetailsID
                             join ins in context.tblinspectionorder on io.InspectionorderdetailsID equals ins.InspectionOrderDetailId
                             where ins.InspectorID == Sessions.LoggedInUser.UserId && sc.IsDeleted == false
                             select sc).ToList();
            var block = (from bl in context.tblBlock where bl.IsDeleted == false select bl).ToList();
            List<SchedulerProperties> rows = new List<SchedulerProperties>();
            foreach (var sc in scheduler)
            {
                SchedulerProperties obj = new SchedulerProperties();
                obj.SchedulerId = sc.ID;
                obj.Comment = sc.Comment;
                obj.StartTime = Convert.ToDateTime(sc.ScheduleDate).ToShortDateString() + " " + sc.StartTime;
                obj.EndTime = Convert.ToDateTime(sc.ScheduleDate).ToShortDateString() + " " + sc.EndTime;
                rows.Add(obj);
            }
            foreach (var bl in block)
            {
                SchedulerProperties obj = new SchedulerProperties();
                obj.SchedulerId = bl.ID;
                obj.Comment = bl.Comment;
                obj.EndTime = "Block";
                obj.StartTime = Convert.ToDateTime(bl.BlockDate).ToShortDateString();
                rows.Add(obj);
            }
            return Json(rows, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertOrUpdateAppointment(string scheduleId, string inspectionOrderId, string scheduleDate, string startTime, string endTime, string comment)
        {
            string message = "Success";
            tblschedule objtblSchedule = new tblschedule();
            if (!string.IsNullOrEmpty(scheduleId) && Convert.ToInt32(scheduleId) > 0)
            {
                int id = Convert.ToInt32(scheduleId);
                var scheduler = (from sc in context.tblSchedule where sc.ID == id select sc).FirstOrDefault();
                if (scheduler != null)
                {
                    scheduler.InspectionOrderId = inspectionOrderId;
                    scheduler.ScheduleDate = Convert.ToDateTime(scheduleDate);
                    scheduler.StartTime = startTime;
                    scheduler.EndTime = endTime;
                    scheduler.Comment = comment;
                    scheduler.ModifiedDate = DateTime.Now;

                 
                
                   
                    context.SaveChanges();
                }

            }
            else
            {
                objtblSchedule.InspectionOrderId = inspectionOrderId;
                objtblSchedule.ScheduleDate = Convert.ToDateTime(scheduleDate);
                objtblSchedule.StartTime = startTime;
                objtblSchedule.EndTime = endTime;
                objtblSchedule.Comment = comment;
                objtblSchedule.IsDeleted = false;
                objtblSchedule.CreatedDate = DateTime.Now;
                context.tblSchedule.Add(objtblSchedule);

                #region Sending Email
                EmailHelper email = new EmailHelper();
                var EmailId = from IO in context.tblinspectionorder
                              join user in context.UserInfoes
                              on IO.RequesterID equals user.ID
                              where IO.InspectionOrderDetailId == inspectionOrderId
                              select user;

                if (EmailId != null)
                {
                    string emailBody = context.EmailTemplates.FirstOrDefault(x => x.TemplateName == "ScheduleInspection").TemplateDiscription;
                    emailBody = emailBody.Replace("[Comments]", comment);
                    emailBody = emailBody.Replace("[startTime]", startTime);
                    emailBody = emailBody.Replace("[endTime]", endTime);
                    bool status = email.SendEmail(Sessions.LoggedInUser.EmailAddress, "ScheduleInspectionEmail", "", emailBody, EmailId.FirstOrDefault().EmailAddress);
                    if (status == true)
                        _messageToShow.Message = "Success";
                    else
                        _messageToShow.Message = "Mail Send Failure";
                #endregion
                }

                var order = context.tblinspectionorder.Where(x => x.InspectionOrderDetailId == inspectionOrderId).FirstOrDefault();
                order.IsAcceptedOrRejected = true;
                context.SaveChanges();
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult DeleteAppointment(string scheduleId)
        {
            string message = "Success";
            if (!string.IsNullOrEmpty(scheduleId) && Convert.ToInt32(scheduleId) > 0)
            {
                int id = Convert.ToInt32(scheduleId);
                var scheduler = (from sc in context.tblSchedule where sc.ID == id select sc).FirstOrDefault();
                if (scheduler != null)
                {
                    scheduler.IsDeleted = true;
                    scheduler.ModifiedDate = DateTime.Now;
                    context.SaveChanges();
                }

            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult InsertOrUpdateBlockTime(string blockId, string blockDate, string comment)
        {
            string message = "Success";
            tblblock objtblBlock = new tblblock();
            if (!string.IsNullOrEmpty(blockId) && Convert.ToInt32(blockId) > 0)
            {
                int id = Convert.ToInt32(blockId);
                var block = (from bl in context.tblBlock where bl.ID == id select bl).FirstOrDefault();
                if (block != null)
                {
                    block.BlockDate = Convert.ToDateTime(blockDate);
                    block.Comment = comment;
                    block.ModifiedDate = DateTime.Now;
                    context.SaveChanges();
                }
            }
            else
            {
                objtblBlock.BlockDate = Convert.ToDateTime(blockDate);
                objtblBlock.Comment = comment;
                objtblBlock.IsDeleted = false;
                objtblBlock.Createddate = DateTime.Now;
                context.tblBlock.Add(objtblBlock);
                context.SaveChanges();
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult DeleteBlockTime(string blockId)
        {
            string message = "Success";
            if (!string.IsNullOrEmpty(blockId) && Convert.ToInt32(blockId) > 0)
            {
                int id = Convert.ToInt32(blockId);
                var block = (from bl in context.tblBlock where bl.ID == id select bl).FirstOrDefault();
                if (block != null)
                {
                    block.IsDeleted = true;
                    block.ModifiedDate = DateTime.Now;
                    context.SaveChanges();
                }

            }
            return Json(message);
        }

        public JsonResult GetAppointmentdetails(string scheduleId)
        {
            int id = Convert.ToInt32(scheduleId);
            // var scheduler = (from sc in context.tblSchedule where sc.ID == id select sc).FirstOrDefault();
            var scheduler = (from sc in context.tblSchedule
                             join io in context.tblinspectionorderdetail
                                 on sc.InspectionOrderId equals io.InspectionorderdetailsID
                             join ins in context.tblinspectionorder on io.InspectionorderdetailsID equals ins.InspectionOrderDetailId
                             where sc.ID == id
                             select new { sc, io }).FirstOrDefault();
            string details = "{\"Id\":\"" + scheduler.sc.ID + "\",\"OwnerName\":\"" + scheduler.io.RequesterFName + " " + scheduler.io.RequesterLName + "\",\"InspectionOrderId\":\"" + scheduler.sc.InspectionOrderId + "\",\"ScheduleDate\":\"" + Convert.ToDateTime(scheduler.sc.ScheduleDate).ToString("yyyy-MM-dd") + "\",\"StartTime\":\"" + scheduler.sc.StartTime + "\",\"EndTime\":\"" + scheduler.sc.EndTime.ToString() + "\",\"Comment\":\"" + scheduler.sc.Comment.ToString() + "\"}";
            return Json(details);
        }

        public JsonResult GetBlockTimeDetails(string blockId)
        {
            int id = Convert.ToInt32(blockId);
            var block = (from bl in context.tblBlock where bl.ID == id select bl).FirstOrDefault();
            string details = "{\"Id\":\"" + block.ID + "\",\"BlockDate\":\"" + Convert.ToDateTime(block.BlockDate).ToString("yyyy-MM-dd") + "\",\"Comment\":\"" + block.Comment + "\"}";
            return Json(details);
        }

        public JsonResult CheckBlockTimeByDate(string blockDate)
        {
            string IsBlock = "false";
            DateTime dtBlock = Convert.ToDateTime(blockDate);
            var block = (from bl in context.tblBlock where bl.BlockDate == dtBlock select bl).FirstOrDefault();
            if (block != null)
            {
                IsBlock = "true";
            }
            return Json(IsBlock);
        }
    }

}
