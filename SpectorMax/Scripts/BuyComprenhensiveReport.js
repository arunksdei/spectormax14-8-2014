﻿
    $(document).ready(function () {

     
        $("#btnPayment").click(function () {
            var errors = "";
            if ($("#txtCardNumber").val() != "") {
                if (isNaN($("#txtCardNumber").val())) {
                    errors += "Card Number must be numeric.<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;
                }
                else if( $("#txtCardNumber").val().length<16)
                {
                    errors += "Card Number must be 16 digit number.<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;
                
                }
            }
            else {
                errors += "Card Number must not be empty.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;
            }
            var year=$("#ddlYear").val()
            var minYear=parseInt(new Date().getFullYear(),10)
            var month=parseInt($("#ddlMonth").val(),10)
            var minMonth=new Date().getMonth()+1
            if(!(year > minYear || (year == minYear && month >= minMonth)))
            {
                errors += "Your Credit Card Expiration date is invalid.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;
            }


            

            if ($("#txtCVV").val() == "") {
                errors += "CVV is required.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;
            }
            if ($("#chkTwoMonth").is(":checked")) {
                var Reportid = GetQueryStringParams('ReportId')
                $("#btnPayment").toggleClass('active');
                $.ajax({
                    type: "POST",
                    url: $URL._BuyReport,
                    data: { cardNumber: $("#txtCardNumber").val(), expiryMonth: $('#ddlMonth').val(), expiryYear: $("#ddlYear").val(), securityCode: $("#txtCVV").val(), amount: $("#hdnAmount").val(), ReportID: $("#hdnReportId").val() },
                    dataType: "Json",
                    success: function (result) {
                        $("#divMessage").html('');
                        if (result.Message == "SuccessOneTime") {
                            $(".bs-exampleExp").css("display", "none");
                            $("#SucessExp").css("display", "block");
                            $("#lblSuccess").html("Payment done successfully.Please wait while you are redirecting to comprehensive report.");
                            $("#txtCountry").val('');
                            $("#txtCardNumber").val('');
                            $("#txtNameOnCard").val('');
                            $("#txtCVV").val('');
                            $("#btnPayment").removeClass('active');
                            $(location).attr("href", $URL._ViewComprehensiveReport + "?inspectionOrderId=" + Reportid)

                        }
                        else if (result.Message == "SuccessPaymenPlan") {
                            $(".bs-exampleExp").css("display", "none");
                            $("#SucessExp").css("display", "block");
                            $("#lblSuccess").html("Payment done successfully.");
                            $("#txtCountry").val('');
                            $("#txtCardNumber").val('');
                            $("#txtNameOnCard").val('');
                            $("#txtCVV").val('');
                            $("#btnPayment").removeClass('active');
                            $(location).attr("href", $URL._DashBoard)
                        }

                        else {
                            $("#btnPayment").removeClass('active');
                            $(".bs-exampleExp").css("display", "block");
                            $(".bs-exampleExp").html('<div class="alert alert-danger alert-error">' + result.Message + '</div>');
                        }
                    },
                    error: function () {

                    },
                });
            }
            else {
                errors += "Please select Two Month Membership Agreement.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;
            }
        });

        function GetQueryStringParams(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }



       
    });


