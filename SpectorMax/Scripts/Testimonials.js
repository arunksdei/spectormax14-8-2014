﻿$(document).ready(function () {
    var Expid;
    var commenttd;
    $(".Edit").click(function () {
        $("#SucessExp").hide();
        commenttd = $(this).parents('tr').find('.Comment');
        $(".bs-exampleExp").hide();
        Expid = $(this).attr("data-Id");
        var comment=$(this).parents('tr').find('.Comment').attr("title");
        var isChecked=$(this).parents('tr').find('.Active').find('input').attr("checked");
        $("#chkActive").attr("checked", isChecked);
        $("#txtComments").val(comment.trim());
        $('#myModal2').modal({ backdrop: 'static', keyboard: true })
        $("#myModal2").modal("show");

    });
    $(".Delete").click(function () {
        var span = $(this).find("#lblRequest")
        $(this).addClass('active')
        $(span).html("Please Wait ....")
        var Id = $(this).attr("data-Id");
        var tr = $(this).parents('tr');
        $.ajax({
            type: "POST",
            url: $URL._DeleteTestimonial,
            dataType: "json",
            data: { Id: Id },
            success: function (result) {
                if (result == "Success") {
                    $(tr).remove();
                }
                else if (result == "Session Over")
                {
                    $(location).attr("href", $URL._HomePage);
                }
            },
            error: function (e) {

            },

        });
    });

    $("#btnShareExperience").click(function () {
        if ($("#txtComments").val().trim().length > 0) {
           
            $("#btnShareExperience").addClass('active');
            $("#lblSubmit").html("Please Wait..")
            $.ajax({
                type: "POST",
                url: $URL._EditTestimonial,
                dataType: "json",
                data: { Id: Expid, Comments: $("#txtComments").val(),IsActive:$("#chkActive").attr("checked")},
                success: function (result) {
                    if (result == "Success") {
                        $("#SucessExp").show();
                        $(commenttd).attr("title", $("#txtComments").val())
                        if ($("#txtComments").val().length > 50) {
                            $(commenttd).html($("#txtComments").val().substr(0, 15))
                        }
                        else {
                            $(commenttd).html($("#txtComments").val())
                        }
                        $(".bs-exampleExp").hide();
                        $("#lblSubmit").html("Submit")
                        $("#btnShareExperience").removeClass('active');
                    }
                    else if (result == "Session Over") {
                        $(location).attr("href", $URL._HomePage);
                    }
                    else if (result == "Error")
                    {
                        $("#lblSubmit").html("Submit");
                        $("#btnShareExperience").removeClass('active');
                        $(".bs-exampleExp").show();
                        $("#lblErrorExp").html("There might be some problem. Please try after some time.");
                        $("#SucessExp").hide();
                    }
                },
                error: function (e) {

                },

            });
        }
        else {
            $(".bs-exampleExp").show();
            $("#lblErrorExp").html("Please enter comment.");
            $("#SucessExp").hide();
        }

    });
});