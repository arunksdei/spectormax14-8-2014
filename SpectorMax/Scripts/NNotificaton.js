﻿$(document).ready(function () {

    var FromId;
    $("#btnReply").click(function () {
        FromId = $(this).attr("data-to");
        $('#ReplyMessage').modal({ backdrop: 'static', keyboard: true })
        $("#ReplyMessage").modal("show");
    })
    $("#btnSendReply").click(function () {

        $("#btnSendReply").addClass('active');
        $.ajax({
            type: "get",
            url: $URL._MessageReply,
            async: false,
            data: { Message: $("#txtMessageReply").val(), To: FromId, MessageId: $("#hdnMessageId").val(), Subject: $("#hdnSubject").val() },
            //dataType: "Json",

            success: function (result) {
                // alert(result);
                if (result == "Success")
                {
                    $("#Sucess").css("display", "block");
                    $("#btnSendReply").removeClass('active');
                }
                else if (result == "Error")
                {
                    $(".bs-example").css("display", "block");
                    $("#lblError").html("There might be some problem. Please try after some time");
                    $("#btnSendReply").removeClass('active');
                }
                else if (result == "Session Over") {
                    $(location).attr("href", $URL._HomePage);
                }
                else {
                    $(".bs-example").css("display", "block");
                    $("#lblError").html("There might be some problem. Please try after some time");
                    $("#btnSendReply").removeClass('active');
                }
            },
            error: function (e) {
                // alert(e);
            },
        });

        
    });
    $("#txtZipCode").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    showLoader();
    $.ajax({
        type: "get",
        url: $URL._NotificationList,
        async: false,
        data: {},
        //dataType: "Json",

        success: function (result) {
            // alert(result);
            $('#tblNotificationList').html(result);
        },
        error: function (e) {
            // alert(e);
        },
    });

    hideLoader();


    $("#btnDelete").click(function(){
        var marked = ""; var i = 0;
        var n = $('input:checked').length
        $('input:checked').each(function () {
            var value = $(this).attr('value')
            if (i == 0)
            {
                marked = value;
                i = 1;
            }
            else
            {
                marked += ","+value
            }
        });

        // 
        if (marked != "") {
            showLoader();
            $.ajax({
                type: "POST",
                url: $URL._DeleteNotificationMessage,
                async: false,
                data: { UserNotificationIds: marked },
                //dataType: "Json",
                success: function (result) {
                    BindNotification();
                },
                error: function (e) {
                    // alert(e);
                },
            });

            hideLoader();
        }
    });

    $("#txtFirstName").keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#txtFirstName").val().length > 0) {
                SearchInspector();
            }
        }
    });
    $("#txtLastName").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#txtLastName").val().length > 0) {
                SearchInspector();
            }
        }
    });
    $("#txtZipCode").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#txtZipCode").val().length > 0) {
                SearchInspector();
            }
        }
    })
});

function BindNotification()
{
    $.ajax({
        type: "get",
        url: $URL._NotificationList,
        async: false,
        data: {},
        //dataType: "Json",

        success: function (result) {
            // alert(result);
            $('#tblNotificationList').html(result);
        },
        error: function (e) {
            // alert(e);
        },
    });
}

function CompressMessagePopup(toAddress, userId) {
    $('#Sucess').css("display", "none");
    $('#txtToAddress').val('');
    $('#hdfToUserId').val('');
    $('#txtSubject').val('');
    $('#txtMessage').val('');
    $('#txtToAddress').val(toAddress);
    $('#hdfToUserId').val(userId);
    $('#ComposeMessage').modal({ backdrop: 'static', keyboard: true })
    $("#ComposeMessage").modal("show");
    //$(".bs-exampleExp").css("display", "none");
    // $("#SucessExp").css("display", "none");
}

function SaveComposeMessage()
{
    $("#btnInvite").addClass('active');
    $.ajax({
        type: "POST",
        url: $URL._SaveComposeMessage,
        async: false,
        data: {ToUserId:$('#hdfToUserId').val(),Subject:$('#txtSubject').val(),Message:$('#txtMessage').val()},
        dataType: "Json",
        success: function (result) {
            $('#txtSubject').val('');
            $('#txtMessage').val('');
            $('#Sucess').css("display", "block");
            $("#btnInvite").removeClass('active');
        },
        error: function (e) {
            // alert(e);
            $("#btnInvite").removeClass('active');
        },

    });
    $("#btnInvite").removeClass('active');
 
}

function SearchInspector() {
    showLoader();
   
    $.ajax({
        type: "get",
        url: $URL._SearchInspector,
        async: false,
        data: { FirstName: $("#txtFirstName").val(), LastName: $("#txtLastName").val(), ZipCode: $('#txtZipCode').val() },
        //dataType: "Json",

        success: function (result) {
           
              $("#tblFindInspector").html(result);
        },
        error: function (e) {
            // alert(e);
        },
    });

    hideLoader();
}

function ViewNotificationMessage(IsRead, NotiFicationId, ToAddress, Subject, Message) {
   // alert(IsRead); alert(NotiFicationId); alert(ToAddress); alert(Subject); alert(Message);
    if (IsRead == 'False') {
        
        showLoader();
        $.ajax({
            type: "POST",
            url: $URL._UpdateMessageReadStatus,
            async: false,
            data: { UserNotificationId: NotiFicationId },
            //dataType: "Json",

            success: function (result) {
               // alert(result);
                //  $('#tblFindInspector').html(result);
            },
            error: function (e) {
                // alert(e);
            },
        });
        hideLoader();
   }
   $('#lblToAddress').text(ToAddress);
   $('#lblSubject').text(Subject);
   $('#lblMessage').text(Message);
   $('#ViewMessage').modal({ backdrop: 'static', keyboard: true })
   $("#ViewMessage").modal("show");
}



function showLoader() {
    $("#loadingModel").show();
}
function hideLoader() {
    $("#loadingModel").hide();
}


