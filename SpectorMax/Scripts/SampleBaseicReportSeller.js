﻿function showLoader() {
    $("#loadingModel").show();
}
function hideLoader() {
    $("#loadingModel").hide();
}
function DisplayMsg(result) {
   var Reportid= getQueryStringByName("orderid")
    if (result.responseText == "\"SuccessPaymenPlan\"") {
        $(".bs-exampleExp").css("display", "none")
        $("#SucessExp").css("display", "block")
        ClearAll();
        hideLoader();
        $(location).attr("href", $URL._ViewComprehensiveReport + "?inspectionOrderId=" + Reportid)
    }
    else if (result.responseText == "\"SuccessOneTime\"")
    {
       
        $(".bs-exampleExp").css("display", "none")
            $("#SucessExp").css("display", "block")
            ClearAll();
            hideLoader();
        $(location).attr("href", $URL._ViewComprehensiveReport + "?inspectionOrderId=" + Reportid)
    }
    else if (result.responseText == "\"Session Over\"") {
        $(location).attr("href", $URL._HomePage);
    }
    else {
        $("#bs-exampleExp").css("display", "block")
        $("#Sucess").css("display", "none")
        hideLoader();
    }

}
function ClearAll() {
    $('#Registration1').find('input:text, input:password, input:file, select').val('');
}
function getQueryStringByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
$(document).ready(function () {
 //   $("#Comprehensive").css("height", "197px");
 //   $("#Plan").css("height", "181px");
    $("#OrderId").val(getQueryStringByName("orderid"));

    $("#hdnPackage").val("false");
    /*Form Submit*/
   // $("#amount").val($("#OneTimePayment").val());
   // $("#paymenttype").val("OneTimePayment")

   
    $("#btnBuyerComprehensive").click(function () {
        $("#hdnPackage").val("true");
        $("#divTwoMonthMembership").css("display", "none");
        $("#btnBuyerComprehensive").html("Selected");
        $("#btnBuyerPaln").html("Select");
        $("#amount").val($("#OneTimePayment").val());
        $("#paymenttype").val("OneTimePayment");
        $("#Comprehensive").css("height", "215px");
        $("#Plan").css("height", "205px");
        $("#Comprehensive").addClass("grndv")
        $("#Comprehensive").removeClass("bluedv")
        $("#Plan").addClass("bluedv")
        $("#Plan").removeClass("grndv")
    });

    $("#btnBuyerPaln").click(function () {
        $("#hdnPackage").val("true");
        $("#divTwoMonthMembership").css("display", "block");
        $("#btnBuyerComprehensive").html("Select");
        $("#btnBuyerPaln").html("Selected");
        $("#amount").val($("#PurchasePlan").val());
        $("#paymenttype").val("PaymentPlan");
        $("#Comprehensive").css("height", "205px");
        $("#Plan").css("height", "215px");
        $("#Comprehensive").addClass("bluedv")
        $("#Comprehensive").removeClass("grndv")
        $("#Plan").addClass("grndv")
        $("#Plan").removeClass("bluedv")
    });






    $("#Registration1").submit(function () {
        var errors = "";
       
        if ($("#amount").val() == "")
        {
            errors += "Please select package.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");
            return false;
        }

        if ($("#cardNumber").val() != "") {
            if (isNaN($("#cardNumber").val())) {
                errors += "Card Number must be numeric.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;
            }
            else if ($("#cardNumber").val().length < 16) {
                errors += "Card Number must be 16 digit number.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;

            }
        }
        else {
            errors += "Card Number must not be empty.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");
            return false;
        }
        var year = $("#expiryYear").val()
        var minYear = parseInt(new Date().getFullYear(), 10)
        var month = parseInt($("#expiryMonth").val(), 10)
        var minMonth = new Date().getMonth() + 1
        if (!(year > minYear || (year == minYear && month >= minMonth))) {
            errors += "Your Credit Card Expiration date is invalid.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");
            return false;
        }

        if ($("#securityCode").val() == "") {
            errors += "CVV is required.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");

            return false;
        }


        if ($("#paymenttype").val() == "PaymentPlan") {
            if ($('#chkTerms').is(":checked") && $('#chkHomeInspections').is(":checked") && $('#chkTwoMonth').is(":checked")) {

            }
            else {
                if (!$('#chkTerms').is(":checked")) {
                    errors += "Please Select  Terms And Conditions.<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;
                }
                else if (!$('#chkHomeInspections').is(":checked")) {
                    errors += "Please Select Home Inspection User Agreement..<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;

                }
                else if (!$('#chkTwoMonth').is(":checked")) {
                    errors += "Please Select Two Month Membership Agreement..<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;
                }
            }
        }

        else {
            if ($('#chkTerms').is(":checked") && $('#chkHomeInspections').is(":checked")) {

            }
            else {
                if (!$('#chkTerms').is(":checked"))
                {
                    errors += "Please Select  Terms And Conditions.<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;
                }
                else if (!$('#chkHomeInspections').is(":checked"))
                {
                    errors += "Please Select Home Inspection User Agreement..<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;

                }
                
            }
        }
    });




    $("#StateId").change(function () {
        showLoader();
        $.ajax({
            type: "POST",
            url: $URL._NGetCity,
            dataType: "json",
            data: { state: $('#StateId :selected').val() },
            success: function (result) {
                $('#CityId').empty();
                //    $('#City').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#CityId').append($('<option></option>').val(this.Value).html(this.Text));
                });
               
            },
            error: function (e) {
                
            },
            
        });
        hideLoader();
    });

    /*Fetch zip based on city*/
    $("#CityId").change(function () {
        showLoader();
        $.ajax({
            type: "POST",
            url: $URL._NGetZipCode,
            dataType: "json",
            data: { City: $('#CityId :selected').val() },
            success: function (result) {
                $('#ZipCode').empty();

                $(result).each(function (index, item) {
                    $('#ZipCode').append($('<option></option>').val(this.Value).html(this.Text.toString()));
                });
              
            },
            error: function (e) {
               
            },
        });
        hideLoader();
    });
});


