﻿function DisplayMsg(result) {

    if (result.responseText == "\"Success\"") {
        $("#bs-exampleExp").css("display", "none")
        $("#Sucess").css("display", "block")
        $("#btnNext").remove();
        $("#btnNext_Copy").text("Return to dashboard");
        $("#btnNext_Copy").click(function () {
            window.location.href = "../UserInfoes/Dashboard";
        });
        ClearAll();
        hideLoader();
    }
    else if (result.responseText == "\"Select Other Date\"") {
        $("#bs-exampleExp").css("display", "block");
        $("#Error").html("Please select some other date");
        $("#Sucess").css("display", "none");
        hideLoader();
    }
    else if (result.responseText == "\"Session Out\"") {

        $(location).attr("href", $URL._HomePage);
    }
    else {
        $("#bs-exampleExp").css("display", "block")
        $("#Sucess").css("display", "none")

        hideLoader();
    }
}


function ClearAll() {
    $('#CreateNewInspection').find('input:text, input:password, input:file, select').val('');
    $('#AgentSameInfo').prop('checked', false);
    $('#propertySameInfo').prop('checked', false); 
    $("#Seller").prop('checked', true);
}

$(document).ready(function () {
//$("#BuyerAgentOffice").mask('(999) 999-9999');
//$("#SellerAgentOffice").mask('(999) 999-9999');
    $('#PhoneNumber').mask('(999) 999-9999');
    $('#OwnerPhoneNumber').mask('(999) 999-9999');
    $('#BuyerAgentPhoneDay').mask('(999) 999-9999');
    $('#SellerAgentPhoneDay').mask('(999) 999-9999');
    $('.datepicker').datepicker()
   // $("#datepicker").datepicker("dateFormat", 'yy-mm-dd');
   // $('.datepicker').datepicker();
   // $("#format").change(function () {
   // $(".datepicker").datepicker("option", "dateFormat", 'yy-mm-dd');
  //  });
    //$('.mask').mask('999-999-9999');
    //    $('#ScheduleInspectionDate').change(function (event) {
    //        //  $(this).focus();

    //        if ($(this).val() != "") {
    //            var InspectionDateRow = $(this).val();
    //            var InspectionDate = $(this).val().replace(/\-/g, '');
    //            var d = new Date();
    //            var yyyy = d.getFullYear();
    //            var mm = d.getMonth() + 1;
    //            var dd = d.getDate();
    //            var CurrentDate = yyyy + "" + (mm < 10 ? '0' : '') + mm + "" + (dd < 10 ? '0' : '') + dd;
    //            if (CurrentDate > InspectionDate) {
    //                //alert("Schedule Inspection Date should not be less then Current Date")
    //                ShowAlertMessage("Create Inspection", "Schedule Inspection Date should not be less then Current Date");
    //                $(this).val("");
    //            }
    //        }
    //        //  $(".datepicker-dropdown").hide()

    //});
   
    $("#State").change(function () {
        $("#imgCity").show();
        $.ajax({
            type: "POST",
            url: $URL._GetCity,
            dataType: "json",
            data: { state: $('#State :selected').val() },
            success: function (result) {
                $('#City').empty();
                //    $('#City').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#City').append($('<option></option>').val(this.Value).html(this.Value));
                });
                $("#imgCity").hide();
            },
            error: function (e) {
                $("#imgCity").hide();
            },
        });
    });

    /*Fetch zip based on city*/
    $("#City").change(function () {
        $("#imgCity").show();
        $.ajax({
            type: "POST",
            url: $URL._NGetZipCode,
            dataType: "json",
            data: { City: $('#City :selected').val() },
            success: function (result) {
                $('#Zip').empty();
                $('#Zip').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#Zip').append($('<option></option>').val(this.toString()).html(this.toString()));
                });
                $("#imgCity").hide();
            },
            error: function (e) {
                $("#imgCity").hide();
            },
        });
    });

    $("#PropertyAddressState").change(function () {
        showLoader();
        $("#imgCityProperty").show();
        $.ajax({
            type: "POST",
            url: $URL._NGetCity,
            async:false,
            dataType: "json",
            data: { state: $('#PropertyAddressState :selected').val() },
            success: function (result) {
                $('#PropertyAddressZip').empty();
                $('#PropertyAddressCity').empty();
                //  $('#PropertyAddressCity').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#PropertyAddressCity').append($('<option></option>').val(this.Value).html(this.Text));
                });
                $("#imgCityProperty").hide();
            },
            error: function (e) {
                $("#imgCityProperty").hide();
            },
        });
       hideLoader();
    });

    /*Fetch zip based on city*/
    $("#PropertyAddressCity").change(function () {
        showLoader();
        $("#imgCityProperty").show();
        $.ajax({
            type: "POST",
            url: $URL._NGetZipCode,
            dataType: "json",
            async: false,
            data: { City: $('#PropertyAddressCity :selected').val() },
            success: function (result) {
                $('#PropertyAddressZip').empty();
                //    $('#PropertyAddressZip').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#PropertyAddressZip').append($('<option></option>').val(this.Value.toString()).html(this.Text.toString()));
                });
                $("#imgCityProperty").hide();
            },
            error: function (e) {
                $("#imgCityProperty").hide();
            },
        });
      hideLoader();
    });

    $("#BuyerAgentState").change(function () {
        showLoader();
        // $("#imgCityBuyer").show();
        $.ajax({
            type: "POST",
            url: $URL._NGetCity,
            dataType: "json",
            async: false,
            data: { state: $('#BuyerAgentState :selected').val() },
            success: function (result) {
                $('#BuyerAgentCity').empty();
                $('#BuyerAgentZip').empty();
                $(result).each(function (index, item) {
                    $('#BuyerAgentCity').append($('<option></option>').val(this.Value).html(this.Text));
                });
                //    $("#imgCityBuyer").hide();
            },
            error: function (e) {
                //   $("#imgCityBuyer").hide();
            },
        });
        hideLoader();
    });

    /*Fetch zip based on city*/
    $("#BuyerAgentCity").change(function () {
        //  $("#imgCityBuyer").show();
       showLoader();
        $.ajax({
            type: "POST",
            url: $URL._NGetZipCode,
            dataType: "json",
            async: false,
            data: { City: $('#BuyerAgentCity :selected').val() },
            success: function (result) {
                $('#BuyerAgentZip').empty();
                //   $('#buyerAgent_BuyerAgentZip').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#BuyerAgentZip').append($('<option></option>').val(this.Value.toString()).html(this.Text.toString()));
                });
                //   $("#imgCityBuyer").hide();
            },
            error: function (e) {
                //    $("#imgCityBuyer").hide();
            },
        });
        hideLoader();
    });
    $("#SellerAgentState").change(function () {
        //  $("#imgCitySeller").show();
        showLoader();
        $.ajax({
            type: "POST",
            url: $URL._NGetCity,
            dataType: "json",
            async: false,
            data: { state: $('#SellerAgentState :selected').val() },
            success: function (result) {
                $('#SellerAgentCity').empty();
                $('#SellerAgentZip').empty();
                //      $('#sellerAgent_SellerAgentCity').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#SellerAgentCity').append($('<option></option>').val(this.Value).html(this.Text));
                });
                //   $("#imgCitySeller").hide();
            },
            error: function (e) {
                //  $("#imgCitySeller").hide();
            },
        });
        hideLoader();
    });

    /*Fetch zip based on city*/
    $("#SellerAgentCity").change(function () {
        //   $("#imgCitySeller").show();
        showLoader();
        $.ajax({
            type: "POST",
            url: $URL._NGetZipCode,
            dataType: "json",
            async: false,
            data: { City: $('#SellerAgentCity :selected').val() },
            success: function (result) {
                $('#SellerAgentZip').empty();
                //  $('#sellerAgent_SellerAgentZip').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#SellerAgentZip').append($('<option></option>').val(this.Value.toString()).html(this.Text.toString()));
                });
                //      $("#imgCitySeller").hide();
            },
            error: function (e) {
                //    $("#imgCitySeller").hide();
            },
        });
        hideLoader();
    });

    $("#RequestedByState").change(function () {
        $("#imgCityRequester").show();
        showLoader();
        $.ajax({
            type: "POST",
            url: $URL._NGetCity,
            dataType: "json",
            async: false,
            data: { state: $('#RequestedByState :selected').val() },
            success: function (result) {
                $('#RequestedByZip').empty();
                $('#RequestedByCity').empty();
                //      $('#RequestedByCity').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#RequestedByCity').append($('<option></option>').val(this.Value).html(this.Text));
                });
                $("#imgCityRequester").hide();
            },
            error: function (e) {
                $("#imgCityRequester").hide();
            },
        });
        hideLoader();
    });

    /*Fetch zip based on city*/
    $("#RequestedByCity").change(function () {
        showLoader();
        $("#imgCityRequester").show();
        $.ajax({
            type: "POST",
            url: $URL._NGetZipCode,
            dataType: "json",
            async: false,
            data: { City: $('#RequestedByCity :selected').val() },
            success: function (result) {
                $('#RequestedByZip').empty();
                //  $('#RequestedByZip').append($('<option></option>').val("--Select--").html("--Select--"));
                $(result).each(function (index, item) {
                    $('#RequestedByZip').append($('<option></option>').val(this.Value).html(this.Text.toString()));
                });
                $("#imgCityRequester").hide();
            },
            error: function (e) {
                $("#imgCityRequester").hide();
            },
        });
        hideLoader();
    });


    $("#AgentSameInfo").click(function () {
        showLoader();
        if ($("#AgentSameInfo").is(':checked')) {

            $("#SellerAgentFirstName").val($("#BuyerAgentFirstName").val());
            $("#SellerAgentLastName").val($("#BuyerAgentLastName").val());
            $("#SellerAgentOffice").val($("#BuyerAgentOffice").val());
            $("#SellerAgentPhoneDay").val($("#BuyerAgentPhoneDay").val());
            $("#SellerAgentStreetAddress").val($("#BuyerAgentStreetAddress").val());
            $("#SellerAgentEmailAddress").val($("#BuyerAgentEmailAddress").val());
            $("#SellerAgentState").val($("#BuyerAgentState").val());
            $("#SellerAgentStreetAddress").val($('#BuyerAgentStreetAddress').val())
            if ($("#BuyerAgentState>option:selected").text() != "--Select--") {
                GetStates($("#BuyerAgentState").val(), $("#BuyerAgentCity").attr("id"), $("#SellerAgentCity").attr("id"));
            }
            if ($("#BuyerAgentCity>option:selected").text() != "--Select--") {
                GetZipCodes($("#BuyerAgentCity").val(), $("#BuyerAgentZip").attr("id"), $("#SellerAgentZip").attr("id"))
            }
            
        }

        else {
            $("#sellerAgent_SellerAgentFirstName").val('');
            $("#sellerAgent_SellerAgentLastName").val('');
            $("#sellerAgent_SellerAgentOffice").val('');
            $("#sellerAgent_SellerAgentPhoneDay").val('');
            $("#sellerAgent_SellerAgentStreetAddress").val('');
            $("#sellerAgent_SellerAgentEmailAddress").val('');
            $("#sellerAgent_SellerAgentState").val('');
            $("#sellerAgent_SellerAgentCity").val('');
            $('#sellerAgent_SellerAgentZip').val('');
            $("#sellerAgent_SellerAgentStreetAddress").val('')
        }
        hideLoader();
    });
    $('.date').each(function () {

        var id = $(this).attr("id")
        var start = new Date($("#" + id).val()).getDate();
        var end = new Date().getDate();
        if (start > end || start == end) {
            $("#" + id).removeClass("input-validation-error")
        }
        else {

            $("#" + id).addClass("input-validation-error")
        }
    });

});

function GetStates(state, source, destination) {


    $.ajax({
        type: "POST",
        async: "false",
        url: $URL._NGetCity,
        dataType: "json",
        async: false,
        data: { state: state },
        success: function (result) {
            $("#" + destination).empty();
            //   $('#'+destination).append($('<option></option>').val("--Select--").html("--Select--"));
            $(result).each(function (index, item) {
                $("#" + destination).append($('<option></option>').val(this.Value).html(this.Text));
            });

            $("#" + destination).val(($("#" + source).val()))
            //   $("#imgCity").hide();
        },
        error: function (e) {
            //   $("#imgCity").hide();
        },
    });
}

function GetZipCodes(City, source, destination) {

    $.ajax({
        type: "POST",
        async: "false",
        url: $URL._NGetZipCode,
        dataType: "json",
        async: false,
        data: { City: City },
        success: function (result) {
            $('#' + destination).empty();

            $(result).each(function (index, item) {
                $('#' + destination).append($('<option></option>').val(this.Value.toString()).html(this.Text));
            });
            // $("#PropertyAddressZip").val($("#RequestedByZip").val())
            $("#" + destination).val(($("#" + source).val()))
        },
        error: function (e) {
        },
    });



}

function DisplayErroMessage() {

    $(".field-validation-error").closest("li").each(function () {

        $(this).find(".form").live("mouseover", function () {

            $(this).find(".field-validation-error .input-validation-error").attr("style", "display:block !important");
        });
        $(this).find(".form").live("mouseout", function () { $(this).find(".field-validation-error .input-validation-error").attr("style", "display:none !important"); });
    });
}

function GetSameAsAddress() {
    showLoader();
    var isChecked = $("#propertySameInfo").is(':checked');
    if (isChecked) {
        $.ajax({
            type: "POST",
            async: "false",
            url: $URL._GetSameAsAddressOnNCreateNewInspectopnStep2,
            dataType: "json",
            async: false,
            data: {},
            success: function (result) {
                var obj = jQuery.parseJSON(result);
                $('#PropertyAddress').val(obj.Address);
                $('#OwnerPhoneNumber').val(obj.Phone);
                $('#OwnerEmailAddress').val(obj.Email);
                $("#PropertyAddressState").val(obj.State);
                GetStates($("#PropertyAddressState").val(), $("#PropertyAddressCity").attr("id"), $("#PropertyAddressCity").attr("id"));
                $("#PropertyAddressCity").val(obj.City);
                GetZipCodes($("#PropertyAddressCity").val(), $("#PropertyAddressZip").attr("id"), $("#PropertyAddressZip").attr("id"))
                $("#PropertyAddressZip").val(obj.Zip);
            },
            error: function (e) {
            },
        });
    }
    else {
        $('#PropertyAddress').val("");
        $('#OwnerPhoneNumber').val("");
        $('#OwnerEmailAddress').val("");
        $("#PropertyAddressState").val("--Select--");
        $("#PropertyAddressCity").empty();
        $("#PropertyAddressCity").append($('<option></option>').val("--Select--").html("--Select--"));
        $("#PropertyAddressZip").empty();
        $("#PropertyAddressZip").append($('<option></option>').val("--Select--").html("--Select--"));
    }
   hideLoader();
}



function showLoader() {
    $("#loadingModel").show();
}
function hideLoader() {
    $("#loadingModel").hide();
}
