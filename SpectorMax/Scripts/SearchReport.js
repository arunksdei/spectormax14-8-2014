﻿$(document).ready(function () {

    $("#SearchReportNo").dataTable();

    $("#SearchReportNo tr #BasicWithPurchase").click(function () {
        var orderid = $(this).attr("data-orderid");
        $(location).attr("href", $URL._BasicReport + "?orderid=" + orderid)
    });
    $("#SearchReportNo tr #BasicWithoutPurchase").click(function () {
        var orderid = $(this).attr("data-orderid");
        $(location).attr("href", $URL._NFreeBasiReport + "?orderid=" + orderid)
    });
    $("#SearchReportNo tr #Detail1").click(function () {
        var Reportid = $(this).attr("data-orderid");
        //var Reportid = $(this).attr("data-ReportId");
        $(location).attr("href", $URL._NFreeBasiReport + "?orderid=" + Reportid)
    });
    $("#SearchReportNo tr #Detail1View").click(function () {
        var Reportid = $(this).attr("data-orderid");
        //var Reportid = $(this).attr("data-ReportId");
        $(location).attr("href", $URL._ViewComprehensiveReport + "?inspectionOrderId=" + Reportid)
    });
});