﻿$(document).ready(function () {
    $("#btnGenerateQrCode").click(function () {
        $("#btnGenerateQrCode").addClass('active');
        $.ajax({
            type: "POST",
            url: $URL._GetLastQRCodePrinted,
            async: false,
            data: {},
            //dataType: "Json",

            success: function (result) {
                if (result.Message == "Success") {
                    $("#txtFrQr").val(result.Result);
                    $("#txtCFrQr").val(''); 
                    $("#txtCToQr").val('');
                    $("#txtToQr").val();
                    $(".bs-exampleExp").css('display', 'none');
                    $("#btnGenerateQrCode").removeClass('active');
                    $('#divGenerateQRCode').modal({ backdrop: 'static', keyboard: true })
                    $("#divGenerateQRCode").modal("show");
                }
                else if (result.Message == "No Code") {
                    $("#btnGenerateQrCode").removeClass('active');
                    $('#divGenerateQRCode').modal({ backdrop: 'static', keyboard: true })
                    $("#divGenerateQRCode").modal("show");
                    $("#txtCFrQr").val('');
                    $("#txtCToQr").val('');
                    $("#txtToQr").val('');
                }
                else if (result.Message == "Session Over") {
                    $(location).attr("href", $URL._HomePage);
                }
                else {

                }

            },
            error: function (e) {

            },
        });

    })

    $('#txtToQr').keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#txtToQr").val().length > 0) {
                $("#btnFrTo").click();
            }
        }

    });
    $('#txtCFrQr').keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#txtCFrQr").val().length > 0 && $("#txtCToQr").val().length>0) {
                $("#btnCFrTo").click();
            }
        }

    });
    $('#txtCToQr').keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#txtCFrQr").val().length > 0 && $("#txtCToQr").val().length > 0) {
                $("#btnCFrTo").click();
            }
        }

    });
    $("#btnFrTo").click(function () {
        var FromNo = parseInt($("#txtFrQr").val())
        var ToNo = parseInt($("#txtToQr").val())
        var count = 0;
        if (FromNo <= ToNo) {
            $("#hdnReportno").val(true);
            $("#btnFrTo").addClass("active");
            $.ajax({
                type: "POST",
                url: $URL._GetReportNo,
                async: false,
                data: { ReportNoFrom: $("#txtFrQr").val(), ReportNoTo: $("#txtToQr").val() },
                dataType: "Json",
                success: function (result) {
                    if (result.responseText == "\"error\"") {
                        $("#btnFrTo").removeClass("active");
                        $(".bs-exampleExp").css("Display", "none");
                        $("#lblErrorExp").html("There might be some problem. Please try after some time.")
                    }
                    else if (result.responseText == "\"NoCode\"") {
                        $(".bs-exampleExp").css("Display", "none");
                        $("#lblErrorExp").html("Please check qrcode number to print.")
                    }
                    else if (result.responseText == "\"Session Over\"") {
                        $(location).attr("href", $URL._HomePage);
                    }
                    else {
                        $("#btnFrTo").removeClass("active");
                        var Createdhtml = "<table width='100%'>";
                        $.each(result, function (idx, obj) {
                            if (count == 0 || count == 3) {
                                Createdhtml = Createdhtml + "<tr>";
                            }
                            $("#hdnLastNumPrinted").val(obj.QRCodeNo);
                            var ImageString = 'http://chart.apis.google.com/chart?cht=qr&chs=100x100&chl=http://spectormax.com/Report/FreeBasicReport?UniqueId=' + obj.ReportNo;
                            Createdhtml = Createdhtml + "<td>" + "<label><img src='" + ImageString + "' /><label>" + "</td>";
                            Createdhtml = Createdhtml + "<td>" + "<label>Report ID :" + obj.ReportNo + "</label>" + "</td>";
                            count++;
                            if (count == 3) {
                                Createdhtml = Createdhtml + "</tr>";
                                count = 0;
                            }
                        });
                        Createdhtml = Createdhtml + "</table>";
                        $("#QRCode .divDialogElements").html("");
                        $("#QRCode .divDialogElements").html(Createdhtml);
                        $('#QRCode').modal({ backdrop: 'static', keyboard: true })
                        $("#QRCode").modal("show");
                        var nextnum = ToNo + 1
                        $("#txtFrQr").val("00"+nextnum);
                        
                    }
                },
                error: function (e) {
                    $("#btnFrTo").removeClass("active");
                },
            });
        }
        else {
            $(".bs-exampleExp").css("display", "block");
            $("#lblErrorExp").html("Please check the numbers carefully. From cannot be greater than To.")
        }

    });
    $("#btnCFrTo").click(function () {

        var FromNo = parseInt($("#txtCFrQr").val())
        var ToNo = parseInt($("#txtCToQr").val())
        var count = 0;
        if (FromNo <= ToNo) {
            $("#hdnReportno").val(false);
            $("#btnCFrTo").addClass('active');
            $.ajax({
                type: "POST",
                url: $URL._PrintQrCode,
                async: false,
                data: { ReportNoFrom: $("#txtCFrQr").val(), ReportNoTo: $("#txtCToQr").val() },
                //dataType: "Json",

                success: function (result) {
                    if (result.responseText == "\"error\"") {
                        $("#btnCFrTo").removeClass("active");
                        $(".bs-exampleExp").css("Display", "block");
                        $("#lblErrorExp").html("There might be some problem. Please try after some time.")
                    }
                    else if (result.responseText == "\"NoCode\"") {
                        $("#btnCFrTo").removeClass("active");
                        $(".bs-exampleExp").css("Display", "block");
                        $("#lblErrorExp").html("Please check qrcode number to print.")
                    }
                    else if (result.responseText == "\"Session Over\"") {
                        $("#btnCFrTo").removeClass("active");
                        $(location).attr("href", $URL._HomePage);
                    }
                    else {
                        $(".bs-exampleExp").css("Display", "none");
                        $("#btnCFrTo").removeClass("active");
                        var Createdhtml = "<table width='100%'>";
                        $.each(result, function (idx, obj) {
                            if (count == 0 || count == 3) {
                                Createdhtml = Createdhtml + "<tr>";
                            }
                            var ImageString = 'http://chart.apis.google.com/chart?cht=qr&chs=100x100&chl=http://spectormax.com/Report/FreeBasicReport?UniqueId=' + obj.ReportNo;
                            Createdhtml = Createdhtml + "<td>" + "<label><img src='" + ImageString + "' /><label>" + "</td>";
                            Createdhtml = Createdhtml + "<td>" + "<label>Report ID :" + obj.ReportNo + "</label>" + "</td>";
                            count++;
                            if (count == 3) {
                                Createdhtml = Createdhtml + "</tr>";
                                count = 0;
                            }
                        });
                        Createdhtml = Createdhtml + "</table>";
                        $("#QRCode .divDialogElements").html("");
                        $("#QRCode .divDialogElements").html(Createdhtml);
                        $('#QRCode').modal({ backdrop: 'static', keyboard: true })
                        $("#QRCode").modal("show");
                    }

                },
                error: function (e) {

                },
            });
        }
        else {
            $(".bs-exampleExp").css("display", "block");
            $("#lblErrorExp").html("Please check the numbers carefully. From cannot be greater than To.")
        }
    });
 
    $("input[type=text]").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            return false;
        }
    });

});