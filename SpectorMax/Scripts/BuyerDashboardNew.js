﻿$(document).ready(function () {
    /*Close divNoVerificationNumer*/
    $('#btnOk').click(function () {
        $('#divNoVerificationNumer').modal('toggle');
    });

    /*Request verification number   divNoVerificationNumer*/
    $('#btnRequestVerificfationNumber').click(function () {
        var orderid = $(this).attr("data-orderid");
        $(this).addClass('active')
        var span = $(this).find("#lblRequest")
        var button = $(this);
        $(span).html("Sending Mail...")
        $.ajax({
            type: "POST",
            url: $URL._RequestPrivilages,
            async: false,
            data: { OrderID: orderid },
            success: function (result) {
                if (result.Message == "Success") {
                    $(button).removeClass('active')
                    $(span).html("Mail Sent")
                    $("#divNoVerificationNumer #Sucess7").show();
                }
                else if (result.Message == "Mail Not Sent") {
                    $(button).removeClass('active')
                    $(span).html("Error Occured")
                }
                else if (result.Message == "Error") {
                    $(button).removeClass('active')
                    $(span).html("Error Occured")
                }
                else if (result.Message == "Session Over") {
                    $(location).attr("href", $URL._HomePage);
                }
                else {
                }
            },
            error: function (e) {

            },
        });
    });


    function showLoader() {
        $("#loadingModel").css("display", "block");
    }
    function hideLoader() {
        $("#loadingModel").css("display", "none");
    }
    showLoader();
    YourReport();
    BuyerRequestReport();
    YourUpdatableReport();
    YourInvoices();
    $("#tblPreinspectionDetail").dataTable();
    var UpdatableReportRowCount = $('#tblPreinspectionDetail tbody tr td').length;
    if (UpdatableReportRowCount <= 1) {
        $(".UpdatableReport").hide();
    }
    else {

        $(".UpdatableReport").show();
    }
    $("#tblBuyerRequest").dataTable();
    var BuyerRequesRowCount = $('#tblBuyerRequest tbody tr td').length;
    if (BuyerRequesRowCount <= 1) {
        $(".BuyerRequest").hide();
    }
    else {

        $(".BuyerRequest").show();
    }
    $('#exampleRecentViewed').dataTable();
    var YourReportRowCount = $('#exampleRecentViewed tbody tr td').length;
    if (YourReportRowCount <= 1) {
        $(".YourReport").hide();
    }
    else {
        $(".YourReport").show();
    }
    $('#tblInvoiceDetail').dataTable();
    var YourInvoiceRowCount = $('#tblInvoiceDetail tbody tr td').length;
    if (YourInvoiceRowCount <= 1) {
        $(".YourInvoice").hide();
    }
    else {
        $(".YourInvoice").show();
    }

    $(".dataTables_length").hide();
    $(".dataTables_filter").hide();
    $(".dataTables_info").hide();
    $("#exampleRecentViewed").append("<tr><td colspan='7'><a href='#' class='smblue-btn pull-right' id='shareexp'>Share your experience with others</a></td></tr>")
    hideLoader();
    $("#shareexp").click(function () {

        $('#myModal2').modal({ backdrop: 'static', keyboard: true })
        $("#myModal2").modal("show");
        $(".bs-exampleExp").css("display", "none");
        $("#SucessExp").css("display", "none");
    });

    $("#btnSearchReport").unbind().click(function () {
        if ($("#txtReportNo").val().length > 0) {
            $(location).attr("href", $URL._SearchReportByReportNo + "?ReportNo=" + $("#txtReportNo").val());
        }
    })

    $('#txtReportNo').keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#txtReportNo").val().length > 0) {
                $(location).attr("href", $URL._SearchReportByReportNo + "?ReportNo=" + $("#txtReportNo").val());
            }
        }

    });
    $("#btnSearchAddress").unbind().click(function () {
        $(location).attr("href", $URL._SearchReportByAdderss + "?Address=" + $("#txtAddress").val() + "&City=" + $("#txtCity").val() + "&State=" + $("#State option:selected").text() + "&Zip=" + $("#txtZip").val());
    })
    $("#btnInvite").click(function () {
        var sEmail = $('#txtInviteFriend').val();
        if ($.trim(sEmail).length == 0) {
            $("#Sucess").css("display", "none");
            $(".bs-example").css("display", "block");
            $("#lblError").html("Email address not valid")
        }
        if (CheckEmail(sEmail)) {
            $("#btnInvite").toggleClass('active');
            $.ajax({
                type: "POST",
                url: $URL._InviteFriendPost,
                async: false,
                data: { emailTo: $("#txtInviteFriend").val() },
                //dataType: "Json",

                success: function (result) {
                    if (result.Message == "Success") {
                        $(".bs-example").css("display", "none");
                        $("#Sucess").css("display", "block");
                        $("#txtInviteFriend").val('');
                        $("#btnInvite").removeClass('active');
                    }
                    else {
                        $("#Sucess").css("display", "none");
                        $(".bs-example").css("display", "block");
                        $("#lblError").html("There is some problem, please try after sometime")
                        $("#btnInvite").removeClass('active');
                    }
                },
                error: function (e) {

                },
            });

        }
        else {
            $("#Sucess").css("display", "none");
            $(".bs-example").css("display", "block");
            $("#lblError").html("Email address not valid")

        }
    });
    function CheckEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }
    $("#btnShareExperience").click(function () {

        var isValid = true;
        $('#myModal2 input[type="text"]').each(function () {
            if ($.trim($(this).val()) == '') {
                isValid = false;

                $(this).addClass("input-validation-error")
            }
            else {
                $(this).removeClass("input-validation-error")

            }
        });
        $('#myModal2 select').each(function () {
            if ($(this).val() == "0" || $(this).val() == '') {
                isValid = false;
                $(this).addClass("input-validation-error")
            }
            else {
                $(this).removeClass("input-validation-error")
            }
        });

        if (isValid == true) {
            $("#drpState").removeClass("input-validation-error")

            $("#btnShareExperience").toggleClass('active');

            $.ajax({
                type: "POST",
                url: $URL._ShareYourExperience,
                async: false,
                data: { FirstName: $("#txtFirstName").val(), LastName: $("#txtLastName").val(), State: $("#drpState").val(), ReportNo: $("#txtReportNo1").val(), Comment: $("#txtComments").val(), Role: $("#drpRole").val() },
                //dataType: "Json",

                success: function (result) {
                    if (result.Message == "Success") {

                        $(".bs-exampleExp").css("display", "none");
                        $("#SucessExp").css("display", "block");
                        $("#btnShareExperience").removeClass('active');
                        // ClearAll();
                    }
                    else if (result.Message == "Session Over") {
                        $(location).attr("href", $URL._HomePage);
                    }
                    else {
                        $("#SucessExp").css("display", "none");
                        $(".bs-exampleExp").css("display", "block");
                        $("#btnShareExperience").removeClass('active');
                    }

                },
                error: function (e) {

                },
            });
        }
    });
    //$("#tblInvoiceDetail tr #anchorprviousPaymentInvoice").click(function () {
    //    var amount = $(this).parents('tr').find('.amount').html();
    //    var Date = $(this).parents('tr').find('.date').html();

    //    $('#divInvoice').modal({ backdrop: 'static', keyboard: true })

    //    $('#divInvoice .modal-body').html($("#hdnInvoiceTemplate").val());
    //    $("#Amount").html(amount);
    //    $("#PaymentDate").html(Date);
    //    $("#divInvoice").modal("show");
    //})


    $("#btnInviteFriend").click(function () {

        $('#divInviteFriend').modal({ backdrop: 'static', keyboard: true })
        $("#divInviteFriend").modal("show");
        $(".bs-example").css("display", "none");
        $("#Sucess").css("display", "none");
    });
    $("#exampleRecentViewed tr #DetailFreeBAsic").click(function () {

        var orderid = $(this).attr("data-orderid");
        $(location).attr("href", $URL._NFreeBasiReport + "?orderid=" + orderid)
    });
    $("#tblBuyerRequest tr .basic").live("click", function () {

        var orderid = $(this).attr("data-orderid");
        $(location).attr("href", $URL._NFreeBasiReport + "?orderid=" + orderid)

    });
    $("#tblBuyerRequest tr .Comp").live("click", function () {

        var orderid = $(this).attr("data-orderid");
        $(location).attr("href", $URL._ViewComprehensiveReport + "?inspectionOrderId=" + orderid)

    });
    $("#exampleRecentViewed tr #Detail1Buy").click(function () {
        var Reportid = $(this).attr("data-orderid");
        //var Reportid = $(this).attr("data-ReportId");
        $(location).attr("href", $URL._NBuyComprehensive + "?ReportId=" + Reportid)
    });
    $("#exampleRecentViewed tr #Detail1View").click(function () {
        var Reportid = $(this).attr("data-orderid");
        //var Reportid = $(this).attr("data-ReportId");
        $(location).attr("href", $URL._ViewComprehensiveReport + "?inspectionOrderId=" + Reportid)
    });
    $("#exampleRecentViewed tr #DetailPre").click(function () {
        var orderid = $(this).attr("data-orderid");
        $(location).attr("href", $URL._PreClosingCheckListIndex + "?inspectionOrderId=" + orderid);
    });
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^([\w-\.]+)@@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
        return pattern.test(emailAddress);
    };
    var id;
    $("#tblPreinspectionDetail tr #Detail").click(function () {
        id = $(this).attr("data-orderid");
        $.ajax({
            type: "POST",
            url: $URL._GetVerificationNumber,
            async: false,
            data: { ReportId: id },
            success: function (result) {
                if (result.Successfull == true) {
                    if (result.Result != "" && result.Result != null) {
                        if (result.Message == "True") {
                            $(location).attr("href", $URL._ViewDetailReport + "?inspectionOrderId=" + id);
                        }
                        else {
                            $("#txtVerificationNo").val('');
                            $('#divVerification').modal({ backdrop: 'static', keyboard: true })
                            $("#divVerification").modal("show");
                            $("#btnVerificfation").attr("data-id", id);
                        }
                    }
                }
                 
            }
            //url: $URL._VerifyIsUpdatable,
            //async: false,
            //data: { ReportId: id },
            //success: function (result) {
            //    if (result == true) {
            //        $(location).attr("href", $URL._ViewDetailReport + "?inspectionOrderId=" + id);
            //    }
            //    else {
            //        id = $(this).attr("data-orderid");
            //        $("#txtVerificationNo").val('');
            //        //var orderid = $(this).attr("data-orderid");
            //        //$(location).attr("href", $URL._ViewDetailReport + "?inspectionOrderId=" + orderid)
            //        $('#divVerification').modal({ backdrop: 'static', keyboard: true })
            //        $("#divVerification").modal("show");
            //        $(".bs-example").css("display", "none");
            //        $("#SucessV").css("display", "none");
            //    }
            ///},
             
        });
    });
    $("#tblPreinspectionDetail tr #DetailReport").click(function () {
        id = $(this).attr("data-orderid");
        $(location).attr("href", $URL._ViewDetailReport + "?inspectionOrderId=" + id)
    });
    $("#tblPreinspectionDetail tr #PurchaseReport").click(function () {
        var orderid = $(this).attr("data-orderid");
        $.ajax({
            type: "POST",
            url: $URL._GetVerificationNumber,
            async: false,
            data: { ReportId: orderid },
            success: function (result) {
                if (result.Successfull == true) {
                    if (result.Result != "" && result.Result != null) {
                        if (result.Message == "False") {
                            //$("#txtVerificationNo").val('');
                            //$('#divVerification').modal({ backdrop: 'static', keyboard: true })
                            //$("#divVerification").modal("show");
                            //$("#btnVerificfation").attr("data-id", orderid);
                            $(location).attr("href", $URL._BuyUpdatableReport + "?inspectionOrderId=" + orderid)
                        }
                        else {
                            $('#divNoVerificationNumer').modal({ backdrop: 'static', keyboard: true });
                            $("#divNoVerificationNumer").modal("show");
                            $("#btnRequestVerificfationNumber").attr("data-orderid", orderid);
                        }
                    }
                    else {
                        $('#divNoVerificationNumer').modal({ backdrop: 'static', keyboard: true });
                        $("#divNoVerificationNumer").modal("show");
                        $("#btnRequestVerificfationNumber").attr("data-orderid", orderid);
                    }
                }
                 
            }
        });
        //$(location).attr("href", $URL._BuyUpdatableReport + "?inspectionOrderId=" + orderid)
    });
    $("#btnVerificfation").click(function () {
        id = $(this).attr("data-id");
        if (id == undefined) {
            var sPageURL = window.location.href;
            var indexOfLastSlash = sPageURL.lastIndexOf("/");

            if (indexOfLastSlash > 0 && sPageURL.length - 1 != indexOfLastSlash)
                id = sPageURL.substring(indexOfLastSlash + 1);
        }
        if ($("#txtVerificationNo").val().trim().length > 0) {
            $("#btnVerificfation").addClass("active")
            $.ajax({
                type: "POST",
                url: $URL._VerifyReportno,
                async: false,
                data: { Code: $("#txtVerificationNo").val(), ReportId: id },
                success: function (result) {
                    if (result == "Success") {
                        $("#SucessV").show();
                        $(".bs-example").hide();
                        if ($("#isUpdatable").val() == "True") {
                            $(location).attr("href", $URL._ViewDetailReport + "?inspectionOrderId=" + id)
                        }
                        else {
                            $(location).attr("href", $URL._BuyUpdatableReport + "?inspectionOrderId=" + id)
                        }
                        $("#btnVerificfation").removeClass("active")
                    }
                    else {
                        $("#SucessV").css("display", "none");
                        $(".bs-example").css("display", "block");
                        $("#lblErrorV").html("Verification no not matched. Please check verification code.")
                        $("#btnVerificfation").removeClass("active")
                    }
                },
                error: function (e) {

                },
            });
        }
        else {
            $(".bs-example").show();
            $("#lblErrorV").html("Please enter verification code.")
            $("#SucessV").hide();
        }

    });

    $('#txtVerificationNo').keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#txtVerificationNo").val().length > 0) {
                $("#btnVerificfation").click();
            }
        }
    });
    $("#tblPreinspectionDetail tr #RequestUpdatePrivilages").click(function () {
        var orderid = $(this).attr("data-orderid");
        $(this).addClass('active')
        var span = $(this).find("#lblRequest")

        var button = $(this);
        $(span).html("Sending Mail...")
        $.ajax({
            type: "POST",
            url: $URL._RequestPrivilages,
            async: false,
            data: { OrderID: orderid },
            //dataType: "Json",



            success: function (result) {

                if (result.Message == "Success") {

                    $(button).removeClass('active')
                    $(span).html("Mail Sent")
                }
                else if (result.Message == "Mail Not Sent") {

                    $(button).removeClass('active')
                    $(span).html("Error Occured")
                }
                else if (result.Message == "Error") {

                    $(button).removeClass('active')
                    $(span).html("Error Occured")
                }
                else if (result.Message == "Session Over") {
                    $(location).attr("href", $URL._HomePage);
                }
                else {


                }
            },
            error: function (e) {

            },
        });
    });
});

function BuyerRequestReport() {

    $.ajax({
        type: "POST",
        async: false,
        url: $URL._BuyerRequestReport,
        // data: { BuyerID: 101 },
        //dataType: "Json",
        success: function (result) {
            $("#renderBuyerRequest").html("");
            if (result.Message == "Success") {

                $("#renderBuyerRequest").html(result.Result);

            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderBuyerRequest").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });

}


function ViewInspectorDetails(inspectorId) {
    showLoader();
    $.ajax({
        type: "POST",
        async: false,
        url: $URL._ViewInspectorProfile,
        data: { InspectorId: inspectorId },
        success: function (result) {
            $("#renderIndpectorDetails").html("");
            if (result.Message == "Success") {

                $("#renderIndpectorDetails").html(result.Result);
                $('#divViewInspectorDetails').modal({ backdrop: 'static', keyboard: true })
                $("#divViewInspectorDetails").modal("show");
                $(".bs-example").css("display", "none");
                $("#Sucess").css("display", "none");
            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderIndpectorDetails").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });
    hideLoader();

}

function showLoader() {
    $("#loadingModel").show();
}
function hideLoader() {
    $("#loadingModel").hide();
}
function YourReport() {


    $.ajax({
        type: "POST",
        url: $URL._GetResentReportsViewed,
        async: false,
        data: { BuyerID: 101 },
        //dataType: "Json",



        success: function (result) {
            $("#renderRecentViewedReport").html("");
            if (result.Message == "Success") {

                $("#renderRecentViewedReport").html(result.Result);

            }
            else if (result.Message == "Item not Found") {
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'>No item found against given key words. </h3></center>");

            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });
}
function YourInvoices() {

    $.ajax({
        type: "POST",
        async: false,
        url: $URL._YourInvoices,
        data: { BuyerID: 101 },
        //dataType: "Json",



        success: function (result) {
            $("#renderInvoices").html("");
            if (result.Message == "Success") {

                $("#renderInvoices").html(result.Result);

            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });
}


function YourUpdatableReport() {

    $.ajax({
        type: "POST",
        async: false,
        url: $URL._YourUpdatableReport,
        data: { BuyerID: 101 },
        //dataType: "Json",



        success: function (result) {
            $("#renderUpdatableReport").html("");
            if (result.Message == "Success") {

                $("#renderUpdatableReport").html(result.Result);

            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });
}


function ClearAll() {

    $('#myModal2').find('input:text, input:file,textarea').val('');
    $('#myModal2').find('select').val("0");
}