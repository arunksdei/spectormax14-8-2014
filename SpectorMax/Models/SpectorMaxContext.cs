using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SpectorMax.Models
{
    public class SpectorMaxContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<SpectorMax.Models.SpectorMaxContext>());
        public SpectorMaxContext()
            : base("db_spectromaxEntities")
        {
        }

        public DbSet<SpectorMaxDAL.electrialcommansection> ElectrialCommanSection { get; set; }

        public DbSet<SpectorMaxDAL.electricalpanel> ElectricalPanel { get; set; }

        public DbSet<SpectorMaxDAL.electricalroom> ElectricalRoom { get; set; }

        public DbSet<SpectorMaxDAL.electricalservice> ElectricalService { get; set; }

        public DbSet<SpectorMaxDAL.role> Roles { get; set; }

        public DbSet<SpectorMaxDAL.userinfo> UserInfoes { get; set; }

        public DbSet<SpectorMaxDAL.checklist> Checklists { get; set; }

        public DbSet<SpectorMaxDAL.inspectionorder> InspectionOrders { get; set; }

        public DbSet<SpectorMaxDAL.insectionorderdetail> InsectionOrderDetails { get; set; }

        public DbSet<SpectorMaxDAL.agentlibrary> AgentLibraries { get; set; }

        public DbSet<SpectorMaxDAL.payment> Payments { get; set; }

        public DbSet<SpectorMaxDAL.emailtemplate> EmailTemplates { get; set; }

        public DbSet<SpectorMaxDAL.inspectorrating> InspectorRatings { get; set; }
        public DbSet<SpectorMaxDAL.mainsection> MainSections { get; set; }
        public DbSet<SpectorMaxDAL.subsection> SubSections { get; set; }
        public DbSet<SpectorMaxDAL.appliancescommonsection> Appliancescommonsections { get; set; }
        public DbSet<SpectorMaxDAL.photolibrary> PhotoLibraries { get; set; }
        public DbSet<SpectorMaxDAL.appliancesdishwashdisposalrange_section> AppliancesDishwashDisposalRange_sections { get; set; }
        public DbSet<SpectorMaxDAL.appliancesdryerdoorbellsectiion> AppliancesDryerDoorBellSectiions { get; set; }
        public DbSet<SpectorMaxDAL.applianceshoodventothersection> AppliancesHoodVentOtherSections { get; set; }
        public DbSet<SpectorMaxDAL.appliancesovensection> AppliancesOvenSections { get; set; }
        public DbSet<SpectorMaxDAL.groundcommonsection> GroundCommonSections { get; set; }
        public DbSet<SpectorMaxDAL.groundgassection> GroundGassections { get; set; }
        public DbSet<SpectorMaxDAL.groundh2osection> GroundH2OSections { get; set; }
        public DbSet<SpectorMaxDAL.groundsewersection> GroundSewerSections { get; set; }
        public DbSet<SpectorMaxDAL.groundsprinksyssection> GroundSprinkSysSections { get; set; }
        public DbSet<SpectorMaxDAL.hvaccommonsection> HVACCommonSections { get; set; }
        public DbSet<SpectorMaxDAL.hvaccooling> HVACCoolings { get; set; }
        public DbSet<SpectorMaxDAL.hvacductsvent> HVACDuctsVents { get; set; }
        public DbSet<SpectorMaxDAL.hvacequipboileradiantsection> HVACEquipBoileRadiantSections { get; set; }
        public DbSet<SpectorMaxDAL.optionalcommonsection> OptionalCommonSections { get; set; }
        public DbSet<SpectorMaxDAL.optionaltestingsection> OptionalTestingSections { get; set; }
        public DbSet<SpectorMaxDAL.plumbingcommonsection> PlumbingCommonSections { get; set; }
        public DbSet<SpectorMaxDAL.plumbingmainlinekitchenothers_section> PlumbingMainlineKitchenOthers_Sections { get; set; }
        public DbSet<SpectorMaxDAL.plumbingwaterheat> PlumbingWaterheats { get; set; }
        public DbSet<SpectorMaxDAL.structuralcommonsection> StructuralCommonSections { get; set; }

        public DbSet<SpectorMaxDAL.reportviewed> ReportViewed { get; set; }
        public DbSet<SpectorMaxDAL.inspectordetail> InspectorDetail { get; set; }
        public DbSet<SpectorMaxDAL.zipcodeassigned> ZipCodeAssigned { get; set; }

        public DbSet<SpectorMaxDAL.errorlog> ErrLog { get; set; }


        public DbSet<SpectorMaxDAL.notificationtype> NotificationType { get; set; }
        public DbSet<SpectorMaxDAL.notificationdetail> NotificationDetail { get; set; }

        public DbSet<SpectorMaxDAL.permission> Permission { get; set; }
        public DbSet<SpectorMaxDAL.state> State { get; set; }
        public DbSet<SpectorMaxDAL.statesassigned> StatesAssigned { get; set; }

        public DbSet<SpectorMaxDAL.subsectionstatu> SubSectionStatu { get; set; }
        public DbSet<SpectorMaxDAL.inspectionreport> InspectionReport { get; set; }
        public DbSet<SpectorMaxDAL.zipcode> ZipCode { get; set; }
        public DbSet<SpectorMaxDAL.directiondropmaster> DirectionDropMaster { get; set; }
        public DbSet<SpectorMaxDAL.securityquestion> SecurityQuestion { get; set; }
        public DbSet<SpectorMaxDAL.inspectionadditionalinfo> InspectionAdditionalInfo { get; set; }

        public DbSet<SpectorMaxDAL.commentbox> CommentBox { get; set; }
        public DbSet<SpectorMaxDAL.admincommentlibrary> AdminCommentLibrary { get; set; }

        public DbSet<SpectorMaxDAL.tblcity> tblcity { get; set; }
        public DbSet<SpectorMaxDAL.tblstate> tblstate { get; set; }
        public DbSet<SpectorMaxDAL.tblzip> tblzip { get; set; }
        public DbSet<SpectorMaxDAL.tblinspectionorder> tblinspectionorder { get; set; }
        public DbSet<SpectorMaxDAL.tblinspectionorderdetail> tblinspectionorderdetail { get; set; }
        public DbSet<SpectorMaxDAL.tblpropertydescription> tblpropertydescription { get; set; }
        public DbSet<SpectorMaxDAL.tblphotolibrary> tblPhotoLibrary { get; set; }
        public DbSet<SpectorMaxDAL.tbltestimonial> tblTestimonial { get; set; }
        public DbSet<SpectorMaxDAL.usersnotification> UsersNotification { get; set; }
        public DbSet<SpectorMaxDAL.tblqrcode> tblQRCode { get; set; }
        public DbSet<SpectorMaxDAL.tblblock> tblBlock { get; set; }
        public DbSet<SpectorMaxDAL.tblschedule> tblSchedule { get; set; }
        public DbSet<SpectorMaxDAL.tblsamplereport> tblsamplereport { get; set; }
        public DbSet<SpectorMaxDAL.paymentaddionaltesting> paymentaddionaltesting { get; set; }
        public DbSet<SpectorMaxDAL.tblowner> tblowner { get; set; }
        public DbSet<SpectorMaxDAL.applicationlink> applicationlink { get; set; }
        public DbSet<SpectorMaxDAL.tblisincluded> tblisincluded { get; set; }
        public DbSet<SpectorMaxDAL.tblinspectorcomment> tblinspectorcomment { get; set; }
    }
}