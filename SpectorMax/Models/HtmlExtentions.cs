﻿using SpectorMax.Entities.Classes;
using SpectorMax.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

    public static class HtmlExtentions
    {
        
        #region ActivePage
        /// <summary>
        /// this function used for highlight active menu on the top menu.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <returns>classValue</returns>
        public static string ActivePage(this HtmlHelper helper, string controller, string action)
        {
            string classValue = "";

            string currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
            string currentAction = helper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();
            
            if (currentController == controller && currentAction == action)
            {
                classValue = "active"; 
            }
            return classValue;
        }

      
        #endregion
    }

