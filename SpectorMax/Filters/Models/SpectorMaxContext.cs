using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SpectorMax.Models
{
    public class SpectorMaxContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<SpectorMax.Models.SpectorMaxContext>());
        public SpectorMaxContext()
            : base("db_spectromaxEntities")
        {
        }

        public DbSet<SpectorMaxDAL.ElectrialCommanSection> ElectrialCommanSection { get; set; }

        public DbSet<SpectorMaxDAL.ElectricalPanel> ElectricalPanel { get; set; }

        public DbSet<SpectorMaxDAL.ElectricalRoom> ElectricalRoom { get; set; }

        public DbSet<SpectorMaxDAL.ElectricalService> ElectricalService { get; set; }

        public DbSet<SpectorMaxDAL.Role> Roles { get; set; }

        public DbSet<SpectorMaxDAL.UserInfo> UserInfoes { get; set; }

        public DbSet<SpectorMaxDAL.Checklist> Checklists { get; set; }

        public DbSet<SpectorMaxDAL.InspectionOrder> InspectionOrders { get; set; }

        public DbSet<SpectorMaxDAL.InsectionOrderDetail> InsectionOrderDetails { get; set; }

        public DbSet<SpectorMaxDAL.AgentLibrary> AgentLibraries { get; set; }

        public DbSet<SpectorMaxDAL.Payment> Payments { get; set; }

        public DbSet<SpectorMaxDAL.EmailTemplate> EmailTemplates { get; set; }

        public DbSet<SpectorMaxDAL.InspectorRating> InspectorRatings { get; set; }
        public DbSet<SpectorMaxDAL.MainSection> MainSections { get; set; }
        public DbSet<SpectorMaxDAL.SubSection> SubSections { get; set; }
        public DbSet<SpectorMaxDAL.Appliancescommonsection> Appliancescommonsections { get; set; }
        public DbSet<SpectorMaxDAL.PhotoLibrary> PhotoLibraries { get; set; }
        public DbSet<SpectorMaxDAL.AppliancesDishwashDisposalRange_section> AppliancesDishwashDisposalRange_sections { get; set; }
        public DbSet<SpectorMaxDAL.AppliancesDryerDoorBellSectiion> AppliancesDryerDoorBellSectiions { get; set; }
        public DbSet<SpectorMaxDAL.AppliancesHoodVentOtherSection> AppliancesHoodVentOtherSections { get; set; }
        public DbSet<SpectorMaxDAL.AppliancesOvenSection> AppliancesOvenSections { get; set; }
        public DbSet<SpectorMaxDAL.GroundCommonSection> GroundCommonSections { get; set; }
        public DbSet<SpectorMaxDAL.GroundGassection> GroundGassections { get; set; }
        public DbSet<SpectorMaxDAL.GroundH2OSection> GroundH2OSections { get; set; }
        public DbSet<SpectorMaxDAL.GroundSewerSection> GroundSewerSections { get; set; }
        public DbSet<SpectorMaxDAL.GroundSprinkSysSection> GroundSprinkSysSections { get; set; }
        public DbSet<SpectorMaxDAL.HVACCommonSection> HVACCommonSections { get; set; }
        public DbSet<SpectorMaxDAL.HVACCooling> HVACCoolings { get; set; }
        public DbSet<SpectorMaxDAL.HVACDuctsVent> HVACDuctsVents{ get; set; }
        public DbSet<SpectorMaxDAL.HVACEquipBoileRadiantSection> HVACEquipBoileRadiantSections { get; set; }
        public DbSet<SpectorMaxDAL.OptionalCommonSection> OptionalCommonSections { get; set; }
        public DbSet<SpectorMaxDAL.OptionalTestingSection> OptionalTestingSections { get; set; }
        public DbSet<SpectorMaxDAL.PlumbingCommonSection> PlumbingCommonSections { get; set; }
        public DbSet<SpectorMaxDAL.PlumbingMainlineKitchenOthers_Section> PlumbingMainlineKitchenOthers_Sections { get; set; }
        public DbSet<SpectorMaxDAL.PlumbingWaterheat> PlumbingWaterheats { get; set; }
        public DbSet<SpectorMaxDAL.StructuralCommonSection> StructuralCommonSections { get; set; }

        public DbSet<SpectorMaxDAL.ReportViewed> ReportViewed { get; set; }
        public DbSet<SpectorMaxDAL.InspectorDetail> InspectorDetail { get; set; }
        public DbSet<SpectorMaxDAL.ZipCodeAssigned> ZipCodeAssigned { get; set; }

        public DbSet<SpectorMaxDAL.ErrorLog> ErrLog { get; set; }
 

        public DbSet<SpectorMaxDAL.NotificationType> NotificationType { get; set; }
        public DbSet<SpectorMaxDAL.NotificationDetail> NotificationDetail { get; set; }
 
        public DbSet<SpectorMaxDAL.Permission> Permission { get; set; }
        public DbSet<SpectorMaxDAL.State> State { get; set; }
        public DbSet<SpectorMaxDAL.StatesAssigned> StatesAssigned { get; set; }

        public DbSet<SpectorMaxDAL.SubSectionStatu> SubSectionStatu { get; set; }
        public DbSet<SpectorMaxDAL.InspectionReport> InspectionReport { get; set; }
        public DbSet<SpectorMaxDAL.ZipCode> ZipCode { get; set; }
        public DbSet<SpectorMaxDAL.DirectionDropMaster> DirectionDropMaster { get; set; }
        public DbSet<SpectorMaxDAL.SecurityQuestion> SecurityQuestion { get; set; }
        public DbSet<SpectorMaxDAL.InspectionAdditionalInfo> InspectionAdditionalInfo { get; set; }

        public DbSet<SpectorMaxDAL.CommentBox> CommentBox { get; set; }
        public DbSet<SpectorMaxDAL.AdminCommentLibrary> AdminCommentLibrary { get; set; }
        public DbSet<SpectorMaxDAL.tblcity> tblcity { get; set; }
        public DbSet<SpectorMaxDAL.tblstate> tblstate { get; set; }
        public DbSet<SpectorMaxDAL.tblzip> tblzip { get; set; }     
        public DbSet<SpectorMaxDAL.tblinspectionorder> tblinspectionorder { get; set; }
        public DbSet<SpectorMaxDAL.tblinspectionorderdetail> tblinspectionorderdetail { get; set; }
        public DbSet<SpectorMaxDAL.tblpropertydescription> tblpropertydescription { get; set; }
        public DbSet<SpectorMaxDAL.tblPhotoLibrary> tblPhotoLibrary { get; set; }
        public DbSet<SpectorMaxDAL.tblTestimonial> tblTestimonial { get; set; }
    }
}