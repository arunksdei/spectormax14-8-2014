﻿
$(document).ready(function () {


    showLoader();
    YourReport();
    BuyerRequestReport();
    YourUpdatableReport();
    YourInvoices();
    $("#tblPreinspectionDetail").dataTable();
    $("#tblBuyerRequest").dataTable();
    $('#exampleRecentViewed').dataTable();
    $('#tblInvoiceDetail').dataTable();
    $(".dataTables_length").hide();
    $(".dataTables_filter").hide();
    $(".dataTables_info").hide();
    $("#exampleRecentViewed").append("<tr><td colspan='7'><a href='#' class='smblue-btn pull-right' id='shareexp'>Share your experience with others</a><a href='" +$URL._InspectionRating + "' class='rate pull-right'>Rate your Inspector</a></td></tr>")
    hideLoader();
    $("#shareexp").click(function () {

        $('#myModal2').modal({ backdrop: 'static', keyboard: true })
        $("#myModal2").modal("show");
        $(".bs-exampleExp").css("display", "none");
        $("#SucessExp").css("display", "none");
    });
   
    $("#btnSearchReport").unbind().click(function () {
        if ($("#txtReportNo").val().length > 0) {
            $(location).attr("href", $URL._SearchReportByReportNo + "?ReportNo=" + $("#txtReportNo").val());
        }
    })

    $('#txtReportNo').keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#txtReportNo").val().length > 0) {
                $(location).attr("href", $URL._SearchReportByReportNo + "?ReportNo=" + $("#txtReportNo").val());
            }
        }

    });
    $("#btnSearchAddress").unbind().click(function () {
        $(location).attr("href", $URL._SearchReportByAdderss + "?Address=" + $("#txtAddress").val() + "&City=" + $("#txtCity").val() + "&State=" + $("#State option:selected").text() + "&Zip=" + $("#txtZip").val());
    })
    $("#btnInvite").click(function () {
        var sEmail = $('#txtInviteFriend').val();
        if ($.trim(sEmail).length == 0)
        {
            $("#Sucess").css("display", "none");
            $(".bs-example").css("display", "block");
            $("#lblError").html("Email address not valid")
        }
        if (CheckEmail(sEmail))
        {
            $("#btnInvite").toggleClass('active');
            $.ajax({
                type: "POST",
                url: $URL._InviteFriendPost,
                async: false,
                data: { emailTo: $("#txtInviteFriend").val() },
                //dataType: "Json",

                success: function (result) {
                    if (result.Message == "Success") {
                        $(".bs-example").css("display", "none");
                        $("#Sucess").css("display", "block");
                        $("#txtInviteFriend").val('');
                        $("#btnInvite").removeClass('active');
                    }
                    else {
                        $("#Sucess").css("display", "none");
                        $(".bs-example").css("display", "block");
                        $("#lblError").html("There is some problem, please try after sometime")
                        $("#btnInvite").removeClass('active');
                    }
                },
                error: function (e) {

                },
            });

        }
        else {
            $("#Sucess").css("display", "none");
            $(".bs-example").css("display", "block");
            $("#lblError").html("Email address not valid")
           
        }
    });
    $("#btnShareExperience").click(function () {

        var isValid = true;
        $('#myModal2 input[type="text"]').each(function () {
            if ($.trim($(this).val()) == '') {
                isValid = false;
                //$(this).css({
                //    "border": "1px solid red",
                //    "background": "#FFCECE"
                //});
                $(this).addClass("input-validation-error")
            }
            else {
                $(this).removeClass("input-validation-error")
                //$(this).css({
                //    "border": "",
                //    "background": ""
                //});
            }
        });
        if (isValid == false && $("#drpState").val() == "0") {
           
            $("#drpState").addClass("input-validation-error")
          //  e.preventDefault();
        }
        else if (isValid == false && $("#drpState").val() != "0") {
         
            $("#drpState").removeClass("input-validation-error")
            // e.preventDefault();
        }
        else {
            $("#drpState").removeClass("input-validation-error")

            $("#btnShareExperience").toggleClass('active');
            $.ajax({
                type: "POST",
                url: $URL._ShareYourExperience,
                async: false,
                data: { FirstName: $("#txtFirstName").val(), LastName: $("#txtLastName").val(), State: $("#drpState").val(), ReportNo: $("#txtReportNo1").val(), Comment: $("#txtComments").val() },
                //dataType: "Json",

                success: function (result) {
                    if (result.Message == "Success") {
                       
                        $(".bs-exampleExp").css("display", "none");
                        $("#SucessExp").css("display", "block");
                        $("#btnShareExperience").removeClass('active');
                        ClearAll();
                    }
                    else if (result.Message == "Session Over") {
                        $(location).attr("href", $URL._HomePage);
                    }
                    else {
                        $("#SucessExp").css("display", "none");
                        $(".bs-exampleExp").css("display", "block");
                        $("#btnShareExperience").removeClass('active');
                    }

                },
                error: function (e) {

                },
            });
        }
    });
    $("#tblInvoiceDetail tr #anchorprviousPaymentInvoice").click(function () {
        var amount = $(this).parents('tr').find('.amount').html();
        var Date = $(this).parents('tr').find('.date').html();
        $("#Amount").html("$ " + amount);
        $("#PaymentDate").html( Date);
        $('#divInvoice').modal({ backdrop: 'static', keyboard: true })
        $("#divInvoice").modal("show");
    })
    $("#btnPrint").click(function () {
        $('#divInvoice').printElement();
    });
function CheckEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}




$("#btnInviteFriend").click(function () {

    $('#divInviteFriend').modal({ backdrop: 'static', keyboard: true })
    $("#divInviteFriend").modal("show");
    $(".bs-example").css("display", "none");
    $("#Sucess").css("display", "none");
});
$("#exampleRecentViewed tr #DetailFreeBAsic").click(function () {
   
    var orderid = $(this).attr("data-orderid");
    $(location).attr("href", $URL._NFreeBasiReport + "?orderid=" + orderid)
});
$("#exampleRecentViewed tr #Detail1Buy").click(function () {
    var Reportid = $(this).attr("data-orderid");
    //var Reportid = $(this).attr("data-ReportId");
    $(location).attr("href", $URL._NBuyComprehensive + "?ReportId=" + Reportid)
});
$("#exampleRecentViewed tr #Detail1View").click(function () {
    var Reportid = $(this).attr("data-orderid");
    //var Reportid = $(this).attr("data-ReportId");
    $(location).attr("href", $URL._ViewComprehensiveReport + "?inspectionOrderId=" + Reportid)
});
$("#exampleRecentViewed tr #DetailPre").click(function () {
    var Reportid = $(this).attr("data-orderid");
    $(location).attr("href", $URL._PreClosingCheckListIndex+"?OrderId="+Reportid );
}); 

$("#tblPreinspectionDetail tr #Detail").click(function () {
    
    var orderid = $(this).attr("data-orderid");
    $(location).attr("href", $URL._ViewDetailReport + "?inspectionOrderId=" + orderid)
});
$("#tblPreinspectionDetail tr #RequestUpdatePrivilages").click(function () {
    var orderid = $(this).attr("data-orderid");

    $.ajax({
        type: "POST",
        url: $URL._RequestPrivilages,
        async: false,
        data: { OrderID: orderid },
        //dataType: "Json",



        success: function (result) {
            $("#renderRecentViewedReport").html("");
            if (result.Message == "Success") {

                $("#renderRecentViewedReport").html(result.Result);

            }
            else if (result.Message == "Item not Found") {
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'>No item found against given key words. </h3></center>");

            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });
});



function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^([\w-\.]+)@@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
    return pattern.test(emailAddress);
};
});

function YourReport() {


    $.ajax({
        type: "POST",
        url: $URL._GetResentReportsViewed,
        async: false,
        data: { BuyerID: 101 },
        //dataType: "Json",



        success: function (result) {
            $("#renderRecentViewedReport").html("");
            if (result.Message == "Success") {

                $("#renderRecentViewedReport").html(result.Result);

            }
            else if (result.Message == "Item not Found") {
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'>No item found against given key words. </h3></center>");

            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });
}
function YourInvoices() {

    $.ajax({
        type: "POST",
        async: false,
        url: $URL._YourInvoices,
        data: { BuyerID: 101 },
        //dataType: "Json",



        success: function (result) {
            $("#renderInvoices").html("");
            if (result.Message == "Success") {

                $("#renderInvoices").html(result.Result);

            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });
}

function YourUpdatableReport() {

    $.ajax({
        type: "POST",
        async: false,
        url: $URL._YourUpdatableReport,
        data: { BuyerID: 101 },
        //dataType: "Json",



        success: function (result) {
            $("#renderUpdatableReport").html("");
            if (result.Message == "Success") {

                $("#renderUpdatableReport").html(result.Result);

            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderRecentViewedReport").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });
}


function BuyerRequestReport() {

    $.ajax({
        type: "POST",
        async: false,
        url: $URL._BuyerRequestReport,
        // data: { BuyerID: 101 },
        //dataType: "Json",
        success: function (result) {
            $("#renderBuyerRequest").html("");
            if (result.Message == "Success") {

                $("#renderBuyerRequest").html(result.Result);

            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderBuyerRequest").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });

}

function showLoader() {
    $("#loadingModel").show();
}
function hideLoader() {
    $("#loadingModel").hide();
}
function ClearAll() {

    $('#myModal2').find('input:text, input:file,textarea').val('');
    $('#myModal2').find('select').val("0");
    
}


function ViewInspectorDetails(inspectorId) {
    showLoader();
    $.ajax({
        type: "POST",
        async: false,
        url: $URL._ViewInspectorProfile,
        data: { InspectorId: inspectorId },
        success: function (result) {
            $("#renderIndpectorDetails").html("");
            if (result.Message == "Success") {

                $("#renderIndpectorDetails").html(result.Result);
                $('#divViewInspectorDetails').modal({ backdrop: 'static', keyboard: true })
                $("#divViewInspectorDetails").modal("show");
                $(".bs-example").css("display", "none");
                $("#Sucess").css("display", "none");
            }
            else if (result.Message == "Session Over") {
                $(location).attr("href", $URL._HomePage);
            }
            else {
                // $("#PopupDiv p").html(result.Message); renderTable
                $("#renderIndpectorDetails").html("<center><h3 style='width: 464px; color:red'> Error: " + result.Message + ". </h3></center>");

            }
        },
        error: function (e) {

        },
    });
    hideLoader();

}