﻿
$(document).ready(function () {
    //Check File API support
    /*Show Preview of image without loading on server*/
    var PreImageUrl = "/spectromax/Images/";
    //var PreImageUrl = "/spectromaxImages/Uploaded/";
    if (window.File && window.FileList && window.FileReader) {
        var filesInput = document.getElementById("files1");
        filesInput.addEventListener("change", function (event) {
            var files = event.target.files; //FileList object
            if (files.length >= 0 && files.length <= 4) {
                //var output = document.getElementById("result1");
                var div = document.createElement("div");
                div.setAttribute("style", "margin-left:100%;width:600px");
                var count = 0;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    //Only pics
                    if (!file.type.match('image'))
                        continue;
                    var picReader = new FileReader();
                    if (files.length != 4) {
                        if ($("#ImgCertificateValue").val() == 5) {
                            var x = "1";
                        }
                        else {
                            var x = $("#ImgCertificateValue").val();
                        }
                    }
                    else {
                        var x = 1;
                    }

                    picReader.addEventListener("load", function (event) {
                        if (x >= 5) {
                            x = 1;
                        }
                        var picFile = event.target;
                        $("#img" + x).prop("src", picFile.result);
                        //$("#Certification" + x).val(picFile.result);
                        x++;
                        $("#ImgCertificateValue").val(x);
                    });
                    //Read the image
                    picReader.readAsDataURL(file);
                    $(".crop").show();
                }
            }
            else {
                alert("Only 4 Images can be uploaded.");
            }
        });
    }
    else {
        console.log("Your browser does not support File API");
    }



    $('#triggerUpload').click(function () {
        manualuploader.fineUploader('uploadStoredFiles');
        alert(Imagesurl);
    });

    $("#btnSubmitComment").unbind().click(function () {
        if ($("#txtRatingComments").val() == "") {
            $(".bs-exampleExp").css("display", "block")
            $("#lblErrorExp").html("Please Enter Comment's Value.");
            return false;
        }
        else {
            var element1 = $(this);
            var imagesSrc = new Array(4);
            var imgurl1 = $("#img1").attr('src');

            //  var liElement="<li style='margin-top: 1%'><section class='form' style='width: 100%'><div><img src='~/Images/Uploaded/"+imagesSrc[0]+".jpg' style='border: solid 2px;  width: 300px; height:300px; margin-right: 2%;'></div></section></li>"
            var liElement = "<div class='row'><div class='col-md-6'><img class='img-responsive' alt='' src='~/Images/Uploaded/" + imagesSrc[0] + ".jpg'</div></div>";

            imagesSrc[0] = imgurl1.replace(PreImageUrl, '');
            var imgurl2 = $("#img2").attr('src');

            imagesSrc[1] = imgurl2.replace(PreImageUrl, '');

            var imgurl3 = $("#img3").attr('src');
            imagesSrc[2] = imgurl3.replace(PreImageUrl, '');
            var imgurl4 = $("#img4").attr('src');
            imagesSrc[3] = imgurl4.replace(PreImageUrl, '');

            var jsonDataImagesSrc = JSON.stringify(imagesSrc);
            var array = [];
            array.push({
                xc: $("#hdnx").val(),
                y: $("#hdny").val(),
                w: $("#hdnw").val(),
                h: $("#hdnh").val(),
                IsCropped: $("#hdnIsCropped1").val()
            });
            array.push({
                xc: $("#hdnx1").val(),
                y: $("#hdny1").val(),
                w: $("#hdnw1").val(),
                h: $("#hdnh1").val(),
                IsCropped: $("#hdnIsCropped2").val()
            });
            array.push({
                xc: $("#hdnx2").val(),
                y: $("#hdny2").val(),
                w: $("#hdnw2").val(),
                h: $("#hdnh2").val(),
                IsCropped: $("#hdnIsCropped3").val()
            });
            array.push({
                xc: $("#hdnx3").val(),
                y: $("#hdny3").val(),
                w: $("#hdnw3").val(),
                h: $("#hdnh3").val(),
                IsCropped: $("#hdnIsCropped4").val()
            });
            var CoordinatesString = JSON.stringify(array);
            var comments = $("#txtRatingComments").val();
            $("#btnSubmitComment").toggleClass('active');
            debugger;
            $.ajax({
                url: $URL._UpdateSubSectionInfoPost,
                type: "Post",
                contenttype: "application/json; charset=utf-8",
                data: { orderId: $("#hdnOrder").val(), sectionId: $("#hdnSecId").val(), subsectionId: $("#hdnSubSecId").val(), tableName: $("#hdnTableName").val(), comments: $("#txtRatingComments").val(), imagesUrl: jsonDataImagesSrc, InspectionReportID: $("#hdnInspectionReportId").val(), xc: CoordinatesString },
                dataType: "Json",
                success: function (result) {
                    debugger;
                    $("#messages").html('');
                    if (result.Message == "Updation done successfully.") {
                        var updatehtml = $("#DivOwnerUpdates").html();

                        updatehtml = updatehtml.replace('Comments', comments)
                        updatehtml = updatehtml.replace("#img1", "<img height='36px' width='36px' src='" + imgurl1 + "' alt='' class='img-responsive heiwid'>");
                        updatehtml = updatehtml.replace("#img2", "<img height='36px' width='36px' src='" + imgurl2 + "' alt='' class='img-responsive heiwid'>");
                        updatehtml = updatehtml.replace("#img3", "<img height='36px' width='36px' src='" + imgurl3 + "' alt='' class='img-responsive heiwid'>");
                        updatehtml = updatehtml.replace("#img4", "<img height='36px' width='36px' src='" + imgurl4 + "' alt='' class='img-responsive heiwid'>");

                        //$("#updatecount").text(parseInt($("#updatecount").text()) + 1)

                        $(updatehtml).insertAfter($("input[value='" + $("#hdnSubSecId").val() + "']").parents('.noclass'));
                    }
                    if (result.Message == "Success") {
                        $("#txtRatingComments").val('');
                        $("#btnSubmitComment").removeClass('active');
                        $(".bs-exampleExp").css("display", "none");
                        $("#SucessExp").css("display", "block");
                        $(element1).attr("disabled", "disabled");
                        $(element1).css("cursor", "not-allowed");
                        $(element1).css("background-color", "#102B75");
                        $(buttonElement).remove();
                        for (var i = 0; i < 4; i++) {
                            $(uiElement).append(liElement)
                        }
                    }
                    else if (result.Message == "Already Updated.") {
                        $("#btnSubmitComment").removeClass('active');
                        $("#SucessExp").css('display', 'block');
                        $("#SucessExp").html("<p style='color:red'><i>-- Section already updated. --</i></p>");
                    }
                    else {
                        $("#btnSubmitComment").removeClass('active');
                        $(buttonElement).remove();
                        $(".bs-exampleExp").css("display", "none");
                        $("#SucessExp").css("display", "block");
                        $(pElement).append("<div class='row ad-wrapper'><div class='col-md-12'><h1>Owner Updates</h1><div class='setcb'><img class='pull-left' alt='' src='/Content/Nimg/ad-wrapper-text.png'><p>" + comments + "</p></div><div class='row'><div class='col-md-3'><img class='img-responsive' alt='' src='" + imagesSrc[0] + "'/></div><div class='col-md-3'><img class='img-responsive' alt='' src='" + imagesSrc[1] + "'/></div><div class='col-md-3'><img class='img-responsive' alt='' src='" + imagesSrc[2] + "'/></div><div class='col-md-3'><img class='img-responsive' alt='' src='" + imagesSrc[3] + "'/></div></div></div></div>");
                    }
                },
                error: function () {
                    $("#btnSubmitComment").removeClass('active');
                    $("#messages").css('display', 'block');
                    $("#messages").html("<p style='color:red'><i>-- There is an error. --</i></p>");
                },
            });
        }
    });

    var s = $("#sticker");
    var pos = s.position();
    $(window).scroll(function () {
        var windowpos = $(window).scrollTop();
        //  s.html("Distance from top:" + pos.top + "<br />Scroll position: " + windowpos);
        if (windowpos >= pos.top) {
            s.addClass("stick");
        } else {
            s.removeClass("stick");
        }
    });

});
