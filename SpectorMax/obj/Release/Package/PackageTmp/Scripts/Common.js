﻿


function showLoader() {
    $("#loadingModel").show();
}
function hideLoader() {
    $("#loadingModel").hide();
}
$(document).ready(function () {
    var zipelement;
    $('#tblinspectionDetail').dataTable({
        "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "aButtons": [
                "copy",
                {
                    "sExtends": "collection",
                    "sButtonText": "Save",
                    "aButtons": ["csv", "xls", "pdf"]
                }
            ]
        }
    });

    $("#DeleteInspector").live("click", function () {

        var UserId = $(this).attr('data-UserId');
        var Rowelement = $(this).parents('tr:first')
        showLoader();
        $.ajax({
            type: "Post",
            url: $URL._DeleteSingleInspector,
            data: { UserId: UserId },
            success: function (result) {
                if (result == "Success") {
                    $(".success").css("display", "block");
                    $("#SuccessMsg").html(" USERS DELETED SUCCESSFULLY")
                    $(".error").css("display", "none");
                    Rowelement.remove();
                    hideLoader();
                }

                else if (result == "Error") {

                    $(".success").css("display", "none");
                    $(".error").css("display", "block");
                    hideLoader();
                }
                else if (result == "SessionOver") {
                    hideLoader();
                }
            },
            error: function () { hideLoader(); alert("error"); },
        });


    });
    $("#DeleteSubAdmin").live("click", function () {

        var UserId = $(this).attr('data-UserId');
        var Rowelement = $(this).parents('tr:first')
        showLoader();
        $.ajax({
            type: "Post",
            url: $URL._DeleteSingleAdmin,
            data: { UserId: UserId },
            success: function (result) {
                if (result == "Success") {
                    $(".success").css("display", "block");
                    $("#SuccessMsg").html(" USERS DELETED SUCCESSFULLY")
                    $(".error").css("display", "none");
                    Rowelement.remove();
                    hideLoader();
                }

                else if (result == "Error") {

                    $(".success").css("display", "none");
                    $(".error").css("display", "block");
                    hideLoader();
                }
                else if (result == "SessionOver") {
                    hideLoader();
                }
            },
            error: function () { hideLoader(); alert("error"); },
        });


    });

    $("#DeleteMultipleAdmin").live("click", function () {
        var marked = "";
        var n = $('input:checked').length
        $('input:checked').each(function () {
            var value = $(this).attr('value')
            marked += value + ","
        });

        showLoader();
        $.ajax({
            type: "Post",
            url: $URL._DeleteMultipleAdmin,
            data: { UserId: marked.substr(0, marked.length - 1) },
            success: function (result) {
                if (result == "Success") {
                    $(".success").css("display", "block");
                    $("#SuccessMsg").html(n + " USERS DELETED SUCCESSFULLY")
                    $(".error").css("display", "none");
                    $('input:checkbox').removeAttr('checked');
                    window.location.reload();
                    hideLoader();
                }

                else if (result == "Error") {

                    $(".success").css("display", "none");
                    $(".error").css("display", "block");
                    hideLoader();
                }
                else if (result == "SessionOver") {
                    hideLoader();
                }
            },
            error: function () { hideLoader(); alert("error"); },
        });
    });





    $("#DeleteMultipleAdmin").live("click", function () {
        var marked = "";
        var n = $('input:checked').length
        $('input:checked').each(function () {
            var value = $(this).attr('value')
            marked += value + ","
        });

        showLoader();
        $.ajax({
            type: "Post",
            url: $URL._DeleteMultipleAdmin,
            data: { UserId: marked.substr(0, marked.length - 1) },
            success: function (result) {
                if (result == "Success") {
                    $(".success").css("display", "block");
                    $("#SuccessMsg").html(n + " USERS DELETED SUCCESSFULLY")
                    $(".error").css("display", "none");
                    $('input:checkbox').removeAttr('checked');
                    window.location.reload();
                    hideLoader();
                }

                else if (result == "Error") {

                    $(".success").css("display", "none");
                    $(".error").css("display", "block");
                    hideLoader();
                }
                else if (result == "SessionOver") {
                    hideLoader();
                }
            },
            error: function () { hideLoader(); alert("error"); },
        });
    });



    $("#DeleteMultipleUser").live("click", function () {
        var marked = "";

        var n = $('input:checked').length
        $('input:checked').each(function () {
            var value = $(this).attr('value')
            marked += value + ","
        });

        showLoader();
        $.ajax({
            type: "Post",
            url: $URL._DeleteMultipleAdmin,
            data: { UserId: marked.substr(0, marked.length - 1) },
            success: function (result) {
                if (result == "Success") {
                    $(".success").css("display", "block");
                    $("#SuccessMsg").html(n + " USERS DELETED SUCCESSFULLY")
                    $(".error").css("display", "none");
                    $('input:checkbox').removeAttr('checked');
                    window.location.reload();
                    hideLoader();
                }

                else if (result == "Error") {

                    $(".success").css("display", "none");
                    $(".error").css("display", "block");
                    hideLoader();
                }
                else if (result == "SessionOver") {
                    hideLoader();
                }
            },
            error: function () { hideLoader(); alert("error"); },
        });
    });
    $("#DeleteMultipleInspector").live("click", function () {
        var marked = "";

        var n = $('input:checked').length
        $('input:checked').each(function () {
            var value = $(this).attr('value')
            marked += value + ","
        });

        showLoader();
        $.ajax({
            type: "Post",
            url: $URL._DeleteMultipleInspector,
            data: { UserId: marked.substr(0, marked.length - 1) },
            success: function (result) {
                if (result == "Success") {
                    $(".success").css("display", "block");
                    $("#SuccessMsg").html(n + " USERS DELETED SUCCESSFULLY")
                    $(".error").css("display", "none");
                    $('input:checkbox').removeAttr('checked');
                    window.location.reload();
                    hideLoader();
                }

                else if (result == "Error") {

                    $(".success").css("display", "none");
                    $(".error").css("display", "block");
                    hideLoader();
                }
                else if (result == "SessionOver") {
                    hideLoader();
                }
            },
            error: function () { hideLoader(); alert("error"); },
        });
    });


    $("#tblinspetorReportDetail").dataTable();
    $("#tblinspectionReports").dataTable();
    $("#tblinspectorPaymentDetails").dataTable();
    $("#tblinspectionDetail tr #Percent").live('click', function () {
        showLoader();
        var inspectorId = $(this).attr('data-inspectorid');
        var Payment = $(this).attr('data-payment');
        $("#hdnInspectorId").val(inspectorId);
        $('#New').val("");
        $("#Current").val(Payment);
        $("#PopUp-error").css("display", "none")
        $("#PopUp-success").css("display", "none")
        
        $('#PopupDiv').modal({ backdrop: 'static', keyboard: true })
        $("#PopupDiv").modal("show");
        hideLoader();
    });

    $("#btnsubmitPercent").click(function () {
      
        var intRegex = /^\d+$/;
        var newPercent = $('#New').val();

        if (intRegex.test(newPercent) && newPercent <= 100) {
            var inspectorId = $("#hdnInspectorId").val()
            var v = "";
            $("#btnsubmitPercent").addClass('active')
            $.ajax({
                type: "Post",
                url: $URL._UpdatePercent,
                data: { inspectorId: inspectorId, newPercent: newPercent },
                success: function (result) {
                    if (result == "success") {
                        $("a[data-inspectorid='" + inspectorId + "']").attr('data-payment', newPercent)
                        $("a[data-inspectorid='" + inspectorId + "']").parent('td').prev('td').html(newPercent);
                        $("#lblMessage").html("Percent Updated Successfully");
                        $("#PopUp-success").css("display", "block");
                        $("#PopUp-error").css("display", "none");
                        // window.setTimeout('location.reload()', 3000);
                        $("#btnsubmitPercent").removeClass('active')
                    }

                    else if (result == "error") {
                        $("#lblMessage1").html("some error occred ! Please try again");
                        $("#PopUp-error").css("display", "block");
                        $("#PopUp-success").css("display", "none");
                        $("#btnsubmitPercent").removeClass('active')
                    }
                },
                error: function () { $("#btnsubmitPercent").removeClass('active') },
            });
        }
        else {
            $("#lblMessage1").html("Please enter valid percent");
            $("#PopUp-error").css("display", "block");
            $("#PopUp-success").css("display", "none");
        }
    });



    /*Get Zip codes of inspector:Event Click here to Assign Zip Code Button*/
    $("#tblinspectionDetail tr #zip").live('click', function () {
        zipelement = $(this).parents('td:first').prevAll(".zipcodes");

        orderid = $(this).attr('data-orderid');
      ///  $("#PopUp-success").css("display", "none");
      //  $("#Zip-error").css("display", "none");
        /*initializing Progress Bar*/
        showLoader();
        $("#hdnInspectorDetailsId").val(orderid);
        $("#lblMessage").html(" ");
        $("#PopUp-success").css("display", "none");
        $('#txtZipCode').val("");
        $.getJSON($URL._AssignZipCode, { id: orderid },
           function (classesData) {

               var select = $("#ZipCode");
               select.empty();
               $.each(classesData, function (index, itemData) {

                   select.append($('<option/>', {
                       value: itemData.Value,
                       text: itemData.Text
                   }));
               });
            
              
               $('#PopupDiv').modal({ backdrop: 'static', keyboard: true })
               $("#PopupDiv").modal("show");
           });
        hideLoader();
    });
    /*Delete Zip codes of inspector:Event Delete Button*/
    $("#btnDelete").click(function () {

        var value = $('#ZipCode').val();
        var text = $('#ZipCode option:selected').text();
        var InspectorDetailsid = $("#hdnInspectorDetailsId").val()
        var v = "";
        var zipHtml = $(zipelement).html();
        var zipArray = zipHtml.split(',');
        if (value != null) {
            for (i = 0; i < value.length; i++) {
                v += value[i] + ",";
            }
            $("#btnDelete").addClass('active');
            $.ajax({
                type: "Post",
                url: $URL._DeleteZipCode,
                data: { id: v.substr(0, v.length - 1), InspectorDetailsid: InspectorDetailsid },
                success: function (result) {
                    if (result == "Success") {
                        for (i = 0; i < value.length; i++) {
                            $("#ZipCode option[value='" + value[i] + "']").remove();

                        }
                        $("#btnDelete").removeClass('active');
                        zipHtml = zipHtml.replace(text + ",", "");
                        $(zipelement).html(zipHtml);
                        $("#lblMessage").html("Zip code deleted successfully");
                        $("#PopUp-success").css("display", "block");
                    }

                    else if (result == "error") {
                        $("#btnDelete").removeClass('active');
                    }
                },
                error: function () { $("#btnDelete").removeClass('active'); },
            });
        }
        else {
            $("#PopUp-success").css("display", "none");
            $("#Zip-error").css("display", "block");
            $("#lblMessageziperror").html("Please Select Zip Code");
            $("#btnDelete").removeClass('active');
        }
    });
    /*Assign Zip codes to inspector:Event Assing Button*/
    $("#AddZip").click(function () {
        var value = $('#txtZipCode').val();
        var InspectorDetailsid = $("#hdnInspectorDetailsId").val()
        var Zcodes = $('#ZipCode');
        var flag = true;
        var zipHtml = $(zipelement).html();

        $('#ZipCode option').each(function () {
            optionText = $(this).html();
            optionVal = $(this).val();
            if (value == optionText) {
                flag = false;

            }
        });

        if (flag == true && value != "") {
            $("#AddZip").addClass('active');
            $.ajax({
                type: "Post",
                url: $URL._AddZipCode,
                data: { id: value, InspectorDetailsid: InspectorDetailsid },
                success: function (result) {
                    if (result.indexOf("Success") != -1) {
                        var listText = result.split("&");

                        var htmlSelect = $("#ZipCode");
                        htmlSelect.append($('<option/>', {
                            value: listText[1],
                            text: listText[2]
                        }));
                        zipHtml = zipHtml + value + ",";
                        $(zipelement).html(zipHtml)
                        $("#Zip-error").css("display", "none");
                        $("#lblMessage").html("Zip code assigned successfully");
                        $("#PopUp-success").css("display", "block");
                        $('#txtZipCode').val("");
                        $("#AddZip").removeClass('active');
                    }
                    else if(result=="error") {
                        $("#PopUp-success").css("display", "none");
                        $("#Zip-error").css("display", "block");
                        $("#lblMessageziperror").html("Zip code already assigned to someone else");
                        $("#AddZip").removeClass('active');
                    }
                    else if (result == "NotExist") {
                        $("#PopUp-success").css("display", "none");
                        $("#Zip-error").css("display", "block");
                        $("#lblMessageziperror").html("Zip code is not exist.");
                        $("#AddZip").removeClass('active');
                    }
                },
                error: function () { $("#AddZip").removeClass('active'); },
            });
        }
        else {
            $("#PopUp-success").css("display", "none");
            $("#Zip-error").css("display", "block");
            if (value == "") {
                $("#lblMessageziperror").html("Zip enter zip code.");
            }
            else {
                $("#lblMessageziperror").html("Zip code already assigned.");
            }
            $("#AddZip").removeClass('active');
        }

    });


    /*Inspector Payment Cycle-Start*/
    $("#tblinspectionDetail tr #PaymentCylce").live('click', function () {

        InspectorID = $(this).attr('data-orderid');
        /*initializing Progress Bar*/
        showLoader();
        $("#hdnInspectorDetailsId").val(InspectorID);
        $("#lblMessagePayment").html(" ");
        $("#PopUpPayment").css("display", "none");
        $("#PopUp-error").css("display", "none");
        $.getJSON($URL._AssignPaymentCycle, { id: InspectorID },
           function (classesData) {

               $.each(classesData, function (index, itemData) {
                   var FromDate;
                   var EndDate;
                   if (itemData.PaymentCycleStartDate == null && itemData.PaymentCycleEndDate == null) {
                       FromDate = new Date();
                       EndDate = new Date();
                       $("#from").val(FromDateformatted);
                       $("#To").val(EndDateformatted);
                   }
                   else {
                       FromDate = new Date(parseInt(itemData.PaymentCycleStartDate.substr(6)));
                       var FromDateformatted = FromDate.getFullYear() + "-" +
                        ("0" + (FromDate.getMonth() + 1)).slice(-2) + "-" +
                         ("0" + FromDate.getDate()).slice(-2);
                       $("#from").val(FromDateformatted);
                       EndDate = new Date(parseInt(itemData.PaymentCycleEndDate.substr(6)));
                       var EndDateformatted = EndDate.getFullYear() + "-" +
                        ("0" + (EndDate.getMonth() + 1)).slice(-2) + "-" +
                         ("0" + EndDate.getDate()).slice(-2);
                       $("#To").val(EndDateformatted);
                   }
               });
               hideLoader();
               $('#PopupDivPayment').modal({ backdrop: 'static', keyboard: true })
               $("#PopupDivPayment").modal("show");
           });
    });

    $("#btnSubmitPaymentCycle").click(function () {
        var FromDate = $('#from').val();
        var EndDate = $('#To').val();

        var FDate = new Date(FromDate);
        var EDate = new Date(EndDate);
        if (FDate < EDate) {

            var InspectorDetailsid = $("#hdnInspectorDetailsId").val()
            $("#btnSubmitPaymentCycle").addClass("active")
            $.ajax({
                type: "Post",
                url: $URL._UpdatePaymentCycle,
                data: { id: InspectorDetailsid, FromDate: FromDate, EndDate: EndDate },
                success: function (result) {
                    if (result == "success") {
                        $("#lblMessagePayment").html("Payment cycle updated successfully");
                        $("#PopUp-error").css("display", "none");
                        $("#PopUpPayment").css("display", "block");
                        $("#PopUp-error").css("display", "none");
                        $("#btnSubmitPaymentCycle").removeClass("active")
                    }
                    else if (result == "error") {
                        $("#lblMessage1").html("Some error occured.Please try again");
                        $("#PopUp-error").css("display", "block");
                        $("#PopUpPayment").css("display", "none");
                        $("#btnSubmitPaymentCycle").removeClass("active")
                    }
                },
                error: function () { hideLoader(); alert("error"); },
            });
        }
        else {
            $("#PopUpPayment").css("display", "none");
            $("#lblMessage1").html("From Date cant be greater than To Date");
            $("#PopUp-error").css("display", "block");
            $("#btnSubmitPaymentCycle").removeClass("active")
        }
    });

    /*Inspector Payment Cycle-end*/





    function GetQueryStringParams(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }


    $("#tblinspectionDetail tr #AssingInspector").live('click', function () {
        showLoader();
        var InspectorID = $(this).attr('data-inspectorid');
        var Orderid = GetQueryStringParams('orderid');
      
      
        $("#hdnInspectorDetailsId").val(InspectorID);
        $("#display-success").css("display", "none");


        $.ajax({
            type: "Post",
            url: $URL._AssignInspectoinRequest,
            data: { id: InspectorID, Orderid: Orderid },
            success: function (result) {
                if (result == "success") {
                    $(".bs-exampleExp").hide()
                    $("#success").show();
                    hideLoader();
                }

                else if (result == "Mail Not Send") {
                    $(".bs-exampleExp").hide()
                    $("#success").show();
                    hideLoader();
                }
            },
            error: function () { hideLoader();; alert("error"); },
        });


    });

    $("#tblRejected tr .Delete").live('click', function () {
        if (confirm("Are you sure you want to delete?")) {
            $(this).addClass('active')
            var span = $(this).find("#lblRequest")
            $(span).html("Please Wait ....")
            var OrderId = $(this).attr('data-OrderID');
            var tr = $(this).parents('tr');
            $.ajax({
                type: "Post",
                url: $URL._DeleteOrder,
                data: { OrderId: OrderId },
                dataType: "Json",
                success: function (result) {
                    if (result == "Success") {
                        $(this).removeClass('active')
                        $(tr).remove();
                     //   $("#lblRequest").html("Delete")
                        $(".bs-exampleExp").hide();
                        $("#success").show();
                    }
                    else if (result == "Error")
                    {
                    //    $("#lblRequest").html("Delete")
                        $(this).removeClass('active')
                        $(".bs-exampleExp").show();
                        $("#success").hide();
                    }

                },
                error: function (xhr) {
                    
                }
            });
        }

    });
    
    $("#tblPending tr .Delete").live('click', function () {
        if (confirm("Are you sure you want to delete?")) {
            $(this).addClass('active')
            var span = $(this).find("#lblRequest")
            $(span).html("Please Wait ....")
            var OrderId = $(this).attr('data-OrderID');
            var tr = $(this).parents('tr');
            $.ajax({
                type: "Post",
                url: $URL._DeleteOrder,
                data: { OrderId: OrderId },
                dataType: "Json",
                success: function (result) {
                    if (result == "Success") {
                        $(this).removeClass('active')
                        $(tr).remove();
                        //   $("#lblRequest").html("Delete")
                        $(".bs-exampleExp").hide();
                        $("#success").show();
                    }
                    else if (result == "Error") {
                        //    $("#lblRequest").html("Delete")
                        $(this).removeClass('active')
                        $(".bs-exampleExp").show();
                        $("#success").hide();
                    }

                },
                error: function (xhr) {

                }
            });
        }

    });
    $("#tblInspectionReports tr .Delete").live('click', function () {
        if (confirm("Are you sure you want to delete?")) {
            $(this).addClass('active')
            var span = $(this).find("#lblRequest")
            $(span).html("Please Wait ....")
            var OrderId = $(this).attr('data-OrderID');
            var tr = $(this).parents('tr');
            $.ajax({
                type: "Post",
                url: $URL._DeleteOrder,
                data: { OrderId: OrderId },
                dataType: "Json",
                success: function (result) {
                    if (result == "Success") {
                        $(this).removeClass('active')
                        $(tr).remove();
                        //   $("#lblRequest").html("Delete")
                        $(".bs-exampleExp").hide();
                        $("#success").show();
                    }
                    else if (result == "Error") {
                        //    $("#lblRequest").html("Delete")
                        $(this).removeClass('active')
                        $(".bs-exampleExp").show();
                        $("#success").hide();
                    }

                },
                error: function (xhr) {

                }
            });
        }

    });
    
   
    $('.datepicker').datepicker({
        //format: 'mm-dd-yyyy'
        format: 'yyyy-mm-dd'
    });

    $("#btnSearchInspectoionReport").click(function () {

        showLoader();
        $.ajax({
            type: "Post",
            url: $URL._FilterInspectionReport,
            data: {
                startDate: $("#txtFromDate").val(),
                endDate: $("#txtTodate").val(),
                ExportToExcel: false

            },
            dataType: "Json",
            success: function (result) {
                hideLoader();
                $(".payment").html(result.Result);

            },
            error: function (xhr) {
                hideLoader();
                alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                hideLoader();
            }
        });
    });

    $(".red").live('click', function () {
        showLoader();

        var inspectonId = $(this).attr('data-inspectonid');

        if ($(this).find("a").html() == "View Report") {
            $(location).attr("href", $URL._ViewDetailReport + "?inspectionOrderId=" + inspectonId);
        }
        else {
            var hdnrole = $(this).find(":hidden")

            $(location).attr('href', $URL._InsectionOrderDetailsCreate + "?inspectionOrderId=" + $(this).attr('data-inspectonid') + "&ReportType=InspectionRequest&role=" + hdnrole.val());
        }
        hideLoader();
    });
    $("#btnLogout").click(function () {
        $(location).attr("href", $URL._LogOut);
    });



    $('#txtZipCode').typeahead({
        source: function (query, process) {
            return $.ajax({
                url: "../AutoComplete.ashx?data=" + query,
                type: 'post',
                data: { query: query },
                dataType: 'json',
                success: function (result) {
                    return process(result);
                }
            });
        }
    });

    $('#PropertyAddressZip').typeahead({
        source: function (query, process) {
            return $.ajax({
                url: "../AutoComplete.ashx?data=" + query,
                type: 'post',
                data: { query: query },
                dataType: 'json',
                success: function (result) {
                    return process(result);
                }
            });
        }
    });

    $('#buyerAgent_BuyerAgentZip').typeahead({
        source: function (query, process) {
            return $.ajax({
                url: "../AutoComplete.ashx?data=" + query,
                type: 'post',
                data: { query: query },
                dataType: 'json',
                success: function (result) {
                    return process(result);
                }
            });
        }
    });



    $('#sellerAgent_SellerAgentZip').typeahead({
        source: function (query, process) {
            return $.ajax({
                url: "../AutoComplete.ashx?data=" + query,
                type: 'post',
                data: { query: query },
                dataType: 'json',
                success: function (result) {
                    return process(result);
                }
            });
        }
    });


    $('#RequestedByZip').typeahead({
        source: function (query, process) {
            return $.ajax({
                url: "../AutoComplete.ashx?data=" + query,
                type: 'post',
                data: { query: query },
                dataType: 'json',
                success: function (result) {
                    return process(result);
                }
            });
        }
    });


    $('#Zip').typeahead({
        source: function (query, process) {
            return $.ajax({
                url: "../AutoComplete.ashx?data=" + query,
                type: 'post',
                data: { query: query },
                dataType: 'json',
                success: function (result) {
                    return process(result);
                }
            });
        }
    });


    $('#ZipCode').typeahead({
        source: function (query, process) {
            return $.ajax({
                url: "../AutoComplete.ashx?data=" + query,
                type: 'post',
                data: { query: query },
                dataType: 'json',
                success: function (result) {
                    if (result.length == 0) {
                        result = {
                            "options": [
                                "No Zip Exists"
                            ]
                        };
                        //result.push("No Zip Exists");

                        return process(result.options);
                        $('#ZipCode').val("");

                    }
                    else {

                        return process(result);
                    }
                }
            });
        }
    });
    //$('#Zipcode').typeahead({
    //    source: function (query, process) {
    //        return $.ajax({
    //            url: "../AutoComplete.ashx?data=" + query,
    //            type: 'post',
    //            data: { query: query },
    //            dataType: 'json',
    //            success: function (result) {
    //                return process(result);
    //            }
    //        });
    //    }
    //});
    $('#txtZipCode').typeahead({
        source: function (query, process) {
            $("#ZipCodeProgressBar").show();
            return $.ajax({
                url: "../AutoComplete.ashx?data=" + query,
                type: 'post',
                data: { query: query },
                dataType: 'json',
                success: function (result) {
                    $("#ZipCodeProgressBar").hide();
                    return process(result);
                },
                error: function () {
                    $("#ZipCodeProgressBar").hide();
                }
            });
        }
    });


    //function process(resultList) {
    //    alert(resultList.length)
    //    var newData = [];

    //    $.each(data, function () {
    //        newData.push(this.name);
    //    });
    //    return process(newData);
    //}


    $("#btnSendOffer").click(function () {


        showLoader();
        $("#PopUpPayment").css("display", "none");
        $("#PopUp - error").css("display", "none");
        $.getJSON($URL._GetOfferTemplate, {},
          function (classesData) {

              $("#Content").html(classesData.TemplateDiscription);
              $(".jqte_green_editor").html(classesData.TemplateDiscription);
             
              $('#PopupDivPayment').modal({ backdrop: 'static', keyboard: true })
              $("#PopupDivPayment").modal("show");

          })

        hideLoader();
    });

    $("#btnSubmitOffer").click(function () {

        var emailBody = $("#Content").html();
        var emailToSend = $("#EmailAddress").val();
        $("#btnSubmitOffer").addClass("active")
        $("#PopUpPayment").css("display", "none");
        $("#PopUp - error").css("display", "none");
        $.ajax({
            type: "Post",
            url: $URL._SendOfferTemplate,
            data: { emailBody: emailBody, emailToSend: emailToSend },
            success: function (result) {
                if (result == "success") {
                    $("#lblMessagePayment").html("Offer Sent Successfully");
                    $("#PopUpPayment").css("display", "block");
                    $("#PopUp - error").css("display", "none");
                    $("#btnSubmitOffer").removeClass("active")
                }
                else if (result == "error") {
                    $("#lblMessage1").html("There is some error. Please try after some time !");
                    $("#PopUpPayment").css("display", "none");
                    $("#PopUp - error").css("display", "block");

                    $("#btnSubmitOffer").removeClass("active")
                }
            },
            error: function () { $("#btnSubmitOffer").removeClass("active"); alert("error"); },
        });

    });

    ///*Fetch City based on States*/
    $("#State").change(function () {
        $("#imgCity").show();
        $.ajax({
            type: "POST",
            url: $URL._GetCity,
            dataType: "json",
            data: { state: $('#State :selected').val() },
            success: function (result) {
                $('#City').empty();
                $(result).each(function (item) {
                    $('#City').append($('<option></option>').val(this.Value).html(this.Value));
                });
                $("#imgCity").hide();
            },
            error: function (e) {
                $("#imgCity").hide();
            },
        });
    });

    /*Fetch zip based on city*/
    $("#City").change(function () {
        $("#imgZipcode").show();
        $.ajax({
            type: "POST",
            url: $URL._GetZipCode,
            dataType: "json",
            data: { City: $('#City :selected').text() },
            success: function (result) {
                $('#Zipcode').empty();
                $(result).each(function (item) {
                    $('#Zipcode').append($('<option></option>').val(this.toString()).html(this.toString()));
                });
                $("#imgZipcode").hide();
            },
            error: function (e) {
                $("#imgZipcode").hide();
            },
        });
    });
});




/*Image Editing functions*/
var details;

function EditImage(data) {
   
    $('#EditImage').modal({ backdrop: 'static', keyboard: true })
    $("#EditImage").modal("show");

   // $("#Image").attr("src", $("#img1").attr("src"));
   
  
    EditImageNew($("#img1").prop('src'));
}

function saveImg(image) {
    var _this = this;
    var myCanvas = $(".wPaint-canvas")[0];
   
    image = myCanvas.toDataURL("image/jpg");
    
    image = image.replace('data:image/jpg;base64,', '');
  
   
    var x1 = parseInt(details.x);
    var y1 = parseInt(details.y);
    var w1 = parseInt(details.w);
    var h1 = parseInt(details.h);

    var imagearr1 = $("#Image").attr('src');
    var abc;
    $.ajax({
        url: '/Notification/Cropper',
        type: 'POST',
        //data: data,
        data: { x: x1, y: y1, w: w1, h: h1, img: image },
        dataType: 'json',
        async: false,
        success: function (result) {
            alert('result' + result);
            abc = result;
            $("#HiddenField1").val('/Images/CropImage//' + result);
           // $("[id$=HiddenFieldCroped]").val("Croped");
           // EditImageNew(result);
          //  var imageData = $("#wPaint").wPaint("image"); // if you want to maintain the image after resizing the canvas
           // alert('hi image data======'+imageData);
          

           // $("#wPaint").wPaint("image", imageData);
            
             EditImageNew('/Images/CropImage//' + result);
          //  $('#wPaint').wPaint('clear');
          
        },
        error: function (e) { alert('error'); console.log(e); }
    });
    
}

//document.ready(function () {
//    alert($("#HiddenField1").val());
//    if ($("#HiddenField1").val() != "") {
//        EditImageNew($("#HiddenField1").val());
//    }
//});

function EditImageNew(path) {
   
    //$('#wPaint').wPaint('clear');
    $('#wPaint').wPaint({
        image: path,
       // menuOffsetLeft: -5,
        menuOffsetTop: -50,
        saveImg: saveImg,
        loadImgBg: loadImgBg,
        loadImgFg: loadImgFg,
        menuHandle: false,
        imageStretch: true,
      //  menuOrientation: 'vertical',
       // menu: ['undo', 'clear', 'rectangle', 'ellipse', 'line', 'pencil', 'text', 'eraser', 'fillColor', 'lineWidth', 'strokeColor'], // menu items - appear in order they are set
        bg: null
    });

    alert($("[id$=HiddenFieldCroped]").val());
    $(".wPaint-menu-icon-img:visible").each(function () {
        
        if ($(this).parent().attr("title") == "Crop") {
            //$(this).hide();
           // $('.wPaint-menu-icon').hide();
            // $('.wPaint-menu-icon-name-crop').show();

            $(this).html("Crop");
            $(this).parent().css("width", "70px");
            $(this).parent().parent().css("width", "370px");
            $(this).css("width", "60px");
        }

    });
}
function loadImgBg() {

    // internal function for displaying background images modal
    // where images is an array of images (base64 or url path)
    // NOTE: that if you can't see the bg image changing it's probably
    // becasue the foregroud image is not transparent.
    this._showFileModal('bg', images);
}

function loadImgFg() {
    alert('hi 13213212321');
    // internal function for displaying foreground images modal
    // where images is an array of images (base64 or url path)
    this._showFileModal('fg', images);
}


function CropImage() {
   
    $('#Image').Jcrop({
        onChange: updatePreview,
        onSelect: updatePreview
    });

    $("#btnAction").val("Apply Crop");
}

function EndCrop() {
   
    //var data = "x=" + parseInt(details.x);
    //data += "&y=" + parseInt(details.y);
    //data += "&w=" + parseInt(details.w);
    //data += "&h=" + parseInt(details.h);
    //data += "&img=" + $("#Image").attr('src');
    var x1 = parseInt(details.x);
    var y1 = parseInt(details.y);
    var w1 = parseInt(details.w);
    var h1 = parseInt(details.h);
    var imagearr1 = $("#Image").attr('src');
 
    $.ajax({
        url: '/Notification/Cropper',
        type: 'POST',
        //data: data,
        data: { x: x1, y: y1, w:w1,h: h1,img: imagearr1 },
        dataType: 'json',
        async: false,
        success: function (result) {
            $("#HiddenField1").val('/content/images/' + result);
            $("[id$=HiddenFieldCroped]").val("Croped");

            location.reload();
        },
        error: function (e) { console.log(e); }
    });
    return false;
}

function updatePreview(c) {
    if (parseInt(c.w) > 0) {
        details = c;
    }
};

