﻿//<![CDATA[ 
$(window).load(function () {
    $(document).ready(function () {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
        var oName = getQueryStringByName('on');
        var orderId = getQueryStringByName('Id');
        $('#lblOwnerName').text(oName);
        $('#hdnOrderDetailId').val(orderId);
        BindCalendar();
    });
});//]]>  

function BindCalendar() {
    $('#calendar').html('');
    var calendar = $('#calendar').fullCalendar({
        defaultView: 'month',
        editable: true,
        selectable: true,
        droppable: true,
        drop: function (date, allDay) {
            alert("Dropped on " + date + " with allDay=" + allDay);
        },
        axisFormat: 'hh TT',
        timeFormat: {
            agenda: "hh:mm TT{ - hh:mm TT}",
            '': "hh:mm TT{ - hh:mm TT}"
        },
        titleFormat: {
            month: 'MMMM yyyy',
            week: "MMM dd[ yyyy]{ '&#8212;'[ MMM] dd yyyy}",
            day: 'dddd, MMM dd, yyyy'
        },
        columnFormat: {
            month: 'ddd',
            week: 'ddd MM/dd',
            day: 'dddd MM/dd'
        },
        header: {
            left: 'today prev,next',
            center: 'title',
            right: ''
        },
        editable: false,
        droppable: false,

        dayClick: function (dateObj, allDay, jsEvent, view) {
            today = new Date();
            today.setHours(0, 0, 0, 0, 0);
            if (today > dateObj) {
                PreviousDaySelectMessagePopup();
            }
            else {
                if (CheckBlockTime(dateObj)) {
                    BlockTimeMessagePopup();
                }
                else {
                    AddSchedularPopup(dateObj, 0);
                }
            }
        },

        events: function (start, end, callback) {
            $.ajax({
                url: $URL._Scheduler,
                dataType: 'json',
                data: {},
                async: false,
                success: function (doc) {
                    var events = [];
                    $.each(doc, function (key, val) {
                        //alert(val.start);
                        if (val.EndTime != "Block") {
                            var st = new Date(val.StartTime);
                            var ed = new Date(val.EndTime);
                            //alert(st);
                            events.push({
                                id: val.SchedulerId,
                                title: val.Comment,
                                start: new Date(st),
                                end: new Date(ed),
                                allDay: false,

                            });
                        }
                        else {
                            events.push({
                                id: val.SchedulerId,
                                title: val.Comment,
                                start: val.StartTime,
                                end: val.endTime,
                                allDay: true,

                            });
                        }
                    });

                    callback(events);
                }
            });
        },
        eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) { },
        eventClick: function (calEvent, jsEvent, view) {
            if (calEvent.end == null) {
                BlockTimePopup(calEvent.id);
            } else {
                AddSchedularPopup(calEvent.scheduleDate, calEvent.id);
            }

        }

    });
    $('.fc-header-right').append("<input type='button' onclick='BlockTimePopup(0);' class='red-btn' value='Block Time' id='ButtonBlockTime' title='Create a blocked time slot' />")
}

function AddSchedularPopup(date, id) {
    $("#txtScheduleDate").datepicker("setDate", date);
    $('#txtAppointmentNotes').val('');
    $("#btnAppointment").html('');
    $('#txtScheduleDate').removeClass("input-validation-error");
    $('#txtStartTime').removeClass("input-validation-error");
    $('#txtEndTime').removeClass("input-validation-error");
    $('#txtAppointmentNotes').removeClass("input-validation-error");

    if (id == 0) {
        $('#lblOwnerNameOnEdit').css('display', 'none');
        $('#lblOwnerName').css('display', 'block');
        $('#hdnOrderDetailIdOnEdit').val('');
        $('#hdnSchedulerId').val('');
        var btn = "<input type=\"button\"  class=\"blu-btn\" value=\"Save\" id=\"btnSaveAppointment\" onclick=\"InsertOrUpdateAppointment();\" />";
        $("#btnAppointment").html(btn);
    }
    else {
        $.ajax({
            type: "POST",
            url: $URL._GetAppointmentdetails,
            async: false,
            data: { scheduleId: id },
            success: function (result) {

                var obj = jQuery.parseJSON(result);
                $('#hdnSchedulerId').val(obj.Id);
                $('#lblOwnerName').css('display', 'none');
                $('#lblOwnerNameOnEdit').text(obj.OwnerName);
                $('#lblOwnerNameOnEdit').css('display', 'block');
                $('#hdnOrderDetailIdOnEdit').val(obj.InspectionOrderId);
                $('#txtScheduleDate').val(obj.ScheduleDate);
                $('#txtStartTime').val(obj.StartTime);
                $("#txtEndTime").val(obj.EndTime);
                $("#txtAppointmentNotes").val(obj.Comment);

            },
            error: function (e) {

            },
        });


        var btn = "<input type=\"button\"  class=\"blu-btn\" value=\"Delete\" id=\"btnDeleteAppointment\" onclick=\"DeleteAppointment();\" />&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"button\"  class=\"blu-btn\" value=\"Update\" id=\"btnUpdateAppointment\" onclick=\"InsertOrUpdateAppointment();\" />";
        $("#btnAppointment").html(btn);
    }
    $('#divAddSchedular').modal({ backdrop: 'static', keyboard: true })
    $("#divAddSchedular").modal("show");
}

function PreviousDaySelectMessagePopup() {
    $('#divPreviousDay').modal({ backdrop: 'static', keyboard: true })
    $("#divPreviousDay").modal("show");
}

function BlockTimeMessagePopup() {
    $('#divBlockTimeMsg').modal({ backdrop: 'static', keyboard: true })
    $("#divBlockTimeMsg").modal("show");
}

function BlockTimePopup(id) {
    $("#btnBlock").html('');
    $("#txtBlockDate").removeClass("input-validation-error");
    $("#txtBlockNotes").removeClass("input-validation-error");
    if (id == 0) {
        $("#txtBlockDate").datepicker("setDate", new Date());
        $("#txtBlockNotes").val('');
        $('#hdnBlockId').val('');
        var btn = "<input type=\"button\"  class=\"blu-btn\" value=\"Save\" id=\"btnSaveBlockTime\" onclick=\"InsertOrUpdateBlockTime();\" />";
        $("#btnBlock").html(btn);
    }
    else {

        //_GetBlockTimeDetails
        $.ajax({
            type: "POST",
            url: $URL._GetBlockTimeDetails,
            async: false,
            data: { blockId: id },
            success: function (result) {
                var obj = jQuery.parseJSON(result);
                $('#hdnBlockId').val(obj.Id);
                $("#txtBlockDate").val(obj.BlockDate);
                $("#txtBlockNotes").val(obj.Comment);
            },
            error: function (e) {

            },
        });
        var btn = "<input type=\"button\"  class=\"blu-btn\" value=\"Delete\" id=\"btnDeleteBlockTime\" onclick=\"DeleteBlockTime();\" />&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"button\"  class=\"blu-btn\" value=\"Update\" id=\"btnUdateBlockTime\" onclick=\"InsertOrUpdateBlockTime();\" />";
        $("#btnBlock").html(btn);
    }

    $('#divBlockTime').modal({ backdrop: 'static', keyboard: true })
    $("#divBlockTime").modal("show");
}

function getQueryStringByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function InsertOrUpdateAppointment() {
    var inspectionOrderId;

    var isfocus = "";
    var ScheduleDate = $('#txtScheduleDate');
    var StartTime = $('#txtStartTime');
    var EndTime = $('#txtEndTime');
    var AppointmentNotes = $('#txtAppointmentNotes');
    if (ScheduleDate.val() == "") {
        $(ScheduleDate).addClass("input-validation-error");
        isfocus = ScheduleDate;
    }
    if (StartTime.val() == "") {
        $(StartTime).addClass("input-validation-error");
        if (isfocus == "") {
            isfocus = StartTime;
        }
    }
    if (EndTime.val() == "") {
        $(EndTime).addClass("input-validation-error");
        if (isfocus == "") {
            isfocus = EndTime;
        }
    }
    if (AppointmentNotes.val() == "") {
        $(AppointmentNotes).addClass("input-validation-error");
        if (isfocus == "") {
            isfocus = AppointmentNotes;
        }
    }
    if (isfocus != "") {
        isfocus.focus();
    } else {

        if ($('#hdnSchedulerId').val() == '') {
            inspectionOrderId = $('#hdnOrderDetailId').val();
        }
        else {
            inspectionOrderId = $('#hdnOrderDetailIdOnEdit').val();
        }
        $.ajax({
            type: "POST",
            url: $URL._InsertOrUpdateAppointment,
            async: false,
            data: { scheduleId: $('#hdnSchedulerId').val(), inspectionOrderId: inspectionOrderId, scheduleDate: $('#txtScheduleDate').val(), startTime: $('#txtStartTime').val(), endTime: $('#txtEndTime').val(), comment: $('#txtAppointmentNotes').val() },

            success: function (result) {
                $("#divAddSchedular").modal("hide");
                BindCalendar();
            },
            error: function (e) {

            },
        });
    }
}

function DeleteAppointment() {
    $.ajax({
        type: "POST",
        url: $URL._DeleteAppointment,
        async: false,
        data: { scheduleId: $('#hdnSchedulerId').val() },

        success: function (result) {
            BindCalendar();
        },
        error: function (e) {

        },
    });
}

function InsertOrUpdateBlockTime() {

    var isfocus = "";
    var BlockDate = $('#txtBlockDate');
    var BlockNotes = $('#txtBlockNotes');
    if (BlockDate.val() == "") {
        $(BlockDate).addClass("input-validation-error");
        isfocus = BlockDate;
    }
    if (BlockNotes.val() == "") {
        $(BlockNotes).addClass("input-validation-error");
        if (isfocus == "") {
            isfocus = BlockNotes;
        }
    }
    if (isfocus != "") {
        isfocus.focus();
    } else {
        $.ajax({
            type: "POST",
            url: $URL._InsertOrUpdateBlockTime,
            async: false,
            data: { blockId: $('#hdnBlockId').val(), blockDate: $('#txtBlockDate').val(), comment: $('#txtBlockNotes').val() },

            success: function (result) {
                $("#divBlockTime").modal("hide");
                BindCalendar();
            },
            error: function (e) {

            },
        });
    }
}

function DeleteBlockTime() {
    $.ajax({
        type: "POST",
        url: $URL._DeleteBlockTime,
        async: false,
        data: { blockId: $('#hdnBlockId').val() },

        success: function (result) {
            BindCalendar();
        },
        error: function (e) {

        },
    });
}

function CheckBlockTime(date) {
    var isValid = false;
    var d = new Date(date);

    $.ajax({
        type: "POST",
        url: $URL._CheckBlockTimeByDate,
        async: false,
        data: { blockDate: d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() },

        success: function (result) {
            // BindCalendar();
            if (result == "true") {
                isValid = true;
            }
        },
        error: function (e) {

        },
    });
    return isValid;
}