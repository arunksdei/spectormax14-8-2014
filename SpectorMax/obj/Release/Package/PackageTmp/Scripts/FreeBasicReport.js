﻿function showLoader() {
    $("#loadingModel").show();
}
function hideLoader() {
    $("#loadingModel").hide();
}

function ClearAll() {
    $('#Registration1').find('input:text, input:password, input:file, select').val('');
}
function getQueryStringByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


$(document).ready(function () {
    // $("#hdnPackage").val("true");
    //$("#Comprehensive").css("height", "240px");
    /*Form Submit*/
    // $("#amount").val($("#OneTimePayment").val());
    //$("#paymenttype").val("OneTimePayment")

    $("#btimseller").click(function () {

        $(".Seller").css("display", "block");
        $(".Buyer").css("display", "none");
        $(".Sellerpkg").removeClass("bluedv");
        $(".Buyerpkg").removeClass("grndv");
        $(".Sellerpkg").addClass("grndv");
        $(".Buyerpkg").addClass("bluedv");
        $("#btimseller").html("Selected");
        $("#btnimbuyer").html("Select");
        $("#Role").val($(this).attr("data-Roleid"));
    })

    $("#btnimbuyer").click(function () {

        $(".Seller").css("display", "none");
        $(".Buyer").css("display", "block");
        $(".Sellerpkg").removeClass("grndv");
        $(".Buyerpkg").removeClass("bluedv");
        $(".Sellerpkg").addClass("bluedv");
        $(".Buyerpkg").addClass("grndv ");
        $("#btimseller").html("Select");
        $("#btnimbuyer").html("Selected");
        $("#Role").val($(this).attr("data-Roleid"));
    })
    $("#btnBuyerComprehensive").click(function () {

        $("#divTwoMonthMembership").css("display", "none");
        $("#btnBuyerComprehensive").html("Selected");
        $("#btnPaymentPaln").html("Select");
        $("#btnVerification").html("Select");
        $("#btnBuyerPaln").html("Select");
        $("#amount").val($("#OneTimePayment").val());
        $("#paymenttype").val("OneTimePayment");
        $("#Comprehensive").removeClass("bluedv")
        $("#Comprehensive").css("height", "240px");
        $("#divPaymentPlan").css("height", "231px");
        $("#Verification").css("height", "231px");
        $("#Comprehensive").addClass("grndv")
        $("#divPaymentPlan").removeClass("bluedv")
        $("#divPaymentPlan").addClass("bluedv")
        $("#Verification").removeClass("grndv")
        $("#Verification").addClass("bluedv")

    });

    $("#btnPaymentPaln").click(function () {
        $("#divTwoMonthMembership").css("display", "block");
        $("#btnBuyerComprehensive").html("Select");
        $("#btnPaymentPaln").html("Selected");
        $("#btnVerification").html("Select");
        $("#amount").val($("#PurchasePlan").val());
        $("#paymenttype").val("PaymentPlan");
        $("#Comprehensive").removeClass("grndv")
        $("#Comprehensive").addClass("bluedv")
        $("#divPaymentPlan").removeClass("bluedv")
        $("#divPaymentPlan").addClass("grndv")
        $("#divPaymentPlan").css("height", "240px");
        $("#Verification").css("height", "231px");
        $("#Comprehensive").css("height", "231px");
        $("#Verification").removeClass("grndv")
        $("#Verification").addClass("bluedv");

    });

    $("#btnVerification").click(function () {
        $("#amount").val("0");
        $("#paymenttype").val("Verification");
        $("#amount").val($("#PurchasePlan").val());
        $("#btnVerification").html("Selected");
        $("#btnPaymentPaln").html("Select");
        $("#btnBuyerComprehensive").html("Select");
        $("#Comprehensive").removeClass("grndv")
        $("#Comprehensive").addClass("bluedv")
        $("#divPaymentPlan").removeClass("grndv")
        $("#divPaymentPlan").addClass("bluedv")
        $("#Verification").removeClass("bluedv")
        $("#Verification").addClass("grndv")
        $("#Verification").css("height", "240px");
        $("#divPaymentPlan").css("height", "231px");
        $("#Comprehensive").css("height", "231px");

    });



    $("#Registration1").submit(function () {
        var errors = "";
        if ($("#amount").val() == "") {
            errors += "Please select package.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");
            return false;
        }

        if ($("#cardNumber").val() != "") {
            if (isNaN($("#cardNumber").val())) {
                errors += "Card Number must be numeric.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;
            }
            else if ($("#cardNumber").val().length < 16) {
                errors += "Card Number must be 16 digit number.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;

            }
        }
        else {
            errors += "Card Number must not be empty.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");
            return false;
        }
        var year = $("#expiryYear").val()
        var minYear = parseInt(new Date().getFullYear(), 10)
        var month = parseInt($("#expiryMonth").val(), 10)
        var minMonth = new Date().getMonth() + 1
        if (!(year > minYear || (year == minYear && month >= minMonth))) {
            errors += "Your Credit Card Expiration date is invalid.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");
            return false;
        }

        if ($("#securityCode").val() == "") {
            errors += "CVV is required.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");

            return false;
        }


        if ($("#paymenttype").val() == "PaymentPlan") {
            if ($('#chkTerms').is(":checked") && $('#chkHomeInspections').is(":checked") && $('#chkTwoMonth').is(":checked")) {

            }
            else {
                if (!$('#chkTerms').is(":checked")) {
                    errors += "Please Select  Terms And Conditions.<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;
                }
                else if (!$('#chkHomeInspections').is(":checked")) {
                    errors += "Please Select Home Inspection User Agreement..<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;

                }
                else if (!$('#chkTwoMonth').is(":checked")) {
                    errors += "Please Select Two Month Membership Agreement..<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;
                }
            }
        }

        else {
            if ($('#chkTerms').is(":checked") && $('#chkHomeInspections').is(":checked")) {

            }
            else {
                if (!$('#chkTerms').is(":checked")) {
                    errors += "Please Select  Terms And Conditions.<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;
                }
                else if (!$('#chkHomeInspections').is(":checked")) {
                    errors += "Please Select Home Inspection User Agreement..<br/>";
                    $("#lblErrorExp").html(errors);
                    $(".bs-exampleExp").css("display", "block");
                    return false;

                }

            }
        }

        
        //if ($("#paymenttype").val() == "Verification")
        //{
        //    errors += "Please select package for register.<br/>";
        //    $("#lblErrorExp").html(errors);
        //    $(".bs-exampleExp").css("display", "block");
        //    return false;
        //}
    });

});
