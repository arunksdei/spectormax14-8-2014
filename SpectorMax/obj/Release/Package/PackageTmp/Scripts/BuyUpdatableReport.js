﻿
$(document).ready(function () {


    $("#btnPayment").click(function () {
        var errors = "";
        if ($("#txtCardNumber").val() != "") {
            if (isNaN($("#txtCardNumber").val())) {
                errors += "Card Number must be numeric.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;
            }
            else if ($("#txtCardNumber").val().length < 16) {
                errors += "Card Number must be 16 digit number.<br/>";
                $("#lblErrorExp").html(errors);
                $(".bs-exampleExp").css("display", "block");
                return false;

            }
        }
        else {
            errors += "Card Number must not be empty.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");
            return false;
        }
        var year = $("#ddlYear").val()
        var minYear = parseInt(new Date().getFullYear(), 10)
        var month = parseInt($("#ddlMonth").val(), 10)
        var minMonth = new Date().getMonth() + 1
        if (!(year > minYear || (year == minYear && month >= minMonth))) {
            errors += "Your Credit Card Expiration date is invalid.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");
            return false;
        }




        if ($("#txtCVV").val() == "") {
            errors += "CVV is required.<br/>";
            $("#lblErrorExp").html(errors);
            $(".bs-exampleExp").css("display", "block");
            return false;
        }
        //if (!$(':radio[name=PaymentType]').is(':checked')) {
        //    errors += "Card type must be checked.<br/>";
        //    $("#divMessage").html(errors);
        //    return false;
        //}
        //else
        //{
        //    var paymentType=$("input:radio[name='PaymentType']:checked").val();
        //    var cardNumber=$("#txtCardNumber").val()
        //    var cvvMaxlength=$("#txtCVV")
        //    if(paymentType=="Visa")
        //    {
        //        if(cardNumber.charAt(0)!=4)
        //        {

        //            errors += "Card number must be start from 4.<br/>";
        //            $("#divMessage").html(errors);
        //            return false;
        //        }
        //        else if(cvvMaxlength.val().length>3)
        //        {

        //            errors += "Cvv number should be of 3 digit.<br/>";
        //            $("#divMessage").html(errors);
        //            return false
        //        }
        //    }
        //    else if (paymentType=="American Express")
        //    {
        //        if(cvvMaxlength.val().length<4)
        //        {

        //            errors += "Cvv number should be of 4 digit.<br/>";
        //            $("#divMessage").html(errors);
        //            return false
        //        }
        //    }
        //}
        var Reportid = $("#hdnReportId").val();
        $("#btnPayment").toggleClass('active');
        $.ajax({
            type: "POST",
            url: $URL._BuyUpdateReport,
            data: { cardNumber: $("#txtCardNumber").val(), expiryMonth: $('#ddlMonth').val(), expiryYear: $("#ddlYear").val(), securityCode: $("#txtCVV").val(), amount: $("#hdnAmount").val(), ReportID: $("#hdnReportId").val(), VerificationCode: $("#txtVerification").val() },
            dataType: "Json",
            success: function (result) {
                $("#divMessage").html('');
                if (result.Message == "Success") {
                    $(".bs-exampleExp").css("display", "none");
                    $("#SucessExp").css("display", "block");
                    $("#lblSuccess").html("Payment done successfully.Please wait while you are redirecting to comprehensive report.");
                    $("#txtCountry").val('');
                    $("#txtCardNumber").val('');
                    $("#txtNameOnCard").val('');
                    $("#txtCVV").val('');
                    $("#btnPayment").removeClass('active');
                    $(location).attr("href", $URL._ViewDetailReport + "?inspectionOrderId=" + Reportid)

                }
                else if (result.Message == "NotMatched") {
                    $(".bs-exampleExp").css("display", "block");
                    $("#SucessExp").css("display", "none");
                    $("#lblErrorExp").html("Verification Code is not matched. Please enter correct code");
                    $("#txtCountry").val('');
                    $("#txtCardNumber").val('');
                    $("#txtNameOnCard").val('');
                    $("#txtCVV").val('');
                    $("#btnPayment").removeClass('active');
                }

                else {
                    $("#btnPayment").removeClass('active');
                    $(".bs-exampleExp").css("display", "block");
                    $("#lblErrorExp").html( + result.Message );
                }
            },
            error: function () {

            },
        });
    });

    function GetQueryStringParams(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

});
