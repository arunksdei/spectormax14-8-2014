/*
* jQuery File Upload Plugin JS Example 6.5.1
* https://github.com/blueimp/jQuery-File-Upload
*
* Copyright 2010, Sebastian Tschan
* https://blueimp.net
*
* Licensed under the MIT license:
* http://www.opensource.org/licenses/MIT
*/

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */
var files;
var fileInput;
$(function () {
    initFileUploads();
});
function initFileUploads() {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('.imgToUpload').fileupload({
        maxNumberOfFiles: 1,
        autoUpload: false,
        done: handleUploadResponse,
        url: $URL._ImageUpload,
        add: function (e, data) {
            FileTypeValidation(e, data, 'Image');
            data.submit();
        }
    }).bind("fileuploadadd", ImageAddHandler);
}

function ImageAddHandler(e, data) {
  
    $("#btnSvaeImage").addClass('active');
   
}
function handleUploadResponse(e, data) {
    var result;
    if ($.browser.msie) {
        result = data.result[0].body.innerText;
    }
    else {
        result = data.jqXHR.responseText
    }
    $('#imgCompanyLogo').attr('src',result);
    $("#btnSvaeImage").removeClass('active');
    
}
function FileTypeValidation(e, data, type) {

    var valid = true;
    var re;
    var Message = '';
    switch (type) {

        case "File":
            re = /^.+\.((doc)|(xls)|(xlsx)|(docx)|(pdf)|(pts))$/i;
            Message = "Only (doc)|(xls)|(xlsx)|(docx)|(pdf) flies are allowed";
            break;
        case "Image":
            re = /^.+\.((jpg)|(jpeg)|(png)|(gif)|(bmp))$/i;
            Message = 'Only (jpg)|(jpeg)|(png)|(gif)|(bmp) flies are allowed';
            break;
        case "Video":
            re = /^.+\.((mpeg)|(wmv)|(avi)|(mp4))$/i;
            Message = "Only (mpeg)|(wmv)|(avi)|(mp4) flies are allowed";
            break;
        case "Audio":
            re = /^.+\.((mp3))$/i;
            Message = "Only (mp3) flies are allowed";
            break;
        case "DocsFile":
            re = /^.+\.((doc)|(docx))$/i;
            Message = "Only (doc) or (docx) flies are allowed";
            break;
        default:
            $("#btnSvaeImage").removeClass('active');
            return false;
    }
    $.each(data.files, function (index, file) {
        if (!re.test(file.name)) {
           
            $('.bs-exampleExp').show();
            valid = false;
          //  hideLoader();
        }
    });
    if (valid) {
        data.submit();
    }
    else {
     //   hideLoader();
    }
}
function removeVehicleDocument(t) {
    var id = $(t).parents('li').prev('li').find('#Document_documentId').val();
    var postData = { id: id }
    $.ajax(
        {
            url: $_DeleteDocumentByIdUrl,
            data: postData,
            type: 'POST',
            success: function (msg) {
            }
        }
        );
    $(t).parents('li').prev('li').remove();
    $(t).parents('li').remove();

}
function openFile(path) {
    var postedData={base64String:path};
    $.ajax({
        url: $_DownloadBase64EncodedFiles,
        data:postedData,
        type: "POST",
        success: function (msg) {
            if (msg.indexOf("Failure")!=-1) {
                alert("An error occurred, Please try again later.");
            }
            else if(msg!="") {
                alert("Download complete, please find the file at " + msg);
            }
        }
    });
}