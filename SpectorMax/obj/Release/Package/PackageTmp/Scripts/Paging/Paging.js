﻿

$(document).ready(function () {
    $('.datepicker').datepicker();
    $('#tblinspectionDetail').dataTable();
   
    $(":submit").mouseout(function () { DisplayErroMessage(); });
    function DisplayErroMessage() {
        $(".field-validation-error").closest("tr").each(function () {
            $(this).find(".form").live("mouseover", function () { $(this).find(".field-validation-error .input-validation-error").attr("style", "display:block !important"); });
            $(this).find(".form").live("mouseout", function () { $(this).find(".field-validation-error .input-validation-error").attr("style", "display:none !important"); });
        });
    }

    $("#btnSearch").click(function () {
        if ($("#txtFromDate").val() != "" && $("#txtTodate").val() != "") {
            var PerPageRecord = $("#hdnPerPageRecord").val();
            loadPartialView1(0, 20, false);
        }
    });
});


function Pagging(val) {
    var totalrecord = $("#hdnTotalRecord").val();   
    var PageNumber = $("#currentPage").val();
    var PerPageRecord = $("#hdnPerPageRecord").val();
    var totalpage = $("#hdnTotalPage").val();
    var islast = false;
    if (val == '0') {
        if (parseInt(PageNumber, 10) != 1) {
            PageNumber = 1;
        }
        else
            PageNumber = 0;
    }
    else if (val == '-1') {
        PageNumber = parseInt(PageNumber, 10) - 1;
    }
    else if (val == '+1') {
        PageNumber = parseInt(PageNumber, 10) + 1;
    }
    else if (val == '2') {
        islast = true;
        if (parseInt(PageNumber, 10) == parseInt(totalpage, 10)) {
            PageNumber = parseInt(PageNumber, 10) + 1;
        }
    }

    if (parseInt(PageNumber, 10) > 0 && parseInt(totalpage, 10) >= PageNumber) {
        loadPartialView1(PageNumber, PerPageRecord, islast);
    }

}

function Pagging1(perpage) {
    $("#hdnPerPageRecord").val(perpage);
    var totalrecord = $("#hdnTotalRecord").val();
    var PageNumber = 1;
    var PerPageRecord = $("#hdnPerPageRecord").val();

    loadPartialView1(PageNumber, PerPageRecord, false);
}
function Pagging2(val) {
    var totalrecord = $("#hdnTotalRecord").val();
    var PageNumber = $("#currentPage").val();
    var PerPageRecord = $("#hdnPerPageRecord").val();
    var islast = false;
    if (val == '0') {
        PageNumber = 1;
    }
    else {
        PageNumber = (parseInt(PageNumber, 10));
    }
    loadPartialView1(PageNumber, PerPageRecord, islast);
}

function loadPartialView1(PageNumber, PerPageRecord, islast) {
    if ($("#txtFromDate").val() != "" && $("#txtTodate").val() != "") {
        var fromdate = $("#txtFromDate").val().replace(/\-/g, '');
        var todate = $("#txtTodate").val().replace(/\-/g, '');
        if (todate < fromdate) {
            ShowAlertMessage("title", "ToDate must be greater than or equal to FromDate");
            return false;
        }
    }
    
    $.ajax({
        type: "Post",
        url: $URL._FilterInspectionOrderDetials,
        data: {
            PageNumber: PageNumber,
            PerPageRecord: PerPageRecord,
            islast: islast,
            startDate: $("#txtFromDate").val(),
            endDate: $("#txtTodate").val()
        },
        dataType: "Json",
        success: function (result) {
           
            $("#container").html(result.Result);
           
        
        },
        error: function () { hideLoader(); },
    });
}
function showLoader() {
    $("#loadingModel").show();
}
function hideLoader() {
    $("#loadingModel").hide();
}